#################################################################
Deployment Guide
1. Login with correct username (SANDBOX: eddie_li@vfc.com.testing / salesforce1)
2. Not used 2 target anymore.  Select VF build target, then edit bundle ID
For production, select release mode, Bundle ID=com.vfc.laputab
For sandbox, select debug mode, Bundle ID=com.vfc.laputab.dev


#################################################################
(Not Phase 1)
- Management View
	- Performance between country > Cross brand
								  > Within brand > Cross DS > Individual Store
- Staff Management
	- Store Performance by staff
	- Roster
	- Staff list per store
	- Staff Chart
- Customer Management
	- CRM performance report
	- VIP program


