//
//  SettingViewController.m
//  VF Ordering App
//
//  Created by Developer 2 on 3/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import <sys/utsname.h>
#import "SettingViewController.h"
#import "AppDelegate.h"
#import "ShareLocale.h"
#import "GeneralUiTool.h"
#import "UIHelper.h"
#import "HomeViewController.h"
#import "Constants.h"
#import "SFAuthenticationManager.h"
#import <MessageUI/MessageUI.h>

@interface SettingViewController ()

@end

@implementation SettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSUserDefaults *localCache = [NSUserDefaults standardUserDefaults];
    
    self.countryCodeMap = [NSMutableDictionary dictionary];
    for(NSInteger i=701; i<=707; i++){
        UIButton* countryButton = (UIButton*)[self.view viewWithTag:i];
        NSString* countryCode = countryButton.titleLabel.text;
        [self.countryCodeMap setObject:[NSNumber numberWithInteger:i] forKey:countryCode];
        countryButton.alpha = .5f;
        countryButton.userInteractionEnabled = NO;
    }
    
    self.theme1Button.alpha = .5f;
    self.theme1Button.userInteractionEnabled = NO;
    self.theme2Button.alpha = .5f;
    self.theme2Button.userInteractionEnabled = NO;
    self.theme3Button.alpha = .5f;
    self.theme3Button.userInteractionEnabled = NO;
    self.theme4Button.alpha = .5f;
    self.theme4Button.userInteractionEnabled = NO;
    self.theme5Button.alpha = .5f;
    self.theme5Button.userInteractionEnabled = NO;
    self.theme6Button.alpha = .5f;
    self.theme6Button.userInteractionEnabled = NO;
    self.theme7Button.alpha = .5f;
    self.theme7Button.userInteractionEnabled = NO;
    
    
    self.userLeftLabel.text = [ShareLocale textFromKey:@"setting_user"];
    self.versionLeftLabel.text = [ShareLocale textFromKey:@"setting_version"];
    self.brandLeftLabel.text = [ShareLocale textFromKey:@"setting_brand"];
    self.countryLeftLabel.text = [ShareLocale textFromKey:@"setting_country"];
    self.languageLeftLabel.text = [ShareLocale textFromKey:@"setting_lang"];

    
    
    NSArray* availAccount = [localCache objectForKey: AVAIL_ACCOUNT_CODES];
    if(availAccount!=nil){
        for(NSString* acctCode in availAccount){
            if([acctCode isEqualToString: ACCOUNT_TBL_CODE]){
                self.theme1Button.userInteractionEnabled = YES;
            }
            else if ([acctCode isEqualToString: ACCOUNT_VAN_CODE]){
                self.theme2Button.userInteractionEnabled = YES;
            }
            else if ([acctCode isEqualToString: ACCOUNT_KTG_CODE]){
                self.theme3Button.userInteractionEnabled = YES;
            }
            else if ([acctCode isEqualToString: ACCOUNT_LEE_CODE]){
                self.theme4Button.userInteractionEnabled = YES;
            }
            else if ([acctCode isEqualToString: ACCOUNT_TNF_CODE]){
                self.theme5Button.userInteractionEnabled = YES;
            }
            else if ([acctCode isEqualToString: ACCOUNT_WGR_CODE]){
                self.theme6Button.userInteractionEnabled = YES;
            }
            else if ([acctCode isEqualToString: ACCOUNT_NAP_CODE]){
                self.theme7Button.userInteractionEnabled = YES;
            }
        }
        
        NSString* selectedAccountCode = [localCache objectForKey: USER_PREFER_ACCOUNT_CODE];
        if(selectedAccountCode!=nil){
            if([selectedAccountCode isEqualToString: ACCOUNT_TBL_CODE]){
                [self setTheme: ACCOUNT_TBL_TAG ];
            }
            else if([selectedAccountCode isEqualToString: ACCOUNT_VAN_CODE]){
                [self setTheme: ACCOUNT_VAN_TAG ];
            }
            else if([selectedAccountCode isEqualToString: ACCOUNT_KTG_CODE]){
                [self setTheme: ACCOUNT_KTG_TAG ];
            }
            else if([selectedAccountCode isEqualToString: ACCOUNT_LEE_CODE]){
                [self setTheme: ACCOUNT_LEE_TAG ];
            }
            else if([selectedAccountCode isEqualToString: ACCOUNT_TNF_CODE]){
                [self setTheme: ACCOUNT_TNF_TAG ];
            }
            else if([selectedAccountCode isEqualToString: ACCOUNT_WGR_CODE]){
                [self setTheme: ACCOUNT_WGR_TAG ];
            }
            else if([selectedAccountCode isEqualToString: ACCOUNT_NAP_CODE]){
                [self setTheme: ACCOUNT_NAP_TAG ];
            }
        }else{
            selectedAccountCode = ACCOUNT_TBL_CODE;
            [self setTheme: ACCOUNT_TBL_TAG];
        }
        
        NSString* selectedCountryCode = [localCache objectForKey:USER_PREFER_COUNTRY_CODE];
        if(selectedCountryCode==nil){
            NSArray* availCountry = [localCache objectForKey: AVAIL_COUNTRY_CODES];
            for(NSString* ccode in availCountry){
                int tag = [self.countryCodeMap[ccode] intValue];
                UIButton* countryButton = (UIButton*)[self.view viewWithTag: tag];
                countryButton.alpha = 1;
                countryButton.userInteractionEnabled = NO;
                [self setCountry: ccode ];
                break;
            }
        }
        

        
//        NSArray* uniqueCodes = [localCache objectForKey: AVAIL_BRAND_UNIQUE_CODES];
//        if(uniqueCodes!=nil && availCountry!=nil){
//            for(NSString* ucode in uniqueCodes){
//                if( userPreferredUniqueCode iseq ){
//                    
//                }
//            }
//        }
    }
    
    
    NSString* userName = [localCache objectForKey:@"userName"];
    if(userName==nil){
        userName = @"Guest";
    }
    
    SFNetworkEngine* networkEngine = [SFNetworkEngine sharedInstance];
    NSString* remoteHost = [networkEngine.remoteHost lowercaseString];
    
    NSString* sandbox = @"";
    if( [remoteHost rangeOfString:@"test"].location != NSNotFound
       || [remoteHost rangeOfString:@"cs6"].location != NSNotFound
       || [remoteHost rangeOfString:@"cs5"].location != NSNotFound ){
        sandbox = @"(Sandbox)";
    }
    else{
        sandbox = @"";
    }
    
    self.userLabel.text = userName; //[ShareLocale textFromKey:@"setting_user"]
    self.versionLabel.text =[NSString stringWithFormat:@"%.2f %@", [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion" ] floatValue], sandbox];

    
    [self refreshBackground];
    
    
    NSString* lang = [[ShareLocale sharedLocalSystem] getLanguage];
    if(lang==nil){
        [self updateLanguageButton:503];
    }
    else if ([lang isEqualToString:@"zh-Hant"]){
        [self updateLanguageButton:501];
    }
    else if ([lang isEqualToString:@"zh-Hans"]){
        [self updateLanguageButton:502];
    }
    else if ([lang isEqualToString:@"ja"]){
        [self updateLanguageButton:504];
    }
    else if ([lang isEqualToString:@"ko"]){
        [self updateLanguageButton:505];
    }
    else{
        [self updateLanguageButton:503];    
    }
    
    
    [self refreshLocale];
}



-(void) reloadCountryButtonStatusOfCountry:(NSString*)country{
    NSUserDefaults *localCache = [NSUserDefaults standardUserDefaults];
    
    for(NSInteger i=701; i<=707; i++){
        UIButton* countryButton = (UIButton*)[self.view viewWithTag:i];
        if(![countryButton.titleLabel.text isEqualToString:country]){
            countryButton.alpha = .5f;
        }else{
            countryButton.alpha = 1;
        }
        countryButton.userInteractionEnabled = NO;
    }

    NSString* selectedAccountCode = [localCache objectForKey: USER_PREFER_ACCOUNT_CODE];
    NSArray* uniqueCodes = [localCache objectForKey: AVAIL_BRAND_UNIQUE_CODES];
    
    for(NSString* ucode in uniqueCodes){
        NSString* acode = [ucode substringToIndex:3];
        NSString* ccode = [ucode substringFromIndex:3];
        if([acode isEqualToString:selectedAccountCode]){
            int tag = [self.countryCodeMap[ccode] intValue];
            UIButton* countryButton = (UIButton*)[self.view viewWithTag: tag];
            countryButton.userInteractionEnabled = YES;
            
        }
    }
    

    
}


-(void)updateLanguageButton:(int)index{
    self.lang1Button.alpha = 0.5;
    self.lang2Button.alpha = 0.5;
    self.lang3Button.alpha = 0.5;
    self.lang4Button.alpha = 0.5;
    self.lang5Button.alpha = 0.5;
    if(501==index){
        self.lang1Button.alpha = 1;
    }
    else if (502==index){
        self.lang2Button.alpha = 1;
    }
    else if(504==index){
        self.lang4Button.alpha = 1;
    }
    else if (505==index){
        self.lang5Button.alpha = 1;
    }
    else{
        self.lang3Button.alpha = 1;
    }
        
}



-(void) setLanguage:(int)index{
    
    
    if(index==501){
        [[ShareLocale sharedLocalSystem] setLanguage:@"zh-Hant"];
    }
    else if(index==502){
        [[ShareLocale sharedLocalSystem] setLanguage:@"zh-Hans"];
    }
    else if(index==504){
        [[ShareLocale sharedLocalSystem] setLanguage:@"ja"];
    }
    else if(index==505){
        [[ShareLocale sharedLocalSystem] setLanguage:@"ko"];
    }
    else{//503
        [[ShareLocale sharedLocalSystem] setLanguage:@"en"];
    }
    [self updateLanguageButton:index];
    
    
}



-(void) setTheme:(int)index{
    self.theme1Button.alpha = 0.5;
    self.theme2Button.alpha = 0.5;
    self.theme3Button.alpha = 0.5;
    self.theme4Button.alpha = 0.5;
    self.theme5Button.alpha = 0.5;
    self.theme6Button.alpha = 0.5;
    self.theme7Button.alpha = 0.5;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if(index==ACCOUNT_TBL_TAG){
        self.theme1Button.alpha = 1;
        [defaults setValue:ACCOUNT_TBL_CODE forKey: USER_PREFER_ACCOUNT_CODE];
    }
    else if(index==ACCOUNT_VAN_TAG){
        self.theme2Button.alpha = 1;
        [defaults setValue:ACCOUNT_VAN_CODE forKey: USER_PREFER_ACCOUNT_CODE];
    }
    else if(index==ACCOUNT_KTG_TAG){
        self.theme3Button.alpha = 1;
        [defaults setValue:ACCOUNT_KTG_CODE forKey: USER_PREFER_ACCOUNT_CODE];
    }
    else if(index==ACCOUNT_LEE_TAG){
        self.theme4Button.alpha = 1;
        [defaults setValue:ACCOUNT_LEE_CODE forKey: USER_PREFER_ACCOUNT_CODE];
    }
    else if(index==ACCOUNT_TNF_TAG){
        self.theme5Button.alpha = 1;
        [defaults setValue:ACCOUNT_TNF_CODE forKey: USER_PREFER_ACCOUNT_CODE];
    }
    else if(index==ACCOUNT_WGR_TAG){
        self.theme6Button.alpha = 1;
        [defaults setValue:ACCOUNT_WGR_CODE forKey: USER_PREFER_ACCOUNT_CODE];
    }
    else if(index==ACCOUNT_NAP_TAG){
        self.theme7Button.alpha = 1;
        [defaults setValue:ACCOUNT_NAP_CODE forKey: USER_PREFER_ACCOUNT_CODE];
    }
    [defaults synchronize];
    
    
    [self reloadCountryButtonStatusOfCountry: [defaults objectForKey:USER_PREFER_COUNTRY_CODE]];

}

-(void) refreshLocale{
    
    self.headerTitleLabel.text = [ShareLocale textFromKey:@"setting_title"];
    [self.logoutButton setTitle:[ShareLocale textFromKey:@"setting_logout"] forState:UIControlStateNormal];
    [self.bugReportButton setTitle:[ShareLocale textFromKey:@"setting_bug_report"] forState:UIControlStateNormal];
    
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_backButton release];
    [_logoutButton release];
    [_theme1Button release];
    [_theme2Button release];
    [_theme3Button release];
    [_theme4Button release];
    [_theme5Button release];
    [_theme6Button release];
    [_theme7Button release];

    [_versionLabel release];
    [_userLabel release];
    [_lang1Button release];
    [_lang2Button release];
    [_lang3Button release];
    [_headerTitleLabel release];
    [_bugReportButton release];

    [_userLeftLabel release];
    [_versionLeftLabel release];
    [_brandLeftLabel release];
    [_countryLeftLabel release];
    [_languageLeftLabel release];
    [_lang4Button release];
    [_lang5Button release];
    [super dealloc];
}

- (IBAction)onBackButtonClicked:(id)sender {
    
    NSUserDefaults *localCache = [NSUserDefaults standardUserDefaults];
    NSString* countryCode = [localCache objectForKey: USER_PREFER_COUNTRY_CODE];
    if(countryCode!=nil){
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"" message:[ShareLocale textFromKey:@"warning_must_select_country"]];
    }
    

}

- (IBAction)onContactButtonClicked:(id)sender {
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil) {
        
        struct utsname systemInfo;
        
        uname(&systemInfo);
        
        NSString *modelName = [NSString stringWithCString:systemInfo.machine
                                                 encoding:NSUTF8StringEncoding];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString* userName = [defaults objectForKey:@"userName"];
        // Email Subject
        NSString *emailTitle = [NSString stringWithFormat: @"VF Store Performance - Bug Report Email from %@", userName];
        // Email Content
        NSString *messageBody = [NSString stringWithFormat: @"Bug Report Email from %@\r\nApp Version:%.2f\r\nModel Name:%@ ", userName, [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion" ] floatValue], modelName];
        // To address
        NSArray *toRecipents = [NSArray arrayWithObject:@"support@laputab.com"];
    
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
    
        if([mailClass canSendMail]) {
            // Present mail view controller on screen
            [self presentViewController:mc animated:YES completion:NULL];
        }
    }
}


-(void) setCountry:(NSString*)country{
    

    [self reloadCountryButtonStatusOfCountry:country];


    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:country forKey:USER_PREFER_COUNTRY_CODE];
    [defaults synchronize];

    int tag =  [self.countryCodeMap[country] intValue];;
    UIButton* selectedButton = (UIButton*)[self.view viewWithTag:tag];
    
    selectedButton.userInteractionEnabled = NO;
    selectedButton.alpha = 1;
    
    for(NSInteger i=701; i<=707; i++){
        UIButton* button = (UIButton*)[self.view viewWithTag:i];
        if(button!=selectedButton){
            button.alpha = .5f;
        }
    }
    
}

- (IBAction)onCountryButtonClicked:(id)sender {
    
    UIButton* button = (UIButton*)sender;
    NSString* ccode = button.titleLabel.text; //TODO: should replace by another map search later
    [self setCountry: ccode ];
    
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)onThemeChanged:(id)sender {
    UIView* view = (UIView*) sender;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults  removeObjectForKey:USER_PREFER_COUNTRY_CODE];
    [defaults synchronize];
    
    [self setTheme: view.tag ];
    [self refreshBackground];
}


- (IBAction)onLanguageChanged:(id)sender {
    UIView* view = (UIView*) sender;

    [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"" message:[ShareLocale textFromKey:@"dialog_change_lang"] yesHanlder:^(id data) {
       
        [self setLanguage:view.tag ];
        [self.navigationController popToRootViewControllerAnimated:YES];
        
    } noHandler:^(id data) {
        
        
    }];
    
}

-(void) clearLocalCache{
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [defs dictionaryRepresentation];
    for (id key in dict) {
        [defs removeObjectForKey:key];
    }
    [defs synchronize];
    
    VFDAO* vf = [VFDAO sharedInstance];
    vf.cache = [NSMutableDictionary dictionary];
    


}

- (IBAction)onLogoutButtonClicked:(id)sender {
    
    [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"" message:[ShareLocale textFromKey:@"dialog_confirm_logout"] yesHanlder:^(id data) {
        [[SFAuthenticationManager sharedManager] logout];
        [self.navigationController popViewControllerAnimated:YES];
    } noHandler:^(id data) {
        
        
    }];
    
}



@end
