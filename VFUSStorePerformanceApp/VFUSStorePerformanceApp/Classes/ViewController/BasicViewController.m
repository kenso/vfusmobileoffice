//
//  BasicViewController.m
//
//  Created by Developer 2 on 4/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import "BasicViewController.h"
#import "SFRestAPI.h"
#import "VFDAO.h"
#import "DataHelper.h"
#import "ShareLocale.h"
#import "GeneralUiTool.h"
#import "Constants.h"
#import "UIHelper.h"
#import "CommentSidebarView.h"

@interface BasicViewController ()

@end

@implementation BasicViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self refreshBackground];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void) viewDidDisappear:(BOOL)animated{
    if(self.commentSidebar!=nil){
        self.commentSidebar.needReload = YES;
        [self.commentSidebar hideSidebar];
    }
    [super viewDidDisappear:animated];
}

-(void) dealloc{
    [_lodingView release];
    [_commentSidebar release];
    [super dealloc];
}



#pragma mark - Helper UI Method

/**
 *  Comment Popup
 */
-(void) initCommentSidebarWithStoreId:(NSString*)storeId{
    if(self.commentSidebar==nil){
        CommentSidebarView* sidebar = [CommentSidebarView newInstance];
        self.commentSidebar = sidebar;
        sidebar.frame = CGRectMake(1024-30, 110, sidebar.frame.size.width, sidebar.frame.size.height);
        sidebar.controllerDelegate = self;
        [sidebar selfBinding];
        sidebar.selectedStoreId = storeId;
        sidebar.needReload = YES;
        
        [self.view addSubview: sidebar];
        //[sidebar release];
    }
}

/**
 * Loading Popup
 */


-(void) changeLoadingPopupText:(NSString*)text{
    if(self.lodingView!=nil){
        if([NSThread isMainThread]){
            UILabel* label = (UILabel*)[self.lodingView viewWithTag:1023376];
            label.text = text;
        }else{
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                UILabel* label = (UILabel*)[self.lodingView viewWithTag:1023376];
                label.text = text;
            });
        }
    }
}

-(void) showLoadingPopup:(UIView*)parent{
    [self showLoadingPopup:parent text:[ShareLocale textFromKey:@"loading..."]];
}


-(void) showLoadingPopup:(UIView*)parent text:(NSString*)text{
    
    if([NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [self showLoadingPopupInMainThread:parent text:text];
        });
    }else{
            [self showLoadingPopupInMainThread:parent text:text];
    }
}


-(void) showLoadingPopupInMainThread:(UIView*)parent text:(NSString*)text{

//    NSLog(@"isIgnoringInteractionEvents before #1.1 %@", [UIApplication sharedApplication].isIgnoringInteractionEvents?@"YES":@"NO");

    [[GeneralUiTool shareInstance] pauseUserInteraction];

    
//    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];

//    NSLog(@"isIgnoringInteractionEvents after #1.2 %@", [UIApplication sharedApplication].isIgnoringInteractionEvents?@"YES":@"NO");

    
    if(self.lodingView==nil){
        int viewSizeW = 240;
        int viewSizeH = 80;
        float w = parent.frame.size.width;
        float h = parent.frame.size.height;

        UIView* container = [[UIView alloc] initWithFrame: CGRectMake(w/2 - viewSizeW/2, h/2 - viewSizeH/2, viewSizeW, viewSizeH)] ;
        container.alpha = 1;
        container.backgroundColor = [UIColor colorWithWhite:1 alpha:0.3];

        int txtViewW = 200;
        int txtViewH = 60;
        UILabel* textView = [[UILabel alloc] initWithFrame:CGRectMake(viewSizeW/2 - txtViewW/2, viewSizeH/2 - txtViewH/2, txtViewW, txtViewH)];
        textView.font = [UIFont fontWithName:@"Helvetica Neue" size:24];
        textView.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        textView.textAlignment = NSTextAlignmentCenter;
        textView.minimumScaleFactor = .6f;
        textView.adjustsFontSizeToFitWidth = YES;
        textView.text = text;
        textView.textColor = [UIColor colorWithWhite:1 alpha:1];
        textView.layer.shadowColor = [[UIColor blackColor] CGColor];
        textView.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
        textView.layer.shadowOpacity = 1.0f;
        textView.layer.shadowRadius = 1.0f;
        textView.tag = 1023376;


        [container addSubview:textView];
        [textView release];
        
        [parent addSubview:container];
        
        container.layer.masksToBounds = NO;
        container.layer.shadowOffset = CGSizeMake(5, 5);
        container.layer.shadowRadius = 3;
        container.layer.shadowOpacity = 0.8;
        
        self.lodingView =container;
        
        [container release];
    }

}

-(void) hideLoadingPopup{
    if(![NSThread isMainThread]){
        [self performSelectorOnMainThread:@selector(hideLoadingPopup) withObject:nil waitUntilDone:NO];
        return;
    }
//    NSLog(@"isIgnoringInteractionEvents #2.1 before %@", [UIApplication sharedApplication].isIgnoringInteractionEvents?@"YES":@"NO");

    //[[UIApplication sharedApplication] endIgnoringInteractionEvents];
    
    [[GeneralUiTool shareInstance] resumeUserInteraction];
    
    if(self.lodingView!=nil){
        [self.lodingView removeFromSuperview];
        self.lodingView = nil;
    }
//    NSLog(@"isIgnoringInteractionEvents #2.2 after %@", [UIApplication sharedApplication].isIgnoringInteractionEvents?@"YES":@"NO");
}

-(void) hideLoadingPopupWithFadeout{
    if(![NSThread isMainThread]){
        [self performSelectorOnMainThread:@selector(hideLoadingPopupWithFadeout) withObject:nil waitUntilDone:NO];
        return;
    }

    if(self.lodingView!=nil){
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
        [self.lodingView setAlpha:0.0];
        [UIView commitAnimations];
        [self performSelector:@selector(hideLoadingPopup) withObject:nil afterDelay:0.3];

    }else{
//        NSLog(@"isIgnoringInteractionEvents before #2.3 %@", [UIApplication sharedApplication].isIgnoringInteractionEvents?@"YES":@"NO");
        [[GeneralUiTool shareInstance] resumeUserInteraction];

//        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
//        NSLog(@"isIgnoringInteractionEvents after #2.4 %@", [UIApplication sharedApplication].isIgnoringInteractionEvents?@"YES":@"NO");

    }
}

/**
 * Label & Button
 */

-(UITextField*) createNewTextFieldWithDefaultStyle:(CGRect)rect parent:(UIView*)parent tag:(NSInteger) tag{
    return [self createNewTextFieldWithDefaultStyle:rect parent:parent tag:tag alignment:NSTextAlignmentCenter vAlignment:UIControlContentVerticalAlignmentCenter];
}
-(UITextField*) createNewTextFieldWithDefaultStyle:(CGRect)rect parent:(UIView*)parent tag:(NSInteger) tag alignment:(NSTextAlignment)alignment{
    return [self createNewTextFieldWithDefaultStyle:rect parent:parent tag:tag alignment:alignment vAlignment:UIControlContentVerticalAlignmentCenter];
}

-(UITextField*) createNewTextFieldWithDefaultStyle:(CGRect)rect parent:(UIView*)parent tag:(NSInteger) tag alignment:(NSTextAlignment)alignment vAlignment:(UIControlContentVerticalAlignment)vAlignment{
    UITextField* tempText = [[[UITextField alloc] initWithFrame: rect] autorelease];
    tempText.textColor = [UIColor colorWithWhite:.95 alpha:1];
    tempText.font = [UIFont fontWithName:@"Helvetica Neue" size:14];
    tempText.tag = tag;
    tempText.textAlignment = alignment;
    tempText.contentVerticalAlignment = vAlignment;
    tempText.backgroundColor = [UIColor colorWithWhite:1 alpha:.2f];
    [tempText addTarget:self action:@selector(textChanged:) forControlEvents:UIControlEventEditingDidEnd];
    [parent addSubview:tempText];
    return tempText;
}


-(UILabel*) createNewTextLabelWithDefaultStyle:(CGRect)rect parent:(UIView*)parent tag:(NSInteger) tag{
    return [self createNewTextLabelWithDefaultStyle:rect parent:parent tag:tag alignment:NSTextAlignmentLeft];
}

-(UILabel*) createNewTextLabelWithDefaultStyle:(CGRect)rect parent:(UIView*)parent tag:(NSInteger)tag alignment:(NSTextAlignment)alignment{
    UILabel* tempText = [[[UILabel alloc] initWithFrame: rect] autorelease];
    tempText.textColor = [UIColor colorWithWhite:.95 alpha:1];
    tempText.font = [UIFont fontWithName:@"Helvetica Neue" size:14];
    tempText.tag = tag;
    tempText.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
    tempText.textAlignment = alignment;
    [parent addSubview:tempText];
    return tempText;
    
}

-(UIButton*) createNewButtonWithDefaultStyle:(CGRect)rect parent:(UIView*)parent tag:(NSInteger) tag{
    UIButton* tempbutton = [[[UIButton alloc] initWithFrame: rect] autorelease];
    tempbutton.titleLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:14];
    [tempbutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [tempbutton setTitle:@"xxxxxx" forState:UIControlStateNormal];
    tempbutton.tag = tag;
    [UIHelper applyGreenTinyButtonImageOnButton:tempbutton];
    [parent addSubview:tempbutton];
    return tempbutton;
}


-(UIButton*) createNewButton:(CGRect)rect parent:(UIView*)parent tag:(NSInteger) tag color:(int)colorIndex{
    UIButton* tempbutton = [[[UIButton alloc] initWithFrame: rect] autorelease];
    tempbutton.titleLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:14];
    [tempbutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [tempbutton setTitle:@"xxxxxx" forState:UIControlStateNormal];
    tempbutton.tag = tag;
    if(colorIndex==0){
        [UIHelper applyGreenTinyButtonImageOnButton:tempbutton];
    }
    else if (colorIndex==1){
        [UIHelper applyBlueTinyButtonImageOnButton:tempbutton];
    }
    else if (colorIndex==2){
        [UIHelper applyYellowTinyButtonImageOnButton:tempbutton];
    }
    else if (colorIndex==3){
        [UIHelper applyPurpleTinyButtonImageOnButton:tempbutton];
    }
    else if (colorIndex==4){
        [UIHelper applyOrangeTinyButtonImageOnButton:tempbutton];
    }
    [parent addSubview:tempbutton];
    return tempbutton;
}



#pragma mark - Helper SF Data Method
-(NSString*) getFieldDisplayWithFieldName:(NSString*)fieldName withDictionary:(NSDictionary*) data{
    return [DataHelper getFieldDisplayWithFieldName:fieldName withDictionary: data];
}




#pragma mark - Theme automation
-(void) refreshBackground{
    
    //Logo
    NSString* tblImgURL = @"timberland_black_small.png";
    NSString* vansImgURL = @"vans_logo_small.jpg";
    NSString* kiplingImgURL = @"kipling_logo.jpg";
    NSString* leeImgURL = @"lee_logo.png";
    NSString* tnfImgURL = @"northface_logo.png";
    NSString* wgrImgURL = @"wrangler_logo.png";
    NSString* napImgURL = @"napa_logo.png";
    
    //background
    NSString* tblBgURL = @"timberland_bg.jpg";
    NSString* vansBgURL = @"vans_bg.jpg";
    NSString* kiplingBgURL = @"kipling_bg.jpg";
    NSString* leeBgURL = @"general_bg.jpg";
    NSString* tnfBgURL = @"tnf_bg.jpg";
    NSString* wgrBgURL = @"general_bg.jpg";
    NSString* napBgURL = @"napa_bg.png";

    
    //background
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* theme = [defaults objectForKey: USER_PREFER_ACCOUNT_CODE];
    UIImageView* bgView = (UIImageView*)[self.view viewWithTag:999];
    if(theme!=nil){
        if([theme isEqualToString: ACCOUNT_TBL_CODE]){
            [bgView setImage: [UIImage imageNamed:tblBgURL]];
        }
        else if([theme isEqualToString: ACCOUNT_VAN_CODE]){
            [bgView setImage: [UIImage imageNamed:vansBgURL]];
        }
        else if([theme isEqualToString: ACCOUNT_KTG_CODE]){
            [bgView setImage: [UIImage imageNamed:kiplingBgURL]];
        }
        else if([theme isEqualToString: ACCOUNT_LEE_CODE]){
            [bgView setImage: [UIImage imageNamed:leeBgURL]];
        }
        else if([theme isEqualToString: ACCOUNT_TNF_CODE]){
            [bgView setImage: [UIImage imageNamed:tnfBgURL]];
        }
        else if([theme isEqualToString: ACCOUNT_WGR_CODE]){
            [bgView setImage: [UIImage imageNamed:wgrBgURL]];
        }
        else if([theme isEqualToString: ACCOUNT_NAP_CODE]){
            [bgView setImage: [UIImage imageNamed:napBgURL]];
        }
        else{
            [bgView setImage: [UIImage imageNamed:@"general_bg.jpg"]];
        }
    }
    else{
        [bgView setImage: [UIImage imageNamed:@"general_bg.jpg"]];
    }
    
    //Logo for every page
    UIImageView* logoView = (UIImageView*)[self.view viewWithTag:600];
    logoView.backgroundColor = [UIColor whiteColor];

    
    if(logoView!=nil){
        if(theme!=nil){
            if([theme isEqualToString: ACCOUNT_TBL_CODE]){
                logoView.backgroundColor = [UIColor blackColor];
                [logoView setImage: [UIImage imageNamed:tblImgURL]];
                logoView.frame = CGRectMake(789, 20, 223, 45);
            }
            else if([theme isEqualToString: ACCOUNT_VAN_CODE]){
                logoView.backgroundColor = [UIColor whiteColor];
                [logoView setImage: [UIImage imageNamed:vansImgURL]];
                logoView.frame = CGRectMake(789+153, 20, 70, 45);
            }
            else if([theme isEqualToString: ACCOUNT_KTG_CODE]){
                logoView.backgroundColor = [UIColor whiteColor];
                [logoView setImage: [UIImage imageNamed:kiplingImgURL]];
                logoView.frame = CGRectMake(789+106, 20, 117, 45);
            }
            else if([theme isEqualToString: ACCOUNT_LEE_CODE]){
                logoView.backgroundColor = [UIColor whiteColor];
                [logoView setImage: [UIImage imageNamed:leeImgURL]];
                logoView.frame = CGRectMake(789+117, 20, 106, 45);
            }
            else if([theme isEqualToString: ACCOUNT_TNF_CODE]){
                logoView.backgroundColor = [UIColor whiteColor];
                [logoView setImage: [UIImage imageNamed: tnfImgURL]];
                logoView.frame = CGRectMake(789+117, 20, 106, 45);
            }
            else if([theme isEqualToString: ACCOUNT_WGR_CODE]){
                logoView.backgroundColor = [UIColor whiteColor];
                [logoView setImage: [UIImage imageNamed: wgrImgURL]];
                logoView.frame = CGRectMake(789+117, 20, 106, 45);
            }
            else if([theme isEqualToString: ACCOUNT_NAP_CODE]){
                logoView.backgroundColor = [UIColor whiteColor];
                [logoView setImage: [UIImage imageNamed: napImgURL]];
                logoView.frame = CGRectMake(789+117, 20, 106, 45);
            }
        }
        else{
            logoView.backgroundColor = [UIColor whiteColor];
            [logoView setImage: [UIImage imageNamed:kiplingImgURL]];
            logoView.frame = CGRectMake(789+117, 20, 106, 45);
        }
    }
    
    
}




#pragma mark - Default Network error handling
- (void)request:(SFRestRequest *)request didFailLoadWithError:(NSError*)error{
    [self popupErrorDialogWithMessage: [NSString stringWithFormat:@"%@: %@",[ShareLocale textFromKey:@"error"], error]];
    
    [self hideLoadingPopupWithFadeout];
}

- (void)requestDidCancelLoad:(SFRestRequest *)request{
    [self popupErrorDialogWithMessage: [ShareLocale textFromKey:@"error_request_cancelled"]];

    [self hideLoadingPopupWithFadeout];
}

- (void)requestDidTimeout:(SFRestRequest *)request{
    [self popupErrorDialogWithMessage: [ShareLocale textFromKey:@"error_timeout"]];

    [self hideLoadingPopupWithFadeout];
}



- (void)popupErrorDialogWithMessage:(NSString*)message{
    if( ![NSThread isMainThread] ){
        [self performSelectorOnMainThread:@selector(popupErrorDialogWithMessage:) withObject:message waitUntilDone:NO];
        return;
    }
    UIAlertView* alert = [[[UIAlertView alloc] initWithTitle:[ShareLocale textFromKey:@"error"] message:message delegate:self cancelButtonTitle:[ShareLocale textFromKey: @"OK"] otherButtonTitles: nil] autorelease];
    [alert show];
}

- (NSNumberFormatter*)getSimplePriceFormatter{
    NSNumberFormatter *priceFormatter = [[NSNumberFormatter alloc] init];
    [priceFormatter setPositiveFormat:@"###,##0;"];
    return priceFormatter;
}

- (NSNumberFormatter*)getFullPriceFormatter{
    NSNumberFormatter *priceFormatter = [[NSNumberFormatter alloc] init];
    [priceFormatter setPositiveFormat:@"###,##0.0;"];
    return priceFormatter;
}

- (NSNumberFormatter*)getAllPriceFormatter{
    NSNumberFormatter *priceFormatter = [[NSNumberFormatter alloc] init];
    [priceFormatter setPositiveFormat:@"###,##0.00;"];
    return priceFormatter;
}

- (NSString*)getSimplePriceStringWithFloat:(float)price{
    return [NSString stringWithFormat:@"$%@", [[self getSimplePriceFormatter] stringFromNumber:[NSNumber numberWithFloat:price]] ];
}

- (NSString*)getFullPriceStringWithFloat:(float)price{
    return [NSString stringWithFormat:@"$%@", [[self getFullPriceFormatter] stringFromNumber:[NSNumber numberWithFloat:price]]];
}

- (NSString*)getAllPriceStringWithFloat:(float)price{
    return [NSString stringWithFormat:@"$%@", [[self getAllPriceFormatter] stringFromNumber:[NSNumber numberWithFloat:price]]];
}



#pragma mark - Handle proper orientation
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    if (interfaceOrientation == UIInterfaceOrientationLandscapeLeft
//        || interfaceOrientation == UIInterfaceOrientationLandscapeRight)
//    {
//        return YES;
//    }
//    return NO;
//}
//
//- (BOOL)shouldAutorotate {
//    return NO;
//}
//
//- (NSUInteger)supportedInterfaceOrientations {
//    return UIInterfaceOrientationMaskLandscape;
//}


#pragma mark - Common Data error handling
-(BOOL)handleQueryDataErrorCode:(int)code description:(NSString*)message{
    [self hideLoadingPopup];
    if(code == NETWORK_ERROR_CODE_1009){
        [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:[ShareLocale textFromKey:@"error"] message:[ShareLocale textFromKey:@"network_not_available_switch_offline"] yesHanlder:^(id data) {
            VFDAO* dao = [VFDAO sharedInstance];
            [dao isOnlineMode: NO];
        } noHandler:^(id data) {
            
        }];
        return YES;
    }
    else if(code == NETWORK_ERROR_CODE_1003){
        [self popupErrorDialogWithMessage: [ShareLocale textFromKey:@"host_not_found"]];
        return YES;
    }
    else if(code == 80002 || code == NETWORK_ERROR_CODE_1001 ){
        [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:[ShareLocale textFromKey:@"error_timeout"] message:[ShareLocale textFromKey:@"network_not_stable_switch_offline"] yesHanlder:^(id data) {
            VFDAO* dao = [VFDAO sharedInstance];
            [dao isOnlineMode: NO];
        } noHandler:^(id data) {
            
        }];
        return YES;
    }
    else{
        [self popupErrorDialogWithMessage: [NSString stringWithFormat:@"%@", message]];
        return YES;
    }
    return NO;
}



@end
