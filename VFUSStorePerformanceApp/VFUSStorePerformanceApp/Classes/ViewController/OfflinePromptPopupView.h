//
//  OfflinePromptPopupView.h
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 4/12/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#ifndef VFStorePerformanceApp_Dev2_OfflinePromptPopupView_h
#define VFStorePerformanceApp_Dev2_OfflinePromptPopupView_h

@interface OfflinePromptPopupView : UIView

-(void) initButtonBindingTo:(id)obj;


@end

#endif
