//
//  ImagePreviewViewController.h
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 16/6/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "ImageLoader.h"


@interface ImagePreviewViewController : BasicViewController<ImageLoaderDelegate>

@property (nonatomic, retain) NSString* feedItemID;
@property (retain, nonatomic) IBOutlet UIImageView *photoView;

@property (retain, nonatomic) IBOutlet UILabel *headerTitleLabel;

-(id) initWithFeedItemID:(NSString*)feedItemID;

- (IBAction)onHomeButtonClicked:(id)sender;

- (IBAction)onBackButtonClicked:(id)sender;

@end
