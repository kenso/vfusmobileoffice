//
//  CommentViewController.h
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 8/1/15.
//  Copyright (c) 2015 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebImageView.h"

@interface CommentViewController : UIViewController


@property (retain, nonatomic) IBOutlet UILabel *titleLabel;
@property (retain, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (retain, nonatomic) IBOutlet UILabel *dateLabel;

- (IBAction)onClickBackground:(id)sender;

@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@property (retain, nonatomic) NSDictionary* data;
@property (retain, nonatomic) NSMutableArray* imgDataArray;

-(id)initWithCommentData:(NSDictionary*)data;

- (IBAction)onClickCloseButton:(id)sender;

@end
