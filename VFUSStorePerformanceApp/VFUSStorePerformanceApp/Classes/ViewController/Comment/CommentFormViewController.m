//
//  CommentFormViewController.m
//  VFStorePerformanceApp
//
//  Created by Developer on 2014-03-31.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "CommentFormViewController.h"
#import "Constants.h"
#import "ShareLocale.h"
#import "GeneralUiTool.h"
#import "SFOAuthCoordinator.h"
#import "WebImageView.h"
#import "ImageSaver.h"
#import "SFAccountManager.h"

@interface CommentFormViewController ()

@end

@implementation CommentFormViewController


-(id)initWithData:(NSDictionary *)data{
    self = [super initWithNibName:@"CommentFormViewController" bundle:nil];
    if(self){
        NSLog(@"CommentFormViewController initWithData:%@", data);
        self.storeId = [data objectForKey:@"storeId"];
        self.storeName = [data objectForKey:@"storeName"];
        if([self.storeId isEqualToString:@""]){
            self.storeId = nil;
        }
        if(self.storeId == nil || self.storeName==nil || [self.storeName isEqualToString:@""]){
            self.storeName = [ShareLocale textFromKey: @"select"];
        }else{
            VFDAO* vf = [VFDAO sharedInstance];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

            NSArray* storeArray = [vf getStoresByBrandCode: [defaults objectForKey: USER_PREFER_ACCOUNT_CODE] countryCode:[defaults objectForKey: USER_PREFER_COUNTRY_CODE] needOptionAll:NO];
            for(NSDictionary* d in storeArray){
                if([d[@"Id"] isEqualToString: self.storeId]){
                    self.storeName = d[@"Name"];
                }
            }
        }
        
    }
    return self;
}

- (IBAction)onClickCategoryButton:(id)sender {
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;

    // Do any additional setup after loading the view from its nib.
    NSDateFormatter* df = [[[NSDateFormatter alloc] init] autorelease];
    [df setDateFormat:DEFAULT_SIMPLE_DATE_FORMAT];
    self.labelDate.text = [df stringFromDate:[NSDate date]];
    [self.storeNameButton setTitle:self.storeName  forState:UIControlStateNormal];
    
    [self.storeNameButton addTarget:self action:@selector(onClickSelectStoreButton:) forControlEvents:UIControlEventTouchUpInside];
    
    //[self.textFieldTitle setDelegate:self];
    //[self.textViewComment setDelegate:self];
    
    //[self registerForKeyboardNotifications];
    
    VFDAO* vf = [VFDAO sharedInstance];
    self.commentRecordId = [vf.cache objectForKey:@"record_type_comment"];
    if([self.commentRecordId isEqualToString:@""]) self.commentRecordId = nil;
    
    if(self.commentRecordId==nil){
        [self showInputErrorDialog:[ShareLocale textFromKey:@"corrupt_warning"]];
    }
    self.categoryButton.hidden = YES;
    
    [self.categoryButton setTitle:[ShareLocale textFromKey:@"select"] forState:UIControlStateNormal];
    
    self.storeNameLabel.text = [ShareLocale textFromKey:@"commtform_storename:"];
    self.logDateLabel.text = [ShareLocale textFromKey:@"commtform_logdate:"];
    self.titleLabel.text = [ShareLocale textFromKey:@"commtform_title:"];
    self.detailLabel.text = [ShareLocale textFromKey:@"commtform_detail:"];
    self.headerTitleLabel.text = [ShareLocale textFromKey:@"commtform_header_title"];
    [self.saveButton setTitle:[ShareLocale textFromKey:@"commtform_save"] forState:UIControlStateNormal];
    [self.formTitleLabel setTitle:[ShareLocale textFromKey:@"commtform_formtitle"] forState:UIControlStateNormal];

    self.attachLabel.text = [ShareLocale textFromKey:@"commtform_attach_image"];
    [self initUploadPhotoView:self.photoImageView];
    [self initUploadPhotoView:self.photoImageView2];
    [self initUploadPhotoView:self.photoImageView3];
    [self initUploadPhotoView:self.photoImageView4];
    [self initUploadPhotoView:self.photoImageView5];
    [self initUploadPhotoView:self.photoImageView6];
    [self initUploadPhotoView:self.photoImageView7];
    [self initUploadPhotoView:self.photoImageView8];
    [self initUploadPhotoView:self.photoImageView9];
    [self initUploadPhotoView:self.photoImageView10];
    self.textViewComment.delegate = self;
    self.textFieldTitle.delegate = self;
    
}


-(void) initUploadPhotoView:(WebImageView*)view{
    view.onImageChangedBlock = ^(WebImageView* v, NSString* s){
        [v.data setObject:s forKey:@"emfa__FeedItemId__c"];
    };
    [view enableClickCaptureOnParentView:self.view onViewController:self];
    [view enableLongPressPreviewOnParentView:self.view onViewController:self];

}
//
//-(void) onClickPhotoView{
//    if(self.photoImageView.imageLoaded){
//        self.enlargedPhotoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1024, 768)];
//        [self.view addSubview: self.enlargedPhotoView];
//        
//        UIImageView* v = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 1024, 768)];
//        [self.enlargedPhotoView addSubview:v];
//        
//        v.image = self.photoImageView.image;
//        
//        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickLargePhotoView)];
//        singleTap.numberOfTapsRequired = 1;
//        [v setUserInteractionEnabled:YES];
//        [v addGestureRecognizer:singleTap];
//    }
//}
//
//-(void) onClickLargePhotoView{
//    if(self.enlargedPhotoView!=nil){
//        [self.enlargedPhotoView removeFromSuperview];
//        self.enlargedPhotoView = nil;
//    }
//}


-(void) onClickSelectStoreButton:(UIButton*)button{
    userAlreadyInput = YES;
    VFDAO* vf = [VFDAO sharedInstance];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSArray* storeArray = [vf getStoresByBrandCode: [defaults objectForKey: USER_PREFER_ACCOUNT_CODE] countryCode:[defaults objectForKey: USER_PREFER_COUNTRY_CODE] needOptionAll:NO];
    
    StoreSelectionViewController* storePopup = [[[StoreSelectionViewController alloc] initWithStoreList: storeArray] autorelease];
    storePopup.delegate = self;
    self.storeSelectPopup = [[[UIPopoverController alloc] initWithContentViewController:storePopup] autorelease];
    self.storeSelectPopup.delegate = self;
    [self.storeSelectPopup presentPopoverFromRect:button.frame  inView:button.superview permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) showInputErrorDialog:(NSString*) msg{
    [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:[ShareLocale textFromKey:@"validation_error"] message:msg];
}

-(void) showOnlineSubmitionSuccessDialog{
    [self hideLoadingPopup];
    [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"" message:[ShareLocale textFromKey:@"dialog_save_comment"] yesHanlder:^(id data) {
        [self.navigationController popViewControllerAnimated:YES];
        
        if (self.delegate!=nil){
            [self.delegate onSaveCommentSuccess];
        }
        
    }];
}

//Save Image
-(void) onAllImageUploadedCompleted{
    [self performSelectorOnMainThread:@selector(showOnlineSubmitionSuccessDialog) withObject:nil waitUntilDone:YES];
}

-(void) onImageUploadedCompleted:(NSString*)feedId {
    //Do nth
    if([VFDAO sharedInstance]){
        
    }
}

-(void) onImageUploadedFail:(NSString*)feedId error:(NSError*)error{
    [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:[ShareLocale textFromKey:@"error"] message:[ShareLocale textFromKey:@"Image upload fail."]];
}




//Save form
- (void)startToSubmitForm{
    [self showLoadingPopup:self.view];

    
    NSString * title = self.textFieldTitle.text;
    NSString * comment;
    if ( self.textViewComment.text != nil ){
        comment = self.textViewComment.text;
    }else {
        comment = @"";
    }
    NSDateFormatter* df = [GeneralUiTool getSFDateFormatter];
    SFOAuthCredentials* s = [SFAccountManager sharedInstance].credentials;
    
    NSDictionary* data = @{
                           @"Subject__c":title,
                           @"Description__c": comment,
                           @"Store__c":self.storeId,
                           @"ActivityDate__c":[df stringFromDate:[NSDate date]],
                           @"Creator__c": s.userId
                           };
    
    self.submitRequest = [[VFDAO sharedInstance] saveNewComment:data delegate:self];
    
}



-(void)daoRequest:(DAOBaseRequest*)request queryOnlineDataSuccess:(NSArray*) response{
    
    if(self.submitRequest == request){
        //After comment created, upload image
            
        NSArray* imageViews = @[self.photoImageView, self.photoImageView2, self.photoImageView3, self.photoImageView4,
                                    self.photoImageView5, self.photoImageView6, self.photoImageView7, self.photoImageView8,
                                    self.photoImageView9, self.photoImageView10];
            
            
        NSMutableArray* pendingUploadPhotoDataArray = [NSMutableArray array];
        for(NSInteger i=1; i<=10; i++){
            
            WebImageView* webImgView = imageViews[i-1];
            if(webImgView.imageLoaded){
                NSString* imgFileName = [webImgView saveImageInDB: webImgView.image];
                [pendingUploadPhotoDataArray addObject:@{ @"objId":response[0], @"category":[NSString stringWithFormat:@"Comment Photo %d", i], @"localFileName":imgFileName }];
            }
        }
        if([pendingUploadPhotoDataArray count]>0){
            [[ImageSaver shareInstance] uploadImagesWithFeedItemIDArray:pendingUploadPhotoDataArray delegate:self];
        }
        else{
            [self performSelectorOnMainThread:@selector(showOnlineSubmitionSuccessDialog) withObject:nil waitUntilDone:YES];
        }
    }
}




-(void)daoRequest:(DAOBaseRequest*)request queryOnlineDataError:(NSString*) response code:(int)code{
    [self hideLoadingPopupWithFadeout];
    
    [super handleQueryDataErrorCode:code description:response];
    
}


- (BOOL)isValidForm{
    NSString* taskTitle = self.textFieldTitle.text;
    NSString* taskComment = self.textViewComment.text;
    if(self.storeId==nil){
        [self showInputErrorDialog:[ShareLocale textFromKey:@"warnning_no_store_selected"]];
        return NO;
    }
    else if(taskTitle==nil || [[taskTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0){
        [self showInputErrorDialog:[ShareLocale textFromKey:@"warning_no_title"]];
        return NO;
    }
    else if([taskTitle length]>255){
        [self showInputErrorDialog:[ShareLocale textFromKey:@"warning_title_too_long"]];
        return NO;
    }
    else if(taskComment==nil || [[taskComment stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0){
        [self showInputErrorDialog: [ShareLocale textFromKey:@"warning_no_comment"]];
        return NO;
    }
    else if([taskComment length]>255){
        [self showInputErrorDialog:[ShareLocale textFromKey:@"warning_comment_too_long"]];
        return NO;
    }
    
    
    return YES;
}


- (IBAction)onHomeButtonClicked:(id)sender {
    if(userAlreadyInput){
        [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"" message:[ShareLocale textFromKey:@"newvisit_confirm_leave"] yesHanlder:^(id data) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        } noHandler:^(id data) {
            
        }];
    }else{
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}



- (IBAction)onBackButtonClicked:(id)sender {
    if(userAlreadyInput){
        [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"" message:[ShareLocale textFromKey:@"newvisit_confirm_leave"] yesHanlder:^(id data) {
            [self.navigationController popViewControllerAnimated:YES];
        } noHandler:^(id data) {
            
        }];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}



- (IBAction)onSubmitButtonClicked:(id)sender {
    if ( [self isValidForm]){
        [self startToSubmitForm];
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    userAlreadyInput = YES;
}
- (void)textViewDidBeginEditing:(UITextView *)textView{
    userAlreadyInput = YES;
}

- (void)dealloc {
    [_textViewComment release];
    [_storeId release];
    [_labelDate release];
    [_textFieldTitle release];
    [_storeNameButton release];
    [_categoryButton release];
    [_headerTitleLabel release];
    [_formTitleLabel release];
    [_storeNameLabel release];
    [_logDateLabel release];
    [_titleLabel release];
    [_detailLabel release];
    [_saveButton release];
    [_photoImageView release];
    [_photoImageView2 release];
    [_photoImageView3 release];
    [_photoImageView4 release];
    [_photoImageView5 release];
    [_photoImageView6 release];
    [_photoImageView7 release];
    [_photoImageView8 release];
    [_photoImageView9 release];
    [_photoImageView10 release];
    [_uploadButton release];
    [_attachLabel release];
    [super dealloc];
}



#pragma mark - Popup delegate
-(void) onSelectdStore:(NSDictionary*) storeObj{
    userAlreadyInput = YES;
    [self.storeSelectPopup dismissPopoverAnimated:YES];
    if(storeObj){
        NSLog(@"storeObj=%@", storeObj);
        [self.storeNameButton setTitle:[storeObj objectForKey:@"Name"] forState:UIControlStateNormal];
        self.storeName = [storeObj objectForKey:@"Name"];
        self.storeId = [storeObj objectForKey:@"Id"];
    }
}



- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    
    
    
}


- (IBAction)onUploadButtonClicked:(id)sender {
    
    
    
}


@end
