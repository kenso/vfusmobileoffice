//
//  CommentFormViewController.h
//  VFStorePerformanceApp
//
//  Created by Developer on 2014-03-31.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "WebImageView.h"
#import "ImageSaver.h"


@protocol CommentFormViewDelegate <NSObject>

-(void) onSaveCommentSuccess;

@end





@interface CommentFormViewController : BasicViewController<DAODelegate, UITextFieldDelegate, UITextViewDelegate, StoreSelectionViewControllerDelegate, UIPopoverControllerDelegate, ImageSaverDelegate>{
    BOOL editing;
    BOOL userAlreadyInput;
}


@property (assign, nonatomic) id<CommentFormViewDelegate> delegate;

@property (retain, nonatomic) IBOutlet UILabel *attachLabel;

@property (retain, nonatomic) UIPopoverController* storeSelectPopup;

@property (retain, nonatomic) IBOutlet WebImageView *photoImageView;
@property (retain, nonatomic) IBOutlet WebImageView *photoImageView2;
@property (retain, nonatomic) IBOutlet WebImageView *photoImageView3;
@property (retain, nonatomic) IBOutlet WebImageView *photoImageView4;
@property (retain, nonatomic) IBOutlet WebImageView *photoImageView5;
@property (retain, nonatomic) IBOutlet WebImageView *photoImageView6;
@property (retain, nonatomic) IBOutlet WebImageView *photoImageView7;
@property (retain, nonatomic) IBOutlet WebImageView *photoImageView8;
@property (retain, nonatomic) IBOutlet WebImageView *photoImageView9;
@property (retain, nonatomic) IBOutlet WebImageView *photoImageView10;

@property (retain, nonatomic) IBOutlet UIButton *uploadButton;

@property (retain, nonatomic) IBOutlet UIButton *categoryButton;
@property (retain, nonatomic) DAOBaseRequest *submitRequest;
@property (retain, nonatomic) NSString *storeId;
@property (retain, nonatomic) NSString *storeName;
@property (retain, nonatomic) IBOutlet UITextField *textFieldTitle;
@property (retain, nonatomic) IBOutlet UITextView *textViewComment;

@property (retain, nonatomic) IBOutlet UIButton *storeNameButton;
@property (retain, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (retain, nonatomic) IBOutlet UIButton *formTitleLabel;
@property (retain, nonatomic) IBOutlet UIButton *saveButton;

@property (retain, nonatomic) UIView* enlargedPhotoView;

@property (retain, nonatomic) NSString* commentRecordId;
@property (retain, nonatomic) IBOutlet UILabel *detailLabel;

@property (retain, nonatomic) IBOutlet UILabel *labelDate;
@property (retain, nonatomic) IBOutlet UILabel *storeNameLabel;
@property (retain, nonatomic) IBOutlet UILabel *titleLabel;

@property (retain, nonatomic) IBOutlet UILabel *logDateLabel;

-(id)initWithData:(NSDictionary*)data;

- (IBAction)onHomeButtonClicked:(id)sender;

- (IBAction)onBackButtonClicked:(id)sender;

- (IBAction)onSubmitButtonClicked:(id)sender;

- (IBAction)onUploadButtonClicked:(id)sender;

@end
