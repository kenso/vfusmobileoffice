//
//  CommentListTableView.m
//  VFStorePerformanceApp
//
//  Created by Developer on 2014-03-31.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "CommentListTableView.h"
#import "Constants.h"
#import "CommentViewController.h"
#import "GeneralUiTool.h"
#import "DataHelper.h"

@implementation CommentListTableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initView];
    }
    return self;
}

- (void)initView{
    NSLog(@"initView");
    self.arrData = [[NSArray alloc] init];
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    self.tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 15);
    [self.tableView setSeparatorColor:TABLE_SEPARATOR_COLOR];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    [self addSubview:self.tableView];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)refreshTableView{
    [self.tableView reloadData];
}

- (int)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.arrData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
    if(cell == nil){
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyIdentifier"] autorelease];
        UIView *selectionColor = [[[UIView alloc] init] autorelease];
        selectionColor.backgroundColor = TABLE_ROW_HIGHLIGHT_COLOR;
        cell.selectedBackgroundView = selectionColor;
        
        cell.textLabel.hidden = YES;
        cell.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        
        [self createNewTextLabelWithDefaultStyle: CGRectMake(20, 5, 150, 40) parent:cell tag: 901];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(200, 5, 180, 40) parent:cell tag: 902];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(480, 5, 330, 40) parent:cell tag: 903];
    }
    


    NSDictionary* data = [self.arrData objectAtIndex: indexPath.row];
    //Description
    //Subject
    //ActivityDate

    UILabel* dateLabel = (UILabel*)[cell viewWithTag:901];
    UILabel* titleLabel = (UILabel*)[cell viewWithTag:902];
    UILabel* descriptionLabel = (UILabel*)[cell viewWithTag:903];
    
    NSString* date = gfrd(data, @"ActivityDate__c", @""); //[data objectForKey:@"ActivityDate__c"];
    NSString* sub = gfrd(data, @"Subject__c", @""); //[data objectForKey:@"Subject__c"];
    NSString* desc = gfrd(data, @"Description__c", @"");
    
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm"];
    dateLabel.text = date==(id)[NSNull null]?@"N/A": [df stringFromDate: [ [GeneralUiTool getSFDateFormatter] dateFromString: date ] ];
    titleLabel.text = sub==(id)[NSNull null]?@"N/A":sub;
    descriptionLabel.text = desc==(id)[NSNull null]?@"N/A":desc;

    return cell;
    
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.parentVC){
        NSDictionary* data = [self.arrData objectAtIndex: indexPath.row];
        CommentViewController* vc = [[CommentViewController alloc] initWithCommentData:data];
        [vc setModalPresentationStyle:UIModalPresentationCurrentContext]; //UIModalPresentationOverCurrentContext
        
        self.parentVC.modalPresentationStyle = UIModalPresentationCurrentContext |UIModalPresentationFormSheet;
        [self.parentVC.navigationController presentViewController:vc animated:NO completion:nil];
    }
}

-(UILabel*) createNewTextLabelWithDefaultStyle:(CGRect)rect parent:(UIView*)parent tag:(NSInteger) tag{
    return [self createNewTextLabelWithDefaultStyle:rect parent:parent tag:tag alignment:NSTextAlignmentLeft];
}

-(UILabel*) createNewTextLabelWithDefaultStyle:(CGRect)rect parent:(UIView*)parent tag:(NSInteger)tag alignment:(NSTextAlignment)alignment{
    UILabel* tempText = [[[UILabel alloc] initWithFrame: rect] autorelease];
    tempText.textColor = [UIColor colorWithWhite:.95 alpha:1];
    tempText.font = [UIFont fontWithName:@"Helvetica Neue" size:14];
    tempText.tag = tag;
    tempText.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
    tempText.textAlignment = alignment;
    [parent addSubview:tempText];
    return tempText;    
}


@end
