//
//  CommentSidebarView.h
//  VFStorePerformanceApp_Billy
//
//  Created by Developer 2 on 30/4/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StoreSelectionViewController.h"
#import "VFDAO.h"

@interface CommentSidebarView : UIView<StoreSelectionViewControllerDelegate, UIPopoverControllerDelegate, UITableViewDataSource, UITableViewDelegate, DAODelegate>

@property (assign) BOOL loading;

@property (retain) UIButton* addNewButton;
@property (retain) UIButton* siwtchButton;
@property (retain) UIButton* filterButton;
@property (retain) UIButton* viewAllButton;
@property (retain) UITableView* tableView;

@property (retain) UIPopoverController* storeSelectPopup;

@property (assign) BOOL needReload;

@property (retain) NSString* selectedStoreId;
@property (retain) NSDictionary* selectedStoreData;
@property (retain) NSMutableArray* commentsArray;
@property (retain) NSArray* storeArray;

@property (assign) UIViewController* controllerDelegate;

+(id)newInstance;

-(void) loadStoreComments;

-(void) selfBinding;

-(void) hideSidebar;

- (void)onClickSwitchButton:(id)sender;
- (void)onClickAddButton:(id)sender;
- (void)onClickFilterButton:(id)sender;
- (void)onClickViewAllButton:(id)sender;

@end
