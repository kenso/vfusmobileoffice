//
//  CommentPopupView.h
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 10/2/15.
//  Copyright (c) 2015 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebImageView.h"

@interface CommentPopupView : UIView<UIScrollViewDelegate>


@property (retain, nonatomic) IBOutlet UILabel *titleLabel;
@property (retain, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (retain, nonatomic) IBOutlet UILabel *dateLabel;
@property (retain, nonatomic) IBOutlet UILabel *creatorLabel;
@property (retain, nonatomic) NSDictionary* data;

@property (retain, nonatomic) IBOutlet UIPageControl *pageControl;

@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
-(void)reloadWithCommentData:(NSDictionary*)data;
@property (retain, nonatomic) NSMutableArray* imgDataArray;

//- (IBAction)onClickBackground:(id)sender;

- (IBAction)onClickCloseButton:(id)sender;



@end
