//
//  CommentListTableView.h
//  VFStorePerformanceApp
//
//  Created by Developer on 2014-03-31.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentListTableView : UIView<UITableViewDataSource, UITableViewDelegate>{
    
}

- (void)initView;
- (void)refreshTableView;

@property (nonatomic, retain) NSArray *arrData;
@property (nonatomic, retain) UITableView *tableView;

@property (nonatomic, assign) UIViewController* parentVC;

@end
