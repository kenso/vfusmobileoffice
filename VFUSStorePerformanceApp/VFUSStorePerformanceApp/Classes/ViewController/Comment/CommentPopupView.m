//
//  CommentPopupView.m
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 10/2/15.
//  Copyright (c) 2015 vf.com. All rights reserved.
//

#import "CommentPopupView.h"
#import "DataHelper.h"
#import "GeneralUiTool.h"

@implementation CommentPopupView


-(void)reloadWithCommentData:(NSDictionary*)data{
    self.data = data;
    [self setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
    
    NSString* subj = gfrd(self.data, @"Subject__c", @"");
    NSString* desc = gfrd(self.data, @"Description__c", @"");
    NSString* actDate = gfrd(self.data, @"ActivityDate__c", @"");
    if(subj==nil || subj==(id)[NSNull null]) subj = @"N/A";
    if(desc==nil || desc==(id)[NSNull null]) desc = @"N/A";
    
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSString* dateStr = [df stringFromDate: [[GeneralUiTool getSFDateFormatter] dateFromString: actDate]];
    
    self.titleLabel.text = subj;
    self.descriptionLabel.text = desc;
    [self.descriptionLabel sizeToFit];
    self.dateLabel.text = dateStr;
    self.creatorLabel.text = gfrd(data, @"Creator__r.Name", @"-" );
    
    NSDictionary* mediaDict = [self.data objectForKey:@"emfa__Shared_Media__r"];
    if(mediaDict!=nil && mediaDict!=(id)[NSNull null] && mediaDict[@"records"]!=nil && mediaDict[@"records"] !=(id)[NSNull null] ){
        NSArray* mediaArray = mediaDict[@"records"];
        
        self.imgDataArray = [mediaArray mutableCopy];
        
        [self initImageView];
    }else{
        [self hideImageView];
    }
}



-(void) initImageView{
    if([self.imgDataArray count]>0){
        NSInteger offsetX = 0;
        NSInteger num = 0;
        for(NSDictionary* mediaData in self.imgDataArray){
            if( mediaData[@"emfa__FeedItemId__c"]!=nil && mediaData[@"emfa__FeedItemId__c"]!=[NSNull null] ){
                WebImageView* newImgView = [[WebImageView alloc] init];
                [newImgView setFrame:  CGRectMake(offsetX, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height)];
                offsetX += self.scrollView.frame.size.width;
                newImgView.image = nil;
                newImgView.contentMode = UIViewContentModeScaleAspectFit;
                [newImgView loadImageWithFeedID: mediaData[@"emfa__FeedItemId__c"] completeBlock:^(WebImageView *imgView, UIImage *img) {
                    imgView.contentMode = UIViewContentModeScaleAspectFit;
                }];
                [self.scrollView addSubview: newImgView ];
                num++;
            }
        }
        if(num>0){
            self.pageControl.numberOfPages = num;
            self.scrollView.delegate = self;
            self.scrollView.contentSize = CGSizeMake(offsetX, self.scrollView.frame.size.height);
            return;
        }
    }
    [self hideImageView];
}

-(void) hideImageView{
    self.pageControl.hidden = YES;
    self.scrollView.hidden = YES;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat pageWidth = self.scrollView.frame.size.width; // you need to have a **iVar** with getter for scrollView
    float fractionalPage = self.scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    self.pageControl.currentPage = page; // you need to have a **iVar** with getter for pageControl
}



- (IBAction)onClickCloseButton:(id)sender{
    [self removeFromSuperview];
}


- (void)dealloc {
    [_data release];
    [_descriptionLabel release];
    [_titleLabel release];
    [_creatorLabel release];
    [_scrollView release];
    [_pageControl release];
    [super dealloc];
}
@end
