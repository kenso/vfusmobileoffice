//
//  CommentSidebarView.m
//  VFStorePerformanceApp_Billy
//
//  Created by Developer 2 on 30/4/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "ShareLocale.h"
#import "CommentSidebarView.h"
#import "StoreSelectionViewController.h"
#import "CommentFormViewController.h"
#import "DataHelper.h"
#import "GeneralUiTool.h"
#import "VFDAO.h"
#import "Constants.h"
//#import "CommentViewController.h"
#import "CommentPopupView.h"

@implementation CommentSidebarView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSLog(@"CommentSidebarView initWithFrame");
    }
    return self;
}


-(id)init{
    self = [super init];
    if(self){
        NSLog(@"CommentSidebarView init");
    }
    return self;
}


- (void)dealloc {
    [_addNewButton release];
    [_siwtchButton release];
    [_filterButton release];
    [_viewAllButton release];
    [_tableView release];
    _controllerDelegate = nil;
    [_storeSelectPopup release];
    [_selectedStoreId release];
    [_selectedStoreData release];
    [_commentsArray release];
    [_storeArray release];
    
    [super dealloc];
}


-(void) selfBinding{
    NSLog(@"CommentSidebarView selfBinding");
    
    VFDAO* vf = [VFDAO sharedInstance];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray* allStores = [vf getStoresByBrandCode: [defaults objectForKey: USER_PREFER_ACCOUNT_CODE] countryCode:[defaults objectForKey:USER_PREFER_COUNTRY_CODE] needOptionAll:NO];
    self.storeArray = allStores;
    
    self.addNewButton = (UIButton*)[self viewWithTag:100];
    self.siwtchButton = (UIButton*)[self viewWithTag:101];
    self.filterButton = (UIButton*)[self viewWithTag:102];
    self.viewAllButton = (UIButton*)[self viewWithTag:103];
    self.tableView = (UITableView*)[self viewWithTag:104];
    
    [self.addNewButton addTarget:self action:@selector(onClickAddButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.siwtchButton addTarget:self action:@selector(onClickSwitchButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.filterButton addTarget:self action:@selector(onClickFilterButton:) forControlEvents:UIControlEventTouchUpInside];
    NSString* title = [ShareLocale textFromKey:@"cmmt_no_store"];
    [self.filterButton setTitle:title forState:UIControlStateNormal];

    [self.viewAllButton addTarget:self action:@selector(onClickViewAllButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.siwtchButton setBackgroundImage:[UIImage imageNamed:[ShareLocale textFromKey:@"comment_on_png"]] forState:UIControlStateNormal];

    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
}



-(void)reloadComments{
    [self loadStoreComments];
    
}



-(void) hideSidebar{
    int targetX = 1024 - 30;
    [self.siwtchButton setBackgroundImage:[UIImage imageNamed:[ShareLocale textFromKey:@"comment_on_png"]] forState:UIControlStateNormal];

    self.frame = CGRectMake(targetX, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
    
}


+(id)newInstance
{
    CommentSidebarView* view = [[[NSBundle mainBundle] loadNibNamed:@"CommentSideBarView" owner:self options:nil] lastObject];
    if ([view isKindOfClass:[CommentSidebarView class]])
        return view;
    else
        return nil;

}


- (UIViewController*)viewController
{
    for (UIView* next = [self superview]; next; next = next.superview)
    {
        UIResponder* nextResponder = [next nextResponder];
        
        if ([nextResponder isKindOfClass:[UIViewController class]])
        {
            return (UIViewController*)nextResponder;
        }
    }
    
    return nil;
}


-(void) loadStoreComments{
    NSLog(@"loadStoreComments");
    
    BasicViewController* vc = (BasicViewController*)[self viewController];
    if(vc!=nil){
        [vc showLoadingPopup:vc.view];
    }
    
    self.commentsArray = [NSMutableArray array];

    if(self.selectedStoreId!=nil){
        for(NSDictionary* data in self.storeArray){
            if([[data objectForKey:@"Id"] isEqualToString:self.selectedStoreId]){
                self.selectedStoreData = data;
                [self.filterButton setTitle:[data objectForKey:@"Name"] forState:UIControlStateNormal];
            }

        }
        self.loading = YES;
        [[VFDAO sharedInstance] selectCommentsWithDelegate:self withStoreID:self.selectedStoreId];
    }else{
        [[VFDAO sharedInstance] selectCommentsWithDelegate:self withStoreID:nil];
    }
    
}



- (void)onClickSwitchButton:(id)sender {
    int targetX = 0;
    if(self.frame.origin.x >950){
        targetX = 1024 - self.frame.size.width;
        [self.siwtchButton setBackgroundImage:[UIImage imageNamed:[ShareLocale textFromKey:@"comment_off_png"]] forState:UIControlStateNormal];
        
        if(self.needReload){
            self.needReload = NO;
            [self loadStoreComments];
        }
        
    }else{
        targetX = 1024 - 30;
        [self.siwtchButton setBackgroundImage:[UIImage imageNamed:[ShareLocale textFromKey:@"comment_on_png"]] forState:UIControlStateNormal];
    }
    
    __block CommentSidebarView* me = self;

    [UIView animateWithDuration:.3f animations:^{
        me.frame = CGRectMake(targetX, me.frame.origin.y, me.frame.size.width, me.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];

}

- (void)onClickAddButton:(id)sender {
    NSString* storeId = [self.selectedStoreData objectForKey:@"Id"];
    NSString* storeName = [self.selectedStoreData objectForKey:@"Name"];
    if(storeId==nil){
        storeId = @"";
    }
    if(storeName==nil){
        storeName = @"";
    }
    CommentFormViewController *vc = [[CommentFormViewController alloc] initWithData:@{
        @"storeId": storeId,
        @"storeName": storeName
    }];

    [self.controllerDelegate.navigationController pushViewController:vc animated:YES];
    [vc release];
    
}

- (void)onClickFilterButton:(id)sender {
    CGRect f = CGRectMake(30,0, 320,30);
    
    StoreSelectionViewController* storePopup = [[StoreSelectionViewController alloc] initWithStoreList: self.storeArray] ;
    storePopup.delegate = self;
    self.storeSelectPopup = [[UIPopoverController alloc] initWithContentViewController:storePopup] ;
    self.storeSelectPopup.delegate = self;
    [self.storeSelectPopup presentPopoverFromRect:f  inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    [storePopup release];
    [self.storeSelectPopup release];

}

- (void)onClickViewAllButton:(id)sender{
}


#pragma mark - SelectStoreDelegate
-(void) onSelectdStore:(NSDictionary*) storeObj{
    if(self.storeSelectPopup ){
        NSLog(@"onSelectdStore = %@", storeObj);
        if(storeObj!=nil && [[storeObj objectForKey:@"Id" ] length]!=0 ){
            self.selectedStoreId = [storeObj objectForKey:@"Id"];
            self.selectedStoreData = storeObj;
            [self.filterButton setTitle:[storeObj objectForKey:@"Name"] forState:UIControlStateNormal];
        }else{
            NSString* title = [ShareLocale textFromKey:@"cmmt_no_store"];
            [self.filterButton setTitle:title forState:UIControlStateNormal];
            self.selectedStoreData = nil;
            self.selectedStoreId = nil;
        }
        [self.storeSelectPopup dismissPopoverAnimated:YES];
        self.storeSelectPopup = nil;
        [self loadStoreComments];
    }
}


#pragma mark - TableView delegate
-(float) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary* data = [self.commentsArray objectAtIndex:indexPath.row];
    
    CommentPopupView* popup =      [[[NSBundle mainBundle] loadNibNamed:@"CommentPopupView" owner:self options:nil] objectAtIndex:0];
    [popup reloadWithCommentData: data];
    //[[CommentPopupView alloc] initWithCommentData:data];
    UIViewController* c = (UIViewController*)self.controllerDelegate;
    [c.view addSubview: popup];
    
//    CommentViewController* vc = [[CommentViewController alloc] initWithCommentData:data];
//    [vc setModalPresentationStyle: UIModalPresentationCurrentContext]; //UIModalPresentationOverCurrentContext
//
//    UIViewController* c = (UIViewController*)self.controllerDelegate;
//    c.modalPresentationStyle = UIModalPresentationCurrentContext |UIModalPresentationFormSheet;
//    [c.navigationController presentViewController:vc animated:NO completion:nil];

}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary* data = [self.commentsArray objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SidebarComment"];
    if(cell == nil){
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"SidebarComment"] autorelease];
        cell.textLabel.numberOfLines = 2;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.detailTextLabel setTextColor:[UIColor lightGrayColor]];
        UIImageView* v = [[UIImageView alloc] initWithFrame:CGRectMake(300, 30, 10, 10)];
        v.image = [UIImage imageNamed:@"tag.png"];
        v.tag = 978;
        [cell addSubview: v];
    }
    
    UIImageView* v = (UIImageView*)[cell viewWithTag:978];
    v.hidden = YES;
    if(data[@"Shared_Media__r"]!=nil && data[@"Shared_Media__r"]!=(id)[NSNull null]){
        if ([data[@"Shared_Media__r"][@"records"] count]>0){
            v.hidden = NO;
        }
    }
    
    
    NSString* desc = [data objectForKey:@"Subject__c"];
    if(desc!=nil && desc!=(id)[NSNull null]){
        cell.textLabel.text = desc;
    }
    else {
        cell.textLabel.text = @"";
    }
    NSString* titleformat = [ShareLocale textFromKey:@"cmmt_detail_format"];
    
    NSDate* date = [[GeneralUiTool getSFDateFormatter] dateFromString:[data objectForKey:@"ActivityDate__c"]];
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    if([data objectForKey:@"Creator__r"]!=nil && [data objectForKey:@"Creator__r"]!=(id)[NSNull null] ){
        cell.detailTextLabel.text = [NSString stringWithFormat:titleformat, [data objectForKey:@"Store_Name__c"] , [[data objectForKey:@"Creator__r"] objectForKey:@"Name" ], [df stringFromDate:date]];
    }else{
        cell.detailTextLabel.text = [NSString stringWithFormat:titleformat, [data objectForKey:@"Store_Name__c"] , @"", [df stringFromDate:date]];
    }
    

    return cell;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.commentsArray count];
}


#pragma mark - DAO delegate
-(void)daoRequest:(DAORequest*)request queryOnlineDataSuccess:(NSArray*) response{
    self.loading = NO;
    
    self.commentsArray = [NSMutableArray arrayWithArray:response];
//    VFDAO* vf = [VFDAO sharedInstance];
//    NSDictionary* dict = vf.cache;
//    NSDictionary* storeMap = [dict objectForKey:@"store_daily_analyse"];
    
    
    NSMutableArray* refinedComments = [[NSMutableArray alloc] init];
    for(NSDictionary* store in self.storeArray){
        NSString* sid = [store objectForKey:@"Id"];
        NSString* commentStoreCode = [store objectForKey:@"emfa__Store_Code__c"];
        for(NSDictionary* comment in self.commentsArray){
            NSString* refStoreId = [comment objectForKey:@"Store__c"];
            
            if([DataHelper isSameSalesforceId:refStoreId and:sid] && comment[@"ActivityDate__c"]!=(id)[NSNull null]){
                NSMutableDictionary* newComment = [NSMutableDictionary dictionaryWithDictionary:comment];
                [newComment setObject:commentStoreCode forKey:@"Store_Name__c"];
                [refinedComments addObject:newComment ];
            }
        }
    }
    
    [refinedComments sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSDictionary* record1 = (NSDictionary*)obj1;
        NSDictionary* record2 = (NSDictionary*)obj2;
        
        NSDate* d1 = [[GeneralUiTool getSFDateFormatter] dateFromString:record1[@"ActivityDate__c"]];
        NSDate* d2 = [[GeneralUiTool getSFDateFormatter] dateFromString:record2[@"ActivityDate__c"]];
        
        NSComparisonResult result = [d1 compare:d2];
        if(result == NSOrderedAscending){
            return NSOrderedDescending;
        }
        else if(result == NSOrderedDescending){
            return NSOrderedAscending;
        }
        else{
            return NSOrderedSame;
        }
    }];
    
    self.commentsArray = refinedComments;
    [refinedComments release];
    
    [self.tableView reloadData];
    
    BasicViewController* vc = (BasicViewController*)[self viewController];
    if(vc!=nil){
        [vc hideLoadingPopup];
    }
}

//response must contains error(int) and description(string)
-(void)daoRequest:(DAORequest*)request queryOnlineDataError:(NSString*) response code:(int)code{
    self.loading = NO;
    [self.tableView reloadData];
    
    BasicViewController* vc = (BasicViewController*)[self viewController];
    if(vc!=nil){
        [vc hideLoadingPopup];
    }
}



@end
