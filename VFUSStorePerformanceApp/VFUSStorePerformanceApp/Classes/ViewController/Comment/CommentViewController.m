//
//  CommentViewController.m
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 8/1/15.
//  Copyright (c) 2015 vf.com. All rights reserved.
//

#import "CommentViewController.h"
#import "GeneralUiTool.h"
#import "DataHelper.h"

@interface CommentViewController ()

@end

@implementation CommentViewController



-(id)initWithCommentData:(NSDictionary*)data{
    if(self = [super init]){
        self.data = data;
    }
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
    
    NSString* subj = gfrd(self.data, @"Subject__c", @"N/A");
    NSString* desc = gfrd(self.data, @"Description__c", @"N/A");
    NSString* actDate = gfrd(self.data, @"ActivityDate__c", @"");

    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSString* dateStr = [df stringFromDate: [[GeneralUiTool getSFDateFormatter] dateFromString: actDate]];
    
    self.titleLabel.text = subj;
    self.descriptionLabel.text = desc;
    [self.descriptionLabel sizeToFit];
    self.dateLabel.text = dateStr;
    
    
    NSDictionary* mediaDict = [self.data objectForKey:@"emfa__Shared_Media__r"];
    if(mediaDict!=nil && mediaDict!=(id)[NSNull null] && mediaDict[@"records"]!=nil && mediaDict[@"records"] !=(id)[NSNull null] ){
        NSArray* mediaArray = mediaDict[@"records"];
        
        
        self.imgDataArray = [mediaArray mutableCopy];
        
        [self initImageView];
    }
}


-(void) initImageView{
    if([self.imgDataArray count]>0){
        NSInteger offsetX = 0;
        for(NSDictionary* mediaData in self.imgDataArray){
            if( mediaData[@"emfa__FeedItemId__c"]!=nil && mediaData[@"emfa__FeedItemId__c"]!=(id)[NSNull null] ){
                WebImageView* newImgView = [[WebImageView alloc] init];
                [newImgView setFrame:  CGRectMake(offsetX, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height)];
                offsetX += self.scrollView.frame.size.width;
                newImgView.image = nil;
                newImgView.contentMode = UIViewContentModeScaleAspectFit;
                [newImgView loadImageWithFeedID: mediaData[@"emfa__FeedItemId__c"] completeBlock:^(WebImageView *imgView, UIImage *img) {
                    imgView.contentMode = UIViewContentModeScaleAspectFit;
                }];
                
            }else{
                //self.photoView.image = nil;
            }
            self.scrollView.contentSize = CGSizeMake(offsetX, self.scrollView.frame.size.height);
            
        }
    }
    else{
        //self.photoView.image = nil;
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)dealloc {
    [_titleLabel release];
    [_descriptionLabel release];
    [_dateLabel release];
    [_scrollView release];
    [super dealloc];
}

- (IBAction)onClickBackground:(id)sender {
    //[self.navigationController popViewControllerAnimated:NO];
    //[self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)onClickCloseButton:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
