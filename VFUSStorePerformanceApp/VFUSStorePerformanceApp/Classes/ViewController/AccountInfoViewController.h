//
//  AccountInfoViewController.h
//
//  Created by Developer 2 on 9/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SFRestAPI.h"
#import "SFRestRequest.h"
#import "BasicViewController.h"

@interface AccountInfoViewController : BasicViewController<SFRestDelegate, UITableViewDataSource, UITableViewDelegate>{
    int requestCount;
    SFRestRequest* fetchStoreRequest;
    SFRestRequest* fetchContactRequest;
}

@property (retain, nonatomic) NSDictionary* data;


@property (retain) NSMutableArray* contactRecords;
@property (retain) NSMutableArray* storeRecords;

@property (retain, nonatomic) IBOutlet UIButton *backButton;
@property (retain, nonatomic) IBOutlet UILabel *parentCompanyNameLabel;
@property (retain, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (retain, nonatomic) IBOutlet UILabel *customerCodeLabel;
@property (retain, nonatomic) IBOutlet UILabel *SAPCodeLabel;
@property (retain, nonatomic) IBOutlet UILabel *customerGroupLabel;
@property (retain, nonatomic) IBOutlet UILabel *accountReceivableLabel;
@property (retain, nonatomic) IBOutlet UILabel *creditLimitLabel;
@property (retain, nonatomic) IBOutlet UILabel *phoneLabel;
@property (retain, nonatomic) IBOutlet UILabel *faxLabel;
@property (retain, nonatomic) IBOutlet UILabel *emailLabel;
@property (retain, nonatomic) IBOutlet UILabel *websiteLabel;
@property (retain, nonatomic) IBOutlet UILabel *mailAddrLabel;

@property (retain, nonatomic) IBOutlet UITableView *storesTable;
@property (retain, nonatomic) IBOutlet UITableView *contactTable;


-(IBAction)onBackButtonClicked:(id)sender;

- (id)initWithDictionary:(NSDictionary*)data;

@end
