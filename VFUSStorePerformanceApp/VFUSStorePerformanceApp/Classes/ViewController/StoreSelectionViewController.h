//
//  StoreSelectionViewController.h
//  VFStorePerformanceApp_Billy
//
//  Created by Developer 2 on 1/4/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol StoreSelectionViewControllerDelegate <NSObject>

//return nil if any
-(void) onSelectdStore:(NSDictionary*) storeObj;

@end

@interface StoreSelectionViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>


@property (retain, nonatomic) NSArray* recordArray;
@property (retain, nonatomic) IBOutlet UITableView *tableView;

@property (assign, nonatomic) id<StoreSelectionViewControllerDelegate> delegate;


- (id)initWithStoreList:(NSArray*)storeArray;


@end
