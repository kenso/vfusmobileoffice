//
//  OfflinePromptPopupView.m
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 4/12/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OfflinePromptPopupView.h"
#import "ShareLocale.h"

@interface OfflinePromptPopupView ()

@end

@implementation OfflinePromptPopupView


-(id)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super initWithCoder:aDecoder]){
        [self baseInit];
    }
    return self;
}

-(id)init{
    if(self = [super init]){
        [self baseInit];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame{
    if(self = [super initWithFrame:frame]){
        [self baseInit];
    }
    return self;
}

-(void) baseInit{

    UILabel* titleLabel = (UILabel*)[self viewWithTag:73601];
    titleLabel.text = [ShareLocale  textFromKey: @"prompt_title"];
    
    UILabel* label1 = (UILabel*)[self viewWithTag:73701];
    UILabel* label2 = (UILabel*)[self viewWithTag:73702];
    UILabel* label3 = (UILabel*)[self viewWithTag:73703];
    label1.text = [ShareLocale  textFromKey: @"prompt_option_1_desc"];
    label2.text = [ShareLocale  textFromKey: @"prompt_option_2_desc"];
    label3.text = [ShareLocale  textFromKey: @"prompt_option_3_desc"];
    
    UIButton* fullDownloadButton = (UIButton*)[self viewWithTag:73801];
    UIButton* visitDownloadButton = (UIButton*)[self viewWithTag:73802];
    UIButton* skipDownloadButton = (UIButton*)[self viewWithTag:73803];
    UIButton* cancelDownloadButton = (UIButton*)[self viewWithTag:73804];
    [fullDownloadButton setTitle:[ShareLocale textFromKey:@"prompt_option_1_button"] forState:UIControlStateNormal];
    [visitDownloadButton setTitle:[ShareLocale textFromKey:@"prompt_option_2_button"] forState:UIControlStateNormal];
    [skipDownloadButton setTitle:[ShareLocale textFromKey:@"prompt_option_3_button"] forState:UIControlStateNormal];
    [cancelDownloadButton setTitle:[ShareLocale textFromKey:@"prompt_option_4_button"] forState:UIControlStateNormal];
    
    
}

-(void) initButtonBindingTo:(id)obj{
    UIButton* fullDownloadButton = (UIButton*)[self viewWithTag:73801];
    UIButton* visitDownloadButton = (UIButton*)[self viewWithTag:73802];
    UIButton* skipDownloadButton = (UIButton*)[self viewWithTag:73803];
    UIButton* cancelDownloadButton = (UIButton*)[self viewWithTag:73804];
    
    [fullDownloadButton addTarget:obj action:@selector(onClickOfflinePopupButton:) forControlEvents:UIControlEventTouchUpInside];
    [visitDownloadButton addTarget:obj action:@selector(onClickOfflinePopupButton:) forControlEvents:UIControlEventTouchUpInside];
    [skipDownloadButton addTarget:obj action:@selector(onClickOfflinePopupButton:) forControlEvents:UIControlEventTouchUpInside];
    [cancelDownloadButton addTarget:obj action:@selector(onClickOfflinePopupButton:) forControlEvents:UIControlEventTouchUpInside];
}


- (void)dealloc {
    [super dealloc];
}

- (IBAction)onClickPopupButton:(id)sender {
    NSLog(@"onClickPopupButton");
}
@end