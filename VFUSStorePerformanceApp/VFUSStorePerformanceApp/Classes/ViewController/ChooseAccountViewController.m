//
//  ChooseAccountViewController.m
//
//  Created by Developer 2 on 4/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import "ChooseAccountViewController.h"
#import "ChooseStoreViewController.h"
#import "OrderCreationManager.h"
#import "AccountInfoViewController.h"
#import "UIHelper.h"
#import "Constants.h"

@interface ChooseAccountViewController ()

@end

@implementation ChooseAccountViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self.searchBar setDelegate:self];
    UITextField *searchField = [self.searchBar valueForKey:@"_searchField"];
    searchField.textColor = [UIColor whiteColor];
    
    [UIHelper applyPurpleSmallButtonImageOnButton:self.backButton];
    [UIHelper applyBlueSmallButtonImageOnButton:self.reloadButton];
    
    self.accountTableView.backgroundColor = TABLE_BG_COLOR_RED;
    self.accountTableView.backgroundView = nil;
    self.recordAry = [NSArray array];
    self.displayRecordAry = [[NSMutableArray alloc] init];
    self.searchText = [[NSMutableString alloc] init];
    self.accountTableView.delegate = self;
    self.accountTableView.dataSource = self;
    [self loadData];
    
}


- (void)dealloc {
    [_accountTableView release];
    [_backButton release];
    [_reloadButton release];
    [_searchBar release];
    [super dealloc];
}

#pragma mark - Data Handling
- (void) loadData{
    [self showLoadingPopup: self.view];
    SFRestRequest *request = [[SFRestAPI sharedInstance] requestForQuery:@"SELECT Id, Name, Parent.Id, Parent.Name, Account_Code__c, Customer_Code__c, Credit_Limit__c, Account_Receivable__c, Customer_Group__c, Customer_Group__r.Name, Email__c, Mailing_Address__c, Phone, Fax, Website, To_Div_Head__c FROM Account WHERE Active__c = TRUE ORDER BY Name asc"];
    [[SFRestAPI sharedInstance] send:request delegate:self];
    
}


- (void)request:(SFRestRequest *)request didLoadResponse:(id)jsonResponse{
    [self hideLoadingPopupWithFadeout];
    NSLog(@"ChooseAccountViewController::didLoadResponse() jsonResponse = %@", jsonResponse);
    id done = [jsonResponse objectForKey:@"done"];
    if(done!=nil && [done intValue]==1){
        //get array of records with key "records"
        //get number of record from "totalSize"
        NSArray* records = [jsonResponse objectForKey:@"records"];
        self.recordAry = records;
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [self filterDisplayData];
            [self.accountTableView reloadData];
        });

    }else{
        
    }
}

- (void)request:(SFRestRequest *)request didFailLoadWithError:(NSError*)error{
    [self hideLoadingPopupWithFadeout];
    [super popupErrorDialogWithMessage: [NSString stringWithFormat:@"Error: %@", error]];
}

- (void)requestDidCancelLoad:(SFRestRequest *)request{
    [self hideLoadingPopupWithFadeout];
    [super popupErrorDialogWithMessage: @"Request is cancelled"];
}

- (void)requestDidTimeout:(SFRestRequest *)request{
    [self hideLoadingPopupWithFadeout];
    [super popupErrorDialogWithMessage: @"Loading time out"];
}


- (void)filterDisplayData{
    [self.displayRecordAry removeAllObjects];
    BOOL match = NO;

    if ( self.searchText == nil || self.searchText == (id)[NSNull null] || [self.searchText isEqualToString:@""] ){
        [self.displayRecordAry addObjectsFromArray: self.recordAry];
        return;
    }
    
    for ( NSDictionary *item in self.recordAry ){
        match = NO;
        NSString *nameStr = [[NSString alloc] init];
        NSString *codeStr = [[NSString alloc] init];
        
        if ( [item objectForKey: @"Name"] != nil && [item objectForKey: @"Name"] != (id)[NSNull null] ){
            nameStr = [[NSString stringWithFormat:@"%@", [item objectForKey: @"Name"]] lowercaseString];
        }else {
            nameStr = @"";
        }
        
        if ( [item objectForKey: @"Customer_Code__c"] != nil && [item objectForKey: @"Customer_Code__c"] != (id)[NSNull null] ){
            codeStr = [[NSString stringWithFormat:@"%@", [item objectForKey: @"Customer_Code__c"]] lowercaseString];
        }else {
            codeStr = @"";
        }
        
        if ( [nameStr rangeOfString:[self.searchText lowercaseString] ].location != NSNotFound ){
            match = YES;
        }else if ( [codeStr rangeOfString:[self.searchText lowercaseString] ].location != NSNotFound ){
            match  = YES;
        }
        
        if ( match ){
            [self.displayRecordAry addObject:item];
        }
    }
}



#pragma mark - Table Data source handling
- (float) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    return [self.recordAry count];
    return [self.displayRecordAry count];
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
    if(cell == nil){
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyIdentifier"] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.hidden = YES;
        cell.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(10, 0, 188, 40) parent:cell tag: 901];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(198, 0, 270, 40) parent:cell tag: 902];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(428, 0, 110, 40) parent:cell tag: 903];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(538, 0, 110, 40) parent:cell tag: 904];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(688, 0, 180, 40) parent:cell tag: 905];
        [self createNewButtonWithDefaultStyle: CGRectMake(868, 4, 62, 38) parent:cell tag:906];
        [self createNewButtonWithDefaultStyle: CGRectMake(938, 4, 62, 38) parent:cell tag:907];
    }
    
    
    NSLog(@"tag = %i", indexPath.row);
    cell.tag = indexPath.row;
    
    NSDictionary* data = [self.displayRecordAry objectAtIndex: indexPath.row];
    
    UILabel* orderIdTxtView = (UILabel*)[cell viewWithTag:901];
    orderIdTxtView.text = [self getFieldDisplayWithFieldName:@"Customer_Code__c" withDictionary: data];
    UILabel* statusTxtView = (UILabel*)[cell viewWithTag:902];
    statusTxtView.text = [self getFieldDisplayWithFieldName:@"Name" withDictionary: data];
    UILabel* referIdTxtView = (UILabel*)[cell viewWithTag:903];
    referIdTxtView.text = [self getSimplePriceStringWithFloat:[[data objectForKey:@"Credit_Limit__c"] floatValue]];
//    referIdTxtView.text = [self getFieldDisplayWithFieldName:@"Credit_Limit__c" withDictionary: data];
    [referIdTxtView setTextAlignment:NSTextAlignmentRight];
    UILabel* brandTxtView = (UILabel*)[cell viewWithTag:904];
    brandTxtView.text = [self getSimplePriceStringWithFloat:[[data objectForKey:@"Account_Receivable__c"] floatValue]];
//    brandTxtView.text = [self getFieldDisplayWithFieldName:@"Account_Receivable__c" withDictionary: data];
    [brandTxtView setTextAlignment:NSTextAlignmentRight];
    UILabel* dateTxtView = (UILabel*)[cell viewWithTag:905];
    dateTxtView.text = [self getFieldDisplayWithFieldName:@"Customer_Group__r.Name" withDictionary: data];
    UIButton* selectButton = (UIButton*)[cell viewWithTag:906];
    [selectButton setTitle:@"Select" forState:UIControlStateNormal];
    [selectButton setTitle:@"Select" forState:UIControlStateSelected];
    [selectButton setTitle:@"Select" forState:UIControlStateDisabled];
    [selectButton addTarget:self action:@selector(onSelectedAccount:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton* infoButton = (UIButton*)[cell viewWithTag:907];
    [infoButton setTitle:@"Info" forState:UIControlStateNormal];
    [infoButton setTitle:@"Info" forState:UIControlStateSelected];
    [infoButton setTitle:@"Info" forState:UIControlStateDisabled];
    [infoButton addTarget:self action:@selector(onShowAccountInfo:) forControlEvents:UIControlEventTouchUpInside];

    return cell;
}

-(void) onShowAccountInfo:(id)sender{
    NSLog(@"onShowAccountInfo %@", sender);
    UIView* v = (UIView*)sender;
    NSInteger tag = v.superview.superview.tag;
    NSDictionary* data = [self.displayRecordAry objectAtIndex: tag];

    AccountInfoViewController* vc = [[[AccountInfoViewController alloc] initWithDictionary:data] autorelease];
    [self.navigationController pushViewController: vc animated:YES];

}



-(void) onSelectedAccount:(id)sender{
    UIView* view = sender;
    int tagVal = view.superview.superview.tag;
    OrderCreationManager* mgr = [OrderCreationManager shareInstance];
    mgr.accountId = [[self.displayRecordAry objectAtIndex:tagVal] objectForKey:@"Id"];
    mgr.accountName = [[self.displayRecordAry objectAtIndex:tagVal] objectForKey:@"Name"];
    mgr.customerGroup = [[self.displayRecordAry objectAtIndex:tagVal] objectForKey:@"Customer_Group__c"];
    mgr.accountCode = [[self.displayRecordAry objectAtIndex:tagVal] objectForKey:@"Account_Code__c"];
    mgr.toDivHead = [[[self.displayRecordAry objectAtIndex:tagVal] objectForKey:@"To_Div_Head__c"] boolValue];
    
    ChooseStoreViewController* vc = [[[ChooseStoreViewController alloc] init] autorelease];
    [self.navigationController pushViewController: vc animated:YES];

}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//    cell.contentView.backgroundColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:1.0];
}




//#pragma mark - Handle Click event of Back button & Reload button
- (IBAction)onBackButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onReloadButtonClicked:(id)sender {
    [self loadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    NSLog(@"SerachBar didChange with : %@", searchText);
    [self.searchText setString:searchText];
    [self filterDisplayData];
    [self.accountTableView reloadData];
}

@end
