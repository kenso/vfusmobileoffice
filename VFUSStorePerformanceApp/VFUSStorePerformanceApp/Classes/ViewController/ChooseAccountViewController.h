//
//  ChooseAccountViewController.h
//
//  Created by Developer 2 on 4/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SFRestAPI.h"
#import "SFRestRequest.h"
#import "BasicViewController.h"

@interface ChooseAccountViewController : BasicViewController<SFRestDelegate, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>{

}
@property (retain, nonatomic) IBOutlet UIButton *reloadButton;
@property (retain, nonatomic) IBOutlet UIButton *backButton;
@property (retain, nonatomic) IBOutlet UITableView *accountTableView;
@property (retain, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, retain) NSMutableString *searchText;


@property (retain, nonatomic) NSArray* recordAry;
@property (retain, nonatomic) NSMutableArray* displayRecordAry;

-(IBAction)onBackButtonClicked:(id)sender;

-(IBAction)onReloadButtonClicked:(id)sender;

@end
