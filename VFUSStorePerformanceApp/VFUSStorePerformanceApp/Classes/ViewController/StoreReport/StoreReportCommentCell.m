//
//  StoreReportCommentCell.m
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 23/3/2016.
//  Copyright © 2016 vf.com. All rights reserved.
//

#import "StoreReportCommentCell.h"
#import "DataHelper.h"
#import "GeneralUiTool.h"
#import "WebImageView.h"

@implementation StoreReportCommentCell

- (void)awakeFromNib {
    self.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];


}

-(void) reloadDisplay{
    
    NSString* desc = (NSString*)gfrd(self.customData, @"Description__c", @"");
    desc = [desc stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];

    self.subjectLabel.text = (NSString*)gfrd(self.customData, @"Subject__c", @"");
    self.descriptionLabel.text =  desc;
    
    NSInteger startMediaIndex = 65781;
    NSMutableArray* imgViewArray = [NSMutableArray array];
    for(int i=0; i<10; i++){
        WebImageView* view = (WebImageView*)[self viewWithTag: (startMediaIndex+i)];
        view.hidden = YES;
        view.image = nil;
        [imgViewArray addObject: view];
    }
    
    NSDictionary* mdata = self.customData[@"Shared_Media__r"];
    if(mdata!=nil && mdata!=(id)[NSNull null]){
        NSArray* mediaArray = mdata[@"records"];
        int index = 0;
        for(NSDictionary* m in mediaArray){
            NSString* feedID = m[@"emfa__FeedItemId__c"];
//            NSString* ext = m[@"emfa__File_Ext__c"];
            WebImageView* view = imgViewArray[index];
            view.hidden = NO;
            [view loadImageWithFeedID:feedID completeBlock:^(WebImageView *imgView, UIImage *img) {
                
            }];
            index++;
        }
    }
    
    self.dateLabel.text = [NSString stringWithFormat:@"%@", (NSString*)gfrd(self.customData, @"Creator__r.Name", @"") ];
}


- (void)dealloc {
    [_subjectLabel release];
    [_dateLabel release];
    [_descriptionLabel release];
    [super dealloc];
}
@end
