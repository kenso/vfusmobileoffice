//
//  StoreReportCommentCell.h
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 23/3/2016.
//  Copyright © 2016 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoreReportCommentCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *subjectLabel;

@property (retain, nonatomic) NSDictionary* customData;

@property (retain, nonatomic) IBOutlet UILabel *dateLabel;

@property (retain, nonatomic) IBOutlet UILabel *descriptionLabel;

-(void) reloadDisplay;

@end
