//
//  StoreReportVC.m
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 22/3/\n2016.
//  Copyright © 2016 vf.com. All rights reserved.
//

#import "StoreReportVC.h"
#import "StoreReportCommentCell.h"
#import "StoreReportPhotoCell.h"
#import "Constants.h"
#import "SurveyFormViewController.h"
#import "CommentPopupView.h"
#import "GeneralUiTool.h"
#import "VFDAO.h"
#import "DataHelper.h"
#import "ShareLocale.h"

#define ACTIVE_BUTTON_COLOR [UIColor colorWithRed:0x37/255.0f green:0x37/255.0f blue:0x37/255.0f alpha:1]
#define INACTIVE_BUTTON_COLOR [UIColor colorWithRed:20/255.0f green:20/255.0f blue:20/255.0f alpha:1]

#define COMMENT_CELL_ID @"StoreReportCommentCell"
#define PHOTO_CELL_ID @"StoreReportPhotoCell"

@interface StoreReportVC ()

@end

@implementation StoreReportVC



-(id) initWithStoreID:(NSString*)storeID{
    if(self = [super init]){
        self.storeID = storeID;
    }
    return self;
}



#pragma mark - Network function
-(void) loadData{
    [self showLoadingPopup:self.view];
    [self loadReportData];

}

-(void) loadReportData{//Inventory tab, Fixed with latest cut to date
    VFDAO* vf = [VFDAO sharedInstance];
    self.storeReportSummaryRequest = [vf selectRecentStoreReportsWithDelegate:self storeId:self.storeID];
    NSLog(@"loadReportData");
}

-(void) loadComments{
    VFDAO* vf = [VFDAO sharedInstance];
    self.storeCommentsRequest = [vf selectCommentsWithDelegate:self withStoreID:self.storeID];
    NSLog(@"loadComments");
}



-(void)daoRequest:(DAOBaseRequest*)request queryOnlineDataSuccess:(NSArray*) response{

    if(self.storeReportSummaryRequest==request){
        self.reportDataArray = [NSMutableArray array];
        
        //1 for three months, 2 for six months, 3 for 1 year
        float visitCount1 = 0;
        float visitCount2 = 0;
        float visitCount3 = 0;
        float auditCount1 = 0;
        float auditCount2 = 0;
        float auditCount3 = 0;
        float mspCount1 = 0;
        float mspCount2 = 0;
        float mspCount3 = 0;
        float visitSum1 = 0;
        float visitSum2 = 0;
        float visitSum3 = 0;
        float auditSum1 = 0;
        float auditSum2 = 0;
        float auditSum3 = 0;
        float mspSum1 = 0;
        float mspSum2 = 0;
        float mspSum3 = 0;
        
        for(NSDictionary* reportData in response){
            NSDictionary* surveyData = reportData[@"Survey__r"];
            if(surveyData==(id)[NSNull null]) continue;
            
            NSDate* visitDate = [[GeneralUiTool getSFDateFormatter] dateFromString: reportData[@"Planned_Start_Date_Time__c"] ];
            
            NSArray* surveys = surveyData[@"records"];
            NSString* visitScore = (id)[NSNull null];
            NSString* auditScore = (id)[NSNull null];
            NSString* mspScore = (id)[NSNull null];
            
            float period1 = 24*60*60*365/12.0f*3;
            float period2 = 24*60*60*365/12.0f*6;
            float period3 = 24*60*60*365/12.0f*12;
            
            NSString* visitFormID = (id)[NSNull null];
            NSString* auditFormID = (id)[NSNull null];
            NSString* mspFormID = (id)[NSNull null];
            
            if(surveys!=nil && surveys!=(id)[NSNull null]){
                for(NSDictionary* s in surveys){
                    if ([s[@"Master_Type__c"] isEqualToString:@"Visit Form"]){
                        float vs = [s[@"Total_Score__c"] floatValue];
                        if ( -[visitDate timeIntervalSinceNow] < period1 ){
                            visitCount1 += 1;
                            visitSum1 += vs;
                        }
                        if ( -[visitDate timeIntervalSinceNow] < period2 ){
                            visitCount2 += 1;
                            visitSum2 += vs;
                        }
                        if ( -[visitDate timeIntervalSinceNow] < period3 ){
                            visitCount3 += 1;
                            visitSum3 += vs;
                        }
                        visitScore = [NSString stringWithFormat:@"%.0f", vs];
                        visitFormID = s[@"Id"];
                    }
                    if ([s[@"Master_Type__c"] isEqualToString:@"SOP Audit"]){
                        float vs = [s[@"Total_Score__c"] floatValue];
                        if ( -[visitDate timeIntervalSinceNow] < period1 ){
                            auditCount1 += 1;
                            auditSum1 += vs;
                        }
                        if ( -[visitDate timeIntervalSinceNow] < period2 ){
                            auditCount2 += 1;
                            auditSum2 += vs;
                        }
                        if ( -[visitDate timeIntervalSinceNow] < period3 ){
                            auditCount3 += 1;
                            auditSum3 += vs;
                        }
                        auditScore = [NSString stringWithFormat:@"%.0f", vs];
                        auditFormID = s[@"Id"];
                    }
                    if ([s[@"Master_Type__c"] isEqualToString:@"MSP Survey"]){
                        float vs = [s[@"Total_Score__c"] floatValue];
                        if ( -[visitDate timeIntervalSinceNow] < period1 ){
                            mspCount1 += 1;
                            mspSum1 += vs;
                        }
                        if ( -[visitDate timeIntervalSinceNow] < period2 ){
                            mspCount2 += 1;
                            mspSum2 += vs;
                        }
                        if ( -[visitDate timeIntervalSinceNow] < period3 ){
                            mspCount3 += 1;
                            mspSum3 += vs;
                        }
                        mspScore = [NSString stringWithFormat:@"%.0f", vs];
                        mspFormID = s[@"Id"];
                    }
                }
            }
            NSDateFormatter* df = [[[NSDateFormatter alloc] init] autorelease];
            [df setDateFormat:@"MMM\r\nYYYY"];
            NSString* timeStr = [df stringFromDate: visitDate ];
            
            [self.reportDataArray addObject:@{
                                              @"date": timeStr,
                                              @"visit": visitScore,
                                              @"audit": auditScore,
                                              @"msp": mspScore,
                                              @"visitID":visitFormID,
                                              @"auditID":auditFormID,
                                              @"mspID":mspFormID
                                              }];
        }
        
        if(visitCount1>0) self.visitSummaryLabel1.text = [NSString stringWithFormat:@"%.1f", visitSum1/visitCount1];
        if(visitCount2>0) self.visitSummaryLabel2.text = [NSString stringWithFormat:@"%.1f", visitSum2/visitCount2];
        if(visitCount3>0) self.visitSummaryLabel3.text = [NSString stringWithFormat:@"%.1f", visitSum3/visitCount3];
        if(auditCount1>0) self.auditSummaryLabel1.text = [NSString stringWithFormat:@"%.1f", auditSum1/auditCount1];
        if(auditCount2>0) self.auditSummaryLabel2.text = [NSString stringWithFormat:@"%.1f", auditSum2/auditCount2];
        if(auditCount3>0) self.auditSummaryLabel3.text = [NSString stringWithFormat:@"%.1f", auditSum3/auditCount3];
        if(mspCount1>0) self.mspSummaryLabel1.text = [NSString stringWithFormat:@"%.1f", mspSum1/mspCount1];
        if(mspCount2>0) self.mspSummaryLabel2.text = [NSString stringWithFormat:@"%.1f", mspSum2/mspCount2];
        if(mspCount3>0) self.mspSummaryLabel3.text = [NSString stringWithFormat:@"%.1f", mspSum3/mspCount3];
        
    
        [self refreshStoreReportSummary];
        
        [self loadComments];
    }
    else if (request == self.storeCommentsRequest){
        [self hideLoadingPopup];
        
        
        NSDateFormatter* df = [GeneralUiTool getSFSimpleDateFormatter];
        NSDateFormatter* df2 = [GeneralUiTool getSFSimpleDateFormatter];
        [df2 setDateFormat:DEFAULT_DATE_TIME_FORMAT];
        
        self.commentDisplayArray = [NSMutableArray array];
        for(NSDictionary* commentData in response){
            
            NSString* actDateStr = (NSString*)gfrd(commentData, @"ActivityDate__c", @"");
            NSDate* visitDate = [[GeneralUiTool getSFDateFormatter] dateFromString: actDateStr ];
            actDateStr = [df stringFromDate:visitDate];
            
            if([self.commentDisplayArray count]==0){
                NSMutableDictionary* sectionData = [NSMutableDictionary dictionary];
                sectionData[@"Name"] =  actDateStr;
                sectionData[@"records"] =  [NSMutableArray array];
                [self.commentDisplayArray addObject: sectionData];
            }
            else {
                NSDictionary* lastSectionData = [self.commentDisplayArray lastObject];
                if( ![lastSectionData[@"Name"] isEqualToString:actDateStr] ){
                    NSMutableDictionary* sectionData = [NSMutableDictionary dictionary];
                    sectionData[@"Name"] =  actDateStr;
                    sectionData[@"records"] =  [NSMutableArray array];
                    [self.commentDisplayArray addObject: sectionData];
                }
            }
            NSDictionary* sectionData = [self.commentDisplayArray lastObject];
            NSMutableArray* recordsInSection = sectionData[@"records"];
            [recordsInSection addObject:commentData];
            
            //handle media
            NSDictionary* mdata = commentData[@"Shared_Media__r"];
            if(mdata!=nil && mdata!=(id)[NSNull null]){
                NSArray* mediaArray = mdata[@"records"];
                NSString* timestampStr = [df2 stringFromDate:visitDate];
                for(NSDictionary* media in mediaArray){
                    NSMutableDictionary* lastMediaData =  [self.photoDisplayArray lastObject];
                    if(lastMediaData==nil || lastMediaData[@"feed_id3"]!=[NSNull null]){
                        [self.photoDisplayArray addObject:[@{@"feed_id1":[NSNull null],
                                                            @"feed_id2":[NSNull null],
                                                            @"feed_id3":[NSNull null]} mutableCopy]];
                    }
                    lastMediaData =  [self.photoDisplayArray lastObject];
                    
                    if(lastMediaData[@"feed_id1"]==(id)[NSNull null]){
                        lastMediaData[@"feed_id1"] = media[@"emfa__FeedItemId__c"];
                        lastMediaData[@"label_1"] = timestampStr;
                    }
                    else if(lastMediaData[@"feed_id2"]==(id)[NSNull null]){
                        lastMediaData[@"feed_id2"] = media[@"emfa__FeedItemId__c"];
                        lastMediaData[@"label_2"] = timestampStr;
                    }
                    else if(lastMediaData[@"feed_id3"]==(id)[NSNull null]){
                        lastMediaData[@"feed_id3"] = media[@"emfa__FeedItemId__c"];
                        lastMediaData[@"label_3"] = timestampStr;
                    }
                }
            }
        }
        [self.commentTableView reloadData];
    }
}

-(void)daoRequest:(DAOBaseRequest*)request queryOnlineDataError:(NSString*) response code:(int)code{
    if(self.storeReportSummaryRequest==request){
        
    }
    
}



#pragma mark - Standard UI handling
- (void)viewDidLoad {
    [super viewDidLoad];
    index = 0;
    
    VFDAO* vf = [VFDAO sharedInstance];
    NSDictionary* storeObj = [vf getSingleStoreById: self.storeID];
    self.titleLabel.text = storeObj[@"Name"];

    self.commentTableView.dataSource = self;
    self.commentTableView.delegate = self;
    
    self.photoTableView.dataSource = self;
    self.photoTableView.delegate = self;
    
    self.commentTableView.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
    self.photoTableView.backgroundColor =   [UIColor colorWithWhite:1 alpha:0];
    
    self.visitFormLabel.text = [ShareLocale textFromKey:@"surveytitle_visit"];
    self.auditFormLabel.text = [ShareLocale textFromKey:@"surveytitle_audit"];
    self.mspFormLabel.text =  [ShareLocale textFromKey:@"surveytitle_msp"];
    self.dateFormLabel.text = [ShareLocale textFromKey:@"surveylist_date"];
    self.period1Label.text = [ShareLocale textFromKey:@"store_period_1"];
    self.period2Label.text = [ShareLocale textFromKey:@"store_period_2"];
    self.period3Label.text = [ShareLocale textFromKey:@"store_period_3"];
    [self.menuCommentButton setTitle:[ShareLocale textFromKey:@"Comment"] forState:UIControlStateNormal];
    [self.menuPhotoButton setTitle:[ShareLocale textFromKey:@"photo"] forState:UIControlStateNormal];
    
    
    self.rawCommentArray = [[NSMutableArray alloc] init];
    self.photoDisplayArray = [[NSMutableArray alloc] init];
        [self.commentTableView reloadData];
    [self.photoTableView reloadData];
    
    [self reloadTableDisplay];
    
    NSLog(@"viewDidload");
    
    [self loadData];

    
}


-(void) refreshStoreReportSummary{
    
    float ox = 160;
    float oy = 5;
    float sy = 40;
    
    UIFont* font = [UIFont fontWithName:@"Helvetica Neue" size:12];
    
    int dataIndex = 0;
    for(NSDictionary* reportData in self.reportDataArray){
        UILabel* dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(ox+4, oy, 30, 30)];
        UIButton* visitLabel = [UIButton buttonWithType:UIButtonTypeCustom]; //[[UIButton alloc] initWithFrame:CGRectMake(ox, oy+sy, 30, 30)];
        UIButton* auditLabel = [UIButton buttonWithType:UIButtonTypeCustom];//[[UIButton alloc] initWithFrame:CGRectMake(ox, oy+sy*2, 30, 30)];
        UIButton* mspLabel = [UIButton buttonWithType:UIButtonTypeCustom];//[[UIButton alloc] initWithFrame:CGRectMake(ox, oy+sy*3, 30, 30)];
        
        visitLabel.tag  = 71000 + dataIndex;
        auditLabel.tag  = 72000 + dataIndex;
        mspLabel.tag    = 73000 + dataIndex;
        
        [visitLabel addTarget:self action:@selector(onClickSurveyScore:) forControlEvents:UIControlEventTouchUpInside];
        [auditLabel addTarget:self action:@selector(onClickSurveyScore:) forControlEvents:UIControlEventTouchUpInside];
        [mspLabel addTarget:self action:@selector(onClickSurveyScore:) forControlEvents:UIControlEventTouchUpInside];
        
        visitLabel.frame = CGRectMake(ox, oy+sy, 30, 30);
        auditLabel.frame = CGRectMake(ox, oy+sy*2, 30, 30);
        mspLabel.frame = CGRectMake(ox, oy+sy*3, 30, 30);
        
        dateLabel.numberOfLines = 2;
        dateLabel.textColor = [UIColor whiteColor];
        dateLabel.font = font;
        
        visitLabel.titleLabel.font = font;
        auditLabel.titleLabel.font = font;
        mspLabel.titleLabel.font = font;
        
        ox+=35;
        [self.scoreView addSubview:dateLabel];
        if(reportData[@"date"] != [NSNull null]){
            dateLabel.text = reportData[@"date"];
        }else{
            dateLabel.text = @"";
        }
        if(reportData[@"visit"] != [NSNull null]){
            [visitLabel setTitle: [NSString stringWithFormat:@"%@",reportData[@"visit"]] forState:UIControlStateNormal] ;
            [self.scoreView addSubview:visitLabel];
        }
        if(reportData[@"audit"] != [NSNull null]){
            [auditLabel setTitle: [NSString stringWithFormat:@"%@",reportData[@"audit"]] forState:UIControlStateNormal] ;
            [self.scoreView addSubview:auditLabel];
        }
        if(reportData[@"msp"] != [NSNull null]){
            [self.scoreView addSubview:mspLabel];
            [mspLabel setTitle: [NSString stringWithFormat:@"%@",reportData[@"msp"]] forState:UIControlStateNormal] ;
        }
        dataIndex+=1;
    }

}

- (IBAction)onHomeButtonClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)onBackButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}




- (void)dealloc {
    [_commentTableView release];
    [_photoTableView release];
    [_menuCommentButton release];
    [_menuPhotoButton release];
    [_visitFormLabel release];
    [_auditFormLabel release];
    [_mspFormLabel release];
    [_scoreView release];
    [_titleLabel release];
    [_visitSummaryLabel1 release];
    [_auditSummaryLabel1 release];
    [_mspSummaryLabel1 release];
    [_visitSummaryLabel2 release];
    [_visitSummaryLabel3 release];
    [_auditSummaryLabel2 release];
    [_auditSummaryLabel3 release];
    [_mspSummaryLabel2 release];
    [_mspSummaryLabel3 release];
    [_dateFormLabel release];
    [_period1Label release];
    [_period2Label release];
    [_period3Label release];
    [super dealloc];
}


#pragma mark - Tabbar menu button handling
- (IBAction)onClickMenuCommentButton:(id)sender {
    index = 0;
    self.menuCommentButton.backgroundColor = ACTIVE_BUTTON_COLOR;
    self.menuPhotoButton.backgroundColor = INACTIVE_BUTTON_COLOR;
    [self reloadTableDisplay];
}

- (IBAction)onClickMenuPhotoButton:(id)sender {
    index = 1;
    self.menuPhotoButton.backgroundColor = ACTIVE_BUTTON_COLOR;
    self.menuCommentButton.backgroundColor = INACTIVE_BUTTON_COLOR;
    [self reloadTableDisplay];
}




#pragma mark - TableView handling

-(void) reloadTableDisplay{
    if(index==0){
        self.commentTableView.hidden = NO;
        self.photoTableView.hidden = YES;
        [self.commentTableView reloadData];
    }
    else{
        self.commentTableView.hidden = YES;
        self.photoTableView.hidden = NO;
        [self.photoTableView reloadData];
    }
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(tableView==_commentTableView){
        return [self.commentDisplayArray count];
    }
    else{
        return 1;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if(tableView==_commentTableView){
        UILabel *myLabel = [[UILabel alloc] init];
        myLabel.frame = CGRectMake(0, 0, tableView.frame.size.width - 5, 16);
        myLabel.font = [UIFont boldSystemFontOfSize:14];
        myLabel.text = [self tableView:tableView titleForHeaderInSection:section];
        myLabel.textColor = [UIColor whiteColor];
        myLabel.textAlignment = NSTextAlignmentRight;
        
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width - 5, 16)];
        headerView.backgroundColor = [UIColor colorWithWhite:1 alpha:.05f];
        [headerView addSubview:myLabel];
        
        return headerView;
    }
    else{
        UIView *headerView = [[UIView alloc] init];
        headerView.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        return headerView;
    }
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(tableView==_commentTableView){
        return self.commentDisplayArray[section][@"Name"];
    }else{
        return @"";
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView==_commentTableView){
        NSDictionary* data = self.commentDisplayArray[indexPath.section][@"records"][indexPath.row];
        NSDictionary* mdata = data[@"Shared_Media__r"];
        if(mdata==nil || mdata==(id)[NSNull null]){
            return 60;
        }else{
            NSArray* mediaArray = mdata[@"records"];
            if(mediaArray!=nil && [mediaArray count]>0) return 140;
            return 60;
        }

    }else{
        return 241;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView==_commentTableView){
        return [self.commentDisplayArray[section][@"records"] count];
    }
    else{
        return [self.photoDisplayArray count];
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CommentPopupView* popup =      [[[NSBundle mainBundle] loadNibNamed:@"CommentPopupView" owner:self options:nil] objectAtIndex:0];
    [popup reloadWithCommentData: self.commentDisplayArray[indexPath.section][@"records"][indexPath.row]];
    [self.view addSubview: popup];

}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString* identifer = nil;
    if(_commentTableView == tableView){
        identifer = COMMENT_CELL_ID;
    }else{
        identifer = PHOTO_CELL_ID;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifer];
    if(cell == nil){
        NSArray* viewArray = [[NSBundle mainBundle] loadNibNamed:identifer owner:self options:nil];
       cell = (UITableViewCell*)[viewArray firstObject];
    }
    
    if(_commentTableView == tableView){
        StoreReportCommentCell* tcell = (StoreReportCommentCell*)cell;
        NSDictionary* data = self.commentDisplayArray[indexPath.section][@"records"][indexPath.row];
        tcell.customData = data;
        [tcell reloadDisplay];
    }
    else{
        StoreReportPhotoCell* tcell = (StoreReportPhotoCell*)cell;
        tcell.customData = self.photoDisplayArray[indexPath.row];
        [tcell.imgView1 enableLongPressPreviewOnParentView:self.view onViewController:self];
        [tcell.imgView2 enableLongPressPreviewOnParentView:self.view onViewController:self];
        [tcell.imgView3 enableLongPressPreviewOnParentView:self.view onViewController:self];
        [tcell reloadDisplay];
    }
    
    return cell;
}


-(void) onClickSurveyScore:(id) sender{
    UIButton* button = (UIButton*)sender;
    NSString* recordID;
    if (button.tag >= 73000){
        recordID = self.reportDataArray[button.tag-73000][@"mspID"];
    }else if (button.tag >= 72000){
        recordID = self.reportDataArray[button.tag-72000][@"auditID"];
    }
    else{
        recordID = self.reportDataArray[button.tag-71000][@"visitID"];
    }
    
    SurveyFormViewController* vc = [[SurveyFormViewController alloc] initWithSurveyMode:SurveyFormModePreviewSurvey andSurveyId:recordID editable:NO];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
