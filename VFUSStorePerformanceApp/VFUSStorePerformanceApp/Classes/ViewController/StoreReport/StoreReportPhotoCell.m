//
//  StoreReportPhotoCell.m
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 23/3/2016.
//  Copyright © 2016 vf.com. All rights reserved.
//

#import "StoreReportPhotoCell.h"

@implementation StoreReportPhotoCell

- (void)awakeFromNib {
    self.backgroundColor = [UIColor colorWithWhite:1 alpha:0];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)dealloc {
    [_imgView1 release];
    [_imgView2 release];
    [_imgView3 release];
    [_customData release];
    [_label1 release];
    [_label2 release];
    [_label3 release];
    [super dealloc];
}


-(void) reloadDisplay{
    if(self.customData[@"feed_id1"]!=[NSNull null]){
        _imgView1.data = [@{@"emfa__FeedItemId__c":self.customData[@"feed_id1"]} mutableCopy];
        _imgView1.contentMode = UIViewContentModeScaleAspectFit;
        [_imgView1 loadImageWithFeedID:self.customData[@"feed_id1"] completeBlock:^(WebImageView *imgView, UIImage *img) {
            _imgView1.contentMode = UIViewContentModeScaleAspectFit;
            
        }];
        _label1.hidden = NO;
        _label1.text = self.customData[@"label_1"];
    }else{
        _imgView1.data = nil;
        _imgView1.feedId = nil;
        _imgView1.image = nil;
        _label1.hidden = YES;
    }
    if(self.customData[@"feed_id2"]!=[NSNull null]){
        _imgView2.data = [@{@"emfa__FeedItemId__c":self.customData[@"feed_id2"]} mutableCopy];
        _imgView2.contentMode = UIViewContentModeScaleAspectFit;
        [_imgView2 loadImageWithFeedID:self.customData[@"feed_id2"] completeBlock:^(WebImageView *imgView, UIImage *img) {
            _imgView2.contentMode = UIViewContentModeScaleAspectFit;
        }];
         _label2.hidden = NO;
        _label2.text = self.customData[@"label_2"];
    }else{
        _imgView2.data = nil;
        _imgView2.feedId = nil;
        _imgView2.image = nil;
        _label2.hidden = YES;
        
    }
    if(self.customData[@"feed_id3"]!=[NSNull null]){
        _imgView3.data = [@{@"emfa__FeedItemId__c":self.customData[@"feed_id3"]} mutableCopy];
        _imgView3.contentMode = UIViewContentModeScaleAspectFit;
        [_imgView3 loadImageWithFeedID:self.customData[@"feed_id3"] completeBlock:^(WebImageView *imgView, UIImage *img) {
            _imgView3.contentMode = UIViewContentModeScaleAspectFit;
        }];
        _label3.hidden = NO;
        _label3.text = self.customData[@"label_3"];
    }else{
        _imgView3.data = nil;
        _imgView3.feedId = nil;
        _imgView3.image = nil;
        _label3.hidden = YES;
        
    }

    

}



@end
