//
//  StoreReportVC.h
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 22/3/2016.
//  Copyright © 2016 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"

@interface StoreReportVC : BasicViewController<DAODelegate, UITableViewDataSource, UITableViewDelegate>{
    NSInteger index;
}
@property (retain, nonatomic) IBOutlet UILabel *titleLabel;

@property (retain, nonatomic) NSString* storeID;
@property (retain, nonatomic) IBOutlet UIView *scoreView;

@property (retain, nonatomic) NSMutableArray* reportDataArray;

@property (retain, nonatomic) DAOBaseRequest* storeReportSummaryRequest;
@property (retain, nonatomic) DAOBaseRequest* storeCommentsRequest;

@property (retain, nonatomic) NSMutableArray* rawCommentArray;
@property (retain, nonatomic) NSMutableArray* commentDisplayArray;
@property (retain, nonatomic) NSMutableArray* photoDisplayArray;

@property (retain, nonatomic) IBOutlet UITableView *commentTableView;
@property (retain, nonatomic) IBOutlet UITableView *photoTableView;

@property (retain, nonatomic) IBOutlet UILabel *visitSummaryLabel1;
@property (retain, nonatomic) IBOutlet UILabel *auditSummaryLabel1;
@property (retain, nonatomic) IBOutlet UILabel *mspSummaryLabel1;
@property (retain, nonatomic) IBOutlet UILabel *visitSummaryLabel2;
@property (retain, nonatomic) IBOutlet UILabel *auditSummaryLabel2;
@property (retain, nonatomic) IBOutlet UILabel *mspSummaryLabel2;
@property (retain, nonatomic) IBOutlet UILabel *visitSummaryLabel3;
@property (retain, nonatomic) IBOutlet UILabel *auditSummaryLabel3;
@property (retain, nonatomic) IBOutlet UILabel *mspSummaryLabel3;



@property (retain, nonatomic) IBOutlet UIButton *menuCommentButton;
@property (retain, nonatomic) IBOutlet UIButton *menuPhotoButton;

@property (retain, nonatomic) IBOutlet UILabel *dateFormLabel;
@property (retain, nonatomic) IBOutlet UILabel *visitFormLabel;
@property (retain, nonatomic) IBOutlet UILabel *auditFormLabel;
@property (retain, nonatomic) IBOutlet UILabel *mspFormLabel;
@property (retain, nonatomic) IBOutlet UILabel *period1Label;
@property (retain, nonatomic) IBOutlet UILabel *period2Label;
@property (retain, nonatomic) IBOutlet UILabel *period3Label;

- (IBAction)onClickMenuCommentButton:(id)sender;
- (IBAction)onClickMenuPhotoButton:(id)sender;

- (IBAction)onHomeButtonClicked:(id)sender;

- (IBAction)onBackButtonClicked:(id)sender;

-(id) initWithStoreID:(NSString*)storeID;


@end
