//
//  StoreReportPhotoCell.h
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 23/3/2016.
//  Copyright © 2016 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebImageView.h"

@interface StoreReportPhotoCell : UITableViewCell

@property (retain, nonatomic) IBOutlet WebImageView *imgView1;
@property (retain, nonatomic) IBOutlet WebImageView *imgView2;
@property (retain, nonatomic) IBOutlet WebImageView *imgView3;

@property (retain, nonatomic) IBOutlet UILabel *label1;
@property (retain, nonatomic) IBOutlet UILabel *label2;
@property (retain, nonatomic) IBOutlet UILabel *label3;

@property (retain, nonatomic) NSDictionary* customData;

-(void) reloadDisplay;


@end
