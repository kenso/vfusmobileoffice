//
//  StoreInfoViewController.m
//  VF Ordering App
//
//  Created by Developer 2 on 9/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>

#import "BusinessLogicHelper.h"
#import "StoreMapViewController.h"
#import "StoreInfoViewController.h"
#import "CommentFormViewController.h"
#import "ShareLocale.h"
#import "CommentListTableView.h"
#import "ImageLoader.h"
#import "ImageSaver.h"
#import "Constants.h"
#import "UIHelper.h"
#import "SurveyListViewController.h"
#import "SurveyFormViewController.h"
#import "SalesPerformanceViewController.h"
#import "GeneralUiTool.h"

@interface StoreInfoViewController ()

@end

@implementation StoreInfoViewController

- (id)initWithId:(NSString*)storeID
{
    self = [super init];
    if (self) {
        self.records = [NSMutableArray array];
        self.storeID = storeID;
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    self.contactTable.delegate = self;
    self.contactTable.dataSource = self;
    self.contactTable.backgroundColor = TABLE_BG_COLOR_GREEN;
    self.contactTable.backgroundView = nil;
    self.contactTable.separatorColor = TABLE_SEPARATOR_COLOR;
    self.contactRecords = [NSMutableArray array];
    
    UILabel* titleLabel = (UILabel*)[self.view viewWithTag:601];
    titleLabel.text = @"Store Performance Analysis"; //[NSString stringWithFormat:@"Store Perofrmance > %@", [self.data objectForKey:@"Name"] ];
    
    [self fillData];
    
    
    storeInfoView.backgroundColor = TABLE_BG_COLOR_BLUE;
    
    [self.commentHistoryView initView];
    self.commentHistoryView.parentVC = self;
    
    [self loadData];
    
    [self refreshLocale];

}

- (void)dealloc {
    [_storeNameLabel release];
    [_storeCodeLabel release];
    [_SAPSiteCodeLabel release];
    [_storeTypeLabel release];
    [_addressLabel release];
    [_emailLabel release];
    [_phoneLabel release];
    [_faxLabel release];
    [_mapButton release];
    [_storeID release];
    self.data = nil;
    self.records = nil;
    self.pendingImageRecords = nil;
//    self.picker = nil;
//    self.pickerViewSurvey = nil;
    
    [_contactTable release];
    [_addressChineseLabel release];
    [_dayOffLabel release];
    [storeInfoView release];
    [_storePerformanceButton release];
    [_commentHistoryView release];
    [_storeNameChiLabel release];
    [_brandCodeLabel release];
    [_systemCodeLabel release];
    [_retailAELabel release];
    [_sizeNetLabel release];
    [_countryLabel release];
    [_regionLabel release];
    [_profileLabel release];
    [_keyStoreLabel release];
    [_compStatusLabel release];
    [_dayOnLabel release];
    [_latitudeLabel release];
    [_longitudeLabel release];
    [_nameLbl release];
    [_nameChiLbl release];
    [_sapLbl release];
    [_posLbl release];
    [_brandLbl release];
    [_systemLbl release];
    [_typeLbl release];
    [_addrLbl release];
    [_addrChiLbl release];
    [_retailLbl release];
    [_sizeLbl release];
    [_emailLbl release];
    [_phoneLbl release];
    [_faxLbl release];
    [_countryLbl release];
    [_regionLbl release];
    [_profileLbl release];
    [_keyStoreLbl release];
    [_compStatuLbl release];
    [_latLbl release];
    [_longLbl release];
    [_openDateLbl release];
    [_closDateLbl release];
    [_statusBtn release];
    [_phoneLbl release];
    [_mobileBtn release];
    [_emailBtn release];
    [_titleBtn release];
    [_departBtn release];
    [_commentBtn release];
    [_headerTitleLabel release];
    [_auditResultBtn release];
    [_infoTitleLabel release];
    [_phoneInfoLabel release];
    [super dealloc];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) reloadContactTable{
    if([NSThread isMainThread]){
        [self.contactTable reloadData];
    }else{
        [self performSelectorOnMainThread:@selector(reloadContactTable) withObject:nil waitUntilDone:NO];
    }
    
}



-(void) refreshLocale {
    
    [self.infoTitleLabel setTitle:[ShareLocale textFromKey:@"store_basic_info"] forState:UIControlStateNormal];
    self.typeLbl.text = [ShareLocale textFromKey:@"Store Type:"];
    self.nameLbl.text = [ShareLocale textFromKey:@"Store Name(English):"];
    self.nameChiLbl.text = [ShareLocale textFromKey:@"Store Name(Chinese):"];
    self.brandLbl.text = [ShareLocale textFromKey:@"Brand Code:"];
    self.systemLbl.text = [ShareLocale textFromKey:@"System Code:"];
    self.sapLbl.text = [ShareLocale textFromKey:@"SAP Code:"];
    self.posLbl.text = [ShareLocale textFromKey:@"POS Code:"];
    self.addrLbl.text = [ShareLocale textFromKey:@"Address(English):"];
    self.addrChiLbl.text = [ShareLocale textFromKey:@"Address(Chinese):"];
    self.retailLbl.text = [ShareLocale textFromKey:@"Retail AE:"];
    self.sizeLbl.text = [ShareLocale textFromKey:@"Size Net:"];
    self.emailLbl.text = [ShareLocale textFromKey:@"Email:"];
    self.phoneLbl.text = [ShareLocale textFromKey:@"Phone:"];
    self.faxLbl.text = [ShareLocale textFromKey:@"Fax:"];
    self.countryLbl.text = [ShareLocale textFromKey:@"Country:"];
    self.regionLbl.text = [ShareLocale textFromKey:@"Region:"];
    self.profileLbl.text = [ShareLocale textFromKey:@"Profile:"];
    self.keyStoreLbl.text = [ShareLocale textFromKey:@"Key Store:"];
    self.compStatuLbl.text = [ShareLocale textFromKey:@"Comp Status:"];
    self.latLbl.text = [ShareLocale textFromKey:@"Latitude:"];
    self.longLbl.text = [ShareLocale textFromKey:@"Longitude:"];
    self.openDateLbl.text = [ShareLocale textFromKey:@"Opening Date:"];
    self.closDateLbl.text = [ShareLocale textFromKey:@"Closure Date:"];
    
    self.phoneInfoLabel.text = [ShareLocale textFromKey:@"Phone:"];
    self.profileLbl.text = [ShareLocale textFromKey:@"Profile:"];
    self.keyStoreLbl.text = [ShareLocale textFromKey:@"KeyStore:"];
    
    
    [self.statusBtn setTitle:[ShareLocale textFromKey:@"Staff"] forState:UIControlStateNormal];
    [self.phoneBtn setTitle:[ShareLocale textFromKey:@"Phone"] forState:UIControlStateNormal];
    [self.mobileBtn setTitle:[ShareLocale textFromKey:@"Mobile"] forState:UIControlStateNormal];
    [self.emailBtn setTitle:[ShareLocale textFromKey:@"Email"] forState:UIControlStateNormal];
    [self.titleBtn setTitle:[ShareLocale textFromKey:@"Title"] forState:UIControlStateNormal];
    [self.departBtn setTitle:[ShareLocale textFromKey:@"Department"] forState:UIControlStateNormal];
    [self.commentBtn setTitle:[ShareLocale textFromKey:@"Comment"] forState:UIControlStateNormal];
    [self.auditResultBtn setTitle:[ShareLocale textFromKey:@"Audit Result"] forState:UIControlStateNormal];
    [self.storePerformanceButton setTitle:[ShareLocale textFromKey:@"Performance Analysis"] forState:UIControlStateNormal];
}


-(void) fillData{
    
    self.headerTitleLabel.text = [NSString stringWithFormat:[ShareLocale textFromKey:@"storeinfo_title"], [self.data objectForKey:@"Name"] ];

    //column 1
    self.storeNameLabel.text = [self getFieldDisplayWithFieldName:@"Name" withDictionary:self.data];
    self.storeNameChiLabel.text = [self getFieldDisplayWithFieldName:@"Store_Name_CN__c" withDictionary:self.data];
    self.storeCodeLabel.text = [self getFieldDisplayWithFieldName:@"POS_Store_Code__c" withDictionary:self.data];
    self.SAPSiteCodeLabel.text = [self getFieldDisplayWithFieldName:@"Store_Code__c" withDictionary:self.data];
    self.brandCodeLabel.text = [self getFieldDisplayWithFieldName:@"Brand_Code__c" withDictionary:self.data];
    self.systemCodeLabel.text = [self getFieldDisplayWithFieldName:@"System_Code__c" withDictionary:self.data];
    self.storeTypeLabel.text = [self getFieldDisplayWithFieldName:@"Store_Type__c" withDictionary:self.data];
    self.addressLabel.text = [self getFieldDisplayWithFieldName:@"emfa__Address__c" withDictionary:self.data];
    self.addressChineseLabel.text = [self getFieldDisplayWithFieldName:@"Address_CN__c" withDictionary:self.data];

    //column 2
    NSString* retailID = [self getFieldDisplayWithFieldName:@"Retail_AE_ID__c" withDictionary:self.data];
    NSString* retailName = [self getFieldDisplayWithFieldName:@"Retail_AE_Name__c" withDictionary:self.data];
    self.retailAELabel.text = [NSString stringWithFormat:@"%@ (%@)", retailName, retailID];
    self.sizeNetLabel.text = [NSString stringWithFormat:@"%.2f", [[self getFieldDisplayWithFieldName:@"Size_Net__c" withDictionary:self.data] floatValue] ];
    self.emailLabel.text = [self getFieldDisplayWithFieldName:@"emfa__Email__c" withDictionary:self.data];
    self.phoneLabel.text = [self getFieldDisplayWithFieldName:@"emfa__Phone__c" withDictionary:self.data];
    self.faxLabel.text = [self getFieldDisplayWithFieldName:@"emfa__Fax__c" withDictionary:self.data];
    self.countryLabel.text = [self getFieldDisplayWithFieldName:@"Country__c" withDictionary:self.data];
    self.regionLabel.text = [self getFieldDisplayWithFieldName:@"Region__c" withDictionary:self.data];
    
    self.profileLabel.text = [self getFieldDisplayWithFieldName:@"Profile__c" withDictionary:self.data];
    self.keyStoreLabel.text = [self getFieldDisplayWithFieldName:@"Key_Store__c" withDictionary:self.data];
    self.compStatusLabel.text = [self getFieldDisplayWithFieldName:@"Comp_Status__c" withDictionary:self.data];
    self.latitudeLabel.text = [self getFieldDisplayWithFieldName:@"emfa__Primary_Latitude__c" withDictionary:self.data];
    self.longitudeLabel.text = [self getFieldDisplayWithFieldName:@"emfa__Primary_Longitude__c" withDictionary:self.data];

    self.dayOnLabel.text = [self getFieldDisplayWithFieldName:@"Opening_Date__c" withDictionary:self.data];
    self.dayOffLabel.text = [self getFieldDisplayWithFieldName:@"Closure_Date__c" withDictionary:self.data];
    

    

    
    
    if ( [self.data objectForKey:@"Contacts__r"] != nil && [self.data objectForKey:@"Contacts__r"] != (id)[NSNull null]){
        self.contactRecords = [[self.data objectForKey:@"Contacts__r"] objectForKey:@"records"];
    }
    [self.contactTable reloadData];
}




#pragma mark - Image Network handling
-(void) loadData{
    [self showLoadingPopup:self.view];
    self.storeInfoRequest = [[VFDAO sharedInstance] selectStoreWithDelegate:self storeId: self.storeID];
}


-(void) loadComments{
    self.commentRequest = [[VFDAO sharedInstance] selectCommentsWithDelegate:self withStoreID:self.storeID];
}


-(void)daoRequest:(DAORequest *)request queryOnlineDataSuccess:(NSArray *)records{
    
    if(self.storeInfoRequest == request){
        if(records!=nil && [records count]>0){
            self.data = [records objectAtIndex:0];
            if ( [self.data objectForKey:@"Contacts__r"] != nil && [self.data objectForKey:@"Contacts__r"] != (id)[NSNull null]){
                self.contactRecords = [[self.data objectForKey:@"Contacts__r"] objectForKey:@"records"];
            }
            [self reloadContactTable];
            
            [self loadComments];
        }
        else{
            [self hideLoadingPopupWithFadeout];
            [self popupErrorDialogWithMessage: [NSString stringWithFormat:@"Error. No record found."]];
        }
    }
    else if (self.commentRequest == request){
        if(records!=nil && [records count]>0){
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [self.commentHistoryView setArrData: records];
                [self.commentHistoryView refreshTableView];
            });
        }else{
            
        }
        [self fillData];
        [self hideLoadingPopupWithFadeout];

    }
    
    

}

-(void) daoRequest:(DAORequest *)request queryOnlineDataError:(NSString *)response code:(int)code{
    [self hideLoadingPopupWithFadeout];

    [super handleQueryDataErrorCode:code description:response];
}





#pragma mark - UI Button handler
- (IBAction)onHomeButtonClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction) onBackButtonClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)onAddTaskButtonClicked:(id)sender {
    CommentFormViewController *vc = [[CommentFormViewController alloc] initWithData:@{
        @"storeId": [self getFieldDisplayWithFieldName:@"Id" withDictionary:self.data],
        @"storeName": [NSString stringWithFormat:@"%@ %@", self.data[@"Store_Code__c"], self.data[@"Name"]]
        //[self getFieldDisplayWithFieldName:@"Name" withDictionary:self.data]
    }];
    
    [self.navigationController pushViewController:vc animated:YES];
    [vc release];
}
- (IBAction)onMapButtonClicked:(id)sender {
    
    NSMutableArray* storeDataAry = [[[NSMutableArray alloc] init] autorelease];
    [storeDataAry addObject: self.data];
/*
    if ( [self.data objectForKey:@"Primary_Latitude__c"] == nil ||
        [self.data objectForKey:@"Primary_Latitude__c"] == (id)[NSNull null] ){
        [self popupErrorDialogWithMessage:@"Map data is not valid"];
    }else{
*/
        [self.navigationController pushViewController:[[StoreMapViewController alloc] initWithStoreDataArray:storeDataAry mode: nil] animated:YES];
//    }
}

- (IBAction)onStorePerformanceClicked:(id)sender {

    VFDAO* vf = [VFDAO sharedInstance];
    NSMutableDictionary* cache = vf.cache;
    NSDictionary* storeDataMap = [cache objectForKey: @"store_daily_analyse"];
    NSDictionary* selectedStoreInfo = [storeDataMap objectForKey:[self.data objectForKey:@"Store_Code__c"]];
    
    [self.navigationController pushViewController: [[SalesPerformanceViewController alloc] initWithStoreID:[selectedStoreInfo objectForKey:@"store_id"] name:[selectedStoreInfo objectForKey:@"name"] code:[self.data objectForKey:@"Store_Code__c"]] animated:YES] ;
    
    
}

- (IBAction)onSurveyButtonClicked:(id)sender {
    
    //[self.pickerViewSurvey show];
    
    int index = ((UIView*)sender).tag;
    
    if(index == 13){
        //SOP
        SurveyListViewController *vc = [[SurveyListViewController alloc] initWithType:SURVEY_TYPE_SOP_Audit title: [ShareLocale textFromKey:@"surveytitle_audit"] mode:SurveyListModeSurveyHistory];
        [vc setCurrStore: [self.data objectForKey:@"Id"]];
        [vc setCurrStoreName: [self.data objectForKey:@"Name"]];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if(index==12){
        //MSP
        SurveyListViewController *vc = [[SurveyListViewController alloc] initWithType:SURVEY_TYPE_MSP_SURVEY title: [ShareLocale textFromKey:@"surveytitle_msp"] mode:SurveyListModeSurveyHistory];
        [vc setCurrStore: [self.data objectForKey:@"Id"]];
        [vc setCurrStoreName: [self.data objectForKey:@"Name"]];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else{
        //Visit
        SurveyListViewController *vc = [[SurveyListViewController alloc] initWithType:SURVEY_TYPE_VISIT_FORM title: [ShareLocale textFromKey:@"surveytitle_visit" ] mode:SurveyListModeSurveyHistory];
        [vc setCurrStore: [self.data objectForKey:@"Id"]];
        [vc setCurrStoreName: [self.data objectForKey:@"Name"]];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - table data source and delegate

-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == self.contactTable){
        
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView == self.contactTable){
        NSLog(@"contactTable: Row: %i ", [self.contactRecords count]);
        return [self.contactRecords count];
    }else{
        return 0;
    }
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(tableView == self.contactTable){
        return [self getContactTableViewCell:indexPath.row];
    }
    return nil;
}


-(UITableViewCell*)getContactTableViewCell:(int)index{
    UITableViewCell *cell = [self.contactTable dequeueReusableCellWithIdentifier:@"ContactCell"];
    if(cell == nil){
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ContactCell"] autorelease];
        //cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.hidden = YES;
        cell.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        UIView *selectionColor = [[[UIView alloc] init] autorelease];
        selectionColor.backgroundColor = TABLE_ROW_HIGHLIGHT_COLOR;
        cell.selectedBackgroundView = selectionColor;
        
        [self createNewTextLabelWithDefaultStyle: CGRectMake(20, 0, 200, 40) parent:cell tag: 901];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(209, 0, 113, 40) parent:cell tag: 902];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(322, 0, 113, 40) parent:cell tag: 903];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(435, 0, 174, 40) parent:cell tag: 904];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(609, 0, 221, 40) parent:cell tag: 905];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(830, 0, 184, 40) parent:cell tag: 906];
//s        [self createNewButton: CGRectMake(900, 0, 80, 40) parent:cell tag: 907 color:4];
    }
    NSDictionary* data = [self.contactRecords objectAtIndex: index];
    UITextView* txtView1 = (UITextView*)[cell viewWithTag:901];
    txtView1.text = [self getFieldDisplayWithFieldName:@"Name" withDictionary: data];
    UITextView* txtView2 = (UITextView*)[cell viewWithTag:902];
    txtView2.text = [self getFieldDisplayWithFieldName:@"Phone" withDictionary: data];
    UITextView* txtView3 = (UITextView*)[cell viewWithTag:903];
    txtView3.text = [self getFieldDisplayWithFieldName:@"MobilePhone" withDictionary: data];
    UITextView* txtView4 = (UITextView*)[cell viewWithTag:904];
    txtView4.text = [self getFieldDisplayWithFieldName:@"Email" withDictionary: data];
    UITextView* txtView5 = (UITextView*)[cell viewWithTag:905];
    txtView5.text = [self getFieldDisplayWithFieldName:@"Title" withDictionary: data];
    UITextView* txtView6 = (UITextView*)[cell viewWithTag:906];
    txtView6.text = [self getFieldDisplayWithFieldName:@"Department" withDictionary: data];
    
/*    UIButton* button = (UIButton*)[cell viewWithTag:907];
    [button setTitle:@"New Task" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(onTaskButtonClicked:) forControlEvents:UIControlEventTouchUpInside];*/
    
    return cell;
}





#pragma mark - Image Network handling

@end



