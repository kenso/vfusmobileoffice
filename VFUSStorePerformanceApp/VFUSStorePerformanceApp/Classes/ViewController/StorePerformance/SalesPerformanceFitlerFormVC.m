//
//  SalesPerformanceFitlerFormVC.m
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 3/12/2015.
//  Copyright © 2015 vf.com. All rights reserved.
//

#import "SalesPerformanceFitlerFormVC.h"
#import "Constants.h"
#import "GeneralUiTool.h"
#import "ShareLocale.h"

@interface SalesPerformanceFitlerFormVC ()

@end

@implementation SalesPerformanceFitlerFormVC

-(id) initWithKey:(NSString*)key winTag:(NSInteger)wtag selectedOptions:(NSArray*)selectedOptions availableOptions:(NSArray*)availOptions{
    if (self = [super init]){
        self.originalKey = key;
        self.winTag = wtag;
        self.selectedOptions = (selectedOptions==nil)?[NSMutableArray array]:[selectedOptions mutableCopy];
        self.availableOptions = (availOptions==nil)?[NSMutableArray array]:[availOptions mutableCopy];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.backgroundView = nil;
    self.tableView.backgroundColor = [UIColor colorWithWhite:1 alpha:0.1f];
    [self.tableView reloadData];
    
    self.filterNameLabel.text = [ShareLocale textFromKey:@"custom_filter_name"];
    self.filterOptionLabel.text = [ShareLocale textFromKey:@"custom_filter_options"];
    self.nameTextField.text = [self.originalKey substringFromIndex:21];
    self.titleLabel.text = [ShareLocale textFromKey:@"store_performance_filter_title"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)dealloc {
    [_tableView release];
    [_nameTextField release];
    [_saveButton release];
    [_deleteButton release];
    [_filterNameLabel release];
    [_filterOptionLabel release];
    [_titleLabel release];
    [super dealloc];
}

- (IBAction)onClickSaveButton:(id)sender {
    //Add validation
    NSString* newKey = self.nameTextField.text;
    NSUserDefaults* user = [NSUserDefaults standardUserDefaults];
    if(self.originalKey!=nil){
        [user removeObjectForKey:self.originalKey];
    }
    
    NSUserDefaults *defaultPref = [NSUserDefaults standardUserDefaults];
    NSString* preferBrandCode = [defaultPref objectForKey: USER_PREFER_ACCOUNT_CODE];
    NSString* preferCountryCode = [defaultPref objectForKey: USER_PREFER_COUNTRY_CODE];

    
    NSString* newFilterKey = [NSString stringWithFormat:@"%@_%@_%@_%@",preferBrandCode, preferCountryCode, LOCAL_FILTER_NAME, newKey];

    NSString* globalFilterKey = [NSString stringWithFormat:@"%@_%@_%@s_%d",preferBrandCode, preferCountryCode, LOCAL_FILTER_NAME, self.winTag];
    NSMutableArray* filtersGroup = [user objectForKey:globalFilterKey];
    if(filtersGroup==nil){
        filtersGroup = [NSMutableArray array];
    }else{
        filtersGroup = [filtersGroup mutableCopy];
    }
    if(self.originalKey!=nil && [filtersGroup containsObject:self.originalKey]){
        [filtersGroup removeObject:self.originalKey];
    }
    if(![filtersGroup containsObject:newFilterKey]){
        [filtersGroup addObject:newFilterKey];
    }
    
    [user setObject:filtersGroup forKey: globalFilterKey ];
    [user setObject:self.selectedOptions forKey: newFilterKey ];
    [user synchronize];
    
    __block SalesPerformanceFitlerFormVC* me = self;
    [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"Save completed" message:@"" yesHanlder:^(id data) {
        [me.navigationController popViewControllerAnimated:YES];
    }];
}

- (IBAction)onClickDeleteButton:(id)sender {
    NSUserDefaults* user = [NSUserDefaults standardUserDefaults];
    if(self.originalKey!=nil){
        [user removeObjectForKey:self.originalKey];
    }
    NSUserDefaults *defaultPref = [NSUserDefaults standardUserDefaults];
    NSString* preferBrandCode = [defaultPref objectForKey: USER_PREFER_ACCOUNT_CODE];
    NSString* preferCountryCode = [defaultPref objectForKey: USER_PREFER_COUNTRY_CODE];
    
    NSString* globalFilterKey = [NSString stringWithFormat:@"%@_%@_%@s_%d", preferBrandCode, preferCountryCode, LOCAL_FILTER_NAME, self.winTag];
    NSMutableArray* filtersGroup = [user objectForKey:globalFilterKey];
    if(filtersGroup==nil){
        filtersGroup = [NSMutableArray array];
    }else{
        filtersGroup = [filtersGroup mutableCopy];
    }
    if(self.originalKey!=nil && [filtersGroup containsObject:self.originalKey]){
        [filtersGroup removeObject:self.originalKey];
    }
    
    [user setObject:filtersGroup forKey: globalFilterKey ];
    [user synchronize];
    
    __block SalesPerformanceFitlerFormVC* me = self;
    [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"Delete completed" message:@"" yesHanlder:^(id data) {
        [me.navigationController popViewControllerAnimated:YES];
    }];
}

#pragma mark - TableView Delegate
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.availableOptions count];
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary* availOption = self.availableOptions[indexPath.row];
    
    UITableViewCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"FilterOptionCell"];
    if(cell == nil){
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FilterOptionCell"] autorelease];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(0+10, 0, 450, 50) parent:cell tag: 901];
        UIButton* button = [self createNewButton:CGRectMake(900, 5, 40, 40) parent:cell tag:902 color:-1];
        [button addTarget:self action:@selector(onSelectCheckbox:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    cell.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
    UIView *selectionColor = [[[UIView alloc] init] autorelease];
    selectionColor.backgroundColor = TABLE_ROW_HIGHLIGHT_COLOR;
    cell.selectedBackgroundView = selectionColor;
    
    BOOL found = NO;
    for(NSString* optionKey in self.selectedOptions){
        if(self.winTag==2){
            if([optionKey isEqualToString:availOption[@"grade"]]){
                found = YES;
            }
        }else{
            NSArray* parts = [optionKey componentsSeparatedByString:@"--"];
            if ([availOption[@"gender"] isEqualToString:parts[0]] && [availOption[@"cat"] isEqualToString:parts[1]] ){
                found = YES;
            }
        }
    }
    
    UILabel* label = [cell viewWithTag:901];
    UIButton* checkbox = [cell viewWithTag:902];
    label.text = availOption[@"Label"];
    
    if(found){
        [checkbox setImage:[UIImage imageNamed:@"checkbox_on"] forState:UIControlStateNormal];
    }else{
        [checkbox setImage:[UIImage imageNamed:@"checkbox_off"] forState:UIControlStateNormal];
    }
    
    return cell;
}

#pragma mark - UI delegate
-(void) onSelectCheckbox:(id)sender{
    UIButton* button = sender;
    UIView* superView = [button superview];
    while( ![superView isKindOfClass:[UITableViewCell class]] ){
        superView = [superView superview];
        if(superView==nil) break;
    }
    UITableViewCell* cell = (UITableViewCell*)superView;
    NSInteger index = [self.tableView indexPathForCell:cell].row;
    
    NSMutableDictionary* availOption = [self.availableOptions[index] mutableCopy];

    NSString* val;

    if(self.winTag == 2){
        val = availOption[@"grade"];
    }else if(self.winTag == 4){
        val = [NSString stringWithFormat:@"%@--%@", availOption[@"gender"], availOption[@"cat"]];
    }else{
        return;
    }
    
    if([self.selectedOptions containsObject: val]){
        [self.selectedOptions removeObject: val];
        [button setImage:[UIImage imageNamed:@"checkbox_off"] forState:UIControlStateNormal];
    }else{
        [self.selectedOptions addObject: val];
        [button setImage:[UIImage imageNamed:@"checkbox_on"] forState:UIControlStateNormal];
    }
    
    
    
}

- (IBAction)onHomeButtonClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction) onBackButtonClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
