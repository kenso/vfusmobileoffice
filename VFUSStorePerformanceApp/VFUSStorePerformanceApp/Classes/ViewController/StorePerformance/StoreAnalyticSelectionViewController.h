//
//  StoreAnalyticSelectionViewController.h
//  VFStorePerformanceApp_Billy
//
//  Created by Developer 2 on 10/4/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol StoreAnalyicSelectionDelegate <NSObject>

-(void) onSelectedMode:(NSDictionary*)data;

@end



@interface StoreAnalyticSelectionViewController : UITableViewController


@property (nonatomic, retain) NSArray* modeArray;

@property (nonatomic, assign) id<StoreAnalyicSelectionDelegate> delegate;

@end
