//
//  UserSelectionViewController.m
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 14/8/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "UserSelectionViewController.h"
#import "ShareLocale.h"

@interface UserSelectionViewController ()

@end

@implementation UserSelectionViewController

- (id)initWithUserArray:(NSArray*)userArray
{
    self = [super initWithNibName:@"UserSelectionViewController" bundle:nil];
    if (self) {
        NSMutableArray* tempArray = [NSMutableArray arrayWithArray:userArray];
        [tempArray insertObject:@{@"Label":[ShareLocale textFromKey:@"all"], @"Value":@""} atIndex:0];
        self.userArray = [NSArray arrayWithArray:tempArray];
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.tableView reloadData];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}

- (void)dealloc {
    [_tableView release];
    [super dealloc];
}




#pragma mark - TableView delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.userArray count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary* data = [self.userArray objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserCell"];
    
    if(cell == nil){
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"UserCell"] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.textLabel.hidden = YES;
        
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(20, 5, 300, 40)];
        label.tag = 901;
        label.text = @"";
        [cell addSubview:label];
        [label release];
    }
    
    UILabel* nameLabel = (UILabel*)[cell viewWithTag:901];
    NSString* name = [data objectForKey:@"Label"];
    nameLabel.text = (name==nil || name==(id)[NSNull null])? @"-": name ;
    
    return cell;
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary* data = [self.userArray objectAtIndex:indexPath.row];
    NSLog(@"Clicked on %@", data);
    if(self.delegate!=nil){
        [self.delegate onSelectedUser:data];
    }
}


@end
