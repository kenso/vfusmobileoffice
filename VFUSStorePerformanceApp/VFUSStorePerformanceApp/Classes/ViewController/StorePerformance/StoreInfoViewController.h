//
//  StoreInfoViewController.h
//  VF Ordering App
//
//  Created by Developer 2 on 9/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import "BasicViewController.h"
//#import "ImageLoader.h"
//#import "LPTSelectionView.h"
#import "CommentListTableView.h"
//#import "ImageSaver.h"
#import "VFDAO.h"


@interface StoreInfoViewController : BasicViewController< UINavigationControllerDelegate, DAODelegate, UITableViewDataSource, UITableViewDelegate>{
    int selectedPhotoBoxIndex;
    IBOutlet UIView *storeInfoView;
}

@property (retain, nonatomic) NSString* storeID;//passing from init
@property (retain, nonatomic) NSDictionary* data;
@property (retain, nonatomic) NSMutableArray* records;
@property (retain) NSMutableArray* contactRecords;
@property (retain, nonatomic) NSMutableArray* pendingImageRecords;

@property (retain, nonatomic) IBOutlet UIButton *infoTitleLabel;

@property (retain, nonatomic) DAOBaseRequest* storeInfoRequest;
@property (retain, nonatomic) DAOBaseRequest* commentRequest;

@property (retain, nonatomic) IBOutlet UIButton *mapButton;
@property (retain, nonatomic) IBOutlet UIButton *storePerformanceButton;
@property (retain, nonatomic) IBOutlet UITableView *contactTable;
@property (retain, nonatomic) IBOutlet CommentListTableView *commentHistoryView;
@property (retain, nonatomic) IBOutlet UILabel *phoneInfoLabel;




@property (retain, nonatomic) IBOutlet UILabel *longitudeLabel;
@property (retain, nonatomic) IBOutlet UILabel *latitudeLabel;
@property (retain, nonatomic) IBOutlet UILabel *dayOnLabel;
@property (retain, nonatomic) IBOutlet UILabel *compStatusLabel;
@property (retain, nonatomic) IBOutlet UILabel *profileLabel;
@property (retain, nonatomic) IBOutlet UILabel *keyStoreLabel;
@property (retain, nonatomic) IBOutlet UILabel *regionLabel;
@property (retain, nonatomic) IBOutlet UILabel *countryLabel;
@property (retain, nonatomic) IBOutlet UILabel *sizeNetLabel;
@property (retain, nonatomic) IBOutlet UILabel *retailAELabel;
@property (retain, nonatomic) IBOutlet UILabel *systemCodeLabel;
@property (retain, nonatomic) IBOutlet UILabel *brandCodeLabel;
@property (retain, nonatomic) IBOutlet UILabel *storeNameChiLabel;
@property (retain, nonatomic) IBOutlet UILabel *storeNameLabel;
@property (retain, nonatomic) IBOutlet UILabel *storeCodeLabel;//SAP
@property (retain, nonatomic) IBOutlet UILabel *SAPSiteCodeLabel;//POS
@property (retain, nonatomic) IBOutlet UILabel *storeTypeLabel;
@property (retain, nonatomic) IBOutlet UILabel *dayOffLabel;
@property (retain, nonatomic) IBOutlet UILabel *addressLabel;
@property (retain, nonatomic) IBOutlet UILabel *addressChineseLabel;
@property (retain, nonatomic) IBOutlet UILabel *emailLabel;
@property (retain, nonatomic) IBOutlet UILabel *phoneLabel;
@property (retain, nonatomic) IBOutlet UILabel *faxLabel;

//Label for translation
@property (retain, nonatomic) IBOutlet UILabel *addrChiLbl;
@property (retain, nonatomic) IBOutlet UILabel *typeLbl;
@property (retain, nonatomic) IBOutlet UILabel *addrLbl;
@property (retain, nonatomic) IBOutlet UILabel *nameLbl;
@property (retain, nonatomic) IBOutlet UILabel *nameChiLbl;
@property (retain, nonatomic) IBOutlet UILabel *brandLbl;
@property (retain, nonatomic) IBOutlet UILabel *systemLbl;
@property (retain, nonatomic) IBOutlet UILabel *sapLbl;
@property (retain, nonatomic) IBOutlet UILabel *posLbl;
@property (retain, nonatomic) IBOutlet UILabel *retailLbl;
@property (retain, nonatomic) IBOutlet UILabel *sizeLbl;
@property (retain, nonatomic) IBOutlet UILabel *emailLbl;
@property (retain, nonatomic) IBOutlet UILabel *phoneLbl;
@property (retain, nonatomic) IBOutlet UILabel *faxLbl;
@property (retain, nonatomic) IBOutlet UILabel *countryLbl;
@property (retain, nonatomic) IBOutlet UILabel *regionLbl;
@property (retain, nonatomic) IBOutlet UILabel *profileLbl;
@property (retain, nonatomic) IBOutlet UILabel *keyStoreLbl;
@property (retain, nonatomic) IBOutlet UILabel *compStatuLbl;
@property (retain, nonatomic) IBOutlet UILabel *latLbl;
@property (retain, nonatomic) IBOutlet UILabel *longLbl;
@property (retain, nonatomic) IBOutlet UILabel *openDateLbl;
@property (retain, nonatomic) IBOutlet UILabel *closDateLbl;
@property (retain, nonatomic) IBOutlet UIButton *statusBtn;
@property (retain, nonatomic) IBOutlet UIButton *phoneBtn;
@property (retain, nonatomic) IBOutlet UIButton *mobileBtn;
@property (retain, nonatomic) IBOutlet UIButton *emailBtn;
@property (retain, nonatomic) IBOutlet UIButton *titleBtn;
@property (retain, nonatomic) IBOutlet UIButton *departBtn;
@property (retain, nonatomic) IBOutlet UIButton *commentBtn;
@property (retain, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (retain, nonatomic) IBOutlet UIButton *auditResultBtn;









- (IBAction)onHomeButtonClicked:(id)sender;
-(IBAction)onBackButtonClicked:(id)sender;
- (IBAction)onMapButtonClicked:(id)sender;
- (IBAction)onStorePerformanceClicked:(id)sender;
- (IBAction)onSurveyButtonClicked:(id)sender;
- (IBAction)onAddTaskButtonClicked:(id)sender;

- (id)initWithId:(NSString*)storeID;




@end
