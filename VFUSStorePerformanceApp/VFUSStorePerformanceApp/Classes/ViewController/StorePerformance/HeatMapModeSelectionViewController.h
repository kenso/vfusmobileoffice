//
//  HeatMapModeSelectionViewController.h
//  VFStorePerformanceApp_Billy
//
//  Created by Developer 2 on 2/4/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HeatMapModeSelectionViewControllerDelegate <NSObject>

-(void) onSelectdMode:(NSDictionary*) modeData;

@end


@interface HeatMapModeSelectionViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>{
    int selectedPeriodIndex;
}


@property (retain, nonatomic) NSArray* recordArray;
@property (retain, nonatomic) NSArray* subRecordArray;

@property (retain, nonatomic) IBOutlet UITableView *tableView;
@property (retain, nonatomic) IBOutlet UITableView *detailTableView;

@property (assign, nonatomic) id<HeatMapModeSelectionViewControllerDelegate> delegate;

@end