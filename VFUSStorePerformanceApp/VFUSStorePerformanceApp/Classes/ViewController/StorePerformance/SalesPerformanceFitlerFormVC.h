//
//  SalesPerformanceFitlerFormVC.h
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 3/12/2015.
//  Copyright © 2015 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"

@interface SalesPerformanceFitlerFormVC : BasicViewController<UITableViewDataSource, UITableViewDelegate>


-(id) initWithKey:(NSString*)key winTag:(NSInteger)wtag selectedOptions:(NSArray*)selectedOptions availableOptions:(NSArray*)availOptions;

@property (retain, nonatomic) IBOutlet UILabel *titleLabel;
@property (assign) NSInteger winTag;
@property (retain) NSString* originalKey;
@property (retain) NSMutableArray* selectedOptions;
@property (retain) NSMutableArray* availableOptions;

@property (retain, nonatomic) IBOutlet UITableView *tableView;
@property (retain, nonatomic) IBOutlet UITextField *nameTextField;
@property (retain, nonatomic) IBOutlet UIButton *saveButton;
@property (retain, nonatomic) IBOutlet UIButton *deleteButton;

- (IBAction)onClickSaveButton:(id)sender;

- (IBAction)onClickDeleteButton:(id)sender;
- (IBAction)onHomeButtonClicked:(id)sender;
-(IBAction)onBackButtonClicked:(id)sender;
@property (retain, nonatomic) IBOutlet UILabel *filterNameLabel;
@property (retain, nonatomic) IBOutlet UILabel *filterOptionLabel;

@end
