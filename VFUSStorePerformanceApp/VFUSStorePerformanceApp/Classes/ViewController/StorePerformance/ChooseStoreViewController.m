//
//  ChooseStoreViewController.m
//
//  Created by Developer 2 on 4/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//
#import "ShareLocale.h"
#import "ChooseStoreViewController.h"
#import "OrderCreationManager.h"
#import "BusinessLogicHelper.h"
#import "StoreInfoViewController.h"
#import "GeneralUiTool.h"
#import "UIHelper.h"
#import "Constants.h"
#import "StoreAnalyticSelectionViewController.h"
#import "UserSelectionViewController.h"
#import "SalesPerformanceViewController.h"
#import "StoreMapViewController.h"
#import "InventoryViewController.h"
#import "GeneralSelectionPopup.h"


@interface ChooseStoreViewController ()

@end

@implementation ChooseStoreViewController


-(id)initWithMode:(int)mode{
    
    id instance = [super initWithNibName:@"ChooseStoreViewController" bundle:nil];

    _mode = mode;
    
    return instance;
}

-(id)init{
    id instance = [super initWithNibName:@"ChooseStoreViewController" bundle:nil];
    _mode = 0;
    return instance;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    NSLog(@"ChooseStoreViewController viewDidLoad() mode=%d", _mode);

    self.storeTableView.backgroundColor = TABLE_BG_COLOR_BLUE;
    self.storeTableView.backgroundView = nil;
    self.recordAry = [NSArray array];
    self.userArray = [NSArray array];
    self.storeTableView.delegate = self;
    self.storeTableView.dataSource = self;
    
    self.selectedStoreTypeValArray = [NSMutableArray array];
    self.selectedCompStatusValArray = [NSMutableArray array];
    
    self.summaryView.backgroundColor = TABLE_BG_COLOR_RED;
    
    [self switchViewMode: VIEWMODE_STORE];
    
    [self refreshMode];
    [self refreshLocale];
    
    [self initCompStatusArray];
    [self initStoreTypeArray];
    
//    [self refreshUserAndMapButton];
    
    [self initCommentSidebarWithStoreId: nil];
}

-(void) initCompStatusArray{
    NSMutableSet* compStatusSet = [NSMutableSet set];
    self.compStatusArray = [NSMutableArray array];
//    [self.compStatusArray addObject:@{ @"label":@"All", @"val":@"" }];
    for(NSDictionary* data in self.recordAry){
        id type = data[@"Comp_Status__c"];
        if(type!=(id)[NSNull null] && type!=nil && ![compStatusSet containsObject:type]){
            [compStatusSet addObject:type];
            [self.compStatusArray addObject:@{@"label":type, @"val":type}];
        }
    }
}

-(void) initStoreTypeArray{
    NSMutableSet* storeTypeSet = [NSMutableSet set];
    self.storeTypeArray = [NSMutableArray array];
//    [self.storeTypeArray addObject:@{ @"label":@"All", @"val":@"" }];
    for(NSDictionary* data in self.recordAry){
        id type = data[@"Store_Type__c"];
        if(type!=(id)[NSNull null] && type!=nil && ![storeTypeSet containsObject:type]){
            [storeTypeSet addObject:type];
            [self.storeTypeArray addObject:@{@"label":type, @"val":type}];
        }
    }
}


-(void) refreshMode{
    int offsetX = 170;

    self.defaultHeaderView.hidden = YES;
    self.txnHeaderView.hidden = YES;
    self.inventoryHeaderView.hidden = YES;
    
    bool needShowSummary = NO;

    
    if (_mode == MODE_INVENTORY){
        self.headerTitleLabel.text = [ShareLocale textFromKey:@"invpage_title"];
        self.inventoryHeaderView.hidden = NO;
        self.heatMapButton.hidden = YES;
        self.modeButton.hidden = YES;
        self.compStatusButton.hidden = YES;
        self.storeTypeButton.hidden = YES;
        //FIXME
        self.fxDateLabel.frame = CGRectMake(offsetX, self.fxDateLabel.frame .origin.y, self.fxDateLabel.frame.size.width, self.fxDateLabel.frame .size.height);
        self.summaryView.hidden = YES;
        CGRect r = self.storeTableView.frame;
        self.storeTableView.frame = CGRectMake(r.origin.x, r.origin.y, r.size.width, 625);
        self.userFilterButton.hidden = YES;
        self.groupButton.hidden = YES;
    }
    else{
        //show heatmap button
        self.heatMapButton.hidden = NO;

        VFDAO* app = [VFDAO sharedInstance];
        NSString* roleName = app.cache[@"user_role_name"];
        if([roleName rangeOfString:@"DS"].location!=NSNotFound){
            self.userFilterButton.hidden = YES;
            self.groupButton.hidden = YES;
            //FIXME
//            self.modeButton.frame = CGRectMake(offsetX, offsetY, self.modeButton.frame.size.width, self.modeButton.frame.size.height);
//            self.heatMapButton.frame = CGRectMake(offsetX + self.modeButton.frame.size.width+5, offsetY, self.heatMapButton.frame.size.width, self.heatMapButton.frame.size.height);
//            self.fxDateLabel.frame = CGRectMake(offsetX, self.fxDateLabel.frame .origin.y, self.fxDateLabel.frame.size.width, self.fxDateLabel.frame .size.height);
        }
        else{
            self.userFilterButton.hidden = NO;
            self.groupButton.hidden = NO;
//            self.fxDateLabel.frame = CGRectMake(593, self.fxDateLabel.frame .origin.y, self.fxDateLabel.frame.size.width, self.fxDateLabel.frame .size.height);
            if(self.viewMode==VIEWMODE_DS){
                self.userFilterButton.hidden = YES;
                [self.weeklyStoreButton setTitle:[ShareLocale textFromKey:@"user_name"] forState:UIControlStateNormal];
                [self.defaultStoreButton setTitle:[ShareLocale textFromKey:@"user_name"] forState:UIControlStateNormal];
            }
            else{
                self.userFilterButton.hidden = NO;
                [self.weeklyStoreButton setTitle:[ShareLocale textFromKey:@"weekly_header_store"] forState:UIControlStateNormal];
                [self.defaultStoreButton setTitle:[ShareLocale textFromKey:@"weekly_header_store"] forState:UIControlStateNormal];
            }

        }
        
        
        if(_mode == 0){//default 9 columns
            self.headerTitleLabel.text = [ShareLocale textFromKey:@"weekly_title"];
            self.defaultHeaderView.hidden = NO;
            [self.modeButton setTitle:[ShareLocale textFromKey:@"storelist_weekly_summary"] forState:UIControlStateNormal];
            needShowSummary = YES;
            
            
        }
        else{//not default, show WTD, MTD, YTD and WT, MT, YT
            self.headerTitleLabel.text = [ShareLocale textFromKey:@"weekly_title"];
            self.txnHeaderView.hidden = NO;
            if(_mode == CARD_TRANSACTION_COUNT) [self.modeButton setTitle:[ShareLocale textFromKey:@"s_transaction_count"] forState:UIControlStateNormal];
            else if (_mode == CARD_OPS_SALES) [self.modeButton setTitle:[ShareLocale textFromKey:@"ops_sales"] forState:UIControlStateNormal];
            else if (_mode == CARD_DISCOUNT) [self.modeButton setTitle:[ShareLocale textFromKey:@"discount"] forState:UIControlStateNormal];
            else if (_mode == CARD_SALES_PRODUCTIVITY) [self.modeButton setTitle:[ShareLocale textFromKey:@"store_prod"] forState:UIControlStateNormal];
            else if (_mode == CARD_ATV) [self.modeButton setTitle:[ShareLocale textFromKey:@"atv"] forState:UIControlStateNormal];
            else if (_mode == CARD_UPT) [self.modeButton setTitle:[ShareLocale textFromKey:@"upt"] forState:UIControlStateNormal];
            else if (_mode == CARD_VISITOR_COUNT) [self.modeButton setTitle:[ShareLocale textFromKey:@"visitor_count"] forState:UIControlStateNormal];
            else if (_mode == CARD_PEL_OFF) [self.modeButton setTitle:[ShareLocale textFromKey:@"peel_off"] forState:UIControlStateNormal];
            else if (_mode == CARD_CONVERSION_RATE) [self.modeButton setTitle:[ShareLocale textFromKey:@"conversion_rate"] forState:UIControlStateNormal];
            needShowSummary = YES;
        }
    }
    
    
    [self refreshTableData];
    
    if(needShowSummary){
        [self reloadSummaryView];
    }
    
}



-(void) refreshLocale{
    
    //Inventory
    [self.invAddrButton setTitle:[ShareLocale textFromKey:@"invpage_header__addr"] forState:UIControlStateNormal];
    [self.invCodeButton setTitle:[ShareLocale textFromKey:@"weekly_header_code"] forState:UIControlStateNormal];
    [self.invStoreButton setTitle:[ShareLocale textFromKey:@"weekly_header_store"] forState:UIControlStateNormal];
    //Weekly
    [self.weeklyCodeButton setTitle:[ShareLocale textFromKey:@"weekly_header_code"] forState:UIControlStateNormal];
    [self.weeklyStoreButton setTitle:[ShareLocale textFromKey:@"weekly_header_store"] forState:UIControlStateNormal];

    [self.defaultCodeButton setTitle:[ShareLocale textFromKey:@"weekly_header_code"] forState:UIControlStateNormal];
    [self.defaultStoreButton setTitle:[ShareLocale textFromKey:@"weekly_header_store"] forState:UIControlStateNormal];
    [self.defaultOpsSalesButton setTitle:[ShareLocale textFromKey:@"s_ops_sales"] forState:UIControlStateNormal];
    [self.defaultDiscountButton setTitle:[ShareLocale textFromKey:@"s_discount"] forState:UIControlStateNormal];
    [self.defaultSalesProdButton setTitle:[ShareLocale textFromKey:@"s_store_prod"] forState:UIControlStateNormal];
    [self.defaultATVButton setTitle:[ShareLocale textFromKey:@"s_atv"] forState:UIControlStateNormal];
    [self.defaultUPTButton setTitle:[ShareLocale textFromKey:@"s_upt"] forState:UIControlStateNormal];
    [self.defaultTxnButton setTitle:[ShareLocale textFromKey:@"s_transaction_count"] forState:UIControlStateNormal];
    [self.defaultVisitorButton setTitle:[ShareLocale textFromKey:@"s_visitor_count"] forState:UIControlStateNormal];
    [self.defaultPeelOffButton setTitle:[ShareLocale textFromKey:@"s_peel_off"] forState:UIControlStateNormal];
    [self.defaultConvButton setTitle:[ShareLocale textFromKey:@"s_conversion_rate"] forState:UIControlStateNormal];
    
    [self.heatMapButton setTitle:[ShareLocale textFromKey:@"heat_map"] forState:UIControlStateNormal];
    
    
    [self.userFilterButton setTitle:[ShareLocale textFromKey:@"user_name"] forState:UIControlStateNormal];

}



-(NSNumber*) addNumberValue:(NSNumber*)num key:(NSString*)key data:(NSDictionary*)data{
    id val = [data objectForKey:key];
    if(val==nil || val==(id)[NSNull null]){
        return num;
    }else{
        return [NSNumber numberWithFloat: [val floatValue] + [num floatValue]] ;
    }
}
//
//-(NSNumber*) divNumber:(NSNumber*)num by:(NSNumber*)num2{
//    if([num2 floatValue]==0){
//        return 0;
//    }
//    return [NSNumber numberWithFloat:([num floatValue] / [num2 floatValue]) ];
//    
//}


-(StoreData*)calculateSummaryDataForAEID:(NSString*)aeID storeType:(NSArray*)typeAry compStatus:(NSArray*)statusAry storeList:(NSArray*)storeList{
    
    StoreData* s = [[StoreData alloc] init];
    
    NSNumber* sumSize = 0;
    NSNumber* wtdGroassSales = 0;
    NSNumber* mtdGroassSales = 0;
    NSNumber* ytdGroassSales = 0;
    NSNumber* wtdUnit = 0;
    NSNumber* mtdUnit = 0;
    NSNumber* ytdUnit = 0;
    NSNumber* wtdOutside = 0;
    NSNumber* mtdOutside = 0;
    NSNumber* ytdOutside = 0;
    
    NSMutableSet* storeSet = [[NSMutableSet alloc] init];
    
    
    for(NSDictionary* storeDayInfo in storeList){
        
        NSString* sAEID = storeDayInfo[@"aeId"];
        if(sAEID==(id)[NSNull null]) sAEID = nil;
        NSString* sType = storeDayInfo[@"Store_Type__c"];
        if(sType==(id)[NSNull null]) sType = nil;
        NSString* sComp = storeDayInfo[@"Comp_Status__c"];
        if(sComp==(id)[NSNull null]) sComp = nil;
        
        if( ( aeID==nil || [aeID isEqualToString:sAEID])
           && ( [typeAry count]==0 ||  [typeAry containsObject:sType])
           && ( [statusAry count]==0 || [statusAry containsObject:sComp]) ){
            
            if(![storeSet containsObject: storeDayInfo[@"Store_Code__c"]]){
                [storeSet addObject: storeDayInfo[@"Store_Code__c"]];
                sumSize = [self addNumberValue:sumSize key:@"size" data: storeDayInfo];
            }
            s.opsSales.wtd = [self addNumberValue:s.opsSales.wtd key:@"Weekly_Ops_Sales__c" data:storeDayInfo];
            s.opsSales.mtd =[self addNumberValue:s.opsSales.mtd key:@"Monthly_Ops_Sales__c" data:storeDayInfo];
            s.opsSales.ytd =[self addNumberValue: s.opsSales.ytd key:@"Yearly_Ops_Sales__c" data:storeDayInfo];
            
            s.opsSales.lywtd = [self addNumberValue:s.opsSales.lywtd key:@"Weekly_Ops_Sales_LY" data:storeDayInfo];
            s.opsSales.lymtd =[self addNumberValue:s.opsSales.lymtd key:@"Monthly_Ops_Sales_LY" data:storeDayInfo];
            s.opsSales.lyytd =[self addNumberValue: s.opsSales.lyytd key:@"Yearly_Ops_Sales_LY" data:storeDayInfo];
            
            s.txn.wtd =[self addNumberValue: s.txn.wtd key:@"Weekly_Transaction_Count__c" data:storeDayInfo];
            s.txn.mtd =[self addNumberValue: s.txn.mtd key:@"Monthly_Transaction_Count__c" data:storeDayInfo];
            s.txn.ytd =[self addNumberValue: s.txn.ytd key:@"Yearly_Transaction_Count__c" data:storeDayInfo];

            s.txn.lywtd =[self addNumberValue: s.txn.lywtd key:@"Weekly_Transaction_Count_LY" data:storeDayInfo];
            s.txn.lymtd =[self addNumberValue: s.txn.lymtd key:@"Monthly_Transaction_Count_LY" data:storeDayInfo];
            s.txn.lyytd =[self addNumberValue: s.txn.lyytd key:@"Yearly_Transaction_Count_LY" data:storeDayInfo];

            s.ttxn.wtd =[self addNumberValue: s.ttxn.lywtd key:@"Weekly_Traffic_Transaction_Count__c" data:storeDayInfo];
            s.ttxn.mtd =[self addNumberValue: s.ttxn.lymtd key:@"Monthly_Traffic_Transaction_Count__c" data:storeDayInfo];
            s.ttxn.ytd =[self addNumberValue: s.ttxn.lyytd key:@"Yearly_Traffic_Transaction_Count__c" data:storeDayInfo];

            s.ttxn.lywtd =[self addNumberValue: s.ttxn.lywtd key:@"Weekly_Traffic_Transaction_Count_LY" data:storeDayInfo];
            s.ttxn.lymtd =[self addNumberValue: s.ttxn.lymtd key:@"Monthly_Traffic_Transaction_Count_LY" data:storeDayInfo];
            s.ttxn.lyytd =[self addNumberValue: s.ttxn.lyytd key:@"Yearly_Traffic_Transaction_Count_LY" data:storeDayInfo];

            s.visitor.wtd =[self addNumberValue: s.visitor.wtd key:@"Weekly_Visitor_Count__c" data:storeDayInfo];
            s.visitor.mtd =[self addNumberValue: s.visitor.mtd key:@"Monthly_Visitor_Count__c" data:storeDayInfo];
            s.visitor.ytd =[self addNumberValue: s.visitor.ytd key:@"Yearly_Visitor_Count__c" data:storeDayInfo];

            s.visitor.lywtd =[self addNumberValue: s.visitor.lywtd key:@"Weekly_Visitor_Count_LY" data:storeDayInfo];
            s.visitor.lymtd =[self addNumberValue: s.visitor.lymtd key:@"Monthly_Visitor_Count_LY" data:storeDayInfo];
            s.visitor.lyytd =[self addNumberValue: s.visitor.lyytd key:@"Yearly_Visitor_Count_LY" data:storeDayInfo];

            s.tvisitor.wtd =[self addNumberValue: s.visitor.wtd key:@"Weekly_T_Visitor_Count__c" data:storeDayInfo];
            s.tvisitor.mtd =[self addNumberValue: s.visitor.mtd key:@"Monthly_T_Visitor_Count__c" data:storeDayInfo];
            s.tvisitor.ytd =[self addNumberValue: s.visitor.ytd key:@"Yearly_T_Visitor_Count__c" data:storeDayInfo];
            
            s.tvisitor.lywtd =[self addNumberValue: s.visitor.lywtd key:@"Weekly_T_Visitor_Count_LY" data:storeDayInfo];
            s.tvisitor.lymtd =[self addNumberValue: s.visitor.lymtd key:@"Monthly_T_Visitor_Count_LY" data:storeDayInfo];
            s.tvisitor.lyytd =[self addNumberValue: s.visitor.lyytd key:@"Yearly_T_Visitor_Count_LY" data:storeDayInfo];

            wtdGroassSales =[self addNumberValue:wtdGroassSales key:@"Weekly_Cross_Sales__c" data:storeDayInfo];
            mtdGroassSales =[self addNumberValue:mtdGroassSales key:@"Monthly_Cross_Sales__c" data:storeDayInfo];
            ytdGroassSales =[self addNumberValue:ytdGroassSales key:@"Yearly_Cross_Sales__c" data:storeDayInfo];
            
            wtdUnit =[self addNumberValue:wtdUnit key:@"Weekly_Unit_Count__c" data:storeDayInfo];
            mtdUnit =[self addNumberValue:mtdUnit key:@"Monthly_Unit_Count__c" data:storeDayInfo];
            ytdUnit =[self addNumberValue:ytdUnit key:@"Yearly_Unit_Count__c" data:storeDayInfo];
            
            wtdOutside =[self addNumberValue:wtdOutside key:@"Weekly_Outside_Traffic__c" data:storeDayInfo];
            
            mtdOutside =[self addNumberValue:mtdOutside key:@"Monthly_Outside_Traffic__c" data:storeDayInfo];
            ytdOutside =[self addNumberValue:ytdOutside key:@"Yearly_Outside_Traffic__c" data:storeDayInfo];
            
        }
    }
    
    //formula value
    //replace txn to ttxn for change request
    s.cr.wtd = ([s.tvisitor.wtd floatValue]!=0)?[NSNumber numberWithFloat:([s.ttxn.wtd floatValue] / [s.tvisitor.wtd floatValue]) ]: @0;
    s.cr.mtd = ([s.tvisitor.mtd floatValue]!=0)?[NSNumber numberWithFloat:([s.ttxn.mtd floatValue] / [s.tvisitor.mtd floatValue]) ]: @0;
    s.cr.ytd = ([s.tvisitor.ytd floatValue]!=0)?[NSNumber numberWithFloat:([s.ttxn.ytd floatValue] / [s.tvisitor.ytd floatValue]) ]: @0;
    
    s.discount.wtd = ([wtdGroassSales floatValue]!=0)?[NSNumber numberWithFloat:(1 - [s.opsSales.wtd floatValue] / [wtdGroassSales floatValue]) ] : @0;
    s.discount.mtd = ([mtdGroassSales floatValue]!=0)?[NSNumber numberWithFloat:(1 - [s.opsSales.mtd floatValue] / [mtdGroassSales floatValue]) ] : @0;
    s.discount.ytd = ([ytdGroassSales floatValue]!=0)?[NSNumber numberWithFloat:(1 - [s.opsSales.ytd floatValue] / [ytdGroassSales floatValue]) ] : @0;
    s.prod.wtd = ([sumSize floatValue]!=0)?[NSNumber numberWithFloat:([s.opsSales.wtd floatValue] / [sumSize floatValue]) ] : @0;
    s.prod.mtd= ([sumSize floatValue]!=0)? [NSNumber numberWithFloat:([s.opsSales.mtd floatValue] / [sumSize floatValue]) ] : @0;
    s.prod.ytd = ([sumSize floatValue]!=0)? [NSNumber numberWithFloat:([s.opsSales.ytd floatValue] / [sumSize floatValue]) ] : @0;
    s.atv.wtd = ([s.txn.wtd floatValue]!=0)? [NSNumber numberWithFloat:([s.opsSales.wtd floatValue] / [s.txn.wtd floatValue]) ] : @0;
    s.atv.mtd = ([s.txn.mtd floatValue]!=0)? [NSNumber numberWithFloat:([s.opsSales.mtd floatValue] / [s.txn.mtd floatValue]) ] : @0;
    s.atv.ytd = ([s.txn.ytd floatValue]!=0)? [NSNumber numberWithFloat:([s.opsSales.ytd floatValue] / [s.txn.ytd floatValue]) ] : @0;
    s.upt.wtd = ([s.txn.wtd floatValue]!=0)?([NSNumber numberWithFloat:([wtdUnit floatValue] / [s.txn.wtd floatValue]) ]):@0;
    s.upt.mtd = ([s.txn.mtd floatValue]!=0)?([NSNumber numberWithFloat:([mtdUnit floatValue] / [s.txn.mtd floatValue]) ]):@0;
    s.upt.ytd = ([s.txn.ytd floatValue]!=0)?([NSNumber numberWithFloat:([ytdUnit floatValue] / [s.txn.ytd floatValue]) ]):@0;
    s.peel.wtd = ([wtdOutside floatValue]!=0)?([NSNumber numberWithFloat:([s.visitor.wtd floatValue] / [wtdOutside floatValue]) ]):@0;
    s.peel.mtd = ([mtdOutside floatValue]!=0)?([NSNumber numberWithFloat:([s.visitor.mtd floatValue] / [mtdOutside floatValue]) ]):@0;
    s.peel.ytd = ([ytdOutside floatValue]!=0)?([NSNumber numberWithFloat:([s.visitor.ytd floatValue] / [ytdOutside floatValue]) ]):@0;
    
    return s;
}

//If no user is selected in the DS list, then it display the data from dashboard
//Else, no user
-(void) reloadSummaryView{

    VFDAO* vf = [VFDAO sharedInstance];
    NSString* dateStr = [vf.cache objectForKey:@"fxdateYYYYMD"];
    NSString* key = [NSString stringWithFormat:@"current_summary_%@", dateStr ];
    
    NSDictionary* summaryParam =  [vf.cache objectForKey: key];
    
    for(UIView* v in [self.summaryView subviews]){
        [v removeFromSuperview];
    }
    
    
    NSNumber* wtdOpsSales = [summaryParam objectForKey:@"ops_sales_wtd"];
    NSNumber* mtdOpsSales = [summaryParam objectForKey:@"ops_sales_mtd"];
    NSNumber* ytdOpsSales = [summaryParam objectForKey:@"ops_sales_ytd"];

    NSNumber* wtdDiscount = [summaryParam objectForKey:@"discount_wtd"];
    NSNumber* mtdDiscount = [summaryParam objectForKey:@"discount_mtd"];
    NSNumber* ytdDiscount = [summaryParam objectForKey:@"discount_ytd"];
    
    NSNumber* wtdProd = [summaryParam objectForKey:@"salesprod_wtd"];
    NSNumber* mtdProd = [summaryParam objectForKey:@"salesprod_mtd"];
    NSNumber* ytdProd = [summaryParam objectForKey:@"salesprod_ytd"];
    
    NSNumber* wtdAtv = [summaryParam objectForKey:@"atv_wtd"];
    NSNumber* mtdAtv = [summaryParam objectForKey:@"atv_mtd"];
    NSNumber* ytdAtv = [summaryParam objectForKey:@"atv_ytd"];
    
    NSNumber* wtdUPT = [summaryParam objectForKey:@"upt_wtd"];
    NSNumber* mtdUPT = [summaryParam objectForKey:@"upt_mtd"];
    NSNumber* ytdUPT = [summaryParam objectForKey:@"upt_ytd"];
    
    NSNumber* wtdTxn = [summaryParam objectForKey:@"txn_wtd"];
    NSNumber* mtdTxn = [summaryParam objectForKey:@"txn_mtd"];
    NSNumber* ytdTxn = [summaryParam objectForKey:@"txn_ytd"];

    NSNumber* wtdTTxn = [summaryParam objectForKey:@"ttxn_wtd"];
    NSNumber* mtdTTxn = [summaryParam objectForKey:@"ttxn_mtd"];
    NSNumber* ytdTTxn = [summaryParam objectForKey:@"ttxn_ytd"];

    NSNumber* wtdVisitor = [summaryParam objectForKey:@"visitor_wtd"];
    NSNumber* mtdVisitor = [summaryParam objectForKey:@"visitor_mtd"];
    NSNumber* ytdVisitor = [summaryParam objectForKey:@"visitor_ytd"];
    
    NSNumber* wtdPel = [summaryParam objectForKey:@"pel_wtd"];
    NSNumber* mtdPel = [summaryParam objectForKey:@"pel_mtd"];
    NSNumber* ytdPel = [summaryParam objectForKey:@"pel_ytd"];
    
    NSNumber* wtdCR = [summaryParam objectForKey:@"cr_wtd"];
    NSNumber* mtdCR = [summaryParam objectForKey:@"cr_mtd"];
    NSNumber* ytdCR = [summaryParam objectForKey:@"cr_ytd"];
    
    
    if(self.selectedUserVal!=nil || [self.selectedStoreTypeValArray count]>0 || [self.selectedCompStatusValArray count]>0){
        StoreData* sumData = [self calculateSummaryDataForAEID: self.selectedUserVal storeType:self.selectedStoreTypeValArray compStatus:self.selectedCompStatusValArray storeList:self.recordAry];
        wtdOpsSales = sumData.opsSales.wtd;
        mtdOpsSales = sumData.opsSales.mtd;
        ytdOpsSales = sumData.opsSales.ytd;
        wtdDiscount = sumData.discount.wtd;
        mtdDiscount = sumData.discount.mtd;
        ytdDiscount = sumData.discount.ytd;
        wtdProd = sumData.prod.wtd;
        mtdProd = sumData.prod.mtd;
        ytdProd = sumData.prod.ytd;
        wtdAtv = sumData.atv.wtd;
        mtdAtv = sumData.atv.mtd;
        ytdAtv = sumData.atv.ytd;
        wtdUPT = sumData.upt.wtd;
        mtdUPT = sumData.upt.mtd;
        ytdUPT = sumData.upt.ytd;
        wtdTxn = sumData.txn.wtd;
        mtdTxn = sumData.txn.mtd;
        ytdTxn = sumData.txn.ytd;
        wtdTTxn = sumData.ttxn.wtd;
        mtdTTxn = sumData.ttxn.mtd;
        ytdTTxn = sumData.ttxn.ytd;
        wtdVisitor = sumData.visitor.wtd;
        mtdVisitor = sumData.visitor.mtd;
        ytdVisitor = sumData.visitor.ytd;
        wtdPel = sumData.peel.wtd;
        mtdPel = sumData.peel.mtd;
        ytdPel = sumData.peel.ytd;
        wtdCR = sumData.cr.wtd;
        mtdCR = sumData.cr.mtd;
        ytdCR = sumData.cr.ytd;
        
    }
    
    int w = 100;
    
    int y1 = 10;
    int y2 = 40;
    int y3 = 65;
    int y4 = 90;
    
    [self addSummaryLabelAtX:10 y:y1 text: [ShareLocale textFromKey:@"summary"]];
    [self addSummaryLabelAtX:10 y:y2 text: @"WTD:"];
    [self addSummaryLabelAtX:10 y:y3 text: @"MTD:"];
    [self addSummaryLabelAtX:10 y:y4 text: @"YTD:"];
    
    [self addSummaryLabelAtX:100 y:y1 text: [ShareLocale textFromKey:@"s_ops_sales"] ];
    [self addSummaryLabelAtX:100 y:y2 number: wtdOpsSales format:@"%.2fk" scale:0.001];
    [self addSummaryLabelAtX:100 y:y3 number: mtdOpsSales format:@"%.2fk" scale:0.001];
    [self addSummaryLabelAtX:100 y:y4 number: ytdOpsSales format:@"%.2fk" scale:0.001];

    [self addSummaryLabelAtX:100+w y:y1 text: [ShareLocale textFromKey:@"s_discount"]];
    [self addSummaryLabelAtX:100+w y:y2 number: wtdDiscount format:@"%.1f%%" scale:100];
    [self addSummaryLabelAtX:100+w y:y3 number: mtdDiscount format:@"%.1f%%" scale:100];
    [self addSummaryLabelAtX:100+w y:y4 number: ytdDiscount format:@"%.1f%%" scale:100];
    
    [self addSummaryLabelAtX:100+2*w y:y1 text: [ShareLocale textFromKey:@"s_store_prod"]];
    [self addSummaryLabelAtX:100+2*w y:y2 number: wtdProd format:@"%.2fk" scale:0.001];
    [self addSummaryLabelAtX:100+2*w y:y3 number: mtdProd format:@"%.2fk" scale:0.001];
    [self addSummaryLabelAtX:100+2*w y:y4 number: ytdProd format:@"%.2fk" scale:0.001];
    
    [self addSummaryLabelAtX:100+3*w y:y1 text: [ShareLocale textFromKey:@"s_atv"]];
    [self addSummaryLabelAtX:100+3*w y:y2 number: wtdAtv format:@"%.2fk" scale:0.001];
    [self addSummaryLabelAtX:100+3*w y:y3 number: mtdAtv format:@"%.2fk" scale:0.001];
    [self addSummaryLabelAtX:100+3*w y:y4 number: ytdAtv format:@"%.2fk" scale:0.001];

    [self addSummaryLabelAtX:100+4*w y:y1 text: [ShareLocale textFromKey:@"s_upt"]];
    [self addSummaryLabelAtX:100+4*w y:y2 number: wtdUPT];
    [self addSummaryLabelAtX:100+4*w y:y3 number: mtdUPT];
    [self addSummaryLabelAtX:100+4*w y:y4 number: ytdUPT];

    [self addSummaryLabelAtX:100+5*w y:y1 text: [ShareLocale textFromKey:@"s_transaction_count"]];
    [self addSummaryLabelAtX:100+5*w y:y2 number: wtdTxn format:@"%.0f" scale:1];
    [self addSummaryLabelAtX:100+5*w y:y3 number: mtdTxn format:@"%.0f" scale:1];
    [self addSummaryLabelAtX:100+5*w y:y4 number: ytdTxn format:@"%.0f" scale:1];

    [self addSummaryLabelAtX:100+6*w y:y1 text: [ShareLocale textFromKey:@"s_visitor_count"]];
    [self addSummaryLabelAtX:100+6*w y:y2 number: wtdVisitor format:@"%.2fk" scale:0.001];
    [self addSummaryLabelAtX:100+6*w y:y3 number: mtdVisitor format:@"%.2fk" scale:0.001];
    [self addSummaryLabelAtX:100+6*w y:y4 number: ytdVisitor format:@"%.2fk" scale:0.001];

    [self addSummaryLabelAtX:100+7*w y:y1 text: [ShareLocale textFromKey:@"s_peel_off"]];
    [self addSummaryLabelAtX:100+7*w y:y2 number: wtdPel format:@"%.1f%%" scale:100];
    [self addSummaryLabelAtX:100+7*w y:y3 number: mtdPel format:@"%.1f%%" scale:100];
    [self addSummaryLabelAtX:100+7*w y:y4 number: ytdPel format:@"%.1f%%" scale:100];

    [self addSummaryLabelAtX:100+8*w y:y1 text: [ShareLocale textFromKey:@"s_conversion_rate"]];
    [self addSummaryLabelAtX:100+8*w y:y2 number:wtdCR format:@"%.1f%%" scale:100];
    [self addSummaryLabelAtX:100+8*w y:y3 number:mtdCR format:@"%.1f%%" scale:100];
    [self addSummaryLabelAtX:100+8*w y:y4 number:ytdCR format:@"%.1f%%" scale:100];
    
}


-(UILabel*) addSummaryLabelAtX:(int)x y:(int)y number:(NSNumber*)num {
    return [self addSummaryLabelAtX:x y:y text:[NSString stringWithFormat:@"%.2f", [num floatValue]]] ;
}

-(UILabel*) addSummaryLabelAtX:(int)x y:(int)y number:(NSNumber*)num format:(NSString*)format scale:(float)scale{
    return [self addSummaryLabelAtX:x y:y text:[NSString stringWithFormat:format, [num floatValue]*scale]];
}


-(UILabel*) addSummaryLabelAtX:(int)x y:(int)y text:(NSString*)text {
    UILabel* label = [[[UILabel alloc] initWithFrame:CGRectMake(x, y, 180, 22)] autorelease];
    label.font = [UIFont fontWithName:@"Arial" size:14];
    label.text = text;
    label.textColor = [UIColor whiteColor];
    [self.summaryView addSubview:label];
    return label;
}


- (void)dealloc {
    [_recordAry release];
    [_modePopup release];
    [_storeTableView release];
    [_backButton release];
    [_reloadButton release];

    [_filterPopupView release];
    [_preferencePopupView release];
    [_defaultHeaderView release];
    [_txnHeaderView release];
    [_modeButton release];
    [_headerTitleLabel release];
    [_inventoryHeaderView release];
    [_fxDateLabel release];
    [_summaryView release];
    [_invCodeButton release];
    [_invStoreButton release];
    [_invAddrButton release];
    [_weeklyCodeButton release];
    [_weeklyStoreButton release];
    [_weeklyWTDButton release];
    [_weeklyMTDButton release];
    [_weeklyYTDButton release];
    [_weeklyMTDTargetButton release];
    [_weeklyYTDTargetButton release];
    [_defaultCodeButton release];
    [_defaultStoreButton release];
    [_defaultOpsSalesButton release];
    [_defaultDiscountButton release];
    [_defaultSalesProdButton release];
    [_defaultATVButton release];
    [_defaultUPTButton release];
    [_defaultTxnButton release];
    [_defaultVisitorButton release];
    [_defaultPeelOffButton release];
    [_defaultConvButton release];
    [_heatMapButton release];
    [_userFilterButton release];
    [_groupButton release];

    [_compStatusButton release];
    [_storeTypeButton release];
    [super dealloc];
}

#pragma mark - Data Handling
-(void) switchViewMode{
    if(self.viewMode == VIEWMODE_STORE){
        [self switchViewMode: VIEWMODE_DS];
    }else{
        [self switchViewMode: VIEWMODE_STORE];
    }
}

-(void) switchViewMode:(NSInteger )newViewMode{
    self.viewMode = newViewMode;
    if(newViewMode == VIEWMODE_DS){
        [self.groupButton setTitle:[ShareLocale textFromKey:@"ds_view"] forState:UIControlStateNormal];
    }else{
        [self.groupButton setTitle:[ShareLocale textFromKey:@"store_view"] forState:UIControlStateNormal];
    }
}

- (void) refreshTableData{
    NSLog(@"ChooseStoreViewController::refreshTableData()");

    VFDAO* vf = [VFDAO sharedInstance];
    NSMutableDictionary* defaults = vf.cache;
    NSDictionary* storeDailyData = [defaults objectForKey:@"store_daily_analyse"];
    NSMutableArray* dataArray = [NSMutableArray array];
    NSMutableSet* tempUserSet = [NSMutableSet set];

    NSString* fxdateStr = [defaults objectForKey:@"fxdateYYYYMD"];
    self.fxDateLabel.text =  [NSString stringWithFormat:[ShareLocale textFromKey:@"fs_date"], [defaults objectForKey:@"fxdate"]];
    
    NSArray* storeDailyDataArray = [storeDailyData allValues];
    
    NSString* dateStr = [defaults objectForKey:@"fxdateYYYYMD"];
    NSString* key = [NSString stringWithFormat:@"current_summary_%@", dateStr ];
    NSDictionary* summaryParam =  [vf.cache objectForKey: key];
    int week = [[summaryParam objectForKey:@"week"] intValue];
    int month = [[summaryParam objectForKey:@"month"] intValue];
    int year = [[summaryParam objectForKey:@"year"] intValue];
    
    
    
    NSSet* userDataLevels = [NSSet setWithArray: defaults[@"visible_data_level"]];
    
    NSUserDefaults *defaultPref = [NSUserDefaults standardUserDefaults];
    NSString* preferBrandCode = [defaultPref objectForKey: USER_PREFER_ACCOUNT_CODE];
    NSString* preferCountryCode = [defaultPref objectForKey:USER_PREFER_COUNTRY_CODE];

    
    for(NSDictionary* storeData in storeDailyDataArray){
        NSDictionary* dailyRecordDict = [storeData objectForKey: @"dayInfoMap"];
        NSDictionary* latestRecord = [dailyRecordDict objectForKey:fxdateStr];
        
        if(storeData[@"account_code"] ==(id)[NSNull null] ||
           ![storeData[@"account_code"] isEqualToString: preferBrandCode] ||
           storeData[@"country_code"] ==(id)[NSNull null] ||
           ![storeData[@"country_code"] isEqualToString: preferCountryCode]){
            continue;
        }
        
        //FIXME:
        //Following lines of codes are very similar in HomeViewController.
        //Please refacrtor later
        //
        float opsSaleTargetW = 0;
        float discountTargetW = 0;
        float uptTargetW = 0;
        float atvTargetW = 0;
        float crTargetW = 0;
        
        float opsSaleTargetM = 0;
        float discountTargetM = 0;
        float uptTargetM = 0;
        float atvTargetM = 0;
        float crTargetM = 0;
        
        float opsSaleTargetY = 0;
        float discountTargetY = 0;
        float uptTargetY = 0;
        float atvTargetY = 0;
        float crTargetY = 0;
        
        
        NSArray* targetList = [defaults objectForKey:@"targets"];
        for(NSDictionary* dict in targetList){
            NSString* code = [dict objectForKey:@"Store_Code__c"];
            NSString* type = [dict objectForKey:@"Type__c"];
            
            if([code isEqualToString: [storeData objectForKey:@"code"] ]){

                if([type isEqualToString:@"weekly"]){
                    int w = [[dict objectForKey:@"Financial_Week__c"] intValue];
                    int y = [[dict objectForKey:@"Financial_Year__c"] intValue];
                    if(w==week && y==year){
                        if([dict objectForKey:@"OPS_Sales_Amount__c"]!=[NSNull null]){
                            opsSaleTargetW += [[dict objectForKey:@"OPS_Sales_Amount__c"] floatValue];
                        }
                        if([dict objectForKey:@"Discount__c"]!=[NSNull null] &&  discountTargetW==0){
                            discountTargetW = [[dict objectForKey:@"Discount__c"] floatValue];
                        }
                        if([dict objectForKey:@"UPT__c"]!=[NSNull null] && uptTargetW==0){
                            uptTargetW = [[dict objectForKey:@"UPT__c"] floatValue];
                        }
                        if([dict objectForKey:@"ATV__c"]!=[NSNull null] && atvTargetW==0){
                            atvTargetW = [[dict objectForKey:@"ATV__c"] floatValue];
                        }
                        if([dict objectForKey:@"Conversion_Rate__c"]!=[NSNull null] && crTargetW==0){
                            crTargetW = [[dict objectForKey:@"Conversion_Rate__c"] floatValue];
                        }
                    }
                }
                else if([type isEqualToString:@"monthly"]){
                    int m = [[dict objectForKey:@"Financial_Month__c"] intValue];
                    int y = [[dict objectForKey:@"Financial_Year__c"] intValue];
                    if(m==month && y==year){
                        if([dict objectForKey:@"OPS_Sales_Amount__c"]!=[NSNull null]){
                            opsSaleTargetM += [[dict objectForKey:@"OPS_Sales_Amount__c"] floatValue];
                        }
                        if([dict objectForKey:@"Discount__c"]!=[NSNull null] && discountTargetM==0){
                            discountTargetM = [[dict objectForKey:@"Discount__c"] floatValue];
                        }
                        if([dict objectForKey:@"UPT__c"]!=[NSNull null] && uptTargetM==0){
                            uptTargetM = [[dict objectForKey:@"UPT__c"] floatValue];
                        }
                        if([dict objectForKey:@"ATV__c"]!=[NSNull null] && atvTargetM==0){
                            atvTargetM = [[dict objectForKey:@"ATV__c"] floatValue];
                        }
                        if([dict objectForKey:@"Conversion_Rate__c"]!=[NSNull null] && crTargetM==0){
                            crTargetM = [[dict objectForKey:@"Conversion_Rate__c"] floatValue];
                        }
                    }
                }
                else if([type isEqualToString:@"yearly"]){
                    int y = [[dict objectForKey:@"Financial_Year__c"] intValue];
                    if(y==year){
                        if([dict objectForKey:@"OPS_Sales_Amount__c"]!=[NSNull null]){
                            opsSaleTargetY += [[dict objectForKey:@"OPS_Sales_Amount__c"] floatValue];
                        }
                        if([dict objectForKey:@"Discount__c"]!=[NSNull null] && discountTargetY==0){
                            discountTargetY = [[dict objectForKey:@"Discount__c"] floatValue];
                        }
                        if([dict objectForKey:@"UPT__c"]!=[NSNull null] && uptTargetY==0){
                            uptTargetY = [[dict objectForKey:@"UPT__c"] floatValue];
                        }
                        if([dict objectForKey:@"ATV__c"]!=[NSNull null] && atvTargetY==0){
                            atvTargetY = [[dict objectForKey:@"ATV__c"] floatValue];
                        }
                        if([dict objectForKey:@"Conversion_Rate__c"]!=[NSNull null] && crTargetY==0){
                            crTargetY = [[dict objectForKey:@"Conversion_Rate__c"] floatValue];
                        }
                    }
                }
            }
        }
        
        NSString* recordOwnerAEID = [self patchNull:storeData[@"aeId"]];
        NSString* recordOwnerName = recordOwnerAEID;
        
        //NSArray* userList = defaults[@"user_list"];
        NSDictionary* userMap = defaults[@"user_map"];
        if(recordOwnerAEID!=nil && userMap[recordOwnerAEID]!=nil){
            recordOwnerName = userMap[recordOwnerAEID][@"name"];
        }

//        for(NSDictionary* u in userList){
//            NSString* userAEId =  u[@"AE_ID__c"];
//            if(userAEId!=(id)[NSNull null]  ){
//                if ([[userAEId lowercaseString] isEqualToString:recordOwnerAEID]){
//                    recordOwnerName = u[@"Name"];
//                    break;
//                }
//            }
//        }
        
        if(![recordOwnerAEID isEqualToString:@""]){
            [tempUserSet addObject: @{@"Label":recordOwnerName,
                                  @"Value":recordOwnerAEID } ];
        }
        
        [dataArray addObject:@{
                                //basic info
                                @"Id": [self patchNull:[storeData objectForKey:@"store_id"]],
                                @"Name": [self patchNull:[storeData objectForKey:@"name"]],
                                @"Data_Level__c": [self patchNull:[storeData objectForKey:@"data_level"]],
                                @"aeId": [self patchNull:[storeData objectForKey:@"aeId"]],
                                @"Profile__c": [self patchNull:[storeData objectForKey:@"profile"]],
                                @"Store_Type__c": [self patchNull:[storeData objectForKey:@"store_type"]],
                                @"Comp_Status__c": [self patchNull:[storeData objectForKey:@"comp_status"]],
                                @"emfa__Address__c": [self patchNull:[storeData objectForKey:@"addr"]],
                                @"emfa__Email__c": [self patchNull:[storeData objectForKey:@"email"]],
                                @"emfa__Fax__c": [self patchNull:[storeData objectForKey:@"fax"]],
                                @"emfa__Phone__c": [self patchNull:[storeData objectForKey:@"phone"]],
                                @"Store_Code__c": [self patchNull:[storeData objectForKey:@"code"]],
                                @"LastModifiedDate": [self patchNull:[storeData objectForKey:@"last_mod_date"]],
                                @"emfa__Primary_Latitude__c": [self patchNull:[storeData objectForKey:@"pri_lat"]],
                                @"emfa__Primary_Longitude__c": [self patchNull:[storeData objectForKey:@"pri_long"]],
                                
                                //dashboard data
                                @"size": [self patchNull:[storeData objectForKey:@"size"]],
                                @"Weekly_Cross_Sales__c": [self patchNull:[[latestRecord objectForKey:@"cross_sales"] objectForKey:@"TYWTD"]],
                                @"Weekly_Unit_Count__c": [self patchNull:[[latestRecord objectForKey:@"unit_count"] objectForKey:@"TYWTD"]],
                                @"Weekly_Outside_Traffic__c": [self patchNull:[[latestRecord objectForKey:@"outsidie_traffic"] objectForKey:@"TYWTD"]],
                                
                                @"Monthly_Cross_Sales__c": [self patchNull:[[latestRecord objectForKey:@"cross_sales"] objectForKey:@"TYMTD"]],
                                @"Monthly_Unit_Count__c": [self patchNull:[[latestRecord objectForKey:@"unit_count"] objectForKey:@"TYMTD"]],
                                @"Monthly_Outside_Traffic__c": [self patchNull:[[latestRecord objectForKey:@"outsidie_traffic"] objectForKey:@"TYMTD"]],

                                
                                @"Yearly_Cross_Sales__c": [self patchNull:[[latestRecord objectForKey:@"cross_sales"] objectForKey:@"TYYTD"]],
                                @"Yearly_Unit_Count__c": [self patchNull:[[latestRecord objectForKey:@"unit_count"] objectForKey:@"TYYTD"]],
                                @"Yearly_Outside_Traffic__c": [self patchNull:[[latestRecord objectForKey:@"outsidie_traffic"] objectForKey:@"TYYTD"]],
                                
                                
                                //Last year weekly data
                                @"Weekly_Ops_Sales_LY": [self patchNull:[[latestRecord objectForKey:@"ops_sales"] objectForKey:@"LYWTD"] ],
                                @"Weekly_Discount_LY": [self patchNull:[[latestRecord objectForKey:@"discount"] objectForKey:@"LYWTD"] ],
                                @"Weekly_Sales_Productivity_LY": [self patchNull:[[latestRecord objectForKey:@"sales_prod"] objectForKey:@"LYWTD"]] ,
                                @"Weekly_ATV_LY": [self patchNull:[[latestRecord objectForKey:@"atv"] objectForKey:@"LYWTD"] ],
                                @"Weekly_UPT_LY": [self patchNull:[[latestRecord objectForKey:@"upt"] objectForKey:@"LYWTD"] ],
                                @"Weekly_Transaction_Count_LY": [self patchNull:[[latestRecord objectForKey:@"txn_count"] objectForKey:@"LYWTD"] ],
                                @"Weekly_Traffic_Transaction_Count_LY": [self patchNull:[[latestRecord objectForKey:@"t_txn_count"] objectForKey:@"LYWTD"] ],
                                @"Weekly_T_Visitor_Count_LY": [self patchNull:[[latestRecord objectForKey:@"t_visitor_count"] objectForKey:@"LYWTD"] ],
                                @"Weekly_Visitor_Count_LY": [self patchNull:[[latestRecord objectForKey:@"visitor_count"] objectForKey:@"LYWTD"] ],
                                @"Weekly_Pel_Off_LY": [self patchNull:[[latestRecord objectForKey:@"pel_off"] objectForKey:@"LYWTD"] ],
                                @"Weekly_Conversion_Rate_LY": [self patchNull:[[latestRecord objectForKey:@"conv_rate"] objectForKey:@"LYWTD"] ],
                                
                                //Last year monthly data
                                @"Monthly_Ops_Sales_LY": [self patchNull:[[latestRecord objectForKey:@"ops_sales"] objectForKey:@"LYMTD"] ],
                                @"Monthly_Discount_LY": [self patchNull:[[latestRecord objectForKey:@"discount"] objectForKey:@"LYMTD"] ],
                                @"Monthly_Sales_Productivity_LY": [self patchNull:[[latestRecord objectForKey:@"sales_prod"] objectForKey:@"LYMTD"] ],
                                @"Monthly_ATV_LY": [self patchNull: [[latestRecord objectForKey:@"atv"] objectForKey:@"LYMTD"]] ,
                                @"Monthly_UPT_LY": [self patchNull:[[latestRecord objectForKey:@"upt"] objectForKey:@"LYMTD"]] ,
                                @"Monthly_Transaction_Count_LY": [self patchNull:[[latestRecord objectForKey:@"txn_count"] objectForKey:@"LYMTD"] ],
                                @"Monthly_Traffic_Transaction_Count_LY": [self patchNull:[[latestRecord objectForKey:@"t_txn_count"] objectForKey:@"LYMTD"] ],
                                @"Monthly_T_Visitor_Count_LY": [self patchNull:[[latestRecord objectForKey:@"t_visitor_count"] objectForKey:@"LYMTD"] ],
                                @"Monthly_Visitor_Count_LY": [self patchNull:[[latestRecord objectForKey:@"visitor_count"] objectForKey:@"LYMTD"] ],
                                @"Monthly_Pel_Off_LY": [self patchNull:[[latestRecord objectForKey:@"pel_off"] objectForKey:@"LYMTD"] ],
                                @"Monthly_Conversion_Rate_LY": [self patchNull:[[latestRecord objectForKey:@"conv_rate"] objectForKey:@"LYMTD"] ],
                                
                                //Last year yearly data
                                @"Yearly_Ops_Sales_LY": [self patchNull:[[latestRecord objectForKey:@"ops_sales"] objectForKey:@"LYYTD"] ],
                                @"Yearly_Discount_LY": [self patchNull:[[latestRecord objectForKey:@"discount"] objectForKey:@"LYYTD"] ],
                                @"Yearly_Sales_Productivity_LY": [self patchNull:[[latestRecord objectForKey:@"sales_prod"] objectForKey:@"LYYTD"] ],
                                @"Yearly_ATV_LY": [self patchNull:[[latestRecord objectForKey:@"atv"] objectForKey:@"LYYTD"] ],
                                @"Yearly_UPT_LY": [self patchNull:[[latestRecord objectForKey:@"upt"] objectForKey:@"LYYTD"] ],
                                @"Yearly_Transaction_Count_LY": [self patchNull:[[latestRecord objectForKey:@"txn_count"] objectForKey:@"LYYTD"] ],
                                @"Yearly_Traffic_Transaction_Count_LY": [self patchNull:[[latestRecord objectForKey:@"t_txn_count"] objectForKey:@"LYYTD"] ],
                                @"Yearly_T_Visitor_Count_LY": [self patchNull:[[latestRecord objectForKey:@"t_visitor_count"] objectForKey:@"LYYTD"] ],
                                @"Yearly_Visitor_Count_LY": [self patchNull:[[latestRecord objectForKey:@"visitor_count"] objectForKey:@"LYYTD"] ],
                                @"Yearly_Pel_Off_LY": [self patchNull:[[latestRecord objectForKey:@"pel_off"] objectForKey:@"LYYTD"] ],
                                @"Yearly_Conversion_Rate_LY": [self patchNull:[[latestRecord objectForKey:@"conv_rate"] objectForKey:@"LYYTD"] ],
                                
                                
                                //this year weekly data
                                @"Weekly_Ops_Sales__c":  [self patchNull:[[latestRecord objectForKey:@"ops_sales"] objectForKey:@"TYWTD"]],
                                @"Weekly_Discount__c":  [self patchNull:[[latestRecord objectForKey:@"discount"] objectForKey:@"TYWTD"]],
                                @"Weekly_Sales_Productivity__c":  [self patchNull:[[latestRecord objectForKey:@"sales_prod"] objectForKey:@"TYWTD"]],
                                @"Weekly_ATV__c":  [self patchNull:[[latestRecord objectForKey:@"atv"] objectForKey:@"TYWTD"]],
                                @"Weekly_UPT__c":  [self patchNull:[[latestRecord objectForKey:@"upt"] objectForKey:@"TYWTD"]],
                                @"Weekly_Transaction_Count__c":  [self patchNull:[[latestRecord objectForKey:@"txn_count"] objectForKey:@"TYWTD"]],
                                @"Weekly_Traffic_Transaction_Count__c":  [self patchNull:[[latestRecord objectForKey:@"t_txn_count"] objectForKey:@"TYWTD"]],
                                @"Weekly_T_Visitor_Count__c": [self patchNull: [[latestRecord objectForKey:@"t_visitor_count"] objectForKey:@"TYWTD"]],
                                @"Weekly_Visitor_Count__c": [self patchNull: [[latestRecord objectForKey:@"visitor_count"] objectForKey:@"TYWTD"]],
                                @"Weekly_Pel_Off__c": [self patchNull: [[latestRecord objectForKey:@"pel_off"] objectForKey:@"TYWTD"]],
                                @"Weekly_Conversion_Rate__c": [self patchNull: [[latestRecord objectForKey:@"conv_rate"] objectForKey:@"TYWTD"]],
                                
                                //this year monthly data
                                @"Monthly_Ops_Sales__c":  [self patchNull: [[latestRecord objectForKey:@"ops_sales"] objectForKey:@"TYMTD"]],
                                @"Monthly_Discount__c":  [self patchNull: [[latestRecord objectForKey:@"discount"] objectForKey:@"TYMTD"]],
                                @"Monthly_Sales_Productivity__c": [self patchNull: [[latestRecord objectForKey:@"sales_prod"] objectForKey:@"TYMTD"]],
                                @"Monthly_ATV__c": [self patchNull: [[latestRecord objectForKey:@"atv"] objectForKey:@"TYMTD"]],
                                @"Monthly_UPT__c": [self patchNull: [[latestRecord objectForKey:@"upt"] objectForKey:@"TYMTD"]],
                                @"Monthly_Transaction_Count__c": [self patchNull: [[latestRecord objectForKey:@"txn_count"] objectForKey:@"TYMTD"]],
                                @"Monthly_Traffic_Transaction_Count__c": [self patchNull: [[latestRecord objectForKey:@"t_txn_count"] objectForKey:@"TYMTD"]],
                                @"Monthly_T_Visitor_Count__c": [self patchNull: [[latestRecord objectForKey:@"t_visitor_count"] objectForKey:@"TYMTD"] ],
                                @"Monthly_Visitor_Count__c": [self patchNull: [[latestRecord objectForKey:@"visitor_count"] objectForKey:@"TYMTD"] ],
                                @"Monthly_Pel_Off__c": [self patchNull: [[latestRecord objectForKey:@"pel_off"] objectForKey:@"TYMTD"]],
                                @"Monthly_Conversion_Rate__c":  [self patchNull:[[latestRecord objectForKey:@"conv_rate"] objectForKey:@"TYMTD"]],
                                
                                //this year yearly data
                                @"Yearly_Ops_Sales__c": [self patchNull: [[latestRecord objectForKey:@"ops_sales"] objectForKey:@"TYYTD"]],
                                @"Yearly_Discount__c": [self patchNull: [[latestRecord objectForKey:@"discount"] objectForKey:@"TYYTD"]],
                                @"Yearly_Sales_Productivity__c": [self patchNull: [[latestRecord objectForKey:@"sales_prod"] objectForKey:@"TYYTD"] ],
                                @"Yearly_ATV__c": [self patchNull: [[latestRecord objectForKey:@"atv"] objectForKey:@"TYYTD"] ],
                                @"Yearly_UPT__c": [self patchNull: [[latestRecord objectForKey:@"upt"] objectForKey:@"TYYTD"] ],
                                @"Yearly_Transaction_Count__c": [self patchNull: [[latestRecord objectForKey:@"txn_count"] objectForKey:@"TYYTD"] ],
                                @"Yearly_Traffic_Transaction_Count__c": [self patchNull: [[latestRecord objectForKey:@"t_txn_count"] objectForKey:@"TYYTD"] ],
                                @"Yearly_T_Visitor_Count__c": [self patchNull: [[latestRecord objectForKey:@"t_visitor_count"] objectForKey:@"TYYTD"] ],
                                @"Yearly_Visitor_Count__c": [self patchNull: [[latestRecord objectForKey:@"visitor_count"] objectForKey:@"TYYTD"] ],
                                @"Yearly_Pel_Off__c": [self patchNull: [[latestRecord objectForKey:@"pel_off"] objectForKey:@"TYYTD"] ],
                                @"Yearly_Conversion_Rate__c": [self patchNull: [[latestRecord objectForKey:@"conv_rate"] objectForKey:@"TYYTD"] ],
                                
                                //targets
                                @"Weekly_Ops_Sales_Target__c": [NSNumber numberWithFloat: opsSaleTargetW],
                                @"Monthly_Ops_Sales_Target__c": [NSNumber numberWithFloat: opsSaleTargetM],
                                @"Yearly_Ops_Sales_Target__c": [NSNumber numberWithFloat: opsSaleTargetY],
                                @"Weekly_ATV_Target__c": [NSNumber numberWithFloat: atvTargetW],
                                @"Monthly_ATV_Target__c": [NSNumber numberWithFloat: atvTargetM],
                                @"Yearly_ATV_Target__c": [NSNumber numberWithFloat: atvTargetY],
                                @"Weekly_UPT_Target__c": [NSNumber numberWithFloat: uptTargetW],
                                @"Monthly_UPT_Target__c": [NSNumber numberWithFloat: uptTargetM],
                                @"Yearly_UPT_Target__c": [NSNumber numberWithFloat: uptTargetY],
                                @"Weekly_Conversion_Rate_Target__c": [NSNumber numberWithFloat: crTargetW],
                                @"Monthly_Conversion_Rate_Target__c": [NSNumber numberWithFloat: crTargetM],
                                @"Yearly_Conversion_Rate_Target__c": [NSNumber numberWithFloat: crTargetY],
                                @"Weekly_Discount_Target__c": [NSNumber numberWithFloat: discountTargetW],
                                @"Monthly_Discount_Target__c": [NSNumber numberWithFloat: discountTargetM],
                                @"Yearly_Discount_Target__c": [NSNumber numberWithFloat: discountTargetY],
                               }];
    }
    
    self.userArray = [NSArray arrayWithArray: [tempUserSet allObjects]];
    
    if(self.viewMode == VIEWMODE_STORE){
        //Filter1: filter out all record that he can not see
        if([userDataLevels count]>0){
            NSMutableArray* filterArray = [NSMutableArray array];
            for(NSDictionary* s in dataArray){
                NSString* storeLv = [s objectForKey:@"Data_Level__c"];
                if(storeLv==nil || storeLv==(id)[NSNull null] || [userDataLevels containsObject: storeLv]){
                    [filterArray addObject: s];
                }
            }
            dataArray = filterArray;
        }

        if(self.selectedUserVal!=nil){
            NSMutableArray* filterArray = [NSMutableArray array];
            for(NSDictionary* s in dataArray){
                NSString* storeAeID = [s[@"aeId"] lowercaseString];
                
                if([storeAeID isEqualToString:self.selectedUserVal]){
                    [filterArray addObject: s];
                }
            }
            dataArray = filterArray;
        }
        
        if([self.selectedStoreTypeValArray count]>0){
            NSMutableArray* filterArray = [NSMutableArray array];
            for(NSDictionary* s in dataArray){
                NSString* storeType = s[@"Store_Type__c"] ;
                
                if( [self.selectedStoreTypeValArray containsObject:storeType]){
                    [filterArray addObject: s];
                }
            }
            dataArray = filterArray;
        }
        
        if([self.selectedCompStatusValArray count]>0){
            NSMutableArray* filterArray = [NSMutableArray array];
            for(NSDictionary* s in dataArray){
                NSString* comp = s[@"Comp_Status__c"] ;
                
                if( [self.selectedCompStatusValArray containsObject:comp]){
                    [filterArray addObject: s];
                }
            }
            dataArray = filterArray;
        }

    }
    else if(self.viewMode == VIEWMODE_DS){
        NSMutableArray* groupArray = [NSMutableArray array];
        for(NSDictionary* obj in  tempUserSet ){
            StoreData* sumData = [self calculateSummaryDataForAEID: obj[@"Value"] storeType:self.selectedStoreTypeValArray compStatus:self.selectedCompStatusValArray storeList: dataArray];
            NSMutableDictionary* data = [sumData dictionary];
            data[@"Name"] = obj[@"Label"];
            data[@"Store_Code__c"] = obj[@"Value"];
            [groupArray addObject: data];
            //generate a new store dictionary that sum related data
            //replace  to groupArray
            
        }
        dataArray = groupArray;
    }
    
    self.recordAry = [dataArray sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSString *first = [(NSDictionary*)a objectForKey:@"Store_Code__c"];
        NSString *second = [(NSDictionary*)b objectForKey:@"Store_Code__c"];
        return [first compare:second];
    }];
    


    [self.storeTableView reloadData];
}

-(id) patchNull:(id) val{
    if(val==nil){
        return [NSNull null];
    }
    return val;
}



#pragma mark - Table Data source handling

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if(_mode == MODE_INVENTORY){
        [self onShowInventoryView: indexPath.row];
    }
    else{
        if(self.viewMode == VIEWMODE_DS){
            [self onSelectedUserRow: indexPath.row];
        }
        else if(self.viewMode == VIEWMODE_STORE){
            [self onShowStoreInfo: indexPath.row];
        }
    }
    [tableView deselectRowAtIndexPath: indexPath animated:NO];
}

- (float) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.recordAry count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    UIImageView *colorIcon;
    
    if(_mode==0){
         cell = [tableView dequeueReusableCellWithIdentifier:@"Store9ColumnCell"];
        if(cell == nil){
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Store9ColumnCell"] autorelease];
            //2+9 columns
            UILabel* codeLabel = [self createNewTextLabelWithDefaultStyle: CGRectMake(0+10, 0, 45, 50) parent:cell tag: 901];
            codeLabel.adjustsFontSizeToFitWidth = YES;
            codeLabel.minimumScaleFactor = .6f;
            
            [self createNewTextLabelWithDefaultStyle: CGRectMake(60, 0, 150, 50) parent:cell tag: 908];
            [self createNewTextLabelWithDefaultStyle: CGRectMake(200+25, 0, 75, 50) parent:cell tag: 902];
            [self createNewTextLabelWithDefaultStyle: CGRectMake(290+25, 0, 75, 50) parent:cell tag: 903];
            [self createNewTextLabelWithDefaultStyle: CGRectMake(380+25, 0, 75, 50) parent:cell tag: 904];
            [self createNewTextLabelWithDefaultStyle: CGRectMake(470+25, 0, 75, 50) parent:cell tag: 905];
            [self createNewTextLabelWithDefaultStyle: CGRectMake(560+25, 0, 75, 50) parent:cell tag: 906];
            [self createNewTextLabelWithDefaultStyle: CGRectMake(650+25, 0, 75, 50) parent:cell tag: 907];
            [self createNewTextLabelWithDefaultStyle: CGRectMake(740+25, 0, 75, 50) parent:cell tag: 909];
            [self createNewTextLabelWithDefaultStyle: CGRectMake(830+25, 0, 75, 50) parent:cell tag: 910];
            [self createNewTextLabelWithDefaultStyle: CGRectMake(920+25, 0, 75, 50) parent:cell tag: 911];
            
            colorIcon = [[UIImageView alloc] initWithFrame:CGRectMake(200+8, 16, 15, 22)];
            [colorIcon setTag:951];
            colorIcon.contentMode = UIViewContentModeScaleAspectFit;
            [cell.contentView addSubview: colorIcon];
            [colorIcon release];
            colorIcon = [[UIImageView alloc] initWithFrame:CGRectMake(290+8, 16, 15, 22)];
            colorIcon.contentMode = UIViewContentModeScaleAspectFit;
            [colorIcon setTag:952];
            [cell.contentView addSubview: colorIcon];
            [colorIcon release];
            colorIcon = [[UIImageView alloc] initWithFrame:CGRectMake(380+8, 16, 15, 22)];
            colorIcon.contentMode = UIViewContentModeScaleAspectFit;
            [colorIcon setTag:953];
            [cell.contentView addSubview: colorIcon];
            [colorIcon release];
            colorIcon = [[UIImageView alloc] initWithFrame:CGRectMake(470+8, 16, 15, 22)];
            colorIcon.contentMode = UIViewContentModeScaleAspectFit;
            [colorIcon setTag:954];
            [cell.contentView addSubview: colorIcon];
            [colorIcon release];
            colorIcon = [[UIImageView alloc] initWithFrame:CGRectMake(560+8, 16, 15, 22)];
            colorIcon.contentMode = UIViewContentModeScaleAspectFit;
            [colorIcon setTag:955];
            [cell.contentView addSubview: colorIcon];
            [colorIcon release];
            colorIcon = [[UIImageView alloc] initWithFrame:CGRectMake(650+8, 16, 15, 22)];
            colorIcon.contentMode = UIViewContentModeScaleAspectFit;
            [colorIcon setTag:956];
            [cell.contentView addSubview: colorIcon];
            [colorIcon release];
            colorIcon = [[UIImageView alloc] initWithFrame:CGRectMake(740+8, 16, 15, 22)];
            colorIcon.contentMode = UIViewContentModeScaleAspectFit;
            [colorIcon setTag:957];
            [cell.contentView addSubview: colorIcon];
            [colorIcon release];
            colorIcon = [[UIImageView alloc] initWithFrame:CGRectMake(830+8, 16, 15, 22)];
            colorIcon.contentMode = UIViewContentModeScaleAspectFit;
            [colorIcon setTag:958];
            [cell.contentView addSubview: colorIcon];
            [colorIcon release];
            colorIcon = [[UIImageView alloc] initWithFrame:CGRectMake(920+8, 16, 15, 22)];
            colorIcon.contentMode = UIViewContentModeScaleAspectFit;
            [colorIcon setTag:959];
            [cell.contentView addSubview: colorIcon];
            [colorIcon release];
            
            colorIcon = [[UIImageView alloc] initWithFrame:CGRectMake(10, 30, 16, 16)];
            colorIcon.contentMode = UIViewContentModeScaleAspectFit;
            [colorIcon setTag:960];
            [cell.contentView addSubview: colorIcon];
            [colorIcon release];

        }
    }
    else if(_mode==MODE_INVENTORY){
        cell = [tableView dequeueReusableCellWithIdentifier:@"Store3ColumnCell"];
        if(cell == nil){
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Store3ColumnCell"] autorelease];
            //2+9 columns
            UILabel* codeLabel = [self createNewTextLabelWithDefaultStyle: CGRectMake(0+10, 0, 45, 50) parent:cell tag: 901];
            codeLabel.adjustsFontSizeToFitWidth = YES;
            codeLabel.minimumScaleFactor = .6f;

            [self createNewTextLabelWithDefaultStyle: CGRectMake(60, 0, 250, 50) parent:cell tag: 908];
            [self createNewTextLabelWithDefaultStyle: CGRectMake(320, 0, 600, 50) parent:cell tag: 902];
            colorIcon = [[UIImageView alloc] initWithFrame:CGRectMake(10, 30, 16, 16)];
            colorIcon.contentMode = UIViewContentModeScaleAspectFit;
            [colorIcon setTag:960];
            [cell.contentView addSubview: colorIcon];
            [colorIcon release];
        }
    }
    else{
        cell = [tableView dequeueReusableCellWithIdentifier:@"Store6ColumnCell"];
        if(cell == nil){
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Store6ColumnCell"] autorelease];
            //2+6 columns
            UILabel* codeLabel = [self createNewTextLabelWithDefaultStyle: CGRectMake(0+10, 0, 45, 50) parent:cell tag: 901];
            codeLabel.adjustsFontSizeToFitWidth = YES;
            codeLabel.minimumScaleFactor = .6f;

            [self createNewTextLabelWithDefaultStyle: CGRectMake(60, 0, 150, 50) parent:cell tag: 908];
            [self createNewTextLabelWithDefaultStyle: CGRectMake(200+25, 0, 130, 50) parent:cell tag: 902];
            [self createNewTextLabelWithDefaultStyle: CGRectMake(330+25, 0, 130, 50) parent:cell tag: 903];
            [self createNewTextLabelWithDefaultStyle: CGRectMake(460+25, 0, 130, 50) parent:cell tag: 904];
            [self createNewTextLabelWithDefaultStyle: CGRectMake(590+25, 0, 130, 50) parent:cell tag: 905];
            [self createNewTextLabelWithDefaultStyle: CGRectMake(720+25, 0, 130, 50) parent:cell tag: 906];
            [self createNewTextLabelWithDefaultStyle: CGRectMake(850+25, 0, 130, 50) parent:cell tag: 907];
            
            colorIcon = [[UIImageView alloc] initWithFrame:CGRectMake(200+8, 16, 15, 22)];
            colorIcon.contentMode = UIViewContentModeScaleAspectFit;
            [colorIcon setTag:951];
            [cell.contentView addSubview: colorIcon];
            colorIcon.contentMode = UIViewContentModeScaleAspectFit;
            [colorIcon release];
            colorIcon = [[UIImageView alloc] initWithFrame:CGRectMake(330+8, 16, 15, 22)];
            [colorIcon setTag:952];
            [cell.contentView addSubview: colorIcon];
            colorIcon.contentMode = UIViewContentModeScaleAspectFit;
            [colorIcon release];
            colorIcon = [[UIImageView alloc] initWithFrame:CGRectMake(460+8, 16, 15, 22)];
            [colorIcon setTag:953];
            [cell.contentView addSubview: colorIcon];
            colorIcon.contentMode = UIViewContentModeScaleAspectFit;
            [colorIcon release];
            colorIcon = [[UIImageView alloc] initWithFrame:CGRectMake(590+8, 16, 15, 22)];
            [colorIcon setTag:954];
            [cell.contentView addSubview: colorIcon];
            colorIcon.contentMode = UIViewContentModeScaleAspectFit;
            [colorIcon release];
            colorIcon = [[UIImageView alloc] initWithFrame:CGRectMake(720+8, 16, 15, 22)];
            [colorIcon setTag:955];
            [cell.contentView addSubview: colorIcon];
            colorIcon.contentMode = UIViewContentModeScaleAspectFit;
            [colorIcon release];
            colorIcon = [[UIImageView alloc] initWithFrame:CGRectMake(850+8, 16, 15, 22)];
            [colorIcon setTag:956];
            [cell.contentView addSubview: colorIcon];
            colorIcon.contentMode = UIViewContentModeScaleAspectFit;
            [colorIcon release];
            colorIcon = [[UIImageView alloc] initWithFrame:CGRectMake(10, 30, 16, 16)];
            colorIcon.contentMode = UIViewContentModeScaleAspectFit;
            [colorIcon setTag:960];
            [cell.contentView addSubview: colorIcon];
            [colorIcon release];
        }
    }
    cell.textLabel.hidden = YES;
    cell.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
    UIView *selectionColor = [[[UIView alloc] init] autorelease];
    selectionColor.backgroundColor = TABLE_ROW_HIGHLIGHT_COLOR;
    cell.selectedBackgroundView = selectionColor;
    
    cell.tag = indexPath.row;
    NSDictionary* data = [self.recordAry objectAtIndex: indexPath.row];
    
    UILabel* txtView2 = (UILabel*)[cell viewWithTag:901];
    txtView2.text = [self getFieldDisplayWithFieldName:@"Store_Code__c" withDictionary: data];
    
    UILabel* txtView12 = (UILabel*)[cell viewWithTag:908];
    txtView12.frame = CGRectMake(60, 16, (_mode==MODE_INVENTORY)?250:150, 50);
    txtView12.text = [self getFieldDisplayWithFieldName:@"Name" withDictionary: data];
    txtView12.numberOfLines = 0;
    [txtView12 sizeToFit];
    
    NSString* storeType = [data objectForKey:@"Store_Type__c"];
    colorIcon = (UIImageView*)[cell viewWithTag:960];
    if([storeType isEqualToString:@"OWN OUTLET"] || [storeType isEqualToString:@"SIS OUTLET"]){
        colorIcon.image = [UIImage imageNamed:@"home32.png"];
    }
//    else if ([storeType isEqualToString:@"Outlet"]){
//        colorIcon.image = [UIImage imageNamed:@"home32.png"];
//    }
    else{
        colorIcon.image = nil;
    }
    
    if(_mode==0) return [self updateTableCellForDefaultView:data cell:cell];
    else if (_mode == CARD_OPS_SALES) return [self updateTableCellForOpsSalesView:data cell:cell];
    else if (_mode == CARD_DISCOUNT) return [self updateTableCellForDiscountView:data cell:cell];
    else if (_mode == CARD_SALES_PRODUCTIVITY) return [self updateTableCellForSalesProdView:data cell:cell];
    else if (_mode == CARD_ATV) return [self updateTableCellForAtvView:data cell:cell];
    else if (_mode == CARD_UPT) return [self updateTableCellForUptView:data cell:cell];
    else if (_mode == CARD_TRANSACTION_COUNT) return [self updateTableCellForTransactionView:data cell:cell];
    else if (_mode == CARD_VISITOR_COUNT) return [self updateTableCellForVisitorCountView:data cell:cell];
    else if (_mode == CARD_PEL_OFF) return [self updateTableCellForPelOffView:data cell:cell];
    else if (_mode == CARD_CONVERSION_RATE) return [self updateTableCellForConvRateView:data cell:cell];
    else if (_mode == MODE_INVENTORY) return [self updateTableCellForInventoryView:data cell:cell];
    else return cell;

}

-(UITableViewCell*) updateTableCellForDefaultView:(NSDictionary*)data cell:(UITableViewCell*)cell{
    //Total 9 columns
    
    //Ops Sales
    UILabel* txtView902 = (UILabel*)[cell viewWithTag:902];
    [self fillRawValueTableCell:cell labelWithTag:902 imageWithTag:951 data:data dataKey:@"Weekly_Ops_Sales" colorKey:W_PREF_OPS_SALES format:@"%.2f" autoShortForm:YES];
        [self addCellHiddenButtonToTableCell:cell frame:txtView902.frame tag:78001 referSelector:@selector(onOpsSalesPinClicked:)];
    //Discount
    UILabel* txtView903 = (UILabel*)[cell viewWithTag:903];
    [self fillRawValueTableCell:cell labelWithTag:903 imageWithTag:952 data:data dataKey:@"Weekly_Discount" colorKey:W_PREF_OPS_SALES format:@"%.1f%%" autoShortForm:NO scale:100 negativeComparing:YES skipTarget:NO];
    [self addCellHiddenButtonToTableCell:cell frame:txtView903.frame tag:78002 referSelector:@selector(onDiscountPinClicked:)];
    //Store Productivity
    UILabel* txtView904 = (UILabel*)[cell viewWithTag:904];
    [self fillRawValueTableCell:cell labelWithTag:904 imageWithTag:953 data:data dataKey:@"Weekly_Sales_Productivity" colorKey:W_PREF_SALES_PROD format:@"%.2f" autoShortForm:YES];
    [self addCellHiddenButtonToTableCell:cell frame:txtView904.frame tag:78003 referSelector:@selector(onSalesPerformancePinClicked:)];

    //ATV
    UILabel* txtView905 = (UILabel*)[cell viewWithTag:905];
    [self fillRawValueTableCell:cell labelWithTag:905 imageWithTag:954 data:data dataKey:@"Weekly_ATV" colorKey:W_PREF_ATV format:@"%.2f" autoShortForm:YES scale:1 negativeComparing:NO skipTarget:YES];
    [self addCellHiddenButtonToTableCell:cell frame:txtView905.frame tag:78004 referSelector:@selector(onATVPinClicked:)];
    
    //UPT
    UILabel* txtView906 = (UILabel*)[cell viewWithTag:906];
    [self fillRawValueTableCell:cell labelWithTag:906 imageWithTag:955 data:data dataKey:@"Weekly_UPT" colorKey:W_PREF_UPT format:@"%.2f" autoShortForm:YES];
    [self addCellHiddenButtonToTableCell:cell frame:txtView906.frame tag:78005 referSelector:@selector(onUPTPinClicked:)];
    //TXN
    UILabel* txtView907 = (UILabel*)[cell viewWithTag:907];
    [self fillRawValueTableCell:cell labelWithTag:907 imageWithTag:956 data:data dataKey:@"Weekly_Transaction_Count" colorKey:W_PREF_TXN format:@"%.2f" autoShortForm:YES];
    [self addCellHiddenButtonToTableCell:cell frame:txtView907.frame tag:78006 referSelector:@selector(onTransactionPinClicked:)];

    //VISITOR
    UILabel* txtView909 = (UILabel*)[cell viewWithTag:909];
    [self fillRawValueTableCell:cell labelWithTag:909 imageWithTag:957 data:data dataKey:@"Weekly_Visitor_Count" colorKey:W_PREF_VISITOR_COUNT format:@"%.1f" autoShortForm:YES scale:1];
    [self addCellHiddenButtonToTableCell:cell frame:txtView909.frame tag:78007 referSelector:@selector(onVisitorPinClicked:)];
    //PEEL OFF
    UILabel* txtView910 = (UILabel*)[cell viewWithTag:910];
    [self fillRawValueTableCell:cell labelWithTag:910 imageWithTag:958 data:data dataKey:@"Weekly_Pel_Off" colorKey:W_PREF_PEL_OFF format:@"%.1f%%" autoShortForm:NO scale:100];
    [self addCellHiddenButtonToTableCell:cell frame:txtView910.frame tag:78008 referSelector:@selector(onPelOffPinClicked:)];
    //CR
    UILabel* txtView911 = (UILabel*)[cell viewWithTag:911];
    [self fillRawValueTableCell:cell labelWithTag:911 imageWithTag:959 data:data dataKey:@"Weekly_Conversion_Rate" colorKey:W_PREF_CONV_RATE format:@"%.1f%%" autoShortForm:NO scale:100];
    [self addCellHiddenButtonToTableCell:cell frame:txtView911.frame tag:78009 referSelector:@selector(onConversionRatePinClicked:)];
    
    return cell;
}


-(UITableViewCell*) updateTableCellForInventoryView:(NSDictionary*)data cell:(UITableViewCell*)cell{
    UILabel* txtView3 = (UILabel*)[cell viewWithTag:902];
    txtView3.text = [self getFieldDisplayWithFieldName:@"emfa__Address__c" withDictionary: data];
    return cell;
}



//Shared function to handle label value, color pin and target of each pair cell in 6-columns tableview
//autoFormat: True then it will convert the value display automatically; else it use user provided format
-(void) fillRawValueTableCell:(UITableViewCell*)cell labelWithTag:(int)tag imageWithTag:(int)imgTag data:(NSDictionary*)data dataKey:(NSString*)key  colorKey:(NSString*)colorKey format:(NSString*)format autoShortForm:(BOOL)shortForm{
    [self fillRawValueTableCell:cell labelWithTag:tag imageWithTag:imgTag data:data dataKey:key colorKey:colorKey format:format autoShortForm:shortForm scale:1 negativeComparing:NO skipTarget:NO];
}

-(void) fillRawValueTableCell:(UITableViewCell*)cell labelWithTag:(int)tag imageWithTag:(int)imgTag data:(NSDictionary*)data dataKey:(NSString*)key  colorKey:(NSString*)colorKey format:(NSString*)format autoShortForm:(BOOL)shortForm scale:(float) scale{
    [self fillRawValueTableCell:cell labelWithTag:tag imageWithTag:imgTag data:data dataKey:key colorKey:colorKey format:format autoShortForm:shortForm scale:scale negativeComparing:NO skipTarget:NO];
}


-(void) fillRawValueTableCell:(UITableViewCell*)cell labelWithTag:(int)tag imageWithTag:(int)imgTag data:(NSDictionary*)data dataKey:(NSString*)key  colorKey:(NSString*)colorKey format:(NSString*)format autoShortForm:(BOOL)shortForm scale:(float)scale negativeComparing:(BOOL)flag skipTarget:(BOOL)skipTarget{
    UILabel* txtView = (UILabel*)[cell viewWithTag:tag];
    float periodVal = [[self getFieldDisplayWithFieldName:[NSString stringWithFormat:@"%@__c", key] withDictionary: data] floatValue];
    
    if(shortForm){
        if(periodVal*scale>1000*1000){
            txtView.text = [NSString stringWithFormat:@"%.2fm",periodVal*scale/1000/1000];
        }else if (periodVal*scale>1000){
            txtView.text = [NSString stringWithFormat:@"%.2fk",periodVal*scale/1000];
        }else{
            txtView.text = [NSString stringWithFormat:@"%.2f",periodVal*scale];
        }
    }
    else{
        txtView.text = [NSString stringWithFormat:format,periodVal*scale];
    }
    
    float lastYearVal = [[self getFieldDisplayWithFieldName:[NSString stringWithFormat:@"%@_LY", key] withDictionary: data] floatValue];
    
    UIImageView *colorIcon = (UIImageView *)[cell viewWithTag:imgTag];
    if(lastYearVal==0){
        colorIcon.hidden = YES;
    }else{
        colorIcon.hidden = NO;
        
        if(!flag){
            if(periodVal >= lastYearVal){
                [colorIcon setImage: [UIImage imageNamed: @"green_up_arrow.png"]];
            }else{
                [colorIcon setImage: [UIImage imageNamed: @"red_down_arrow.png"]];
            }
        }else{
            if(periodVal <= lastYearVal){
                [colorIcon setImage: [UIImage imageNamed: @"green_down_arrow.png"]];
            }else{
                [colorIcon setImage: [UIImage imageNamed: @"red_up_arrow.png"]];
            }
        }
        
        //else [colorIcon setImage: [UIImage imageNamed: @"green_down_arrow.png"]];
        //else [colorIcon setImage: [UIImage imageNamed: @"red_up_arrow.png"]];
    }
    
    if(!skipTarget){
        UILabel* txtView6 = (UILabel*)[cell viewWithTag:tag+3];
        if(txtView6==nil || txtView6==(id)[NSNull null]) return;
        
        float periodT = [[self getFieldDisplayWithFieldName:[NSString stringWithFormat:@"%@_Target__c", key] withDictionary: data] floatValue];
        float finalV = 100*periodVal/periodT;
        colorIcon = (UIImageView *)[cell viewWithTag:imgTag+3];
        colorIcon.hidden = YES;
        
        if(!isnan(finalV) && !isinf(finalV)){
            txtView6.text = [NSString stringWithFormat:@"%.1f%%", finalV];
            
//            if(periodVal>=periodT){
//                if(!flag) [colorIcon setImage: [UIImage imageNamed: @"green_up_arrow.png"]];
//                else [ colorIcon setImage:[UIImage imageNamed: @"green_down_arrow.png"]];
//            }
//            else {
//                if(!flag) [colorIcon setImage: [UIImage imageNamed: @"red_down_arrow.png"]];
//                else [ colorIcon setImage:[UIImage imageNamed: @"red_up_arrow.png"]];
//            }
            
        }else{
            txtView6.text = @"-";
        }
    }
}




-(void) addRowHiddenButtonToTableCell:(UITableViewCell*)cell referSelector:(SEL)selector{
    
    for(int i=1; i<=9; i++){
        UIButton* button = (UIButton*)[cell viewWithTag:78000+i];
        if(button!=nil){
            [button removeFromSuperview];
        }
    }
    
    UIButton* button = (UIButton*)[cell viewWithTag:78000];
    if(button==nil || button==(id)[NSNull null]){
        button = [UIButton buttonWithType:UIButtonTypeSystem];
        [button setBackgroundImage:nil forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageNamed:@"button_blue.png"] forState:UIControlStateHighlighted];
        button.tag = 78000;
        button.frame = CGRectMake(204, 0, 800, 50);
        [cell addSubview:button];
        [button setTitle:@"" forState:UIControlStateNormal];
    }
    [button removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
    [button addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
    
}

-(void) addCellHiddenButtonToTableCell:(UITableViewCell*)cell frame:(CGRect)rect tag:(int)tag referSelector:(SEL)selector{
    UIButton* button = (UIButton*)[cell viewWithTag:78000];
    if(button!=nil){
        [button removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
        [button removeFromSuperview];
    }
    
    button = (UIButton*)[cell viewWithTag:tag];
    if(button==nil || button==(id)[NSNull null]){
        button = [UIButton buttonWithType:UIButtonTypeSystem];
        [button setBackgroundImage:nil forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageNamed:@"button_blue.png"] forState:UIControlStateHighlighted];
        button.tag = tag;
        button.frame = CGRectMake(rect.origin.x - 20, rect.origin.y, rect.size.width+15, rect.size.height);
        [cell addSubview:button];
        [button setTitle:@"" forState:UIControlStateNormal];
    }

    [button addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
    
}

-(UITableViewCell*) updateTableCellForVisitorCountView:(NSDictionary*)data cell:(UITableViewCell*)cell{
    
    [self fillRawValueTableCell:cell labelWithTag:902 imageWithTag:951 data:data dataKey:@"Weekly_Visitor_Count" colorKey:W_PREF_VISITOR_COUNT format:@"%.2f" autoShortForm:YES];
    [self fillRawValueTableCell:cell labelWithTag:903 imageWithTag:952 data:data dataKey:@"Monthly_Visitor_Count" colorKey:M_PREF_VISITOR_COUNT format:@"%.2f" autoShortForm:YES];
    [self fillRawValueTableCell:cell labelWithTag:904 imageWithTag:953 data:data dataKey:@"Yearly_Visitor_Count" colorKey:Y_PREF_VISITOR_COUNT format:@"%.2f" autoShortForm:YES];
    [self addRowHiddenButtonToTableCell:cell referSelector:@selector(onVisitorPinClicked:)];
    return cell;
}

-(UITableViewCell*) updateTableCellForPelOffView:(NSDictionary*)data cell:(UITableViewCell*)cell{
    
    [self fillRawValueTableCell:cell labelWithTag:902 imageWithTag:951 data:data dataKey:@"Weekly_Pel_Off" colorKey:W_PREF_PEL_OFF format:@"%.2f%%" autoShortForm:NO scale:100];
    [self fillRawValueTableCell:cell labelWithTag:903 imageWithTag:952 data:data dataKey:@"Monthly_Pel_Off" colorKey:M_PREF_PEL_OFF format:@"%.2f%%" autoShortForm:NO scale:100];
    [self fillRawValueTableCell:cell labelWithTag:904 imageWithTag:953 data:data dataKey:@"Yearly_Pel_Off" colorKey:Y_PREF_PEL_OFF format:@"%.2f%%" autoShortForm:NO scale:100];
    [self addRowHiddenButtonToTableCell:cell referSelector:@selector(onPelOffPinClicked:)];
    return cell;
}

-(UITableViewCell*) updateTableCellForConvRateView:(NSDictionary*)data cell:(UITableViewCell*)cell{
    
    [self fillRawValueTableCell:cell labelWithTag:902 imageWithTag:951 data:data dataKey:@"Weekly_Conversion_Rate" colorKey:W_PREF_CONV_RATE format:@"%.2f%%" autoShortForm:NO scale:100];
    [self fillRawValueTableCell:cell labelWithTag:903 imageWithTag:952 data:data dataKey:@"Monthly_Conversion_Rate" colorKey:M_PREF_CONV_RATE format:@"%.2f%%" autoShortForm:NO scale:100];
    [self fillRawValueTableCell:cell labelWithTag:904 imageWithTag:953 data:data dataKey:@"Yearly_Conversion_Rate" colorKey:Y_PREF_CONV_RATE format:@"%.2f%%" autoShortForm:NO scale:100];
    [self addRowHiddenButtonToTableCell:cell referSelector:@selector(onConversionRatePinClicked:)];
    return cell;
}

-(UITableViewCell*) updateTableCellForUptView:(NSDictionary*)data cell:(UITableViewCell*)cell{
    
    [self fillRawValueTableCell:cell labelWithTag:902 imageWithTag:951 data:data dataKey:@"Weekly_UPT" colorKey:W_PREF_UPT format:@"%.2f" autoShortForm:YES];
    [self fillRawValueTableCell:cell labelWithTag:903 imageWithTag:952 data:data dataKey:@"Monthly_UPT" colorKey:M_PREF_UPT format:@"%.2f" autoShortForm:YES];
    [self fillRawValueTableCell:cell labelWithTag:904 imageWithTag:953 data:data dataKey:@"Yearly_UPT" colorKey:Y_PREF_UPT format:@"%.2f" autoShortForm:YES];
    [self addRowHiddenButtonToTableCell:cell referSelector:@selector(onUPTPinClicked:)];
    return cell;
}


-(UITableViewCell*) updateTableCellForAtvView:(NSDictionary*)data cell:(UITableViewCell*)cell{
    
    [self fillRawValueTableCell:cell labelWithTag:902 imageWithTag:951 data:data dataKey:@"Weekly_ATV" colorKey:W_PREF_ATV format:@"%.2f" autoShortForm:YES];
    [self fillRawValueTableCell:cell labelWithTag:903 imageWithTag:952 data:data dataKey:@"Monthly_ATV" colorKey:M_PREF_ATV format:@"%.2f" autoShortForm:YES];
    [self fillRawValueTableCell:cell labelWithTag:904 imageWithTag:953 data:data dataKey:@"Yearly_ATV" colorKey:Y_PREF_ATV format:@"%.2f" autoShortForm:YES];
    [self addRowHiddenButtonToTableCell:cell referSelector:@selector(onATVPinClicked:)];
    return cell;
}

-(UITableViewCell*) updateTableCellForSalesProdView:(NSDictionary*)data cell:(UITableViewCell*)cell{
    
    [self fillRawValueTableCell:cell labelWithTag:902 imageWithTag:951 data:data dataKey:@"Weekly_Sales_Productivity" colorKey:W_PREF_SALES_PROD format:@"%.2f" autoShortForm:YES];
    [self fillRawValueTableCell:cell labelWithTag:903 imageWithTag:952 data:data dataKey:@"Monthly_Sales_Productivity" colorKey:M_PREF_SALES_PROD format:@"%.2f" autoShortForm:YES];
    [self fillRawValueTableCell:cell labelWithTag:904 imageWithTag:953 data:data dataKey:@"Yearly_Sales_Productivity" colorKey:Y_PREF_SALES_PROD format:@"%.2f" autoShortForm:YES];
    [self addRowHiddenButtonToTableCell:cell referSelector:@selector(onSalesPerformancePinClicked:)];
    return cell;
}

-(UITableViewCell*) updateTableCellForDiscountView:(NSDictionary*)data cell:(UITableViewCell*)cell{
    
    [self fillRawValueTableCell:cell labelWithTag:902 imageWithTag:951 data:data dataKey:@"Weekly_Discount" colorKey:W_PREF_DISCOUNT format:@"%.2f%%" autoShortForm:NO scale:100 negativeComparing:YES skipTarget:NO];
    [self fillRawValueTableCell:cell labelWithTag:903 imageWithTag:952 data:data dataKey:@"Monthly_Discount" colorKey:M_PREF_DISCOUNT format:@"%.2f%%" autoShortForm:NO scale:100 negativeComparing:YES skipTarget:NO];
    [self fillRawValueTableCell:cell labelWithTag:904 imageWithTag:953 data:data dataKey:@"Yearly_Discount" colorKey:Y_PREF_DISCOUNT format:@"%.2f%%" autoShortForm:NO scale:100 negativeComparing:YES skipTarget:NO];
    [self addRowHiddenButtonToTableCell:cell referSelector:@selector(onDiscountPinClicked:)];
    return cell;
}


-(UITableViewCell*) updateTableCellForOpsSalesView:(NSDictionary*)data cell:(UITableViewCell*)cell{

    
    [self fillRawValueTableCell:cell labelWithTag:902 imageWithTag:951 data:data dataKey:@"Weekly_Ops_Sales" colorKey:W_PREF_OPS_SALES format:@"%.2f" autoShortForm:YES];
    [self fillRawValueTableCell:cell labelWithTag:903 imageWithTag:952 data:data dataKey:@"Monthly_Ops_Sales" colorKey:M_PREF_OPS_SALES format:@"%.2f" autoShortForm:YES];
    [self fillRawValueTableCell:cell labelWithTag:904 imageWithTag:953 data:data dataKey:@"Yearly_Ops_Sales" colorKey:Y_PREF_OPS_SALES format:@"%.2f" autoShortForm:YES];
    [self addRowHiddenButtonToTableCell:cell referSelector:@selector(onOpsSalesPinClicked:)];
    return cell;
}


-(UITableViewCell*) updateTableCellForTransactionView:(NSDictionary*)data cell:(UITableViewCell*)cell{
    [self fillRawValueTableCell:cell labelWithTag:902 imageWithTag:951 data:data dataKey:@"Weekly_Transaction_Count" colorKey:W_PREF_TXN format:@"%.0f" autoShortForm:YES];
    [self fillRawValueTableCell:cell labelWithTag:903 imageWithTag:952 data:data dataKey:@"Monthly_Transaction_Count" colorKey:M_PREF_TXN format:@"%.0f" autoShortForm:YES];
    [self fillRawValueTableCell:cell labelWithTag:904 imageWithTag:953 data:data dataKey:@"Yearly_Transaction_Count" colorKey:Y_PREF_TXN format:@"%.0f" autoShortForm:YES];
    [self addRowHiddenButtonToTableCell:cell referSelector:@selector(onTransactionPinClicked:)];
    return cell;
}


-(void) onShowInventoryView:(int)index{
    NSLog(@"$1Open Page: onShowInventoryView");
    NSDictionary* storeData = [self.recordAry objectAtIndex:index];
    InventoryViewController* vc = [[InventoryViewController alloc] initWithDictionary:storeData] ;
    [self.navigationController pushViewController: vc animated:YES];
    [vc release];
}

-(void) onShowStoreInfo:(int)index{
    NSLog(@"#2Open Page: onShowStoreInfo");
    NSDictionary* storeData = [self.recordAry objectAtIndex:index];
    StoreInfoViewController* vc = [[StoreInfoViewController alloc] initWithId:[storeData objectForKey:@"Id"]] ;
    [self.navigationController pushViewController: vc animated:YES];
    [vc release];
}

#pragma mark - Advance Page redirecting for clicking on Color Pin
-(void) onOpsSalesPinClicked:(id)sender{
    if(self.viewMode == VIEWMODE_DS){
        [self onSelectedUserRow: [GeneralUiTool getParentTableCellTag:(UIView *)sender]];
        return;
    }
    NSLog(@"#3Open Page: Ops Sales");
    NSDictionary* data = self.recordAry[ [GeneralUiTool getParentTableCellTag:(UIView *)sender]];
    [self.navigationController pushViewController:[[ [SalesPerformanceViewController alloc] initWithStoreID:[data objectForKey:@"Id"] name:[data objectForKey:@"Name"] code:[data objectForKey:@"Store_Code__c"] defaultPage: CARD_OPS_SALES] autorelease] animated:YES];
}

-(void) onDiscountPinClicked:(id)sender{
    if(self.viewMode == VIEWMODE_DS){
        [self onSelectedUserRow: [GeneralUiTool getParentTableCellTag:(UIView *)sender]];
        return;
    }

    NSLog(@"#3Open Page: Discount");
    NSDictionary* data = self.recordAry[ [GeneralUiTool getParentTableCellTag:(UIView *)sender]];
    [self.navigationController pushViewController:[[ [SalesPerformanceViewController alloc] initWithStoreID:[data objectForKey:@"Id"] name:[data objectForKey:@"Name"] code:[data objectForKey:@"Store_Code__c"] defaultPage: CARD_DISCOUNT] autorelease] animated:YES];
}

-(void) onSalesPerformancePinClicked:(id)sender{
    if(self.viewMode == VIEWMODE_DS){
        [self onSelectedUserRow: [GeneralUiTool getParentTableCellTag:(UIView *)sender]];
        return;
    }

    NSLog(@"#4Open Page: Sales Perofrmance");
    NSDictionary* data = self.recordAry[ [GeneralUiTool getParentTableCellTag:(UIView *)sender]];
    [self.navigationController pushViewController:[[ [SalesPerformanceViewController alloc] initWithStoreID:[data objectForKey:@"Id"] name:[data objectForKey:@"Name"] code:[data objectForKey:@"Store_Code__c"] defaultPage: CARD_SALES_PRODUCTIVITY] autorelease] animated:YES];

}

-(void) onATVPinClicked:(id)sender{
    if(self.viewMode == VIEWMODE_DS){
        [self onSelectedUserRow: [GeneralUiTool getParentTableCellTag:(UIView *)sender]];
        return;
    }

    NSLog(@"#5Open Page: ATV");
    NSDictionary* data = self.recordAry[ [GeneralUiTool getParentTableCellTag:(UIView *)sender]];
    [self.navigationController pushViewController:[[ [SalesPerformanceViewController alloc] initWithStoreID:[data objectForKey:@"Id"] name:[data objectForKey:@"Name"] code:[data objectForKey:@"Store_Code__c"] defaultPage: CARD_ATV] autorelease] animated:YES];

}

-(void) onUPTPinClicked:(id)sender{
    if(self.viewMode == VIEWMODE_DS){
        [self onSelectedUserRow: [GeneralUiTool getParentTableCellTag:(UIView *)sender]];
        return;
    }

    NSLog(@"#6Open Page: UPT");
    NSDictionary* data = self.recordAry[ [GeneralUiTool getParentTableCellTag:(UIView *)sender]];
    [self.navigationController pushViewController:[[ [SalesPerformanceViewController alloc] initWithStoreID:[data objectForKey:@"Id"] name:[data objectForKey:@"Name"] code:[data objectForKey:@"Store_Code__c"] defaultPage: CARD_UPT] autorelease] animated:YES];

}

-(void) onVisitorPinClicked:(id)sender{
    if(self.viewMode == VIEWMODE_DS){
        [self onSelectedUserRow: [GeneralUiTool getParentTableCellTag:(UIView *)sender]];
        return;
    }

    NSLog(@"#7Open Page: Visitor");
    NSDictionary* data = self.recordAry[ [GeneralUiTool getParentTableCellTag:(UIView *)sender]];
    [self.navigationController pushViewController:[[ [SalesPerformanceViewController alloc] initWithStoreID:[data objectForKey:@"Id"] name:[data objectForKey:@"Name"] code:[data objectForKey:@"Store_Code__c"] defaultPage: CARD_VISITOR_COUNT] autorelease] animated:YES];

}

-(void) onPelOffPinClicked:(id)sender{
    if(self.viewMode == VIEWMODE_DS){
        [self onSelectedUserRow: [GeneralUiTool getParentTableCellTag:(UIView *)sender]];
        return;
    }

    NSLog(@"#8Open Page: Peel");
    NSDictionary* data = self.recordAry[ [GeneralUiTool getParentTableCellTag:(UIView *)sender]];
    [self.navigationController pushViewController:[[ [SalesPerformanceViewController alloc] initWithStoreID:[data objectForKey:@"Id"] name:[data objectForKey:@"Name"] code:[data objectForKey:@"Store_Code__c"] defaultPage:CARD_PEL_OFF] autorelease]  animated:YES];
}

-(void) onConversionRatePinClicked:(id)sender{
    if(self.viewMode == VIEWMODE_DS){
        [self onSelectedUserRow: [GeneralUiTool getParentTableCellTag:(UIView *)sender]];
        return;
    }

    NSLog(@"#9Open Page: CR");
    NSDictionary* data = self.recordAry[ [GeneralUiTool getParentTableCellTag:(UIView *)sender]];
    [self.navigationController pushViewController:[[ [SalesPerformanceViewController alloc] initWithStoreID:[data objectForKey:@"Id"] name:[data objectForKey:@"Name"] code:[data objectForKey:@"Store_Code__c"] defaultPage:CARD_CONVERSION_RATE] autorelease] animated:YES];
}

-(void) onTransactionPinClicked:(id)sender{
    if(self.viewMode == VIEWMODE_DS){
        [self onSelectedUserRow: [GeneralUiTool getParentTableCellTag:(UIView *)sender]];
        return;
    }

    NSLog(@"#9Open Page: TXN");
    NSDictionary* data = self.recordAry[ [GeneralUiTool getParentTableCellTag:(UIView *)sender]];
    [self.navigationController pushViewController:[[ [SalesPerformanceViewController alloc] initWithStoreID:[data objectForKey:@"Id"] name:[data objectForKey:@"Name"] code:[data objectForKey:@"Store_Code__c"] defaultPage:CARD_TRANSACTION_COUNT] autorelease] animated:YES];
}

-(void) onSelectedUserRow:(NSInteger)index{
    NSString* aeID = self.recordAry[index][@"Store_Code__c"];//at this moment, Store_Code__c is aeID
    NSString* name = self.recordAry[index][@"Name"];
    [self.userFilterButton setTitle:name forState:UIControlStateNormal];
    self.selectedUserVal = [aeID lowercaseString];

    [self switchViewMode: VIEWMODE_STORE];
    [self refreshMode];
    [self refreshTableData];
}


#pragma mark - Threshold Setting

-(void) saveAndReloadThreshold{
}

-(void) showPreferencePopup{
}
-(void) hidePreferencePopup{
}

-(void) showFilterPopup{
    [self.view addSubview: self.filterPopupView];
}
-(void) hideFilterPopup{
    [self.filterPopupView removeFromSuperview];
}



#pragma mark - Handle Click event of Back button & Reload button
- (IBAction)onBackButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)onHomeButtonClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (IBAction)onReloadButtonClicked:(id)sender {
    [self showFilterPopup];
}


- (IBAction)onThreshouldSettingClicked:(id)sender {
    [self showPreferencePopup];
}


- (IBAction)onUserButtonClicked:(id)sender {
    if(self.userPopup==nil){
        UIView* view = (UIView*) sender;
        UserSelectionViewController* vc = [[UserSelectionViewController alloc ] initWithUserArray: self.userArray];
        vc.delegate = self;
        self.userPopup = [[[UIPopoverController alloc] initWithContentViewController:vc] autorelease];
        self.userPopup.delegate = self;
        [self.userPopup presentPopoverFromRect:view.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}



- (IBAction)onMapButtonClicked:(id)sender {
    BOOL isValid = NO;

    for ( int i = 0; i < [self.recordAry count]; i++ ){
        NSDictionary *dataDict = [[[NSDictionary alloc] initWithDictionary:[self.recordAry objectAtIndex:i]] autorelease];
        if ( [dataDict objectForKey:@"emfa__Primary_Latitude__c"] != nil &&
            [dataDict objectForKey:@"emfa__Primary_Latitude__c"] != (id)[NSNull null] && [dataDict objectForKey:@"emfa__Primary_Longitude__c"] != nil &&
            [dataDict objectForKey:@"emfa__Primary_Longitude__c"] != (id)[NSNull null] ){
            isValid = YES;
            StoreMapViewController* vc = [[StoreMapViewController alloc] initWithStoreDataArray:self.recordAry mode:W_PREF_OPS_SALES];
            [self.navigationController pushViewController:vc  animated:YES];
            [vc release];
            return;
        }
    }
    
    [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"Error" message:@"No address information available for all stores."];
    
}


//select ATV, UPT, Weekly Summary, etc
- (IBAction)onModeButtonClicked:(id)sender {
    if(self.modePopup==nil){
        UIView* view = (UIView*) sender;
        StoreAnalyticSelectionViewController* vc = [[StoreAnalyticSelectionViewController alloc ] initWithNibName:@"StoreAnalyticSelectionViewController" bundle:nil];
        vc.delegate = self;
        self.modePopup = [[[UIPopoverController alloc] initWithContentViewController:vc] autorelease];
        self.modePopup.delegate = self;
        [self.modePopup presentPopoverFromRect:view.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];

    }
}


//select Store or DS
- (IBAction)onGroupButtonClicked:(id)sender {
    self.selectedUserVal = nil;
    [self.userFilterButton setTitle: [ShareLocale textFromKey:@"all"] forState:UIControlStateNormal];

    [self switchViewMode];
    [self refreshMode];
}

- (IBAction)onCompStatusButtonClicked:(id)sender{
    if(self.compStatusPopup==nil){
        UIView* view = (UIView*) sender;
        GeneralSelectionPopup* vc = [[GeneralSelectionPopup alloc ] initAsMultiSelectTableWithDataArray:self.compStatusArray selectedArray:self.selectedCompStatusValArray];
        vc.delegate = self;
        self.compStatusPopup = [[[UIPopoverController alloc] initWithContentViewController:vc] autorelease];
        self.compStatusPopup.delegate = self;
        [self.compStatusPopup presentPopoverFromRect:view.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}

//select Store type
- (IBAction)onStoreTypeButtonClicked:(id)sender {
    if(self.storeTypePopup==nil){
        
        UIView* view = (UIView*) sender;
        GeneralSelectionPopup* vc = [[GeneralSelectionPopup alloc ] initAsMultiSelectTableWithDataArray: self.storeTypeArray selectedArray:self.selectedStoreTypeValArray];
        vc.delegate = self;
        vc.supportMultiSelection = YES;
        self.storeTypePopup = [[[UIPopoverController alloc] initWithContentViewController:vc] autorelease];
        self.storeTypePopup.delegate = self;
        [self.storeTypePopup presentPopoverFromRect:view.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }

}


- (IBAction)onPreferenceCancelButtonClicked:(id)sender {
    [self hidePreferencePopup];
}

- (IBAction)onPreferenceSaveButtonClicked:(id)sender {
    [self hidePreferencePopup];
    [self saveAndReloadThreshold];
}

- (IBAction)onFilterCancelButtonCliked:(id)sender {
    [self hideFilterPopup];
}

- (IBAction)onFilterReloadButtonClicked:(id)sender {
    [self hideFilterPopup];
    [self refreshTableData];
}



#pragma mark - Popup delegate
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    if(self.modePopup!=nil){
        self.modePopup = nil;
    }
    else if(self.storeTypePopup!=nil){
        self.storeTypePopup = nil;
    }
    else if(self.compStatusPopup!=nil){
        self.compStatusPopup = nil;
    }
    else if(self.userPopup!=nil){
        self.userPopup = nil;
    }
}

-(void) onSelectedMode:(NSDictionary*)data{
    if(self.modePopup ){
        int mode = [[data objectForKey:@"Value"] intValue];
        _mode = mode;
        [self.modePopup dismissPopoverAnimated:YES];
        self.modePopup = nil;
        [self refreshMode];
    }
}

-(void) onSelectedUser:(NSDictionary*)userData{
    if(self.userPopup ){
        //userData
        self.selectedUserVal = [[userData objectForKey:@"Value"] lowercaseString];
        if([self.selectedUserVal isEqualToString:@""]){
            self.selectedUserVal = nil;
            [self.userFilterButton setTitle:[ShareLocale textFromKey: @"user_name"] forState:UIControlStateNormal];
        }else{
            NSString* labelName = [userData objectForKey:@"Label"];
            if(labelName==(id)[NSNull null] || labelName==nil){
                labelName = @"-";
            }
            [self.userFilterButton setTitle:labelName forState:UIControlStateNormal];
        }
        [self.userPopup dismissPopoverAnimated:YES];
        self.userPopup = nil;
        [self refreshMode];
    }
}


-(void) onMultiRowSelected:(NSArray*)indexPathArray{
    if(self.storeTypePopup!=nil){
        self.selectedStoreTypeValArray = [NSMutableArray array];
        for(NSIndexPath* index in indexPathArray){
            [self.selectedStoreTypeValArray addObject: self.storeTypeArray[index.row][@"val"] ];
        }
        
        NSString* labelName;
        if([self.selectedStoreTypeValArray count]>0){
            labelName = [NSString stringWithFormat:@"%ld selected", (long)[self.selectedStoreTypeValArray count]];
        }else{
            labelName = [ShareLocale textFromKey: @"Store Type"];
        }
        [self.storeTypeButton setTitle:labelName forState:UIControlStateNormal];
        self.storeTypePopup = nil;
        [self refreshMode];
    }
    else if(self.compStatusPopup!=nil){
        self.selectedCompStatusValArray = [NSMutableArray array];
        for(NSIndexPath* index in indexPathArray){
            [self.selectedCompStatusValArray addObject: self.compStatusArray[index.row][@"val"] ];
        }
        NSString* labelName;
        if([self.selectedCompStatusValArray count]>0){
            labelName = [NSString stringWithFormat:@"%ld selected", (long)[self.selectedCompStatusValArray count]];
        }else{
            labelName = [ShareLocale textFromKey: @"Comp Status"];
        }

        [self.compStatusButton setTitle:labelName forState:UIControlStateNormal];
        self.compStatusPopup = nil;
        [self refreshMode];
        
    }
}


-(void) onRowSelected:(int) index{
//    if(self.storeTypePopup!=nil){
//
//        self.selectedStoreTypeVal = [self.storeTypeArray objectAtIndex:index][@"val"];
//        NSString* labelName = [self.storeTypeArray objectAtIndex:index][@"label"];
//        if([self.selectedStoreTypeVal isEqualToString:@""]){
//            self.selectedStoreTypeVal = nil;
//            labelName = [ShareLocale textFromKey: @"Store Type"];
//        }
//        [self.storeTypeButton setTitle:labelName forState:UIControlStateNormal];
//        [self.storeTypePopup dismissPopoverAnimated:YES];
//        self.storeTypePopup = nil;
//        [self refreshMode];
//    }
//    else if(self.compStatusPopup!=nil){
//        
//        self.selectedCompStatusVal = [self.compStatusArray objectAtIndex:index][@"val"];
//        NSString* labelName = [self.compStatusArray objectAtIndex:index][@"label"];
//        if([self.selectedCompStatusVal isEqualToString:@""]){
//            self.selectedCompStatusVal = nil;
//            labelName = [ShareLocale textFromKey: @"Comp Status"];
//        }
//        [self.compStatusButton setTitle:labelName forState:UIControlStateNormal];
//        [self.compStatusPopup dismissPopoverAnimated:YES];
//        self.compStatusPopup = nil;
//        [self refreshMode];
//        
//    }
}




@end