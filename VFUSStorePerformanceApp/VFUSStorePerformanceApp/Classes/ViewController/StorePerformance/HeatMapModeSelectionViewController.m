//
//  HeatMapModeSelectionViewController.m
//  VFStorePerformanceApp_Billy
//
//  Created by Developer 2 on 2/4/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "HeatMapModeSelectionViewController.h"
#import "BusinessLogicHelper.h"
#import "ShareLocale.h"

@interface HeatMapModeSelectionViewController ()

@end

@implementation HeatMapModeSelectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    selectedPeriodIndex = -1;
    
    self.recordArray = @[
//                         @{@"Label":@"Weekly APT", @"Name": W_PREF_APT},
//                         @{@"Label":@"Weekly ATV", @"Name": W_PREF_ATV},
                         @{@"Label":[ShareLocale textFromKey:@"weekly"], @"Name": @0},
                         @{@"Label":[ShareLocale textFromKey:@"monthly"], @"Name": @1},
                         @{@"Label":[ShareLocale textFromKey:@"yearly"], @"Name": @2},
//                         @{@"Label":@"Weekly Sales Amount", @"Name": W_PREF_SALES_AMT},
//                         @{@"Label":@"Weekly UPT", @"Name": W_PREF_UPT},
//                         @{@"Label":@"Weekly Visitor Count", @"Name": W_PREF_VISITOR_COUNT},
                         ];
    self.subRecordArray = @[
                            @{@"Label":[ShareLocale textFromKey:@"ops_sales"], @"Name": @CARD_OPS_SALES},
                            @{@"Label":[ShareLocale textFromKey:@"discount"], @"Name": @CARD_DISCOUNT},
                            @{@"Label":[ShareLocale textFromKey:@"store_prod"], @"Name": @CARD_SALES_PRODUCTIVITY},
                            @{@"Label":[ShareLocale textFromKey:@"atv"], @"Name": @CARD_ATV},
                            @{@"Label":[ShareLocale textFromKey:@"upt"], @"Name": @CARD_UPT},
                            @{@"Label":[ShareLocale textFromKey:@"transaction_count"], @"Name": @CARD_TRANSACTION_COUNT},
                            @{@"Label":[ShareLocale textFromKey:@"visitor_count"], @"Name": @CARD_VISITOR_COUNT},
                            @{@"Label":[ShareLocale textFromKey:@"peel_off"], @"Name": @CARD_PEL_OFF},
                            @{@"Label":[ShareLocale textFromKey:@"conversion_rate"], @"Name": @CARD_CONVERSION_RATE},
                            
                            ];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView reloadData];
    
    self.detailTableView.delegate = self;
    self.detailTableView.dataSource = self;
    [self.detailTableView reloadData];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [_tableView release];
    [_detailTableView release];
    [super dealloc];
}



#pragma mark - Table view datasource/delegate

-(float) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(self.tableView == tableView){
        return [self.recordArray count];
    }else if(self.detailTableView == tableView){
        if(selectedPeriodIndex>=0){
            return [self.subRecordArray count];
        }else{
            return 0;
        }
    }else{
        return 0;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(self.tableView == tableView){
        selectedPeriodIndex = indexPath.row;
        [self.detailTableView reloadData];
    }
    else if(tableView == self.detailTableView){
        NSDictionary* mainData = [self.recordArray objectAtIndex: selectedPeriodIndex];
        NSDictionary* data = [self.subRecordArray objectAtIndex:indexPath.row];
        
        if(self.delegate!=nil){
            [self.delegate onSelectdMode: @{@"Label":[NSString stringWithFormat:@"%@ %@", [mainData objectForKey:@"Label"], [data objectForKey:@"Label"]], @"Period": [NSNumber numberWithInt:selectedPeriodIndex], @"Type":[data objectForKey:@"Name"]}];
        }
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tview cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    
    if(tview == self.tableView){
        NSDictionary* data = [self.recordArray objectAtIndex:indexPath.row];

        cell = [tview dequeueReusableCellWithIdentifier:@"HeatMapCell_Period"];
        if(cell == nil){
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HeatMapCell_Period"] autorelease];
//            UIView *selectionColor = [[UIView alloc] init];
//            selectionColor.backgroundColor = [UIColor colorWithRed:(245/255.0) green:(245/255.0) blue:(245/255.0) alpha:.2f];
//            cell.selectedBackgroundView = selectionColor;
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
            
            cell.textLabel.hidden = YES;
//            cell.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
            
            UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(20, 5, 300, 40)];
            label.tag = 901;
            label.text = @"";
            [cell addSubview:label];
        }
        
        UILabel* dateLabel = (UILabel*)[cell viewWithTag:901];
        dateLabel.text = [data objectForKey:@"Label"];
        cell.tag = indexPath.row;
        return cell;
    }else{
        NSDictionary* data = [self.subRecordArray objectAtIndex:indexPath.row];

        cell = [tview dequeueReusableCellWithIdentifier:@"HeatMapCell_Data"];
        if(cell == nil){
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HeatMapCell_Data"] autorelease];
            UIView *selectionColor = [[UIView alloc] init];
            selectionColor.backgroundColor = [UIColor colorWithRed:(245/255.0) green:(245/255.0) blue:(245/255.0) alpha:.2f];
            cell.selectedBackgroundView = selectionColor;
            
            cell.textLabel.hidden = YES;
            cell.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
            
            UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(20, 5, 300, 40)];
            label.tag = 901;
            label.text = @"";
            [cell addSubview:label];
        }
        
        UILabel* dateLabel = (UILabel*)[cell viewWithTag:901];
        dateLabel.text = [data objectForKey:@"Label"];
        cell.tag = indexPath.row;
        return cell;
    }
}


@end
