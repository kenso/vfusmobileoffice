//
//  SalesPerformanceViewController.h
//  VFStorePerformanceApp
//
//  Created by Developer 2 on 5/3/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "BasicViewController.h"
#import "CorePlot-CocoaTouch.h"
#import "SalesPerformanceSelectionViewController.h"


//StorePerformanceSelectionViewControllerDelegate,
@interface SalesPerformanceViewController : BasicViewController< UIPopoverControllerDelegate, DAODelegate, StoreSelectionViewControllerDelegate>{
    UIView* curDashboardView;
    int defaultIndex;
}

@property (retain, nonatomic) IBOutlet UILabel *fxDateLabel;

@property (retain, nonatomic) NSArray* nonVipGrades;
@property (retain, nonatomic) NSString* fxDate;
@property (assign, nonatomic) int fxMonth;
@property (assign, nonatomic) int fxWeek;
@property (assign, nonatomic) int fxYear;


@property (retain, nonatomic) NSMutableArray* catgenArray;

@property (retain, nonatomic) NSMutableArray* categoryArray;
@property (retain, nonatomic) NSMutableArray* genderArray;

@property (retain, nonatomic) NSMutableArray* vipGradeArray;

//@property (retain, nonatomic) NSString* categoryFilter;
//@property (retain, nonatomic) NSString* genderFilter;

@property (retain, nonatomic) NSMutableArray* selectedFilterOptions;

//sales mix page
@property (retain, nonatomic) NSMutableArray* selectedGlobalSalesMixOptions;
@property (retain, nonatomic) NSMutableArray* selectedDailySalesMixOptions;
@property (retain, nonatomic) NSMutableArray* selectedWeeklySalesMixOptions;
@property (retain, nonatomic) NSMutableArray* selectedMonthlySalesMixOptions;
@property (retain, nonatomic) NSMutableArray* selectedDailyQuantityMixOptions;
@property (retain, nonatomic) NSMutableArray* selectedWeeklyQuantityMixOptions;
@property (retain, nonatomic) NSMutableArray* selectedMonthlyQuantityMixOptions;

//vip page
@property (retain, nonatomic) NSMutableArray* selectedGlobalVIPInfoOptions;
@property (retain, nonatomic) NSMutableArray* selectedDailyVIPCountOptions;
@property (retain, nonatomic) NSMutableArray* selectedWeeklyVIPCountOptions;
@property (retain, nonatomic) NSMutableArray* selectedMonthlyVIPCountOptions;
@property (retain, nonatomic) NSMutableArray* selectedDailyVIPTxnCountOptions;
@property (retain, nonatomic) NSMutableArray* selectedWeeklyVIPTxnCountOptions;
@property (retain, nonatomic) NSMutableArray* selectedMonthlyVIPTxnCountOptions;
@property (retain, nonatomic) NSMutableArray* selectedDailyVIPSalesOptions;
@property (retain, nonatomic) NSMutableArray* selectedWeeklyVIPSalesOptions;
@property (retain, nonatomic) NSMutableArray* selectedMonthlyVIPSalesOptions;

@property (retain, nonatomic) NSDictionary* vipCodeLabelMap;

@property (retain, nonatomic) NSDictionary* storeData;
@property (retain, nonatomic) NSDictionary* allChartData;
@property (retain, nonatomic) NSDictionary* allChartDataExtra;
@property (assign, nonatomic) int tabIndex;

@property (retain, nonatomic) DAOBaseRequest* request;
@property (retain, nonatomic) DAOBaseRequest* compareStoreRequest;

@property (retain, nonatomic) UIPopoverController* selectionPopup;
@property (retain, nonatomic) UIPopoverController* comparePopup;

@property (retain, nonatomic) IBOutlet UIButton *compareButton;

@property (retain, nonatomic) NSArray* tabArray;
@property (retain, nonatomic) IBOutlet UIButton *tab1;
@property (retain, nonatomic) IBOutlet UIButton *tab2;
@property (retain, nonatomic) IBOutlet UIButton *tab3;
@property (retain, nonatomic) IBOutlet UIButton *tab4;
@property (retain, nonatomic) IBOutlet UIButton *tab7;
@property (retain, nonatomic) IBOutlet UIButton *tab8;
@property (retain, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (retain, nonatomic) IBOutlet UIView *tabContentView;
@property (retain, nonatomic) IBOutlet UIScrollView *contentScrollView;
@property (retain, nonatomic) IBOutlet UIView *tab1View;
@property (retain, nonatomic) IBOutlet UIView *tab2View;
@property (retain, nonatomic) IBOutlet UIView *tab3View;
@property (retain, nonatomic) IBOutlet UIView *tab4View;
@property (retain, nonatomic) IBOutlet UIView *tab7View;
@property (retain, nonatomic) IBOutlet UIView *tab8View;

@property (retain, nonatomic) IBOutlet UIButton *salesMixChangeButton;
@property (retain, nonatomic) IBOutlet UIButton *vipinfoChangeButton;


-(id)initWithStoreID:(NSString*)sid name:(NSString*)name code:(NSString*)code;
-(id)initWithStoreID:(NSString*)sid name:(NSString*)name code:(NSString*)code defaultPage:(int)cardIndex;

//-(id)initWithData:(NSDictionary*)data;
//-(id)initWithData:(NSDictionary*)data defaultPage:(int)cardIndex;

-(IBAction) onChangeGlobalOptionButtonClicked:(id)sender;


- (IBAction) onChangeOptionButtonClicked:(id)sender;
- (IBAction)onTabSwitched:(id)sender;
- (IBAction)onBackButtonClicked:(id)sender ;
- (IBAction)onHomeButtonClicked:(id)sender;
- (IBAction)onCompareButtonClicked:(id)sender;

@end
