//
//  SalesPerformanceViewController.m
//  VFStorePerformanceApp
//
//  Created by Developer 2 on 5/3/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//
#import "ShareLocale.h"
#import "SalesPerformanceViewController.h"
#import "SalesPerformanceSelectionViewController.h"
#import "CorePlot-CocoaTouch.h"
#import "BusinessLogicHelper.h"
#import "GeneralUiTool.h"
#import "VFDAO.h"
#import "Constants.h"
#import "ChartLine.h"
#import <math.h>




#define HEADER_DATE_VALUE_TARGET [ShareLocale textFromKey:@"analysis_date_target"]
#define HEADER_WEEK_VALUE_TARGET [ShareLocale textFromKey:@"analysis_week_target"]
#define HEADER_MONT_VALUE_TARGET [ShareLocale textFromKey:@"analysis_month_target"]


#define HEADER_DATE_VALUE_TOTAL [ShareLocale textFromKey:@"analysis_date_total"]
#define HEADER_WEEK_VALUE_TOTAL [ShareLocale textFromKey:@"analysis_week_total"]
#define HEADER_MONT_VALUE_TOTAL [ShareLocale textFromKey:@"analysis_month_total"]


@interface SalesPerformanceViewController ()

@end

@implementation SalesPerformanceViewController

-(id)initWithStoreID:(NSString*)sid name:(NSString*)name code:(NSString*)code defaultPage:(int)cardIndex{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        self.storeData = @{
                           @"Id":sid,
                           @"Name":name,
                           @"code": code
                           };
        if(cardIndex==CARD_OPS_SALES || cardIndex==CARD_DISCOUNT || cardIndex==CARD_SALES_PRODUCTIVITY){ //SALES INFO
            defaultIndex = 5;
        }
        else if (cardIndex==CARD_ATV || cardIndex==CARD_UPT || cardIndex==CARD_TRANSACTION_COUNT){//TXN INFO
            defaultIndex = 0;
        }
        else if (cardIndex==CARD_VISITOR_COUNT || cardIndex==CARD_PEL_OFF || cardIndex==CARD_CONVERSION_RATE){//VISITOR INFO
            defaultIndex = 1;
        }
        else{
            defaultIndex = 5;
        }

    }
    return self;
}

-(id)initWithStoreID:(NSString*)sid name:(NSString*)name code:(NSString*)code{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        self.storeData = @{
                    @"Id":sid,
                    @"Name":name,
                    @"code": code
        };
        defaultIndex = 5;
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.catgenArray = [NSMutableArray array];
    self.categoryArray = [NSMutableArray array];
    self.genderArray = [NSMutableArray array];
    
    self.vipGradeArray = [NSMutableArray array];
    
    self.tabArray = @[_tab1, _tab2, _tab3, _tab4, _tab7, _tab8];
    
    self.tabIndex = defaultIndex;
    
    
    //PILOT_CHANGE
    //self.compareButton.hidden = YES;
    [self.compareButton setTitle:[ShareLocale textFromKey:@"analysis_compare"] forState:UIControlStateNormal];
    
    [self loadStoreChartData];
    
    NSString* storeId = [self.storeData objectForKey:@"Id"];

    [self initCommentSidebarWithStoreId: storeId!=nil?storeId:[self.storeData objectForKey:@"store_id"]];
    
    [self refreshLocale];
    
}

-(void) refreshLocale{
    
    self.headerTitleLabel.text = [NSString stringWithFormat: [ShareLocale textFromKey:@"analysis_title"], [self.storeData objectForKey:@"Name"]];
    
    [self.tab8 setTitle: [ShareLocale textFromKey:@"tab_sales_info"] forState:UIControlStateNormal];
    [self.tab1 setTitle: [ShareLocale textFromKey:@"tab_txn_info"] forState:UIControlStateNormal];
    [self.tab2 setTitle: [ShareLocale textFromKey:@"tab_visitor_info"] forState:UIControlStateNormal];
    [self.tab3 setTitle: [ShareLocale textFromKey:@"tab_vip_info"] forState:UIControlStateNormal];
    [self.tab7 setTitle: [ShareLocale textFromKey:@"tab_salesmix_info"] forState:UIControlStateNormal];
    
    
    
    [self.vipinfoChangeButton setTitle:[ShareLocale textFromKey:@"change"] forState:UIControlStateNormal];
    [self.salesMixChangeButton setTitle:[ShareLocale textFromKey:@"change"] forState:UIControlStateNormal];

    
}


-(void) viewWillDisappear:(BOOL)animated{
    NSDate* now = [NSDate date];
    NSLog(@"viewWillDisappear = %f", (double)[now timeIntervalSince1970]);
    
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.contentScrollView flashScrollIndicators];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    
    [_catgenArray release];
    [_categoryArray release];
    [_genderArray release];
    [_vipGradeArray release];
    
    [_fxDate release];
//    [_categoryFilter release];
//    [_genderFilter release];
    [_storeData release];
    [_allChartData release];
    [_selectionPopup release];
    [_comparePopup release];
    [_tabArray release];
    
    curDashboardView = nil;
    [_tab1 release];
    [_tab2 release];
    [_tab3 release];
    [_tab4 release];
    [_tab7 release];
    [_tab8 release];

    [_tab1View release];
    [_tab2View release];
    [_tab3View release];
    [_tab4View release];
    [_tab7View release];
    [_tab8View release];
    
    [_tabContentView release];
    [_headerTitleLabel release];
    [_contentScrollView release];
    [_fxDateLabel release];
    [_compareButton release];
    
    
    [_request release];
    [_compareStoreRequest release];
    
    [_salesMixChangeButton release];
    [_vipinfoChangeButton release];
    [super dealloc];
}


-(void) loadStoreChartData{
    [self showLoadingPopup:self.view];
    NSString* storeId = [self.storeData objectForKey:@"Id"];
    self.request = [[VFDAO sharedInstance] selectStoreChartDataWithDelegate:self storeId: storeId!=nil?storeId:[self.storeData objectForKey:@"store_id"]];
    
}


-(void) loadComparableStoreChartDataWithStoreId:(NSString*)storeId{
    [self showLoadingPopup:self.view];
    self.compareStoreRequest = [[VFDAO sharedInstance] selectStoreChartDataWithDelegate:self storeId: storeId];
    
}



-(void)daoRequest:(DAOBaseRequest*)request queryOnlineDataSuccess:(NSArray*) response{
    if(self.request == request){
        self.allChartData = [response firstObject];
        self.fxYear = [[self.allChartData objectForKey:@"fxYear"] intValue];
        self.fxMonth = [[self.allChartData objectForKey:@"fxMonth"] intValue];
        self.fxWeek = [[self.allChartData objectForKey:@"fxWeek"] intValue];
        self.fxDate = [self.allChartData objectForKey:@"fxDate"];
        
        self.vipCodeLabelMap = [[self.allChartData objectForKey:@"vipCodeLabelMap"] mutableCopy];

        self.nonVipGrades = [self.allChartData objectForKey:@"nonVIPGrades"];
        if(self.nonVipGrades==nil || self.nonVipGrades==(id)[NSNull null]) self.nonVipGrades = @[];
        
        
        [self reloadContent];
    }
    else if (self.compareStoreRequest == request){
        self.allChartDataExtra = [response firstObject];
        [self reloadContent];
    }
    else{
        NSAssert(NO, @"SalesPerformanceViewController >> queryOnlineDataSuccess >> request not handled");
    }
    [self hideLoadingPopupWithFadeout];

}




-(void)daoRequest:(DAOBaseRequest*)request queryOnlineDataError:(NSString*) response code:(int)code{
    if(self.request == request){
        [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"Network Error" message:@"Reload?" yesHanlder:^(id data) {
            [self loadStoreChartData];
        } noHandler:^(id data) {
            
        }];
    }
    else if (self.compareStoreRequest == request){
        [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"Network Error" message:@"Reload?" yesHanlder:^(id data) {
            [self loadStoreChartData];
        } noHandler:^(id data) {
            
        }];
    }
    else{
        NSAssert(NO, @"SalesPerformanceViewController >> queryOnlineDataSuccess >> request not handled");
    }

    [self hideLoadingPopupWithFadeout];
    
}



#pragma mark - CompareButton/FilterSelectButton Popup delegate

- (IBAction)onCompareButtonClicked:(id)sender {
    NSUserDefaults *localCache = [NSUserDefaults standardUserDefaults];
    NSString* userPreferBrandCode = [localCache objectForKey: USER_PREFER_ACCOUNT_CODE];
    NSString* userPreferCountryCode = [localCache objectForKey: USER_PREFER_COUNTRY_CODE];

    //TODO:
    //Example: If user is manager, then
    //1. He can see his DS names if this page is viewing DS level data
    //2. He can see store names(except current one) owned by all his DS if this page is viewing Store level data
    VFDAO* app = [VFDAO sharedInstance];
    NSMutableArray* comparableStoreList = [NSMutableArray arrayWithArray: [app getStoresByBrandCode:userPreferBrandCode countryCode:userPreferCountryCode needOptionAll:NO ]];

    for(NSDictionary* store in comparableStoreList){
        if([store[@"Id"] isEqualToString:self.storeData[@"Id"]]){
            [comparableStoreList removeObject:store];
            break;
        }
    }
    [comparableStoreList insertObject: @{@"Name":[ShareLocale textFromKey:@"none"], @"Id":@"", @"Store_Code__c":@""} atIndex:0];
    
    
    StoreSelectionViewController* vc = [[StoreSelectionViewController alloc] initWithStoreList: comparableStoreList];
    vc.delegate = self;
    self.comparePopup = [[[UIPopoverController alloc] initWithContentViewController:vc] autorelease];
    self.comparePopup.delegate = self;
    [self.comparePopup presentPopoverFromRect: self.compareButton.frame inView: self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    [vc release];

}


-(IBAction) onChangeGlobalOptionButtonClicked:(id)sender{
    UIView* view = (UIView*)sender;
    int windowTag = [[view superview] superview].tag;
    NSMutableArray* selectedArray = nil;
    if(windowTag == 4){//sales mix
        selectedArray = self.selectedGlobalSalesMixOptions;
    }else if (windowTag == 2){//vip grade
        selectedArray = self.selectedGlobalVIPInfoOptions;
    }else{
        NSAssert(NO, @"No valid window tag");
    }
    
    NSMutableArray* availChoices = [NSMutableArray array];
    
    if(windowTag == 4){
        for(NSString * cat in self.categoryArray){
            for(NSString* gender in self.genderArray){
                for(NSString* catgen in self.catgenArray){
                    NSString* label = [NSString stringWithFormat:@"%@ %@", cat, gender];
                    if([label isEqualToString:catgen]){
                        
                        NSString* selected = @"N";
                        if(selectedArray!=nil){
                            for(NSDictionary* filter in selectedArray){
                                if([filter objectForKey:@"cat"]!=(id)[NSNull null] && [filter objectForKey:@"gender"]!=(id)[NSNull null]  ){
                                    if([cat isEqualToString:[filter objectForKey:@"cat"]] && [gender isEqualToString:[filter objectForKey:@"gender"]] ){
                                        selected = @"Y";
                                    }
                                }else if([filter objectForKey:@"cat"]!=(id)[NSNull null]){
                                    if([cat isEqualToString:[filter objectForKey:@"cat"]]){
                                        selected = @"Y";
                                    }
                                }else if([filter objectForKey:@"gender"]!=(id)[NSNull null]){
                                    if([gender isEqualToString:[filter objectForKey:@"gender"]]){
                                        selected = @"Y";
                                    }
                                }
                            }
                        }
                        [availChoices addObject: @{
                                                   @"Label": label,
                                                   @"gender": gender,
                                                   @"cat": cat,
                                                   @"selected": selected,
                                                   @"custom":@NO
                                                   }];
                    }
                }
            }
        }
    }else{
        
        for(NSString * g in self.vipGradeArray){
            NSString* selected = @"N";
            if(selectedArray!=nil){
                for(NSDictionary* grade in selectedArray){
                    if([g isEqualToString:[grade objectForKey:@"grade"]] ){
                        selected = @"Y";
                    }
                }
            }
            NSString* label = self.vipCodeLabelMap[g];
            if (label==nil || label==(id)[NSNull null]){
                label = g;
            }
            [availChoices addObject: @{
                                       @"Label": label,
                                       @"grade": g,
                                       @"selected": selected,
                                       @"custom":@NO
                                       }];
        }
    }

    SalesPerformanceSelectionViewController* vc = [[SalesPerformanceSelectionViewController alloc] initWithArray:availChoices cardTag:-1 windowTag:windowTag caller:self];
    self.selectionPopup = [[[UIPopoverController alloc] initWithContentViewController:vc] autorelease];
    self.selectionPopup.delegate = self;
    [self.selectionPopup presentPopoverFromRect:view.frame inView:view.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    [vc release];

    return;
}


-(void) onChangeOptionButtonClicked:(id)sender{
    
    UIView* view = (UIView*)sender;
    
    int windowTag = [[[[view superview] superview] superview] superview].tag;
    int cardTag = [[view superview] superview].tag;
    //2,10  2,11  2,12
    //2,13  2,14  2,15
    //2,16  2,17  2,18
    
    //4,10  4,11
    //4,13  4,14
    //4,16  4,17
    
    NSMutableArray* selectedArray = nil;
    
    if(windowTag == 4){//sales mix
        if(cardTag == 10){
            selectedArray = self.selectedDailyQuantityMixOptions;
        }
        else if (cardTag == 11){
            selectedArray = self.selectedDailySalesMixOptions;
        }
        else if (cardTag == 13){
            selectedArray = self.selectedWeeklyQuantityMixOptions;
        }
        else if (cardTag == 14){
            selectedArray = self.selectedWeeklySalesMixOptions;
        }
        else if (cardTag == 16){
            selectedArray = self.selectedMonthlyQuantityMixOptions;
        }
        else if (cardTag == 17){
            selectedArray = self.selectedMonthlySalesMixOptions;
        }
    }
    else if (windowTag == 2){//vip grade
        if(cardTag == 10){
            selectedArray = self.selectedDailyVIPCountOptions;
        }
        else if (cardTag == 11){
            selectedArray = self.selectedDailyVIPTxnCountOptions;
        }
        else if (cardTag == 12){
            selectedArray = self.selectedDailyVIPSalesOptions;
        }
        else if (cardTag == 13){
            selectedArray = self.selectedWeeklyVIPCountOptions;
        }
        else if (cardTag == 14){
            selectedArray = self.selectedWeeklyVIPTxnCountOptions;
        }
        else if (cardTag == 15){
            selectedArray = self.selectedWeeklyVIPSalesOptions;
        }
        else if (cardTag == 16){
            selectedArray = self.selectedMonthlyVIPCountOptions;
        }
        else if (cardTag == 17){
            selectedArray = self.selectedMonthlyVIPTxnCountOptions;
        }
        else if (cardTag == 18){
            selectedArray = self.selectedMonthlyVIPSalesOptions;
        }
    }
    else{
        NSAssert(NO, @"No valid window tag");
    }
    
    NSMutableArray* availChoices = [NSMutableArray array];
    
    if(windowTag == 4){
        for(NSString * cat in self.categoryArray){
            for(NSString* gender in self.genderArray){
                for(NSString* catgen in self.catgenArray){
                    NSString* label = [NSString stringWithFormat:@"%@ %@", cat, gender];
                    if([label isEqualToString:catgen]){
                        
                        NSString* selected = @"N";
                        if(selectedArray!=nil){
                            for(NSDictionary* filter in selectedArray){
                                if([filter objectForKey:@"cat"]!=(id)[NSNull null] && [filter objectForKey:@"gender"]!=(id)[NSNull null]  ){
                                    if([cat isEqualToString:[filter objectForKey:@"cat"]] && [gender isEqualToString:[filter objectForKey:@"gender"]] ){
                                        selected = @"Y";
                                    }
                                }else if([filter objectForKey:@"cat"]!=(id)[NSNull null]){
                                    if([cat isEqualToString:[filter objectForKey:@"cat"]]){
                                        selected = @"Y";
                                    }
                                }else if([filter objectForKey:@"gender"]!=(id)[NSNull null]){
                                    if([gender isEqualToString:[filter objectForKey:@"gender"]]){
                                        selected = @"Y";
                                    }
                                }
                            }
                        }
                        [availChoices addObject: @{
                                                   @"Label": label,
                                                   @"gender": gender,
                                                   @"cat": cat,
                                                   @"selected": selected
                                                   }];
                    }
                }
            }
        }
    }else{
        
        for(NSString * g in self.vipGradeArray){
            NSString* selected = @"N";
            if(selectedArray!=nil){
                for(NSDictionary* grade in selectedArray){
                    if([g isEqualToString:[grade objectForKey:@"grade"]] ){
                        selected = @"Y";
                    }
                }
            }
            NSString* label = self.vipCodeLabelMap[g];
            if (label==nil || label==(id)[NSNull null]){
                label = g;
            }
            [availChoices addObject: @{
                                       @"Label": label,
                                       @"grade": g,
                                       @"selected": selected
                                       }];
        }
    }
    
    SalesPerformanceSelectionViewController* vc = [[SalesPerformanceSelectionViewController alloc] initWithArray:availChoices cardTag:cardTag windowTag:windowTag];
    self.selectionPopup = [[[UIPopoverController alloc] initWithContentViewController:vc] autorelease];
    self.selectionPopup.delegate = self;
    [self.selectionPopup presentPopoverFromRect:view.frame inView:view.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    [vc release];

}



- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    if(self.selectionPopup!=nil){
        SalesPerformanceSelectionViewController* vc = (SalesPerformanceSelectionViewController*)[self.selectionPopup contentViewController];
        NSMutableArray* tempArray = [NSMutableArray array];;
        for(id val in vc.recordArray){
            if(![val isKindOfClass:[NSString class]]){
                [tempArray addObject:val];
            }
        }
        [self onSelectdOption: tempArray windowTag:vc.windowTag cardTag:vc.cardTag ];
        self.selectionPopup = nil;
    }
    else if (self.comparePopup!=nil){
        self.comparePopup = nil;
    }
}



-(void) onSelectdOption:(NSArray*)updateOptions windowTag:(int)wtag cardTag:(int)ctag{
    
    NSMutableArray* selectedFilterOptions = [NSMutableArray array];

    if(ctag == -1){
        NSArray* ctagArray;
        if(wtag==4){
            ctagArray = @[@10,@11,@13,@14,@16,@17];
            for(NSDictionary* op in updateOptions){
                if([[op objectForKey:@"selected"] isEqualToString:@"Y"]){
                    NSString* gender = [op objectForKey:@"gender"];
                    NSString* category = [op objectForKey:@"cat"];
                    if(gender==nil){
                        gender = (id)[NSNull null];
                    }
                    if(category==nil){
                        category = (id)[NSNull null];
                    }
                    [selectedFilterOptions addObject: @{@"cat":category, @"gender":gender}];
                }
            }
            self.selectedGlobalSalesMixOptions = selectedFilterOptions;
        }else if (wtag==2){
            ctagArray = @[@10,@11,@12,@13,@14,@15,@16,@17,@18];
            for(NSDictionary* op in updateOptions){
                if([[op objectForKey:@"selected"] isEqualToString:@"Y"]){
                    NSString* g = [op objectForKey:@"grade"];
                    if(g==nil){
                        g = (id)[NSNull null];
                    }
                    [selectedFilterOptions addObject: @{@"grade":g}];
                }
            }
            self.selectedGlobalVIPInfoOptions = selectedFilterOptions;
        }else{
            return;
        }
        for(NSNumber* b in ctagArray){
            [self onSelectdOption:updateOptions windowTag:wtag cardTag: [b intValue] ];
        }
        return;
    }
    
    //only run on Sales Mix Info(4) OR VIP Info(2) tabs
    if(wtag == 4 || wtag == 2){
        NSString* type = nil;
        
        
        NSString* title = nil;
        
        
        NSString* summaryName;
        NSString* summaryPeriodExtraName = nil;
        NSString* summaryPeriod1Name = nil;
        NSString* summaryPeriod2Name = nil;
        NSString* summaryPeriod3Name = nil;
        
        UIView* parentView;
        NSString* category;
        NSDictionary* data;
        NSDictionary* compareData;
        NSDictionary* currentPeriodData;
        NSString* headerName;
        if(wtag==4){
            for(NSDictionary* op in updateOptions){
                if([[op objectForKey:@"selected"] isEqualToString:@"Y"]){
                    NSString* gender = [op objectForKey:@"gender"];
                    NSString* category = [op objectForKey:@"cat"];
                    if(gender==nil){
                        gender = (id)[NSNull null];
                    }
                    if(category==nil){
                        category = (id)[NSNull null];
                    }
                    [selectedFilterOptions addObject: @{@"cat":category, @"gender":gender}];
                }
            }
            parentView = self.tab7View;
            if(ctag == 11){
                headerName = HEADER_DATE_VALUE_TOTAL;
                category = @"mix_sales";
                data = [self.allChartData objectForKey:@"weekSalesMix"];
                compareData = [self.allChartDataExtra objectForKey:@"weekSalesMix"];
                type = @"week";
                title = [ShareLocale textFromKey:@"Recent Daily Ops Sales(%@)"];
                self.selectedDailySalesMixOptions = selectedFilterOptions;
                summaryName = @"refSalesMix";
                summaryPeriod1Name = @"tywtd";
                summaryPeriod2Name = @"lywtd";
                summaryPeriod3Name = @"lwwtd";
            }
            else if (ctag == 10){
                headerName = HEADER_DATE_VALUE_TOTAL;
                category = @"mix_qty";

                data = [self.allChartData objectForKey:@"weekQuantityMix"];
                compareData = [self.allChartDataExtra objectForKey:@"weekQuantityMix"];

                type = @"week";
                title = [ShareLocale textFromKey:@"Recent Daily Sales Quantity(%@)"];
                self.selectedDailyQuantityMixOptions = selectedFilterOptions;
                summaryName = @"refQuantityMix";
                summaryPeriod1Name = @"tywtd";
                summaryPeriod2Name = @"lywtd";
                summaryPeriod3Name = @"lwwtd";
            }
            else if(ctag == 14){
                headerName = HEADER_WEEK_VALUE_TOTAL;
                category = @"mix_sales";
                
                data = [self.allChartData objectForKey:@"monthSalesMix"];
                compareData = [self.allChartDataExtra objectForKey:@"monthSalesMix"];
                currentPeriodData = [self.allChartData objectForKey:@"weekSalesMix"];
                type = @"month";
                title = [ShareLocale textFromKey:@"Recent Weekly Ops Sales(%@)"];
                self.selectedWeeklySalesMixOptions = selectedFilterOptions;
                summaryName = @"refSalesMix";
                summaryPeriod1Name = @"tymtd";
                summaryPeriod2Name = @"lymtd";
                summaryPeriod3Name = @"lmmtd";
                summaryPeriodExtraName = @"tywtd";
            }
            else if (ctag == 13){
                headerName = HEADER_WEEK_VALUE_TOTAL;
                category = @"qty_mix";
                data = [self.allChartData objectForKey:@"monthQuantityMix"];
                compareData = [self.allChartDataExtra objectForKey:@"monthQuantityMix"];

                currentPeriodData = [self.allChartData objectForKey:@"weekQuantityMix"];
                type = @"month";
                title = [ShareLocale textFromKey:@"Recent Weekly Sales Quantity(%@)"];
                self.selectedWeeklyQuantityMixOptions = selectedFilterOptions;
                summaryName = @"refQuantityMix";
                summaryPeriod1Name = @"tymtd";
                summaryPeriod2Name = @"lymtd";
                summaryPeriod3Name = @"lmmtd";
                summaryPeriodExtraName = @"tywtd";
            }
            else if(ctag == 17){
                headerName = HEADER_MONT_VALUE_TOTAL;
                category = @"mix_sales";
                data = [self.allChartData objectForKey:@"yearSalesMix"];
                compareData = [self.allChartDataExtra objectForKey:@"yearSalesMix"];
                currentPeriodData = [self.allChartData objectForKey:@"monthSalesMix"];
                type = @"year";
                title = [ShareLocale textFromKey:@"Recent Monthly Ops Sales(%@)"];
                self.selectedMonthlySalesMixOptions = selectedFilterOptions;
                summaryName = @"refSalesMix";
                summaryPeriod1Name = @"tyytd";
                summaryPeriod2Name = @"lyytd";
                summaryPeriod3Name = nil;
                summaryPeriodExtraName = @"tymtd";

            }
            else if (ctag == 16){
                headerName = HEADER_MONT_VALUE_TOTAL;
                category = @"mix_qty";
                data = [self.allChartData objectForKey:@"yearQuantityMix"];
                compareData = [self.allChartDataExtra objectForKey:@"yearQuantityMix"];
                currentPeriodData = [self.allChartData objectForKey:@"monthQuantityMix"];
                type = @"year";
                title = [ShareLocale textFromKey:@"Recent Monthly Sales Quantity(%@)"];
                self.selectedMonthlyQuantityMixOptions = selectedFilterOptions;
                summaryName = @"refQuantityMix";
                summaryPeriod1Name = @"tyytd";
                summaryPeriod2Name = @"lyytd";
                summaryPeriod3Name = nil;
                summaryPeriodExtraName = @"tymtd";

            }
            else{
                NSAssert(NO, @"Invalid card tag");
                return;
            }
        }
        else if(wtag==2){
            
            for(NSDictionary* op in updateOptions){
                if([[op objectForKey:@"selected"] isEqualToString:@"Y"]){
                    NSString* g = [op objectForKey:@"grade"];
                    if(g==nil){
                        g = (id)[NSNull null];
                    }
                    [selectedFilterOptions addObject: @{@"grade":g}];
                }
            }
            parentView = self.tab3View;
            if (ctag == 10){
                headerName = HEADER_DATE_VALUE_TOTAL;
                category = @"vip_count";
                data = [self.allChartData objectForKey:@"weekVIPCount"];
                compareData = [self.allChartDataExtra objectForKey:@"weekVIPCount"];
                type = @"week";
                title = [ShareLocale textFromKey:@"Recent Daily New VIP Count(%@)"];
                self.selectedDailyVIPCountOptions = selectedFilterOptions;
                summaryName = nil;
            }
            else if(ctag == 11){
                headerName = HEADER_DATE_VALUE_TOTAL;
                category = @"vip_txn";
                data = [self.allChartData objectForKey:@"weekVIPTxn"];
                compareData = [self.allChartDataExtra objectForKey:@"weekVIPTxn"];
                type = @"week";
                title = [ShareLocale textFromKey:@"Recent Daily VIP TXN Count(%@)"];
                self.selectedDailyVIPTxnCountOptions = selectedFilterOptions;
                summaryName = @"refVIPTxn";
                summaryPeriod1Name = @"tywtd";
                summaryPeriod2Name = @"lywtd";
                summaryPeriod3Name = @"lwwtd";

            }
            else if(ctag == 12){
                headerName = HEADER_DATE_VALUE_TOTAL;
                category = @"vip_sales";
                data = [self.allChartData objectForKey:@"weekVIPOpsSale"];
                compareData = [self.allChartDataExtra objectForKey:@"weekVIPOpsSale"];
                type = @"week";
                title = [ShareLocale textFromKey:@"Recent Daily VIP Ops Sales(%@)"];
                self.selectedDailyVIPSalesOptions = selectedFilterOptions;
                summaryName = @"refVIPSales";
                summaryPeriod1Name = @"tywtd";
                summaryPeriod2Name = @"lywtd";
                summaryPeriod3Name = @"lwwtd";

            }
            else if (ctag == 13){
                headerName = HEADER_WEEK_VALUE_TOTAL;
                category = @"vip_count";
                data = [self.allChartData objectForKey:@"monthVIPCount"];
                compareData = [self.allChartDataExtra objectForKey:@"monthVIPCount"];
                currentPeriodData = [self.allChartData objectForKey:@"weekVIPCount"];
                type = @"month";
                title = [ShareLocale textFromKey:@"Recent Weekly New VIP Count(%@)"];
                self.selectedWeeklyVIPCountOptions = selectedFilterOptions;
                summaryName = nil;
                summaryPeriodExtraName = @"tywtd";
            }
            else if(ctag == 14){
                category = @"vip_txn";
                headerName = HEADER_WEEK_VALUE_TOTAL;
                data = [self.allChartData objectForKey:@"monthVIPTxn"];
                compareData = [self.allChartDataExtra objectForKey:@"monthVIPTxn"];
                currentPeriodData = [self.allChartData objectForKey:@"weekVIPTxn"];
                type = @"month";
                title = [ShareLocale textFromKey:@"Recent Weekly VIP TXN Count(%@)"];
                self.selectedWeeklyVIPTxnCountOptions = selectedFilterOptions;
                summaryName = @"refVIPTxn";
                summaryPeriod1Name = @"tymtd";
                summaryPeriod2Name = @"lymtd";
                summaryPeriod3Name = @"lmmtd";
                summaryPeriodExtraName = @"tywtd";
            }
            else if(ctag == 15){
                category = @"vip_sales";
                headerName = HEADER_WEEK_VALUE_TOTAL;
                data = [self.allChartData objectForKey:@"monthVIPOpsSale"];
                compareData = [self.allChartDataExtra objectForKey:@"monthVIPOpsSale"];
                currentPeriodData = [self.allChartData objectForKey:@"weekVIPOpsSale"];
                type = @"month";
                title = [ShareLocale textFromKey:@"Recent Weekly VIP Ops Sales(%@)"];
                self.selectedWeeklyVIPSalesOptions = selectedFilterOptions;
                summaryName = @"refVIPSales";
                summaryPeriod1Name = @"tymtd";
                summaryPeriod2Name = @"lymtd";
                summaryPeriod3Name = @"lmmtd";
                summaryPeriodExtraName = @"tywtd";
            }
            else if (ctag == 16){
                headerName = HEADER_MONT_VALUE_TOTAL;
                category = @"vip_count";
                data = [self.allChartData objectForKey:@"yearVIPCount"];
                compareData = [self.allChartDataExtra objectForKey:@"yearVIPCount"];
                currentPeriodData = [self.allChartData objectForKey:@"monthVIPCount"];
                type = @"year";
                title = [ShareLocale textFromKey:@"Recent Monthly New VIP Count(%@)"];
                self.selectedMonthlyVIPCountOptions = selectedFilterOptions;
                summaryName = nil;
            }
            else if(ctag == 17){
                category = @"vip_txn";
                headerName = HEADER_MONT_VALUE_TOTAL;
                data = [self.allChartData objectForKey:@"yearVIPTxn"];
                compareData = [self.allChartDataExtra objectForKey:@"yearVIPTxn"];
                currentPeriodData = [self.allChartData objectForKey:@"monthVIPTxn"];
                type = @"year";
                title = [ShareLocale textFromKey:@"Recent Monthly VIP TXN Count(%@)"];
                self.selectedMonthlyVIPTxnCountOptions = selectedFilterOptions;
                summaryName = @"refVIPTxn";
                summaryPeriod1Name = @"tyytd";
                summaryPeriod2Name = @"lyytd";
                summaryPeriod3Name = nil;
                summaryPeriodExtraName = @"tymtd";
            }
            else if (ctag == 18){
                category = @"vip_sales";
                headerName = HEADER_MONT_VALUE_TOTAL;
                data = [self.allChartData objectForKey:@"yearVIPOpsSale"];
                compareData = [self.allChartDataExtra objectForKey:@"yearVIPOpsSale"];
                currentPeriodData = [self.allChartData objectForKey:@"monthVIPOpsSale"];
                type = @"year";
                title = [ShareLocale textFromKey:@"Recent Monthly VIP Ops Sales(%@)"];
                self.selectedMonthlyVIPSalesOptions = selectedFilterOptions;
                summaryName = @"refVIPSales";
                summaryPeriod1Name = @"tyytd";
                summaryPeriod2Name = @"lyytd";
                summaryPeriod3Name = nil;
                summaryPeriodExtraName = @"tymtd";
            }
            else{
                NSAssert(NO, @"Invalid card tag");
                return;
            }
            
            
        }else{
            NSAssert(NO, @"Invalid window tag");
            return;
        }
        
        //prepare data with no filter
        self.selectedFilterOptions = [NSMutableArray array];
        NSDictionary* referData = @{@"title":@"",
                                    @"types":type,
                                    @"nodes": (wtag==4)?[self filterMixedData: data]:[self filterVIPGradeData:data]};

        //Apply filter
        self.selectedFilterOptions = selectedFilterOptions;
        //prepare data with selected filter
        
        NSArray* filteredRecords = (wtag==4)?[self filterMixedData: data]:[self filterVIPGradeData:data];
        NSArray* filteredCompareRecords = (wtag==4)?[self filterMixedData: compareData]:[self filterVIPGradeData:compareData];
        NSDictionary* finalData = @{
                                    @"type":type,
                                    @"nodes": filteredRecords
                                    };
        NSDictionary* finalCompareData = @{
                                    @"type":type,
                                    @"nodes": filteredCompareRecords
                                    };
        
        NSDictionary* extraNode = nil;
        if([type isEqualToString:@"month"]){
            NSArray* extraSumArray = [self getSummaryValuesForSelectedFilters:selectedFilterOptions filterCategory:category refName:summaryName period1Name: summaryPeriodExtraName period2Name:nil period3Name:nil];
            extraNode = @{@"is_null":@NO, @"val": extraSumArray[0], @"period":[NSString stringWithFormat:@"%d-%d", self.fxYear, self.fxWeek]};
        }
        else if ([type isEqualToString:@"year"]){
            NSArray* extraSumArray = [self getSummaryValuesForSelectedFilters:selectedFilterOptions filterCategory:category refName:summaryName period1Name: summaryPeriodExtraName period2Name:nil period3Name:nil];
            extraNode = @{@"is_null":@NO, @"val": extraSumArray[0], @"period":[NSString stringWithFormat:@"%d-%d", self.fxYear, self.fxMonth]};
        }
        

        //tymtd, lymtd, lmmtd
        NSArray* sumArray = [self getSummaryValuesForSelectedFilters:selectedFilterOptions filterCategory:category refName:summaryName period1Name:summaryPeriod1Name period2Name:summaryPeriod2Name period3Name:summaryPeriod3Name];
        

        [self generateChartDiagramWithData: finalData compareData:finalCompareData title:[self getMixDataTitleFromData:data format: title] view: [parentView viewWithTag:ctag] category:@"NA" target:nil header:headerName displayScale:1 summaryFormat:nil referData:referData negativeComparing:NO sumTYPTD:sumArray[0] sumLYPTD:sumArray[1] sumLPPTD:sumArray[2] extraNode: extraNode];
        
    }
}



-(void)onSelectdStore:(NSDictionary *)storeObj{
    if(self.comparePopup!=nil){
        [self.comparePopup dismissPopoverAnimated:YES];
        
        NSString* storeName = storeObj[@"Name"];
        [self.compareButton setTitle:storeName forState:UIControlStateNormal];
        
        

        NSString* storeId = storeObj[@"Id"];
        if([storeId isEqualToString:@""]){
            self.allChartDataExtra = nil;
            [self reloadContent];
        }else{
            [self loadComparableStoreChartDataWithStoreId: storeObj[@"Id"]];
        }
        
    }
}





#pragma mark - Handle Click event of Back button & Read button
- (IBAction)onBackButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onHomeButtonClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)onTabSwitched:(id)sender {
    UIView* view = (UIView*) sender;
    if(self.tabIndex != view.tag){
        self.tabIndex = view.tag;
        [self reloadContent];
    }
}

-(UILabel*) addNewLabelToView:(UIView*)view text:(NSString*)text x:(int)x y:(int)y{
    UILabel* lbl = [[[UILabel alloc] initWithFrame:CGRectMake(x, y, 280, 22)] autorelease];
    lbl.textColor = [UIColor whiteColor];
    lbl.font = [UIFont fontWithName:@"Courier" size:12];
    lbl.text = text;
    [view addSubview:lbl];
    return lbl;
}

#pragma mark - Reload tab content

-(void) reloadContent{
    for(UIButton* button in self.tabArray){
        button.backgroundColor = [UIColor blackColor];
    }
    
    self.contentScrollView.scrollEnabled = YES;
    
    UIButton* selectedButton = [self.tabArray objectAtIndex:self.tabIndex];
    selectedButton.backgroundColor = [UIColor colorWithWhite:55/255.0f alpha:1];
    
    if([self.tab1View superview]) [self.tab1View removeFromSuperview];
    if([self.tab2View superview]) [self.tab2View removeFromSuperview];
    if([self.tab3View superview]) [self.tab3View removeFromSuperview];
    if([self.tab4View superview]) [self.tab4View removeFromSuperview];
    if([self.tab7View superview]) [self.tab7View removeFromSuperview];
    if([self.tab8View superview]) [self.tab8View removeFromSuperview];
    
    self.selectedFilterOptions = nil;
    
    if(self.tabIndex == 0){
        [self reloadTab1View];
    }
    else if(self.tabIndex == 1){
        [self reloadTab2View];
    }
    else if(self.tabIndex == 2){
        [self reloadTab3View];
    }
    else if(self.tabIndex == 3){
        [self reloadTab4View];
    }
    else if(self.tabIndex == 4){
        [self reloadTab7View];
    }
    else if (self.tabIndex == 5){
        [self reloadTab8View];
    }
    
    
    curDashboardView.alpha = 0;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5f];
    [UIView setAnimationDelay:.2f];
    curDashboardView.alpha = 1;
    [UIView commitAnimations];
    
}



//Tab2: Transaction Info
-(void) reloadTab1View{
    
    [self.contentScrollView addSubview:self.tab1View];
    self.contentScrollView.contentSize = CGSizeMake(983,580*3);
    curDashboardView = self.tab1View;
    
    
    NSDictionary* weekTxn = [self.allChartData objectForKey:@"weekTxn"];
    NSDictionary* weekTxnExtra = [self.allChartDataExtra objectForKey:@"weekTxn"];
    [self generateChartDiagramWithData: weekTxn compareData:weekTxnExtra  title: [ShareLocale textFromKey:@"Recent Daily Transaction"]  view: [self.tab1View viewWithTag:10] category:@"txn_count" target:nil header:HEADER_DATE_VALUE_TARGET];
    
    NSDictionary* weekUPT = [self.allChartData objectForKey:@"weekUpt"];
    NSDictionary* weekUPTExtra = [self.allChartDataExtra objectForKey:@"weekUpt"];
    [self generateChartDiagramWithData: weekUPT compareData:weekUPTExtra title: [ShareLocale textFromKey:@"Recent Daily UPT"]  view: [self.tab1View viewWithTag:11] category:@"upt" target:[self getTargetDataWithType:@"weekly" category:@"UPT__c"] header:HEADER_DATE_VALUE_TARGET];
    
    NSDictionary* weekATV = [self.allChartData objectForKey:@"weekAtv"];
    NSDictionary* weekATVExtra = [self.allChartDataExtra objectForKey:@"weekAtv"];
    [self generateChartDiagramWithData: weekATV compareData:weekATVExtra title: [ShareLocale textFromKey:@"Recent Daily ATV"]  view: [self.tab1View viewWithTag:12] category:@"atv" target:[self getTargetDataWithType:@"weekly" category:@"ATV__c"] header:HEADER_DATE_VALUE_TARGET];
    
    
    NSDictionary* monthTxn = [self.allChartData objectForKey:@"monthTxn"];
    NSDictionary* monthTxnExtra = [self.allChartDataExtra objectForKey:@"monthTxn"];
    [self generateChartDiagramWithData: monthTxn compareData:monthTxnExtra title: [ShareLocale textFromKey:@"Recent Weekly Transaction"]  view: [self.tab1View viewWithTag:13] category:@"txn_count" target:nil header:HEADER_WEEK_VALUE_TARGET displayScale:1 summaryFormat:@"%.2f" referData:nil negativeComparing:NO sumTYPTD:nil sumLYPTD:nil sumLPPTD:nil extraNode:[self generateExtraNodeForCategory:@"txn_count" type:@"month" targetCategory:nil ]];
    
    NSDictionary* monthUpt = [self.allChartData objectForKey:@"monthUpt"];
    NSDictionary* monthUptExtra = [self.allChartDataExtra objectForKey:@"monthUpt"];
    [self generateChartDiagramWithData: monthUpt compareData:monthUptExtra title: [ShareLocale textFromKey:@"Recent Weekly UPT"]  view: [self.tab1View viewWithTag:14] category:@"upt" target:[self getTargetDataWithType:@"monthly" category:@"UPT__c"] header:HEADER_WEEK_VALUE_TARGET  displayScale:1 summaryFormat:@"%.2f" referData:nil negativeComparing:NO sumTYPTD:nil sumLYPTD:nil sumLPPTD:nil extraNode:[self generateExtraNodeForCategory:@"upt" type:@"month" targetCategory:@"UPT__c" ] ];

    
    NSDictionary* monthAtv = [self.allChartData objectForKey:@"monthAtv"];
    NSDictionary* monthAtvExtra = [self.allChartDataExtra objectForKey:@"monthAtv"];
    [self generateChartDiagramWithData: monthAtv compareData:monthAtvExtra title: [ShareLocale textFromKey:@"Recent Weekly ATV"]  view: [self.tab1View viewWithTag:15] category:@"atv" target:[self getTargetDataWithType:@"monthly" category:@"ATV__c"] header:HEADER_WEEK_VALUE_TARGET  displayScale:1 summaryFormat:@"%.2f" referData:nil negativeComparing:NO sumTYPTD:nil sumLYPTD:nil sumLPPTD:nil extraNode:[self generateExtraNodeForCategory:@"atv" type:@"month" targetCategory:@"ATV__c" ] ];
    
    
    NSDictionary* yearTxn = [self.allChartData objectForKey:@"yearTxn"];
    NSDictionary* yearTxnExtra = [self.allChartDataExtra objectForKey:@"yearTxn"];
    [self generateChartDiagramWithData: yearTxn compareData:yearTxnExtra title: [ShareLocale textFromKey:@"Recent Monthly Transaction"]  view: [self.tab1View viewWithTag:16] category:@"txn_count" target:nil header:HEADER_MONT_VALUE_TARGET  displayScale:1 summaryFormat:@"%.2f" referData:nil negativeComparing:NO sumTYPTD:nil sumLYPTD:nil sumLPPTD:nil extraNode:[self generateExtraNodeForCategory:@"txn_count" type:@"year" targetCategory:nil ]];
    
    NSDictionary* yearUpt = [self.allChartData objectForKey:@"yearUpt"];
    NSDictionary* yearUptExtra = [self.allChartDataExtra objectForKey:@"yearUpt"];
    [self generateChartDiagramWithData: yearUpt compareData:yearUptExtra title: [ShareLocale textFromKey:@"Recent Monthly UPT"]  view: [self.tab1View viewWithTag:17] category:@"upt" target:[self getTargetDataWithType:@"yearly" category:@"UPT__c"] header:HEADER_MONT_VALUE_TARGET displayScale:1 summaryFormat:@"%.2f" referData:nil negativeComparing:NO sumTYPTD:nil sumLYPTD:nil sumLPPTD:nil extraNode:[self generateExtraNodeForCategory:@"upt" type:@"year" targetCategory:@"UPT__c" ] ];
    
    
    NSDictionary* yearAtv = [self.allChartData objectForKey:@"yearAtv"];
    NSDictionary* yearAtvExtra = [self.allChartDataExtra objectForKey:@"yearAtv"];
    [self generateChartDiagramWithData: yearAtv compareData:yearAtvExtra title: [ShareLocale textFromKey:@"Recent Monthly ATV"]  view: [self.tab1View viewWithTag:18] category:@"atv" target:[self getTargetDataWithType:@"yearly" category:@"ATV__c"] header:HEADER_MONT_VALUE_TARGET displayScale:1 summaryFormat:@"%.2f" referData:nil negativeComparing:NO sumTYPTD:nil sumLYPTD:nil sumLPPTD:nil extraNode:[self generateExtraNodeForCategory:@"atv" type:@"year" targetCategory:@"ATV__c" ] ];
    
    
}

//Tab3: Visitor Info
-(void) reloadTab2View{
    [self.contentScrollView addSubview:self.tab2View];
    curDashboardView = self.tab2View;
    self.contentScrollView.contentSize = CGSizeMake(983,580*3);

    
    NSDictionary* weekPelOff = [self.allChartData objectForKey:@"weekPelOff"];
    NSDictionary* weekPelOffExtra = [self.allChartDataExtra objectForKey:@"weekPelOff"];
    [self generateChartDiagramWithData: weekPelOff compareData:weekPelOffExtra title: [ShareLocale textFromKey:@"Recent Daily Peel Off"]  view: [self.tab2View viewWithTag:10] category:@"pel_off" target:nil header:HEADER_DATE_VALUE_TARGET displayScale:100  summaryFormat:@"%.2f%%" referData:nil negativeComparing:NO   sumTYPTD:nil sumLYPTD:nil sumLPPTD:nil];
    
    NSDictionary* weekVisitor = [self.allChartData objectForKey:@"weekVisitor"];
    NSDictionary* weekVisitorExtra = [self.allChartDataExtra objectForKey:@"weekVisitor"];
    [self generateChartDiagramWithData: weekVisitor compareData:weekVisitorExtra title: [ShareLocale textFromKey:@"Recent Daily Visitor Count"]  view: [self.tab2View viewWithTag:11] category:@"visitor_count" target:nil header:HEADER_DATE_VALUE_TARGET];
    NSDictionary* weekConvRate = [self.allChartData objectForKey:@"weekConvRate"];
    NSDictionary* weekConvRateExtra = [self.allChartDataExtra objectForKey:@"weekConvRate"];
    [self generateChartDiagramWithData: weekConvRate compareData:weekConvRateExtra title: [ShareLocale textFromKey:@"Recent Daily Conversion Rate"]  view: [self.tab2View viewWithTag:12] category:@"conv_rate" target:[self getTargetDataWithType:@"weekly" category:@"Conversion_Rate__c"] header:HEADER_DATE_VALUE_TARGET displayScale:100  summaryFormat:@"%.2f%%" referData:nil negativeComparing:NO   sumTYPTD:nil sumLYPTD:nil sumLPPTD:nil];//0.056, 0.061, 0.053, 0.049, 0.046
    
    NSDictionary* monthPelOff = [self.allChartData objectForKey:@"monthPelOff"];
    NSDictionary* monthPelOffExtra = [self.allChartDataExtra objectForKey:@"monthPelOff"];
    [self generateChartDiagramWithData: monthPelOff compareData:monthPelOffExtra title: [ShareLocale textFromKey:@"Recent Weekly Peel Off"]  view: [self.tab2View viewWithTag:13] category:@"pel_off" target:nil header:HEADER_WEEK_VALUE_TARGET displayScale:100  summaryFormat:@"%.2f%%" referData:nil negativeComparing:NO   sumTYPTD:nil sumLYPTD:nil sumLPPTD:nil extraNode:[self generateExtraNodeForCategory:@"pel_off" type:@"month" targetCategory:nil] ];
    
    
    NSDictionary* monthVisitor = [self.allChartData objectForKey:@"monthVisitor"];
    NSDictionary* monthVisitorExtra = [self.allChartDataExtra objectForKey:@"monthVisitor"];
    [self generateChartDiagramWithData: monthVisitor compareData:monthVisitorExtra title: [ShareLocale textFromKey:@"Recent Weekly Visitor Count"]  view: [self.tab2View viewWithTag:14] category:@"visitor_count" target:nil header:HEADER_WEEK_VALUE_TARGET displayScale:1 summaryFormat:@"%.2f" referData:nil negativeComparing:NO sumTYPTD:nil sumLYPTD:nil sumLPPTD:nil extraNode:[self generateExtraNodeForCategory:@"visitor_count" type:@"month" targetCategory:nil]];
    
    NSDictionary* monthConvRate = [self.allChartData objectForKey:@"monthConvRate"];
    NSDictionary* monthConvRateExtra = [self.allChartDataExtra objectForKey:@"monthConvRate"];
    [self generateChartDiagramWithData: monthConvRate compareData:monthConvRateExtra title: [ShareLocale textFromKey:@"Recent Weekly Conversion Rate"]  view: [self.tab2View viewWithTag:15] category:@"conv_rate" target:[self getTargetDataWithType:@"monthly" category:@"Conversion_Rate__c"] header:HEADER_WEEK_VALUE_TARGET displayScale:100  summaryFormat:@"%.2f%%" referData:nil negativeComparing:NO   sumTYPTD:nil sumLYPTD:nil sumLPPTD:nil extraNode:[self generateExtraNodeForCategory:@"conv_rate" type:@"month" targetCategory:@"Conversion_Rate__c" ] ];

    NSDictionary* yearPelOff = [self.allChartData objectForKey:@"yearPelOff"];
    NSDictionary* yearPelOffExtra = [self.allChartDataExtra objectForKey:@"yearPelOff"];
    [self generateChartDiagramWithData: yearPelOff compareData:yearPelOffExtra title: [ShareLocale textFromKey:@"Recent Monthly Peel Off"]  view: [self.tab2View viewWithTag:16] category:@"pel_off" target:nil header:HEADER_MONT_VALUE_TARGET displayScale:100  summaryFormat:@"%.2f%%" referData:nil negativeComparing:NO   sumTYPTD:nil sumLYPTD:nil sumLPPTD:nil extraNode:[self generateExtraNodeForCategory:@"pel_off" type:@"year" targetCategory:nil]];
    
    NSDictionary* yearVisitor = [self.allChartData objectForKey:@"yearVisitor"];
    NSDictionary* yearVisitorExtra = [self.allChartDataExtra objectForKey:@"yearVisitor"];
    [self generateChartDiagramWithData: yearVisitor compareData:yearVisitorExtra title: [ShareLocale textFromKey:@"Recent Monthly Visitor Count"]  view: [self.tab2View viewWithTag:17] category:@"visitor_count" target:nil header:HEADER_MONT_VALUE_TARGET displayScale:1 summaryFormat:@"%.2f" referData:nil negativeComparing:NO sumTYPTD:nil sumLYPTD:nil sumLPPTD:nil extraNode:[self generateExtraNodeForCategory:@"visitor_count" type:@"year" targetCategory:nil] ];
    
    NSDictionary* yearConvRate = [self.allChartData objectForKey:@"yearConvRate"];
    NSDictionary* yearConvRateExtra = [self.allChartData objectForKey:@"yearConvRate"];
    [self generateChartDiagramWithData: yearConvRate compareData:yearConvRateExtra title: [ShareLocale textFromKey:@"Recent Monthly Conversion Rate"]  view: [self.tab2View viewWithTag:18] category:@"conv_rate" target:[self getTargetDataWithType:@"yearly" category:@"Conversion_Rate__c"] header:HEADER_MONT_VALUE_TARGET displayScale:100  summaryFormat:@"%.2f%%" referData:nil negativeComparing:NO sumTYPTD:nil sumLYPTD:nil sumLPPTD:nil  extraNode:[self generateExtraNodeForCategory:@"conv_rate" type:@"year" targetCategory:@"Conversion_Rate__c" ]];
}


//Tab4: VIP Info
-(void) reloadTab3View{
    
    self.selectedDailyVIPCountOptions = nil;
    self.selectedWeeklyVIPCountOptions = nil;
    self.selectedMonthlyVIPCountOptions = nil;
    self.selectedDailyVIPTxnCountOptions = nil;
    self.selectedWeeklyVIPTxnCountOptions = nil;
    self.selectedMonthlyVIPTxnCountOptions = nil;
    self.selectedDailyVIPSalesOptions = nil;
    self.selectedWeeklyVIPSalesOptions = nil;
    self.selectedMonthlyVIPSalesOptions = nil;
    
    NSMutableSet* vipGradeSet = [NSMutableSet set];
    [self.vipGradeArray removeAllObjects];
    
    //Instead of display target value, show the total%
    
    [self.contentScrollView addSubview:self.tab3View];
    curDashboardView = self.tab3View;
    self.contentScrollView.contentSize = CGSizeMake(983, 580*3);
    
    NSDictionary* weekVIPCount = [self.allChartData objectForKey:@"weekVIPCount"];
    NSDictionary* weekVIPTxn = [self.allChartData objectForKey:@"weekVIPTxn"];
    NSDictionary* weekVIPOpsSale = [self.allChartData objectForKey:@"weekVIPOpsSale"];
    NSDictionary* monthVIPCount = [self.allChartData objectForKey:@"monthVIPCount"];
    NSDictionary* monthVIPTxn = [self.allChartData objectForKey:@"monthVIPTxn"];
    NSDictionary* monthVIPOpsSale = [self.allChartData objectForKey:@"monthVIPOpsSale"];
    NSDictionary* yearVIPCount = [self.allChartData objectForKey:@"yearVIPCount"];
    NSDictionary* yearVIPTxn = [self.allChartData objectForKey:@"yearVIPTxn"];
    NSDictionary* yearVIPOpsSale = [self.allChartData objectForKey:@"yearVIPOpsSale"];
    [self copyVIPGradeFromData:weekVIPCount toGradeSet:vipGradeSet];
    [self copyVIPGradeFromData:weekVIPTxn toGradeSet:vipGradeSet];
    [self copyVIPGradeFromData:weekVIPOpsSale toGradeSet:vipGradeSet];
    [self copyVIPGradeFromData:monthVIPCount toGradeSet:vipGradeSet];
    [self copyVIPGradeFromData:monthVIPTxn toGradeSet:vipGradeSet];
    [self copyVIPGradeFromData:monthVIPOpsSale toGradeSet:vipGradeSet];
    [self copyVIPGradeFromData:yearVIPCount toGradeSet:vipGradeSet];
    [self copyVIPGradeFromData:yearVIPTxn toGradeSet:vipGradeSet];
    [self copyVIPGradeFromData:yearVIPOpsSale toGradeSet:vipGradeSet];
    [self.vipGradeArray addObjectsFromArray:[vipGradeSet allObjects]];
    
    //handle compare line
    NSDictionary* weekVIPCountExtra = [self.allChartDataExtra objectForKey:@"weekVIPCount"];
    NSDictionary* weekVIPTxnExtra = [self.allChartDataExtra objectForKey:@"weekVIPTxn"];
    NSDictionary* weekVIPOpsSaleExtra = [self.allChartDataExtra objectForKey:@"weekVIPOpsSale"];
    NSDictionary* monthVIPCountExtra = [self.allChartDataExtra objectForKey:@"monthVIPCount"];
    NSDictionary* monthVIPTxnExtra = [self.allChartDataExtra objectForKey:@"monthVIPTxn"];
    NSDictionary* monthVIPOpsSaleExtra = [self.allChartDataExtra objectForKey:@"monthVIPOpsSale"];
    NSDictionary* yearVIPCountExtra = [self.allChartDataExtra objectForKey:@"yearVIPCount"];
    NSDictionary* yearVIPTxnExtra = [self.allChartDataExtra objectForKey:@"yearVIPTxn"];
    NSDictionary* yearVIPOpsSaleExtra = [self.allChartDataExtra objectForKey:@"yearVIPOpsSale"];

    
    NSDictionary* filterweekVIPCount = @{
                                       @"type":@"week",
                                       @"nodes": [self filterVIPGradeData: weekVIPCount]
                                       };
    NSDictionary* filterweekVIPCountExtra = @{
                                         @"type":@"week",
                                         @"nodes": [self filterVIPGradeData: weekVIPCountExtra]
                                         };
    
    
    [self generateChartDiagramWithData: filterweekVIPCount compareData:filterweekVIPCountExtra title: [self getMixDataTitleFromData:filterweekVIPCount format:[ShareLocale textFromKey:@"Recent Daily New VIP Count(%@)"]]  view: [self.tab3View viewWithTag:10] category:@"vip_visitor_count" target:nil header:HEADER_DATE_VALUE_TOTAL displayScale:1 summaryFormat:nil referData:filterweekVIPCount negativeComparing:NO  sumTYPTD:nil sumLYPTD:nil sumLPPTD:nil];
    
    NSArray* weekVIPTxnSumArray = [self getSummaryValuesForSelectedFilters:@[] filterCategory:@"vip_txn" refName:@"refVIPTxn" period1Name:@"tywtd" period2Name:@"lywtd" period3Name:@"lwwtd"];
    NSDictionary* filterweekVIPTxn = @{
                                         @"type":@"week",
                                         @"nodes": [self filterVIPGradeData: weekVIPTxn]
                                         };
    NSDictionary* filterweekVIPTxnExtra = @{
                                       @"type":@"week",
                                       @"nodes": [self filterVIPGradeData: weekVIPTxnExtra]
                                       };
    [self generateChartDiagramWithData: filterweekVIPTxn compareData:filterweekVIPTxnExtra title: [self getMixDataTitleFromData:filterweekVIPTxn format:[ShareLocale textFromKey:@"Recent Daily VIP TXN Count(%@)"]]  view: [self.tab3View viewWithTag:11] category:@"vip_txn_count" target:nil header:HEADER_DATE_VALUE_TOTAL displayScale:1 summaryFormat:nil referData:filterweekVIPTxn  negativeComparing:NO sumTYPTD:[weekVIPTxnSumArray objectAtIndex:0] sumLYPTD:[weekVIPTxnSumArray objectAtIndex:1] sumLPPTD:[weekVIPTxnSumArray objectAtIndex:2]];
    
    
    
    NSArray* weekVIPSalesSumArray = [self getSummaryValuesForSelectedFilters:@[] filterCategory:@"vip_sales" refName:@"refVIPSales" period1Name:@"tywtd" period2Name:@"lywtd" period3Name:@"lwwtd"];
    NSDictionary* filterweekVIPSales = @{
                                          @"type":@"week",
                                          @"nodes": [self filterVIPGradeData: weekVIPOpsSale]
                                          };
    NSDictionary* filterweekVIPSalesExtra = @{
                                         @"type":@"week",
                                         @"nodes": [self filterVIPGradeData: weekVIPOpsSaleExtra]
                                         };

    [self generateChartDiagramWithData: filterweekVIPSales compareData:filterweekVIPSalesExtra title: [self getMixDataTitleFromData:filterweekVIPSales format: [ShareLocale textFromKey:@"Recent Daily VIP Ops Sales(%@)" ]]  view: [self.tab3View viewWithTag:12] category:@"vip_ops_sales" target:nil header:HEADER_DATE_VALUE_TOTAL displayScale:1 summaryFormat:nil referData:filterweekVIPSales negativeComparing:NO  sumTYPTD:[weekVIPSalesSumArray objectAtIndex:0] sumLYPTD:[weekVIPSalesSumArray objectAtIndex:1] sumLPPTD:[weekVIPSalesSumArray objectAtIndex:2] ];

    
    
    NSDictionary* filtermonthVIPCount = @{
                                        @"type":@"month",
                                        @"nodes": [self filterVIPGradeData: monthVIPCount]
                                        };
    NSDictionary* filtermonthVIPCountExtra = @{
                                          @"type":@"month",
                                          @"nodes": [self filterVIPGradeData: monthVIPCountExtra]
                                          };
    [self generateChartDiagramWithData: filtermonthVIPCount compareData:filtermonthVIPCountExtra title: [self getMixDataTitleFromData:filtermonthVIPCount format:[ShareLocale textFromKey:@"Recent Weekly New VIP Count(%@)"]]  view: [self.tab3View viewWithTag:13] category:@"vip_visitor_count" target:nil header:HEADER_WEEK_VALUE_TOTAL displayScale:1 summaryFormat:nil referData:filtermonthVIPCount negativeComparing:NO  sumTYPTD:nil sumLYPTD:nil sumLPPTD:nil extraNode: [self generateExtraNodeForCategory:@"vip_visitor_count" type:@"month" targetCategory:nil]];
    
    

    NSArray* monthVIPTxnSumArray = [self getSummaryValuesForSelectedFilters:@[] filterCategory:@"vip_txn" refName:@"refVIPTxn" period1Name:@"tymtd" period2Name:@"lymtd" period3Name:@"lmmtd"];
    NSDictionary* filtermonthVIPTxn = @{
                                          @"type":@"month",
                                          @"nodes": [self filterVIPGradeData: monthVIPTxn]
                                          };
    NSDictionary* filtermonthVIPTxnExtra = @{
                                        @"type":@"month",
                                        @"nodes": [self filterVIPGradeData: monthVIPTxnExtra]
                                        };

    [self generateChartDiagramWithData: filtermonthVIPTxn compareData:filtermonthVIPTxnExtra title: [self getMixDataTitleFromData:filtermonthVIPTxn format:[ShareLocale textFromKey:@"Recent Weekly VIP TXN Count(%@)"]]  view: [self.tab3View viewWithTag:14] category:@"vip_txn_count" target:nil header:HEADER_WEEK_VALUE_TOTAL displayScale:1 summaryFormat:nil referData:filtermonthVIPTxn negativeComparing:NO  sumTYPTD:monthVIPTxnSumArray[0] sumLYPTD:monthVIPTxnSumArray[1] sumLPPTD:monthVIPTxnSumArray[2] extraNode:  @{@"is_null":@NO, @"val": weekVIPTxnSumArray[0], @"period":[NSString stringWithFormat:@"%d-%d", self.fxYear, self.fxWeek]}];
    

    NSArray* monthVIPSalesSumArray = [self getSummaryValuesForSelectedFilters:@[] filterCategory:@"vip_sales" refName:@"refVIPSales" period1Name:@"tymtd" period2Name:@"lymtd" period3Name:@"lmmtd"];

    NSDictionary* filtermonthVIPSales = @{
                                         @"type":@"month",
                                         @"nodes": [self filterVIPGradeData: monthVIPOpsSale]
                                         };
    NSDictionary* filtermonthVIPSalesExtra = @{
                                          @"type":@"month",
                                          @"nodes": [self filterVIPGradeData: monthVIPOpsSaleExtra]
                                          };
    [self generateChartDiagramWithData: filtermonthVIPSales compareData:filtermonthVIPSalesExtra title: [self getMixDataTitleFromData:filtermonthVIPSales format:[ShareLocale textFromKey:@"Recent Weekly VIP Ops Sales(%@)" ]]   view: [self.tab3View viewWithTag:15] category:@"vip_ops_sales" target:nil header:HEADER_WEEK_VALUE_TOTAL displayScale:1 summaryFormat:nil referData:filtermonthVIPSales negativeComparing:NO  sumTYPTD:monthVIPSalesSumArray[0] sumLYPTD:monthVIPSalesSumArray[1] sumLPPTD:monthVIPSalesSumArray[2] extraNode:  @{@"is_null":@NO, @"val": weekVIPSalesSumArray[0], @"period":[NSString stringWithFormat:@"%d-%d", self.fxYear, self.fxWeek]}];
    
    
    
    NSDictionary* filteryearVIPCount = @{
                                           @"type":@"year",
                                           @"nodes": [self filterVIPGradeData: yearVIPCount]
                                           };
    NSDictionary* filteryearVIPCountExtra = @{
                                         @"type":@"year",
                                         @"nodes": [self filterVIPGradeData: yearVIPCountExtra]
                                         };
    [self generateChartDiagramWithData: filteryearVIPCount compareData:filteryearVIPCountExtra  title: [self getMixDataTitleFromData:filteryearVIPCount format:[ShareLocale textFromKey:@"Recent Monthly New VIP Count(%@)"]]  view: [self.tab3View viewWithTag:16] category:@"vip_visitor_count" target:nil header:HEADER_MONT_VALUE_TOTAL displayScale:1 summaryFormat:nil referData:filteryearVIPCount negativeComparing:NO  sumTYPTD:nil sumLYPTD:nil sumLPPTD:nil extraNode:[self generateExtraNodeForCategory:@"vip_visitor_count" type:@"year" targetCategory:nil]];
    
    
    NSArray* yearVIPTxnSumArray = [self getSummaryValuesForSelectedFilters:@[] filterCategory:@"vip_txn" refName:@"refVIPTxn" period1Name:@"tyytd" period2Name:@"lyytd" period3Name:@""];
    NSDictionary* filteryearVIPTxnSale = @{
                                           @"type":@"year",
                                           @"nodes": [self filterVIPGradeData: yearVIPTxn]
                                           };
    NSDictionary* filteryearVIPTxnSaleExtra = @{
                                           @"type":@"year",
                                           @"nodes": [self filterVIPGradeData: yearVIPTxnExtra]
                                           };
    [self generateChartDiagramWithData: filteryearVIPTxnSale compareData:filteryearVIPTxnSaleExtra title: [self getMixDataTitleFromData:filteryearVIPCount format:[ShareLocale textFromKey:@"Recent Monthly VIP TXN Count(%@)"]]  view: [self.tab3View viewWithTag:17] category:@"vip_txn_count" target:nil header:HEADER_MONT_VALUE_TOTAL displayScale:1 summaryFormat:nil referData:filteryearVIPTxnSale  negativeComparing:NO  sumTYPTD:yearVIPTxnSumArray[0] sumLYPTD:yearVIPTxnSumArray[1] sumLPPTD:yearVIPTxnSumArray[2] extraNode:  @{@"is_null":@NO, @"val": monthVIPTxnSumArray[0], @"period":[NSString stringWithFormat:@"%d-%d", self.fxYear, self.fxMonth]}];
    
    
    NSArray* yearVIPSalesSumArray = [self getSummaryValuesForSelectedFilters:@[] filterCategory:@"vip_sales" refName:@"refVIPSales" period1Name:@"tyytd" period2Name:@"lyytd" period3Name:@""];
    NSDictionary* filteryearVIPOpsSale = @{
                                           @"type":@"year",
                                           @"nodes": [self filterVIPGradeData: yearVIPOpsSale]
                                           };
    NSDictionary* filteryearVIPOpsSaleExtra = @{
                                           @"type":@"year",
                                           @"nodes": [self filterVIPGradeData: yearVIPOpsSaleExtra]
                                           };
    [self generateChartDiagramWithData: filteryearVIPOpsSale compareData:filteryearVIPOpsSaleExtra  title: [self getMixDataTitleFromData:filteryearVIPOpsSale format:[ShareLocale textFromKey:@"Recent Monthly VIP Ops Sales(%@)" ]]  view: [self.tab3View viewWithTag:18] category:@"vip_ops_sales" target:nil header:HEADER_MONT_VALUE_TOTAL displayScale:1 summaryFormat:nil referData:filteryearVIPOpsSale negativeComparing:NO  sumTYPTD:yearVIPSalesSumArray[0] sumLYPTD:yearVIPSalesSumArray[1] sumLPPTD:yearVIPSalesSumArray[2] extraNode:  @{@"is_null":@NO, @"val": monthVIPSalesSumArray[0], @"period":[NSString stringWithFormat:@"%d-%d", self.fxYear, self.fxMonth]}];

    
    NSMutableArray* defaultVIPOnlyOptions = [NSMutableArray array];
    for(NSString* vipCode in self.vipGradeArray){
        BOOL found = NO;
        for(NSString* customerCode in self.nonVipGrades){
            if([vipCode isEqualToString:customerCode]){
                found = YES;
            }
        }
        if(!found) [defaultVIPOnlyOptions addObject:@{ @"Label":vipCode, @"grade":vipCode }];
    }

    
    self.selectedDailyVIPCountOptions = nil;
    self.selectedWeeklyVIPCountOptions = nil;
    self.selectedMonthlyVIPCountOptions = nil;
    self.selectedDailyVIPTxnCountOptions = nil;
    self.selectedWeeklyVIPTxnCountOptions = nil;
    self.selectedMonthlyVIPTxnCountOptions = nil;
    self.selectedDailyVIPSalesOptions = nil;
    self.selectedWeeklyVIPSalesOptions = nil;
    self.selectedMonthlyVIPSalesOptions = nil;

}

//not used
-(void) reloadTab4View{

    [self.contentScrollView addSubview:self.tab4View];
    //    self.tab4View.frame = TAB_VIEW_SIZE;
    curDashboardView = self.tab4View;
    self.contentScrollView.contentSize = CGSizeMake(983,580);

    
}


-(void)copyVIPGradeFromData:(NSDictionary*) data toGradeSet:(NSMutableSet*)allGrades{
    NSArray* nodes = [data objectForKey:@"nodes"];
    for(NSDictionary* n in nodes){
        if([[n objectForKey:@"is_null"] intValue]==0){
            NSString* p = [n objectForKey:@"period"];
            NSArray* parts = [p componentsSeparatedByString:@":"];
            NSString* g = [parts objectAtIndex:0];
            if(g!=nil && ![g isEqualToString:@"null"]) [allGrades addObject:g];
        }
    }
}


//For Tab SalesMix Info sleection box
-(void)copyGenderAndCategoryFromData:(NSDictionary*) data toCategorySet:(NSMutableSet*)allCats toGenderSet:(NSMutableSet*)allGenders toCatGenSet:(NSMutableSet*)allCatGens{
    NSArray* nodes = [data objectForKey:@"nodes"];
    for(NSDictionary* n in nodes){
        if([[n objectForKey:@"is_null"] intValue]==0){
            NSString* p = [n objectForKey:@"period"];
            NSArray* parts = [p componentsSeparatedByString:@":"];
            NSString* c = [parts objectAtIndex:0];
            NSString* g = [parts objectAtIndex:1];
            if(c!=nil && ![c isEqualToString:@"null"]) [allCats addObject:c];
            if(g!=nil && ![g isEqualToString:@"null"]) [allGenders addObject:g];
            if(c!=nil && ![c isEqualToString:@"null"] &&
               g!=nil && ![g isEqualToString:@"null"]){
                [allCatGens addObject:[NSString stringWithFormat:@"%@ %@", c, g]];
            }
        }
    }
}

-(NSString*) getMixDataTitleFromData:(NSDictionary*)data format:(NSString*)format{
    //NSString* format = [data objectForKey:@"title"];
    
    if([self.selectedFilterOptions count]==0){
        return [NSString stringWithFormat:format, [ShareLocale textFromKey:@"all"]];
    }else{
        return [NSString stringWithFormat:format, [NSString stringWithFormat: [ShareLocale textFromKey:@"n_selected"], [self.selectedFilterOptions count]] ];
    }
    
//    if(self.categoryFilter==nil && self.genderFilter==nil){
//        return [NSString stringWithFormat:format, @"ALL"];
//    }
//    else if(self.categoryFilter!=nil && self.genderFilter==nil){
//        return [NSString stringWithFormat:format, self.categoryFilter];
//    }
//    else if(self.genderFilter!=nil && self.categoryFilter==nil){
//        return [NSString stringWithFormat:format, self.genderFilter];
//    }else{
//        NSString* combinedFilter = [NSString stringWithFormat:@"%@ and %@", self.categoryFilter, self.genderFilter];
//        return [NSString stringWithFormat:format, combinedFilter];
//    }
}

//Tab5: Sales Mix
-(void) reloadTab7View{
    
    self.selectedDailyQuantityMixOptions = nil;
    self.selectedWeeklyQuantityMixOptions = nil;
    self.selectedMonthlyQuantityMixOptions = nil;
    self.selectedDailySalesMixOptions = nil;
    self.selectedWeeklySalesMixOptions = nil;
    self.selectedMonthlySalesMixOptions = nil;
    self.selectedGlobalSalesMixOptions = nil;
    
    [self.contentScrollView addSubview:self.tab7View];
    
    self.contentScrollView.contentSize = CGSizeMake(983,580*3);
    curDashboardView = self.tab7View;
    
//    self.categoryFilter = nil;
//    self.genderFilter = nil;
    self.selectedFilterOptions = [NSMutableArray array];
    
    NSMutableSet* allCats = [NSMutableSet set];
    NSMutableSet* allGenders = [NSMutableSet set];
    NSMutableSet* allCatGens = [NSMutableSet set];
    
    [self.categoryArray removeAllObjects];
    [self.genderArray removeAllObjects];
    [self.catgenArray removeAllObjects];
    
    NSDictionary* weekSalesMix = [self.allChartData objectForKey:@"weekSalesMix"];
    NSDictionary* monthSalesMix = [self.allChartData objectForKey:@"monthSalesMix"];
    NSDictionary* yearSalesMix = [self.allChartData objectForKey:@"yearSalesMix"];
    NSDictionary* weekSalesQuantity = [self.allChartData objectForKey:@"weekQuantityMix"];
    NSDictionary* monthQuantityMix = [self.allChartData objectForKey:@"monthQuantityMix"];
    NSDictionary* yearQuantityMix = [self.allChartData objectForKey:@"yearQuantityMix"];
    [self copyGenderAndCategoryFromData:weekSalesMix toCategorySet:allCats toGenderSet:allGenders toCatGenSet:allCatGens];
    [self copyGenderAndCategoryFromData:monthSalesMix toCategorySet:allCats toGenderSet:allGenders toCatGenSet:allCatGens];
    [self copyGenderAndCategoryFromData:yearSalesMix toCategorySet:allCats toGenderSet:allGenders toCatGenSet:allCatGens ];
    [self copyGenderAndCategoryFromData:weekSalesQuantity toCategorySet:allCats toGenderSet:allGenders toCatGenSet:allCatGens];
    [self copyGenderAndCategoryFromData:monthQuantityMix toCategorySet:allCats toGenderSet:allGenders toCatGenSet:allCatGens];
    [self copyGenderAndCategoryFromData:yearQuantityMix toCategorySet:allCats toGenderSet:allGenders toCatGenSet:allCatGens];
    [self.catgenArray addObjectsFromArray: [allCatGens allObjects]];
    [self.categoryArray addObjectsFromArray:[allCats allObjects]];
    [self.genderArray addObjectsFromArray:[allGenders allObjects]];

    //Line2
    NSDictionary* weekSalesMixExtra = [self.allChartDataExtra objectForKey:@"weekSalesMix"];
    NSDictionary* monthSalesMixExtra = [self.allChartDataExtra objectForKey:@"monthSalesMix"];
    NSDictionary* yearSalesMixExtra = [self.allChartDataExtra objectForKey:@"yearSalesMix"];
    NSDictionary* weekSalesQuantityExtra = [self.allChartDataExtra objectForKey:@"weekQuantityMix"];
    NSDictionary* monthQuantityMixExtra = [self.allChartDataExtra objectForKey:@"monthQuantityMix"];
    NSDictionary* yearQuantityMixExtra = [self.allChartDataExtra objectForKey:@"yearQuantityMix"];

    
    NSDictionary* filteredWeekSales = @{
                                  @"type":@"week",
                                  @"nodes": [self filterMixedData: weekSalesMix]
                                  };
    NSDictionary* filteredWeekSalesExtra = @{
                                        @"type":@"week",
                                        @"nodes": [self filterMixedData: weekSalesMixExtra]
                                        };
    
    NSArray* weekSalesMixSumArray = [self getSummaryValuesForSelectedFilters:@[] filterCategory:@"mix_sales" refName:@"refSalesMix" period1Name:@"tywtd" period2Name:@"lywtd" period3Name:@"lwwtd"];

    [self generateChartDiagramWithData: filteredWeekSales compareData:filteredWeekSalesExtra title:[self getMixDataTitleFromData:weekSalesMix format:[ShareLocale textFromKey:@"Recent Daily Ops Sales(%@)"]] view: [self.tab7View viewWithTag:11] category:@"NA" target:nil header:HEADER_DATE_VALUE_TOTAL displayScale:1 summaryFormat:nil referData:filteredWeekSales negativeComparing:NO sumTYPTD:[weekSalesMixSumArray objectAtIndex:0] sumLYPTD:[weekSalesMixSumArray objectAtIndex:1] sumLPPTD:[weekSalesMixSumArray objectAtIndex:2]];
    
    
    
    NSDictionary* filteredMonthSales = @{
                                       @"type":@"month",
                                       @"nodes": [self filterMixedData: monthSalesMix]
                                       };
    NSDictionary* filteredMonthSalesExtra = @{
                                         @"type":@"month",
                                         @"nodes": [self filterMixedData: monthSalesMixExtra]
                                         };
    NSArray* monthSalesMixSumArray = [self getSummaryValuesForSelectedFilters:@[] filterCategory:@"mix_sales" refName:@"refSalesMix" period1Name:@"tymtd" period2Name:@"lymtd" period3Name:@"lmmtd"];
    [self generateChartDiagramWithData: filteredMonthSales compareData:filteredMonthSalesExtra title:[self getMixDataTitleFromData:monthSalesMix format:[ShareLocale textFromKey:@"Recent Weekly Ops Sales(%@)"]] view: [self.tab7View viewWithTag:14] category:@"NA" target:nil header:HEADER_WEEK_VALUE_TOTAL displayScale:1 summaryFormat:nil referData:filteredMonthSales negativeComparing:NO sumTYPTD:monthSalesMixSumArray[0] sumLYPTD:monthSalesMixSumArray[1] sumLPPTD:monthSalesMixSumArray[2] extraNode:  @{@"is_null":@NO, @"val": weekSalesMixSumArray[0], @"period":[NSString stringWithFormat:@"%d-%d", self.fxYear, self.fxWeek]}];
    
    
    NSDictionary* filteredYearSales = @{
                                         @"type":@"year",
                                         @"nodes": [self filterMixedData: yearSalesMix]
                                         };
    NSDictionary* filteredYearSalesExtra = @{
                                        @"type":@"year",
                                        @"nodes": [self filterMixedData: yearSalesMixExtra]
                                        };
    NSArray* yearSalesMixSumArray = [self getSummaryValuesForSelectedFilters:@[] filterCategory:@"mix_sales" refName:@"refSalesMix" period1Name:@"tyytd" period2Name:@"lyytd" period3Name:@""];

    [self generateChartDiagramWithData: filteredYearSales compareData:filteredYearSalesExtra title:[self getMixDataTitleFromData:yearSalesMix format:[ShareLocale textFromKey:@"Recent Monthly Ops Sales(%@)"]] view: [self.tab7View viewWithTag:17] category:@"NA" target:nil header:HEADER_MONT_VALUE_TOTAL  displayScale:1 summaryFormat:nil referData:filteredYearSales negativeComparing:NO sumTYPTD:yearSalesMixSumArray[0] sumLYPTD:yearSalesMixSumArray[1] sumLPPTD:yearSalesMixSumArray[2] extraNode:  @{@"is_null":@NO, @"val": monthSalesMixSumArray[0], @"period":[NSString stringWithFormat:@"%d-%d", self.fxYear, self.fxMonth]}];
   
    
    
    
    
    NSDictionary* filteredWeekQuantity = @{
                                        @"type":@"week",
                                        @"nodes": [self filterMixedData: weekSalesQuantity]
                                        };
    NSDictionary* filteredWeekQuantityExtra = @{
                                           @"type":@"week",
                                           @"nodes": [self filterMixedData: weekSalesQuantityExtra]
                                           };
    NSArray* weekQtyMixSumArray = [self getSummaryValuesForSelectedFilters:@[] filterCategory:@"mix_qty" refName:@"refQuantityMix" period1Name:@"tywtd" period2Name:@"lywtd" period3Name:@"lwwtd"];

    [self generateChartDiagramWithData: filteredWeekQuantity compareData:filteredWeekQuantityExtra title:[self getMixDataTitleFromData:weekSalesQuantity format:[ShareLocale textFromKey:@"Recent Daily Sales Quantity(%@)"]] view: [self.tab7View viewWithTag:10] category:@"NA" target:nil header:HEADER_DATE_VALUE_TOTAL  displayScale:1 summaryFormat:nil referData:filteredWeekQuantity negativeComparing:NO sumTYPTD:[weekQtyMixSumArray objectAtIndex:0] sumLYPTD:[weekQtyMixSumArray objectAtIndex:1] sumLPPTD:[weekQtyMixSumArray objectAtIndex:2]];

    
    NSDictionary* filteredMonthQuantity = @{
                                         @"type":@"month",
                                         @"nodes": [self filterMixedData: monthQuantityMix]
                                         };
    NSDictionary* filteredMonthQuantityExtra = @{
                                            @"type":@"month",
                                            @"nodes": [self filterMixedData: monthQuantityMixExtra]
                                            };
    NSArray* monthQtyMixSumArray = [self getSummaryValuesForSelectedFilters:@[] filterCategory:@"mix_qty" refName:@"refQuantityMix" period1Name:@"tymtd" period2Name:@"lymtd" period3Name:@"lmmtd"];

    [self generateChartDiagramWithData: filteredMonthQuantity compareData:filteredMonthQuantityExtra title:[self getMixDataTitleFromData:monthQuantityMix format:[ShareLocale textFromKey:@"Recent Weekly Sales Quantity(%@)"] ] view: [self.tab7View viewWithTag:13] category:@"NA" target:nil header:HEADER_WEEK_VALUE_TOTAL displayScale:1 summaryFormat:nil referData:filteredMonthQuantity negativeComparing:NO sumTYPTD:monthQtyMixSumArray[0] sumLYPTD:monthQtyMixSumArray[1] sumLPPTD:monthQtyMixSumArray[2]  extraNode:  @{@"is_null":@NO, @"val": weekQtyMixSumArray[0], @"period":[NSString stringWithFormat:@"%d-%d", self.fxYear, self.fxWeek]}];
    
    
    NSDictionary* filteredYearQuantity = @{
                                        @"type":@"year",
                                        @"nodes": [self filterMixedData: yearQuantityMix]
                                        };
    NSDictionary* filteredYearQuantityExtra = @{
                                           @"type":@"year",
                                           @"nodes": [self filterMixedData: yearQuantityMixExtra]
                                           };
    NSArray* yearQtyMixSumArray = [self getSummaryValuesForSelectedFilters:@[] filterCategory:@"mix_qty" refName:@"refQuantityMix" period1Name:@"tyytd" period2Name:@"lyytd" period3Name:@""];

    [self generateChartDiagramWithData: filteredYearQuantity compareData:filteredYearQuantityExtra title:[self getMixDataTitleFromData:yearQuantityMix  format:[ShareLocale textFromKey:@"Recent Monthly Sales Quantity(%@)"] ] view: [self.tab7View viewWithTag:16] category:@"NA" target:nil header:HEADER_MONT_VALUE_TOTAL displayScale:1 summaryFormat:nil referData:filteredYearQuantity negativeComparing:NO sumTYPTD:yearQtyMixSumArray[0] sumLYPTD:yearQtyMixSumArray[1] sumLPPTD:yearQtyMixSumArray[2] extraNode:  @{@"is_null":@NO, @"val": monthQtyMixSumArray[0], @"period":[NSString stringWithFormat:@"%d-%d", self.fxYear, self.fxMonth]}];

}


//Tab1: Sales Info
-(void) reloadTab8View{
    
    [self.contentScrollView addSubview:self.tab8View];
    self.contentScrollView.contentSize = CGSizeMake(983,580*3);
    curDashboardView = self.tab8View;
    
    
    NSDictionary* weekOpsSale = [self.allChartData objectForKey:@"weekOpsSale"];
    NSDictionary* weekOpsSaleExtra = [self.allChartDataExtra objectForKey:@"weekOpsSale"];
    [self generateChartDiagramWithData: weekOpsSale compareData:weekOpsSaleExtra title: [ShareLocale textFromKey:@"Recent Daily Ops Sales"]  view: [self.tab8View viewWithTag:10] category:@"ops_sales" target:[self getTargetDataWithType:@"wtd" category:@"OPS_Sales_Amount__c"] header:HEADER_DATE_VALUE_TARGET];
    
    NSDictionary* weekDiscount = [self.allChartData objectForKey:@"weekDiscount"];
    NSDictionary* weekDiscountExtra = [self.allChartDataExtra objectForKey:@"weekDiscount"];
    [self generateChartDiagramWithData: weekDiscount compareData:weekDiscountExtra title: [ShareLocale textFromKey:@"Recent Daily Discount"]  view: [self.tab8View viewWithTag:11] category:@"discount" target:[self getTargetDataWithType:@"wtd" category:@"Discount__c"] header:HEADER_DATE_VALUE_TARGET displayScale:100 summaryFormat:@"%.2f%%" referData:nil negativeComparing:YES   sumTYPTD:nil sumLYPTD:nil sumLPPTD:nil];
    
    NSDictionary* weekSalesProd = [self.allChartData objectForKey:@"weekSalesProd"];
    NSDictionary* weekSalesProdExtra = [self.allChartDataExtra objectForKey:@"weekSalesProd"];
    [self generateChartDiagramWithData: weekSalesProd compareData:weekSalesProdExtra title: [ShareLocale textFromKey:@"Recent Daily Store Productivity"]  view: [self.tab8View viewWithTag:12] category:@"sales_prod" target:nil header:HEADER_DATE_VALUE_TARGET];
 
    NSDictionary* monthOpsSale = [self.allChartData objectForKey:@"monthOpsSale"];
    NSDictionary* monthOpsSaleExtra = [self.allChartDataExtra objectForKey:@"monthOpsSale"];
    [self generateChartDiagramWithData: monthOpsSale compareData:monthOpsSaleExtra  title: [ShareLocale textFromKey:@"Recent Weekly Ops Sales"]  view: [self.tab8View viewWithTag:13] category:@"ops_sales" target:[self getTargetDataWithType:@"mtd" category:@"OPS_Sales_Amount__c"] header:HEADER_WEEK_VALUE_TARGET displayScale:1 summaryFormat:@"%.2f" referData:nil negativeComparing:NO sumTYPTD:nil sumLYPTD:nil sumLPPTD:nil extraNode:[self generateExtraNodeForCategory:@"ops_sales" type:@"month" targetCategory:@"OPS_Sales_Amount__c" ] ];
    
    NSDictionary* monthDiscount = [self.allChartData objectForKey:@"monthDiscount"];
    NSDictionary* monthDiscountExtra = [self.allChartDataExtra objectForKey:@"monthDiscount"];
    [self generateChartDiagramWithData: monthDiscount compareData:monthDiscountExtra  title: [ShareLocale textFromKey:@"Recent Weekly Discount"]   view: [self.tab8View viewWithTag:14] category:@"discount" target:[self getTargetDataWithType:@"mtd" category:@"Discount__c"] header:HEADER_WEEK_VALUE_TARGET displayScale:100  summaryFormat:@"%.2f%%" referData:nil negativeComparing:YES   sumTYPTD:nil sumLYPTD:nil sumLPPTD:nil extraNode:[self generateExtraNodeForCategory:@"discount" type:@"month" targetCategory:@"Discount__c"]];
    
    NSDictionary* monthSalesProd = [self.allChartData objectForKey:@"monthSalesProd"];
    NSDictionary* monthSalesExtra = [self.allChartDataExtra objectForKey:@"monthSalesProd"];
    [self generateChartDiagramWithData: monthSalesProd compareData:monthSalesExtra  title: [ShareLocale textFromKey:@"Recent Weekly Store Productivity"]   view: [self.tab8View viewWithTag:15] category:@"sales_prod" target:nil header:HEADER_WEEK_VALUE_TARGET displayScale:1 summaryFormat:@"%.2f" referData:nil negativeComparing:NO sumTYPTD:nil sumLYPTD:nil sumLPPTD:nil extraNode:[self generateExtraNodeForCategory:@"sales_prod" type:@"month" targetCategory:nil]];
    
    NSDictionary* yearOpsSale = [self.allChartData objectForKey:@"yearOpsSale"];
    NSDictionary* yearOpsSaleExtra = [self.allChartDataExtra objectForKey:@"yearOpsSale"];
    [self generateChartDiagramWithData: yearOpsSale compareData:yearOpsSaleExtra  title: [ShareLocale textFromKey:@"Recent Monthly Ops Sales"]  view: [self.tab8View viewWithTag:16] category:@"ops_sales" target:[self getTargetDataWithType:@"ytd" category:@"OPS_Sales_Amount__c"] header:HEADER_MONT_VALUE_TARGET displayScale:1 summaryFormat:@"%.2f" referData:nil negativeComparing:NO sumTYPTD:nil sumLYPTD:nil sumLPPTD:nil extraNode:[self generateExtraNodeForCategory:@"ops_sales" type:@"year" targetCategory:@"OPS_Sales_Amount__c" ]];
    
    NSDictionary* yearDiscount = [self.allChartData objectForKey:@"yearDiscount"];
    NSDictionary* yearDiscountExtra = [self.allChartDataExtra objectForKey:@"yearDiscount"];
    [self generateChartDiagramWithData: yearDiscount compareData:yearDiscountExtra  title: [ShareLocale textFromKey:@"Recent Monthly Discount"]   view: [self.tab8View viewWithTag:17] category:@"discount" target:[self getTargetDataWithType:@"ytd" category:@"Discount__c"] header:HEADER_MONT_VALUE_TARGET displayScale:100 summaryFormat:@"%.2f%%" referData:nil negativeComparing:YES   sumTYPTD:nil sumLYPTD:nil sumLPPTD:nil extraNode:[self generateExtraNodeForCategory:@"discount" type:@"year" targetCategory:@"Discount__c"]];
    
    NSDictionary* yearSalesProd = [self.allChartData objectForKey:@"yearSalesProd"];
    NSDictionary* yearSalesProdExtra = [self.allChartDataExtra objectForKey:@"yearSalesProd"];
    [self generateChartDiagramWithData: yearSalesProd compareData:yearSalesProdExtra  title: [ShareLocale textFromKey:@"Recent Monthly Store Productivity"]   view: [self.tab8View viewWithTag:18] category:@"sales_prod" target:nil header:HEADER_MONT_VALUE_TARGET displayScale:1 summaryFormat:@"%.2f" referData:nil negativeComparing:NO sumTYPTD:nil sumLYPTD:nil sumLPPTD:nil extraNode:[self generateExtraNodeForCategory:@"sales_prod" type:@"year" targetCategory:nil]];
    
}



-(NSDictionary*)generateExtraNodeForCategory:(NSString*)cat type:(NSString*)type targetCategory:(NSString*)category{
    VFDAO* vf = [VFDAO sharedInstance];
    NSMutableDictionary* defaults = vf.cache;
    NSDateFormatter *df = [[[NSDateFormatter alloc] init] autorelease];
    NSDictionary* storeDataMap = [defaults objectForKey: @"store_daily_analyse"];
    NSString* code = self.storeData[@"Store_Code__c"];
    if(code==nil) code = self.storeData[@"code"];
    NSDictionary* storeDataMap2 = storeDataMap[code];
    NSDictionary* dayInfoMap = storeDataMap2[@"dayInfoMap"];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSDate* fxDate = [df dateFromString:self.fxDate];
    [df setDateFormat:@"yyyy-M-d"];
    NSString* fxDateStr = [df stringFromDate: fxDate];
    NSDictionary* summaryReport = dayInfoMap[fxDateStr];
    
    NSNumber* extraNodeVal = nil;
    NSNumber* extraNodeTargetVal = nil;
    if(summaryReport!=nil && summaryReport[cat]!=nil && summaryReport[cat]!=(id)[NSNull null]){
        if([type isEqualToString:@"month"]){
            extraNodeVal = summaryReport[cat][@"TYWTD"];
            if(category!=nil) extraNodeTargetVal = [self getTargetDataWithType:@"wtd" category:category];
        }
        else if ([type isEqualToString:@"year"]){
            extraNodeVal = summaryReport[cat][@"TYMTD"];
            if(category!=nil) extraNodeTargetVal = [self getTargetDataWithType:@"mtd" category:category];
        }
    }
    if(extraNodeTargetVal==nil){
        extraNodeTargetVal = @0;
    }
    if(extraNodeVal==nil){
        extraNodeVal = @0;
    }

    NSString* period = nil;
    if([type isEqualToString:@"month"]){//2014-33
        int yr = self.fxYear;
        int wk = self.fxWeek;
        period = [NSString stringWithFormat:@"%d-%d", yr,wk];
    }
    else if ([type isEqualToString:@"year"]){//2014-7
        int yr = self.fxYear;
        int mo = self.fxMonth;
        period = [NSString stringWithFormat:@"%d-%d", yr,mo];
    }else{
        NSAssert(NO, @"type is not valid. Must be onth or year");
        return nil;
    }
    return @{
              @"is_null": @NO,
              @"period": period,
              @"target": extraNodeTargetVal,
              @"val": extraNodeVal
              };

}


-(NSArray*) filterVIPGradeData:(NSDictionary*) data{
    
    NSArray* nodes = [data objectForKey:@"nodes"];
    NSMutableDictionary* filteredNodeData = [[[NSMutableDictionary alloc] init] autorelease];
    NSMutableArray* filterNodes = [[[NSMutableArray alloc] init] autorelease];
    if(nodes!=nil && [nodes count]>0){
        for(NSDictionary* d in nodes){
            if([[d objectForKey:@"is_null"] intValue] == 0){
                NSString* key = [d objectForKey:@"period"];
                NSArray* keys = [key componentsSeparatedByString:@":"];
                NSString* grade = [keys objectAtIndex:0];
                NSString* rdate = [keys objectAtIndex:1];//for daily it is yyyy-m-d, for week/month, it is yyyy-x
                double val = [[d objectForKey:@"val"] doubleValue];
                
                BOOL match = NO;
                if([self.selectedFilterOptions count]==0){
                    //all
                    match = YES;
                }else{
                    
                    for(NSDictionary* filter in self.selectedFilterOptions){
                        NSString* c = [filter objectForKey:@"grade"];
                        if(c!=(id)[NSNull null] && [c isEqualToString:grade]){
                            match = YES;
                        }
                    }
                }
                
                if(match){
                    NSMutableDictionary* nodeAtDate = [filteredNodeData objectForKey:rdate];
                    if(nodeAtDate == nil || nodeAtDate == (id)[NSNull null]){
                        nodeAtDate = [[[NSMutableDictionary alloc] init] autorelease];
                        [nodeAtDate setObject:[NSNumber numberWithDouble:0] forKey:@"val"];
                        [nodeAtDate setObject:[NSNumber numberWithDouble:0] forKey:@"is_null"];
                        [nodeAtDate setObject:rdate forKey:@"period"];
                        [filterNodes addObject:nodeAtDate];
                        [filteredNodeData setObject:nodeAtDate forKey:rdate];
                    }
                    double d = ([[nodeAtDate objectForKey:@"val"] doubleValue] + val);
                    [nodeAtDate setObject:[NSNumber numberWithDouble:d] forKey:@"val"];
                }
            }
        }
    }
    
    
    
    
    return filterNodes;
}





-(NSArray*) filterMixedData:(NSDictionary*) data{
    
    NSArray* nodes = [data objectForKey:@"nodes"];
    NSMutableDictionary* filteredNodeData = [[[NSMutableDictionary alloc] init] autorelease];
    NSMutableArray* filterNodes = [[[NSMutableArray alloc] init] autorelease];
    if(nodes!=nil && [nodes count]>0){
        for(NSDictionary* d in nodes){
            if([[d objectForKey:@"is_null"] intValue] == 0){
                NSString* key = [d objectForKey:@"period"];
                NSArray* keys = [key componentsSeparatedByString:@":"];
                NSString* cat = [keys objectAtIndex:0];
                NSString* gender = [keys objectAtIndex:1];
                NSString* rdate = [keys objectAtIndex:2];//for daily it is yyyy-m-d, for week/month, it is yyyy-x
                double val = [[d objectForKey:@"val"] doubleValue];
                
                BOOL match = NO;
                if([self.selectedFilterOptions count]==0){
                    //all
                    match = YES;
                }else{

                    for(NSDictionary* filter in self.selectedFilterOptions){
                        NSString* g = [filter objectForKey:@"gender"];
                        NSString* c = [filter objectForKey:@"cat"];
                        if(g!=(id)[NSNull null] && [g isEqualToString:gender] &&
                           c!=(id)[NSNull null] && [c isEqualToString:cat]){
                            match = YES;
                        }
                        else if (g==(id)[NSNull null] && c!=(id)[NSNull null] && [c isEqualToString:cat]){
                            match = YES;
                        }
                        else if (c==(id)[NSNull null] && g!=(id)[NSNull null] && [g isEqualToString:gender]){
                            match = YES;
                        }
                    }
                }
                
                if(match){
                    NSMutableDictionary* nodeAtDate = [filteredNodeData objectForKey:rdate];
                    if(nodeAtDate == nil || nodeAtDate == (id)[NSNull null]){
                        nodeAtDate = [[[NSMutableDictionary alloc] init] autorelease];
                        [nodeAtDate setObject:[NSNumber numberWithDouble:0] forKey:@"val"];
                        [nodeAtDate setObject:[NSNumber numberWithDouble:0] forKey:@"is_null"];
                        [nodeAtDate setObject:rdate forKey:@"period"];
                        [filterNodes addObject:nodeAtDate];
                        [filteredNodeData setObject:nodeAtDate forKey:rdate];
                    }
                    double d = ([[nodeAtDate objectForKey:@"val"] doubleValue] + val);
                    [nodeAtDate setObject:[NSNumber numberWithDouble:d] forKey:@"val"];
                }
            }
        }
    }
    

    
    
    return filterNodes;
}





//Draw Diagram with given data

-(void) generateChartDiagramWithData:(NSDictionary*) data compareData:(NSDictionary*)compareData title:(NSString*)title  view:(UIView*)columnView category:(NSString*)category target:(NSNumber*)targetVal header:(NSString*) header{
    [self generateChartDiagramWithData:data compareData:compareData title:title view:columnView category:category target:targetVal header:header displayScale:1 summaryFormat:@"%.2f" referData:nil negativeComparing:NO   sumTYPTD:nil sumLYPTD:nil sumLPPTD:nil];
}




-(void) generateChartDiagramWithData: (NSDictionary*) data compareData:(NSDictionary*)compareData title:(NSString*)title  view:(UIView*)columnView category:(NSString*)category target:(NSNumber*)targetVal header:(NSString*) header displayScale:(float)scale{
    [self generateChartDiagramWithData:data compareData:compareData title:title view:columnView category:category target:targetVal header:header displayScale:scale summaryFormat:@"%.2f" referData:nil negativeComparing:NO   sumTYPTD:nil sumLYPTD:nil sumLPPTD:nil];
}

-(void) generateChartDiagramWithData: (NSDictionary*) data compareData:(NSDictionary*)compareData title:(NSString*)title view:(UIView*)columnView category:(NSString*)category target:(NSNumber*)targetVal header:(NSString*) header displayScale:(float)scale  referData:(NSDictionary*)referData negativeComparing:(BOOL)negCompFlag{
    [self generateChartDiagramWithData:data compareData:compareData title:title view:columnView category:category target:targetVal header:header displayScale:scale summaryFormat:@"%.2f" referData:referData negativeComparing:negCompFlag  sumTYPTD:nil sumLYPTD:nil sumLPPTD:nil];
}

-(void) generateChartDiagramWithData: (NSDictionary*) data compareData:(NSDictionary*)compareData title:(NSString*)title view:(UIView*)columnView category:(NSString*)category target:(NSNumber*)targetVal header:(NSString*) header displayScale:(float)scale summaryFormat:(NSString*)format referData:(NSDictionary*)referData negativeComparing:(BOOL)negCompFlag sumTYPTD:(NSNumber*)typtdVal sumLYPTD:(NSNumber*)lyptdVal sumLPPTD:(NSString*)lpptdVal{//p for period,soLYPTD may be LYWTD or LYMTD

    [self generateChartDiagramWithData:data compareData:compareData title:title view:columnView category:category target:targetVal header:header displayScale:scale summaryFormat:format referData:referData negativeComparing:negCompFlag sumTYPTD:typtdVal sumLYPTD:lyptdVal sumLPPTD:lpptdVal extraNode:nil];
    
}

-(void) generateChartDiagramWithData: (NSDictionary*) data compareData:(NSDictionary*)compareData title:(NSString*)title view:(UIView*)columnView category:(NSString*)category target:(NSNumber*)targetVal header:(NSString*) header displayScale:(float)scale summaryFormat:(NSString*)format referData:(NSDictionary*)referData negativeComparing:(BOOL)negCompFlag sumTYPTD:(NSNumber*)typtdVal sumLYPTD:(NSNumber*)lyptdVal sumLPPTD:(NSString*)lpptdVal extraNode:(NSDictionary*)extraNode{
    //p for period,soLYPTD may be LYWTD or LYMTD
    double startY = 0;
    double intervalX = 0;
    double intervalY = 0;
    double s = 1;
    
    
    NSArray* rawNodes = [NSMutableArray arrayWithArray: [data objectForKey:@"nodes"]];
    NSMutableArray* nodes = [[[NSMutableArray alloc] init] autorelease];
    NSMutableSet* periodSet = [[NSMutableSet alloc] init];
    for(NSDictionary* d in rawNodes){
        if([[d objectForKey:@"is_null"] intValue]==1){
            break;
        }
        double d1 = [[d objectForKey:@"val"] doubleValue];
        if(d1 <0) break;
        [nodes addObject: d];
        [periodSet addObject: d[@"period"] ];
    }

    //node sorting
    NSString* ptype = [data objectForKey:@"type"];
    [self sortChartNodeList:nodes type:ptype];

    //nodes are already sorted by date ascending
    //remove too old records if exist
    while([nodes count]>10){
        [nodes removeObjectAtIndex:0];
    }
    
    //Handle Comparable Data
    NSMutableDictionary* nodePeriodValMap = [NSMutableDictionary dictionary];
    NSMutableArray* extraNodes = [[[NSMutableArray alloc] init] autorelease];
    if(compareData!=nil){
        NSArray* extraRawNodes = [NSMutableArray arrayWithArray: [compareData objectForKey:@"nodes"]];
        //only add nodes with same period and not null
        for(NSDictionary* d in extraRawNodes){
            if([[d objectForKey:@"is_null"] intValue]==1){
                break;
            }
            double d1 = [[d objectForKey:@"val"] doubleValue];
            if(d1 <0) break;
            if([periodSet containsObject: d[@"period"]]){
                [extraNodes addObject: d];
                nodePeriodValMap[d[@"period"]] = [NSNumber numberWithFloat:d1];

            }
        }
        NSString* ptype = [compareData objectForKey:@"type"];
        [self sortChartNodeList:extraNodes type:ptype];
    }
    
    UIView* titleOptionView = [columnView viewWithTag:50];
    UIView* chartView = [columnView viewWithTag:51];    //empty white uiview
    if([[chartView subviews] count]>0){
        for(UIView* v in [chartView subviews]){
            [v removeFromSuperview];
        }
    }
    
    UIView* dataTableView = [columnView viewWithTag:52];
    for (UIView *view in [dataTableView subviews]) {
        [view removeFromSuperview];
    }
    dataTableView.frame = CGRectMake(dataTableView.frame.origin.x, dataTableView.frame.origin.y, dataTableView.frame.size.width, 240);

    UILabel* titleLabel = (UILabel*)[titleOptionView viewWithTag:10];
    titleLabel.text = title;
    titleLabel.minimumScaleFactor = 0.5;
    titleLabel.adjustsFontSizeToFitWidth = YES;

    //UIButton* changeButton = (UIButton*)[titleOptionView viewWithTag:1];
    //[changeButton setTitle:[ShareLocale textFromKey:@"change"] forState:UIControlStateNormal];
    
    if([nodes count]>0){
        //calculate the diagram display parameters
        double minY = 0;
        double maxY = 0;
        double avg = 0;
        minY = [[[nodes firstObject] objectForKey:@"val"] doubleValue];
        maxY = [[[nodes firstObject] objectForKey:@"val"] doubleValue];
        for(NSDictionary* p in nodes){
            if([[p objectForKey:@"is_null"] intValue]==1){
                break;
            }
            double c = [[p objectForKey:@"val"] doubleValue];
            avg += c;
            if(c>maxY){
                maxY = c;
            }
            if(c<minY){
                minY = c;
            }
        }
        for(NSDictionary* p in extraNodes){
            if([[p objectForKey:@"is_null"] intValue]==1){
                break;
            }
            double c = [[p objectForKey:@"val"] doubleValue];
            avg += c;
            if(c>maxY){
                maxY = c;
            }
            if(c<minY){
                minY = c;
            }
        }

        double range = maxY - minY;

        s = 1;
        avg = avg / ([nodes count]+[extraNodes count]);
        if(avg >=1000){
            s = 0.001;
        }
        else if(avg<=1){
            s = 100;
        }

        double interval = range / 5;
        double digitCount = log10(interval);
        if(isnan(digitCount) || !isfinite(digitCount)  ){
            intervalY = 0;
            startY = 0;
        }else{
            intervalY = floor(interval / pow(10, digitCount)) * pow(10, digitCount);
            digitCount = log10(minY) - 2;
            startY = floor(minY / pow(10, digitCount)) * pow(10, digitCount);
        }
        //for examples:
        //maxY=738, minY=530, then range=208, intervalY=40, startY=500
        //maxY=2.05, minY=1.79, then range=0.26, intervalY=0.05, startY=1.79
        
        
        
        
    }
    else{
        UILabel* hintLabel = [[UILabel alloc] initWithFrame: CGRectMake(20, 20, chartView.frame.size.width-40, chartView.frame.size.height-40)];
        hintLabel.textColor = [UIColor blackColor];
        hintLabel.textAlignment = NSTextAlignmentCenter;
        hintLabel.text = [ShareLocale textFromKey:@"diagram_not_available"];
        [chartView addSubview:hintLabel];
        [hintLabel release];
        return;
    }
    
    //Prepare Summary Data source
    VFDAO* vf = [VFDAO sharedInstance];
    NSMutableDictionary* defaults = vf.cache;
    NSDateFormatter *df = [[[NSDateFormatter alloc] init] autorelease];
    NSDictionary* storeDataMap = [defaults objectForKey: @"store_daily_analyse"];
    
    NSString* code = [self.storeData objectForKey:@"Store_Code__c"];
    if(code==nil) code = [self.storeData objectForKey:@"code"];
    NSDictionary* storeDataMap2 = [storeDataMap objectForKey:  code];
    
    NSDictionary* dayInfoMap = [storeDataMap2 objectForKey:@"dayInfoMap"];

    //TODO:
    //This part is quite strange...The format should be yyyy-MM-dd, don't know when it become yyyy-M-d here.
    [df setDateFormat:@"yyyy-MM-dd"];
    NSDate* fxDate = [df dateFromString:self.fxDate];
    [df setDateFormat:@"yyyy-M-d"];
    NSString* fxDateStr = [df stringFromDate: fxDate];
    self.fxDateLabel.text = [NSString stringWithFormat:[ShareLocale textFromKey:@"fs_date"], fxDateStr ];
    
    //prepare the 3 summary value on left side
    NSDictionary* summaryReport = [dayInfoMap objectForKey:  fxDateStr];
    
//    NSNumber* extraNodeVal = nil;
//    NSNumber* extraNodeTargetVal = @0;
//    if(summaryReport!=nil && summaryReport[category]!=nil && summaryReport[category]!=(id)[NSNull null]){
//        if([ptype isEqualToString:@"month"]){
//            extraNodeVal = summaryReport[category][@"TYWTD"];
//            extraNodeTargetVal = [self getTargetDataWithType:@"wtd" category:category];
//        }
//        else if ([ptype isEqualToString:@"year"]){
//            extraNodeVal = summaryReport[category][@"TYMTD"];
//            extraNodeTargetVal = [self getTargetDataWithType:@"mtd" category:category];
//        }
//    }
    
    
    //fill in table like list view
    [dataTableView clipsToBounds];
    int offsetX=10;
    int offsetY=10;

    [self addNewLabelToView:dataTableView text:header x:offsetX y:offsetY];
    int indexY = 1;

    NSArray* referNodes = [referData objectForKey:@"nodes"];
    
    //Add extra node
    NSMutableArray* nodesInTable = [nodes mutableCopy];
    if(extraNode!=nil){
        [nodesInTable addObject: extraNode];
    }
    

    for(NSDictionary* node in nodesInTable){
        if([[node objectForKey:@"is_null"] intValue] == 0){
            double val = [[node objectForKey:@"val"] doubleValue];
            double target = 0;
            
            NSString* p = [node objectForKey:@"period"];
            
            if(referNodes==nil){
                if([node objectForKey:@"target"]!=nil && [node objectForKey:@"target"]!=(id)[NSNull null]){
                    target = [[node objectForKey:@"target"] doubleValue];
                }
            }else{
                for(NSDictionary* rnode in referNodes){
                    if([[rnode objectForKey:@"is_null"] intValue] == 0){
                        NSString* rp = [rnode objectForKey:@"period"];
                        if([p isEqualToString:rp]){
                            target = [[rnode objectForKey:@"val"] doubleValue];
                        }
                    }
                }
            }
            
            NSMutableString* period = [NSMutableString stringWithString:p];
            while([period length]<10){
                [period appendString: @" "];
            }
            NSString* valStr = [NSString stringWithFormat:@"%@ %.2f", period, val*scale ];
            if (val*scale>=1000*1000){
                valStr = [NSString stringWithFormat:@"%@ %.2fm", period, val*scale/1000/1000 ];
            }else if(val*scale>=1000){
                valStr = [NSString stringWithFormat:@"%@ %.2fk", period, val*scale/1000 ];
            }
            
            NSString* targetStr;
            NSString* targetPCTStr;
            if(target==0){
                targetStr = @"-";
                targetPCTStr = @"-";
            }
            else{
                if(target*scale>=1000*1000){
                    targetStr = [NSString stringWithFormat:@"%.2fm", target*scale/1000/1000];
                }else if (target*scale>=1000){
                    targetStr = [NSString stringWithFormat:@"%.2fk", target*scale/1000];
                }else{
                    targetStr = [NSString stringWithFormat:@"%.2f", target*scale];
                }
                targetPCTStr = [NSString stringWithFormat:@"%.1f%%", val / target*100];
            }
            
            [self addNewLabelToView:dataTableView text:valStr x:offsetX y:offsetY + indexY * 18];
            [self addNewLabelToView:dataTableView text:targetStr x:offsetX+150 y:offsetY + indexY * 18];
            [self addNewLabelToView:dataTableView text:targetPCTStr x:offsetX+230 y:offsetY + indexY * 18];
            indexY++;

        }
    }

    
    //summary box
    UIView* reportParentBox = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 83, 246)] autorelease];
    reportParentBox.backgroundColor = CHART_COLOR_BORDER_GREY;
    [chartView addSubview: reportParentBox];
    
    UIView* firstBox = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 81, 60)] autorelease];
    UIView* secondBox = [[[UIView alloc] initWithFrame:CGRectMake(0, 60+2, 81, 60)] autorelease];
    UIView* thirdBox = [[[UIView alloc] initWithFrame:CGRectMake(0, 60+2+60+2, 81, 60)] autorelease];
    UIView* fourthBox = [[[UIView alloc] initWithFrame:CGRectMake(0, 60+2+60+2+60+2, 81, 60)] autorelease];
    [reportParentBox addSubview: firstBox];
    [reportParentBox addSubview: secondBox];
    [reportParentBox addSubview: thirdBox];
    [reportParentBox addSubview: fourthBox];


    


    if([ptype isEqualToString:@"week"]){
        float curVal = (typtdVal==nil)?[[[summaryReport objectForKey:category] objectForKey:@"TYWTD"] floatValue]:[typtdVal floatValue];
        float lastVal = (lyptdVal==nil)?[[[summaryReport objectForKey:category] objectForKey:@"LYWTD"] floatValue]:[lyptdVal floatValue];
        float pastVal = (lpptdVal==nil)?[[[summaryReport objectForKey:category] objectForKey:@"LWWTD"] floatValue]:[lpptdVal floatValue];
        [self addChartSummaryToView:firstBox newVal:0 oldVal:curVal title:@"TYWTD" showPCT:NO isTarget:NO valueFormat:format displayScale:scale negativeComparing:negCompFlag];
        [self addChartSummaryToView:secondBox newVal:curVal oldVal:lastVal title:@"LYWTD" showPCT:YES  isTarget:NO  valueFormat:format displayScale:scale negativeComparing:negCompFlag];
        [self addChartSummaryToView:thirdBox newVal:curVal oldVal:pastVal title:@"LWWTD" showPCT:YES isTarget:NO  valueFormat:format displayScale:scale negativeComparing:negCompFlag];
        if(targetVal!=nil)
            [self addChartSummaryToView:fourthBox newVal:[targetVal floatValue] oldVal:curVal title:@"Target" showPCT:YES isTarget:YES valueFormat:format displayScale:scale negativeComparing:negCompFlag];
    }
    else if ([ptype isEqualToString:@"month"]){
        
        
        float curVal = (typtdVal==nil)?[ [[summaryReport objectForKey:category] objectForKey:@"TYMTD"] floatValue]:[typtdVal floatValue];
        float lastVal = (lyptdVal==nil)?[ [[summaryReport objectForKey:category] objectForKey:@"LYMTD"] floatValue]:[lyptdVal floatValue];
        float pastVal = (lpptdVal==nil)?[ [[summaryReport objectForKey:category] objectForKey:@"LMMTD"] floatValue]:[lpptdVal floatValue];
        [self addChartSummaryToView:firstBox newVal:0 oldVal:curVal title:@"TYMTD" showPCT:NO isTarget:NO valueFormat:format displayScale:scale negativeComparing:negCompFlag];
        [self addChartSummaryToView:secondBox newVal:curVal oldVal:lastVal title:@"LYMTD" showPCT:YES isTarget:NO valueFormat:format displayScale:scale negativeComparing:negCompFlag];
        [self addChartSummaryToView:thirdBox newVal:curVal oldVal:pastVal title:@"LMMTD" showPCT:YES isTarget:NO valueFormat:format displayScale:scale negativeComparing:negCompFlag];
        if(targetVal!=nil)
            [self addChartSummaryToView:fourthBox newVal:[targetVal floatValue] oldVal:curVal title:@"Target" showPCT:YES isTarget:YES valueFormat:format displayScale:scale negativeComparing:negCompFlag];
    }
    else if ([ptype isEqualToString:@"year"]){
        
        float curVal = (typtdVal==nil)?[[[summaryReport objectForKey:category] objectForKey:@"TYYTD"] floatValue]:[typtdVal floatValue];
        float lastVal = (lpptdVal==nil)?[[[summaryReport objectForKey:category] objectForKey:@"LYYTD"] floatValue]:[typtdVal floatValue];
        
        [self addChartSummaryToView:firstBox newVal:0 oldVal:curVal title:@"TYYTD" showPCT:NO isTarget:NO valueFormat:format displayScale:scale negativeComparing:negCompFlag];
        [self addChartSummaryToView:secondBox newVal:curVal oldVal:lastVal title:@"LYYTD" showPCT:YES isTarget:NO valueFormat:format displayScale:scale negativeComparing:negCompFlag];
        if(targetVal!=nil)
            [self addChartSummaryToView:fourthBox newVal:[targetVal floatValue] oldVal:curVal title:@"Target" showPCT:YES isTarget:YES valueFormat:format displayScale:scale negativeComparing:negCompFlag];

    }


    UIView* diamgramView = [[UIView alloc] initWithFrame:CGRectMake(84, 0, 300-84, 246)];
    ChartLine* chart = [[ChartLine alloc] initWithFrame:CGRectMake(0, 0, 300-84, 246)];
    chart.title = [data objectForKey:@"title"];
    
    NSMutableArray* labelArray = [NSMutableArray array];
    NSMutableArray* yValueArray = [NSMutableArray array];
    NSMutableArray* yValueArray2 = [NSMutableArray array];
    
    NSDateFormatter* df2 = [[[NSDateFormatter alloc] init] autorelease];
    [df2 setDateFormat:@"d"];
    
    

    for (NSDictionary* node in nodes){
        if ([[node objectForKey:@"is_null"] intValue] == 1){
            break;
        }
        
        if ([ptype isEqualToString:@"week"]){
            NSDate* d = [df dateFromString: [node objectForKey:@"period"]];
            NSString* str = [df2 stringFromDate:d];
            if(str!=nil){
                [labelArray addObject: str ];
            }
            else{
                [labelArray addObject: [NSString stringWithFormat:@"%@", [node objectForKey:@"period"] ] ];
            }
        }
        else if ([ptype isEqualToString:@"month"]){
            NSString* str = [node objectForKey:@"period"];
            NSArray* parts = [str componentsSeparatedByString:@"-"];
            [labelArray addObject: [NSString stringWithFormat:@"W%@", parts[1]]];
        }
        else if ([ptype isEqualToString:@"year"]){
            NSString* str = [node objectForKey:@"period"];
            NSArray* parts = [str componentsSeparatedByString:@"-"];
            NSInteger monthNumber = [parts[1] intValue];
            NSArray* monthNames = [[ShareLocale textFromKey:@"general_month_names"] componentsSeparatedByString:@","];
            NSString *monthName = monthNames[monthNumber-1];
            [labelArray addObject: monthName ];
        }
        
        float yVal = [[node objectForKey:@"val"] floatValue] * s ;
        [yValueArray addObject:  [NSNumber numberWithFloat:yVal]];
        //handle line2
        if(nodePeriodValMap[node[@"period"]]!=nil){
            float yVal = [nodePeriodValMap[node[@"period"]] floatValue] * s;
            [yValueArray2 addObject: [NSNumber numberWithFloat:yVal] ];
        }
        else{
            [yValueArray2 addObject:[NSNull null]];
        }
    }


    chart.startY = startY * s;
    chart.intervalY = intervalY * s;
    chart.intervalX = intervalX;
    chart.xLabelArray = labelArray;
    chart.valueArray1 = yValueArray;
    chart.valueArray2 = yValueArray2;
    

    [chart generateGraph];
    [diamgramView addSubview:chart];
    [chartView addSubview:diamgramView];
    [chart release];
    [diamgramView release];
        
}





-(void) sortChartNodeList:(NSMutableArray*)nodes type:(NSString*)ptype{
    if([ptype isEqualToString:@"week"]){
        [nodes sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            NSString* s1 = [obj1 objectForKey:@"period"];
            NSString* s2 = [obj2 objectForKey:@"period"];
            NSArray* a1 = [s1 componentsSeparatedByString:@"-"] ;
            NSArray* a2 = [s2 componentsSeparatedByString:@"-"] ;
            int y1 = [[a1 objectAtIndex:0] intValue];
            int m1 = [[a1 objectAtIndex:1] intValue];
            int d1 = [[a1 objectAtIndex:2] intValue];
            int y2 = [[a2 objectAtIndex:0] intValue];
            int m2 = [[a2 objectAtIndex:1] intValue];
            int d2 = [[a2 objectAtIndex:2] intValue];
            
            if(y1<y2){
                return NSOrderedAscending;
            }
            else if (y1>y2){
                return NSOrderedDescending;
            }
            else{
                if(m1<m2) return NSOrderedAscending;
                else if(m1>m2) return NSOrderedDescending;
                else{
                    if(d1<d2) return NSOrderedAscending;
                    else if(d1>d2) return NSOrderedDescending;
                    return NSOrderedSame;
                }
            }
        }];
    }
    else if( [ptype isEqualToString:@"month"] || [ptype isEqualToString:@"year"]){
        
        [nodes sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            NSString* s1 = [obj1 objectForKey:@"period"];
            NSString* s2 = [obj2 objectForKey:@"period"];
            NSArray* a1 = [s1 componentsSeparatedByString:@"-"] ;
            NSArray* a2 = [s2 componentsSeparatedByString:@"-"] ;
            int y1 = [[a1 objectAtIndex:0] intValue];
            int m1 = [[a1 objectAtIndex:1] intValue];
            int y2 = [[a2 objectAtIndex:0] intValue];
            int m2 = [[a2 objectAtIndex:1] intValue];
            
            if(y1<y2){
                return NSOrderedAscending;
            }
            else if (y1>y2){
                return NSOrderedDescending;
            }
            else{
                if(m1<m2)
                    return NSOrderedAscending;
                else if(m1>m2)
                    return NSOrderedDescending;
                else
                    return NSOrderedSame;
            }
            
        }];
        
    }

    
}

//set display text of the 3 summary text inside the chart. Each diagram should run it for 3 times
-(void) addChartSummaryToView:(UIView*)view newVal:(float)newVal oldVal:(float)oldVal title:(NSString*)title showPCT:(BOOL)showPCT isTarget:(BOOL)target {
    [self addChartSummaryToView:view newVal:newVal oldVal:oldVal title:title showPCT:showPCT isTarget:target valueFormat:@"%.2f"];
}
-(void) addChartSummaryToView:(UIView*)view newVal:(float)newVal oldVal:(float)oldVal title:(NSString*)title showPCT:(BOOL)showPCT isTarget:(BOOL)target valueFormat:(NSString*)vformat{
    [self addChartSummaryToView:view newVal:newVal oldVal:oldVal title:title showPCT:showPCT isTarget:target valueFormat:vformat displayScale:1 negativeComparing:NO];
}

-(void) addChartSummaryToView:(UIView*)view newVal:(float)newVal oldVal:(float)oldVal title:(NSString*)title showPCT:(BOOL)showPCT isTarget:(BOOL)target valueFormat:(NSString*)vformat displayScale:(float)scale negativeComparing:(BOOL)flag{
    
    UILabel* valLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 3, 80, 16)];
    UILabel* pctLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 3+14+3, 80, 16)];
    UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 3+14+3+16+3, 80, 16)];
    valLabel.textColor = [UIColor whiteColor];
    pctLabel.textColor = [UIColor whiteColor];
    titleLabel.textColor = [UIColor whiteColor];
    valLabel.textAlignment = NSTextAlignmentCenter;
    pctLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    valLabel.font = [UIFont fontWithName:@"Arial" size:12];
    pctLabel.font = [UIFont fontWithName:@"Arial" size:12];
    titleLabel.font = [UIFont fontWithName:@"Arial" size:12];
    
    valLabel.minimumScaleFactor = 0.5f;
    valLabel.adjustsFontSizeToFitWidth = YES;
    pctLabel.minimumScaleFactor = 0.5f;
    pctLabel.adjustsFontSizeToFitWidth = YES;
    titleLabel.minimumScaleFactor = 0.5f;
    titleLabel.adjustsFontSizeToFitWidth = YES;
    
    if(target){
        if(newVal!=0){
            pctLabel.text = [NSString stringWithFormat:@"%.1f%%", 100 * oldVal / newVal];
            valLabel.text = [BusinessLogicHelper convertValueToLimitedCharacterString:newVal*scale format:vformat];
        }else{
            pctLabel.text = @"-";
            valLabel.text = @"-";
        }
    }else{
        if(oldVal!=0){
            float pct = 100*(newVal-oldVal)/oldVal;
            if(isnan(pct) ){
                pctLabel.text = @"-";
                valLabel.text = @"-";
            }else {
                
                if(pct>=0){
                    pctLabel.text = [NSString stringWithFormat:@"+%.1f%%", pct];
                }else{
                    pctLabel.text = [NSString stringWithFormat:@"%.1f%%", pct];
                }
                if(vformat!=nil) valLabel.text = [BusinessLogicHelper convertValueToLimitedCharacterString:oldVal*scale format:vformat];
                valLabel.text = [BusinessLogicHelper convertValueToLimitedCharacterString:oldVal*scale];
            }
            
        }else{
            pctLabel.text = @"-";
            valLabel.text = @"-";
        }
    }


    titleLabel.text = title;
    [view addSubview:valLabel];
    [view addSubview:pctLabel];
    [view addSubview:titleLabel];
    
    if(!showPCT){
        [pctLabel removeFromSuperview];
        valLabel.frame = CGRectMake(valLabel.frame.origin.x, valLabel.frame.origin.y + 8, valLabel.frame.size.width, valLabel.frame.size.height);
        titleLabel.frame = CGRectMake(titleLabel.frame.origin.x, titleLabel.frame.origin.y - 8 , titleLabel.frame.size.width, titleLabel.frame.size.height);
        view.backgroundColor = CHART_COLOR_BLUE;
    }else{
        if(target){
            if(newVal==0){
                view.backgroundColor = CHART_COLOR_BLUE;
            }else{
                if (oldVal / newVal >= 1.0 ){
                    if(!flag) view.backgroundColor = CHART_COLOR_GREEN;
                    else view.backgroundColor = CHART_COLOR_RED;
                }else{
                    if(!flag) view.backgroundColor = CHART_COLOR_RED;
                    else view.backgroundColor = CHART_COLOR_GREEN;
                }
            }
        }else{
            if(oldVal==0){
                view.backgroundColor = CHART_COLOR_BLUE;
            }else{
                if((newVal-oldVal)/oldVal>=0){
                    if(!flag) view.backgroundColor = CHART_COLOR_GREEN;
                    else view.backgroundColor = CHART_COLOR_RED;
                }else{
                    if(!flag) view.backgroundColor = CHART_COLOR_RED;
                    else view.backgroundColor = CHART_COLOR_GREEN;
                }
            }
        }
    }
    [valLabel release];
    [pctLabel release];
    [titleLabel release];
    
}



-(NSNumber*) getTargetDataWithType:(NSString*) type category:(NSString*)cat {
    return [self getTargetDataWithType:type category:cat year:self.fxYear month:self.fxMonth week:self.fxWeek];
}


//summary
-(NSNumber*) getTargetDataWithType:(NSString*) type category:(NSString*)cat year:(int)year month:(int)month week:(int)week{

//    BOOL isWeek = [type isEqualToString:@"weekly" ];
//    BOOL isMonth = [type isEqualToString:@"monthly" ];
//    BOOL isYear = [type isEqualToString:@"yearly" ];
    NSString* code = [self.storeData objectForKey:@"Store_Code__c"];
    if(code == nil) code = [self.storeData objectForKey:@"code"];
    VFDAO* vf = [VFDAO sharedInstance];
    NSArray* allTargetRecords = [vf.cache objectForKey:@"targets"];
    
    for(NSDictionary* record in allTargetRecords){
        NSString* storeCode = [record objectForKey:@"Store_Code__c"];
        NSString* rtype = [[record objectForKey:@"Type__c"] lowercaseString];
        
        NSString* tWeek = [record objectForKey:@"Financial_Week__c"];
        NSString* tMonth = [record objectForKey:@"Financial_Month__c"];
        NSString* tYear = [record objectForKey:@"Financial_Year__c"];
//        NSString* tDateStr = [record objectForKey:@"Financial_Date__c"];
        
        if( //[record objectForKey:cat]!=(id)[NSNull null] &&
           [storeCode isEqualToString:code]){//the list is ordered by year, month, week desc
            
            //Get Weekly Target
            if([rtype isEqualToString:@"weekly"] && [type isEqualToString:@"weekly"] ){
                if([tYear intValue]==year && [tWeek intValue]==week){
                    id val = [record objectForKey:cat];
                    if(val!=nil && val!=(id)[NSNull null]){
                        return [NSNumber numberWithDouble: [val doubleValue]];
                    }
                }
            }
            //Get WTD target
            if([rtype isEqualToString:@"wtd"] && [type isEqualToString:@"wtd"] ){
                if([tYear intValue]==year && [tWeek intValue]==week){
                    id val = [record objectForKey:cat];
                    if(val!=nil && val!=(id)[NSNull null]){
                        return [NSNumber numberWithDouble: [[record objectForKey:cat] doubleValue]];
                    }
                }
            }
            
            //Get Monthly Target
            if([rtype isEqualToString:@"monthly"]  && [type isEqualToString:@"monthly"] ){
                if([tYear intValue]==year && [tMonth intValue]==month){
                    id val = [record objectForKey:cat];
                    if(val!=nil && val!=(id)[NSNull null]){
                        return [NSNumber numberWithDouble: [[record objectForKey:cat] doubleValue]];
                    }
                }
            }
            //Get MTD Target
            if([rtype isEqualToString:@"mtd"]  && [type isEqualToString:@"mtd"]){
                if([tYear intValue]==year && [tMonth intValue]==month){
                    id val = [record objectForKey:cat];
                    if(val!=nil && val!=(id)[NSNull null]){
                        return [NSNumber numberWithDouble: [[record objectForKey:cat] doubleValue]];
                    }
                }
            }
            //Get Yealy Target
            if([rtype isEqualToString:@"yearly"]  && [type isEqualToString:@"yearly"] ){
                if([tYear intValue]==year){
                    id val = [record objectForKey:cat];
                    if(val!=nil && val!=(id)[NSNull null]){
                        return [NSNumber numberWithDouble: [[record objectForKey:cat] doubleValue]];
                    }
                }
            }
            //Get YTD Target
            if([rtype isEqualToString:@"ytd"]  && [type isEqualToString:@"ytd"] ){
                if([tYear intValue]==year){
                    id val = [record objectForKey:cat];
                    if(val!=nil && val!=(id)[NSNull null]){
                        return [NSNumber numberWithDouble: [[record objectForKey:cat] doubleValue]];
                    }
                }
            }
            
//            if(isWeek && [rtype isEqualToString:@"daily"]){
//                return [NSNumber numberWithDouble: [[record objectForKey:cat] doubleValue]];
//            }
//            else if(isWeek && [rtype isEqualToString:@"weekly"]){
//                return [NSNumber numberWithDouble: [[record objectForKey:cat] doubleValue]];
//            }
//            else if(isMonth && [rtype isEqualToString:@"monthly"]){
//                return [NSNumber numberWithDouble: [[record objectForKey:cat] doubleValue]];
//            }
//            else if(isYear){
//                if([rtype isEqualToString:@"monthly"]){
//                    
//                    if([cat isEqualToString:@"OPS_Sales_Amount__c"]){
//                        //There is no yearly target in DB now, just use the monthly target x 12 as yearly for demo
//                        return [NSNumber numberWithDouble: [[record objectForKey:cat] doubleValue] * 12];
//                    }else{
//                        //There is no yearly target in DB now, just use the monthly target as yearly for demo
//                        return [NSNumber numberWithDouble: [[record objectForKey:cat] doubleValue]];
//                    }
//                }
//            }
        }
    }
    
    return [NSNumber numberWithInt:0];
}



//0 is TYWTD, 1 is LYWTD, 2 is LWWTD for weekly diagram
//refName = refQuantityMix
-(NSArray*)getSummaryValuesForSelectedFilters:(NSArray*)filters filterCategory:(NSString*)filterCagtegory refName:(NSString*)refName period1Name:(NSString*)period1 period2Name:(NSString*)period2 period3Name:(NSString*)period3{
    
    NSArray* catParts = [filterCagtegory componentsSeparatedByString:@"_"];
    
    NSMutableArray* filterKeys = [NSMutableArray array];
    if([catParts[0] isEqualToString:@"vip"]){
        for(NSDictionary* f in filters){
            [filterKeys addObject:[f objectForKey:@"grade"]];
        }
        if([filterKeys count]==0){
            filterKeys = self.vipGradeArray;
        }
    }else{
        for(NSDictionary* f in filters){
            NSString* g = [f objectForKey:@"gender"];
            NSString* c = [f objectForKey:@"cat"];
            [filterKeys addObject: [NSString stringWithFormat:@"%@ %@", c, g]];
        }
        if([filterKeys count]==0){
            filterKeys = self.catgenArray;
        }
    }
    NSArray* resultArray = @[[NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0]];
    
    NSDictionary* refData = [self.allChartData objectForKey:refName];
    if(refData==nil || refData==(id)[NSNull null]){
        return resultArray;
    }
    
    NSArray* refNodes = [refData objectForKey:@"nodes"];
    if(refNodes==nil || refNodes==(id)[NSNull null]){
        return resultArray;
    }
    
    NSDateFormatter* df = [[[NSDateFormatter alloc] init] autorelease];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSDate* fxDate = [df dateFromString:self.fxDate];
    [df setDateFormat:@"yyyy-M-d"];
    NSString* fxDateStr = [df stringFromDate: fxDate];


    float val1 = 0;
    float val2 = 0;
    float val3 = 0;
    
    for(NSDictionary* refNode in refNodes){
        NSString* refPeriod = [refNode objectForKey:@"period"];
        NSArray* parts = [refPeriod componentsSeparatedByString:@":"];
        
        NSString* checkPart;
        NSString* periodPart;
        NSString* typePart;
        
        if([catParts[0] isEqualToString:@"vip"]){
            checkPart = [parts objectAtIndex:0];//grade or catgen
            periodPart = [parts objectAtIndex:1];//2014-7-10
            typePart = [[parts objectAtIndex:2] lowercaseString];//lmmtd
        }else{
            checkPart = [NSString stringWithFormat:@"%@ %@", [parts objectAtIndex:0], [parts objectAtIndex:1] ];//catgen
            periodPart = [parts objectAtIndex:2];//2014-7-10
            typePart = [[parts objectAtIndex:3] lowercaseString];//lmmtd
        }
        
        if ([periodPart isEqualToString:fxDateStr]){
            if(filterKeys==nil || [filterKeys containsObject:checkPart]){
                if([typePart isEqualToString:period1]){
                    val1+= [[refNode objectForKey:@"val"] floatValue];
                }
                if([typePart isEqualToString:period2]){
                    val2+= [[refNode objectForKey:@"val"] floatValue];
                }
                if([typePart isEqualToString:period3]){
                    val3+= [[refNode objectForKey:@"val"] floatValue];
                }
            }
        }
    }
    return @[[NSNumber numberWithFloat:val1], [NSNumber numberWithFloat:val2], [NSNumber numberWithFloat:val3]];
    
}


@end
