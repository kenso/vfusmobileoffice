//
//  StoreMapViewController.m
//  VF Ordering App
//
//  Created by Developer 2 on 10/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import "StoreMapViewController.h"
#import "StoreInfoViewController.h"
#import "HeatMapModeSelectionViewController.h"
#import "MapAnnotation.h"
#import "UIHelper.h"
#import <MapKit/MapKit.h>
#import "CustomMKAnnotation.h"
#import "BusinessLogicHelper.h"
#import "GeneralUiTool.h"
#import "ShareLocale.h"

@interface StoreMapViewController ()

@end

@implementation StoreMapViewController

-(id)initWithStoreDataArray:(NSArray*) stores mode:(NSString*)modeName
{
    self = [super init];
    if (self) {
        self.storeDataAry = stores;
        self.heatMapType = modeName;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.mapView.delegate = self;
    
    if ([self.storeDataAry count] == 1 ){//TODO: check type may be better?
        self.heatMapModeButton.hidden = YES;
        [self.infoView setHidden:NO];
        NSDictionary* data = [self.storeDataAry objectAtIndex:0];
        NSLog(@"StoreMapViewController: Address: %@", [data objectForKey:@"emfa__Address__c"]);
        self.addressTextField.textColor = [UIColor colorWithWhite:.95 alpha:1];
        self.addressTextField.font = [UIFont fontWithName:@"Helvetica Neue" size:14];
        self.addressTextField.backgroundColor = [UIColor colorWithWhite:1 alpha:.2f];
        if ( [data objectForKey:@"emfa__Address__c"] == nil ||
            [data objectForKey:@"emfa__Address__c"] == (id)[NSNull null] ){
            self.addressTextField.text = @"";
        }else{
            self.addressTextField.text = [data objectForKey:@"emfa__Address__c"];
        }
        [self initCommentSidebarWithStoreId: [[self.storeDataAry firstObject] objectForKey:@"Id"]];

    }else{
        [self.infoView setHidden:YES];
        [self.heatMapModeButton setTitle: [ShareLocale textFromKey:@"heatmap_mode_default"] forState:UIControlStateNormal];

        self.mapView.frame = CGRectMake(10, 152 - 50, 1004, 606+50);
        [self initCommentSidebarWithStoreId: nil];

    }
    
    
    CLLocationCoordinate2D zoomLocation;
    MKCoordinateRegion viewRegion;

    BOOL hasValidData = NO;
    
    if(self.storeDataAry!=nil && [self.storeDataAry count]>0){
        @try{
            NSDictionary* data = [self.storeDataAry objectAtIndex:0];
            
            if ( [data objectForKey:@"emfa__Primary_Latitude__c"] == nil ||
                [data objectForKey:@"emfa__Primary_Latitude__c"] == (id)[NSNull null] ){
                zoomLocation.latitude = 22.2783;
                zoomLocation.longitude= 114.1589;
            }else {
            
                zoomLocation.latitude = [[data objectForKey:@"emfa__Primary_Latitude__c"] doubleValue];
                zoomLocation.longitude= [[data objectForKey:@"emfa__Primary_Longitude__c"] doubleValue];

                self.allAnnotations = [NSMutableArray array];
            
                for(NSDictionary* data in self.storeDataAry){
                    CLLocationCoordinate2D v;
                    
                    if ( [data objectForKey:@"emfa__Primary_Latitude__c"] == nil ||
                        [data objectForKey:@"emfa__Primary_Latitude__c"] == (id)[NSNull null] ){
                        v.latitude = 22.2783;
                        v.longitude= 114.1589;
                    }else {
                        
                        hasValidData = YES;
                        v.latitude = [[data objectForKey:@"emfa__Primary_Latitude__c"] doubleValue];
                        v.longitude = [[data objectForKey:@"emfa__Primary_Longitude__c"] doubleValue];
                            
                        NSString* dataStr = [self getAnnotationSubtitleByData:data];
                        if(v.latitude==0 && v.longitude==0){
                            NSLog(@"Incorrect corrdinates");
                        }else{
                            MapAnnotation* annotation = [[[MapAnnotation alloc] initWithCoordinate:v title:[data objectForKey:@"Name"] subtitle: [dataStr retain]] autorelease];
                                [self.allAnnotations addObject:annotation];
                            }
                        }
                }
                [self.mapView addAnnotations:self.allAnnotations];
                [self.mapView showAnnotations:self.allAnnotations animated:NO];
            }
        }@catch(NSError* error){
            //record hae no coordinate
        }
    }else{
        NSLog(@"StoreMapViewController: No shop provided");
        //no shop available, use default value
        zoomLocation.latitude = 22.2783;
        zoomLocation.longitude= 114.1589;
        viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 20*METERS_PER_MILE, 20*METERS_PER_MILE);
        [self.mapView setRegion:viewRegion animated:YES];
    }
    
    if ( !hasValidData ){
        [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:[ShareLocale textFromKey:@"error"] message: [ShareLocale textFromKey:@"invalid_map_coor"] ];
    }
    
    [self refreshLocale];
}


-(void) refreshLocale{
    self.headerTitleLabel.text = [ShareLocale textFromKey:@"heatmap_title"];
    [self.refreshButton setTitle:[ShareLocale textFromKey:@"refresh"] forState:UIControlStateNormal];
    [self.addressButton setTitle:[ShareLocale textFromKey:@"invpage_header__addr"] forState:UIControlStateNormal];
}

-(MKAnnotationView*)mapView:(MKMapView*)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    MKPinAnnotationView *annView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"HeatMapPin"];
    
    UIImageView *imageView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"map-pin-blue-lo.png"]] autorelease];
    
    for(int n=0; n<[self.storeDataAry count]; n++){
        if([[[self.storeDataAry objectAtIndex:n] objectForKey:@"Name"] isEqualToString:[annotation title]]){
            [imageView setImage:[self getAnnotationImageByData:[self.storeDataAry objectAtIndex:n] ]];
            annView.tag = n;
            break;
        }
    }
    imageView.tag = 999;
    [imageView setFrame:CGRectMake(-5, -5, 25, 45)];
    annView.animatesDrop = TRUE;
    
    if(self.heatMapType!=nil )
        annView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    
    annView.canShowCallout = YES;
    annView.calloutOffset = CGPointMake(-5, 5);
    [annView addSubview:imageView];
    return annView;
}

-(NSString*) getSubtitleFromData:(NSDictionary*)data format:(NSString*)format key:(NSString*)key{
    return [self getSubtitleFromData:data format:format key:key scale:1];
}

-(NSString*) getSubtitleFromData:(NSDictionary*)data format:(NSString*)format key:(NSString*)key scale:(float)scale{
    id val = [data objectForKey:key];
    if(val!=nil && val!=[NSNull null])
        return [NSString stringWithFormat:format, [val floatValue]*scale];
    else
        return @"N/A";
}

-(NSString*) getAnnotationSubtitleByData:(NSDictionary*)data{
    if([self.heatMapType isEqualToString:W_PREF_UPT]){
        return [self getSubtitleFromData:data format: [ShareLocale textFromKey:@"wk_upt_format"] key:@"Weekly_UPT__c"];
    }else if([self.heatMapType isEqualToString:M_PREF_UPT]){
        return [self getSubtitleFromData:data format: [ShareLocale textFromKey:@"mo_upt_format"] key:@"Monthly_UPT__c"];
    }else if([self.heatMapType isEqualToString:Y_PREF_UPT]){
        return [self getSubtitleFromData:data format: [ShareLocale textFromKey:@"yr_upt_format"] key:@"Yearly_UPT__c"];
    }
    
    else if([self.heatMapType isEqualToString:W_PREF_OPS_SALES]){
        return [self getSubtitleFromData:data format: [ShareLocale textFromKey:@"wk_ops_format"] key:@"Weekly_Ops_Sales__c"];
    }else if([self.heatMapType isEqualToString:M_PREF_OPS_SALES]){
        return [self getSubtitleFromData:data format: [ShareLocale textFromKey:@"mo_ops_format"] key:@"Monthly_Ops_Sales__c"];
    }else if([self.heatMapType isEqualToString:Y_PREF_OPS_SALES]){
        return [self getSubtitleFromData:data format: [ShareLocale textFromKey:@"yr_ops_format"] key:@"Yearly_Ops_Sales__c"];
    }
    
    else if([self.heatMapType isEqualToString:W_PREF_DISCOUNT]){
        return [self getSubtitleFromData:data format: [ShareLocale textFromKey:@"wk_dist_format"] key:@"Weekly_Discount__c" scale:100.0f] ;
    }else if([self.heatMapType isEqualToString:M_PREF_DISCOUNT]){
        return [self getSubtitleFromData:data format: [ShareLocale textFromKey:@"mo_dist_format"] key:@"Monthly_Discount__c" scale:100.0f];
    }else if([self.heatMapType isEqualToString:Y_PREF_DISCOUNT]){
        return [self getSubtitleFromData:data format: [ShareLocale textFromKey:@"yr_dist_format"] key:@"Yearly_Discount__c" scale:100.0];
    }
    
    if([self.heatMapType isEqualToString:W_PREF_SALES_PROD]){
        return [self getSubtitleFromData:data format: [ShareLocale textFromKey:@"wk_prod_format"] key:@"Weekly_Sales_Productivity__c"];
    }else if([self.heatMapType isEqualToString:M_PREF_SALES_PROD]){
        return [self getSubtitleFromData:data format: [ShareLocale textFromKey:@"mo_prod_format"] key:@"Monthly_Sales_Productivity__c"];
    }else if([self.heatMapType isEqualToString:Y_PREF_SALES_PROD]){
        return [self getSubtitleFromData:data format: [ShareLocale textFromKey:@"yr_prod_format"] key:@"Yearly_Sales_Productivity__c"];
    }
    
    else if([self.heatMapType isEqualToString:W_PREF_ATV]){
        return [self getSubtitleFromData:data format: [ShareLocale textFromKey:@"wk_atv_format"] key:@"Weekly_ATV__c"];
    }else if([self.heatMapType isEqualToString:M_PREF_ATV]){
        return [self getSubtitleFromData:data format: [ShareLocale textFromKey:@"mo_atv_format"] key:@"Monthly_ATV__c"];
    }else if([self.heatMapType isEqualToString:Y_PREF_ATV]){
        return [self getSubtitleFromData:data format: [ShareLocale textFromKey:@"yr_atv_format"] key:@"Yearly_ATV__c"];
    }
    
    else if([self.heatMapType isEqualToString:W_PREF_TXN]){
        return [self getSubtitleFromData:data format: [ShareLocale textFromKey:@"wk_txn_format"] key:@"Weekly_Transaction_Count__c"];
    }else if([self.heatMapType isEqualToString:M_PREF_TXN]){
        return [self getSubtitleFromData:data format: [ShareLocale textFromKey:@"mo_txn_format"] key:@"Monthly_Transaction_Count__c"];
    }else if([self.heatMapType isEqualToString:Y_PREF_TXN]){
        return [self getSubtitleFromData:data format: [ShareLocale textFromKey:@"yr_txn_format"] key:@"Yearly_Transaction_Count__c"];
    }
    
    else if([self.heatMapType isEqualToString:W_PREF_VISITOR_COUNT]){
        return [self getSubtitleFromData:data format: [ShareLocale textFromKey:@"wk_visitor_format"] key:@"Weekly_Visitor_Count__c"];
    }else if([self.heatMapType isEqualToString:M_PREF_VISITOR_COUNT]){
        return [self getSubtitleFromData:data format: [ShareLocale textFromKey:@"mo_visitor_format"] key:@"Monthly_Visitor_Count__c"];
    }else if([self.heatMapType isEqualToString:Y_PREF_VISITOR_COUNT]){
        return [self getSubtitleFromData:data format: [ShareLocale textFromKey:@"yr_visitor_format"] key:@"Yearly_Visitor_Count__c"];
    }
    
    else if([self.heatMapType isEqualToString:W_PREF_PEL_OFF]){
        return [self getSubtitleFromData:data format: [ShareLocale textFromKey:@"wk_pel_format"] key:@"Weekly_Pel_Off__c" scale:100.0f] ;
    }else if([self.heatMapType isEqualToString:M_PREF_PEL_OFF]){
        return [self getSubtitleFromData:data format: [ShareLocale textFromKey:@"mo_pel_format"] key:@"Monthly_Pel_Off__c" scale:100.0f] ;
    }else if([self.heatMapType isEqualToString:Y_PREF_PEL_OFF]){
        return [self getSubtitleFromData:data format: [ShareLocale textFromKey:@"yr_pel_format"] key:@"Yearly_Pel_Off__c" scale:100.0f] ;
    }
    
    else if([self.heatMapType isEqualToString:W_PREF_CONV_RATE]){
        return [self getSubtitleFromData:data format:[ShareLocale textFromKey:@"wk_conv_format"] key:@"Weekly_Conversion_Rate__c" scale:100.0f] ;
    }else if([self.heatMapType isEqualToString:M_PREF_CONV_RATE]){
        return [self getSubtitleFromData:data format:[ShareLocale textFromKey:@"mo_conv_format"] key:@"Monthly_Conversion_Rate__c" scale:100.0f] ;
    }else if([self.heatMapType isEqualToString:Y_PREF_CONV_RATE]){
        return [self getSubtitleFromData:data format: [ShareLocale textFromKey:@"yr_conv_format"] key:@"Yearly_Conversion_Rate__c" scale:100.0f] ;
    }
    
    NSString* addr = [data objectForKey:@"emfa__Address__c"];
    if(addr==nil || addr==(id)[NSNull null]){
        addr = @"N/A";
    }
    
    return addr;
}



-(UIImage*) getAnnotationImageByData:(NSDictionary*)data{
    NSString* keyName = nil;
    if([self.heatMapType isEqualToString:W_PREF_UPT]){
        keyName = @"Weekly_UPT";
    }else if([self.heatMapType isEqualToString:M_PREF_UPT]){
        keyName = @"Monthly_UPT";
    }else if([self.heatMapType isEqualToString:Y_PREF_UPT]){
        keyName = @"Yearly_UPT";
    }
    else if([self.heatMapType isEqualToString:W_PREF_OPS_SALES]){
        keyName = @"Weekly_Ops_Sales";
    }else if([self.heatMapType isEqualToString:M_PREF_OPS_SALES]){
        keyName = @"Monthly_Ops_Sales";
    }else if([self.heatMapType isEqualToString:Y_PREF_OPS_SALES]){
        keyName = @"Yearly_Ops_Sales";
    }
    else if([self.heatMapType isEqualToString:W_PREF_DISCOUNT]){
        keyName = @"Weekly_Discount";
    }else if([self.heatMapType isEqualToString:M_PREF_DISCOUNT]){
        keyName = @"Monthly_Discount";
    }else if([self.heatMapType isEqualToString:Y_PREF_DISCOUNT]){
        keyName = @"Yearly_Discount";
    }
    else if([self.heatMapType isEqualToString:W_PREF_SALES_PROD]){
        keyName = @"Weekly_Sales_Productivity";
    }else if([self.heatMapType isEqualToString:M_PREF_SALES_PROD]){
        keyName = @"Monthly_Sales_Productivity";
    }else if([self.heatMapType isEqualToString:Y_PREF_SALES_PROD]){
        keyName = @"Yearly_Sales_Productivity";
    }
    else if([self.heatMapType isEqualToString:W_PREF_ATV]){
        keyName = @"Weekly_ATV";
    }else if([self.heatMapType isEqualToString:M_PREF_ATV]){
        keyName = @"Monthly_ATV";
    }else if([self.heatMapType isEqualToString:Y_PREF_ATV]){
        keyName = @"Yearly_ATV";
    }
    else if([self.heatMapType isEqualToString:W_PREF_TXN]){
        keyName = @"Weekly_Transaction_Count";
    }else if([self.heatMapType isEqualToString:M_PREF_TXN]){
        keyName = @"Monthly_Transaction_Count";
    }else if([self.heatMapType isEqualToString:Y_PREF_TXN]){
        keyName = @"Yearly_Transaction_Count";
    }
    else if([self.heatMapType isEqualToString:W_PREF_VISITOR_COUNT]){
        keyName = @"Weekly_Visitor_Count";
    }else if([self.heatMapType isEqualToString:M_PREF_VISITOR_COUNT]){
        keyName = @"Monthly_Visitor_Count";
    }else if([self.heatMapType isEqualToString:Y_PREF_VISITOR_COUNT]){
        keyName = @"Yearly_Visitor_Count";
    }
    else if([self.heatMapType isEqualToString:W_PREF_PEL_OFF]){
        keyName = @"Weekly_Pel_Off";
    }else if([self.heatMapType isEqualToString:M_PREF_PEL_OFF]){
        keyName = @"Monthly_Pel_Off";
    }else if([self.heatMapType isEqualToString:Y_PREF_PEL_OFF]){
        keyName = @"Yearly_Pel_Off";
    }
    else if([self.heatMapType isEqualToString:W_PREF_CONV_RATE]){
        keyName = @"Weekly_Conversion_Rate";
    }else if([self.heatMapType isEqualToString:M_PREF_CONV_RATE]){
        keyName = @"Monthly_Conversion_Rate";
    }else if([self.heatMapType isEqualToString:Y_PREF_CONV_RATE]){
        keyName = @"Yearly_Conversion_Rate";
    }
    
    if(keyName!=nil){
        id val = [data objectForKey:[NSString stringWithFormat:@"%@__c", keyName ]];
        id LYval = [data objectForKey:[NSString stringWithFormat:@"%@_LY", keyName ]];
        if(val!=nil && val!=[NSNull null] && LYval!=nil && LYval!=[NSNull null]) {
            float v1 = [val floatValue];
            float v2 = [LYval floatValue];
            if(v1 >= v2){
                return [BusinessLogicHelper getColorPinForThreshold:HIGH];
            }else{
                return [BusinessLogicHelper getColorPinForThreshold:LOW];
            }
        }else{
            NSLog(@"Warning: no value found");
            return [UIImage imageNamed: @"map-pin-blue-lo.png"];
        }

    }else{
        return [UIImage imageNamed: @"map-pin-blue-lo.png"];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)onHomeButtonClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)onBackButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onRefreshButtonClicked:(id)sender {
    CLLocationCoordinate2D salon = [self geoCodeUsingAddress:self.addressTextField.text];
    NSLog(@"rayTest LatLong: %f , %f", salon.latitude, salon.longitude);
    
    if ( salon.latitude == 0 || salon.longitude == 0 ){
        [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:[ShareLocale textFromKey:@"error"] message:@"Could not found valid Location Record from Google Map!"];
    }else{
        [self.mapView removeAnnotations:self.allAnnotations];
        [self.allAnnotations removeAllObjects];
        NSDictionary* data = [self.storeDataAry objectAtIndex:0];
        MapAnnotation* annotation = [[[MapAnnotation alloc] initWithCoordinate:salon title:[data objectForKey:@"Name"] subtitle: [data objectForKey:@"emfa__Address__c"]] autorelease];
        [self.allAnnotations addObject:annotation];

        [self.mapView addAnnotations:self.allAnnotations];
        [self.mapView showAnnotations:self.allAnnotations animated:NO];
    }
}

- (IBAction)onHeatMapModeButtonClicked:(id)sender {
    
    UIView* view = (UIView*)sender;
    
    HeatMapModeSelectionViewController* vc = [[HeatMapModeSelectionViewController alloc ] initWithNibName:@"HeatMapModeSelectionViewController" bundle:nil];
    vc.delegate = self;
    self.heatModeSelectionPopup = [[[UIPopoverController alloc] initWithContentViewController:vc] autorelease];
    [self.heatModeSelectionPopup presentPopoverFromRect:view.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    
    
    
}


- (void)dealloc {
    [_mapView release];

    [_addressTextField release];
    [_refreshButton release];
    [_infoView release];
    [_heatMapModeButton release];
    [_headerTitleLabel release];
    [_addressButton release];
    [super dealloc];
}



#pragma mark - Map View Delegate

-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control{
    
    NSDictionary* storeData = [self.storeDataAry objectAtIndex:view.tag];
    StoreInfoViewController* vc = [[StoreInfoViewController alloc] initWithId:[storeData objectForKey:@"Id"]] ;
    [self.navigationController pushViewController: vc animated:YES];
    [vc release];


}

-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
}


- (void)mapViewWillStartLoadingMap:(MKMapView *)mapView{
    
}

- (void)mapViewDidFinishLoadingMap:(MKMapView *)mapView{
    
}

- (void)mapViewDidFailLoadingMap:(MKMapView *)mapView withError:(NSError *)error{
    
}


- (CLLocationCoordinate2D) geoCodeUsingAddress:(NSString *)address
{
    double latitude = 0, longitude = 0;
    NSString *esc_addr =  [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result) {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }
    CLLocationCoordinate2D center;
    center.latitude = latitude;
    center.longitude = longitude;
    return center;
}

-(NSString*) getHeatMapModeByPeriod:(int)peripd type:(int)type{
    
    if(peripd == 0){//week
        switch (type) {
            case CARD_OPS_SALES: return W_PREF_OPS_SALES;
            case CARD_DISCOUNT: return W_PREF_DISCOUNT;
            case CARD_SALES_PRODUCTIVITY: return W_PREF_SALES_PROD;
            case CARD_ATV: return W_PREF_ATV;
            case CARD_UPT: return W_PREF_UPT;
            case CARD_TRANSACTION_COUNT: return W_PREF_TXN;
            case CARD_VISITOR_COUNT: return W_PREF_VISITOR_COUNT;
            case CARD_PEL_OFF: return W_PREF_PEL_OFF;
            case CARD_CONVERSION_RATE: return W_PREF_CONV_RATE;
        }
    }else if (peripd == 1){//month
        switch (type) {
            case CARD_OPS_SALES: return M_PREF_OPS_SALES;
            case CARD_DISCOUNT: return M_PREF_DISCOUNT;
            case CARD_SALES_PRODUCTIVITY: return M_PREF_SALES_PROD;
            case CARD_ATV: return M_PREF_ATV;
            case CARD_UPT: return M_PREF_UPT;
            case CARD_TRANSACTION_COUNT: return M_PREF_TXN;
            case CARD_VISITOR_COUNT: return M_PREF_VISITOR_COUNT;
            case CARD_PEL_OFF: return M_PREF_PEL_OFF;
            case CARD_CONVERSION_RATE: return M_PREF_CONV_RATE;
        }
    }else{//year
        switch (type) {
            case CARD_OPS_SALES: return Y_PREF_OPS_SALES;
            case CARD_DISCOUNT: return Y_PREF_DISCOUNT;
            case CARD_SALES_PRODUCTIVITY: return Y_PREF_SALES_PROD;
            case CARD_ATV: return Y_PREF_ATV;
            case CARD_UPT: return Y_PREF_UPT;
            case CARD_TRANSACTION_COUNT: return Y_PREF_TXN;
            case CARD_VISITOR_COUNT: return Y_PREF_VISITOR_COUNT;
            case CARD_PEL_OFF: return Y_PREF_PEL_OFF;
            case CARD_CONVERSION_RATE: return Y_PREF_CONV_RATE;
        }
    }
    return nil;
}

#pragma mark - HeatMapModeSelectionDelegate
-(void) onSelectdMode:(NSDictionary*) mode{
    [self.heatModeSelectionPopup dismissPopoverAnimated:YES];
    self.heatModeSelectionPopup = nil;
    
    int periodIndex = [[mode objectForKey:@"Period"] intValue];
    int dataType = [[mode objectForKey:@"Type"] intValue];

    self.heatMapType = [self getHeatMapModeByPeriod:periodIndex type:dataType];
    
    [self.heatMapModeButton setTitle:[mode objectForKey:@"Label"] forState:UIControlStateNormal];
    
    [self performSelectorOnMainThread:@selector(reloadMap) withObject:nil waitUntilDone:NO];
}


-(void) reloadMap{

    for(int i=0; i<[self.allAnnotations count]; i++){
        MapAnnotation* ann = [self.allAnnotations objectAtIndex:i];

        UIView* annView = [self.mapView viewForAnnotation:ann];
        UIImageView* imgView = (UIImageView*)[annView viewWithTag:999];
        NSDictionary* data = [self.storeDataAry objectAtIndex:i];
        [ann replaceSubtitle: [self getAnnotationSubtitleByData:data]];
        imgView.image = [self getAnnotationImageByData: data];
    }
    
    NSLog(@"Map reloaded!");
    
}

@end
