//
//  StoreAnalyticSelectionViewController.m
//  VFStorePerformanceApp_Billy
//
//  Created by Developer 2 on 10/4/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "StoreAnalyticSelectionViewController.h"
#import "BusinessLogicHelper.h"
#import "ShareLocale.h"

@interface StoreAnalyticSelectionViewController ()

@end

@implementation StoreAnalyticSelectionViewController


- (void)viewDidLoad{
    
    
    
    
    
    [super viewDidLoad];
    self.modeArray = @[@{@"Label": [ShareLocale textFromKey:@"storelist_weekly_summary"], @"Value":@0},
                       @{@"Label": [ShareLocale textFromKey:@"ops_sales"], @"Value":@CARD_OPS_SALES},
                       @{@"Label": [ShareLocale textFromKey:@"discount"], @"Value":@CARD_DISCOUNT},
                       @{@"Label": [ShareLocale textFromKey:@"store_prod"], @"Value":@CARD_SALES_PRODUCTIVITY},
                       @{@"Label": [ShareLocale textFromKey:@"atv"], @"Value":@CARD_ATV},
                       @{@"Label": [ShareLocale textFromKey:@"upt"], @"Value":@CARD_UPT},
                       @{@"Label": [ShareLocale textFromKey:@"transaction_count"], @"Value":@CARD_TRANSACTION_COUNT},
                       @{@"Label": [ShareLocale textFromKey:@"visitor_count"], @"Value":@CARD_VISITOR_COUNT} ,
                       @{@"Label": [ShareLocale textFromKey:@"peel_off"], @"Value":@CARD_PEL_OFF},
                       @{@"Label": [ShareLocale textFromKey:@"conversion_rate"], @"Value":@CARD_CONVERSION_RATE}
                       ];

    [self.tableView reloadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.modeArray count];
}





#pragma mark - Table view delegate
-(UITableViewCell*) tableView:(UITableView *)tview cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary* data = [self.modeArray objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tview dequeueReusableCellWithIdentifier:@"StoreModeCell"];
    if(cell == nil){
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"StoreModeCell"] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
//        UIView *selectionColor = [[UIView alloc] init];
//        selectionColor.backgroundColor = [UIColor colorWithRed:(245/255.0) green:(245/255.0) blue:(245/255.0) alpha:.2f];
//        cell.selectedBackgroundView = selectionColor;
//        
        cell.textLabel.hidden = YES;
//        cell.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(20, 5, 300, 40)];
        label.tag = 901;
        label.text = @"";
        [cell addSubview:label];
        [label release];
    }
    
    UILabel* dateLabel = (UILabel*)[cell viewWithTag:901];
    dateLabel.text = [data objectForKey:@"Label"];
    cell.tag = indexPath.row;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.delegate){
        [self.delegate onSelectedMode:[self.modeArray objectAtIndex:indexPath.row]];
    }
}


@end
