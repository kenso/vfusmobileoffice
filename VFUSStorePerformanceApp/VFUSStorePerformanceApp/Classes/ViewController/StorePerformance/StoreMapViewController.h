//
//  StoreMapViewController.h
//  VF Ordering App
//
//  Created by Developer 2 on 10/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "BasicViewController.h"
#import "HeatMapModeSelectionViewController.h"


#define METERS_PER_MILE 1609.344


@interface StoreMapViewController : BasicViewController<MKMapViewDelegate, UIPopoverControllerDelegate, HeatMapModeSelectionViewControllerDelegate>{

}
@property (retain, nonatomic) IBOutlet UILabel *headerTitleLabel;

@property (nonatomic, retain) NSString* heatMapType;

@property (nonatomic, retain) NSArray* storeDataAry;
@property (nonatomic, retain) NSMutableArray* allAnnotations;
@property (retain, nonatomic) IBOutlet UITextField *addressTextField;

@property (retain, nonatomic) IBOutlet UIView *infoView;
@property (retain, nonatomic) IBOutlet MKMapView *mapView;
@property (retain, nonatomic) IBOutlet UIButton *refreshButton;
@property (retain, nonatomic) IBOutlet UIButton *heatMapModeButton;
@property (retain, nonatomic) IBOutlet UIButton *addressButton;

@property (retain, nonatomic) UIPopoverController *heatModeSelectionPopup;
- (IBAction)onHomeButtonClicked:(id)sender;

- (IBAction)onBackButtonClicked:(id)sender;
- (IBAction)onRefreshButtonClicked:(id)sender;
- (IBAction)onHeatMapModeButtonClicked:(id)sender;

-(id)initWithStoreDataArray:(NSArray*) stores mode:(NSString*)modeName;

@end
