//
//  SalesPerformanceSelectionViewController.m
//  VFStorePerformanceApp_Billy
//
//  Created by Developer 2 on 4/4/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//
#import "GeneralUiTool.h"
#import "SalesPerformanceSelectionViewController.h"
#import "ShareLocale.h"
#import "Constants.h"
#import "SalesPerformanceFitlerFormVC.h"

@interface SalesPerformanceSelectionViewController ()

@end

@implementation SalesPerformanceSelectionViewController

-(id)initWithArray:(NSArray*)dataArray cardTag:(int)ctag windowTag:(int)wtag caller:(UIViewController*)vc{
    self = [super initWithNibName:@"SalesPerformanceSelectionViewController" bundle:nil];
    if (self) {
        NSMutableArray* recordArray = [NSMutableArray array];
        
        for(NSDictionary* data in dataArray){
            NSMutableDictionary* mdata = [NSMutableDictionary dictionaryWithDictionary: data];
            [recordArray addObject:mdata];
        }
        self.recordArray = recordArray;
        self.cardTag = ctag;
        self.windowTag = wtag;
        self.vc = vc;
        
    }
    return self;
}


//dataArray structure: @[@{ @"Label":@"", @"selected":@"Y" }, ...]
-(id)initWithArray:(NSArray*)dataArray cardTag:(int)ctag windowTag:(int)wtag{
    self = [super initWithNibName:@"SalesPerformanceSelectionViewController" bundle:nil];
    if (self) {
        NSMutableArray* recordArray = [NSMutableArray array];
        
        for(NSDictionary* data in dataArray){
            NSMutableDictionary* mdata = [NSMutableDictionary dictionaryWithDictionary: data];
            [recordArray addObject:mdata];
        }
        self.recordArray = recordArray;
        self.cardTag = ctag;
        self.windowTag = wtag;
    }
    return self;
}

- (IBAction)onClickEditButton:(id)sender {
    self.editMode = !self.editMode;
    
    if(self.editMode){
        self.titleLabel.text = [ShareLocale textFromKey:@"edit_filter"];

        NSMutableArray* tempRecords = [self loadLocalCustomFilters];
        [tempRecords insertObject:@{ @"selected":@"N", @"Label": [ShareLocale textFromKey:@"add_new"], @"value":@"" } atIndex:0];
        
        self.customFilterArray = tempRecords;
        [self.editButton setTitle:[ShareLocale textFromKey:@"Cancel"] forState:UIControlStateNormal];
    }else{
        self.titleLabel.text = [ShareLocale textFromKey:@"select_filter"];

        [self.editButton setTitle:[ShareLocale textFromKey:@"edit"] forState:UIControlStateNormal];
    }
    
    [self.selectionTableView reloadData];
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    self.editMode = NO;
    self.selectionTableView.delegate = self;
    self.selectionTableView.dataSource = self;
    
    self.availableOptions = [self.recordArray copy];
    
    self.titleLabel.text = [ShareLocale textFromKey:@"select_filter"];
    [self.editButton setTitle:[ShareLocale textFromKey:@"edit"] forState:UIControlStateNormal];

    
    NSMutableArray* tempRecords = [self loadLocalCustomFilters];
    [tempRecords addObjectsFromArray:self.recordArray];
    self.recordArray = tempRecords;
}


-(NSMutableArray*) loadLocalCustomFilters{
    NSUserDefaults *defaultPref = [NSUserDefaults standardUserDefaults];
    NSString* preferBrandCode = [defaultPref objectForKey: USER_PREFER_ACCOUNT_CODE];
    NSString* preferCountryCode = [defaultPref objectForKey: USER_PREFER_COUNTRY_CODE];

    NSString* key = [NSString stringWithFormat:@"%@_%@_%@s_%d",preferBrandCode, preferCountryCode, LOCAL_FILTER_NAME, self.windowTag];
    NSArray* customFilters = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    
    NSMutableArray* tempRecords = [NSMutableArray array];
    for(NSDictionary* filter in customFilters){
        [tempRecords addObject: [filter mutableCopy]];
    }
    return tempRecords;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



- (void)dealloc {
    [_selectionTableView release];
    [_titleLabel release];
    [_editButton release];
    [super dealloc];
}




#pragma mark - TableView Delegate
#pragma mark - Table view datasource/delegate

-(float) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(self.editMode){
        return [self.customFilterArray count];
    }else{
        return [self.recordArray count];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableDictionary* data;
    if(self.editMode){
        __block SalesPerformanceSelectionViewController* me = self;
        __block NSString* selectedKey = nil;
        __block NSArray* availableOptions = self.availableOptions;

        if(indexPath.row==0){
            selectedKey = nil;
        }else{
            data =  self.customFilterArray[indexPath.row];
            selectedKey = (NSString*)data;
        }
        
        NSDate* now = [NSDate date];
        NSLog(@"start = %f", (double)[now timeIntervalSince1970]);
        
        
        
        if([NSThread isMainThread]){
            NSLog(@"thread = Main");
        }else{
            NSLog(@"thread = Unknown");
        }
        
        
        [self dismissViewControllerAnimated:NO completion:^{
            NSArray* alreadySelectedOption = (selectedKey==nil)? @[] : [[NSUserDefaults standardUserDefaults] objectForKey:selectedKey];

            SalesPerformanceFitlerFormVC* formVC = [[SalesPerformanceFitlerFormVC alloc] initWithKey:selectedKey winTag:self.windowTag selectedOptions:alreadySelectedOption availableOptions:availableOptions];
            [me.vc.navigationController pushViewController:formVC animated:YES];
           NSDate* now = [NSDate date];
            NSLog(@"end = %f", (double)[now timeIntervalSince1970]);

        }];
    }
    else{
        data = self.recordArray[indexPath.row];
        if([data isKindOfClass:[NSString class]]){
            NSArray* alreadySelectedOption = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)data];
            for(NSMutableDictionary* m in self.recordArray){
                if(![m isKindOfClass:[NSString class]]){
                    m[@"selected"] = @"N";
                    for(NSString* s in alreadySelectedOption){
                        if(self.windowTag == 2){
                            if ([m[@"grade"] isEqualToString:s]){
                                m[@"selected"] = @"Y";
                            }
                        }else if(self.windowTag == 4){
                            NSArray* parts = [s componentsSeparatedByString:@"--"];
                            if ([m[@"gender"] isEqualToString:parts[0]] && [m[@"cat"] isEqualToString:parts[1]] ){
                                m[@"selected"] = @"Y";
                            }
                        }
                    }
                }
            }
            [tableView reloadData];
        }else{
            if([data objectForKey:@"selected"]!=nil && [[data objectForKey:@"selected"] isEqualToString:@"Y"]){
                [data setObject:@"N" forKey:@"selected"];
            }else{
                [data setObject:@"Y" forKey:@"selected"];
            }
            [tableView reloadRowsAtIndexPaths:@[ [NSIndexPath indexPathForRow:indexPath.row inSection:0] ] withRowAnimation:UITableViewRowAnimationNone];
        }
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tview cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSMutableDictionary* data;
    if(self.editMode){
        data =  self.customFilterArray[indexPath.row];
    }else{
        data = self.recordArray[indexPath.row];
    }

    UITableViewCell *cell = [tview dequeueReusableCellWithIdentifier:@"OptionCell"];
    if(cell == nil){
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"OptionCell"] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.textLabel.hidden = YES;
        
        UIView* bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 1, cell.frame.size.width, 48)];
        bgView.backgroundColor = [UIColor colorWithRed:.1f green:.1f blue:1 alpha:.3f];
        bgView.tag = 725413;
        [cell addSubview:bgView];
        [bgView release];
        
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 300, 50)];
        label.tag = 901;
        label.text = @"";
        [cell addSubview:label];
    }
    cell.tag = indexPath.row;
    UIView* bgView = [cell viewWithTag:725413];

    UILabel* dateLabel = (UILabel*)[cell viewWithTag:901];
    if([data isKindOfClass:[NSString class]]){
        NSString* key = (NSString*)data ;
        dateLabel.text = [NSString stringWithFormat:@"%@: %@",[ShareLocale textFromKey:@"custom"], [key substringFromIndex:21]];
        bgView.hidden = YES;
    }else{
        dateLabel.text = [data objectForKey:@"Label"];
        if([data objectForKey:@"selected"]!=nil && [[data objectForKey:@"selected"] isEqualToString:@"Y"]){
            bgView.hidden = NO;
        }else{
            bgView.hidden = YES;
        }
    }
    
    return cell;
    
}

@end
