//
//  UserSelectionViewController.h
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 14/8/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UserSelectionDelegate <NSObject>

-(void) onSelectedUser:(NSDictionary*)userData;

@end



@interface UserSelectionViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (retain, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, retain) NSArray* userArray;
@property (nonatomic, assign) id<UserSelectionDelegate> delegate;


- (id)initWithUserArray:(NSArray*)userArray;


@end
