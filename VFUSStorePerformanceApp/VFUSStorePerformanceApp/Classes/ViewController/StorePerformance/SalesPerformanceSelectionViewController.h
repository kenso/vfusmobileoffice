//
//  SalesPerformanceSelectionViewController.h
//  VFStorePerformanceApp_Billy
//
//  Created by Developer 2 on 4/4/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>




//contains a mutli-selection tableview
@interface SalesPerformanceSelectionViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>{

}

@property (retain, nonatomic) IBOutlet UIButton *editButton;
@property (retain, nonatomic) IBOutlet UITableView *selectionTableView;
@property (assign, nonatomic) NSInteger cardTag;
@property (assign, nonatomic) BOOL editMode;
@property (assign, nonatomic) NSInteger windowTag;
@property (assign, nonatomic) UIViewController* vc;
@property (retain, nonatomic) NSArray* recordArray;
@property (retain, nonatomic) NSArray* availableOptions;
@property (retain, nonatomic) NSArray* customFilterArray;
@property (retain, nonatomic) IBOutlet UILabel *titleLabel;

-(id)initWithArray:(NSArray*)dataArray cardTag:(int)ctag windowTag:(int)wtag;

-(id)initWithArray:(NSArray*)dataArray cardTag:(int)ctag windowTag:(int)wtag caller:(UIViewController*)vc;

- (IBAction)onClickEditButton:(id)sender;
@end

