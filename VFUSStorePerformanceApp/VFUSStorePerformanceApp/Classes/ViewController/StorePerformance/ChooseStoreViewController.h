//
//  ChooseStoreViewController.h
//
//  Created by Developer 2 on 4/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VFDAO.h"
#import "StoreData.h"
#import "BasicViewController.h"
#import "StoreAnalyticSelectionViewController.h"
#import "UserSelectionViewController.h"
#import "GeneralSelectionPopup.h"

@interface ChooseStoreViewController : BasicViewController<UITableViewDelegate, UITableViewDataSource, UIPopoverControllerDelegate, StoreAnalyicSelectionDelegate, UserSelectionDelegate, GeneralSelectionPopupDelegate>{
    int _mode;
}

@property (assign, nonatomic) NSInteger viewMode;//0 for Store view, 1 for DS view

@property (retain, nonatomic) NSString* selectedUserVal;
//@property (retain, nonatomic) NSString* selectedStoreTypeVal;
//@property (retain, nonatomic) NSString* selectedCompStatusVal;
@property (retain, nonatomic) NSMutableArray* selectedStoreTypeValArray;
@property (retain, nonatomic) NSMutableArray* selectedCompStatusValArray;


@property (retain, nonatomic) NSMutableArray* storeTypeArray;
@property (retain, nonatomic) NSMutableArray* compStatusArray;

@property (retain, nonatomic) IBOutlet UILabel *fxDateLabel;
@property (retain, nonatomic) IBOutlet UIButton *backButton;
@property (retain, nonatomic) IBOutlet UIButton *reloadButton;
@property (retain, nonatomic) IBOutlet UILabel *headerTitleLabel;

@property (retain, nonatomic) IBOutlet UIButton *weeklyCodeButton;
@property (retain, nonatomic) IBOutlet UIButton *weeklyStoreButton;
@property (retain, nonatomic) IBOutlet UIButton *weeklyWTDButton;
@property (retain, nonatomic) IBOutlet UIButton *weeklyMTDButton;
@property (retain, nonatomic) IBOutlet UIButton *weeklyYTDButton;
@property (retain, nonatomic) IBOutlet UIButton *weeklyWTDTargetButton;
@property (retain, nonatomic) IBOutlet UIButton *weeklyMTDTargetButton;
@property (retain, nonatomic) IBOutlet UIButton *weeklyYTDTargetButton;

@property (retain, nonatomic) IBOutlet UIButton *defaultCodeButton;
@property (retain, nonatomic) IBOutlet UIButton *defaultStoreButton;
@property (retain, nonatomic) IBOutlet UIButton *defaultOpsSalesButton;
@property (retain, nonatomic) IBOutlet UIButton *defaultDiscountButton;
@property (retain, nonatomic) IBOutlet UIButton *defaultSalesProdButton;
@property (retain, nonatomic) IBOutlet UIButton *defaultATVButton;
@property (retain, nonatomic) IBOutlet UIButton *defaultUPTButton;
@property (retain, nonatomic) IBOutlet UIButton *defaultTxnButton;
@property (retain, nonatomic) IBOutlet UIButton *defaultVisitorButton;
@property (retain, nonatomic) IBOutlet UIButton *defaultPeelOffButton;
@property (retain, nonatomic) IBOutlet UIButton *defaultConvButton;

@property (retain, nonatomic) IBOutlet UIButton *heatMapButton;

@property (retain, nonatomic) IBOutlet UIButton *userFilterButton;

@property (retain, nonatomic) IBOutlet UIButton *invCodeButton;
@property (retain, nonatomic) IBOutlet UIButton *invStoreButton;
@property (retain, nonatomic) IBOutlet UIButton *invAddrButton;





@property (retain, nonatomic) IBOutlet UITableView *storeTableView;
@property (retain, nonatomic) IBOutlet UIView *filterPopupView;
@property (retain, nonatomic) IBOutlet UIView *preferencePopupView;
@property (retain, nonatomic) IBOutlet UIView *inventoryHeaderView;

@property (retain, nonatomic) IBOutlet UIView *defaultHeaderView;

@property (retain, nonatomic) IBOutlet UIView *txnHeaderView;

@property (retain, nonatomic) IBOutlet UIButton *modeButton;

@property (retain, nonatomic) IBOutlet UIButton *groupButton;

@property (retain, nonatomic) UIPopoverController *modePopup;

@property (retain, nonatomic) IBOutlet UIButton *compStatusButton;

@property (retain, nonatomic) IBOutlet UIButton *storeTypeButton;

@property (retain, nonatomic) UIPopoverController *userPopup;

@property (retain, nonatomic) UIPopoverController *storeTypePopup;

@property (retain, nonatomic) UIPopoverController *compStatusPopup;

@property (retain, nonatomic) IBOutlet UIView *summaryView;

@property (retain, nonatomic) NSArray* recordAry;

@property (retain, nonatomic) NSArray* userArray;

- (IBAction)onPreferenceCancelButtonClicked:(id)sender;

- (IBAction)onPreferenceSaveButtonClicked:(id)sender;

- (IBAction)onFilterCancelButtonCliked:(id)sender;

- (IBAction)onFilterReloadButtonClicked:(id)sender;

-(IBAction)onBackButtonClicked:(id)sender;

-(IBAction)onHomeButtonClicked:(id)sender;

-(IBAction)onReloadButtonClicked:(id)sender;

-(IBAction)onThreshouldSettingClicked:(id)sender;

- (IBAction)onUserButtonClicked:(id)sender;

- (IBAction)onMapButtonClicked:(id)sender;

- (IBAction)onModeButtonClicked:(id)sender;

- (IBAction)onGroupButtonClicked:(id)sender;

- (IBAction) onStoreTypeButtonClicked:(id)sender;
- (IBAction) onCompStatusButtonClicked:(id)sender;

-(id)initWithMode:(int)mode;
-(id)init;

@end
