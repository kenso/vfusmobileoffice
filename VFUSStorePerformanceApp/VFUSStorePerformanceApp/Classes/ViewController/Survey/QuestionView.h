//
//  QuestionView.h
//  VFStorePerformanceApp_Ray
//
//  Created by cheng chok kwong on 4/7/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

//This class is the questionView use with the surveyForm class.
//This use for the question type of text, number, yes/no.
//This support the surveyForm in edit, preview and fillSurvey Mode.

#import <UIKit/UIKit.h>

#define SURVEY_QUESTION_VIEW_TOP_PADDING     20
#define SURVEY_QUESTION_VIEW_LEFT_PADDING    20
#define SURVEY_QUESTION_VIEW_WIDTH           760
#define SURVEY_QUESTION_VIEW_HEIGHT          110

typedef enum{
    SurveyQuestionModeNormal,//Fill in mode
    SurveyQuestionModeEdit,
    SurveyQuestionModePreview
}SurveyQuestionMode;

typedef enum{
    SurveyQuestionTypeText,
    SurveyQuestionTypeNumber,
    SurveyQuestionTypeYesNo
}SurveyQuestionType;

@interface QuestionView : UIView<UITextFieldDelegate, UITextViewDelegate>{
   int basic_view_height;
}

- (UIView *)initWithPoint:(CGPoint)point andDict:(NSDictionary *)questDict andType:(NSString *)type andMode:(SurveyQuestionMode)mode andIndex:(int)index andTag:(int)tag  editable:(BOOL)editable andDelegate:(id)delegate;
- (UIView *)initWithPoint:(CGPoint)point andDict:(NSDictionary *)questDict andType:(NSString *)type andMode:(SurveyQuestionMode)mode andIndex:(int)index andTag:(int)tag withAnsDict:(NSDictionary *)ansDict  editable:(BOOL)editable andDelegate:(id)delegate;

@property (nonatomic) int sectionTag; //The tag of the section this belong to
@property (nonatomic) int sectionIndex; // The question order number of this view for this section
@property (assign, nonatomic) BOOL editable;

@property (nonatomic) SurveyQuestionMode mode;
@property (nonatomic) SurveyQuestionType type;
@property (retain, nonatomic) NSMutableDictionary *questionDict;//questionMaster Dict
@property (retain, nonatomic) NSMutableDictionary *answerDict;//questionAnswer Dict

@property (retain, nonatomic) UIViewController *delegate;

- (int)getViewHeight;
@end
