//
//  QuestionCreateView.m
//  VFStorePerformanceApp_Ray
//
//  Created by cheng chok kwong on 4/4/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "QuestionCreateView.h"
#import "GeneralUiTool.h"
#import "ShareLocale.h"

@implementation QuestionCreateView

- (id)initWithFrame:(CGRect)frame withDelegate:(id)delegate
{
    self = [[[[NSBundle mainBundle] loadNibNamed:@"QuestionCreateView" owner:self options:nil] objectAtIndex:0] retain];
    if (self) {
        // Initialization code
        self.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height);
        self.delegate = delegate;
        
        [self.commentButton setTitle:@"" forState:UIControlStateNormal];
        [self.commentButton setTitle:@"" forState:UIControlStateSelected];
        
        [self.commentButton setBackgroundImage:[UIImage imageNamed:@"checkbox_off.png"] forState:UIControlStateNormal];
        [self.commentButton setBackgroundImage:[UIImage imageNamed:@"checkbox_on.png"] forState:UIControlStateSelected];
        [self.commentButton addTarget:self action:@selector(onClickCommentButton:) forControlEvents:UIControlEventTouchUpInside];
        self.mode = QuestionCreateModeText;
        
        [self refreshTypeTab];
        [self refreshContentView];
    }
    return self;
}

- (void)cleanAllData{
    [self.commentButton setSelected:NO];
    [self.titleTextView setText:@""];
    [self.minTextField setText:@""];
    [self.maxTextField setText:@""];
}

- (void)refreshAll{
    [self cleanAllData];
    [self refreshContentView];
    [self refreshTypeTab];
}

- (void)refreshTypeTab{
    [self.textTypeTab setAlpha:0.7f];
    [self.numberTypeTab setAlpha:0.7f];
    [self.yesNoTypeTab setAlpha:0.7f];
    [self.textTypeTab setEnabled:YES];
    [self.numberTypeTab setEnabled:YES];
    [self.yesNoTypeTab setEnabled:YES];

    if ( self.mode == QuestionCreateModeSection){
        [self.textTypeTab setAlpha:0.0f];
        [self.numberTypeTab setAlpha:0.0f];
        [self.yesNoTypeTab setAlpha:0.0f];
    }else if ( self.mode == QuestionCreateModeText){
        [self.textTypeTab setAlpha:1.0f];
        [self.textTypeTab setEnabled:NO];
    }else if ( self.mode == QuestionCreateModeNumber ){
        [self.numberTypeTab setAlpha:1.0f];
        [self.numberTypeTab setEnabled:NO];
    }else if ( self.mode == QuestionCreateModeYesNo ){
        [self.yesNoTypeTab setAlpha:1.0f];
        [self.yesNoTypeTab setEnabled:NO];
        
    }
}
- (void)refreshContentView{
    if ( self.mode == QuestionCreateModeSection){
        [self.titleLabel setTitle:@"Create Section" forState:UIControlStateNormal];
        [self.questionLabel setText:@"Title:"];
        [self.minLabel setHidden:YES];
        [self.maxLabel setHidden:YES];
        [self.commentLabel setHidden:YES];
        [self.minTextField setHidden:YES];
        [self.maxTextField setHidden:YES];
        [self.commentButton setHidden:YES];
    }else if ( self.mode == QuestionCreateModeText){
        [self.titleLabel setTitle:@"Create Text Question" forState:UIControlStateNormal];
        [self.questionLabel setText:@"Question:"];
        [self.minLabel setHidden:YES];
        [self.maxLabel setHidden:YES];
        [self.commentLabel setHidden:YES];
        [self.minTextField setHidden:YES];
        [self.maxTextField setHidden:YES];
        [self.commentButton setHidden:YES];
    }else if ( self.mode == QuestionCreateModeNumber ){
        [self.titleLabel setTitle:@"Create Number Question" forState:UIControlStateNormal];
        [self.questionLabel setText:@"Question:"];
        [self.minLabel setHidden:NO];
        [self.maxLabel setHidden:NO];
        [self.commentLabel setHidden:NO];
        [self.minTextField setHidden:NO];
        [self.maxTextField setHidden:NO];
        [self.commentButton setHidden:NO];
    }else if ( self.mode == QuestionCreateModeYesNo ){
        [self.titleLabel setTitle:@"Create Yes/No Question" forState:UIControlStateNormal];
        [self.questionLabel setText:@"Question:"];        
        [self.minLabel setHidden:YES];
        [self.maxLabel setHidden:YES];
        [self.commentLabel setHidden:NO];
        [self.minTextField setHidden:YES];
        [self.maxTextField setHidden:YES];
        [self.commentButton setHidden:NO];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
    
- (IBAction)onClickCreateButton:(id)sender {
    if ([self isValidForm] ){
        if (self.delegate != nil ){
            NSString *typeName = [[NSString alloc] init];
            if ( self.mode == QuestionCreateModeSection )typeName = @"Question_Group";
            else if ( self.mode == QuestionCreateModeText )typeName = @"Text";
            else if ( self.mode == QuestionCreateModeNumber )typeName = @"Number";
            else if ( self.mode == QuestionCreateModeYesNo )typeName = @"Yes_No";
            
            
            NSString *extraComment;
            if ([self.commentButton isSelected])extraComment = @"1";
            else extraComment = @"0";
            NSDictionary* data = @{
                                   @"Type": typeName,
                                   @"emfa__Title__c":self.titleTextView.text,
                                   @"emfa__Min_Number__c": self.minTextField.text,
                                   @"emfa__Max_Number__c": self.maxTextField.text,
                                   @"emfa__Extra_Comment__c":extraComment
                                   };
            
            [self.delegate onQuestionCreateComplete:data];
            
            [self setHidden:YES];
            [self cleanAllData];
        }
    }else{
        [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:[ShareLocale textFromKey:@"error"] message:self.errorMessage];
    }
}

- (IBAction)onClickCancelButton:(id)sender {
    [self cleanAllData];
    [self setHidden:YES];
}


- (IBAction)onClickTypeTab:(id)sender {
    
    switch ([sender tag]) {
        case 0:{
            self.mode = QuestionCreateModeText;
        }
        break;
        case 1:{
            self.mode = QuestionCreateModeNumber;
        }
            break;
        case 2:{
            self.mode = QuestionCreateModeYesNo;
        }
            break;
        default:
            break;
    }
    
    [self refreshTypeTab];
    [self refreshContentView];
}

- (void)onClickCommentButton:(id)sender{
    if ([self.commentButton isSelected] ){
        [self.commentButton setSelected:NO];
    }else {
        [self.commentButton setSelected:YES];
    }
}

- (BOOL)isValidForm{
    BOOL result = YES;
    
    if ( self.titleTextView.text == nil || [self.titleTextView.text isEqualToString:@""]){
        result = NO;
        if ( QuestionCreateModeSection ){
            self.errorMessage = @"Title should not be blanked";
        }else {
            self.errorMessage = @"Question should not be blanked";
        }
    }
    //Check if only need CreatModeText
    if ( self.mode == QuestionCreateModeNumber ){
        if ( self.minTextField.text == nil || [self.minTextField.text isEqualToString:@""]){
            result = NO;
            self.errorMessage = @"Min Number should not be blanked";
        }else if ( self.maxTextField.text == nil || [self.maxTextField.text isEqualToString:@""]){
            result = NO;
            self.errorMessage = @"Max Number should not be blanked";
        }
    }
    
    return result;
}

- (void)dealloc {
    [_questionTextView release];
    [_titleTextView release];
    [_minTextField release];
    [_maxTextField release];
    [_commentButton release];
    [_titleLabel release];
    [_textTypeTab release];
    [_numberTypeTab release];
    [_yesNoTypeTab release];
    [_minLabel release];
    [_maxLabel release];
    [_commentLabel release];
    [_questionLabel release];
    [super dealloc];
}

@end
