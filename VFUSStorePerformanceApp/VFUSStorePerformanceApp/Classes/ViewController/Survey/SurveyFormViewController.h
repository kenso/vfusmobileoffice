//
//  SurveyFormViewController.h
//  VFStorePerformanceApp
//
//  Created by Developer 2 on 6/3/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "BasicViewController.h"
#import "VFDAO.h"
#import "QuestionCreateView.h"

typedef enum {
    SurveyFormTypeVisitForm,
    SurveyFormTypeSOPForm,
    SurveyFormTypeMSPForm
}SurveyFormType;

typedef enum {
    SurveyFormModeCreateMaster, //Clone as new template
    SurveyFormModeCreateSurvey, //Fill in a new survey
    SurveyFormModePreviewSurvey, //Preview an old survey
} SurveyFormMode;


@protocol SurveyFormSaveDelegate

-(void) onSurveyFormSave:(NSDictionary*)survey;

@end


@interface SurveyFormViewController : BasicViewController<DAODelegate, UIScrollViewDelegate, UITextFieldDelegate, UITextViewDelegate,  QuestionCreateDelegate>{
    
    QuestionCreateView * questionCreatePopup;
    NSMutableArray* answerMasterArray;
    NSMutableArray* questionMasterArray;
    NSMutableArray* sectionTagArray;

    int currTotal_Y;
    UIButton *addNewSectionButton;
    UIButton *addNewQuestionButton;
    BOOL editing;
    
    int top_padding;
    int left_padding;
    int total_y;
    int currSectionTag;
    int currSection_height;
    int question_offset;
    int section_offset;
    int section_width;
    int question_view_width;
    int question_view_padding;
    int basic_view_height;
    
}
@property (retain, nonatomic) IBOutlet UIView *sectionInnerView;

@property (retain, nonatomic) IBOutlet UILabel *topTitleLabel;
@property (retain, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (retain, nonatomic) IBOutlet UIButton *titleLabel;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@property (retain, nonatomic) IBOutlet UIView *formView;
@property (retain, nonatomic) IBOutlet UIButton *submitButton;
@property (retain, nonatomic) IBOutlet UIView *innerView;
@property (retain, nonatomic) IBOutlet UILabel *scoreLabel;

@property (nonatomic) SurveyFormMode mode;
@property (nonatomic) SurveyFormType type;
//@property (nonatomic, retain) DAORequest* queryRecordTypeRequest;
//@property (nonatomic, retain) DAORequest* querySurveyMasterRequest;
@property (nonatomic, retain) DAORequest* querySurveyAnswerRequest;
@property (nonatomic, retain) DAOBaseRequest *submitRequest;
@property (nonatomic, retain) NSString *currStore;//Id
@property (nonatomic, retain) NSString *currVisitPlan;//Id
@property (nonatomic, retain) NSString *currVisitPlanName;//Name
@property (nonatomic, retain) NSString *currSurveyId;//Id
@property (nonatomic, retain) NSString *currSurveyMasterName;//Name
@property (nonatomic, retain) NSDictionary* surveyMasterDict;
@property (nonatomic, retain) NSDictionary* surveyDict;
@property (nonatomic, retain) NSMutableDictionary* dataTypeDict;
@property (nonatomic, retain) NSMutableDictionary* questionAnswerDict;

@property (nonatomic, assign) id<SurveyFormSaveDelegate> saveDelegate;

@property (nonatomic, assign) BOOL editable;

- (id)initWithSurveyMode:(SurveyFormMode)mode andSurveyId:(NSString *)surveyId editable:(BOOL)edit;
- (id)initWithSurveyMode:(SurveyFormMode)mode andSurvey:(NSDictionary *)survey editable:(BOOL)edit;

- (IBAction)onHomeButtonClicked:(id)sender;
- (IBAction)onBackButtonClicked:(id)sender ;
- (IBAction)onSubmitButtonClicked:(id)sender;

@end
