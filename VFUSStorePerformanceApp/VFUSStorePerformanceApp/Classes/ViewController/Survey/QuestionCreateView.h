//
//  QuestionCreateView.h
//  VFStorePerformanceApp_Ray
//
//  Created by cheng chok kwong on 4/4/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    QuestionCreateModeSection,
    QuestionCreateModeText,
    QuestionCreateModeNumber,
    QuestionCreateModeYesNo
} QuestionCreateMode;

@protocol QuestionCreateDelegate <NSObject>

- (void)onQuestionCreateComplete:(NSDictionary *)dict;

@end

@interface QuestionCreateView : UIView{
    
}

- (id)initWithFrame:(CGRect)frame withDelegate:(id)delegate;


@property (retain, nonatomic) id <QuestionCreateDelegate> delegate;
@property (nonatomic, assign) QuestionCreateMode mode;
@property (nonatomic, retain) NSString *errorMessage;


@property (retain, nonatomic) IBOutlet UIButton *textTypeTab;
@property (retain, nonatomic) IBOutlet UIButton *numberTypeTab;
@property (retain, nonatomic) IBOutlet UIButton *yesNoTypeTab;
@property (retain, nonatomic) IBOutlet UIButton *titleLabel;
@property (retain, nonatomic) IBOutlet UILabel *questionLabel;
@property (retain, nonatomic) IBOutlet UILabel *minLabel;
@property (retain, nonatomic) IBOutlet UILabel *maxLabel;
@property (retain, nonatomic) IBOutlet UILabel *commentLabel;
@property (retain, nonatomic) IBOutlet UIView *questionTextView;
@property (retain, nonatomic) IBOutlet UITextView *titleTextView;
@property (retain, nonatomic) IBOutlet UITextField *minTextField;
@property (retain, nonatomic) IBOutlet UITextField *maxTextField;
@property (retain, nonatomic) IBOutlet UIButton *commentButton;



- (IBAction)onClickTypeTab:(id)sender;
- (IBAction)onClickCreateButton:(id)sender;
- (IBAction)onClickCancelButton:(id)sender;


- (void)refreshAll;
- (void)cleanAllData;

@end
