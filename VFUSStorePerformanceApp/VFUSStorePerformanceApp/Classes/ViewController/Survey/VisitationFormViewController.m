//
//  VisitationFormViewController.m
//  VFStorePerformanceApp
//
//  Created by Developer 2 on 10/3/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "VisitationFormViewController.h"

@interface VisitationFormViewController ()

@end

@implementation VisitationFormViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)onBackButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onSubmitButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onHomeButtonClicked:(id)sender {
}
@end
