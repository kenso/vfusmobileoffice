//
//  SurveyListViewController.h
//  VFStorePerformanceApp
//
//  Created by Developer 2 on 6/3/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "BasicViewController.h"
#import "SFRestAPI.h"
#import "ImageLoader.h"
#import "SFRestRequest.h"
#import "VFDAO.h"

typedef enum {
    SurveyMasterListModeCreateSurvey,
    SurveyMasterListModeSurveyHistory,
    SurveyListModeSurveyHistory

} SurveyListMode;

//,UIActionSheetDelegate
@interface SurveyListViewController : BasicViewController<DAODelegate, UITableViewDelegate, UITableViewDataSource, ImageLoaderDelegate>{
    UIActionSheet *menuSheet;
    int selectedIndex;
    int selectedTabIndex;
    
    
}
@property (retain, nonatomic) IBOutlet UIButton *headerCodeButton;
@property (retain, nonatomic) IBOutlet UIButton *formTabButton;
@property (retain, nonatomic) IBOutlet UIButton *reportTabButton;

@property (retain, nonatomic) DAORequest* querySurveyMasterRequest;
@property (retain, nonatomic) DAORequest* querySurveyRequest;
@property (retain, nonatomic) NSMutableArray* recordAry;

@property (retain, nonatomic) UIDocumentInteractionController* documentController;
@property (retain, nonatomic) UITableViewCell* selectedTableViewCell;
@property (retain, nonatomic) NSString* selectedFileName;
@property (retain, nonatomic) NSString* selectedFileType;


@property (retain, nonatomic) IBOutlet UITableView *tableView;
@property (retain, nonatomic) IBOutlet UIButton *headerTitleLabel2;
@property (retain, nonatomic) IBOutlet UIButton *headerTitleLabel3;
@property (retain, nonatomic) IBOutlet UIButton *headerTitleLabel4;
@property (retain, nonatomic) IBOutlet UIButton *headerTitleLabel5;

@property (retain, nonatomic) IBOutlet UIButton *backButton;

@property (retain, nonatomic) NSString* title;
@property (retain, nonatomic) NSString* type;

@property (retain, nonatomic) NSMutableArray* formItemArray;
@property (retain, nonatomic) NSMutableArray* reportItemArray;

@property (retain, nonatomic) NSString* currStore;
@property (retain, nonatomic) NSString* currStoreName;
@property (retain, nonatomic) NSString* currVisitPlan;
@property (retain, nonatomic) NSString* selectedSurveyMasterId;
@property (retain, nonatomic) NSDictionary* surveyMasterData;
@property (atomic) SurveyListMode mode;


@property (retain, nonatomic) IBOutlet UILabel *storeNameLabel;
@property (retain, nonatomic) IBOutlet UILabel *titleLabel;

- (id)initWithMasterId:(NSString *)masterId data:(NSDictionary*)data mode:(SurveyListMode)mode;
- (id)initWithType:(NSString*)type title:(NSString*)title;
- (id)initWithType:(NSString*)type title:(NSString*)title mode:(SurveyListMode)mode;
- (IBAction)onHomeButtonClicked:(id)sender;
- (IBAction)onBackButtonClicked:(id)sender ;
- (IBAction)onFormButtonClicked:(id)sender;
- (IBAction)onReportButtonClicked:(id)sender;


@end
