//
//  QuestionView.m
//  VFStorePerformanceApp_Ray
//
//  Created by cheng chok kwong on 4/7/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "QuestionView.h"
#import "DataHelper.h"
#import "ShareLocale.h"
#import "UIHelper.h"
#import "GeneralUiTool.h"

@implementation QuestionView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (UIView *)initWithPoint:(CGPoint)point andDict:(NSDictionary *)questDict andType:(NSString *)type andMode:(SurveyQuestionMode)mode andIndex:(int)index andTag:(int)tag editable:(BOOL)editable andDelegate:(id)delegate{
    return [self initWithPoint:point andDict:questDict andType:type andMode:mode andIndex:index andTag:tag withAnsDict:nil editable:editable andDelegate:delegate];
}


- (UIView *)initWithPoint:(CGPoint)point andDict:(NSDictionary *)questDict andType:(NSString *)type andMode:(SurveyQuestionMode)mode andIndex:(int)index andTag:(int)tag withAnsDict:(NSDictionary *)ansDict editable:(BOOL)editable andDelegate:(id)delegate{
    self = [super initWithFrame:CGRectMake(point.x, point.y, SURVEY_QUESTION_VIEW_WIDTH, SURVEY_QUESTION_VIEW_HEIGHT)];
    if (self) {
        // Initialization code
        
        self.questionDict = [[NSMutableDictionary alloc] initWithDictionary: questDict];
        if ( ansDict != nil ){
            self.answerDict = [[NSMutableDictionary alloc] initWithDictionary: ansDict];
        }
        self.mode = mode;
        self.sectionIndex = index;
        self.delegate = delegate;
        self.editable = editable;
        self.tag = tag;
        
        if ([type isEqualToString:@"Text"] ){
            self.type = SurveyQuestionTypeText;
        }else if ([type isEqualToString:@"Number"] ){
            self.type = SurveyQuestionTypeNumber;
        }else if ([type isEqualToString:@"Yes_No"] ){
            self.type = SurveyQuestionTypeYesNo;
        }
        
        [self initUIView];
    }
    return self;
}






- (void)initUIView{
    int viewPadding = SURVEY_QUESTION_VIEW_TOP_PADDING;
    int viewWidth = SURVEY_QUESTION_VIEW_WIDTH;
    int viewHeight = SURVEY_QUESTION_VIEW_HEIGHT;
    
    NSString *title = [self getFieldDisplayWithFieldName:@"emfa__Title__c" withDictionary:self.questionDict];
    BOOL extraComment = [[self getFieldDisplayWithFieldName:@"emfa__Extra_Comment__c" withDictionary:self.questionDict] boolValue];
    
//    [self setBackgroundColor:[UIColor colorWithWhite:0.2f alpha:1.0f]];
    
    UILabel * questionLabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(viewPadding, 5, SURVEY_QUESTION_VIEW_WIDTH-60, 60) parent:self tag: 1 alignment:NSTextAlignmentLeft];
    questionLabel.lineBreakMode = NSLineBreakByWordWrapping;
    questionLabel.numberOfLines = 0;
    
    if ( self.editable){
        UIButton* resetButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [resetButton setImage:[UIImage imageNamed:@"reload.png"] forState:UIControlStateNormal];
        resetButton.frame = CGRectMake(questionLabel.frame.origin.x + questionLabel.frame.size.width + 16 , 8, 16, 16);
        [self addSubview:resetButton];
        [resetButton addTarget:self action:@selector(onResetButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    NSString* qtext;
    if ( self.sectionIndex == 0 ){
        qtext = title;
    }else{
        qtext = [NSString stringWithFormat:@"%i. %@", self.sectionIndex, title];
    }
    questionLabel.text = qtext;
    [questionLabel sizeToFit];
    
    int textBoxHeight = 100;
    
    int questionHeight = 5 + questionLabel.frame.size.height + 5;
    
    if ( self.type == SurveyQuestionTypeText){
        UITextView *answerField = [self createNewTextViewWithDefaultStyle:CGRectMake(viewPadding, questionHeight, viewWidth-2*viewPadding , textBoxHeight) parent:self tag:13802 alignment:NSTextAlignmentLeft ];
        [answerField setDelegate:self];
        
        if ( self.answerDict != nil ){
            id answer = [_answerDict objectForKey:@"emfa__Answer_Value__c"];
            if ( answer != nil && answer != (id)[NSNull null] ){
                [answerField setText: [_answerDict objectForKey:@"emfa__Answer_Value__c"]];
            }
        }
        viewHeight = questionHeight + 5 + textBoxHeight + 20;
    }
    else if ( self.type == SurveyQuestionTypeNumber){
        int minNum = [[_questionDict objectForKey:@"emfa__Min_Number__c"] intValue];
        int maxNum = [[_questionDict objectForKey:@"emfa__Max_Number__c"] intValue];
        
        NSMutableArray *itemArray = [[NSMutableArray alloc] initWithCapacity:maxNum - minNum+1];
        for ( int i = minNum; i <= maxNum; i ++){
            [itemArray addObject:[NSString stringWithFormat:@"%i", i ]];
        }
        UISegmentedControl *segControl = [[UISegmentedControl alloc] initWithItems:itemArray];
        segControl.tag = 13802;
        [segControl setFrame:CGRectMake(viewPadding, questionHeight, viewWidth-2*viewPadding, 30)];
        [segControl addTarget:self
                       action:@selector(onClickSegmentControl:)
             forControlEvents:UIControlEventValueChanged];
        [segControl setTintColor:[UIColor whiteColor]];
        [self addSubview:segControl];
        
        if ( self.answerDict != nil ){
            //Preview Mode
            id index = [self.answerDict objectForKey:@"emfa__Answer_Value__c"];
            if ( index != nil && index != (id)[NSNull null] && ![index isEqualToString:@""]){
                int answerIndex = [self getSegmentIndexFrom:segControl byTitle:index];
                [segControl setSelectedSegmentIndex:answerIndex];
                
            }
        }
        viewHeight = questionHeight+5+30;
        if ( extraComment ){
            UILabel * remarkLabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(viewPadding, questionHeight+5+30, textBoxHeight, 30) parent:self tag:900 alignment:NSTextAlignmentLeft];
            remarkLabel.text = [ShareLocale textFromKey:@"remark:"];
            UITextView *remarkField = [self createNewTextViewWithDefaultStyle:CGRectMake(viewPadding+75, questionHeight+5+30+4, 645, textBoxHeight) parent:self tag:13803 alignment:NSTextAlignmentLeft ];
            [remarkField setDelegate:self];
            if ( self.answerDict != nil ){
                id comment = [self.answerDict objectForKey:@"emfa__Comment__c"];
                if ( comment != nil && comment != (id)[NSNull null] ){
                    [remarkField setText: [self.answerDict objectForKey:@"emfa__Comment__c"]];
                }
            }
            viewHeight = questionHeight+5+30+5+textBoxHeight+20;
        }
        
    }else if ( self.type == SurveyQuestionTypeYesNo ){
        NSMutableArray *itemArray = [[NSMutableArray alloc] initWithObjects:@"YES",@"NO", nil];
        UISegmentedControl *segControl = [[UISegmentedControl alloc] initWithItems:itemArray];
        segControl.tag = 13802;
        [segControl setFrame:CGRectMake(viewPadding, questionHeight, viewWidth-2*viewPadding, 30)];
        [segControl addTarget:self
                       action:@selector(onClickSegmentControl:)
             forControlEvents:UIControlEventValueChanged];
        [segControl setTintColor:[UIColor whiteColor]];
        [self addSubview:segControl];
        viewHeight = questionHeight+5+30;
        if ( extraComment ){
            UILabel * remarkLabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(viewPadding, questionHeight+5+30, textBoxHeight, 30) parent:self tag:900 alignment:NSTextAlignmentLeft];
            remarkLabel.text = [ShareLocale textFromKey:@"remark:"];
            UITextView *remarkField = [self createNewTextViewWithDefaultStyle:CGRectMake(viewPadding+75, questionHeight+5+30+2, 645, textBoxHeight) parent:self tag:13803 alignment:NSTextAlignmentLeft ];
            [remarkField setDelegate:self];
            viewHeight = questionHeight+5+30+5+textBoxHeight+20;
            if ( self.answerDict != nil ){
                id comment = [self.answerDict objectForKey:@"emfa__Comment__c"];
                if ( comment != nil && comment != (id)[NSNull null] ){
                    [remarkField setText: [self.answerDict objectForKey:@"emfa__Comment__c"]];
                }
                
                NSString* answer = [self.answerDict objectForKey:@"emfa__Answer_Value__c"];//YES or NO
                if ( answer != nil && answer != (id)[NSNull null] && ![answer isEqualToString:@""] ){
                    int answerIndex = [self getSegmentIndexFrom:segControl byTitle:answer];
                    [segControl setSelectedSegmentIndex:answerIndex];
                }
            }
        }
    }
    viewHeight += 5;
    
    //Set Size
    [self setFrame:CGRectMake(self.frame.origin.x,
                              self.frame.origin.y,
                              viewWidth,
                              viewHeight)];
    
    //Set View effect by Mode
//    if ( self.mode == SurveyQuestionModeNormal){
    if(self.editable){
        self.layer.borderColor = [UIColor whiteColor].CGColor;
        self.layer.borderWidth = 1.0f;
    }
//    else if ( self.mode == SurveyQuestionModeEdit){
//        //Dotted Line for view border
//        CAShapeLayer *_border;
//        _border = [CAShapeLayer layer];
//        _border.strokeColor = [UIColor yellowColor].CGColor;
//        _border.fillColor = nil;
//        _border.lineDashPattern = @[@4, @2];
//        _border.lineWidth = 1.5f;
//        [self.layer addSublayer:_border];
//        _border.path = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
//        _border.frame = self.bounds;
//        
//        //[self setUserInteractionEnabled:NO];
//    }
//    else if ( self.mode == SurveyQuestionModePreview){
//        //[self setUserInteractionEnabled:NO];
//    }
    
    [self setUserInteractionEnabled: self.editable];
    
}

- (int)getSegmentIndexFrom:(UISegmentedControl *)segment byTitle:(NSString *)title{
    int result = 0;
    
    for (int i=0; i < [segment numberOfSegments]; i ++ ){
        if ( [[segment titleForSegmentAtIndex:i] isEqualToString:title] ){
            return i;
        }
    }
    return result;
}

- (int)getViewHeight{
    return self.frame.size.height;
}


-(void) onResetButtonClicked:(id)sender{
    UIView* qview = self;
    
    [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"" message:@"Confirm to reset the answer?" yesHanlder:^(id data) {
        if ( self.type == SurveyQuestionTypeText){
            UITextField* textField = (UITextField*)[qview viewWithTag:13802];
            textField.text  = @"";
        }
        else if ( self.type == SurveyQuestionTypeYesNo ){
            UISegmentedControl* optionControl = (UISegmentedControl*)[qview viewWithTag:13802];
            optionControl.selectedSegmentIndex = -1;
            UITextField* textField = (UITextField*)[qview viewWithTag:13803];
            textField.text  = @"";
        }
        else if ( self.type == SurveyQuestionTypeNumber){
            UISegmentedControl* optionControl = (UISegmentedControl*)[qview viewWithTag:13802];
            optionControl.selectedSegmentIndex = -1;
            UITextField* textField = (UITextField*)[qview viewWithTag:13803];
            textField.text  = @"";
        }
        if(self.delegate!=nil){
            [self.delegate onResetButtonClicked: self.tag];
        }
    } noHandler:^(id data) {
        
    }];

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(UITextField*) createNewTextFieldWithDefaultStyle:(CGRect)rect parent:(UIView*)parent tag:(NSInteger) tag{
    return [self createNewTextFieldWithDefaultStyle:rect parent:parent tag:tag alignment:NSTextAlignmentCenter vAlignment:UIControlContentVerticalAlignmentCenter];
}
-(UITextField*) createNewTextFieldWithDefaultStyle:(CGRect)rect parent:(UIView*)parent tag:(NSInteger) tag alignment:(NSTextAlignment)alignment{
    return [self createNewTextFieldWithDefaultStyle:rect parent:parent tag:tag alignment:alignment vAlignment:UIControlContentVerticalAlignmentCenter];
}

-(UITextField*) createNewTextFieldWithDefaultStyle:(CGRect)rect parent:(UIView*)parent tag:(NSInteger) tag alignment:(NSTextAlignment)alignment vAlignment:(UIControlContentVerticalAlignment)vAlignment{
    UITextField* tempText = [[[UITextField alloc] initWithFrame: rect] autorelease];
    tempText.textColor = [UIColor colorWithWhite:.95 alpha:1];
    tempText.font = [UIFont fontWithName:@"Helvetica Neue" size:14];
    tempText.tag = tag;
    tempText.textAlignment = alignment;
    tempText.contentVerticalAlignment = vAlignment;
    tempText.backgroundColor = [UIColor colorWithWhite:1 alpha:.2f];
    [tempText addTarget:self action:@selector(textChanged:) forControlEvents:UIControlEventEditingDidEnd];
    [parent addSubview:tempText];
    return tempText;
}


-(UITextView*) createNewTextViewWithDefaultStyle:(CGRect)rect parent:(UIView*)parent tag:(NSInteger) tag alignment:(NSTextAlignment)alignment {
    UITextView* tempText = [[[UITextView alloc] initWithFrame: rect] autorelease];
    tempText.textColor = [UIColor colorWithWhite:.95 alpha:1];
    tempText.font = [UIFont fontWithName:@"Helvetica Neue" size:14];
    tempText.tag = tag;
    tempText.textAlignment = alignment;
    tempText.backgroundColor = [UIColor colorWithWhite:1 alpha:.2f];
    [parent addSubview:tempText];
    return tempText;
}



-(UILabel*) createNewTextLabelWithDefaultStyle:(CGRect)rect parent:(UIView*)parent tag:(NSInteger) tag{
    return [self createNewTextLabelWithDefaultStyle:rect parent:parent tag:tag alignment:NSTextAlignmentLeft];
}

-(UILabel*) createNewTextLabelWithDefaultStyle:(CGRect)rect parent:(UIView*)parent tag:(NSInteger)tag alignment:(NSTextAlignment)alignment{
    UILabel* tempText = [[[UILabel alloc] initWithFrame: rect] autorelease];
    tempText.textColor = [UIColor colorWithWhite:.95 alpha:1];
    tempText.font = [UIFont fontWithName:@"Helvetica Neue" size:14];
    tempText.tag = tag;
    tempText.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
    tempText.textAlignment = alignment;
    [parent addSubview:tempText];
    return tempText;
    
}

-(UIButton*) createNewButtonWithDefaultStyle:(CGRect)rect parent:(UIView*)parent tag:(NSInteger) tag{
    UIButton* tempbutton = [[[UIButton alloc] initWithFrame: rect] autorelease];
    tempbutton.titleLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:14];
    [tempbutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [tempbutton setTitle:@"xxxxxx" forState:UIControlStateNormal];
    tempbutton.tag = tag;
    [UIHelper applyGreenTinyButtonImageOnButton:tempbutton];
    [parent addSubview:tempbutton];
    return tempbutton;
}

#pragma mark - Helper SF Data Method
-(NSString*) getFieldDisplayWithFieldName:(NSString*)fieldName withDictionary:(NSDictionary*) data{
    return [DataHelper getFieldDisplayWithFieldName:fieldName withDictionary: data];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    NSLog(@"textFieldDidBeginEditing %@", textField);
    if (self.delegate != nil ){
        [self.delegate textFieldDidBeginEditing:textField];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    NSLog(@"textFieldDidEndEditing %@", textField);
    if (self.delegate != nil ){
        [self.delegate textFieldDidEndEditing:textField];
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    NSLog(@"textViewDidBeginEditing %@", textView);
    
    if (self.delegate != nil ){
        [self.delegate textViewDidBeginEditing:textView];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    NSLog(@"textViewDidEndEditing %@", textView);
    if (self.delegate != nil ){
        [self.delegate textViewDidEndEditing:textView];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if([text isEqualToString:@"\n"] )
    {
        [textView resignFirstResponder];
        return NO;
    }
    
    NSUInteger newLength = [textView.text length] + [text length] - range.length;
    if(newLength>255){//It is 80 originally
        return NO;
    }
    return YES;
}



- (void)textChanged:(id)sender{
    if (self.delegate != nil ){
        [self.delegate textChanged:sender];
    }
}

- (void)onClickSegmentControl:(id)sender{
    [self.delegate onClickSegmentControl:sender];
}



- (void)textViewDidChange:(UITextView *)textView{
    if (self.delegate != nil ){
        [self.delegate textChanged:textView];
    }
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)text{

    if([text isEqualToString:@"\n"] )
    {
        [textField resignFirstResponder];
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [text length] - range.length;
    if(newLength>255){//It is 80 originally
        return NO;
    }
    return YES;
}

@end
