//
//  SurveyListViewController.m
//  VFStorePerformanceApp
//
//  Created by Developer 2 on 6/3/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "SurveyListViewController.h"
#import "Constants.h"
#import "SurveyFormViewController.h"
#import "GeneralUiTool.h"
#import "PDFViewController.h"
#import "ShareLocale.h"
#import "DataHelper.h"

#define TAB_BLUE_COLOR [UIColor colorWithRed:24/255.0f green:48/255.0f blue:193/255.0f alpha:1]

@interface SurveyListViewController ()

@end

@implementation SurveyListViewController

//For Survey List
- (id)initWithMasterId:(NSString *)masterId data:(NSDictionary*)data mode:(SurveyListMode)mode{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        self.mode = mode;
        self.selectedSurveyMasterId = masterId;
        self.surveyMasterData = [[[NSDictionary alloc] initWithDictionary:data] autorelease];
    }
    return self;
}

- (id)initWithType:(NSString*)type title:(NSString*)title{
    return [self initWithType:type title:title mode:SurveyMasterListModeCreateSurvey];
}

//For SurveyMaster List
- (id)initWithType:(NSString*)type title:(NSString*)title mode:(SurveyListMode)mode{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        self.title = title;
        self.type = type;
        self.mode = mode;
    }
    return self;
}

- (IBAction)onHomeButtonClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.backgroundColor = TABLE_BG_COLOR_BLUE;
    self.tableView.backgroundView = nil;
    self.recordAry = [NSMutableArray array];
    self.reportItemArray = [NSMutableArray array];
    self.formItemArray = [NSMutableArray array];

    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    
    if ( self.mode == SurveyMasterListModeCreateSurvey){
        self.titleLabel.text = [NSString stringWithFormat:@"Select %@ Template", self.title];
        [self.headerCodeButton setTitle: [ShareLocale textFromKey:@"surveylist_templatename"] forState:UIControlStateNormal];
        [self.headerTitleLabel2 setTitle: [ShareLocale textFromKey:@"surveylist_templatetitle"] forState:UIControlStateNormal];
        [self.headerTitleLabel3 setTitle: [ShareLocale textFromKey:@"surveylist_question"] forState:UIControlStateNormal];
        [self.headerTitleLabel5 setTitle: [ShareLocale textFromKey:@"surveylist_score"] forState:UIControlStateNormal];

    }
    else if ( self.mode == SurveyMasterListModeSurveyHistory ){
        self.titleLabel.text = [NSString stringWithFormat:@"Select %@ Template", self.title];
        [self.headerCodeButton setTitle: [ShareLocale textFromKey:@"surveylist_templatename"] forState:UIControlStateNormal];
        [self.headerTitleLabel2 setTitle: [ShareLocale textFromKey:@"surveylist_templatetitle"] forState:UIControlStateNormal];
        [self.headerTitleLabel3 setTitle: [ShareLocale textFromKey:@"surveylist_question"] forState:UIControlStateNormal];
        [self.headerTitleLabel5 setTitle: [ShareLocale textFromKey:@"surveylist_score"] forState:UIControlStateNormal];
    }
    else if ( self.mode == SurveyListModeSurveyHistory ){
        self.titleLabel.text = [NSString stringWithFormat: [ShareLocale textFromKey:@"surveylist_title"], self.title];
        [self.headerCodeButton setTitle: [ShareLocale textFromKey:@"surveylist_formid"] forState:UIControlStateNormal];
        [self.headerTitleLabel2 setTitle: [ShareLocale textFromKey:@"surveylist_detail"] forState:UIControlStateNormal];
        [self.headerTitleLabel3 setTitle: [ShareLocale textFromKey:@"surveylist_createby"] forState:UIControlStateNormal];
        [self.headerTitleLabel5 setTitle: [ShareLocale textFromKey:@"surveylist_score"] forState:UIControlStateNormal];

    }
    [self.headerTitleLabel4 setTitle:[ShareLocale textFromKey:@"surveylist_date"] forState:UIControlStateNormal];


    if ( self.currStoreName != nil ){
        self.storeNameLabel.text = self.currStoreName;
    }
    
//    [self loadData];
    if ( self.mode == SurveyMasterListModeCreateSurvey ||
        self.mode == SurveyMasterListModeSurveyHistory){//Is this code still using?
        [self loadSurveyMaster];
    }else{
        [self loadSurvey];
    }
    
    [self initCommentSidebarWithStoreId: nil];
    
    [self.formTabButton setTitle:[ShareLocale textFromKey:@"surveylist_tab1"] forState:UIControlStateNormal];
    [self.reportTabButton setTitle:[ShareLocale textFromKey:@"surveylist_tab2"] forState:UIControlStateNormal];
    
    if([self.type isEqualToString: SURVEY_TYPE_VISIT_FORM]){
        ;
        self.reportTabButton.hidden = YES;
        self.formTabButton.backgroundColor = TAB_BLUE_COLOR;
        self.formTabButton.userInteractionEnabled = YES;
    }else{
        self.formTabButton.backgroundColor = TAB_BLUE_COLOR;
        self.formTabButton.userInteractionEnabled = YES;
        self.reportTabButton.backgroundColor = [UIColor blackColor];
        self.reportTabButton.userInteractionEnabled = YES;
    }
    selectedTabIndex = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_backButton release];
    [_tableView release];
    [_titleLabel release];
    [_headerTitleLabel2 release];
    [_headerTitleLabel3 release];
    [_headerCodeButton release];
    [_storeNameLabel release];
    [_formTabButton release];
    [_reportTabButton release];
    [_headerTitleLabel4 release];
    [_selectedFileName release];
    [_selectedFileType release];
    [_selectedSurveyMasterId release];
    [_selectedTableViewCell release];
    [_headerTitleLabel5 release];
    [super dealloc];
}


-(NSDictionary*) getItemByIndex:(int)index{
    if(self.mode == SurveyListModeSurveyHistory){
        if(selectedTabIndex == 0){
            return [self.formItemArray objectAtIndex:index];
        }
        else{
            return [self.reportItemArray  objectAtIndex:index];
        }
    }else{
        return [self.recordAry  objectAtIndex:index];
    }
}


#pragma mark - Handle Click event of Back button & Reload button
- (IBAction)onBackButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onFormButtonClicked:(id)sender {
    selectedTabIndex = 0;
    self.formTabButton.backgroundColor = TAB_BLUE_COLOR;
    self.reportTabButton.backgroundColor = [UIColor blackColor];
    [self.tableView reloadData];
}

- (IBAction)onReportButtonClicked:(id)sender {
    selectedTabIndex = 1;
    self.reportTabButton.backgroundColor = TAB_BLUE_COLOR;
    self.formTabButton.backgroundColor = [UIColor blackColor];
    [self.tableView reloadData];
}


#pragma mark - Table Delegate

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ( self.mode == SurveyListModeSurveyHistory ){
        //Preview Old Survey
        if(selectedTabIndex==1){
            NSDictionary* data = [self getItemByIndex:indexPath.row];
            NSArray* medias = [[data objectForKey:@"Shared_Media__r"] objectForKey:@"records"];
            NSDictionary* media = [medias objectAtIndex:0];
            
            NSString* fileType = [media objectForKey:@"emfa__File_Ext__c"];
            
            self.selectedFileName = [media objectForKey:@"emfa__File_Name__c"];
            self.selectedFileType = [media objectForKey:@"emfa__File_Ext__c"];

            [self showLoadingPopup:self.view];
            
            self.selectedTableViewCell = [tableView cellForRowAtIndexPath: indexPath];
            
            
            ImageLoader* loader = [ImageLoader shareInstance];
            [loader downloadDocWithFeedID:[self getFieldDisplayWithFieldName:@"emfa__FeedItemId__c" withDictionary: media] type:fileType delegate:self];

            //PDF Report
            
            
//            PDFViewController* vc = [[PDFViewController alloc] initWithFeedItemID: [self getFieldDisplayWithFieldName:@"emfa__FeedItemId__c" withDictionary: media] ];
//            [self.navigationController pushViewController:vc animated:YES];
            
        }else{
            SurveyFormViewController* vc = [[SurveyFormViewController alloc] initWithSurveyMode:SurveyFormModePreviewSurvey andSurvey:[self getItemByIndex:indexPath.row] editable:NO];
            if ( [self.type isEqualToString:SURVEY_TYPE_SOP_Audit]){
                [vc setType:SurveyFormTypeSOPForm];
            }
            else if ( [self.type isEqualToString: SURVEY_TYPE_MSP_SURVEY] ){
                [vc setType:SurveyFormTypeMSPForm];
            }
            else if ( [self.type isEqualToString: SURVEY_TYPE_VISIT_FORM] ){
                [vc setType:SurveyFormTypeVisitForm];
            }
            [vc setCurrVisitPlan:self.currVisitPlan];
            [vc setCurrStore:self.currStore];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
//    else {
//        selectedIndex = indexPath.row;
//        [tableView deselectRowAtIndexPath: indexPath animated:NO];
//        
//        if ( self.currVisitPlan != nil ){
//            menuSheet = [[UIActionSheet alloc] initWithTitle:nil
//                                                    delegate:self
//                                           cancelButtonTitle:@"Cancel"
//                                      destructiveButtonTitle:nil
//                                           otherButtonTitles:@"View history", @"Clone as new template",@"Fill in survey", nil];
//        }else {
//            menuSheet = [[UIActionSheet alloc] initWithTitle:nil
//                                                    delegate:self
//                                           cancelButtonTitle:@"Cancel"
//                                      destructiveButtonTitle:nil
//                                           otherButtonTitles:@"View history", @"Clone as new template", nil];
//        }
//        
//        // Show the sheet
//        [menuSheet showInView:self.view];
//    }
}

//- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
//    if (actionSheet == menuSheet) {
//        //FLOG(@"Button %d", buttonIndex);
//        switch (buttonIndex) {
//            case 0:{
//                //FLOG(@"  Save to Camera Roll");
//                NSDictionary *masterData = [self.recordAry objectAtIndex:selectedIndex];
//                NSString *masterId = [masterData objectForKey:@"Id"];
//                
//                SurveyListViewController *vc = [[SurveyListViewController alloc] initWithMasterId:masterId data:masterData mode:SurveyListModeSurveyHistory];
//                [vc setCurrVisitPlan:self.currVisitPlan];
//                [vc setCurrStore:self.currStore];
//                [self.navigationController pushViewController:vc animated:YES];
//            }
//                break;
//                
//            case 1:{
////                NSLog(@"Clone as new Template");
//                NSAssert(NO, @"Create survey template is not supported");
////                    SurveyFormViewController * vc = [[SurveyFormViewController alloc] initWithSurveyMaster: [self.recordAry objectAtIndex:selectedIndex] andMode:SurveyFormModeCreateMaster];
////                    [vc setCurrVisitPlan:self.currVisitPlan];
////                    [vc setCurrStore:self.currStore];
////                    [self.navigationController pushViewController:vc animated:YES];
//            }
//                break;
//            case 2:{
//                NSLog(@"Fill in Form");
//                if ( self.currVisitPlan != nil){
//                    SurveyFormViewController * vc = [[SurveyFormViewController alloc] initWithSurveyMaster:[self.recordAry objectAtIndex:selectedIndex] andMode:SurveyFormModeCreateSurvey ];
//                    [vc setCurrVisitPlan:self.currVisitPlan];
//                    [vc setCurrStore:self.currStore];
//                    [self.navigationController pushViewController:vc animated:YES];
//                }
//            }
//                break;
//            default:
//                break;
//        }
//    }
//}

- (float) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(self.mode == SurveyListModeSurveyHistory){
        if(selectedTabIndex == 0){
            return [self.formItemArray count];
        }
        else{
            return [self.reportItemArray count];
        }
    }else{
        return [self.recordAry count];
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SurveyIdentifier"];
    if(cell == nil){
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SurveyIdentifier"] autorelease];
        cell.textLabel.hidden = YES;
        //cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        cell.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        UIView *selectionColor = [[[UIView alloc] init] autorelease];
        selectionColor.backgroundColor = TABLE_ROW_HIGHLIGHT_COLOR;
        cell.selectedBackgroundView = selectionColor;
        
        [self createNewTextLabelWithDefaultStyle: CGRectMake(10, 5, 200, 40) parent:cell tag: 901];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(200, 5, 400, 40) parent:cell tag: 902];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(610, 5, 80, 40) parent:cell tag: 905];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(710, 5, 100, 40) parent:cell tag: 903];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(810, 5, 205, 40) parent:cell tag: 904];
        
    }
    
    cell.tag = indexPath.row;
    
    NSDictionary* data = [self getItemByIndex:indexPath.row];
    
    if ( self.mode == SurveyListModeSurveyHistory ){
        
        
        UILabel* txtView1 = (UILabel*)[cell viewWithTag:901];
        txtView1.text = [self getFieldDisplayWithFieldName:@"Name" withDictionary: data];

        UILabel* txtView2 = (UILabel*)[cell viewWithTag:902];
        
        if(selectedTabIndex==1){
            NSArray* medias = [[data objectForKey:@"Shared_Media__r"] objectForKey:@"records"];
            NSDictionary* media = [medias objectAtIndex:0];
            txtView2.text = [self getFieldDisplayWithFieldName:@"emfa__File_Name__c" withDictionary: media];
        }else{
            txtView2.text = [self getFieldDisplayWithFieldName:@"Visitation_Plan_Name__c" withDictionary: data];
        }
        
        id pdf = [self getFieldDisplayWithFieldName:@"PDF_Report_Attached__c" withDictionary: data];
        UILabel* txtView5 = (UILabel*)[cell viewWithTag:905];
        if(pdf!=nil && ![pdf boolValue]){
            float formScore = [[self getFieldDisplayWithFieldName:@"Total_Score__c" withDictionary: data] floatValue];
            float maxScore = [[self getFieldDisplayWithFieldName:@"Maximum_Score__c" withDictionary:data] floatValue];
            //@"emfa__Survey_Master__r.Total_Score__c"
            txtView5.text = [NSString stringWithFormat:@"%.0f/%.0f", formScore, maxScore];
        }else{
            txtView5.text = @"";
        }

        UILabel* txtView3 = (UILabel*)[cell viewWithTag:903];
        if(pdf!=nil && ![pdf boolValue]){
            txtView3.text = [self getFieldDisplayWithFieldName:@"Visitor__c" withDictionary: data];
        }else{
            txtView3.text = [self getFieldDisplayWithFieldName:@"CreatedBy.Name" withDictionary: data];
        }
        UILabel* txtView4 = (UILabel*)[cell viewWithTag:904];
        
        NSDateFormatter* sfdf= [GeneralUiTool getSFDateFormatter];
        NSDateFormatter* displaydf = [[NSDateFormatter alloc] init];
        [displaydf setDateFormat: DEFAULT_DATE_TIME_FORMAT];
        
        NSDate* cdate;
        
        if(pdf!=nil && ![pdf boolValue]){
            cdate = [sfdf dateFromString: [self getFieldDisplayWithFieldName:@"Visit_Start_Date_Time__c" withDictionary: data]];
        }else{
            cdate = [sfdf dateFromString: [self getFieldDisplayWithFieldName:@"CreatedDate" withDictionary: data]];
        }
        txtView4.text = [displaydf stringFromDate:cdate];
    }
    else {
        
        UILabel* txtView1 = (UILabel*)[cell viewWithTag:901];
        txtView1.text = [self getFieldDisplayWithFieldName:@"Name" withDictionary: data];
        
        UILabel* txtView2 = (UILabel*)[cell viewWithTag:902];
        txtView2.text = [self getFieldDisplayWithFieldName:@"Name" withDictionary: data];
        
        UILabel* txtView3 = (UILabel*)[cell viewWithTag:903];
        txtView3.text = [self getFieldDisplayWithFieldName:@"Number_Of_Question__c" withDictionary: data];
        
        UILabel* txtView4 = (UILabel*)[cell viewWithTag:904];
        
        NSDateFormatter* sfdf= [GeneralUiTool getSFDateFormatter];
        NSDateFormatter* displaydf = [[NSDateFormatter alloc] init];
        [displaydf setDateFormat: DEFAULT_SIMPLE_DATE_FORMAT];
        
        NSDate* cdate = [sfdf dateFromString: [self getFieldDisplayWithFieldName:@"CreatedDate" withDictionary: data]];
        
        txtView4.text = [displaydf stringFromDate:cdate];
    }
    return cell;
}



#pragma mark - Image delegate
-(void) onRequestSuccessOfRequest:(ImageLoaderRequest *)req feedItemID:(NSString *)feedItemId response:(id)jsonResponse contentType:(NSString *)contentType{
    [self hideLoadingPopup];
    
    __block SurveyListViewController* me = self;

    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *storePath = [applicationDocumentsDir stringByAppendingPathComponent:
                               [NSString stringWithFormat:@"%@.%@", feedItemId, contentType]];
        
        NSURL* url = [NSURL fileURLWithPath:storePath];
        
        NSLog(@"url=%@", url);
        
        me.documentController = [ UIDocumentInteractionController interactionControllerWithURL: url];
        me.documentController.name = me.selectedFileName;
        
        CGRect cellFrame = me.selectedTableViewCell.frame;
        me.selectedTableViewCell = nil;
        
        BOOL done = [ me.documentController presentOpenInMenuFromRect: cellFrame inView: me.tableView animated: YES ];
        NSLog(@"done %@", done?@"Y":@"N");
    });
    
}






-(void) onRequestFailOfRequest:(ImageLoaderRequest *)req feedItemID:(NSString *)feedItemId error:(NSError *)error{
    [self hideLoadingPopup];
    
    [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"" message:[NSString stringWithFormat:@"Fail to load File due to: %@", error] ];
}




#pragma mark - Network Delegate
-(void) loadSurveyMaster{
    if(self.querySurveyMasterRequest==nil){
        NSLog(@"SurveyListViewController::loadSurveyMaster()");

        [self showLoadingPopup: self.view];
        
        self.querySurveyMasterRequest = [[VFDAO sharedInstance] selectSurveyTemplatesWithDelegate:self surveyType:self.type];
        
    }
}

- (void) loadSurvey{
    if(self.querySurveyRequest==nil){
        NSLog(@"SurveyListViewController::loadSurvey()");
        [self showLoadingPopup: self.view];
        
        [self.recordAry removeAllObjects];
        [self.formItemArray removeAllObjects];
        [self.reportItemArray removeAllObjects];
        
        self.querySurveyRequest = [[VFDAO sharedInstance] selectSurveysFormWithDelegate:self surveyType:self.type];
        
    }
}


-(void) daoRequest:(DAORequest *)request queryOnlineDataSuccess:(NSArray *)records{
    [self hideLoadingPopupWithFadeout];

    if(self.querySurveyRequest == request){
        
        self.recordAry = [NSMutableArray array];
        
        [self.recordAry addObjectsFromArray:records];
        
        NSUserDefaults *defaultPref = [NSUserDefaults standardUserDefaults];
        NSString* preferBrandCode = [defaultPref objectForKey: USER_PREFER_ACCOUNT_CODE];
        NSString* preferCountryCode = [defaultPref objectForKey: USER_PREFER_COUNTRY_CODE];
        
        for(NSDictionary* item in self.recordAry){
            
            NSString* acctCode = [DataHelper getFieldDisplayWithFieldName:@"Account__r.Code__c" withDictionary:item default:nil];
            NSString* acctCountryCode = [DataHelper getFieldDisplayWithFieldName:@"Account__r.Country_Code__c" withDictionary:item default:nil];
            NSString* storeAcctCode = [DataHelper getFieldDisplayWithFieldName:@"Store__r.emfa__Account__r.Code__c" withDictionary:item default:nil];
            NSString* storeCountryCode = [DataHelper getFieldDisplayWithFieldName:@"Store__r.emfa__Account__r.Country_Code__c" withDictionary:item default:nil];
            
            NSString* recordBrandCode = acctCode==nil? storeAcctCode:acctCode;
            NSString* recordCountryCode = acctCountryCode==nil? storeCountryCode:acctCountryCode;
            
            if([recordBrandCode isEqualToString:preferBrandCode] && [recordCountryCode isEqualToString:preferCountryCode]){
                if([[item objectForKey:@"PDF_Report_Attached__c"] intValue] == 1){
                    NSDictionary* medias = [item objectForKey:@"Shared_Media__r"];
                    if(medias!=(id)[NSNull null] &&  [[medias objectForKey:@"records"] count]>0){
                        [self.reportItemArray addObject: item];
                    }
                }else{
                    [self.formItemArray addObject: item];
                }
            }
        }
        
        
        [self performSelectorOnMainThread:@selector(reloadTableList) withObject:nil waitUntilDone:NO];
        self.querySurveyRequest = nil;
    }
    else if (self.querySurveyMasterRequest == request){
        [self.recordAry addObjectsFromArray: records];
        [self performSelectorOnMainThread:@selector(reloadTableList) withObject:nil waitUntilDone:NO];
        self.querySurveyMasterRequest = nil;
    }
}


-(void) daoRequest:(DAORequest *)request queryOnlineDataError:(NSString *)response code:(int)code{
    [self hideLoadingPopupWithFadeout];
    [self handleQueryDataErrorCode:code description:response];
}





#pragma mark - Business Logic
-(void)reloadTableList{
    [self.tableView reloadData];
}

@end
