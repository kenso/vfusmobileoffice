//
//  SurveyFormViewController.m
//  VFStorePerformanceApp
//
//  Created by Developer 2 on 6/3/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "SurveyFormViewController.h"
#import "QuestionView.h"
#import "DataHelper.h"
#import "CustomWebserviceRestApi.h"
#import "GeneralUiTool.h"
#import "Constants.h"
#import "VFDAO.h"
#import "ShareLocale.h"
#import "QuestionCreateView.h"
#import <QuartzCore/QuartzCore.h>

@interface SurveyFormViewController ()

@end

@implementation SurveyFormViewController

/**
    Init a survey with mode and survey ID.  The page will first load survey data from SF, 
    then load survey master data and reload display finally.
    @para mode and surveyId can not be null
 **/
- (id)initWithSurveyMode:(SurveyFormMode)mode andSurveyId:(NSString *)surveyId editable:(BOOL)edit{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        sectionTagArray = [[NSMutableArray alloc] init];
        self.dataTypeDict = [[[NSMutableDictionary alloc] init] autorelease];
        self.questionAnswerDict = [[[NSMutableDictionary alloc] init] autorelease];
        if ( surveyId != nil ) self.currSurveyId = surveyId;
        //else self.surveyDict = [NSMutableDictionary dictionary];
        self.mode = mode;
        self.editable = edit;

    }
    return self;
}

/**
    Init a survey with mode and survey ID.  The page will first survey master data and
    reload display finally.
     
 **/
- (id)initWithSurveyMode:(SurveyFormMode)mode andSurvey:(NSDictionary *)survey editable:(BOOL)edit{
    self = [super initWithNibName:nil bundle:nil];
    if (self){
        sectionTagArray = [[NSMutableArray alloc] init];
        self.dataTypeDict = [[[NSMutableDictionary alloc] init] autorelease];
        self.questionAnswerDict = [[[NSMutableDictionary alloc] init] autorelease];
        self.surveyDict = survey;
        self.mode = mode;
        self.editable = edit;

    }
    return self;
}

- (IBAction)onHomeButtonClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Load SurveyMasterData
    
//    [self loadSurveyMasterData];
    //Load question recordType
//    [self loadQuestionTypeData];
    
//    questionCreatePopup = [[QuestionCreateView alloc] initWithFrame:self.view.frame withDelegate:self];
//    [questionCreatePopup setHidden:YES];
//    [self.view addSubview:questionCreatePopup];
    [self.submitButton setTitle:[ShareLocale textFromKey:@"save"] forState:UIControlStateNormal];
    [self.submitButton setHidden:!self.editable];
    self.scoreLabel.hidden = self.editable;

//    if ( self.mode == SurveyFormModeCreateSurvey ){
//        self.headerTitleLabel.text = [NSString stringWithFormat:@"%@>%@", self.currVisitPlanName, self.currSurveyMasterName];
//        self.topTitleLabel.text = @"Fill in the form";
//    }
//    else if ( self.mode == SurveyFormModePreviewSurvey ){
//        self.headerTitleLabel.text = [NSString stringWithFormat:@"ID: %@", [self.surveyDict objectForKey:@"Name" ]];
//        self.topTitleLabel.text = @"View Form Hisotry";
//    }
    if(self.type == SurveyFormTypeVisitForm){
        self.headerTitleLabel.text = [ShareLocale textFromKey:@"surveytitle_visit"];
    }
    else if (self.type == SurveyFormTypeMSPForm){
        self.headerTitleLabel.text = [ShareLocale textFromKey:@"surveytitle_msp"];
    }
    else if(self.type == SurveyFormTypeSOPForm){
        self.headerTitleLabel.text = [ShareLocale textFromKey:@"surveytitle_audit"];
    }
    
//

    [self registerForKeyboardNotifications];
    
    [self.scrollView setUserInteractionEnabled:YES];
    [self.scrollView setDelegate:self];
    
    [self initCommentSidebarWithStoreId: nil];
    
    
    
    
    if ( self.currSurveyId!=nil ){
        //Case1: Can be view it or edit it, but need first load the survey data
        [self loadSurveyAnswerData];
    }
    else if(self.surveyDict!=nil){
        //Case2: User provided the survey detail, may be view it or edit(depending on editable property)
        [self resetSurveyMasterDataFromCache];
        [self reloadSurveyContent];
    }
    else{
        //Case3: User want to fill in a new form
        [self resetSurveyMasterDataFromCache];
        [self reloadSurveyContent];
    }


    
}

// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}


- (void)dealloc {
    [_titleLabel release];
    [_scrollView release];
    [_formView release];
    [_submitButton release];
    [_headerTitleLabel release];
    [_topTitleLabel release];
//    [_statusIconImage release];
    [sectionTagArray release];
    [_innerView release];
    [_sectionInnerView release];
    [_scoreLabel release];
    [super dealloc];
}


#pragma mark - Click events

- (IBAction)onBackButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onSubmitButtonClicked:(id)sender {
    
    //check input valid
    if ( [self isValidForm] ){
        
        NSMutableDictionary *surveyDict = [[[NSMutableDictionary alloc] init] autorelease];
        [surveyDict setObject:[self.surveyMasterDict objectForKey:@"Id"] forKey:@"emfa__Survey_Master2__c"];
        [surveyDict setObject:self.questionAnswerDict forKey:@"answers"];
        
        
        if( [self.surveyDict objectForKey:@"Id"]!=nil){
            [surveyDict setObject:[self.surveyDict objectForKey:@"Id"] forKey:@"Id"];
        }
        
        [surveyDict setObject:self.currVisitPlan forKey:@"Visit_Plan__c"];
        if(self.currVisitPlanName!=nil) [surveyDict setObject:self.currVisitPlanName forKey:@"Visit_Plan_Name__c"];
        NSLog(@"self.questionAnswerDict=%@", self.questionAnswerDict);
        
        [self showLoadingPopup:self.view];
        
        self.submitRequest = [[VFDAO sharedInstance] upsertSurveyForm:surveyDict delegate:self];
        
    }else{
        [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"" message:[ShareLocale textFromKey:@"error_answer_required"]];
    }

}

//- (void)onRequestComplete:(CustomWebserviceRequest *)request response:(NSDictionary *)response{
//    [self hideLoadingPopupWithFadeout];
//    
//    if ( request == self.submitRequest ){
//        id success = [response objectForKey:@"success"];
//        if ( [success isEqualToString:@"true"] ){
//            
//            self.surveyDict = @{
//                                @"Id": [response objectForKey:@"insertid"],
//                                @"Type__c": (self.type==SurveyFormTypeVisitForm)?SURVEY_TYPE_VISIT_FORM:(self.type==SurveyFormTypeMSPForm?SURVEY_TYPE_MSP_SURVEY:SURVEY_TYPE_SOP_Audit)
//                                };
//            
//            [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"" message:[ShareLocale textFromKey:@"dialog_confirm_saved"] yesHanlder:^(id data){
//                [self.saveDelegate onSurveyFormSave:self.surveyDict];
//                [self.navigationController popViewControllerAnimated:YES];
//            }];
//            self.submitRequest = nil;
//        }
//    }
//}
//
//- (void)onRequestFail:(CustomWebserviceRequest *)request error:(NSString *)error{
//    [self hideLoadingPopupWithFadeout];
//    [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:[ShareLocale textFromKey:@"error"] message: [ShareLocale textFromKey:@"dialog_saved_fail"]];
//}

#pragma mark - Network delegate
- (void)resetSurveyMasterDataFromCache{
    NSString *surveyType = @"";
    if ( self.type == SurveyFormTypeVisitForm ){
        surveyType = SURVEY_TYPE_VISIT_FORM;
    }else if ( self.type == SurveyFormTypeSOPForm ){
        surveyType = SURVEY_TYPE_SOP_Audit;
    }else if ( self.type == SurveyFormTypeMSPForm ){
        surveyType = SURVEY_TYPE_MSP_SURVEY;
    }else{
        surveyType = @"";
    }
    self.surveyMasterDict = [NSDictionary dictionary];
    
    VFDAO* vf = [VFDAO sharedInstance];
    if(self.surveyDict!=nil){
        //need load specific survey master
        NSString* masterID = [self.surveyDict objectForKey:@"emfa__Survey_Master2__c"];
        self.surveyMasterDict = [vf getSurveyMasterFromCacheWithID: masterID];

        NSAssert(self.surveyMasterDict!=nil, @"SurveyMaster should not be nil, masterID=%@", masterID);
    }
    else{
        //load latest and correct type one survey master
        self.surveyMasterDict = [vf getLatestSurveyMasterFromCacheWithType:surveyType];
        
        if(self.surveyMasterDict==nil){
            [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:[ShareLocale textFromKey:@"error"] message:[ShareLocale textFromKey:@"dialog_no_survey_master"]];
            return;
        }
    }
    NSDictionary* questionObj = [self.surveyMasterDict objectForKey:@"emfa__Question_Masters__r"];
    if(questionObj!=(id)[NSNull null]){
        questionMasterArray = [questionObj objectForKey:@"records"];
    }
    else{
        questionMasterArray = (NSMutableArray *)@[];
    }
    
    NSString* name = [self.surveyMasterDict objectForKey:@"Name"];
    NSLog(@"self.surveyMasterDict Name=%@", name);
    
    //a known bug on iOS7.1 SDK.  Change title of the button
    self.titleLabel.enabled = FALSE;
    [self.titleLabel setTitle: name forState:UIControlStateNormal];
    self.titleLabel.enabled = TRUE;

    NSMutableDictionary* defaults = vf.cache;
    NSArray * recordTypes = [defaults objectForKey:@"question_type"];
    for(NSDictionary* type in recordTypes){
        [self.dataTypeDict setObject:type forKey:[type objectForKey:@"Id"]];
    }
}


- (void)loadSurveyAnswerData{
    NSAssert(self.querySurveyAnswerRequest==nil, @"The request is not end");
    
    [self showLoadingPopup: self.view];
    
    self.querySurveyAnswerRequest = [[VFDAO sharedInstance] selectSurveyFormWithDelegate:self surveyId:self.currSurveyId];

}

-(void)daoRequest:(DAORequest*)request queryOnlineDataSuccess:(NSArray*) records{
    [self hideLoadingPopupWithFadeout];
    
    if(self.querySurveyAnswerRequest == request){
        self.querySurveyAnswerRequest = nil;
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            if([records count]>0){
                self.surveyDict = [records objectAtIndex:0];
                
                [self resetSurveyMasterDataFromCache];
                
                [self reloadSurveyContent];
            }else{
                [self handleQueryDataErrorCode:80001 description:@"No record can be found."];
            }
        });
    }
    else if ( request == self.submitRequest ){
        NSDictionary* data = [records lastObject];
        self.surveyDict = @{
                            @"Id": [data objectForKey:@"insertid"],
                            @"Type__c": (self.type==SurveyFormTypeVisitForm)?SURVEY_TYPE_VISIT_FORM:(self.type==SurveyFormTypeMSPForm?SURVEY_TYPE_MSP_SURVEY:SURVEY_TYPE_SOP_Audit)
                            };
        
        [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"" message:[ShareLocale textFromKey:@"dialog_confirm_saved"] yesHanlder:^(id data){
            [self.saveDelegate onSurveyFormSave:self.surveyDict];
            [self.navigationController popViewControllerAnimated:YES];
        }];
        self.submitRequest = nil;
    }
}


-(void)daoRequest:(DAORequest*)request queryOnlineDataError:(NSString*) response code:(int)code{
    [self hideLoadingPopupWithFadeout];
    [self handleQueryDataErrorCode:code description:response];
}



#pragma mark - Other business logic
-(void) reloadSurveyContent{
    
    top_padding = 20;
    left_padding = 20;
    total_y = top_padding;
    currSectionTag = 0;
    currSection_height = 0;
    question_offset = 20;//110;
    section_offset = 20;
    section_width = 800;
    question_view_width = section_width - 20-20;
    question_view_padding = 20;
    basic_view_height = 50;
    
    NSString* surveyAutoID = [self.surveyDict objectForKey:@"Name"];
    if(surveyAutoID!=nil){
        self.topTitleLabel.text = surveyAutoID;
    }else{
        self.topTitleLabel.text = [ShareLocale textFromKey:@"fillin_new_form"];
    }
    
    

    int question_index;
    
    for ( int i = 0; i < [questionMasterArray count]; i ++){
        NSDictionary *dict = [questionMasterArray objectAtIndex:i];
        //Question
        NSString *idStr = [self getFieldDisplayWithFieldName:@"Id" withDictionary:dict];
        NSString *questionStr = [self getFieldDisplayWithFieldName:@"emfa__Title__c" withDictionary:dict];
        
        //Answer
        NSString *recordTypeId = [self getFieldDisplayWithFieldName:@"RecordTypeId" withDictionary:dict];
        NSString *developerName = [[self.dataTypeDict objectForKey:recordTypeId] objectForKey:@"DeveloperName"];

        NSDictionary *ansDict = [self getAnswerDictWithQuestionId:[dict objectForKey:@"Id"]];
        
        NSString *answerStr = [DataHelper getFieldDisplayWithFieldName:@"emfa__Answer_Value__c" withDictionary:ansDict default:@""];
        NSString *extraCommentStr = [DataHelper getFieldDisplayWithFieldName:@"emfa__Comment__c" withDictionary:ansDict default:@""];

        
        
        int required = [[self getFieldDisplayWithFieldName:@"emfa__Required__c" withDictionary:dict] intValue];
        
        if ( [developerName isEqualToString:@"Question_Group"]){
            
            UIView *view = [[[UIView alloc] initWithFrame:CGRectMake(left_padding, total_y+section_offset, section_width, 70)] autorelease];
            [view.layer setBorderColor:[UIColor whiteColor].CGColor];
            [view.layer setBorderWidth:1.0f];
//            [view setBackgroundColor: [UIColor orangeColor]];
            
            UILabel * questionlabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(left_padding+20, 0, 700, 50) parent:view tag:1 alignment:NSTextAlignmentLeft];
            questionlabel.text = questionStr;
            questionlabel.font = [UIFont fontWithName:@"Helvetica Neue" size:20];
            UILabel * answerlabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(left_padding, 0+20, 700, 50) parent:view tag:1 alignment:NSTextAlignmentLeft];
            answerlabel.text = @"--------------------------------------------------------------------------";
            [view setTag:900+i];
            [self.scrollView addSubview:view];
            
            currSectionTag = 900+i;
            [sectionTagArray addObject:[NSString stringWithFormat:@"%i", currSectionTag]];
            currSection_height = view.frame.size.height;
            total_y += question_offset+basic_view_height+section_offset;
            question_index = 1;
        }else{
            CGPoint point = CGPointMake(left_padding+question_view_padding, total_y);

            
//            if ([developerName isEqualToString:@"Number"] ){
//                NSString* ansStr = [ansDict objectForKey:@"emfa__Answer_Value__c"];
//                formScore += [ ansStr floatValue];
//            }
//            else if ([developerName isEqualToString:@"Yes_No"] ){
//                float yesS = [[ansDict objectForKey:@"Yes_Score__c"] floatValue];
//                float noS = [[ansDict objectForKey:@"No_Score__c"] floatValue];
//                NSString* ansStr = [ansDict objectForKey:@"emfa__Answer_Value__c"];
//                
//                if(ansStr!=nil){
//
//                    if([ansStr isEqualToString:@"yes"] || [ansStr isEqualToString:@"YES"] || [ansStr isEqualToString:@"Yes"] || [ansStr isEqualToString:@"Y"] || [ansStr isEqualToString:@"y"]){
//                        formScore += yesS;
//                    }
//                    else if([ansStr isEqualToString:@"no"] || [ansStr isEqualToString:@"NO"] || [ansStr isEqualToString:@"No"] || [ansStr isEqualToString:@"N"] || [ansStr isEqualToString:@"n"]){
//                        formScore += noS;
//                    }
//
//                }
//                
//            }
            
            
            QuestionView* view;
            if ( self.mode == SurveyFormModeCreateSurvey ){
                view = [[QuestionView alloc] initWithPoint:point andDict:dict andType:developerName andMode:SurveyQuestionModeNormal andIndex:question_index andTag:i withAnsDict:ansDict editable:self.editable andDelegate:self];
            
            }
            else if ( self.mode == SurveyFormModePreviewSurvey ){
                view = [[QuestionView alloc] initWithPoint:point andDict:dict andType:developerName andMode:SurveyQuestionModePreview andIndex:question_index andTag:i withAnsDict:ansDict editable:self.editable andDelegate:self];
            }
            else{
                view = nil;
            }
            
            NSMutableDictionary *newAnsDict = [[[NSMutableDictionary alloc] init] autorelease];
            [newAnsDict setObject:[NSString stringWithFormat:@"%i", i] forKey:@"tag"];
            [newAnsDict setObject:idStr forKey:@"emfa__Question_Master__c"];
            [newAnsDict setObject:answerStr forKey:@"emfa__Answer_Value__c"];
            [newAnsDict setObject:extraCommentStr forKey:@"emfa__Comment__c"];
            [newAnsDict setObject:[NSNumber numberWithInt:required] forKey:@"emfa__Required__c"];
            [self.questionAnswerDict setObject:newAnsDict forKey:[NSString stringWithFormat:@"%i", i]];

            
            [self.scrollView addSubview:view];
            currSection_height += view.frame.size.height+20;
            [self setSectionViewWithTag:currSectionTag andHeight:currSection_height];
            
            total_y += view.frame.size.height + question_offset;
            question_index ++;
            
        
        }
        
    }
    
    
    if(!self.editable && [self.surveyDict objectForKey:@"Total_Score__c"]!=(id)[NSNull null] &&
       //[self.surveyMasterDict objectForKey:@"Total_Score__c"]!=(id)[NSNull null]
       [self.surveyDict objectForKey:@"Maximum_Score__c"]!=(id)[NSNull null]
       ){
        float formScore =  [[self.surveyDict objectForKey:@"Total_Score__c"] floatValue];
        float maxScore =  [[self.surveyDict objectForKey:@"Maximum_Score__c"] floatValue];
        self.scoreLabel.text = [NSString stringWithFormat: @"%.0f / %.0f", formScore, maxScore];
    }else{
        self.scoreLabel.text = @"";
    }
    
    UIScrollView* scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.sectionInnerView.frame.size.width, self.sectionInnerView.frame.size.height )];
    [self.sectionInnerView addSubview: scrollView];
    
    //Add A List of SectionShortCut in Right Menu
    for ( int i = 0; i < [sectionTagArray count]; i ++){
        int sectionTag = [[sectionTagArray objectAtIndex:i] intValue];
        UIButton *shortCutButton = [self createNewButtonWithDefaultStyle:CGRectMake(8, 20+65*i, 140, 45) parent:scrollView tag:i];
        shortCutButton.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        NSString *titleStr = [[questionMasterArray objectAtIndex:(sectionTag-900)] objectForKey:@"emfa__Title__c"];
        [shortCutButton setTitle:titleStr forState:UIControlStateNormal];
        [shortCutButton addTarget:self action:@selector(onClickShortCutButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    [scrollView setContentSize:CGSizeMake(140, ([sectionTagArray count])*65+20+20 )];
    [scrollView release];

    
    UILabel * headerLabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(left_padding+20, 5, 700, 50) parent:self.scrollView tag:1 alignment:NSTextAlignmentLeft];
    NSString* headerStr = [self.surveyMasterDict objectForKey:@"emfa__Header__c"];
    if(headerStr==nil || headerStr==(id)[NSNull null]){
        headerStr = @"";
    }
    NSString* footerStr = [self.surveyMasterDict objectForKey:@"emfa__Footer__c"];
    if(footerStr==nil || footerStr==(id)[NSNull null]){
        footerStr = @"";
    }
    headerLabel.text = headerStr;
    headerLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:18];
    UILabel * footerLabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(left_padding+20, total_y, 700, 50) parent:self.scrollView tag:1 alignment:NSTextAlignmentLeft];
    footerLabel.text = footerStr;
    footerLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:18];
    total_y += 100;
    
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, total_y)];
    
    currTotal_Y = total_y;
}

- (NSDictionary *)getAnswerDictWithQuestionId:(NSString *)questionId{
    NSDictionary* recordDict = [self.surveyDict objectForKey:@"emfa__Question_Answers__r"];
    if(recordDict!=nil && recordDict!=(id)[NSNull null]){
        NSArray *answerArr = [recordDict objectForKey:@"records"];
        if ( answerArr != nil ){
            for ( int i = 0; i < [answerArr count]; i ++ ){
                NSDictionary *dict = [answerArr objectAtIndex:i];
                if ( [[dict objectForKey:@"emfa__Question_Master__c"] isEqualToString:questionId]){
                    return dict;
                }
            }
        }
    }
    return [NSDictionary dictionary];
}


-(void)textElementDidBeginEditing:(UIView*)sender{
    editing = YES;
    UIView *view = sender.superview;
    [UIScrollView beginAnimations:@"scrollAnimation2" context:nil];
    [UIScrollView setAnimationDuration:0.5f];
    [self.scrollView setContentOffset:CGPointMake(0, view.frame.origin.y)];
    [UIScrollView commitAnimations];
    
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^ {
                         [self.view setFrame:CGRectMake(0,-100,self.view.frame.size.width,self.view.frame.size.height)];
                     }completion:^(BOOL finished) {
                         
                     }];

}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self textElementDidBeginEditing:textField];
}


-(void)textViewDidBeginEditing:(UITextView *)textView{
    [self textElementDidBeginEditing:textView];
}

-(void)textElementDidEndEditing:(UIView*)sender{
    if ( !editing ){
        [UIView animateWithDuration:0.3
                              delay:0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^ {
                             [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
                         }completion:^(BOOL finished) {
                             
                         }];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self textElementDidEndEditing:textField];
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    [self textElementDidEndEditing:textView];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    NSLog(@"keyboardWillBeHidden Working!!!");
    editing = NO;
}


- (void)onClickSegmentControl:(id)sender{
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    int selectedIndex = segmentedControl.selectedSegmentIndex;
    int index = [segmentedControl superview].tag;
    NSLog( @"onClickSegmentControl in %i   %i  ", selectedIndex, index);
    
    NSMutableDictionary *ansDict = [self.questionAnswerDict objectForKey:[NSString stringWithFormat:@"%i", index]];
    [ansDict setObject:[segmentedControl titleForSegmentAtIndex:selectedIndex] forKey:@"emfa__Answer_Value__c"];
//    [ansDict setObject:@"" forKey:@"emfa__Comment__c"];
    [self.questionAnswerDict setObject:ansDict forKey:[NSString stringWithFormat:@"%i", index]];
}


-(void) onResetButtonClicked:(NSInteger) tag{
    NSMutableDictionary *ansDict = self.questionAnswerDict[[NSString stringWithFormat:@"%i", tag]];
    ansDict[@"emfa__Answer_Value__c"] = @"";
    ansDict[@"emfa__Comment__c"] = @"";
    self.questionAnswerDict[[NSString stringWithFormat:@"%i", tag]] = ansDict ;
    //do nth for remark part
}

#pragma mark - Line Order Items delegate
-(void)textChanged:(UITextField *)textField
{
    int tag = textField.tag;
    int index = [textField superview].tag;
    NSLog( @"Text Change in %i   %i  with %@", tag, index, textField.text);
    NSMutableDictionary *ansDict = [self.questionAnswerDict objectForKey:[NSString stringWithFormat:@"%i", index]];
    if ( tag == 13803 ){//900
        [ansDict setObject:textField.text forKey:@"emfa__Comment__c"];
    }else{
        [ansDict setObject:textField.text forKey:@"emfa__Answer_Value__c"];
    }
    [self.questionAnswerDict setObject:ansDict forKey:[NSString stringWithFormat:@"%i", index]];
}

- (void)onClickShortCutButton:(id)sender{
    UIView* view = (UIView*)sender;
    int tagNum = view.tag;
    int sectionTag = [[sectionTagArray objectAtIndex:tagNum] intValue];
    NSLog(@"sectionTag go: %i", sectionTag);
    
    UIView *sectionView = [self.scrollView viewWithTag:sectionTag];
    
    [self moveScrollViewWithHeight:sectionView.frame.origin.y];
}

- (void)onClickAddNewSectionButton:(id)sender{
    [questionCreatePopup setMode:QuestionCreateModeSection];
    [questionCreatePopup refreshAll];
    [questionCreatePopup setHidden: NO];
}

- (void)onClickAddNewQuestionButton:(id)sender{
    [questionCreatePopup setMode:QuestionCreateModeText];
    [questionCreatePopup refreshAll];
    [questionCreatePopup setHidden: NO];
}

/*CreateQuestionDelegate*/
- (void)onQuestionCreateComplete:(NSDictionary *)dict{
    NSLog(@"onQuestionCreateComplete: dict: %@", dict);
    
    /*
     [dict setObject:[self getQuestionTypeIdByName:@"Number"] forKey:@"RecordTypeId"];
     [dict setObject:@"New Number Question " forKey:@"emfa__Title__c"];
     [dict setObject:@"1" forKey:@"emfa__Min_Number__c"];
     [dict setObject:@"5" forKey:@"emfa__Max_Number__c"];
     [dict setObject:@"false" forKey:@"emfa__Extra_Comment__c"];
     //emfa__Extra_Comment__c
     UIView *newText = [self createQuestionViewWithPoint:CGPointMake(20, currTotal_Y-70) andDict:dict andIndex:0 andTag:0];
     [self.scrollView addSubview:newText];
     [self extentScrollViewWithOffset:110];
     */
    if ( dict != nil ){
        NSMutableDictionary*newDict = [[NSMutableDictionary alloc] initWithDictionary:dict];
        if ([[dict objectForKey:@"Type"] isEqualToString:@"Question_Group"] ){
            //Add New Section
            
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(left_padding, total_y+section_offset-70, section_width, 70)];
//            [view setBackgroundColor: [UIColor orangeColor]];
            [view.layer setBorderColor:[UIColor whiteColor].CGColor];
            [view.layer setBorderWidth:1.0f];

            UILabel * questionlabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(left_padding+20, 0, 700, 50) parent:view tag:1 alignment:NSTextAlignmentLeft];
            questionlabel.text = [dict objectForKey:@"emfa__Title__c"];
            questionlabel.font = [UIFont fontWithName:@"Helvetica Neue" size:20];
            UILabel * answerlabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(left_padding, 0+20, 700, 50) parent:view tag:1 alignment:NSTextAlignmentLeft];
            answerlabel.text = @"--------------------------------------------------------------------------";
            currSectionTag ++;
            [view setTag:currSectionTag];
            [self.scrollView addSubview:view];
            
            currSection_height = view.frame.size.height;
            total_y += question_offset+basic_view_height+section_offset;
            [self extentScrollViewWithOffset:view.frame.size.height +20 withButton:YES];
        }else{
            //Add New Question
            
            [newDict setObject:[self getQuestionTypeIdByName:[dict objectForKey:@"Type"]] forKey:@"RecordTypeId"];
            //        UIView *newView = [self createQuestionViewWithPoint:CGPointMake(20+20, currTotal_Y-70) andDict:newDict andIndex:0 andTag:0];
            QuestionView *newView = [[QuestionView alloc] initWithPoint:CGPointMake(20+20, currTotal_Y-70) andDict:newDict andType:[dict objectForKey:@"Type"] andMode:SurveyQuestionModeEdit andIndex:0 andTag:0 editable:self.editable andDelegate:self];
            [newView setSectionTag:currSectionTag];
            
            CAShapeLayer *_border;
            //Dotted Line for view border
            _border = [CAShapeLayer layer];
            _border.strokeColor = [UIColor yellowColor].CGColor;
            _border.fillColor = nil;
            _border.lineDashPattern = @[@4, @2];
            _border.lineWidth = 1.5f;
            [newView.layer addSublayer:_border];
            _border.path = [UIBezierPath bezierPathWithRect:newView.bounds].CGPath;
            _border.frame = newView.bounds;

            [newView setUserInteractionEnabled:NO];
            [self.scrollView addSubview:newView];
            [self addSectionViewWithTag:[newView sectionTag] andHeight:([newView getViewHeight]+20)];
            [self extentScrollViewWithOffset:[newView getViewHeight] +20 withButton:YES];
        }
    }
}

- (NSString *)getQuestionTypeIdByName:(NSString *)typeName{
    for ( NSDictionary *dict in [self.dataTypeDict allValues]){
        if ( [[dict objectForKey:@"DeveloperName"] isEqualToString:typeName] ){
            return [dict objectForKey:@"Id"];
        }
    }
    return nil;
}

- (void)moveScrollViewWithHeight:(int)height{
    [UIScrollView beginAnimations:@"scrollAnimation2" context:nil];
    [UIScrollView setAnimationDuration:0.5f];
    [self.scrollView setContentOffset:CGPointMake(0, height)];
    [UIScrollView commitAnimations];
}

- (void)extentScrollViewWithOffset:(int)offset{
    [self extentScrollViewWithOffset:offset withButton:NO];
}

- (void)extentScrollViewWithOffset:(int)offset withButton:(BOOL)withButton{
    
    int old_width = self.scrollView.contentSize.width;
    int old_height = self.scrollView.contentSize.height;
    [self.scrollView setContentSize:CGSizeMake(old_width, old_height+offset)];
    currTotal_Y += offset;

    if (withButton){
        [addNewSectionButton setFrame:CGRectMake(20, currTotal_Y-70, 150, 40)];
        [addNewQuestionButton setFrame:CGRectMake(20+200, currTotal_Y-70, 150, 40)];
    }
    
    //Scroll ScrollingView to the end
    [UIScrollView beginAnimations:@"scrollAnimation2" context:nil];
    [UIScrollView setAnimationDuration:1.0f];
    [self.scrollView setContentOffset:CGPointMake(0, self.scrollView.contentSize.height-self.scrollView.frame.size.height)];
    [UIScrollView commitAnimations];
}

- (void)setSectionViewWithTag:(int)tag andHeight:(int)height{
    UIView *sectionView = [self.scrollView viewWithTag:tag];
//    NSLog(@"tag: %i Height:  %i ", tag, height);
    [sectionView setFrame:CGRectMake(sectionView.frame.origin.x,
                                    sectionView.frame.origin.y,
                                    sectionView.frame.size.width,
                                    height)];
}

- (void)addSectionViewWithTag:(int)tag andHeight:(int)height{
    UIView *sectionView = [self.scrollView viewWithTag:tag];
    [self setSectionViewWithTag:tag andHeight:sectionView.frame.size.height +height];
}

- (BOOL)isValidForm{
    NSArray* values = [self.questionAnswerDict allValues];
    BOOL answerFound = NO;
    for(NSDictionary* ans in values){
        if (ans[@"emfa__Answer_Value__c"]!=nil && ans[@"emfa__Answer_Value__c"]!=(id)[NSNull null] && ![ans[@"emfa__Answer_Value__c"] isEqualToString:@""]){
            answerFound = YES;
        }
    }
    if(!answerFound){
        return NO;
    }
    return YES;
}

@end
