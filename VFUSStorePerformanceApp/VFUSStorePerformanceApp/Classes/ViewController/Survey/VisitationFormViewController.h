//
//  VisitationFormViewController.h
//  VFStorePerformanceApp
//
//  Created by Developer 2 on 10/3/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"

/**
 *  For demo
 */
@interface VisitationFormViewController : BasicViewController


//-(id) initWithSurveyID:(NSString*)surveyID;

- (IBAction)onBackButtonClicked:(id)sender;

- (IBAction)onSubmitButtonClicked:(id)sender;

- (IBAction)onHomeButtonClicked:(id)sender;

@end
