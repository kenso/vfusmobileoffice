/*
 Copyright (c) 2011, salesforce.com, inc. All rights reserved.
 
 Redistribution and use of this software in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this list of conditions
 and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list of
 conditions and the following disclaimer in the documentation and/or other materials provided
 with the distribution.
 * Neither the name of salesforce.com, inc. nor the names of its contributors may be used to
 endorse or promote products derived from this software without specific prior written
 permission of salesforce.com, inc.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#import <UIKit/UIKit.h>
#import "CustomWebserviceRestApi.h"
#import "MarqueeView.h"
#import "BasicViewController.h"
#import "VFDAO.h"
#import "GeneralSelectionPopup.h"
#import "OfflinePromptPopupView.h"

@interface HomeViewController : BasicViewController < UITableViewDataSource, UITableViewDelegate, DAODelegate, MarqueeDelegate, StoreSelectionViewControllerDelegate, GeneralSelectionPopupDelegate, UIPopoverControllerDelegate> {//DAODelegate,
    int currentRow;
    
}


@property (retain, nonatomic) IBOutlet UILabel *cardOpsSales;
@property (retain, nonatomic) IBOutlet UILabel *cardDiscount;
@property (retain, nonatomic) IBOutlet UILabel *cardPeeloff;
@property (retain, nonatomic) IBOutlet UILabel *cardProductivity;
@property (retain, nonatomic) IBOutlet UILabel *cardConvRate;
@property (retain, nonatomic) IBOutlet UILabel *cardAtv;
@property (retain, nonatomic) IBOutlet UILabel *cardUpt;
@property (retain, nonatomic) IBOutlet UILabel *cardTxn;
@property (retain, nonatomic) IBOutlet UILabel *cardVisitor;
@property (nonatomic, retain) NSMutableArray* messageRecordArray;
@property (nonatomic, retain) MarqueeView* marqueeView;
@property (retain, nonatomic) IBOutlet UIView *broadNoticeView;
@property (retain, nonatomic) IBOutlet UIButton *globalButton;
@property (retain, nonatomic) IBOutlet UIButton *storeListButton;//1st
@property (retain, nonatomic) IBOutlet UIButton *trainingButton;
@property (retain, nonatomic) IBOutlet UIButton *selectButton;
@property (retain, nonatomic) IBOutlet UIButton *visitPlanButton;
@property (retain, nonatomic) IBOutlet UIButton *syncButton;
@property (retain, nonatomic) IBOutlet UIButton *settingButton;
@property (retain, nonatomic) IBOutlet UIButton *auditButton;
@property (retain, nonatomic) IBOutlet UIButton *staffButton;
@property (retain, nonatomic) IBOutlet UIButton *customerButton;
@property (retain, nonatomic) IBOutlet UIButton *usageButton;
@property (retain, nonatomic) IBOutlet UIButton *inventoryButton;
@property (retain, nonatomic) IBOutlet UIView *offllineDownloadPopup;
@property (retain, nonatomic) IBOutlet UIButton *compStatusButton;

@property (assign, nonatomic) BOOL lastOnlineMode;

//For StoreReport CR
@property (retain, nonatomic) UIPopoverController* storeReportSelectionPopup;
//@property (retain, nonatomic) UIView* auditMenu;

@property (retain, nonatomic) NSString* selectedDashboardDate;
@property (retain, nonatomic) UIPopoverController* filterVC;
@property (retain, nonatomic) NSMutableArray* filterArray;
@property (assign, nonatomic) int selectedFilterIndex;

@property (retain, nonatomic) NSString* selectedCompStatusVal;
@property (retain, nonatomic) UIPopoverController* compStatusPopup;
@property (retain, nonatomic) NSMutableArray* compStatusArray;
@property (retain, nonatomic) NSMutableArray* storeArray;
@property (retain, nonatomic) NSMutableSet* compStatusSet;



@property (retain, nonatomic) NSMutableArray* waitQueryCompAccountArray;

@property (retain, nonatomic) NSMutableArray* waitQueryAccountArray;

@property (nonatomic, retain) DAOBaseRequest* queryBrandListRequest;
@property (nonatomic, retain) DAOBaseRequest* queryBrandDetaliRequest;
//New added @ 2015-01-06 for admin login bug(data too large)
@property (nonatomic, retain) DAOBaseRequest* queryCapacityRequest;
@property (nonatomic, retain) DAOBaseRequest* queryNotificationRequest;
@property (nonatomic, retain) DAOBaseRequest* queryTargetsRequest;
@property (nonatomic, retain) DAOBaseRequest* querySurveyMasterRequest;

@property (nonatomic, retain) OfflinePromptPopupView *offlinePopup;

+(void) setRedirectViewController:(UIViewController*)vc;


- (IBAction)onHelpButtonClicked:(id)sender;
- (IBAction)onGlobalButtonClicked:(id)sender;

- (IBAction)onStoreButtonClicked:(id)sender;
- (IBAction)onInventoryButtonClicked:(id)sender;
- (IBAction)onVisitPlanButtonClicked:(id)sender;
- (IBAction)onStaffButtonClicked:(id)sender;
- (IBAction)onCustomerButtonClicked:(id)sender;
- (IBAction)onAuditResultButtonClicked:(id)sender;
- (IBAction)onTrainingMaterialButtonClicked:(id)sender;
- (IBAction)onUsageReportButtonClicked:(id)sender;
- (IBAction)onSettingButtonClicked:(id)sender;
- (IBAction)onSyncButtonClicked:(id)sender;
- (IBAction)onCompStatusButtonClicked:(id)sender;


- (void)onInboxButtonClicked:(id)sender;

- (IBAction)onReleaseAuditResultButton:(id)sender;


@property (retain, nonatomic) IBOutlet UIView *opsCard;
@property (retain, nonatomic) IBOutlet UIView *discountCard;
@property (retain, nonatomic) IBOutlet UIView *atvCard;
@property (retain, nonatomic) IBOutlet UIView *uptCard;
@property (retain, nonatomic) IBOutlet UIView *conversionCard;
@property (retain, nonatomic) IBOutlet UIView *visitorCard;
@property (retain, nonatomic) IBOutlet UIView *transactionCard;
@property (retain, nonatomic) IBOutlet UIView *peelOffCard;
@property (retain, nonatomic) IBOutlet UIView *salesProductivityCard;



@end