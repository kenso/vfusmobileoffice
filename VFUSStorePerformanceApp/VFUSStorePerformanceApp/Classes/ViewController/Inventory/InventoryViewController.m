//
//  InventoryViewController.m
//  VFStorePerformanceApp_Billy
//
//  Created by Developer 2 on 11/4/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "InventoryViewController.h"
#import "ShareLocale.h"
#import "Constants.h"

@interface InvetorySummaryData: NSObject

@property (nonatomic, retain) NSString* name;
@property (nonatomic, assign) NSInteger quantityOnHand;
@property (nonatomic, assign) CGFloat amountOnhand;

@end

@implementation InvetorySummaryData

-(id)init{
    if(self = [super init]){
        self.quantityOnHand = 0;
        self.amountOnhand = 0;
    }
    return self;
}

@end



@interface InventoryViewController ()

@end

@implementation InventoryViewController

-(id)initWithDictionary:(NSDictionary*)data{

    self = [super initWithNibName:@"InventoryViewController" bundle:nil];
    if (self) {
        self.data = data;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    VFDAO* vf = [VFDAO sharedInstance];
    self.fxdate = [vf.cache objectForKey:@"fxdate"];
    self.fxmonth = [vf.cache objectForKey:@"fxmonth"];
    self.fxweek = [vf.cache objectForKey:@"fxweek"];
    self.fxyear = [vf.cache objectForKey:@"fxyear"];
    
    NSString* storeSfID = [self.data objectForKey:@"Id"];
    [self initCommentSidebarWithStoreId: storeSfID];
    
    [self loadData];
    
    [self refreshLocale];
}




-(void) refreshLocale{
    self.headerTitleLabel.text = [NSString stringWithFormat:[ShareLocale textFromKey:@"invdetail_title"], [self.data objectForKey:@"Name"]];
    [self.topSalesbutton setTitle:[ShareLocale textFromKey:@"invdetail_tab1"] forState:UIControlStateNormal];
    [self.inventoryButton setTitle:[ShareLocale textFromKey:@"invdetail_tab2"] forState:UIControlStateNormal];
    [self.marketingButton setTitle:[ShareLocale textFromKey:@"invdetail_tab3"] forState:UIControlStateNormal];
    
    //top sales
    self.skuLabel.text = [ShareLocale textFromKey:@"invdetail_sku"];
    self.weekOpsSalesLabel.text = [ShareLocale textFromKey:@"invdetail_weekopssales"];
    self.weekSalesQtyLabel.text = [ShareLocale textFromKey:@"invdetail_weeksalesqty"];
    self.onHandInvLabel.text = [ShareLocale textFromKey:@"invdetail_onhandinv"];
    self.productName.text = [ShareLocale textFromKey:@"invdetail_product"];
    //inventory
    self.genderLabel.text = [ShareLocale textFromKey:@"invdetail_gender"];
    self.categoryLabel.text = [ShareLocale textFromKey:@"invdetail_category"];
    self.inventoryClassLabel.text = [ShareLocale textFromKey:@"invdetail_inventoryclass"];
    self.qtyOnhandLabel.text = [ShareLocale textFromKey:@"invdetail_qtyonhand"];
    self.amountOnhandLabel.text = [ShareLocale textFromKey:@"invdetail_amountonhand"];
    self.invCapacityLabel.text = [ShareLocale textFromKey:@"invdetail_capacity"];
    
    //Marketing
    self.articleLabel.text = [ShareLocale textFromKey:@"invdetail_article"];
    self.descLabel.text = [ShareLocale textFromKey:@"invdetail_desc"];
    self.salesQtyLabel.text = [ShareLocale textFromKey:@"invdetail_salesqty"];
    self.totalAmtOnHandLabel.text = [ShareLocale textFromKey:@"invdetail_amountonhand"];
    self.totalQtyOnHandLabel.text = [ShareLocale textFromKey:@"invdetail_qtyonhand"];
    self.inventorySalesLabel.text = [ShareLocale textFromKey:@"invdetail_inventoryvssales"];
    self.opsLabel.text = [ShareLocale textFromKey:@"invdetail_opssales"];
    self.totalOpsLabel.text = [ShareLocale textFromKey:@"invdetail_totalops"];
    self.opsPctLabel.text = [ShareLocale textFromKey:@"invdetail_opspct"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [_lowStockButton release];
    [_topSalesbutton release];
    [_topSalesView release];
    [_lowStockView release];
    [_headerTitleLabel release];
    [_inventoryView release];
    [_inventoryButton release];
    [_productName release];
    [_weekOpsSalesLabel release];
    [_weekSalesQtyLabel release];
    [_onHandInvLabel release];
    [_cutToDateLabel release];
    [_genderLabel release];
    [_categoryLabel release];
    [_inventoryClassLabel release];
    [_qtyOnhandLabel release];
    [_amountOnhandLabel release];
    [_articleLabel release];
    [_descLabel release];
    [_salesQtyLabel release];
    [_totalAmtOnHandLabel release];
    [_totalQtyOnHandLabel release];
    [_inventorySalesLabel release];
    [_opsLabel release];
    [_totalOpsLabel release];
    [_opsPctLabel release];
    [_invCapacityLabel release];
    [_containerView release];
    [super dealloc];
}



- (IBAction)onLowStockButtonClicked:(id)sender {
    _inventoryButton.backgroundColor = [UIColor blackColor];
    _lowStockButton.backgroundColor = [UIColor colorWithWhite:55/255.0f alpha:1];
    _topSalesbutton.backgroundColor = [UIColor blackColor];
    _marketingButton.backgroundColor = [UIColor blackColor];
    [self resetActiveTabPage:_lowStockView];
    
    for (UIView* v in [_lowStockView subviews]){
        if([v isKindOfClass:[UILabel class]] || [v isKindOfClass:[UITextView class]] )
            [v removeFromSuperview];
    }
    int offsetX = 20;
    int offsetY = 100;
    int index = 0;
    int lineHeight = 22;
    for(NSDictionary* data in self.lowStockRecords){
        UITextView* skuLbl = [[UITextView alloc] initWithFrame:CGRectMake(offsetX, offsetY + index * lineHeight, 180, lineHeight)];
        skuLbl.editable = NO;
        skuLbl.scrollEnabled = NO;
        skuLbl.font = [UIFont fontWithName:@"Arial" size:14];
        skuLbl.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        skuLbl.textColor = [UIColor whiteColor];
        skuLbl.text = [data objectForKey:@"Generic_Article__c"];
        UITextView* nameLbl = [[UITextView alloc] initWithFrame:CGRectMake(offsetX + 170, offsetY + index * lineHeight, 380, lineHeight)];
        nameLbl.font = [UIFont fontWithName:@"Arial" size:14];
        nameLbl.editable = NO;
        nameLbl.scrollEnabled = NO;
        nameLbl.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        nameLbl.textColor = [UIColor whiteColor];
        nameLbl.text = [data objectForKey:@"Item_Description__c"];
        UILabel* stockLbl = [[UILabel alloc] initWithFrame:CGRectMake(offsetX + 560, offsetY + index * lineHeight, 40, lineHeight)];
        stockLbl.font = [UIFont fontWithName:@"Arial" size:14];
        stockLbl.textAlignment = NSTextAlignmentRight;
        stockLbl.textColor = [UIColor whiteColor];
        stockLbl.text = [NSString stringWithFormat:@"%@",[data objectForKey:@"Stock_On_Hand__c"]];
        UILabel* salesLbl = [[UILabel alloc] initWithFrame:CGRectMake(offsetX + 700, offsetY + index * lineHeight, 40, lineHeight)];
        salesLbl.font = [UIFont fontWithName:@"Arial" size:14];
        salesLbl.textColor = [UIColor whiteColor];
        id val = [data objectForKey:@"Sales_Quantity__c"];
        if(val==nil || val == [NSNull null]) val = @"-";
        salesLbl.text = [NSString stringWithFormat:@"%@", val];
        salesLbl.textAlignment = NSTextAlignmentRight;
        [_lowStockView addSubview: skuLbl];
        [_lowStockView addSubview: nameLbl];
        [_lowStockView addSubview: stockLbl];
        [_lowStockView addSubview: salesLbl];
        
        index++;
        if(index>=20) break;
    }
    
}



- (IBAction)onMarketingButtonClicked:(id)sender {
    _inventoryButton.backgroundColor = [UIColor blackColor];
    _lowStockButton.backgroundColor = [UIColor blackColor];
    _topSalesbutton.backgroundColor = [UIColor blackColor];
    _marketingButton.backgroundColor = [UIColor colorWithWhite:55/255.0f alpha:1];
    
    [self resetActiveTabPage:_marketingView];
    
    for (UIView* v in [_marketingView subviews]){
        if([v isKindOfClass:[UILabel class]] || [v isKindOfClass:[UITextView class]] )
            [v removeFromSuperview];
    }
    
    UIScrollView* scv = (UIScrollView*)[_marketingView viewWithTag:311];
    for (UIView* v in [scv subviews]){
        if([v isKindOfClass:[UILabel class]] || [v isKindOfClass:[UITextView class]] )
            [v removeFromSuperview];
    }
    
    int offsetX = 18;
    int offsetY = 10;
    int index = 0;
    int lineHeight = 22;
    for(NSDictionary* data in self.marketingRecords){
        UITextView* skuLbl = [[UITextView alloc] initWithFrame:CGRectMake(offsetX, offsetY + index * lineHeight, 140, lineHeight)];
        skuLbl.editable = NO;
        skuLbl.scrollEnabled = NO;
        skuLbl.font = [UIFont fontWithName:@"Arial" size:14];
        skuLbl.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        skuLbl.textColor = [UIColor whiteColor];
        skuLbl.text = [data objectForKey:@"Generic_Article__c"];
        
        UITextView* nameLbl = [[UITextView alloc] initWithFrame:CGRectMake(offsetX + 140, offsetY + index * lineHeight, 300, lineHeight)];
        nameLbl.font = [UIFont fontWithName:@"Arial" size:14];
        nameLbl.editable = NO;
        nameLbl.scrollEnabled = NO;
        nameLbl.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        nameLbl.textColor = [UIColor whiteColor];
        nameLbl.text = [data objectForKey:@"Item_Description__c"];
        
        int numWidth = 60;
        
        
        float totalOps = 0;
        float totalAmount = 0;
        float totalQuantityOnHand = 0;
        NSString* cat = [data objectForKey:@"SBU__c"];
        //NSString* gender = [data objectForKey:@"Gender__c"];

        for(NSDictionary* smr in self.salesMixRecords){
            if( [smr objectForKey:@"Category__c"]!=(id)[NSNull null] && [[smr objectForKey:@"Category__c"] isEqualToString:cat]
               //&& [smr objectForKey:@"Gender__c"]!=(id)[NSNull null] && [[smr objectForKey:@"Gender__c"] isEqualToString:gender]
               ){
                totalOps += [[smr objectForKey:@"OPS_Sales_Amount__c"] floatValue];
            }
        }
        
        
        for(NSDictionary* soh in self.stockOnHandRecords){
            if([soh objectForKey:@"Category__c"]!=(id)[NSNull null] && [[soh objectForKey:@"Category__c"] isEqualToString:cat]){
                totalAmount += [[soh objectForKey:@"Amount_On_Hand__c"] floatValue];
                totalQuantityOnHand += [[soh objectForKey:@"Quantity_On_Hand__c"] floatValue];
                NSLog(@"totalAmount=%.0f", totalAmount);
            }
        }
        
        //QUANTITY
        UILabel* qtyLbl = [[UILabel alloc] initWithFrame:CGRectMake(offsetX + 430 + 0*(5+numWidth), offsetY + index * lineHeight, numWidth, lineHeight)];
        qtyLbl.font = [UIFont fontWithName:@"Arial" size:14];
        qtyLbl.textColor = [UIColor whiteColor];
        id val = [data objectForKey:@"Sales_Quantity__c"];
        int quantity = 0;
        if(val!=nil && val != [NSNull null]){
            quantity = [val intValue];
            val = [NSString stringWithFormat:@"%d", quantity];
        }else{
            val = @"-";
        }
        qtyLbl.text = [NSString stringWithFormat:@"%@", val];
        qtyLbl.textAlignment = NSTextAlignmentRight;
        
        //Total AMOUNT
        UILabel* amountLbl = [[UILabel alloc] initWithFrame:CGRectMake(offsetX + 440 + 2*(5+numWidth), offsetY + index * lineHeight, numWidth, lineHeight)];
        amountLbl.font = [UIFont fontWithName:@"Arial" size:14];
        amountLbl.textAlignment = NSTextAlignmentRight;
        amountLbl.textColor = [UIColor whiteColor];
        amountLbl.text = [NSString stringWithFormat:@"%.1fk", totalAmount/1000];
        
        
        //Total QUANTITY ON HAND
        UILabel* onHandLbl = [[UILabel alloc] initWithFrame:CGRectMake(offsetX + 440 + 1*(5+numWidth), offsetY + index * lineHeight, numWidth, lineHeight)];
        onHandLbl.font = [UIFont fontWithName:@"Arial" size:14];
        onHandLbl.textAlignment = NSTextAlignmentRight;
        onHandLbl.textColor = [UIColor whiteColor];
        onHandLbl.text = [NSString stringWithFormat:@"%.0f", totalQuantityOnHand];
        
        //QUANTITY / TOTAL QUANTITY %
        UILabel* totalOnHandLbl = [[UILabel alloc] initWithFrame:CGRectMake(offsetX + 440 + 3*(5+numWidth), offsetY + index * lineHeight, numWidth, lineHeight)];
        totalOnHandLbl.font = [UIFont fontWithName:@"Arial" size:14];
        totalOnHandLbl.textAlignment = NSTextAlignmentRight;
        totalOnHandLbl.textColor = [UIColor whiteColor];
        if(totalQuantityOnHand!=0){
            if(quantity!=0){
                totalOnHandLbl.text = [NSString stringWithFormat:@"%.0f", totalQuantityOnHand * 7 / quantity ];
            }else{
                totalOnHandLbl.text = @"_";
            }
        }else{
            totalOnHandLbl.text = @"-";
        }
        
        
        //OPS
        UILabel* opsLbl = [[UILabel alloc] initWithFrame:CGRectMake(offsetX + 440 + 4*(5+numWidth), offsetY + index * lineHeight, numWidth, lineHeight)];
        opsLbl.font = [UIFont fontWithName:@"Arial" size:14];
        opsLbl.textAlignment = NSTextAlignmentRight;
        opsLbl.textColor = [UIColor whiteColor];
        float opsSales = [[data objectForKey:@"OPS_Sales_Amount__c"] floatValue];
        opsLbl.text = [NSString stringWithFormat:@"%.1fk", opsSales/1000];
        
        
        //Total OPS
        UILabel* totalLbl = [[UILabel alloc] initWithFrame:CGRectMake(offsetX + 440 + 5*(5+numWidth), offsetY + index * lineHeight, numWidth, lineHeight)];
        totalLbl.font = [UIFont fontWithName:@"Arial" size:14];
        totalLbl.textAlignment = NSTextAlignmentRight;
        totalLbl.textColor = [UIColor whiteColor];
        totalLbl.text = [NSString stringWithFormat:@"%.1fk", totalOps/1000];
        

        //OPS/Total OPS %
        UILabel* totalPCLbl = [[UILabel alloc] initWithFrame:CGRectMake(offsetX + 440 + 6*(5+numWidth), offsetY + index * lineHeight, numWidth, lineHeight)];
        totalPCLbl.font = [UIFont fontWithName:@"Arial" size:14];
        totalPCLbl.textAlignment = NSTextAlignmentRight;
        totalPCLbl.textColor = [UIColor whiteColor];
        if(totalOps==0)  totalPCLbl.text = @"-";
        else totalPCLbl.text = [NSString stringWithFormat:@"%.1f%%", 100 * opsSales / totalOps];
        
        [scv addSubview: skuLbl];
        [scv addSubview: nameLbl];
        [scv addSubview: qtyLbl];
        [scv addSubview: amountLbl];
        [scv addSubview: onHandLbl];
        [scv addSubview: totalOnHandLbl];

        [scv addSubview: opsLbl];
        [scv addSubview: totalLbl];
        [scv addSubview: totalPCLbl];
        
        index++;
        
        scv.contentSize = CGSizeMake( scv.frame.size.width, offsetY + index * lineHeight+30);
        
        //if(index>=20) break;
    }
    
}




- (IBAction)onInventoryClicked:(id)sender {
    _topSalesbutton.backgroundColor = [UIColor blackColor];
    _inventoryButton.backgroundColor = [UIColor colorWithWhite:55/255.0f alpha:1];
    _lowStockButton.backgroundColor = [UIColor blackColor];
    _marketingButton.backgroundColor = [UIColor blackColor];

//    _topSalesView.hidden = YES;
//    _lowStockView.hidden = YES;
//    _inventoryView.hidden = NO;
//    _marketingView.hidden = YES;
//
    [self resetActiveTabPage:_inventoryView];
    
    NSMutableSet* genderSet = [NSMutableSet setWithArray:@[]];
    NSMutableSet* categorySet = [NSMutableSet setWithArray:@[]];
    
    NSMutableArray* genderSummaryArray = [NSMutableArray array];
    NSMutableArray* categorySummaryArray = [NSMutableArray array];

    
    VFDAO* vf = [VFDAO sharedInstance];
    NSArray* capacityRecords = [ vf.cache objectForKey:@"capacity"];
    NSString* code = [self.data objectForKey:@"Store_Code__c"];
    NSMutableArray* storeCapacityRecords = [[NSMutableArray alloc] init];
    for(NSDictionary* d in capacityRecords){
        if([[d objectForKey:@"Store_Code__c"] isEqualToString:code]){
            [storeCapacityRecords addObject:d];
        }
    }
    capacityRecords = nil;
    
    
    [[_invCapacityLabel viewWithTag:11827] removeFromSuperview];
    
    
    if([_inventoryView viewWithTag:998877]!=nil){
        [[_inventoryView viewWithTag:998877] removeFromSuperview];
    }
    
    UIScrollView* scrollingPanel = [[UIScrollView alloc] initWithFrame:CGRectMake(20, 100, 980, 300)];
    scrollingPanel.tag = 998877;
    [_inventoryView addSubview: scrollingPanel];
    scrollingPanel.contentSize = CGSizeMake(scrollingPanel.frame.size.width, scrollingPanel.frame.size.height);
    
    for (UIView* v in [scrollingPanel subviews]){
        if([v isKindOfClass:[UILabel class]] || [v isKindOfClass:[UITextView class]] )
            [v removeFromSuperview];
    }
    
    NSLog(@"storeCapacityRecords = %@", storeCapacityRecords);
    
    int offsetX = 0;//20;
    int offsetY = 0;//100;
    int index = 0;
    int lineHeight = 22;
    int totalHeight = 0;
    for(NSDictionary* data in self.inventoryRecords){
        UITextView* skuLbl = [[UITextView alloc] initWithFrame:CGRectMake(offsetX, offsetY + index * lineHeight, 180, lineHeight)];
        skuLbl.editable = NO;
        skuLbl.scrollEnabled = NO;
        skuLbl.font = [UIFont fontWithName:@"Arial" size:14];
        skuLbl.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        skuLbl.textColor = [UIColor whiteColor];
        NSString* gender = [data objectForKey:@"Gender__c"];
        skuLbl.text = (gender==nil || gender==(id)[NSNull null])?@"-":gender;
        
        UITextView* nameLbl = [[UITextView alloc] initWithFrame:CGRectMake(offsetX + 170, offsetY + index * lineHeight, 220, lineHeight)];
        nameLbl.font = [UIFont fontWithName:@"Arial" size:14];
        nameLbl.editable = NO;
        nameLbl.scrollEnabled = NO;
        nameLbl.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        nameLbl.textColor = [UIColor whiteColor];
        NSString* cat = [data objectForKey:@"Category__c"];;
        nameLbl.text = (cat==nil || cat==(id)[NSNull null])?@"-":cat;
        
        UITextView* invClassLbl = [[UITextView alloc] initWithFrame:CGRectMake(offsetX + 397, offsetY + index * lineHeight, 160, lineHeight)];
        invClassLbl.font = [UIFont fontWithName:@"Arial" size:14];
        invClassLbl.editable = NO;
        invClassLbl.scrollEnabled = NO;
        invClassLbl.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        invClassLbl.textColor = [UIColor whiteColor];
        NSString* invClass = [data objectForKey:@"Inventory_Class__c"];;
        invClassLbl.text = (invClass==nil || invClass==(id)[NSNull null])?@"-":invClass;
        
        //Get Gender Summary data
        InvetorySummaryData* genderData = nil;
        if(gender!=nil && gender!=(id)[NSNull null]){
            if([genderSet containsObject: gender ]){
                for(InvetorySummaryData* d in genderSummaryArray){
                    if([d.name isEqualToString:gender]){
                        genderData = d;
                    }
                }
            }else{
                genderData = [[InvetorySummaryData alloc] init];
                genderData.name = gender;
                [genderSummaryArray addObject:genderData];
                [genderSet addObject:gender];
            }
        }
        //Get Category Summary data
        InvetorySummaryData* catData = nil;
        if(cat!=nil && cat!=(id)[NSNull null]){
            if([categorySet containsObject: cat ]){
                for(InvetorySummaryData* d in categorySummaryArray){
                    if([d.name isEqualToString:cat]){
                        catData = d;
                    }
                }
            }else{
                catData = [[InvetorySummaryData alloc] init];
                catData.name = cat;
                [categorySummaryArray addObject: catData];
                [categorySet addObject:cat];
            }
        }

        
        UILabel* stockLbl = [[UILabel alloc] initWithFrame:CGRectMake(offsetX + 562, offsetY + index * lineHeight, 40, lineHeight)];
        stockLbl.font = [UIFont fontWithName:@"Arial" size:14];
        stockLbl.textAlignment = NSTextAlignmentRight;
        stockLbl.textColor = [UIColor whiteColor];
        stockLbl.text = [NSString stringWithFormat:@"%@",[data objectForKey:@"Quantity_On_Hand__c"]];
        
        catData.quantityOnHand += [[data objectForKey:@"Quantity_On_Hand__c"] intValue];
        genderData.quantityOnHand += [[data objectForKey:@"Quantity_On_Hand__c"] intValue];

        UILabel* amtLbl = [[UILabel alloc] initWithFrame:CGRectMake(offsetX + 700, offsetY + index * lineHeight, 60, lineHeight)];
        amtLbl.font = [UIFont fontWithName:@"Arial" size:14];
        amtLbl.textColor = [UIColor whiteColor];
        amtLbl.textAlignment = NSTextAlignmentRight;
        id amountOnhand = [data objectForKey:@"Amount_On_Hand__c"];
        if(amountOnhand!=(id)[NSNull null]){
            amtLbl.text =  [NSString stringWithFormat:@"%.1fk",[amountOnhand intValue]/1000.0 ];
            catData.amountOnhand += [amountOnhand floatValue];
            genderData.amountOnhand += [amountOnhand floatValue];
        }else{
            amtLbl.text = @"-";
        }

        
        UILabel* salesLbl = [[UILabel alloc] initWithFrame:CGRectMake(offsetX + 800, offsetY + index * lineHeight, 80, lineHeight)];
        salesLbl.font = [UIFont fontWithName:@"Arial" size:14];
        salesLbl.textColor = [UIColor whiteColor];
        salesLbl.textAlignment = NSTextAlignmentRight;
        
        int v1 = 0;
        int v2 = 0;
        for(NSDictionary* c in storeCapacityRecords){
            if([c objectForKey:@"Category__c"] != [NSNull null] && cat!=nil && cat!=(id)[NSNull null]){
                if([[c objectForKey:@"Category__c"] rangeOfString:cat ].location != NSNotFound){
                    v1 += [[c objectForKey:@"Capacity__c"] intValue];
                    v2 += [[c objectForKey:@"Capacity2__c"] intValue];
                }
            }
        }
        if(v1==0 && v2==0){
            salesLbl.text = @"-";
        }
        else if (v1==0){
            salesLbl.text = [NSString stringWithFormat:@"-/%d", v2];
        }
        else if (v2==0){
            salesLbl.text = [NSString stringWithFormat:@"%d/-", v1];
        }
        else{
            salesLbl.text = [NSString stringWithFormat:@"%d/%d", v1, v2];
        }
        
        totalHeight = offsetY + (index+1) * lineHeight;
        
        [scrollingPanel addSubview: skuLbl];
        [scrollingPanel addSubview: amtLbl];
        [scrollingPanel addSubview: nameLbl];
        [scrollingPanel addSubview: invClassLbl];
        [scrollingPanel addSubview: stockLbl];
        [scrollingPanel addSubview: salesLbl];
        
        index++;
        //if(index==15) break;//avoid too many records overlaping the bottom summary block
    }
    
    scrollingPanel.contentSize = CGSizeMake(scrollingPanel.frame.size.width, totalHeight);

    
    NSLog(@"cat %@", categorySummaryArray);
    NSLog(@"gender %@", genderSummaryArray);
    
    
    
    
    //Create Summary View
    NSInteger offetX = 30;
    NSInteger offetY = 10;
    NSInteger cellWidth = 150;
    NSInteger cellHeight = 25;
    NSInteger firstColWidth = 160;
    UIView* summaryPanel = [[UIView alloc] initWithFrame:CGRectMake(0, 560-140, 1003, 768-560+140)];
    summaryPanel.tag = 11827;
    summaryPanel.backgroundColor = TABLE_BG_COLOR_RED;
    [_inventoryView addSubview: summaryPanel];
    
    //Generate display for gender
    UILabel* genderHeader = [self createSumaryWhiteLabelWithOrigin:CGRectMake(offetX, offetY, firstColWidth, 24) alignRight:NO parent:summaryPanel];
    genderHeader.frame = CGRectMake(offetX, offetY, firstColWidth, cellHeight);
    genderHeader.text = [NSString stringWithFormat:@"%@:",[ShareLocale textFromKey:@"gender_group"]];
    
    genderHeader = [self createSumaryWhiteLabelWithOrigin:CGRectMake(offetX, offetY+cellHeight, firstColWidth, 24) alignRight:NO parent:summaryPanel];
    genderHeader.frame = CGRectMake(offetX, offetY+cellHeight,firstColWidth, cellHeight);
    genderHeader.text = [NSString stringWithFormat:@"%@:",[ShareLocale textFromKey:@"invdetail_qtyonhand"]];

    genderHeader = [self createSumaryWhiteLabelWithOrigin:CGRectMake(offetX, offetY+cellHeight*2, firstColWidth, 24) alignRight:NO parent:summaryPanel];
    genderHeader.frame = CGRectMake(offetX, offetY+cellHeight*2,firstColWidth, cellHeight);
    genderHeader.text = [NSString stringWithFormat:@"%@:",[ShareLocale textFromKey:@"invdetail_amountonhand"]];

    for(NSInteger index=0; index<[genderSummaryArray count]; index++){
        InvetorySummaryData* data = [genderSummaryArray objectAtIndex:index];
        UILabel* genderLbl = [self createSumaryWhiteLabelWithOrigin:CGRectMake(offetX + firstColWidth + cellWidth*index, offetY, cellWidth, 24) alignRight:NO parent:summaryPanel];
        genderLbl.text = data.name;
        genderLbl = [self createSumaryWhiteLabelWithOrigin:CGRectMake(offetX + firstColWidth + cellWidth*index, offetY+cellHeight, cellWidth, 24) alignRight:NO parent:summaryPanel];
        genderLbl.text = [NSString stringWithFormat:@"%d", data.quantityOnHand];
        genderLbl = [self createSumaryWhiteLabelWithOrigin:CGRectMake(offetX + firstColWidth + cellWidth*index, offetY+cellHeight*2, cellWidth, 24) alignRight:NO parent:summaryPanel];
        genderLbl.text = [NSString stringWithFormat:@"%.1fk", data.amountOnhand/1000.0];

    }
    


    offetY = 110;
    UILabel* catHeader = [self createSumaryWhiteLabelWithOrigin:CGRectMake(offetX, offetY, firstColWidth, 24) alignRight:NO parent:summaryPanel];
    catHeader.frame = CGRectMake(offetX, offetY, firstColWidth, cellHeight);
    catHeader.text = [NSString stringWithFormat:@"%@:",[ShareLocale textFromKey:@"category_group"]];
    
    catHeader = [self createSumaryWhiteLabelWithOrigin:CGRectMake(offetX, offetY+cellHeight, firstColWidth, 24) alignRight:NO parent:summaryPanel];
    catHeader.frame = CGRectMake(offetX, offetY+cellHeight,firstColWidth, cellHeight);
    catHeader.text = [NSString stringWithFormat:@"%@:",[ShareLocale textFromKey:@"invdetail_qtyonhand"]];
    
    catHeader = [self createSumaryWhiteLabelWithOrigin:CGRectMake(offetX, offetY+cellHeight*2, firstColWidth, 24) alignRight:NO parent:summaryPanel];
    catHeader.frame = CGRectMake(offetX, offetY+cellHeight*2,firstColWidth, cellHeight);
    catHeader.text = [NSString stringWithFormat:@"%@:",[ShareLocale textFromKey:@"invdetail_amountonhand"]];
    
    for(NSInteger index=0; index<[categorySummaryArray count]; index++){
        InvetorySummaryData* data = [categorySummaryArray objectAtIndex:index];
        UILabel* catLbl = [self createSumaryWhiteLabelWithOrigin:CGRectMake(offetX + firstColWidth + cellWidth*index, offetY, cellWidth, 24) alignRight:NO parent:summaryPanel];
        catLbl.text = data.name;
        catLbl = [self createSumaryWhiteLabelWithOrigin:CGRectMake(offetX + firstColWidth + cellWidth*index, offetY+cellHeight, cellWidth, 24) alignRight:NO parent:summaryPanel];
        catLbl.text = [NSString stringWithFormat:@"%d", data.quantityOnHand];
        catLbl = [self createSumaryWhiteLabelWithOrigin:CGRectMake(offetX + firstColWidth + cellWidth*index, offetY+cellHeight*2, cellWidth, 24) alignRight:NO parent:summaryPanel];
        catLbl.text = [NSString stringWithFormat:@"%.1fk", data.amountOnhand/1000.0];
        
    }
}



-(UILabel*) createSumaryWhiteLabelWithOrigin:(CGRect)rect alignRight:(BOOL)alignRight parent:(UIView*)parentView{
    
    UILabel* lbl = [[UILabel alloc] initWithFrame:CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height)];
    lbl.font = [UIFont fontWithName:@"Arial" size:14];
    lbl.textColor = [UIColor whiteColor];
    lbl.textAlignment = alignRight? NSTextAlignmentRight:NSTextAlignmentLeft;
    [parentView addSubview:lbl];
    return lbl;
}



-(void) resetActiveTabPage:(UIView*)view{
    if([_topSalesView superview]!=nil){
        [_topSalesView removeFromSuperview];
    }
    if([_lowStockView superview]!=nil){
        [_lowStockView removeFromSuperview];
    }
    if([_inventoryView superview]!=nil){
        [_inventoryView removeFromSuperview];
    }
    if([_marketingView superview]!=nil){
        [_marketingView removeFromSuperview];
    }
    [self.containerView addSubview: view];
}




- (IBAction)onTopSalesButtonClicked:(id)sender {
    
    _inventoryButton.backgroundColor = [UIColor blackColor];
    _topSalesbutton.backgroundColor = [UIColor colorWithWhite:55/255.0f alpha:1];
    _lowStockButton.backgroundColor = [UIColor blackColor];
    _marketingButton.backgroundColor = [UIColor blackColor];

    [self resetActiveTabPage:_topSalesView];
    
    for (UIView* v in [_topSalesView subviews]){
        if([v isKindOfClass:[UILabel class]] || [v isKindOfClass:[UITextView class]] )
            [v removeFromSuperview];
    }
    int offsetX = 20;
    int offsetY = 100;
    int index = 0;
    int lineHeight = 22;
    for(NSDictionary* data in self.topSalesRecords){
        UITextView* skuLbl = [[UITextView alloc] initWithFrame:CGRectMake(offsetX, offsetY + index * lineHeight, 180, lineHeight)];
        skuLbl.editable = NO;
        skuLbl.font = [UIFont fontWithName:@"Arial" size:14];
        skuLbl.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        skuLbl.scrollEnabled = NO;
        skuLbl.textColor = [UIColor whiteColor];
        skuLbl.text = [data objectForKey:@"Generic_Article__c"];
        UITextView* nameLbl = [[UITextView alloc] initWithFrame:CGRectMake(offsetX + 168, offsetY + index * lineHeight, 220, lineHeight)];
        nameLbl.font = [UIFont fontWithName:@"Arial" size:14];
        nameLbl.editable = NO;
        nameLbl.scrollEnabled = NO;
        nameLbl.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        nameLbl.textColor = [UIColor whiteColor];
        nameLbl.text = [data objectForKey:@"Item_Description__c"];
        
        
        UILabel* opsSalesLbl = [[UILabel alloc] initWithFrame:CGRectMake(offsetX + 430, offsetY + index * lineHeight, 80, lineHeight)];
        opsSalesLbl.font = [UIFont fontWithName:@"Arial" size:14];
        opsSalesLbl.textAlignment = NSTextAlignmentRight;
        opsSalesLbl.textColor = [UIColor whiteColor];
        double v = [[data objectForKey:@"OPS_Sales_Amount__c"] doubleValue];
        if(v>1000){
            opsSalesLbl.text = [NSString stringWithFormat:@"%.1fk",v/1000];
        }else{
            opsSalesLbl.text = [NSString stringWithFormat:@"%.1f",v];
        }

        
        UILabel* weekSalesLbl = [[UILabel alloc] initWithFrame:CGRectMake(offsetX + 580, offsetY + index * lineHeight, 40, lineHeight)];
        weekSalesLbl.font = [UIFont fontWithName:@"Arial" size:14];
        weekSalesLbl.textAlignment = NSTextAlignmentRight;
        weekSalesLbl.textColor = [UIColor whiteColor];
        weekSalesLbl.text = [NSString stringWithFormat:@"%@",[data objectForKey:@"Sales_Quantity__c"]];
        
        UILabel* onHandInvLbl = [[UILabel alloc] initWithFrame:CGRectMake(offsetX + 740, offsetY + index * lineHeight, 40, lineHeight)];
        onHandInvLbl.font = [UIFont fontWithName:@"Arial" size:14];
        onHandInvLbl.textColor = [UIColor whiteColor];
        id val = [data objectForKey:@"Stock_On_Hand__c"];
        if(val==nil || val == [NSNull null]) val = @"-";
        onHandInvLbl.text = [NSString stringWithFormat:@"%@", val];
        onHandInvLbl.textAlignment = NSTextAlignmentRight;
        
        
        [_topSalesView addSubview: skuLbl];
        [_topSalesView addSubview: nameLbl];
        [_topSalesView addSubview: opsSalesLbl];
        [_topSalesView addSubview: weekSalesLbl];
        [_topSalesView addSubview: onHandInvLbl];
        
        index++;
        if(index>=20) break;
    }
}

- (IBAction)onHomeButtonClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)onBackButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


-(void) loadData{
    [self showLoadingPopup:self.view];
    [self loadInventory];
}

-(void) loadInventory{//Inventory tab, Fixed with latest cut to date
    VFDAO* vf = [VFDAO sharedInstance];
    self.inventoryRequest = [vf selectInventoryWithDelegate:self storeCode: [self.data objectForKey:@"Store_Code__c"] ];
}


-(void) loadStockAndSales{//General request for Top/Low/Core tab, Fixed with specific cut to date.  Should change logic
    VFDAO* vf = [VFDAO sharedInstance];
    self.stockSalesRequest = [vf selectStockAndSalesWithDelegate:self storeCode:[self.data objectForKey:@"Store_Code__c"] year: [self.fxyear intValue] week:[self.fxweek intValue]-1 ];
}


-(void) loadSalesMix{//Marketing tab,
    VFDAO* vf = [VFDAO sharedInstance];
    self.salesMixRequest = [vf selectSalesMixWithDelegate:self storeCode:[self.data objectForKey:@"Store_Code__c"] year: [self.fxyear intValue] week:[self.fxweek intValue]-1 ];
}


-(void) loadStockOnHand{//Marketing tab,
    VFDAO* vf = [VFDAO sharedInstance];
    self.stockOnHandRequest = [vf selectStockOnHandWithDelegate:self storeCode:[self.data objectForKey:@"Store_Code__c"] date: self.marketingCutToDate ];
}



-(void)daoRequest:(DAOBaseRequest*)request queryOnlineDataSuccess:(NSArray*) response{
    
    if(self.inventoryRequest == request){
        self.inventoryRecords = response;
        
        //filter out records that not at final date
        NSMutableArray* filteredRecords = [NSMutableArray array];
        if(self.inventoryRecords!=nil && [self.inventoryRecords count]>0){
            NSDictionary* latestData = [self.inventoryRecords objectAtIndex:0];
            
            NSString* cutDate = [latestData objectForKey:@"Cut_To_Day__c"];
            
            UILabel* label = (UILabel*)[self.inventoryView viewWithTag:301];
            label.text = [NSString stringWithFormat: [ShareLocale textFromKey:@"cut_to_date"], cutDate];
            
            for(NSDictionary* d in self.inventoryRecords){
                if( [ [d objectForKey:@"Cut_To_Day__c"] isEqualToString: cutDate] ){
                    [filteredRecords addObject:d];
                }
            }
        }else{
            UILabel* label = (UILabel*)[self.inventoryView viewWithTag:301];
            label.text = [NSString stringWithFormat: [ShareLocale textFromKey:@"cut_to_date"], @"N/A"];

        }
        
        self.inventoryRecords = filteredRecords;
        
        //sort by quantity on hand
        self.inventoryRecords = [self.inventoryRecords sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            NSDictionary* val1 = (NSDictionary*)obj1;
            NSDictionary* val2 = (NSDictionary*)obj2;
            NSString* stock1 = [val1 objectForKey:@"Category__c"];
            NSString* stock2 = [val2 objectForKey:@"Category__c"];
            return -[stock1 compare:stock2 ];

        }];
        [self loadStockAndSales];
    }
    else if(self.stockSalesRequest == request){
        NSMutableArray* salesDatas = [[NSMutableArray alloc] init];
        NSMutableArray* stockDatas = [[NSMutableArray alloc] init];
        NSMutableArray* marketingDatas = [[NSMutableArray alloc] init];
        
        NSString* topSalescutToDate= nil;
        NSString* marketingCutToDate= nil;
        
        for(NSDictionary* data in response){
            
            NSString* type = [data objectForKey:@"Type__c"];
            if([type isEqualToString:@"T"]){
                if(topSalescutToDate==nil) topSalescutToDate = [data objectForKey:@"Cut_To_Day__c"];
                [salesDatas addObject: data];
            }
            else if ([type isEqualToString:@"L"]){
                [stockDatas addObject: data];
            }
            else if ([type isEqualToString:@"C"]){
                if(marketingCutToDate==nil) marketingCutToDate = [data objectForKey:@"Cut_To_Day__c"];
                [marketingDatas addObject:data];
            }
        }
        self.topSalesRecords = [NSArray arrayWithArray: salesDatas];
        self.lowStockRecords = [NSArray arrayWithArray: stockDatas];
        self.marketingRecords = [NSArray arrayWithArray: marketingDatas];

        if(topSalescutToDate==nil) {
            topSalescutToDate = @"N/A";
        }
        
        UILabel* topSalesLabel = (UILabel*)[self.topSalesView viewWithTag:301];
        topSalesLabel.text = [NSString stringWithFormat: [ShareLocale textFromKey:@"cut_to_date"], topSalescutToDate];
        
        if(marketingCutToDate==nil) {
            marketingCutToDate = @"N/A";
        }
        else{
            self.marketingCutToDate = marketingCutToDate;
        }
        UILabel* marketingLlabel = (UILabel*)[self.marketingView viewWithTag:301];
        marketingLlabel.text = [NSString stringWithFormat: [ShareLocale textFromKey:@"cut_to_date"], marketingCutToDate];
        
        //sorting top sales
        self.topSalesRecords = [self.topSalesRecords sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            NSDictionary* val1 = (NSDictionary*)obj1;
            NSDictionary* val2 = (NSDictionary*)obj2;
            id stock1 = [val1 objectForKey:@"Sales_Quantity__c"];
            id stock2 = [val2 objectForKey:@"Sales_Quantity__c"];
            if([stock1 intValue] > [stock2 intValue]){//larger show first
                return -1;
            }else if ([stock1 intValue] < [stock2 intValue]){
                return 1;
            }else{
                id q1 = [val1 objectForKey:@"Stock_On_Hand__c"];
                id q2 = [val2 objectForKey:@"Stock_On_Hand__c"];
                if(q1==nil || q1==[NSNull null]){
                    q1 = [NSNumber numberWithInt:-1];
                }
                if(q2==nil || q2==[NSNull null]){
                    q2 = [NSNumber numberWithInt:-1];
                }
                if([q1 intValue] > [q2 intValue]){//smaller show first
                    return 1;
                }else if ([q1 intValue] < [q2 intValue]){
                    return -1;
                }else{
                    return 0;
                }
            }
        }];
        //sorting low stock
        self.lowStockRecords = [self.lowStockRecords sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            NSDictionary* val1 = (NSDictionary*)obj1;
            NSDictionary* val2 = (NSDictionary*)obj2;
            id stock1 = [val1 objectForKey:@"Stock_On_Hand__c"];
            id stock2 = [val2 objectForKey:@"Stock_On_Hand__c"];
            if([stock1 intValue] > [stock2 intValue]){//smaller show first
                return 1;
            }else if ([stock1 intValue] < [stock2 intValue]){
                return -1;
            }else{
                id q1 = [val1 objectForKey:@"Sales_Quantity__c"];
                id q2 = [val2 objectForKey:@"Sales_Quantity__c"];
                if(q1==nil || q1==[NSNull null]){
                    q1 = [NSNumber numberWithInt:-1];
                }
                if(q2==nil || q2==[NSNull null]){
                    q2 = [NSNumber numberWithInt:-1];
                }
                if([q1 intValue] > [q2 intValue]){//larger show first
                    return -1;
                }else if ([q1 intValue] < [q2 intValue]){
                    return 1;
                }else{
                    return 0;
                }
            }
        }];
        
        [salesDatas release];
        [stockDatas release];
        [marketingDatas release];
        
        [self loadSalesMix];
        
        
    }
    else if(self.salesMixRequest == request){
        
        self.salesMixRecords = response;
        [self loadStockOnHand];
    }
    else if (self.stockOnHandRequest == request){
        self.stockOnHandRecords = response;
        [self hideLoadingPopupWithFadeout];
        [self onTopSalesButtonClicked:nil];
        
    }
}




-(void)daoRequest:(DAOBaseRequest*)request queryOnlineDataError:(NSString*) response code:(int)code{
    [self hideLoadingPopupWithFadeout];
    NSLog(@"response = %@", response);
    
    
}




@end
