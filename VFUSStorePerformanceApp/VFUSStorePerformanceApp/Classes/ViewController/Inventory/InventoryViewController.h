//
//  InventoryViewController.h
//  VFStorePerformanceApp_Billy
//
//  Created by Developer 2 on 11/4/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"

@interface InventoryViewController : BasicViewController<DAODelegate>
@property (retain, nonatomic) IBOutlet UIButton *lowStockButton;
@property (retain, nonatomic) IBOutlet UIView *topSalesView;
@property (retain, nonatomic) IBOutlet UIView *marketingView;
@property (retain, nonatomic) IBOutlet UIView *lowStockView;
@property (retain, nonatomic) IBOutlet UIView *inventoryView;
@property (retain, nonatomic) IBOutlet UIButton *inventoryButton;

@property (retain, nonatomic) IBOutlet UIButton *topSalesbutton;
@property (retain, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (retain, nonatomic) IBOutlet UIButton *marketingButton;

@property (retain, nonatomic) NSDictionary* data;

@property (retain, nonatomic) NSArray* lowStockRecords;
@property (retain, nonatomic) NSArray* marketingRecords;
@property (retain, nonatomic) NSArray* topSalesRecords;
@property (retain, nonatomic) NSArray* inventoryRecords;
@property (retain, nonatomic) NSArray* salesMixRecords;
@property (retain, nonatomic) NSArray* stockOnHandRecords;

@property (retain, nonatomic) DAOBaseRequest* inventoryRequest;
@property (retain, nonatomic) DAOBaseRequest* stockSalesRequest;
@property (retain, nonatomic) DAOBaseRequest* salesMixRequest;
@property (retain, nonatomic) DAOBaseRequest* stockOnHandRequest;

@property (retain, nonatomic) IBOutlet UIView *containerView;

//top sales
@property (retain, nonatomic) IBOutlet UILabel *skuLabel;
@property (retain, nonatomic) IBOutlet UILabel *weekOpsSalesLabel;
@property (retain, nonatomic) IBOutlet UILabel *weekSalesQtyLabel;
@property (retain, nonatomic) IBOutlet UILabel *onHandInvLabel;
@property (retain, nonatomic) IBOutlet UILabel *cutToDateLabel;
@property (retain, nonatomic) IBOutlet UILabel *productName;
//inventory
@property (retain, nonatomic) IBOutlet UILabel *genderLabel;
@property (retain, nonatomic) IBOutlet UILabel *categoryLabel;
@property (retain, nonatomic) IBOutlet UILabel *inventoryClassLabel;
@property (retain, nonatomic) IBOutlet UILabel *qtyOnhandLabel;
@property (retain, nonatomic) IBOutlet UILabel *amountOnhandLabel;
@property (retain, nonatomic) IBOutlet UILabel *invCapacityLabel;
//marketing
@property (retain, nonatomic) IBOutlet UILabel *articleLabel;
@property (retain, nonatomic) IBOutlet UILabel *descLabel;
@property (retain, nonatomic) IBOutlet UILabel *salesQtyLabel;
@property (retain, nonatomic) IBOutlet UILabel *totalAmtOnHandLabel;
@property (retain, nonatomic) IBOutlet UILabel *totalQtyOnHandLabel;
@property (retain, nonatomic) IBOutlet UILabel *inventorySalesLabel;
@property (retain, nonatomic) IBOutlet UILabel *opsLabel;
@property (retain, nonatomic) IBOutlet UILabel *totalOpsLabel;
@property (retain, nonatomic) IBOutlet UILabel *opsPctLabel;



@property (retain, nonatomic) NSString* marketingCutToDate;
@property (retain, nonatomic) NSString* fxdate;
@property (retain, nonatomic) NSNumber* fxweek;
@property (retain, nonatomic) NSNumber* fxmonth;
@property (retain, nonatomic) NSNumber* fxyear;

- (IBAction)onLowStockButtonClicked:(id)sender;
- (IBAction)onInventoryClicked:(id)sender;

- (IBAction)onTopSalesButtonClicked:(id)sender;

- (IBAction)onMarketingButtonClicked:(id)sender;

- (IBAction)onHomeButtonClicked:(id)sender;

- (IBAction)onBackButtonClicked:(id)sender;

-(id)initWithDictionary:(NSDictionary*)data;

@end
