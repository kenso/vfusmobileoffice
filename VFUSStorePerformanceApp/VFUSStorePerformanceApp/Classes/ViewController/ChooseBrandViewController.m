//
//  ChooseBrandViewController.m
//
//  Created by Developer 2 on 4/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import "ChooseBrandViewController.h"
#import "OrderCreationManager.h"
#import "OrderFormViewController.h"
#import "UIHelper.h"
#import "Constants.h"

@interface ChooseBrandViewController ()

@end

@implementation ChooseBrandViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [UIHelper applyPurpleSmallButtonImageOnButton:self.backButton];
    [UIHelper applyBlueSmallButtonImageOnButton:self.reloadButton];

    
    self.brandTableView.backgroundColor = TABLE_BG_COLOR_RED;
    self.brandTableView.backgroundView = nil;
    self.recordAry = [NSArray array];
    self.brandTableView.delegate = self;
    self.brandTableView.dataSource = self;
    [self loadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_brandTableView release];
    [_backButton release];
    [_reloadButton release];
    [super dealloc];
}

#pragma mark - Data Handling
- (void) loadData{
    [self showLoadingPopup: self.view];
    SFRestRequest *request = [[SFRestAPI sharedInstance] requestForQuery:  @"SELECT Id, Name, Accu_Amount_Line__c, Accu_Discount_Percentage__c, OnTop_Amount_Line__c, OnTop_Discount_Percentage__c, Brand_Code__c, Description__c, Approval_Criticial_Amount__c, Approval_Critical_Discount__c FROM emfa__Product_Brand__c  ORDER BY Name asc"];
    [[SFRestAPI sharedInstance] send:request delegate:self];
    
}


- (void)request:(SFRestRequest *)request didLoadResponse:(id)jsonResponse{
    [self hideLoadingPopupWithFadeout];
    NSLog(@"ChooseBrandViewController::didLoadResponse() jsonResponse = %@", jsonResponse);
    id done = [jsonResponse objectForKey:@"done"];
    if(done!=nil && [done intValue]==1){
        //get array of records with key "records"
        //get number of record from "totalSize"
        NSArray* records = [jsonResponse objectForKey:@"records"];
        self.recordAry = records;
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [self.brandTableView reloadData];
        });
    }else{
        
    }
}

- (void)request:(SFRestRequest *)request didFailLoadWithError:(NSError*)error{
    [self hideLoadingPopupWithFadeout];
    [self popupErrorDialogWithMessage: [NSString stringWithFormat:@"Error: %@", error]];
}

- (void)requestDidCancelLoad:(SFRestRequest *)request{
    [self hideLoadingPopupWithFadeout];
    [self popupErrorDialogWithMessage: @"Request is cancelled"];
}

- (void)requestDidTimeout:(SFRestRequest *)request{
    [self hideLoadingPopupWithFadeout];
    [self popupErrorDialogWithMessage: @"Loading time out"];
}





#pragma mark - Table Data source handling
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.recordAry count];
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
    if(cell == nil){
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyIdentifier"] autorelease];
        cell.textLabel.hidden = YES;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(10, 0, 188, 40) parent:cell tag: 901];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(195, 0, 699, 40) parent:cell tag: 902];
        [self createNewButtonWithDefaultStyle: CGRectMake(887, 4, 62, 38) parent:cell tag:906];

    }
    
    cell.tag = indexPath.row;
    
    NSDictionary* data = [self.recordAry objectAtIndex: indexPath.row];
    
    UITextView* orderIdTxtView = (UITextView*)[cell viewWithTag:901];
    orderIdTxtView.text = [data objectForKey:@"Name"];
    UITextView* statusTxtView = (UITextView*)[cell viewWithTag:902];
    statusTxtView.text = [data objectForKey:@"Brand_Code__c"];
    
    UIButton* selectButton = (UIButton*)[cell viewWithTag:906];
    [selectButton setTitle:@"Select" forState:UIControlStateNormal];
    [selectButton setTitle:@"Select" forState:UIControlStateSelected];
    [selectButton setTitle:@"Select" forState:UIControlStateDisabled];
    [selectButton addTarget:self action:@selector(onSelectedBrand:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}


-(void) onSelectedBrand:(id)sender{
    UIView* view = sender;
    int tagVal = view.superview.superview.tag;
    OrderCreationManager* mgr = [OrderCreationManager shareInstance];
    
    NSDictionary* data = [self.recordAry objectAtIndex:tagVal];
    
    mgr.brandId = [data objectForKey:@"Id"];
    mgr.brandName = [data objectForKey:@"Name"];
    mgr.brandAccuAmountLine = [[self getFieldDisplayWithFieldName:@"Accu_Amount_Line__c" withDictionary:data  ] floatValue];
    mgr.brandOnTopAmountLine = [[self getFieldDisplayWithFieldName:@"OnTop_Amount_Line__c" withDictionary:data] floatValue];
    mgr.brandOnTopDiscountPCT = [[self getFieldDisplayWithFieldName:@"OnTop_Discount_Percentage__c" withDictionary:data] floatValue];
    mgr.brandApprovalCriticalAmount = [[self getFieldDisplayWithFieldName:@"Approval_Criticial_Amount__c" withDictionary:data  ] floatValue];
    mgr.brandApprovalCriticalDiscountPCT = [[self getFieldDisplayWithFieldName:@"Approval_Critical_Discount__c" withDictionary:data  ] floatValue];
    OrderFormViewController* vc = [[[OrderFormViewController alloc] init] autorelease];
    [self.navigationController pushViewController: vc animated:YES];
}




#pragma mark - Handle Click event of Back button & Reload button
- (IBAction)onBackButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onReloadButtonClicked:(id)sender {
    [self loadData];
}

@end
