//
//  HelpViewController.m
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 12/6/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//
#import "ShareLocale.h"
#import "HelpViewController.h"

@interface HelpViewController ()

@end

@implementation HelpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.titleLabel.text = [ShareLocale textFromKey:@"helppage_title"];
    
    self.headerKpi.text = [ShareLocale textFromKey:@"header_kpi"];
    self.headerDefine.text = [ShareLocale textFromKey:@"header_define"];
    self.headerSource.text = [ShareLocale textFromKey:@"header_source"];
    
    self.kpiOpsSalesLabel.text = [ShareLocale textFromKey:@"kpi_ops_sales"];
    self.kpiConvRateLabel.text = [ShareLocale textFromKey:@"kpi_conv_rate"];
    self.kpiAtvLabel.text = [ShareLocale textFromKey:@"kpi_atv"];
    self.kpiUptLabel.text = [ShareLocale textFromKey:@"kpi_upt"];
    self.kpiProductivityLabel.text = [ShareLocale textFromKey:@"kpi_prod"];
    self.kpiDiscountlabel.text = [ShareLocale textFromKey:@"kpi_discount"];
    self.kpiStoreLabel.text = [ShareLocale textFromKey:@"kpi_store"];
    self.kpiCompOpsSales.text = [ShareLocale textFromKey:@"kpi_comp_ops_sales"];
    self.kpiTxnLabel.text = [ShareLocale textFromKey:@"kpi_txn"];
    self.kpiVisitorLabel.text = [ShareLocale textFromKey:@"kpi_visitor"];
    
    self.defOpsSales.text = [ShareLocale textFromKey:@"def_kpi_ops_sales"];
    self.defDiscount.text = [ShareLocale textFromKey:@"def_kpi_discount"];
    self.defStore.text = [ShareLocale textFromKey:@"def_kpi_store"];
    self.defCompOpsSales.text = [ShareLocale textFromKey:@"def_kpi_comp_ops_sales"];
    self.defVisitor.text = [ShareLocale textFromKey:@"def_kpi_visitor"];
    self.defTxn.text = [ShareLocale textFromKey:@"def_kpi_txn"];
    self.defConvRate.text = [ShareLocale textFromKey:@"def_kpi_conv_rate"];
    self.defAtv.text = [ShareLocale textFromKey:@"def_kpi_atv"];
    self.defUpt.text = [ShareLocale textFromKey:@"def_kpi_upt"];
    self.defProductivity.text = [ShareLocale textFromKey:@"def_kpi_prod"];
    
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self refreshBackground];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)onHomeButtonClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)onBackButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)dealloc {
    [_titleLabel release];
    [_headerKpi release];
    [_headerDefine release];
    [_headerSource release];
    [_kpiOpsSalesLabel release];
    [_kpiDiscountlabel release];
    [_kpiStoreLabel release];
    [_kpiCompOpsSales release];
    [_kpiTxnLabel release];
    [_kpiVisitorLabel release];
    [_kpiConvRateLabel release];
    [_kpiAtvLabel release];
    [_kpiUptLabel release];
    [_kpiProductivityLabel release];
    [_defOpsSales release];
    [_defDiscount release];
    [_defStore release];
    [_defCompOpsSales release];
    [_defVisitor release];
    [_defTxn release];
    [_defConvRate release];
    [_defAtv release];
    [_defUpt release];
    [_defProductivity release];
    [super dealloc];
}
@end
