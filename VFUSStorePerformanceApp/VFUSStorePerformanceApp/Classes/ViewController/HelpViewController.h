//
//  HelpViewController.h
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 12/6/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"


@interface HelpViewController : BasicViewController


@property (retain, nonatomic) IBOutlet UILabel *titleLabel;



@property (retain, nonatomic) IBOutlet UILabel *headerKpi;
@property (retain, nonatomic) IBOutlet UILabel *headerDefine;
@property (retain, nonatomic) IBOutlet UILabel *headerSource;

@property (retain, nonatomic) IBOutlet UILabel *kpiOpsSalesLabel;
@property (retain, nonatomic) IBOutlet UILabel *kpiConvRateLabel;
@property (retain, nonatomic) IBOutlet UILabel *kpiAtvLabel;
@property (retain, nonatomic) IBOutlet UILabel *kpiUptLabel;
@property (retain, nonatomic) IBOutlet UILabel *kpiProductivityLabel;
@property (retain, nonatomic) IBOutlet UILabel *kpiDiscountlabel;
@property (retain, nonatomic) IBOutlet UILabel *kpiStoreLabel;
@property (retain, nonatomic) IBOutlet UILabel *kpiCompOpsSales;
@property (retain, nonatomic) IBOutlet UILabel *kpiTxnLabel;
@property (retain, nonatomic) IBOutlet UILabel *kpiVisitorLabel;

@property (retain, nonatomic) IBOutlet UITextView *defOpsSales;
@property (retain, nonatomic) IBOutlet UITextView *defDiscount;
@property (retain, nonatomic) IBOutlet UITextView *defStore;
@property (retain, nonatomic) IBOutlet UITextView *defCompOpsSales;
@property (retain, nonatomic) IBOutlet UITextView *defVisitor;
@property (retain, nonatomic) IBOutlet UITextView *defTxn;
@property (retain, nonatomic) IBOutlet UITextView *defConvRate;
@property (retain, nonatomic) IBOutlet UITextView *defAtv;

@property (retain, nonatomic) IBOutlet UITextView *defUpt;
@property (retain, nonatomic) IBOutlet UITextView *defProductivity;





- (IBAction)onHomeButtonClicked:(id)sender;

- (IBAction)onBackButtonClicked:(id)sender;



@end
