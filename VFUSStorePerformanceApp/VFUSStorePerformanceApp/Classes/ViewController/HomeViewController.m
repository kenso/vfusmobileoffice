
/*
 Copyright (c) 2011, salesforce.com, inc. All rights reserved.
 
 Redistribution and use of this software in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this list of conditions
 and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list of
 conditions and the following disclaimer in the documentation and/or other materials provided
 with the distribution.
 * Neither the name of salesforce.com, inc. nor the names of its contributors may be used to
 endorse or promote products derived from this software without specific prior written
 permission of salesforce.com, inc.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#import "SurveyListViewController.h"
#import "SettingViewController.h"
#import "ShareLocale.h"
#import "OrderCreationManager.h"
#import "HelpViewController.h"
#import "GeneralSelectionPopup.h"
#import "HomeViewController.h"
#import "ChooseStoreViewController.h"
#import "UIHelper.h"
#import "GeneralUiTool.h"
#import "CustomWebserviceRestApi.h"
#import "VisitationViewController.h"
#import "UsageReportViewController.h"
#import "NewsViewController.h"
#import "ProductCatalogBrandViewController.h"
#import "NewsDetailViewController.h"
#import "BusinessLogicHelper.h"
#import "OfflineSyncHelper.h"
#import "VFDAO.h"
#import "Constants.h"
#import "OfflinePromptPopupView.h"
#import "SFRestRequest.h"
#import "StoreReportVC.h"
#import "GeneralUiTool.h"
#import "DataHelper.h"

@implementation HomeViewController



#pragma mark Misc

static UIViewController* nextViewController;

+(void) setRedirectViewController:(UIViewController*)vc{
    nextViewController = vc;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{

    [_storeListButton release];
    [_trainingButton release];
    [_visitPlanButton release];
    [_syncButton release];
    [_settingButton release];
    [_opsCard release];
    [_discountCard release];
    [_atvCard release];
    [_uptCard release];
    [_conversionCard release];
    [_salesProductivityCard release];
    [_visitorCard release];
    [_transactionCard release];
    [_peelOffCard release];
    [_broadNoticeView release];
    [_auditButton release];
    [_staffButton release];
    [_customerButton release];
    [_usageButton release];
    [_inventoryButton release];
    [_selectButton release];
    [_cardOpsSales release];
    [_cardDiscount release];
    [_cardProductivity release];
    [_cardAtv release];
    [_cardUpt release];
    [_cardTxn release];
    [_cardVisitor release];
    [_cardPeeloff release];
    [_cardConvRate release];
    [_globalButton release];
    [_offllineDownloadPopup release];
    [_compStatusButton release];
    [super dealloc];
}


#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    [self initMarqueeView];
    [self initCommentSidebarWithStoreId: nil];
    self.selectedFilterIndex = 0;
    
    [self.compStatusButton setTitle:[ShareLocale textFromKey:@"all"] forState:UIControlStateNormal];

    [self loadBrandList];
    
    
    

}




-(void)refreshLocale{
    
    [self.storeListButton setTitle:[ShareLocale textFromKey:@"menu_store_perf"] forState:UIControlStateNormal];
    [self.inventoryButton setTitle:[ShareLocale textFromKey:@"menu_inventory"] forState:UIControlStateNormal];
    [self.visitPlanButton setTitle:[ShareLocale textFromKey:@"menu_visit"]  forState:UIControlStateNormal];
    [self.trainingButton setTitle:[ShareLocale textFromKey:@"menu_knowledge"] forState:UIControlStateNormal];
    [self.customerButton setTitle:[ShareLocale textFromKey:@"menu_customer"] forState:UIControlStateNormal];
    [self.staffButton setTitle:[ShareLocale textFromKey:@"menu_staff"] forState:UIControlStateNormal];
    [self.auditButton setTitle:[ShareLocale textFromKey:@"menu_audit_result"] forState:UIControlStateNormal];
    [self.usageButton setTitle:[ShareLocale textFromKey:@"menu_usage"] forState:UIControlStateNormal];
    UILabel* dashboardFilterLabel = (UILabel*)[self.view viewWithTag:276];
    
    
    if([dashboardFilterLabel.text isEqualToString:@""]){
        dashboardFilterLabel.text = [ShareLocale textFromKey:@"txt_loading_dashboard"];
    }else{
        if(self.selectedFilterIndex>=0 && self.selectedFilterIndex < [self.filterArray count] ){
            NSDictionary* selecedFilterItem = [self.filterArray objectAtIndex:self.selectedFilterIndex];
            NSString* dateStr = [selecedFilterItem objectForKey:@"date"];
            UILabel* dashboardFilterLabel = (UILabel*)[self.view viewWithTag:276];
            dashboardFilterLabel.text = [NSString stringWithFormat: [ShareLocale textFromKey:@"lbl_dashboard_at"] , dateStr];
        }
    }
    
    [self.selectButton setTitle:[ShareLocale textFromKey:@"btn_select"] forState:UIControlStateNormal];
    
    
    self.cardOpsSales.text = [@"  " stringByAppendingString:[ShareLocale textFromKey:@"ops_sales"]];
    self.cardDiscount.text = [@"  " stringByAppendingString:[ShareLocale textFromKey:@"discount"] ];
    self.cardPeeloff.text = [@"  " stringByAppendingString:[ShareLocale textFromKey:@"peel_off"]];
    self.cardProductivity.text = [@"  " stringByAppendingString:[ShareLocale textFromKey:@"store_prod"]];
    self.cardConvRate.text = [@"  " stringByAppendingString:[ShareLocale textFromKey:@"conversion_rate"]];
    self.cardAtv.text = [@"  " stringByAppendingString:[ShareLocale textFromKey:@"atv"]];
    self.cardUpt.text = [@"  " stringByAppendingString:[ShareLocale textFromKey:@"upt"]];
    self.cardTxn.text = [@"  " stringByAppendingString:[ShareLocale textFromKey:@"transaction_count"]];
    self.cardVisitor.text = [@"  " stringByAppendingString:[ShareLocale textFromKey:@"visitor_count"]];
}

-(void) initMarqueeView{
    self.messageRecordArray = [[[NSMutableArray alloc] init] autorelease];
    UIView* broadNoticeView = self.broadNoticeView;
    self.marqueeView = [[[MarqueeView alloc] initWithFrame:CGRectMake(0, 0, broadNoticeView.frame.size.width, broadNoticeView.frame.size.height)] autorelease];
    [self.broadNoticeView addSubview:self.marqueeView];
    
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
    button.frame = self.marqueeView.frame;
    [button setTitle:@"" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(onInboxButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.broadNoticeView addSubview: button];

    self.marqueeView.delegate = self;
}



-(void) applyStartupAnimationToView:(UIView*)view delay:(float)time{
    view.alpha = 0;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelay:time];
    view.alpha = 1;
    
    [UIView commitAnimations];
    
    [self registerCardGestureHandler: view];
}

-(void) registerCardGestureHandler:(UIView*)view{
    UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                        action:@selector(onClickDashboardCard:)];
    [view addGestureRecognizer:singleFingerTap];
    [singleFingerTap release];
}


-(void)onClickDashboardCard:(UITapGestureRecognizer*)sender{
    NSLog(@"onClickDashboardCard %d", sender.view.tag);
    
//    if(sender.view.tag != CARD_TRANSACTION_COUNT && sender.view.tag != CARD_OPS_SALES) return;
    
    UIViewController* vc = [[ChooseStoreViewController alloc] initWithMode:sender.view.tag];
    [self.navigationController pushViewController:vc animated:YES];
    [vc release];
    
    

}


-(void) fillCard:(UIView*)cardView tagOffset:(int)os reportDictionary:(NSDictionary*)dict currentKey:(NSString*)newKey passKey:(NSString*) oldKey format:(NSString*)format target:(float)target scale:(float)scale {
    [self fillCard:cardView tagOffset:os reportDictionary:dict currentKey:newKey passKey:oldKey format:format target:target scale:scale nagativeComparing:NO];
}

-(void) fillCard:(UIView*)cardView tagOffset:(int)os reportDictionary:(NSDictionary*)dict currentKey:(NSString*)newKey passKey:(NSString*) oldKey format:(NSString*)format target:(float)target scale:(float)scale nagativeComparing:(BOOL)flag{
    
    float tywtdV = [[dict objectForKey:newKey] floatValue];
    float lywtdV = [[dict objectForKey:oldKey] floatValue];
    float pct = 100 * (tywtdV - lywtdV) / lywtdV;
    
    if(format!=nil){
        ((UILabel*)[cardView viewWithTag:10+os]).text = [NSString stringWithFormat:format, tywtdV*scale];
    }
    else{
        ((UILabel*)[cardView viewWithTag:10+os]).text = [BusinessLogicHelper convertValueToLimitedCharacterString: tywtdV];
    }
    UIImageView* arrow = ((UIImageView*)[cardView viewWithTag:20+os]);
    
    
    UIImage* goodImg = !flag?[UIImage imageNamed:@"green_up_arrow.png"]:[UIImage imageNamed:@"red_up_arrow.png"];
    UIImage* badImg = !flag?[UIImage imageNamed:@"red_down_arrow.png"]:[UIImage imageNamed:@"green_down_arrow.png"];
    
    if(lywtdV==0){
        arrow.hidden = YES;
    }else{
        arrow.hidden = NO;
        if(pct>0){
            arrow.image = goodImg;
        }
        else{
            arrow.image = badImg;
        }
    }
    
    if(lywtdV!=0) {
        if(pct<100){
            ((UILabel*)[cardView viewWithTag:30+os]).text = [NSString stringWithFormat:@"%.1f%%", pct ];
        }else{
            ((UILabel*)[cardView viewWithTag:30+os]).text = [NSString stringWithFormat:@"%.0f%%", pct ];
        }
    }
    else {
        ((UILabel*)[cardView viewWithTag:30+os]).text = @"-";
    }
    ((UILabel*)[cardView viewWithTag:40+os]).text = @"-";
    UIView* opsSalesTImg = ((UIImageView*)[cardView viewWithTag:50+os]);
    ((UILabel*)[cardView viewWithTag:40+os]).hidden = NO;
    [cardView viewWithTag:80+os].hidden = NO;

    if(target==0){
        ((UILabel*)[cardView viewWithTag:40+os]).text = @"-";
        opsSalesTImg.frame = CGRectMake(opsSalesTImg.frame.origin.x, opsSalesTImg.frame.origin.y, 74, 15);//opsSalesTImg.frame.size.height);
        opsSalesTImg.backgroundColor = [UIColor colorWithHexString:@"B0B0B0"];
    }else{
        float ratio =  tywtdV / target ;
        ((UILabel*)[cardView viewWithTag:40+os]).text = [NSString stringWithFormat:@"%.0f%%", ratio*100];
        if(ratio>1) ratio = 1;
        opsSalesTImg.frame = CGRectMake(opsSalesTImg.frame.origin.x, opsSalesTImg.frame.origin.y, 74 * ratio, 15);//opsSalesTImg.frame.size.height);
        if(ratio<.5f){
            opsSalesTImg.backgroundColor = [UIColor colorWithHexString:@"F50505"];
        }else if (ratio<1){
            opsSalesTImg.backgroundColor = [UIColor colorWithHexString:@"EDED0C"];
        }else{
            opsSalesTImg.backgroundColor = [UIColor colorWithHexString:@"2EED0C"];
        }
    }
}


-(void) onDashboardFilterButtonClicked:(id)sender{
    if(self.filterVC==nil && self.filterArray!=nil && self.compStatusPopup==nil ){
        UIView* buttonView = (UIView*) sender;
        UIView* parentView = [buttonView superview];
        //GeneralSelectionPopup* vc = [[GeneralSelectionPopup alloc ] initWithArray:self.filterArray];
//        NSArray* compDataArray = @[@{}];
        GeneralSelectionPopup* vc = [[GeneralSelectionPopup alloc ] initWithArray:self.filterArray];
        vc.delegate = self;
        self.filterVC = [[[UIPopoverController alloc] initWithContentViewController:vc] autorelease];
        self.filterVC.delegate = self;
        CGRect rect = CGRectMake(parentView.frame.origin.x + buttonView.frame.origin.x, parentView.frame.origin.y + buttonView.frame.origin.y, buttonView.frame.size.width, buttonView.frame.size.height);
        [self.filterVC presentPopoverFromRect:rect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }

}


- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    if(self.filterVC != nil){
        self.filterVC = nil;
    }
    else if (self.compStatusPopup != nil){
        self.compStatusPopup = nil;
    }
    else if (self.storeReportSelectionPopup != nil){
        self.storeReportSelectionPopup = nil;
    }

}

-(void) onSelectdStore:(NSDictionary*) storeObj{
    if(self.storeReportSelectionPopup != nil){
        [self.storeReportSelectionPopup dismissPopoverAnimated:NO];
        self.storeReportSelectionPopup = nil;
        NSString* storeID = storeObj[@"Id"];
        StoreReportVC* vc = [[StoreReportVC alloc] initWithStoreID: storeID];
        [self.navigationController pushViewController:vc animated:YES];
    }
}



-(void) onRowSelected:(int) index{
    if (self.filterVC != nil) {
        [self.filterVC dismissPopoverAnimated:NO];
        self.filterVC = nil;
        
        self.selectedFilterIndex = index;
        [self refreshDashboard];
    }
    else if(self.compStatusPopup != nil){
        self.selectedCompStatusVal = [self.compStatusArray objectAtIndex:index][@"val"];
        NSString* labelName = [self.compStatusArray objectAtIndex:index][@"label"];
        [self.compStatusButton setTitle:labelName forState:UIControlStateNormal];
        [self.compStatusPopup dismissPopoverAnimated:YES];
        self.compStatusPopup = nil;
        [self refreshDashboard];
    }
}




-(void) refreshDashboard{
    NSLog(@"refreshDashboard#1");
    UIButton* filterButton = (UIButton*)[self.view viewWithTag:387];
    [filterButton addTarget:self action:@selector(onDashboardFilterButtonClicked:) forControlEvents:UIControlEventTouchUpInside];

    self.opsCard.hidden = NO;
    self.discountCard.hidden = NO;
    self.atvCard.hidden = NO;
    self.uptCard.hidden = NO;
    self.salesProductivityCard.hidden = NO;
    self.peelOffCard.hidden = NO;
    self.conversionCard.hidden = NO;
    self.visitorCard.hidden = NO;
    self.transactionCard.hidden = NO;
    
    self.filterArray = [NSMutableArray array];
    VFDAO* vf = [VFDAO sharedInstance];
    NSMutableDictionary* cache = vf.cache;

    if(cache!=nil){
        if([[cache allKeys] count] ==0){
            return;
        }
    }
    
    NSUserDefaults *defaultPref = [NSUserDefaults standardUserDefaults];
    NSString* preferBrandCode = [defaultPref objectForKey: USER_PREFER_ACCOUNT_CODE];
    NSString* preferCountryCode = [defaultPref objectForKey: USER_PREFER_COUNTRY_CODE];
    
    NSDictionary* dailyRecordDict;
    if((self.selectedCompStatusVal!=nil && ![self.selectedCompStatusVal isEqualToString:@""])){
        dailyRecordDict = [cache objectForKey:@"comp_summary_daily_analyse"];
    }else{
        dailyRecordDict = [cache objectForKey:@"summary_daily_analyse"];
    }
    NSArray* brandCountryCodes = [dailyRecordDict allKeys];
    for(NSString* brandCountryCode in brandCountryCodes){
        NSString* brandName = [brandCountryCode componentsSeparatedByString:@"-"][0];
        NSString* countryName = [brandCountryCode componentsSeparatedByString:@"-"][1];
        if((preferBrandCode==nil || [preferBrandCode isEqualToString:brandName]) &&
           (preferCountryCode==nil || [preferCountryCode isEqualToString:countryName])){
            NSArray* dailyInfoAry = [dailyRecordDict objectForKey: brandCountryCode];
            for(NSDictionary* dailyInfo in dailyInfoAry){
                NSString* dateStr = [dailyInfo objectForKey:@"name"];
                [self.filterArray addObject:@{
                                              @"label":[NSString stringWithFormat: [ShareLocale textFromKey:@"lbl_dashboard_at"] , dateStr],
                                              @"brand": brandCountryCode,
                                              @"date": dateStr
                                              }];
            }
        }
    }

    [self refreshBackground];
    [self refreshIcon];
    
    NSDictionary* latestSummaryReport = nil;
    NSString* brandCountryCode;
    NSString* dateStr;
    if([self.filterArray count]>0){
        NSDictionary* selecedFilterItem = [self.filterArray objectAtIndex:self.selectedFilterIndex];
        brandCountryCode = [selecedFilterItem objectForKey:@"brand"];
        
        dateStr = [selecedFilterItem objectForKey:@"date"];
        UILabel* dashboardFilterLabel = (UILabel*)[self.view viewWithTag:276];
        dashboardFilterLabel.text = [NSString stringWithFormat: [ShareLocale textFromKey:@"lbl_dashboard_at"] , dateStr];

        
        NSArray* reportArray = [dailyRecordDict objectForKey: brandCountryCode];
        
        
        for(NSDictionary* report in reportArray){
            if([[report objectForKey:@"name"] isEqualToString:dateStr]){
                latestSummaryReport = report;
                break;
            }
        }
    }else{
        brandCountryCode = [dailyRecordDict allKeys][0];
        if([dailyRecordDict[brandCountryCode] count]>0){
            latestSummaryReport = dailyRecordDict[brandCountryCode][0];
            dateStr = latestSummaryReport[@"cdate"];
        }
    }
    
    
    if(latestSummaryReport!=nil && latestSummaryReport!=(id)[NSNull null]){
        NSDictionary* visitorMap = [latestSummaryReport objectForKey:@"visitor_count"];
        NSDictionary* uptMap = [latestSummaryReport objectForKey:@"upt"];
        //NSDictionary* unitCountMap = [latestSummaryReport objectForKey:@"unit_count"];
        NSDictionary* txnCountMap = [latestSummaryReport objectForKey:@"txn_count"];
        NSDictionary* salesProdMap = [latestSummaryReport objectForKey:@"sales_prod"];
        NSDictionary* pelOffMap = [latestSummaryReport objectForKey:@"pel_off"];
        //NSDictionary* outsideTrafficMap = [latestSummaryReport objectForKey:@"outsdie_traffic"];
        NSDictionary* opsSalesMap = [latestSummaryReport objectForKey:@"ops_sales"];
        NSDictionary* discountMap = [latestSummaryReport objectForKey:@"discount"];
        //NSDictionary* crossSalesMap = [latestSummaryReport objectForKey:@"cross_Sales"]
        NSDictionary* convRateMap = [latestSummaryReport objectForKey:@"conv_rate"];
        NSDictionary* atvMap = [latestSummaryReport objectForKey:@"atv"];
        
        int week = [[latestSummaryReport objectForKey:@"wk"] intValue];
        int month = [[latestSummaryReport objectForKey:@"mo"] intValue];
        int year = [[latestSummaryReport objectForKey:@"yr"] intValue];
        
        
        float opsSaleTargetW = 0;
        float discountTargetW = 0;
        float uptTargetW = 0;
        float atvTargetW = 0;
        float crTargetW = 0;
        
        float opsSaleTargetM = 0;
        float discountTargetM = 0;
        float uptTargetM = 0;
        float atvTargetM = 0;
        float crTargetM = 0;
        
        float opsSaleTargetY = 0;
        float discountTargetY = 0;
        float uptTargetY = 0;
        float atvTargetY = 0;
        float crTargetY = 0;

        
        NSArray* targetList = [cache objectForKey:@"targets"];
        NSString* limitBrandCode = [brandCountryCode componentsSeparatedByString:@"-"][0];
        NSString* limitCountryCode = [brandCountryCode componentsSeparatedByString:@"-"][1];
        for(NSDictionary* dict in targetList){
            if( [ dict[@"Brand_Code__c"] isEqualToString: limitBrandCode] &&
               [ dict[@"System_Code__c"] isEqualToString: limitCountryCode] ){
                NSString* type=  [dict objectForKey:@"Type__c"];
                
                if([type isEqualToString:@"weekly"]){
                    int w = [[dict objectForKey:@"Financial_Week__c"] intValue];
                    int y = [[dict objectForKey:@"Financial_Year__c"] intValue];
                    if(w==week && y==year){
                        if([dict objectForKey:@"OPS_Sales_Amount__c"]!=[NSNull null]) {
                            opsSaleTargetW += [[dict objectForKey:@"OPS_Sales_Amount__c"] floatValue];
                        }
                        if([dict objectForKey:@"Discount__c"]!=[NSNull null] &&  discountTargetW==0){
                            discountTargetW = [[dict objectForKey:@"Discount__c"] floatValue];
                        }
                        if([dict objectForKey:@"UPT__c"]!=[NSNull null] && uptTargetW==0){
                            uptTargetW = [[dict objectForKey:@"UPT__c"] floatValue];
                        }
                        if([dict objectForKey:@"ATV__c"]!=[NSNull null] && atvTargetW==0){
                            atvTargetW = [[dict objectForKey:@"ATV__c"] floatValue];
                        }
                        if([dict objectForKey:@"Conversion_Rate__c"]!=[NSNull null] && crTargetW==0){
                            crTargetW = [[dict objectForKey:@"Conversion_Rate__c"] floatValue];
                        }
                    }
                }
                else if([type isEqualToString:@"monthly"]){
                    int m = [[dict objectForKey:@"Financial_Month__c"] intValue];
                    int y = [[dict objectForKey:@"Financial_Year__c"] intValue];
                    if(m==month && y==year){
                        if([dict objectForKey:@"OPS_Sales_Amount__c"]!=[NSNull null]){
                            opsSaleTargetM += [[dict objectForKey:@"OPS_Sales_Amount__c"] floatValue];
                        }
                        if([dict objectForKey:@"Discount__c"]!=[NSNull null] && discountTargetM==0){
                            discountTargetM = [[dict objectForKey:@"Discount__c"] floatValue];
                        }
                        if([dict objectForKey:@"UPT__c"]!=[NSNull null] && uptTargetM==0){
                            uptTargetM = [[dict objectForKey:@"UPT__c"] floatValue];
                        }
                        if([dict objectForKey:@"ATV__c"]!=[NSNull null] && atvTargetM==0){
                            atvTargetM = [[dict objectForKey:@"ATV__c"] floatValue];
                        }
                        if([dict objectForKey:@"Conversion_Rate__c"]!=[NSNull null] && crTargetM==0){
                            crTargetM = [[dict objectForKey:@"Conversion_Rate__c"] floatValue];
                        }
                    }
                }
                else if([type isEqualToString:@"yearly"]){
                    int y = [[dict objectForKey:@"Financial_Year__c"] intValue];
                    if(y==year){
                        if([dict objectForKey:@"OPS_Sales_Amount__c"]!=[NSNull null]){
                            opsSaleTargetY += [[dict objectForKey:@"OPS_Sales_Amount__c"] floatValue];
                        }
                        if([dict objectForKey:@"Discount__c"]!=[NSNull null] && discountTargetY==0){
                            discountTargetY = [[dict objectForKey:@"Discount__c"] floatValue];
                        }
                        if([dict objectForKey:@"UPT__c"]!=[NSNull null] && uptTargetY==0){
                            uptTargetY = [[dict objectForKey:@"UPT__c"] floatValue];
                        }
                        if([dict objectForKey:@"ATV__c"]!=[NSNull null] && atvTargetY==0){
                            atvTargetY = [[dict objectForKey:@"ATV__c"] floatValue];
                        }
                        if([dict objectForKey:@"Conversion_Rate__c"]!=[NSNull null] && crTargetY==0){
                            crTargetY = [[dict objectForKey:@"Conversion_Rate__c"] floatValue];
                        }
                    }
                }
            };
        }
        
        
        
        NSMutableDictionary* summaryParam = [NSMutableDictionary dictionary];
        [summaryParam setObject:[NSNumber numberWithInt:week] forKey:@"week"];
        [summaryParam setObject:[NSNumber numberWithInt:month] forKey:@"month"];
        [summaryParam setObject:[NSNumber numberWithInt:year] forKey:@"year"];
        
        [summaryParam setObject: [self getDoubleWithFieldName:@"TYWTD" fromDictionary:opsSalesMap] forKey:@"ops_sales_wtd"];
        [summaryParam setObject: [self getDoubleWithFieldName:@"TYMTD" fromDictionary:opsSalesMap] forKey:@"ops_sales_mtd"];
        [summaryParam setObject: [self getDoubleWithFieldName:@"TYYTD" fromDictionary:opsSalesMap] forKey:@"ops_sales_ytd"];
        [summaryParam setObject: [self getDoubleWithFieldName:@"TYWTD" fromDictionary:discountMap] forKey:@"discount_wtd"];
        [summaryParam setObject: [self getDoubleWithFieldName:@"TYMTD" fromDictionary:discountMap] forKey:@"discount_mtd"];
        [summaryParam setObject: [self getDoubleWithFieldName:@"TYYTD" fromDictionary:discountMap] forKey:@"discount_ytd"];
        [summaryParam setObject: [self getDoubleWithFieldName:@"TYWTD" fromDictionary:salesProdMap] forKey:@"salesprod_wtd"];
        [summaryParam setObject: [self getDoubleWithFieldName:@"TYMTD" fromDictionary:salesProdMap] forKey:@"salesprod_mtd"];
        [summaryParam setObject: [self getDoubleWithFieldName:@"TYYTD" fromDictionary:salesProdMap] forKey:@"salesprod_ytd"];
        [summaryParam setObject: [self getDoubleWithFieldName:@"TYWTD" fromDictionary:atvMap] forKey:@"atv_wtd"];
        [summaryParam setObject: [self getDoubleWithFieldName:@"TYMTD" fromDictionary:atvMap] forKey:@"atv_mtd"];
        [summaryParam setObject: [self getDoubleWithFieldName:@"TYYTD" fromDictionary:atvMap] forKey:@"atv_ytd"];
        [summaryParam setObject: [self getDoubleWithFieldName:@"TYWTD" fromDictionary:uptMap] forKey:@"upt_wtd"];
        [summaryParam setObject: [self getDoubleWithFieldName:@"TYMTD" fromDictionary:uptMap] forKey:@"upt_mtd"];
        [summaryParam setObject: [self getDoubleWithFieldName:@"TYYTD" fromDictionary:uptMap] forKey:@"upt_ytd"];
        [summaryParam setObject: [self getDoubleWithFieldName:@"TYWTD" fromDictionary:txnCountMap] forKey:@"txn_wtd"];
        [summaryParam setObject: [self getDoubleWithFieldName:@"TYMTD" fromDictionary:txnCountMap] forKey:@"txn_mtd"];
        [summaryParam setObject: [self getDoubleWithFieldName:@"TYYTD" fromDictionary:txnCountMap] forKey:@"txn_ytd"];
        [summaryParam setObject: [self getDoubleWithFieldName:@"TYWTD" fromDictionary:visitorMap] forKey:@"visitor_wtd"];
        [summaryParam setObject: [self getDoubleWithFieldName:@"TYMTD" fromDictionary:visitorMap] forKey:@"visitor_mtd"];
        [summaryParam setObject: [self getDoubleWithFieldName:@"TYYTD" fromDictionary:visitorMap] forKey:@"visitor_ytd"];
        [summaryParam setObject: [self getDoubleWithFieldName:@"TYWTD" fromDictionary:pelOffMap] forKey:@"pel_wtd"];
        [summaryParam setObject: [self getDoubleWithFieldName:@"TYMTD" fromDictionary:pelOffMap] forKey:@"pel_mtd"];
        [summaryParam setObject: [self getDoubleWithFieldName:@"TYYTD" fromDictionary:pelOffMap] forKey:@"pel_ytd"];
        [summaryParam setObject: [self getDoubleWithFieldName:@"TYWTD" fromDictionary:convRateMap]  forKey:@"cr_wtd"];
        [summaryParam setObject: [self getDoubleWithFieldName:@"TYMTD" fromDictionary:convRateMap] forKey:@"cr_mtd"];
        [summaryParam setObject: [self getDoubleWithFieldName:@"TYYTD" fromDictionary:convRateMap] forKey:@"cr_ytd"];

        [cache setObject:summaryParam forKey:[NSString stringWithFormat:@"current_summary_%@", dateStr ]];//2014-6-15
        
        //ops sales
        [self fillCard:self.opsCard tagOffset:0 reportDictionary:opsSalesMap currentKey:@"TYWTD" passKey:@"LYWTD" format:nil target:opsSaleTargetW scale:1];
        [self fillCard:self.opsCard tagOffset:1 reportDictionary:opsSalesMap currentKey:@"TYMTD" passKey:@"LYMTD" format:nil target:opsSaleTargetM scale:1];
        [self fillCard:self.opsCard tagOffset:2 reportDictionary:opsSalesMap currentKey:@"TYYTD" passKey:@"LYYTD" format:nil target:opsSaleTargetY scale:1];
        //discount
        [self fillCard:self.discountCard tagOffset:0 reportDictionary:discountMap currentKey:@"TYWTD" passKey:@"LYWTD" format:@"%.1f%%" target:discountTargetW scale:100 nagativeComparing:YES];
        [self fillCard:self.discountCard tagOffset:1 reportDictionary:discountMap currentKey:@"TYMTD" passKey:@"LYMTD" format:@"%.1f%%" target:discountTargetM scale:100 nagativeComparing:YES];
        [self fillCard:self.discountCard tagOffset:2 reportDictionary:discountMap currentKey:@"TYYTD" passKey:@"LYYTD" format:@"%.1f%%" target:discountTargetY scale:100 nagativeComparing:YES];
        //sales prod
        [self fillCard:self.salesProductivityCard tagOffset:0 reportDictionary:salesProdMap currentKey:@"TYWTD" passKey:@"LYWTD" format:nil target: 0 scale:1];
        [self fillCard:self.salesProductivityCard tagOffset:1 reportDictionary:salesProdMap currentKey:@"TYMTD" passKey:@"LYMTD" format:nil target: 0 scale:1];
        [self fillCard:self.salesProductivityCard tagOffset:2 reportDictionary:salesProdMap currentKey:@"TYYTD" passKey:@"LYYTD" format:nil target: 0 scale:1];
        
        //atv
        [self fillCard:self.atvCard tagOffset:0 reportDictionary:atvMap currentKey:@"TYWTD" passKey:@"LYWTD" format:nil target: atvTargetW scale:1];
        [self fillCard:self.atvCard tagOffset:1 reportDictionary:atvMap currentKey:@"TYMTD" passKey:@"LYMTD" format:nil target: atvTargetM scale:1];
        [self fillCard:self.atvCard tagOffset:2 reportDictionary:atvMap currentKey:@"TYYTD" passKey:@"LYYTD" format:nil  target: atvTargetY scale:1];
        //upt
        [self fillCard:self.uptCard tagOffset:0 reportDictionary:uptMap currentKey:@"TYWTD" passKey:@"LYWTD" format:nil  target: uptTargetW scale:1];
        [self fillCard:self.uptCard tagOffset:1 reportDictionary:uptMap currentKey:@"TYMTD" passKey:@"LYMTD" format:nil  target: uptTargetM scale:1];
        [self fillCard:self.uptCard tagOffset:2 reportDictionary:uptMap currentKey:@"TYYTD" passKey:@"LYYTD" format:nil  target: uptTargetY scale:1];
        //txn
        [self fillCard:self.transactionCard tagOffset:0 reportDictionary:txnCountMap currentKey:@"TYWTD" passKey:@"LYWTD" format:nil  target:0 scale:1];
        [self fillCard:self.transactionCard tagOffset:1 reportDictionary:txnCountMap currentKey:@"TYMTD" passKey:@"LYMTD" format:nil  target:0 scale:1];
        [self fillCard:self.transactionCard tagOffset:2 reportDictionary:txnCountMap currentKey:@"TYYTD" passKey:@"LYYTD" format:nil  target:0 scale:1];
        
        //visitor count
        [self fillCard:self.visitorCard tagOffset:0 reportDictionary:visitorMap currentKey:@"TYWTD" passKey:@"LYWTD" format:nil  target:0 scale:1];
        [self fillCard:self.visitorCard tagOffset:1 reportDictionary:visitorMap currentKey:@"TYMTD" passKey:@"LYMTD" format:nil  target:0 scale:1];
        [self fillCard:self.visitorCard tagOffset:2 reportDictionary:visitorMap currentKey:@"TYYTD" passKey:@"LYYTD" format:nil  target:0 scale:1];
        //peel off
        [self fillCard:self.peelOffCard tagOffset:0 reportDictionary:pelOffMap currentKey:@"TYWTD" passKey:@"LYWTD" format:@"%.1f%%"  target:0 scale:100];
        [self fillCard:self.peelOffCard tagOffset:1 reportDictionary:pelOffMap currentKey:@"TYMTD" passKey:@"LYMTD" format:@"%.1f%%"  target:0 scale:100];
        [self fillCard:self.peelOffCard tagOffset:2 reportDictionary:pelOffMap currentKey:@"TYYTD" passKey:@"LYYTD" format:@"%.1f%%"  target:0 scale:100];
        //conv rate
        [self fillCard:self.conversionCard tagOffset:0 reportDictionary:convRateMap currentKey:@"TYWTD" passKey:@"LYWTD" format:@"%.1f%%"  target:crTargetW scale:100];
        [self fillCard:self.conversionCard tagOffset:1 reportDictionary:convRateMap currentKey:@"TYMTD" passKey:@"LYMTD" format:@"%.1f%%"  target:crTargetM scale:100];
        [self fillCard:self.conversionCard tagOffset:2 reportDictionary:convRateMap currentKey:@"TYYTD" passKey:@"LYYTD" format:@"%.1f%%"  target:crTargetY scale:100];
        
    }else{
        NSLog(@"NO report found");
        [self fillCard:self.opsCard tagOffset:0 reportDictionary:@{} currentKey:@"TYWTD" passKey:@"LYWTD" format:nil target:0 scale:1];
        [self fillCard:self.opsCard tagOffset:1 reportDictionary:@{} currentKey:@"TYMTD" passKey:@"LYMTD" format:nil target:0 scale:1];
        [self fillCard:self.opsCard tagOffset:2 reportDictionary:@{} currentKey:@"TYLTD" passKey:@"LYLTD" format:nil target:0 scale:1];
        [self fillCard:self.discountCard tagOffset:0 reportDictionary:@{} currentKey:@"TYWTD" passKey:@"LYWTD" format:nil target:0 scale:1];
        [self fillCard:self.discountCard tagOffset:1 reportDictionary:@{} currentKey:@"TYMTD" passKey:@"LYMTD" format:nil target:0 scale:1];
        [self fillCard:self.discountCard tagOffset:2 reportDictionary:@{} currentKey:@"TYLTD" passKey:@"LYLTD" format:nil target:0 scale:1];
        [self fillCard:self.salesProductivityCard tagOffset:0 reportDictionary:@{} currentKey:@"TYWTD" passKey:@"LYWTD" format:nil target:0 scale:1];
        [self fillCard:self.salesProductivityCard tagOffset:1 reportDictionary:@{} currentKey:@"TYMTD" passKey:@"LYMTD" format:nil target:0 scale:1];
        [self fillCard:self.salesProductivityCard tagOffset:2 reportDictionary:@{} currentKey:@"TYLTD" passKey:@"LYLTD" format:nil target:0 scale:1];
        [self fillCard:self.atvCard tagOffset:0 reportDictionary:@{} currentKey:@"TYWTD" passKey:@"LYWTD" format:nil target:0 scale:1];
        [self fillCard:self.atvCard tagOffset:1 reportDictionary:@{} currentKey:@"TYMTD" passKey:@"LYMTD" format:nil target:0 scale:1];
        [self fillCard:self.atvCard tagOffset:2 reportDictionary:@{} currentKey:@"TYLTD" passKey:@"LYLTD" format:nil target:0 scale:1];
        [self fillCard:self.uptCard tagOffset:0 reportDictionary:@{} currentKey:@"TYWTD" passKey:@"LYWTD" format:nil target:0 scale:1];
        [self fillCard:self.uptCard tagOffset:1 reportDictionary:@{} currentKey:@"TYMTD" passKey:@"LYMTD" format:nil target:0 scale:1];
        [self fillCard:self.uptCard tagOffset:2 reportDictionary:@{} currentKey:@"TYLTD" passKey:@"LYLTD" format:nil target:0 scale:1];
        [self fillCard:self.transactionCard tagOffset:0 reportDictionary:@{} currentKey:@"TYWTD" passKey:@"LYWTD" format:nil target:0 scale:1];
        [self fillCard:self.transactionCard tagOffset:1 reportDictionary:@{} currentKey:@"TYMTD" passKey:@"LYMTD" format:nil target:0 scale:1];
        [self fillCard:self.transactionCard tagOffset:2 reportDictionary:@{} currentKey:@"TYLTD" passKey:@"LYLTD" format:nil target:0 scale:1];
        [self fillCard:self.visitorCard tagOffset:0 reportDictionary:@{} currentKey:@"TYWTD" passKey:@"LYWTD" format:nil target:0 scale:1];
        [self fillCard:self.visitorCard tagOffset:1 reportDictionary:@{} currentKey:@"TYMTD" passKey:@"LYMTD" format:nil target:0 scale:1];
        [self fillCard:self.visitorCard tagOffset:2 reportDictionary:@{} currentKey:@"TYLTD" passKey:@"LYLTD" format:nil target:0 scale:1];
        [self fillCard:self.peelOffCard tagOffset:0 reportDictionary:@{} currentKey:@"TYWTD" passKey:@"LYWTD" format:nil target:0 scale:1];
        [self fillCard:self.peelOffCard tagOffset:1 reportDictionary:@{} currentKey:@"TYMTD" passKey:@"LYMTD" format:nil target:0 scale:1];
        [self fillCard:self.peelOffCard tagOffset:2 reportDictionary:@{} currentKey:@"TYLTD" passKey:@"LYLTD" format:nil target:0 scale:1];
        [self fillCard:self.conversionCard tagOffset:0 reportDictionary:@{} currentKey:@"TYWTD" passKey:@"LYWTD" format:nil target:0 scale:1];
        [self fillCard:self.conversionCard tagOffset:1 reportDictionary:@{} currentKey:@"TYMTD" passKey:@"LYMTD" format:nil target:0 scale:1];
        [self fillCard:self.conversionCard tagOffset:2 reportDictionary:@{} currentKey:@"TYLTD" passKey:@"LYLTD" format:nil target:0 scale:1];
    }

    
    
    [self applyStartupAnimationToView:self.opsCard delay:.1f ];
    [self applyStartupAnimationToView:self.discountCard delay:.2f ];
    [self applyStartupAnimationToView:self.salesProductivityCard delay:.3f ];
    [self applyStartupAnimationToView:self.atvCard delay:.4f ];
    [self applyStartupAnimationToView:self.uptCard delay:.5f ];
    [self applyStartupAnimationToView:self.transactionCard delay:.6f];
    [self applyStartupAnimationToView:self.visitorCard delay:.7f ];
    [self applyStartupAnimationToView:self.peelOffCard delay:.8f ];
    [self applyStartupAnimationToView:self.conversionCard delay:.9f ];

}



-(NSNumber*) getDoubleWithFieldName:(NSString*)key fromDictionary:(NSDictionary*)data{
    if( [data objectForKey:key]==nil|| [data objectForKey:key]==(id)[NSNull null]){
        return [NSNumber numberWithInt:0];
    }else{
        return [data objectForKey:key];
    }
}


-(void)viewDidAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
    [super viewDidAppear:animated];
    
    [self refreshBackground];
    [self refreshIcon];
    [self refreshLocale];
    
    [self.marqueeView reloadMessageMarqueeViewData];
    
    if (nextViewController!=nil){
        __block HomeViewController* me = self;

        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 50 * NSEC_PER_MSEC), dispatch_get_main_queue(), ^{
            UIViewController* vc = nextViewController;
            [me.navigationController pushViewController:vc animated:YES];
            [vc release];
            nextViewController = nil;
            return;
        });
    }else{
        
        //do nth if user click home button and go back during loading data
//        if ( ![[UIApplication sharedApplication] isIgnoringInteractionEvents] ){
        
            [self refreshNetworkStatusUI];
            [self forceReloadIfNeeded];
            
            if(self.commentSidebar!=nil && self.queryBrandListRequest==nil){
                //if sidebar exist and page is not loading anything
                [self.commentSidebar selfBinding];
                self.commentSidebar.selectedStoreId = nil;
                [self.commentSidebar loadStoreComments];
            }
//        }
    }
    
    [self refreshDashboard];
}


-(void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
//    self.opsCard.hidden = YES;
//    self.discountCard.hidden = YES;
//    self.atvCard.hidden = YES;
//    self.uptCard.hidden = YES;
//    self.salesProductivityCard.hidden = YES;
//    self.peelOffCard.hidden = YES;
//    self.conversionCard.hidden = YES;
//    self.visitorCard.hidden = YES;
//    self.transactionCard.hidden = YES;
    
//StoreReport CR
//    if(self.auditMenu!=nil){
//        [self.auditMenu removeFromSuperview];
//        self.auditMenu = nil;
//    }
}

//To avoid user need to restart the app(kill app manually) everyday,
//added code snippet to check if today loaded basic data already
//If no, force reload
-(void) forceReloadIfNeeded{
    if(self.queryBrandListRequest!=nil){
        return;
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* lastLoadDateTimeStr = [defaults objectForKey: @"LAST_LOAD_USERINFO_TIME"];
    BOOL needReload = NO;
    if(lastLoadDateTimeStr==nil || lastLoadDateTimeStr==(id)[NSNull null]){
        needReload = YES;
    }
    else{
        NSDateFormatter *_df = [[NSDateFormatter alloc] init];
        [_df setDateFormat:@"yyyy-MM-dd"];
        NSDate* lastLoadDateTime = [_df dateFromString:lastLoadDateTimeStr];
        if(lastLoadDateTime==nil){
            needReload = YES;
        }

        NSString *_d1 = [_df stringFromDate:lastLoadDateTime];
        NSString *_d2 = [_df stringFromDate:[NSDate date]];

        if(![_d1 isEqualToString:_d2]){
            needReload = YES;
        }
    }
    
    if(needReload){
        [self loadBrandList];
    }
    
}

-(void) refreshNetworkStatusUI{
    VFDAO* dao = [VFDAO sharedInstance];
    if([dao isOnlineMode]){
        [self.globalButton setBackgroundImage:[UIImage imageNamed:@"button_green_s"] forState:UIControlStateNormal];
        NSLog(@"Refresh button status as ONLINE_MODE");
    }
    else{
        [self.globalButton setBackgroundImage:[UIImage imageNamed:@"button_purple_s"] forState:UIControlStateNormal];
        NSLog(@"Refresh button status as OFFLINE_MODE");
    }

}



-(void) refreshIcon{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString* theme = [defaults objectForKey: USER_PREFER_ACCOUNT_CODE];
    
    UIImageView* iconView = (UIImageView*)[self.view viewWithTag:700];
    int offsetX = 1014;
    int offsetY = 20;

    if(theme!=nil){
        if([theme isEqualToString:ACCOUNT_VAN_CODE]){
            float width = 143;
            float height = 90;
            [iconView setImage: [UIImage imageNamed:@"header_vans_logo.png"]];
            iconView.frame = CGRectMake(offsetX-width, offsetY, width, height);
        }
        else if([theme isEqualToString:ACCOUNT_TBL_CODE]){
            float width = 300;
            float height = 67;
            [iconView setImage: [UIImage imageNamed:@"timberland_black_big.png"]];
            iconView.frame = CGRectMake(offsetX-width, offsetY, width, height);
        }
        else if([theme isEqualToString:ACCOUNT_KTG_CODE]){
            float width = 210*.9f;
            float height = 90*.9f;
            [iconView setImage: [UIImage imageNamed:@"kipling_logo.jpg"]];
            iconView.frame = CGRectMake(offsetX-width, offsetY, width, height);
        }
        else if([theme isEqualToString:ACCOUNT_LEE_CODE]){
            float width = 132;
            float height = 75;
            [iconView setImage: [UIImage imageNamed:@"lee_logo.png"]];
            iconView.frame = CGRectMake(offsetX-width, offsetY, width, height);
        }
        else if([theme isEqualToString:ACCOUNT_TNF_CODE]){
            float width = 102*0.9;
            float height = 97*0.9;
            [iconView setImage: [UIImage imageNamed:@"northface_logo.png"]];
            iconView.frame = CGRectMake(offsetX-width, offsetY, width, height);
        }
        else if([theme isEqualToString:ACCOUNT_WGR_CODE]){
            float width = 139*.9f;
            float height = 96*.9f;
            [iconView setImage: [UIImage imageNamed:@"wrangler_logo.png"]];
            iconView.frame = CGRectMake(offsetX-width, offsetY, width, height);
        }
        else if([theme isEqualToString:ACCOUNT_NAP_CODE]){
            float width = 139*.9f;
            float height = 96*.9f;
            [iconView setImage: [UIImage imageNamed:@"nap_logo.png"]];
            iconView.frame = CGRectMake(offsetX-width, offsetY, width, height);
        }
    }else{
        float width = 300;
        float height = 67;
        [iconView setImage: [UIImage imageNamed:@"timberland_black_big.png"]];
            iconView.frame = CGRectMake(offsetX-width, offsetY, width, height);
    }
    
}



#pragma mark - Menu Button Delegate



- (IBAction)onHelpButtonClicked:(id)sender {
    HelpViewController* vc = [[[HelpViewController alloc] init] autorelease];
    [self.navigationController pushViewController: vc animated:YES];
}


- (IBAction)onStoreButtonClicked:(id)sender{
    ChooseStoreViewController* vc = [[[ChooseStoreViewController alloc] init] autorelease];
    [self.navigationController pushViewController: vc animated:YES];

}

- (IBAction)onTrainingMaterialButtonClicked:(id)sender{
    ProductCatalogBrandViewController* vc = [[ProductCatalogBrandViewController alloc] init] ;
    [self.navigationController pushViewController:vc animated:YES];
    [vc release];
}

- (IBAction)onVisitPlanButtonClicked:(id)sender{
    VisitationViewController* vc = [[[VisitationViewController alloc] init] autorelease];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)onInboxButtonClicked:(id)sender{
    NewsViewController* vc = [[[NewsViewController alloc] init] autorelease];
    [self.navigationController pushViewController:vc animated:YES];
}


//StoreReport CR
//- (void)onReleaseAuditResultButton:(id)sender {
//    if(self.auditMenu!=nil){
//        [self.auditMenu removeFromSuperview];
//        self.auditMenu = nil;
//    }
//}


- (IBAction)onInventoryButtonClicked:(id)sender {
    UIViewController* vc = [[ChooseStoreViewController alloc] initWithMode:MODE_INVENTORY];
    [self.navigationController pushViewController:vc animated:YES];
    [vc release];
}


- (IBAction)onStaffButtonClicked:(id)sender{
    
}

- (IBAction)onCustomerButtonClicked:(id)sender{
    
}

- (IBAction)onAuditResultButtonClicked:(id)sender{

//  #StoreReport CR

    if(self.storeReportSelectionPopup == nil){
//        VFDAO* vf = [VFDAO sharedInstance];
//        NSDictionary* cache = vf.cache;
//        NSDictionary* cacheDataDict = [cache objectForKey:@"store_daily_analyse"];
//        self.storeArray = [[NSMutableArray alloc] init];
//
//        NSUserDefaults *defaultPref = [NSUserDefaults standardUserDefaults];
//        NSString* preferBrandCode = [defaultPref objectForKey: USER_PREFER_ACCOUNT_CODE];
//        NSString* preferCountryCode = [defaultPref objectForKey:USER_PREFER_COUNTRY_CODE];
//        
//        
//        for(NSDictionary* storeData in [cacheDataDict allValues]){
//            if(storeData[@"account_code"] ==(id)[NSNull null] ||
//               ![storeData[@"account_code"] isEqualToString: preferBrandCode] ||
//               storeData[@"country_code"] ==(id)[NSNull null] ||
//               ![storeData[@"country_code"] isEqualToString: preferCountryCode]){
//                continue;
//            }
//            [self.storeArray addObject: @{@"label":storeData[@"name"], @"val":storeData[@"store_id"]}];
//        }
        
        VFDAO* vf = [VFDAO sharedInstance];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSArray* allStores = [vf getStoresByBrandCode: [defaults objectForKey: USER_PREFER_ACCOUNT_CODE] countryCode:[defaults objectForKey:USER_PREFER_COUNTRY_CODE] needOptionAll:NO];
        self.storeArray = [allStores mutableCopy];
        
        UIView* view = (UIView*) sender;
        StoreSelectionViewController* vc = [[StoreSelectionViewController alloc] initWithStoreList: self.storeArray] ;
        vc.delegate = self;
        self.storeReportSelectionPopup = [[[UIPopoverController alloc] initWithContentViewController:vc] autorelease];
        self.storeReportSelectionPopup.delegate = self;
        [self.storeReportSelectionPopup presentPopoverFromRect:view.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }

    
    
//  #Before StoreReport CR
//    UIButton* view = (UIButton*)sender;
//    
//    float cellWidth = view.frame.size.width;
//    float cellHeight = 80;
//    
//    self.auditMenu = [[UIView alloc] init];
//    self.auditMenu.frame = CGRectMake(0,0,1024,768);
//    self.auditMenu.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
//    UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onReleaseAuditResultButton:)];
//    [self.auditMenu addGestureRecognizer:singleFingerTap];
//    [singleFingerTap release];
//    [self.view addSubview: self.auditMenu ];
//    
//    UIButton* visitFormButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    visitFormButton.titleLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:15];
//    [visitFormButton setBackgroundImage:[UIImage imageNamed:@"subbutton_blue.png"] forState:UIControlStateNormal];
//    [visitFormButton setTitle: [ShareLocale textFromKey:@"surveytitle_visit"] forState:UIControlStateNormal];
//    visitFormButton.backgroundColor = [UIColor colorWithWhite:0 alpha:1.0f];
//    visitFormButton.titleLabel.shadowColor = [UIColor darkGrayColor];
//    visitFormButton.titleLabel.shadowOffset = CGSizeMake(1, 1);
//    
//    visitFormButton.frame = CGRectMake(view.frame.origin.x + cellWidth+1, view.frame.origin.y, cellWidth, cellHeight);
//    [self.auditMenu addSubview:visitFormButton];
//    [visitFormButton addTarget:self action:@selector(gotoVisitResultViewController) forControlEvents:UIControlEventTouchUpInside];
//
//    
//    UIButton* sopButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    sopButton.titleLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:15];
//    [sopButton setBackgroundImage:[UIImage imageNamed:@"subbutton_blue.png"] forState:UIControlStateNormal];
//    [sopButton setTitle:[ShareLocale textFromKey:@"surveytitle_audit"] forState:UIControlStateNormal];
//    [self.auditMenu addSubview:sopButton];
//    sopButton.backgroundColor = [UIColor colorWithWhite:0 alpha:1.0f];
//    sopButton.frame = CGRectMake(view.frame.origin.x + cellWidth+1, view.frame.origin.y  + cellHeight, cellWidth, cellHeight);
//    sopButton.titleLabel.shadowColor = [UIColor darkGrayColor];
//    sopButton.titleLabel.shadowOffset = CGSizeMake(1, 1);
//    [sopButton addTarget:self action:@selector(gotoSOPResultViewController) forControlEvents:UIControlEventTouchUpInside];
//    
//    UIButton* mspButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    mspButton.titleLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:15];
//    [mspButton setBackgroundImage:[UIImage imageNamed:@"subbutton_blue.png"] forState:UIControlStateNormal];
//    [mspButton setTitle:[ShareLocale textFromKey:@"surveytitle_msp"] forState:UIControlStateNormal];
//    [self.auditMenu addSubview:mspButton];
//    mspButton.backgroundColor = [UIColor colorWithWhite:0 alpha:1.0f];
//    mspButton.frame = CGRectMake(view.frame.origin.x + cellWidth+1, view.frame.origin.y  + cellHeight*2, cellWidth, cellHeight);
//    mspButton.titleLabel.shadowColor = [UIColor darkGrayColor];
//    mspButton.titleLabel.shadowOffset = CGSizeMake(1, 1);
//    [mspButton addTarget:self action:@selector(gotoMSPResultViewController) forControlEvents:UIControlEventTouchUpInside];
    
    
}

- (IBAction)onUsageReportButtonClicked:(id)sender{
    UsageReportViewController* reportVC = [[UsageReportViewController alloc] initWithNibName:@"UsageReportViewController" bundle:nil];
    [self.navigationController pushViewController:reportVC animated:YES];
}


- (IBAction)onSettingButtonClicked:(id)sender{
    SettingViewController* vc = [[[SettingViewController alloc] init] autorelease];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)onCompStatusButtonClicked:(id)sender {
    if(self.compStatusPopup == nil && self.filterVC == nil){
        
        UIView* view = (UIView*) sender;
        GeneralSelectionPopup* vc = [[GeneralSelectionPopup alloc ] initWithArray:self.compStatusArray];
        vc.delegate = self;
        self.compStatusPopup = [[[UIPopoverController alloc] initWithContentViewController:vc] autorelease];
        self.compStatusPopup.delegate = self;
        [self.compStatusPopup presentPopoverFromRect:view.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}




#pragma mark - DAO delegate & CustomWebserviceApi Delegate
- (void)loadBrandList{
    [self showLoadingPopup:self.view];
    self.queryBrandListRequest = [[VFDAO sharedInstance] selectAppInitDataWithDelegate:self parameter:@{@"action":@"load_brands"}];
}

- (void)loadBrandDetailDataWithCompStatus:(NSString*)compStatus{
    NSDictionary* account;
    if(compStatus!=nil && ![compStatus isEqualToString:@""]){
        account = [self.waitQueryCompAccountArray lastObject];
    }else{
        account = [self.waitQueryAccountArray lastObject];
    }
    VFDAO* vf = [VFDAO sharedInstance];
    NSDictionary* cache = vf.cache;
    
    self.queryBrandDetaliRequest = [[VFDAO sharedInstance] selectBrandDetailDataWithDelegate:self parameter:@{@"action":@"load_data", @"code":account[@"Code__c"], @"countryCode":account[@"Country_Code__c"], @"fxdate":cache[@"fxdate"], @"fxyr":cache[@"fxyear"],@"fxmo":cache[@"fxmonth"],@"fxwk":cache[@"fxweek"],@"comp_status":compStatus}];
}


-(void) loadCapacity{
    self.queryCapacityRequest = [[VFDAO sharedInstance] selectCapacityWithDelegate:self];
}

-(void) loadSurveyMaster{
    self.querySurveyMasterRequest = [[VFDAO sharedInstance] selectSurveyMasterDataWithDelegate:self];
}

-(void) loadTargets{
    self.queryTargetsRequest = [[VFDAO sharedInstance] selectTargetsWithDelegate:self];
}

-(void) loadNotifications{
    self.queryNotificationRequest = [[VFDAO sharedInstance] selectBroadcastNoticeWithDelegate:self];
}




-(void) handleQueryBrandListRequest:(NSArray*)response{
    self.waitQueryAccountArray = [[response firstObject][@"accountList"] mutableCopy];
    self.waitQueryCompAccountArray = [[response firstObject][@"accountList"] mutableCopy];
    
    NSDateFormatter* _df = [[NSDateFormatter alloc] init];
    [_df setDateFormat:@"yyyy-MM-dd"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[_df stringFromDate:[NSDate date]] forKey:@"LAST_LOAD_USERINFO_TIME"];
    [defaults synchronize];

    
    NSDictionary* data = [response firstObject];
    
    NSNumber *num = [DataHelper getFieldDisplayWithFieldName:@"timezone" withDictionary:data default:@0];
    GeneralUiTool* tool = [GeneralUiTool shareInstance];
    tool.timezoneSecond = [num integerValue]/1000;
    
    NSString* userPreferBrandCode = [defaults objectForKey: USER_PREFER_ACCOUNT_CODE];
    NSString* userPreferBrandId = [defaults objectForKey: USER_PREFER_ACCOUNT_ID];
    NSString* userPreferCountryCode = [defaults objectForKey: USER_PREFER_COUNTRY_CODE];
    
    BOOL existingPreferBrandCodeExist = NO;
    BOOL existingPreferCountryCodeExist = NO;
    NSMutableArray* userOwnedCombineCodes = [NSMutableArray array];
    NSMutableArray* userOwnedBrandCodes = [NSMutableArray array];
    NSMutableArray* userOwnedBrandIds = [NSMutableArray array];
    NSMutableArray* userOwnedCountryCodes = [NSMutableArray array];
    for(NSDictionary* brandData in _waitQueryAccountArray){
        NSString* countryCode = brandData[@"Country_Code__c"];
        
        NSString* acctCode = brandData[@"Code__c"];
        NSString* acctId = brandData[@"account_id"];
        if(acctCode==nil){
            acctCode = @"TBL";
        }
        if(acctId==nil){
            acctId = @"0019000000sTJWa";
        }
        NSString* uniqueBrandCode = [NSString stringWithFormat:@"%@%@", acctCode, countryCode];
        if(![userOwnedCombineCodes containsObject:uniqueBrandCode]){
            [userOwnedCombineCodes addObject: uniqueBrandCode];
        }
        
        if(![userOwnedCountryCodes containsObject:countryCode]){
            [userOwnedCountryCodes addObject: countryCode];
            if(userPreferCountryCode!=nil && [userPreferCountryCode isEqualToString:countryCode]){
                existingPreferCountryCodeExist = YES;
            }
        }
        if(![userOwnedBrandCodes containsObject:acctCode]){
            [userOwnedBrandCodes addObject: acctCode];
            [userOwnedBrandIds addObject: acctId];
            if(userPreferBrandCode!=nil && [userPreferBrandCode isEqualToString:acctCode]){
                existingPreferBrandCodeExist = YES;
            }
        }
    
    }
    if(!existingPreferBrandCodeExist){
        if([userOwnedBrandCodes count]>0){
            userPreferBrandCode = userOwnedBrandCodes[0];
            userPreferBrandId = userOwnedBrandIds[0];
            [defaults setObject:userPreferBrandCode forKey: USER_PREFER_ACCOUNT_CODE];
            [defaults setObject:userPreferBrandId forKey: USER_PREFER_ACCOUNT_ID ];
        }
    }
    if(!existingPreferCountryCodeExist){
        if([userOwnedCountryCodes count]>0){
            userPreferCountryCode = userOwnedCountryCodes[0];
            [defaults setObject:userPreferCountryCode forKey: USER_PREFER_COUNTRY_CODE];
        }
    }
    [defaults setObject:userOwnedCombineCodes forKey: AVAIL_BRAND_UNIQUE_CODES];
    [defaults setObject:userOwnedCountryCodes forKey: AVAIL_COUNTRY_CODES];
    [defaults setObject:userOwnedBrandCodes forKey: AVAIL_ACCOUNT_CODES];
    [defaults setObject:userOwnedBrandIds forKey: AVAIL_ACCOUNT_IDS];
    [defaults synchronize];
    
    
    
    NSString* userName = [data objectForKey:@"user_name"];
    NSString* name = [data objectForKey:@"name"];
    
    [defaults setObject:name forKey:@"name"];
    [defaults setObject:userName forKey:@"userName"];
    
    NSArray *questionTypeArray = data[@"question_type"];
    NSArray *taskTypeArray = data[@"task_type"];
    
    NSDictionary* userMap = data[@"user_map"];
    if(userMap==nil){
        userMap = @{};
    }
    
    NSArray *roles = data[@"all_user_roles"];
    NSString *userDevRoleName = data[@"user_role_dev_name"];
    NSString *userRoleName = data[@"user_role_name"];
    if(userDevRoleName==nil || userDevRoleName==(id)[NSNull null]){
        userDevRoleName = @"DS";
    }
    if(userRoleName==nil || userRoleName==(id)[NSNull null]){
        userRoleName = @"DS";
    }
    //user role will be admin if null
    
    NSMutableArray *userVisibleDataLevels;
    if( roles==nil){
        roles = @[];
        //Possible roles: DS > RRM > NRM > GM > VP
        userVisibleDataLevels = [ @[@"Store"] mutableCopy];//Store > DS > RRM > NRM > GM > VP
    }
    else{
        
        NSInteger recordedLevelCount = 0;
        userVisibleDataLevels = [NSMutableArray array];
        for(NSInteger i=[roles count]-1; i>=0; i-- ){
            NSDictionary* r = roles[i];
            if ( [r[@"DeveloperName"] isEqualToString: userRoleName] ){
                recordedLevelCount = 1;
            }
            else if(recordedLevelCount>0){
                recordedLevelCount++;
                [userVisibleDataLevels addObject: r[@"DeveloperName"]];
            }
        }
        if([userVisibleDataLevels count]<=1){
            [userVisibleDataLevels addObject:@"Store"];
        }
    }
    
    
    VFDAO* vf = [VFDAO sharedInstance];
    NSMutableDictionary* cache = vf.cache;
    
    //Set FXDate here
    //Prepare a key with YYYY-M-D format
    [cache setObject:[data objectForKey:@"fxdate"] forKey:@"fxdate"]; //Lastest Financial Date
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSString* fxdateStr = [data objectForKey:@"fxdate"];
    NSDate* fxdate = [df dateFromString: fxdateStr];
    [df setDateFormat:@"yyyy-M-d"];
    fxdateStr = [df stringFromDate:fxdate];
    [cache setObject:fxdateStr forKey:@"fxdateYYYYMD"]; //Lastest Financial Date
    
    [cache setObject:[data objectForKey:@"fxweek"] forKey:@"fxweek"];
    [cache setObject:[data objectForKey:@"fxmonth"] forKey:@"fxmonth"];
    [cache setObject:[data objectForKey:@"fxyear"] forKey:@"fxyear"];
    
    
    //User And Role
    [cache setObject: roles forKey:@"all_user_roles"];
    [cache setObject: userDevRoleName forKey:@"user_role_dev_name"];
    [cache setObject: userRoleName forKey:@"user_role_name"];
    [cache setObject: name forKey:@"user_display_name"];
    [cache setObject: userName forKey:@"user_name"];
    [cache setObject: userVisibleDataLevels forKey:@"visible_data_level"];
    
    //full user list
    //[defaults setObject: userList forKey:@"user_list"];
    [cache setObject: userMap forKey:@"user_map"];
    
    NSString* commentRecordId = @"";
    NSString* todoRecordId = @"";
    NSString* docRecordId = @"";
    for(NSDictionary* r in taskTypeArray){
        if([[r objectForKey:@"Name"] isEqualToString:@"Comment"]){
            commentRecordId = [r objectForKey:@"Id"];
        }
        else if([[r objectForKey:@"Name"] isEqualToString:@"ToDo"]){
            todoRecordId = [r objectForKey:@"Id"];
        }
        else if([[r objectForKey:@"Name"] isEqualToString:@"Document"]){
            docRecordId = [r objectForKey:@"Id"];
        }
    }
    taskTypeArray = nil;
    
    [cache setObject: questionTypeArray forKey:@"question_type"];
    [cache setObject: commentRecordId forKey:@"record_type_comment"];
    [cache setObject: todoRecordId forKey:@"record_type_todo"];
    [cache setObject: docRecordId forKey:@"record_type_doc"];

//    NSArray *currentWeekDates = [data objectForKey:@"currentWeekDates"];//list of dictionary{month/week/year}
//    NSArray *currentMonthWeeks = [data objectForKey:@"currentMonthWeeks"];
//    if(currentWeekDates==nil){
//        currentWeekDates = @[];
//    }
//    if(currentMonthWeeks==nil){
//        currentMonthWeeks = @[];
//    }
//    
//    [cache setObject:currentWeekDates forKey:@"current_week_dates"];
//    [cache setObject:currentMonthWeeks forKey:@"current_month_weeks"];
}



-(void) handleQueryBrandDetailRequest:(NSArray*)response forCompStatus:(NSString*)compStatus{
    NSDictionary* brandData =  [response firstObject];
    
    VFDAO* app = [VFDAO sharedInstance];
    NSMutableDictionary* cache = app.cache;
    
    
    NSDictionary *summaryDayMap = [brandData objectForKey:@"summary_data"];
    NSDictionary *storeMap = [brandData objectForKey:@"store_data"];
    NSDictionary *existStoreMap = [cache objectForKey:@"store_daily_analyse"];
    NSMutableDictionary *newSummaryMap;
    NSMutableDictionary *newStoreMap;
    NSDictionary *existSummaryDayMap;
    
    
    if (compStatus!=nil && ![compStatus isEqualToString:@""]){
        existSummaryDayMap = [cache objectForKey:@"comp_summary_daily_analyse"];
    }else{
        existSummaryDayMap = [cache objectForKey:@"summary_daily_analyse"];
    }
    if(existSummaryDayMap==nil){
        newSummaryMap = [NSMutableDictionary dictionary];
    }else{
        newSummaryMap = [existSummaryDayMap mutableCopy];
    }
    if(existStoreMap==nil){
        newStoreMap = [NSMutableDictionary dictionary];
    }else{
        newStoreMap = [existStoreMap mutableCopy];
    }
    [newStoreMap addEntriesFromDictionary:storeMap];
    [cache setObject: newStoreMap forKey:@"store_daily_analyse"];
    
    if (compStatus!=nil && ![compStatus isEqualToString:@""]){
        [newSummaryMap addEntriesFromDictionary:summaryDayMap];
        [cache setObject: newSummaryMap forKey:@"comp_summary_daily_analyse"];
        return;
    }else{
        [newSummaryMap addEntriesFromDictionary:summaryDayMap];
        [cache setObject: newSummaryMap forKey:@"summary_daily_analyse"];
    }
    
    NSArray* storeDailyDataArray = [newStoreMap allValues];
    NSMutableArray* storeSearchArray = [NSMutableArray array];
    
//    if(self.compStatusSet == nil || self.compStatusArray == nil){
//        self.compStatusSet = [NSMutableSet set];
//        self.compStatusArray = [NSMutableArray array];
//         [self.compStatusArray addObject:@{ @"label":@"All", @"val":@"" }];
//    }
    self.compStatusArray = [ @[ @{@"label":[ShareLocale textFromKey:@"all"], @"val":@""}, @{@"label":@"Comp Stores", @"val":@"30-COMP"} ] mutableCopy];
    
    for(NSDictionary* store in storeDailyDataArray){
        
        NSString* brandCode = [store objectForKey:@"account_code"];
        if(brandCode==nil){
            brandCode = @"TBL";
        }
        NSString* countryCode = [store objectForKey:@"country_code"];
        if(countryCode==nil){
            countryCode = @"US";
        }
        
        [storeSearchArray addObject:@{
                                      @"Id": [store objectForKey:@"store_id"],
                                      @"emfa__Store_Code__c": [store objectForKey:@"code"],
                                      @"Name": [store objectForKey:@"name"],
                                      @"account_code": brandCode,
                                      @"country_code": countryCode,
                                      }];
        
//        id type = store[@"comp_status"];
//        if(type!=(id)[NSNull null] && type!=nil && ![type isEqualToString:@""] && ![self.compStatusSet containsObject:type]){
//            [self.compStatusSet addObject:type];
//            [self.compStatusArray addObject:@{@"label":type, @"val":type}];
//        }
    }
    [cache setObject:[storeSearchArray sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSString *first = [(NSDictionary*)a objectForKey:@"emfa__Store_Code__c"];
        NSString *second = [(NSDictionary*)b objectForKey:@"emfa__Store_Code__c"];
        return [first compare:second];
    }] forKey:@"store_search_list"];
    
    [self.messageRecordArray removeAllObjects];
    

}



-(void)daoRequest:(DAOBaseRequest*)request queryOnlineDataSuccess:(NSArray*) response{

    if ( self.queryBrandListRequest == request){ //AppInitRestAPI
        self.queryBrandListRequest = nil;
        [self handleQueryBrandListRequest:response];
        [self loadBrandDetailDataWithCompStatus:@""];
        
    }else if (self.queryBrandDetaliRequest == request){
        
        
        if([self.waitQueryCompAccountArray count] == [self.waitQueryAccountArray count]){
            [self.waitQueryAccountArray removeLastObject];
            [self handleQueryBrandDetailRequest:response forCompStatus:@""];
            [self loadBrandDetailDataWithCompStatus:@"30-COMP"];
            return;
        }else{
            [self.waitQueryCompAccountArray removeLastObject];
            [self handleQueryBrandDetailRequest:response forCompStatus:@"30-COMP"];
            if([self.waitQueryAccountArray count]>0){
                [self loadBrandDetailDataWithCompStatus:@""];
                return;
            }else{
//                [self loadTargets];
                [self refreshDashboard];
                [self loadSurveyMaster];
            }
        }
    }
    else if (self.queryTargetsRequest == request){
        VFDAO* vf = [VFDAO sharedInstance];
        NSMutableDictionary* defaults = vf.cache;
        NSArray *storeTargets = response;
        [defaults setObject: storeTargets forKey:@"targets"];
//        [self refreshDashboard];

        [self loadCapacity];
    }
    else if (self.queryCapacityRequest == request){
        VFDAO* vf = [VFDAO sharedInstance];
        NSMutableDictionary* defaults = vf.cache;
        NSArray *capacityArray = response;
        [defaults setObject: capacityArray forKey:@"capacity"];
        [self loadSurveyMaster];
    }
    
    else if (self.querySurveyMasterRequest == request){
        VFDAO* vf = [VFDAO sharedInstance];
        NSMutableDictionary* defaults = vf.cache;
        [defaults setObject: response forKey:@"survey_master"];

        [self loadNotifications];
    }
    
    else if (self.queryNotificationRequest == request){
        NSArray *messageArray = response;
        [self.messageRecordArray removeAllObjects];
        [self.messageRecordArray addObjectsFromArray:messageArray];

        [self.marqueeView reloadMessageMarqueeViewData];
        [self.commentSidebar selfBinding];
        self.commentSidebar.selectedStoreId = nil;
        [self.commentSidebar loadStoreComments];
        [self hideLoadingPopupWithFadeout];
    }
}


//Editing here to fix eddie_li account can not run bug!





-(void)daoRequest:(DAOBaseRequest*)request queryOnlineDataError:(NSString*)error code:(int)code{
    [self hideLoadingPopupWithFadeout];
    
    NSString* errorMessage;
    if(code == NETWORK_ERROR_CODE_1009){
        errorMessage = [NSString stringWithFormat:@"%@ %@", [ShareLocale textFromKey:@"error_1009"], @"Try again?" ];
    }else if(code == NETWORK_ERROR_CODE_1003){
        errorMessage = [NSString stringWithFormat:@"%@ %@", [ShareLocale textFromKey:@"error_1003"], @"Try again?" ];
    }else if(code == NETWORK_ERROR_CODE_1001){
        errorMessage = [NSString stringWithFormat:@"%@ %@", [ShareLocale textFromKey:@"error_1001"], @"Try again?" ];
    }else if(code == NETWORK_ERROR_CODE_1005){
        errorMessage = [NSString stringWithFormat:@"%@ %@", [ShareLocale textFromKey:@"error_1005"], @"Try again?" ];
    }else{
        errorMessage = [NSString stringWithFormat: [ShareLocale textFromKey:@"error_unknown"], error];
    }
    
    [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:[ShareLocale textFromKey:@"error"] message:errorMessage yesHanlder:^(id data) {
        [self loadBrandList];
    } noHandler:^(id data) {
        
    }];
}



#pragma mark - Message Table view delegate
-(float) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NewsDetailViewController* vc = [[[NewsDetailViewController alloc] initWithData:[self.messageRecordArray objectAtIndex:indexPath.row]] autorelease];
    [self.navigationController pushViewController:vc animated:YES];
    [tableView deselectRowAtIndexPath: indexPath animated:NO];

}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MessageCell"];
    if(cell == nil){
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MessageCell"] autorelease];
        cell.textLabel.hidden = YES;
        cell.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        UIView* bgView = [[[UIView alloc] init] autorelease];
        bgView.backgroundColor = [UIColor colorWithWhite:1 alpha:0.1];
        cell.selectedBackgroundView = bgView;
    
        [self createNewTextLabelWithDefaultStyle: CGRectMake(10, 5, 580, 40) parent:cell tag: 901];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(400, 5, 140, 40) parent:cell tag: 902];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(560, 5, 180, 40) parent:cell tag: 903];
        
    }

    cell.tag = indexPath.row;

    NSDictionary* data = [self.messageRecordArray objectAtIndex: indexPath.row];

    UILabel* txtView1 = (UILabel*)[cell viewWithTag:901];
    txtView1.text = [self getFieldDisplayWithFieldName:@"Name" withDictionary: data];
    UILabel* txtView2 = (UILabel*)[cell viewWithTag:902];
//    txtView2.text = @"All Staff";
    txtView2.numberOfLines = 2;
    [txtView2 sizeToFit];
    CGRect rect = txtView2.frame;
    rect.origin.y = (50 - rect.size.height)/2;
    txtView2.frame = rect;

    UILabel* txtView3 = (UILabel*)[cell viewWithTag:903];
    NSString* pdateStr = [self getFieldDisplayWithFieldName:@"emfa__Published_Start_Date__c" withDictionary: data];
    NSDate* pdate = [[GeneralUiTool getSFDateFormatter] dateFromString: pdateStr];
    NSDateFormatter* df = [[[NSDateFormatter alloc] init] autorelease];
    [df setDateStyle:NSDateFormatterShortStyle];
    [df setTimeStyle:NSDateFormatterShortStyle];
    txtView3.text =  [df stringFromDate: pdate];
    txtView3.numberOfLines = 2;
    [txtView3 sizeToFit];
    CGRect rect3 = txtView3.frame;
    rect3.origin.y = (50 - rect3.size.height)/2;
    txtView3.frame = rect3;

    return cell;
}


-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.messageRecordArray count];
}



#pragma mark - Others
-(void) gotoVisitResultViewController{
	SurveyListViewController *vc = [[SurveyListViewController alloc] initWithType:SURVEY_TYPE_VISIT_FORM title: [ShareLocale textFromKey:@"surveytitle_visit"] mode:SurveyListModeSurveyHistory];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void) gotoSOPResultViewController{
	SurveyListViewController *vc = [[SurveyListViewController alloc] initWithType:SURVEY_TYPE_SOP_Audit title: [ShareLocale textFromKey:@"surveytitle_audit"] mode:SurveyListModeSurveyHistory];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void) gotoMSPResultViewController{
	SurveyListViewController *vc = [[SurveyListViewController alloc] initWithType:SURVEY_TYPE_MSP_SURVEY title: [ShareLocale textFromKey:@"surveytitle_msp"] mode:SurveyListModeSurveyHistory];
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - Marquee Delegate
-(NSInteger)numberOfRowForMarqueeView:(UIView *)mview {
    return [self.messageRecordArray count];
}

-(UIView*)marqueeView:(UIView *)mview viewAtRow:(int)index{
    NSString* title = [[self.messageRecordArray objectAtIndex:index] objectForKey:@"Name"];
    NSString* pdateStr = [self getFieldDisplayWithFieldName:@"emfa__Published_Start_Date__c" withDictionary: [self.messageRecordArray objectAtIndex:index]];
    NSDate* pdate = [[GeneralUiTool getSFDateFormatter] dateFromString: pdateStr];
    NSDateFormatter* df = [[[NSDateFormatter alloc] init] autorelease];
    [df setDateStyle:NSDateFormatterShortStyle];
    [df setTimeStyle:NSDateFormatterShortStyle];
    
    
    UIView* rowView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, mview.frame.size.width - 80, 30)];
    
    UILabel* datelabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 150, 30)];
    datelabel.font = [UIFont fontWithName:@"Arial" size:14.0f];
    datelabel.text = [NSString stringWithFormat:@"%@", [df stringFromDate: pdate]];
    datelabel.textColor = [UIColor whiteColor];
    
    UILabel* titlelabel = [[UILabel alloc] initWithFrame:CGRectMake(10+150, 0, mview.frame.size.width - 80 - 150, 30)];
    titlelabel.font = [UIFont fontWithName:@"Arial" size:14.0f];
    titlelabel.text = [NSString stringWithFormat:@"%@", title];
    titlelabel.textColor = [UIColor whiteColor];
    
    [rowView addSubview:titlelabel];
    [rowView addSubview:datelabel];
    
    return rowView;
}

-(void)marqueeView:(UIView*)mview didClicked:(int)index{
    UIViewController* vc = [[NewsDetailViewController alloc] initWithData: [self.messageRecordArray objectAtIndex:index]] ;
    [self.navigationController pushViewController:vc animated:YES];
    [vc release];
}



#pragma mark - Online/Offline Mode Control Method



- (IBAction)onSyncButtonClicked:(id)sender{
    
    VFDAO* dao = [VFDAO sharedInstance];
    
    if([dao isOnlineMode]){
        if(dao.isNetworkReachable){
            [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:[ShareLocale textFromKey:@"confirmation"] message:[ShareLocale textFromKey:@"dialog_txt_reload_data"] yesHanlder:^(id data) {
                
//                    [self startUploadProcessWithCompleteBlock:^(BOOL complete) {
//                        [dao isOnlineMode:YES];
//                }];
                
                
                [self showLoadingPopup:self.view ];
                [self startDownloadProcessWithCompleteBlock:^(BOOL complete) {
                    if(complete){
                        [self refreshNetworkStatusUI];
                    }
                } offlineOption: 100];//only refresh basic data

                
            } noHandler:^(id data) {
                
            }];
        }
        else{
            [[GeneralUiTool shareInstance] showConfirmDialogWithTitle: [ShareLocale textFromKey:@"confirmation"]  message:[ShareLocale textFromKey:@"dialog_offline_suggest"] yesHanlder:^(id data) {
                [dao isOnlineMode: NO];
                [self refreshNetworkStatusUI];

            } noHandler:^(id data) {
                
            }];
        }
    }else{
        //in offline mode
        [[GeneralUiTool shareInstance] showConfirmDialogWithTitle: [ShareLocale textFromKey:@"sorry"]  message:[ShareLocale textFromKey:@"dialog_reload_denied"] yesHanlder:^(id data) {
        }];
    }
    
    
    
}




-(void) removeOfflinePopup{
    UIView* v = [self.offlinePopup superview];
    [self.offlinePopup removeFromSuperview];
    [v removeFromSuperview];
    self.offlinePopup = nil;
}


-(void) onClickOfflinePopupButton:(UIView*)button{
    
    NSLog(@"onClickOfflinePopupButton %@", button);

    [self removeOfflinePopup];
    
    if(button.tag == 73804){
        //Offline_Popup Click Cancel

    }
    else if (button.tag == 73801 || button.tag == 73802){
        //Offline_Popup Click Full Download
        VFDAO* dao = [VFDAO sharedInstance];

        if(dao.isNetworkReachable){
            //network available, sync all before switching
            
            int offlineOption = (button.tag == 73801)?0:100;
            
            [self showLoadingPopup:self.view ];
            [self startDownloadProcessWithCompleteBlock:^(BOOL complete) {
                if(complete){
                    [dao isOnlineMode: NO];
                    [self refreshNetworkStatusUI];
                }else{
                }
            } offlineOption: offlineOption];
        }else{
            //no network available, stop switching
            [[GeneralUiTool shareInstance] showConfirmDialogWithTitle: [ShareLocale textFromKey:@"error"]  message:[ShareLocale textFromKey:@"dialog_change_offline_fail"] yesHanlder:^(id data) {
            }];
        }
    }
    else{
        //Offline_Popup Click SKIP Download
        VFDAO* dao = [VFDAO sharedInstance];
        [dao isOnlineMode: NO];
        [self refreshNetworkStatusUI];
        
        [[GeneralUiTool shareInstance] showConfirmDialogWithTitle: @""  message:[ShareLocale textFromKey:@"dialog_offline_skip_warning"] yesHanlder:^(id data) {
            
        }];
    }
    
}

- (IBAction)onGlobalButtonClicked:(id)sender {
    VFDAO* dao = [VFDAO sharedInstance];
    
    if([dao isOnlineMode]){
        
        UIView* bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1024, 768)];
        bgView.backgroundColor = [UIColor colorWithWhite:0 alpha:.8f];
        self.offlinePopup = [[[NSBundle mainBundle] loadNibNamed:@"OfflinePromptPopupView" owner:self options:nil] firstObject];
        self.offlinePopup.frame = CGRectMake(1024/2 - self.offlinePopup.frame.size.width/2, 768/2 - self.offlinePopup.frame.size.height/2, self.offlinePopup.frame.size.width, self.offlinePopup.frame.size.height);
        [self.offlinePopup initButtonBindingTo:self];
        [self.view addSubview: bgView];
        [bgView addSubview:self.offlinePopup];
        
//        [[GeneralUiTool shareInstance] showConfirmDialogWithTitle: [ShareLocale textFromKey:@"confirmation"]  message:[ShareLocale textFromKey:@"dialog_changed_offline_prompt"] yesHanlder:^(id data) {
//            
//            if(dao.isNetworkReachable){
//                //network available, sync all before switching
//                [self showLoadingPopup:self.view ];
//                [self startDownloadProcessWithCompleteBlock:^(BOOL complete) {
//                    if(complete){
//                        [dao isOnlineMode: NO];
//                        [self refreshNetworkStatusUI];
//                    }else{
//                    }
//                }];
//            }else{
//                //no network available, stop switching
//                [[GeneralUiTool shareInstance] showConfirmDialogWithTitle: [ShareLocale textFromKey:@"error"]  message:[ShareLocale textFromKey:@"dialog_change_offline_fail"] yesHanlder:^(id data) {
//                }];
//            }
//        } noHandler:^(id data) {
//            
//        }];
        
    }else{
        //offline mode
        
        if(dao.isNetworkReachable){
            [[GeneralUiTool shareInstance] showConfirmDialogWithTitle: [ShareLocale textFromKey:@"confirmation"]  message:[ShareLocale textFromKey:@"dialog_changed_online_prompt"] yesHanlder:^(id data) {

                //self.lastOnlineMode = [dao isOnlineMode];
                [self startUploadProcessWithCompleteBlock:^(BOOL complete) {
                    if(complete){
                        [dao isOnlineMode: YES];
                    }else{
                        [dao isOnlineMode: NO];
                    }
                    [self refreshNetworkStatusUI];
                }];
                
                
            } noHandler:^(id data) {
                
            }];
        }
        else{
            //no network found
            [[GeneralUiTool shareInstance] showConfirmDialogWithTitle: [ShareLocale textFromKey:@"error"]  message:[ShareLocale textFromKey:@"dialog_change_online_fail"] yesHanlder:^(id data) {
            }];
        }
        
    }
}


#pragma mark - Sync
-(void) startUploadProcessWithCompleteBlock:(void (^)(BOOL complete))completeBlock{
    [self showLoadingPopup:self.view text:[ShareLocale textFromKey:@"uploading..."]];
    
    [[VFDAO sharedInstance] isOnlineMode:YES];
    
    [[OfflineSyncHelper sharedInstance] startUploadDataWithCompleteBlock:^(BOOL success) {
        if(success){
            //[self startDownloadProcessWithCompleteBlock:completeBlock offlineOption:0];
            [self hideLoadingPopup];
            [[VFDAO sharedInstance] isOnlineMode:YES];
            completeBlock(YES);
            [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"" message:[ShareLocale textFromKey:@"dialog_process_complete"] yesHanlder:^(id data) {
                
            }];
        }else{
            [[VFDAO sharedInstance] isOnlineMode:NO];
            [self hideLoadingPopup];
            completeBlock(NO);
        }
    }];
    
}


-(void) startDownloadProcessWithCompleteBlock:(void (^)(BOOL complete))completeBlock offlineOption:(int)index{
    OfflineSyncHelper* syncMgr =  [OfflineSyncHelper sharedInstance];
    syncMgr.offlineDownloadOption = index;
    [syncMgr startDownloadWithCompleteBlock:^(BOOL success) {
        NSLog(@"Sync data from server to local done!");
        [self hideLoadingPopup];
        if(success){
            [[VFDAO sharedInstance] isOnlineMode:YES];
            completeBlock(YES);
            [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"" message:[ShareLocale textFromKey:@"dialog_process_complete"] yesHanlder:^(id data) {
                
            }];
        }
        else{
            [[VFDAO sharedInstance] isOnlineMode:NO];
            completeBlock(NO);
            [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"" message:[ShareLocale textFromKey:@"dialog_process_fail"]];
        }
    } updateBlock:^(DAOBaseRequest* req, NSArray* response, NSString* text){
        if(response!=nil && req!=nil ){
            if( [req.name isEqualToString:REST_API_APP_INIT_GET] ){
                NSDictionary* resData = [response firstObject];
                if(resData!=nil){
                    if(resData[@"user_name"]!=nil && resData[@"user_name"]!=(id)[NSNull null]){
                        [self handleQueryBrandListRequest:response];
                    }else{
                        [self handleQueryBrandDetailRequest:response forCompStatus:@""];
                    }
                }
                
            }
            else if ( [req.name rangeOfString:@"FROM Store_Capacity__c"].location != NSNotFound ){
                VFDAO* vf = [VFDAO sharedInstance];
                NSMutableDictionary* defaults = vf.cache;
                [defaults setObject: response forKey:@"capacity"];
            }
            else if ( [req.name rangeOfString:@"FROM Store_Performance_Target__c"].location != NSNotFound ){
                VFDAO* vf = [VFDAO sharedInstance];
                NSMutableDictionary* defaults = vf.cache;
                [defaults setObject: response forKey:@"targets"];
                [self refreshDashboard];
            }
            else if ( [req.name rangeOfString:@"FROM emfa__Survey_Master__c"].location != NSNotFound ){
                //FIXME: It load 3 times, can it load for one time?
                VFDAO* vf = [VFDAO sharedInstance];
                NSMutableDictionary* defaults = vf.cache;
                [defaults setObject: response forKey:@"survey_master"];
            }
            else if ( [req.name rangeOfString:@"FROM emfa__Notification__c"].location != NSNotFound ){
                [self.messageRecordArray removeAllObjects];
                [self.messageRecordArray addObjectsFromArray: response];
                
                [self.marqueeView reloadMessageMarqueeViewData];
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    [self.marqueeView reloadMessageMarqueeViewData];
                    [self.commentSidebar selfBinding];
                    self.commentSidebar.selectedStoreId = nil;
                    //[self.commentSidebar loadStoreComments];
                });
            }
        }
        if(text!=nil) [self changeLoadingPopupText: text];
    }];
}



@end
