//
//  ProductListViewController.m
//  VFStorePerformanceApp
//
//  Created by Developer 2 on 3/3/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "ProductListViewController.h"
#import "ProductDetailViewController.h"
#import "PDFViewController.h"
#import "ImagePreviewViewController.h"
#import "GeneralSelectionPopup.h"
#import "GeneralUiTool.h"
#import "Constants.h"
#import "ShareLocale.h"

@interface ProductListViewController ()

@end

@implementation ProductListViewController

-(id) initWithBrandCode:(NSString*)brandId
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        self.brandCode = brandId;
        self.selectedCategory = nil;
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [_productsContainer release];
    [_tableView release];
    [_headerTitleLabel release];
    [_fileNameLabel release];
    [_createdDateLabel release];
    [_categoryLabel release];
    [_fileTypeLabel release];
    [_filterButton release];
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.recordAry = [NSMutableArray array];
    
    self.tableView.delegate = self;
    self.tableView.backgroundColor = TABLE_BG_COLOR_BLUE;
    self.tableView.backgroundView = nil;
    self.tableView.dataSource = self;
    
    self.headerTitleLabel.text = [NSString stringWithFormat:[ShareLocale textFromKey:@"knowledge_title"], self.brandCode];
    [self.fileNameLabel setTitle: [ShareLocale textFromKey:@"knowledge_filename"] forState:UIControlStateNormal];
    [self.categoryLabel setTitle: [ShareLocale textFromKey:@"knowledge_category"] forState:UIControlStateNormal];
    [self.fileTypeLabel setTitle: [ShareLocale textFromKey:@"knowledge_filetype"] forState:UIControlStateNormal];
    [self.createdDateLabel setTitle: [ShareLocale textFromKey:@"knowledge_createdate"] forState:UIControlStateNormal];
    
    [self.filterButton setTitle:[ShareLocale textFromKey:@"all"] forState:UIControlStateNormal];
    
    [self loadData];
    
    
}

#pragma mark - Network data from SFDC


-(void) loadData{
    [self showLoadingPopup: self.view];
    
    self.queryKnowledgeRequest = [[VFDAO sharedInstance] selectKnowledgeResourcesWithDelegate:self brandCode: self.brandCode];

}

-(void)daoRequest:(DAOBaseRequest*)request queryOnlineDataSuccess:(NSArray*) response{
    if(self.queryKnowledgeRequest == request){
        self.queryKnowledgeRequest = nil;

        [self hideLoadingPopup];
        
        self.allRecordAry = [NSArray arrayWithArray:response];
        
        [self refreshTableRecordWithSelectedCategory];
    }
}
    
-(void) refreshTableRecordWithSelectedCategory{
    [self.recordAry removeAllObjects];
    
    self.categoryArray = [NSMutableArray arrayWithArray:@[@{@"label":[ShareLocale textFromKey:@"all"], @"value":@""}]];
    NSMutableSet* myset = [NSMutableSet set];
    for(NSDictionary* d in self.allRecordAry){
        [myset addObject: @{@"label":[ShareLocale textFromKey:[d objectForKey:@"Category__c"]], @"value":[d objectForKey:@"Category__c"]}];
        if(self.selectedCategory==nil || [self.selectedCategory isEqualToString:@""] || [self.selectedCategory isEqualToString:[d objectForKey:@"Category__c"]]){
            [self.recordAry addObject:d];
        }
    }
    [self.categoryArray addObjectsFromArray: [myset allObjects] ];
    [self.tableView reloadData];
}


-(void)daoRequest:(DAOBaseRequest*)request queryOnlineDataError:(NSString*) response code:(int)code{
    [self hideLoadingPopup];
    [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"Error" message:[ NSString stringWithFormat:@"Error Code: %d. Fail to load Knowledge & Document due to %@", code, response ]];
}


#pragma mark - Open as
- (UIDocumentInteractionController *)controller {
    
    if (!_controller) {
        _controller = [[UIDocumentInteractionController alloc]init];
        _controller.delegate = self;
    }
    return _controller;
}

-(void) openDocument:(NSURL*) documentURL {

    self.controller.URL = documentURL;
    [self.controller presentPreviewAnimated:YES];
}




#pragma mark - TableView

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"KnowledgeDocument"];
    if(cell == nil){
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"KnowledgeDocument"] autorelease];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(0+10, 0, 450, 50) parent:cell tag: 901];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(460, 0, 200, 50) parent:cell tag: 904];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(680, 0, 120, 50) parent:cell tag: 902];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(800, 0, 220, 50) parent:cell tag: 903];
    }
    NSDictionary* data = [self.recordAry objectAtIndex:indexPath.row];
    
    UILabel* nameLabel = (UILabel*)[cell viewWithTag:901];
    
    //SELECT Id, emfa__FeedItemId__c, emfa__File_Name__c, emfa__File_Ext__c FROM Shared_Media__r
    UILabel* typeLabel = (UILabel*)[cell viewWithTag:902];
    typeLabel.text = @"N/A";
    UILabel* catLabel = (UILabel*)[cell viewWithTag:904];
    if([data objectForKey:@"Shared_Media__r"]!=(id)[NSNull null]){
        NSArray* medias = [[data objectForKey:@"Shared_Media__r"] objectForKey:@"records"];
        NSDictionary* d = [medias objectAtIndex:0];
        typeLabel.text = [d objectForKey:@"emfa__File_Ext__c"];
        nameLabel.text = [d objectForKey:@"emfa__File_Name__c"]; // [data objectForKey:@"Name"];
    }
    
    catLabel.text = [ShareLocale textFromKey: [self getFieldDisplayWithFieldName:@"Category__c" withDictionary:data]];
    
    UILabel* dateLabel = (UILabel*)[cell viewWithTag:903];
    
    NSDate* d = [[GeneralUiTool getSFDateFormatter] dateFromString: [data objectForKey:@"CreatedDate"]];
    
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm"];
    dateLabel.text = [df stringFromDate: d];
    
    cell.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
    UIView *selectionColor = [[[UIView alloc] init] autorelease];
    selectionColor.backgroundColor = TABLE_ROW_HIGHLIGHT_COLOR;
    cell.selectedBackgroundView = selectionColor;
    

    
    return  cell;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary* data = [self.recordAry objectAtIndex:indexPath.row];
    
    if([data objectForKey:@"Shared_Media__r"]!=(id)[NSNull null]){
        NSArray* medias = [[data objectForKey:@"Shared_Media__r"] objectForKey:@"records"];
        NSDictionary* media = [medias objectAtIndex:0];
        NSString* fileType = [media objectForKey:@"emfa__File_Ext__c"];
        
        self.selectedFileName = [media objectForKey:@"emfa__File_Name__c"];
        self.selectedFileType = [media objectForKey:@"emfa__File_Ext__c"];
        
//        if([fileType isEqualToString:@"pdf"] || [fileType isEqualToString:@"PDF"]){
//            PDFViewController* vc = [[PDFViewController alloc] initWithFeedItemID: [self getFieldDisplayWithFieldName:@"emfa__FeedItemId__c" withDictionary: media] ];
//            [self.navigationController pushViewController:vc animated:YES];
//        }
//        else if ([fileType isEqualToString:@"png"] || [fileType isEqualToString:@"PNG"] ||
//                  [fileType isEqualToString:@"jpg"] || [fileType isEqualToString:@"JPG"] ||
//                  [fileType isEqualToString:@"jpeg"] || [fileType isEqualToString:@"JPEG"]){
//            ImagePreviewViewController* vc = [[ImagePreviewViewController alloc] initWithFeedItemID: [self getFieldDisplayWithFieldName:@"emfa__FeedItemId__c" withDictionary: media] ];
//            [self.navigationController pushViewController:vc animated:YES];
//            
//        }else{

            [self showLoadingPopup:self.view];
            
            self.selectedTableViewCell = [tableView cellForRowAtIndexPath: indexPath];


            ImageLoader* loader = [ImageLoader shareInstance];
            [loader downloadDocWithFeedID:[self getFieldDisplayWithFieldName:@"emfa__FeedItemId__c" withDictionary: media] type:fileType delegate:self];
            
//        }

    }
}




-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.recordAry count];
}


#pragma mark - Image dleegate


-(void) onRequestSuccessOfRequest:(ImageLoaderRequest *)req feedItemID:(NSString *)feedItemId response:(id)jsonResponse contentType:(NSString *)contentType{
    [self hideLoadingPopup];
    
    __block ProductListViewController* me = self;

    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *storePath = [applicationDocumentsDir stringByAppendingPathComponent:
                               [NSString stringWithFormat:@"%@.%@", feedItemId, contentType]];
        
        NSURL* url = [NSURL fileURLWithPath:storePath];
        
        NSLog(@"url=%@", url);
        
        me.documentController = [ UIDocumentInteractionController interactionControllerWithURL: url];
        
        me.documentController.name = me.selectedFileName;

        CGRect cellFrame = me.selectedTableViewCell.frame;
        me.selectedTableViewCell = nil;
        
        BOOL done = [ me.documentController presentOpenInMenuFromRect: cellFrame inView: me.tableView animated: YES ];
        NSLog(@"done %@", done?@"Y":@"N");
    });
    
}






-(void) onRequestFailOfRequest:(ImageLoaderRequest *)req feedItemID:(NSString *)feedItemId error:(NSError *)error{
    [self hideLoadingPopup];

    [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"" message:[NSString stringWithFormat:@"Fail to load File due to: %@", error] ];
    

}


#pragma mark - Filter popup delegate
-(void) onRowSelected:(int) index{
    if(self.filterPopup!=nil){
        
        NSDictionary* data = [self.categoryArray objectAtIndex:index];
        
        self.selectedCategory = [data objectForKey:@"value"];
        
        if(self.selectedCategory==(id)[NSNull null] || [self.selectedCategory isEqualToString:@""]){
            [self.filterButton setTitle: [ShareLocale textFromKey:@"all"] forState:UIControlStateNormal];
        }
        else{
            [self.filterButton setTitle: [ShareLocale textFromKey:self.selectedCategory] forState:UIControlStateNormal];
        }
        
        [self.filterPopup dismissPopoverAnimated:YES];
        self.filterPopup = nil;

        [self refreshTableRecordWithSelectedCategory];
        
    }
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    self.filterPopup = nil;
}



#pragma mark - Button delegate

- (IBAction) onBackButtonClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onHomeButtonClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)onFilterButtonClicked:(id)sender {
    if(self.filterPopup==nil){
        UIView* view = (UIView*) sender;
        GeneralSelectionPopup* vc = [[GeneralSelectionPopup alloc ] initWithArray: self.categoryArray];
        vc.delegate = self;
        self.filterPopup = [[[UIPopoverController alloc] initWithContentViewController:vc] autorelease];
        self.filterPopup.delegate = self;
        [self.filterPopup presentPopoverFromRect:view.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
    }

}
@end
