//
//  ProductCatalogBrandViewController.h
//  VFStorePerformanceApp
//
//  Created by Developer 2 on 3/3/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"

@interface ProductCatalogBrandViewController : BasicViewController<UITableViewDataSource, UITableViewDelegate>

- (IBAction)onHomeButtonClicked:(id)sender;
- (IBAction) onBackButtonClicked:(id)sender;

@property (retain, nonatomic) IBOutlet UIImageView *generalButton;
@property (retain, nonatomic) IBOutlet UIImageView *timberlandButton;
@property (retain, nonatomic) IBOutlet UIImageView *vansButton;
@property (retain, nonatomic) IBOutlet UILabel *headerTitleLabel;

@property (retain, nonatomic) IBOutlet UITableView *brandTableView;

@property (retain, nonatomic) NSArray* brandArray;


@property (retain, nonatomic) IBOutlet UILabel *brandLogoHeader;
@property (retain, nonatomic) IBOutlet UILabel *brandNameHeader;



@end
