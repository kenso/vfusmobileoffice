//
//  ProductDetailViewController.m
//  VFStorePerformanceApp
//
//  Created by Developer 2 on 3/3/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "ProductDetailViewController.h"

@interface ProductDetailViewController ()

@end

@implementation ProductDetailViewController

-(id)initWithProductId:(NSString*)productId
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UITapGestureRecognizer *singleTap;

    singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onRefLinkClicked:)];
    [self.refLinkLabel setUserInteractionEnabled:YES];
    [self.refLinkLabel addGestureRecognizer: singleTap];

    [self.refLinkLabel addGestureRecognizer:singleTap];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction) onBackButtonClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onHomeButtonClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void) onRefLinkClicked:(UIGestureRecognizer *)recognizer{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.refLinkLabel.text]];

}

- (void)dealloc {
    [_refLinkLabel release];
    [super dealloc];
}
@end
