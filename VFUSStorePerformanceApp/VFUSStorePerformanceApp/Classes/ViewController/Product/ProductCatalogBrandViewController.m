//
//  ProductCatalogBrandViewController.m
//  VFStorePerformanceApp
//
//  Created by Developer 2 on 3/3/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "ProductCatalogBrandViewController.h"
#import "ProductListViewController.h"
#import "ShareLocale.h"
#import "Constants.h"

@interface ProductCatalogBrandViewController ()

@end

@implementation ProductCatalogBrandViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.brandArray = @[
                        @{@"Name":@"VF", @"Code":@"All", @"Image_Path":@"vfclogo300.jpg"},
                        @{@"Name":@"Timberland", @"Code":@"TBL", @"Image_Path":@"timberland_black_big.png"},
                        @{@"Name":@"Vans", @"Code":@"VAN", @"Image_Path":@"vans_logo_big.jpg"},
                        @{@"Name":@"Kipling", @"Code":@"KPL", @"Image_Path":@"kipling_logo.jpg"},
                        @{@"Name":@"The North Face", @"Code":@"TNF", @"Image_Path":@"northface_logo.png"},
                        @{@"Name":@"LEE", @"Code":@"LEE", @"Image_Path":@"lee_logo.png"},
                        @{@"Name":@"Wrangler", @"Code":@"WGR", @"Image_Path":@"wrangler_logo.png"},
                        @{@"Name":@"Napa", @"Code":@"NAP", @"Image_Path":@"napa_logo.png"},
                        ];
    
    self.brandTableView.backgroundColor = TABLE_BG_COLOR_BLUE;
    self.brandTableView.backgroundView = nil;
    
    self.brandLogoHeader.text = [ShareLocale textFromKey:@"knowledge_logo"];
    self.brandNameHeader.text = [ShareLocale textFromKey:@"knowledge_name"];
    
//    UITapGestureRecognizer *singleTap;
    
    self.headerTitleLabel.text = [ShareLocale textFromKey:@"menu_knowledge"];

//    singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(noBrandClicked:)];
//    [self.timberlandButton setUserInteractionEnabled:YES];
//    [self.timberlandButton addGestureRecognizer: singleTap];
//    [singleTap release];
//    
//    singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(noBrandClicked:)];
//    [self.vansButton setUserInteractionEnabled:YES];
//    [self.vansButton addGestureRecognizer: singleTap];
//    [singleTap release];
//    
//    singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(noBrandClicked:)];
//    [self.generalButton setUserInteractionEnabled:YES];
//    [self.generalButton addGestureRecognizer: singleTap];
//    [singleTap release];

    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UI Button handler
- (IBAction)onHomeButtonClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction) onBackButtonClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)noBrandClicked:(UIGestureRecognizer *)recognizer
{
    NSInteger rtag = recognizer.view.tag;
    if(rtag == 101){
        [self openProductPageByBrand:@"TBL"];
    }
    else if (rtag == 102){
        [self openProductPageByBrand:@"VAN"];
    }
    else if (rtag == 100){
        [self openProductPageByBrand:@"All"];
    }
    else if (rtag == 103){
        [self openProductPageByBrand:@"KPL"];
    }

}

-(void) openProductPageByBrand:(NSString*) brandCode{
    ProductListViewController* vc = [[ProductListViewController alloc] initWithBrandCode:brandCode];
    [self.navigationController pushViewController: vc animated:YES];
    [vc release];
}



- (void)dealloc {
    [_timberlandButton release];
    [_vansButton release];
    [_headerTitleLabel release];
    [_generalButton release];
    [_brandTableView release];
    [_brandLogoHeader release];
    [_brandNameHeader release];
    [super dealloc];
}


#pragma mark - TableView Delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.brandArray count];
}


-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"KnowledgeDocumentBrandCell"];
    if(cell == nil){
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"KnowledgeDocumentBrandCell"] autorelease];
        UIView* selectedBg = [[UIView alloc] initWithFrame: cell.frame];
        selectedBg.backgroundColor = TABLE_ROW_HIGHLIGHT_COLOR;
        cell.selectedBackgroundView = selectedBg;
        UIImageView* imgView = [[UIImageView alloc] initWithFrame: CGRectMake(10, 10, 200/2, 140/2)];
//        imgView.backgroundColor = [UIColor whiteColor];
        imgView.tag = 902;
        [cell addSubview:imgView];
        cell.backgroundColor = [UIColor colorWithWhite:0 alpha:.0f];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(0+230, 0, 300, 90) parent:cell tag: 901];
    }
    NSDictionary* data = self.brandArray[indexPath.row];
    
//    if(indexPath.row%2==0){
//        cell.backgroundColor = [UIColor colorWithWhite:0 alpha:.08f];
//    }else{
//        cell.backgroundColor = [UIColor colorWithWhite:0 alpha:.0f];
//    }
    
    UIImageView* iconImgView = (UIImageView*)[cell viewWithTag:902];
    iconImgView.frame = CGRectMake(10, 10, 200/2, 140/2);
//    iconImgView.backgroundColor = [UIColor whiteColor];
    UIImage* img = [UIImage imageNamed:data[@"Image_Path"]];
    iconImgView.contentMode = UIViewContentModeScaleAspectFit;
    iconImgView.image = img;
    iconImgView.highlightedImage = img;
    
    UILabel* nameLabel = (UILabel*) [cell viewWithTag:901];
    nameLabel.font = [UIFont fontWithName:@"Arial" size:18];
    nameLabel.text = data[@"Name"];
    return cell;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary* data = self.brandArray[indexPath.row];
    NSString* code = data[@"Code"];
    [self openProductPageByBrand: code];
}


@end
