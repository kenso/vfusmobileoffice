//
//  ProductDetailViewController.h
//  VFStorePerformanceApp
//
//  Created by Developer 2 on 3/3/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "BasicViewController.h"

@interface ProductDetailViewController : BasicViewController


-(id)initWithProductId:(NSString*)productId;

- (IBAction) onBackButtonClicked:(id)sender;
- (IBAction)onHomeButtonClicked:(id)sender;

@property (retain, nonatomic) IBOutlet UILabel *refLinkLabel;

@end
