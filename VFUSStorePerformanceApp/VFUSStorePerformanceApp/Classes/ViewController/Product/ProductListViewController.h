//
//  ProductListViewController.h
//  VFStorePerformanceApp
//
//  Created by Developer 2 on 3/3/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "BasicViewController.h"
#import "ImageLoader.h"
#import "GeneralSelectionPopup.h"

@interface ProductListViewController : BasicViewController<DAODelegate, UITableViewDelegate, UITableViewDataSource, UIDocumentInteractionControllerDelegate, ImageLoaderDelegate, GeneralSelectionPopupDelegate, UIPopoverControllerDelegate>{
    UIDocumentInteractionController* _controller;
}
@property (retain, nonatomic) IBOutlet UIButton *filterButton;
@property (retain, nonatomic) UIPopoverController *filterPopup;


@property (retain, nonatomic) UITableViewCell* selectedTableViewCell;
@property (retain, nonatomic) NSString* selectedFileName;
@property (retain, nonatomic) NSString* selectedFileType;
@property (retain, nonatomic) NSString* selectedCategory;

@property (retain, nonatomic) IBOutlet UIButton *fileNameLabel;
@property (retain, nonatomic) IBOutlet UIButton *createdDateLabel;

@property (retain, nonatomic)  UIDocumentInteractionController *documentController;

@property (retain, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (retain, nonatomic) IBOutlet UIButton *categoryLabel;
@property (retain, nonatomic) IBOutlet UIButton *fileTypeLabel;

@property (retain, nonatomic) IBOutlet UIView *productsContainer;

@property (retain, nonatomic) IBOutlet UITableView *tableView;

@property (retain, nonatomic) NSString* brandCode;

@property (retain, nonatomic) NSMutableArray* categoryArray;


@property (retain, nonatomic) DAOBaseRequest* queryKnowledgeRequest;

@property (retain, nonatomic)NSArray* allRecordAry;

@property (retain, nonatomic)NSMutableArray* recordAry;


-(id) initWithBrandCode:(NSString*)brandId;

- (IBAction) onBackButtonClicked:(id)sender;

- (IBAction)onHomeButtonClicked:(id)sender;

- (IBAction)onFilterButtonClicked:(id)sender;



@end
