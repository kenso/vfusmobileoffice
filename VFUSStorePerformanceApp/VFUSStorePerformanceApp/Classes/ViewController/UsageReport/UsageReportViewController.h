//
//  UsageReportViewController.h
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 12/8/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VFDAO.h"
#import "BasicViewController.h"

@interface UsageReportViewController : BasicViewController<DAODelegate>

@property (retain, nonatomic) IBOutlet UIView *contentView;
@property (retain, nonatomic) IBOutlet UILabel *headerTitleLabel;

- (IBAction)onHomeButtonClicked:(id)sender;

- (IBAction)onBackButtonClicked:(id)sender;


@end
