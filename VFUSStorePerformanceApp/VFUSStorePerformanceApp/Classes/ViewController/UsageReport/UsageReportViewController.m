//
//  UsageReportViewController.m
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 12/8/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "UsageReportViewController.h"
#import "VFDAO.h"
#import "ShareLocale.h"
#import "Constants.h"

@interface UsageReportViewController ()

@end

@implementation UsageReportViewController



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}



- (void)viewDidLoad
{
    
    self.headerTitleLabel.text = [ShareLocale textFromKey:@"usage_title"];
    
    [super viewDidLoad];
    
    [self loadData];

}

- (void) loadData{
    [self showLoadingPopup: self.view];
    [[VFDAO sharedInstance] selectUsageReportWithDelegate:self ];
}


#pragma mark - Delegate method of Network Request
-(void)daoRequest:(DAOBaseRequest*)request queryOnlineDataSuccess:(NSArray*) response{
    NSLog(@"response = %@", response);
    /*
     Sample Response:
     {
         content =         {
         cutToDate = "2014-08-12T06:42:14.478Z";
         monthActualDuration = 0;
         monthCommentCount = 0;
         monthLoginCount = 0;
         monthPlanDuration = 0;
         monthVisitCompleteCount = 0;
         monthVisitCount = 0;
         userId = 00590000002k5TFAAY;
         weekActualDuration = 0;
         weekCommentCount = 0;
         weekLoginCount = 0;
         weekPlanDuration = 0;
         weekVisitCompleteCount = 0;
         weekVisitCount = 0;
         yearActualDuration = 0;
         yearCommentCount = 0;
         yearLoginCount = 0;
         yearPlanDuration = 0;
         yearVisitCompleteCount = 0;
         yearVisitCount = 0;
     };
     success = true;
     }
     */
    
    
    NSDictionary* contentData = [[response firstObject] objectForKey:@"content"];
    
    [self resetUsageReportDisplay:contentData];
    
    [self hideLoadingPopup];
}

-(void)daoRequest:(DAOBaseRequest*)request queryOnlineDataError:(NSString*) response code:(int)code{
    [self hideLoadingPopup];
}

-(void) resetUsageReportDisplay:(NSDictionary*)contentData{
    float monthActualDuration = [[contentData objectForKey:@"monthActualDuration"] floatValue];
    float monthCommentCount = [[contentData objectForKey:@"monthCommentCount"] floatValue];
    float monthLoginCount = [[contentData objectForKey:@"monthLoginCount"] floatValue];
    float monthPlanDuration = [[contentData objectForKey:@"monthPlanDuration"] floatValue];
    float monthVisitCompleteCount = [[contentData objectForKey:@"monthVisitCompleteCount"] floatValue];
    float monthVisitCount = [[contentData objectForKey:@"monthVisitCount"] floatValue];
    float weekActualDuration = [[contentData objectForKey:@"weekActualDuration"] floatValue];
    float weekCommentCount = [[contentData objectForKey:@"weekCommentCount"] floatValue];
    float weekLoginCount = [[contentData objectForKey:@"weekLoginCount"] floatValue];
    float weekPlanDuration = [[contentData objectForKey:@"weekPlanDuration"] floatValue];
    float weekVisitCompleteCount = [[contentData objectForKey:@"weekVisitCompleteCount"] floatValue];
    float weekVisitCount = [[contentData objectForKey:@"weekVisitCount"] floatValue];
    float yearActualDuration = [[contentData objectForKey:@"yearActualDuration"] floatValue];
    float yearCommentCount = [[contentData objectForKey:@"yearCommentCount"] floatValue];
    float yearLoginCount = [[contentData objectForKey:@"yearLoginCount"] floatValue];
    float yearPlanDuration = [[contentData objectForKey:@"yearPlanDuration"] floatValue];
    float yearVisitCompleteCount = [[contentData objectForKey:@"yearVisitCompleteCount"] floatValue];
    float yearVisitCount = [[contentData objectForKey:@"yearVisitCount"] floatValue];

    //Header
    UILabel* loginHeader = [self createNewTextLabelWithDefaultStyle:CGRectMake(0, 0, 1004, 50) parent:self.contentView tag:201];
    loginHeader.backgroundColor = BUTTON_COLOR_BLUE;
    loginHeader.text = [ShareLocale textFromKey:@"Login Statistics"];
    loginHeader.font = [UIFont fontWithName:@"Arial" size:22];
    UILabel* commentHeader = [self createNewTextLabelWithDefaultStyle:CGRectMake(0, 200, 1004, 50) parent:self.contentView tag:202];
    commentHeader.backgroundColor = BUTTON_COLOR_BLUE;
    commentHeader.text = [ShareLocale textFromKey:@"Comment Statistics"];
    commentHeader.font = [UIFont fontWithName:@"Arial" size:22];
    UILabel* visitHeader = [self createNewTextLabelWithDefaultStyle:CGRectMake(0, 400, 1004, 50) parent:self.contentView tag:203];
    visitHeader.backgroundColor = BUTTON_COLOR_BLUE;
    visitHeader.text = [ShareLocale textFromKey:@"Visit Statistics"];
    visitHeader.font = [UIFont fontWithName:@"Arial" size:22];
    
    int offset = 60;
    int colWidth = 240;
    int hPad = 50;
    
    //Login
    UILabel* wkLoginLabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(10, offset, colWidth, 30) parent:self.contentView tag:301];
    wkLoginLabel.text = [ShareLocale textFromKey:@"Week Login Count:"];
    UILabel* wkLoginValLabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(10+colWidth+hPad, offset, colWidth, 30) parent:self.contentView tag:401];
    wkLoginValLabel.text = [NSString stringWithFormat:@"%.0f", weekLoginCount];
    
    UILabel* moLoginLabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(10, offset+40, colWidth, 30) parent:self.contentView tag:302];
    moLoginLabel.text = [ShareLocale textFromKey:@"Month Login Count:"];
    UILabel* moLoginValLabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(10+colWidth+hPad, offset+40, colWidth, 30) parent:self.contentView tag:402];
    moLoginValLabel.text = [NSString stringWithFormat:@"%.0f", monthLoginCount];
    
    UILabel* yrLoginLabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(10, offset+40*2, colWidth, 30) parent:self.contentView tag:303];
    yrLoginLabel.text = [ShareLocale textFromKey:@"Year Login Count:"];
    UILabel* yrLoginValLabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(10+colWidth+hPad, offset+40*2, colWidth, 30) parent:self.contentView tag:403];
    yrLoginValLabel.text = [NSString stringWithFormat:@"%.0f", yearLoginCount];
    
    
    //Comment
    offset = 260;
    UILabel* wkCommentLabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(10, offset, colWidth, 30) parent:self.contentView tag:304];
    wkCommentLabel.text = [ShareLocale textFromKey:@"Week Comment Count:"];
    UILabel* wkCommentValLabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(10+colWidth+hPad, offset, colWidth, 30) parent:self.contentView tag:404];
    wkCommentValLabel.text = [NSString stringWithFormat:@"%.0f", weekCommentCount];
    
    UILabel* moCommentLabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(10, offset+40, colWidth, 30) parent:self.contentView tag:302];
    moCommentLabel.text = [ShareLocale textFromKey:@"Month Comment Count:"];
    UILabel* moCommentValLabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(10+colWidth+hPad, offset+40, colWidth, 30) parent:self.contentView tag:402];
    moCommentValLabel.text = [NSString stringWithFormat:@"%.0f", monthCommentCount];
    
    UILabel* yrCommentLabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(10, offset+40*2, colWidth, 30) parent:self.contentView tag:303];
    yrCommentLabel.text = [ShareLocale textFromKey:@"Year Comment Count:"];
    UILabel* yrCommentValLabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(10+colWidth+hPad, offset+40*2, colWidth, 30) parent:self.contentView tag:403];
    yrCommentValLabel.text = [NSString stringWithFormat:@"%.0f", yearCommentCount];

    //Visit Count
    offset = 460;
    UILabel* wkVisitCountLabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(10, offset, colWidth, 30) parent:self.contentView tag:304];
    wkVisitCountLabel.text = [ShareLocale textFromKey:@"Week Visit Count(Checkout/Planned):"];
    UILabel* wkVisitCountValLabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(10+colWidth+hPad, offset, colWidth, 30) parent:self.contentView tag:404];
    wkVisitCountValLabel.text = [NSString stringWithFormat:@"%.0f / %.0f", weekVisitCompleteCount, weekVisitCount];
    
    UILabel* moVisitCountLabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(10, offset+40, colWidth, 30) parent:self.contentView tag:302];
    moVisitCountLabel.text = [ShareLocale textFromKey:@"Month Visit Count(Checkout/Planned):"];
    UILabel* moVisitCountValLabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(10+colWidth+hPad, offset+40, colWidth, 30) parent:self.contentView tag:402];
    moVisitCountValLabel.text = [NSString stringWithFormat:@"%.0f / %.0f", monthVisitCompleteCount, monthVisitCount];
    
    UILabel* yrVisitCountLabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(10, offset+40*2, colWidth, 30) parent:self.contentView tag:303];
    yrVisitCountLabel.text = [ShareLocale textFromKey:@"Year Visit Count(Checkout/Planned):"];
    UILabel* yrVisitCountValLabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(10+colWidth+hPad, offset+40*2, colWidth, 30) parent:self.contentView tag:403];
    yrVisitCountValLabel.text = [NSString stringWithFormat:@"%.0f / %.0f", yearVisitCompleteCount, yearVisitCount ];
    
    //Visit Duration
    offset = 460;
    UILabel* wkVisitDurLabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(10+colWidth+hPad + 180, offset, colWidth, 30) parent:self.contentView tag:304];
    wkVisitDurLabel.text = [ShareLocale textFromKey:@"Week Visit Hours(Actual/Planned):"];
    UILabel* wkVisitDurValLabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(10+colWidth+hPad+colWidth+hPad + 180, offset, colWidth, 30) parent:self.contentView tag:404];
    wkVisitDurValLabel.text = [NSString stringWithFormat:@"%.1f / %.1f", weekActualDuration/60.0f, weekPlanDuration/60.0f];
    
    UILabel* moVisitDurLabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(10+colWidth+hPad + 180, offset+40, colWidth, 30) parent:self.contentView tag:302];
    moVisitDurLabel.text = [ShareLocale textFromKey:@"Month Visit Hours(Actual/Planned):"];
    UILabel* moVisitDurValLabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(10+colWidth+hPad+colWidth+hPad + 180, offset+40, colWidth, 30) parent:self.contentView tag:402];
    moVisitDurValLabel.text = [NSString stringWithFormat:@"%.1f / %.1f", monthActualDuration/60.0f, monthPlanDuration/60.0f];
    
    UILabel* yrVisitDurLabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(10+colWidth+hPad + 180, offset+40*2, colWidth, 30) parent:self.contentView tag:303];
    yrVisitDurLabel.text = [ShareLocale textFromKey:@"Year Visit Hours(Actual/Planned):"];
    UILabel* yrVisitDurValLabel = [self createNewTextLabelWithDefaultStyle:CGRectMake(10+colWidth+hPad+colWidth+hPad+ + 180, offset+40*2, colWidth, 30) parent:self.contentView tag:403];
    yrVisitDurValLabel.text = [NSString stringWithFormat:@"%.1f / %.1f", yearActualDuration/60.0f,yearPlanDuration/60.0f];
    
}




#pragma mark - Button delegate
- (IBAction)onHomeButtonClicked:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)onBackButtonClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)dealloc {
    [_contentView release];
    [_headerTitleLabel release];
    [super dealloc];
}
@end
