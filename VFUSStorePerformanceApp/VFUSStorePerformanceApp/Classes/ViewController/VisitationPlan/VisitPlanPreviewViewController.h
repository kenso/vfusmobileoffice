//
//  VisitPlanPreviewViewController.h
//  VFStorePerformanceApp_Billy
//
//  Created by Developer 2 on 15/4/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "VFDAO.h"

@interface VisitPlanPreviewViewController : BasicViewController<DAODelegate>{
    NSDictionary* _visitFormData;
    NSDictionary* _visitPlanData;
    NSArray* _commentData;
    NSArray* _questionsData;
    
    int questionIndex;
    
    BOOL allowSubmit;
}


@property (nonatomic, retain) DAORequest* surveyRequest;
@property (nonatomic, retain) DAORequest* questionsRequest;
@property (nonatomic, retain) DAORequest* commentRequest;
@property (nonatomic, retain) DAOBaseRequest* submitRequest;
@property (nonatomic, retain) NSString* errorMessage;

@property (retain, nonatomic) IBOutlet UIView *previewFormView;

@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;

#pragma mark - Form Labels

@property (retain, nonatomic) IBOutlet UILabel *storeNameLabel;

@property (retain, nonatomic) IBOutlet UILabel *visitDateLabel;

@property (retain, nonatomic) IBOutlet UILabel *plannedStartTimeLabel;

@property (retain, nonatomic) IBOutlet UILabel *plannedEndTimeLabel;

@property (retain, nonatomic) IBOutlet UILabel *checkinTimeLabel;

@property (retain, nonatomic) IBOutlet UILabel *checkoutTimeLabel;

@property (retain, nonatomic) IBOutlet UILabel *visitWithLabel;

@property (retain, nonatomic) IBOutlet UILabel *purposeLabel;

#pragma mark - Dynamic Position UI components

@property (retain, nonatomic) IBOutlet UIView *docContentView;

@property (retain, nonatomic) IBOutlet UILabel *actionItemLabel;

@property (retain, nonatomic) IBOutlet UILabel *storeCommentLabel;

@property (retain, nonatomic) IBOutlet UILabel *todoHeaderLabel;

@property (retain, nonatomic) IBOutlet UILabel *docHeaderLabel;

@property (retain, nonatomic) IBOutlet UILabel *actionHeaderLabel;

@property (retain, nonatomic) IBOutlet UILabel *visitFormHeaderLabel;
@property (retain, nonatomic) IBOutlet UIView *visitFormView;

@property (retain, nonatomic) IBOutlet UILabel *storeCommentHeaderLabel;
@property (retain, nonatomic) IBOutlet UIView *todoContentView;
@property (retain, nonatomic) IBOutlet UIButton *submitButton;

@property (retain, nonatomic) NSArray* todoTaskArray;
@property (retain, nonatomic) NSArray* docTaskArray;







@property (retain, nonatomic) IBOutlet UILabel *headerTitleLabel;

@property (retain, nonatomic) IBOutlet UIButton *formTitle;
@property (retain, nonatomic) IBOutlet UILabel *formDate;
@property (retain, nonatomic) IBOutlet UILabel *formStartTime;
@property (retain, nonatomic) IBOutlet UILabel *formEndTime;
@property (retain, nonatomic) IBOutlet UILabel *formPurpose;
@property (retain, nonatomic) IBOutlet UILabel *formTodoList;
@property (retain, nonatomic) IBOutlet UILabel *formDocCheckList;
@property (retain, nonatomic) IBOutlet UILabel *formStoreCode;
@property (retain, nonatomic) IBOutlet UILabel *formVIsitWith;
@property (retain, nonatomic) IBOutlet UILabel *formComment;

@property (retain, nonatomic) IBOutlet UILabel *checkInLabel;
@property (retain, nonatomic) IBOutlet UILabel *checkOutLabel;



#pragma mark - methods

- (id)initWithVisitPlan:(NSDictionary*)visitPlan visitForm:(NSDictionary*)visitForm todoTasks:(NSArray*)todoTasks docTasks:(NSArray*)docTasks;

- (IBAction)onHomeButtonClicked:(id)sender;

- (IBAction)onBackButtonClicked:(id)sender;

- (IBAction)onSubmitButtonClicked:(id)sender;

@end
