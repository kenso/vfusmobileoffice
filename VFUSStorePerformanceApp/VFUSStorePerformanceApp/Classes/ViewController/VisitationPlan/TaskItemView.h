//
//  TaskItemView.h
//  VFStorePerformanceApp_Ray
//
//  Created by cheng chok kwong on 4/7/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//
#import <UIKit/UIKit.h>

typedef enum{
    TaskItemViewModeNormal,//Fill in mode
    TaskItemViewModeLock
}TaskItemViewMode;

typedef enum{
    TaskItemViewTypeToDo,
    TaskItemViewTypeDocument
}TaskItemViewType;

@interface TaskItemView : UIView<UITextFieldDelegate, UITextViewDelegate>{
   int basic_view_height;
}


@property (retain, nonatomic) UIViewController *delegate;

//- (int)getViewHeight;
@end
