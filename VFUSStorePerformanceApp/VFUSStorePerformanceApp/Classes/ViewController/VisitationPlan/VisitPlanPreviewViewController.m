//
//  VisitPlanPreviewViewController.m
//  VFStorePerformanceApp_Billy
//
//  Created by Developer 2 on 15/4/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "VisitPlanPreviewViewController.h"
#import "GeneralUITool.h"
#import "SFRestAPI.h"
#import "ShareLocale.h"
#import "SFRestRequest.h"
#import "VFDAO.h"


#define ROW_PADDING 10
#define HEADER_CONTENT_PADDING 5

@interface VisitPlanPreviewViewController ()

@end

@implementation VisitPlanPreviewViewController

- (id)initWithVisitPlan:(NSDictionary*)visitPlan visitForm:(NSDictionary*)visitForm todoTasks:(NSArray*)todoTasks docTasks:(NSArray*)docTasks
{
    self = [super initWithNibName:@"VisitPlanPreviewViewController" bundle:nil];
    if (self) {
        NSLog(@"VisitPlanPreviewViewController visitPlan=%@",   visitPlan);
        
        NSLog(@"VisitPlanPreviewViewController visitForm=%@",   visitForm);
        
        NSLog(@"VisitPlanPreviewViewController todoTasks=%@",   todoTasks);
        
        NSLog(@"VisitPlanPreviewViewController docTasks=%@",   docTasks);
        
        self.todoTaskArray = todoTasks;
        self.docTaskArray = docTasks;
        
        _visitPlanData = [visitPlan retain];//Id = a0Q90000006EaSQEA0, Type = Visit Form
        
        _visitFormData = [visitForm retain];
        
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    allowSubmit = YES;
    _errorMessage = nil;
    
    [self initPreviewContent ];
    
    [self loadDataOfVisitForm];
    
    [self refreshLocale];
}



-(void) refreshLocale{
    self.headerTitleLabel.text = [ShareLocale textFromKey:@"visit_preview_checkout"];
    
    [self.formTitle setTitle:[ShareLocale textFromKey:@"visit_preview_checkout"] forState:UIControlStateNormal];
    self.formDate.text = [ShareLocale textFromKey:@"newvisit_visitdate:"];
    self.formStartTime.text = [ShareLocale textFromKey:@"newvisit_starttime:"];
    self.formEndTime.text = [ShareLocale textFromKey:@"newvisit_endtime:"];
    self.formPurpose.text = [ShareLocale textFromKey:@"newvisit_purpose:"];
    self.formTodoList.text = [ShareLocale textFromKey:@"newvisit_todolist:"];
    self.formDocCheckList.text = [ShareLocale textFromKey:@"newvisit_doclist:"];
    self.formStoreCode.text = [ShareLocale textFromKey:@"newvisit_store:"];
    self.checkInLabel.text = [ShareLocale textFromKey:@"check-in:"];
    self.actionHeaderLabel.text = [ShareLocale textFromKey:@"newvisit_actionitems:"];
    self.checkOutLabel.text = [ShareLocale textFromKey:@"check-out:"];
    self.formVIsitWith.text = [ShareLocale textFromKey:@"newvisit_visitwith:"];
    self.formComment.text = [ShareLocale textFromKey:@"newvisit_formcomment:"];
    [self.submitButton setTitle:[ShareLocale textFromKey:@"newvisit_save"] forState:UIControlStateNormal];
    
    self.visitFormHeaderLabel.text = [ShareLocale textFromKey:@"visit_form"];
}


#pragma mark - Network Delegate
-(void) submitVisitPlanResult{
    NSLog(@"Submit visit plan result");
    [self showLoadingPopup: self.view];
    
    self.submitRequest = [[VFDAO sharedInstance] updateVisitPlanId:[_visitPlanData objectForKey:@"Id"] data:@{@"emfa__Check_Out_Time__c":[[GeneralUiTool getSFDateFormatter] stringFromDate:[NSDate date]]} delegate:self];
    
//    self.submitRequest = [[SFRestAPI sharedInstance] requestForUpdateWithObjectType:@"emfa__Visitation_Plan__c" objectId: [_visitPlanData objectForKey:@"Id"] fields:@{@"Check_Out_Date_Time__c":[[GeneralUiTool getSFDateFormatter] stringFromDate:[NSDate date]]}];
//    [[SFRestAPI sharedInstance] send:self.submitRequest delegate:self];
    
}

-(void) loadDataOfVisitForm{
    NSLog(@"query visit form answer");
    [self showLoadingPopup:self.view];
    NSString* surveyId = [_visitFormData objectForKey:@"Id"];
    if(surveyId!=nil && surveyId!=(id)[NSNull null]){
        
        self.surveyRequest = [[VFDAO sharedInstance] selectSurveyWithDelegate:self withId:[_visitFormData objectForKey:@"Id"]];
        
    }
    else{
        
        allowSubmit = NO;
        self.errorMessage = [ShareLocale textFromKey:@"dialog_error_no_visit_form"];
        //no visit form!
        [self loadDataOfStoreComment];
    }
    
}

-(void) loadDataOfQuestions{
    NSLog(@"loadDataOfQuestions");
    
    self.questionsRequest = [[VFDAO sharedInstance] selectQuestionsWithDelegate:self withMasterID:[_visitFormData objectForKey:@"emfa__Survey_Master2__c"]];
    
//    NSString* questionSql = [NSString stringWithFormat:@"SELECT Id, Name, emfa__Description__c, emfa__Extra_Comment__c, emfa__Ordering__c, emfa__Survey_Master__c, emfa__Title__c, RecordType.Name FROM emfa__Question_Master__c Where emfa__Survey_Master__c='%@' ORDER BY emfa__Ordering__c ASC", [_visitFormData objectForKey:@"emfa__Survey_Master__c"]];
//    
//    self.questionsRequest = [[SFRestAPI sharedInstance] requestForQuery: questionSql];
//    [[SFRestAPI sharedInstance] send:self.questionsRequest delegate:self];
}

-(void) loadDataOfStoreComment{
    NSLog(@"loadDataOfStoreComment");
    
    self.commentRequest = [[VFDAO sharedInstance] selectCommentsWithDelegate:self withStoreID:[_visitPlanData objectForKey:@"emfa__Store__c"]];
    
    
//    NSString* sql = [NSString stringWithFormat: @"SELECT Id, Subject, Description, ActivityDate, Status FROM Task WHERE WhatId = '%@' ORDER BY CreatedDate DESC", [_visitPlanData objectForKey:@"Store__c"]];
//    
//    self.commentRequest = [[SFRestAPI sharedInstance] requestForQuery:sql];
//    [[SFRestAPI sharedInstance] send:self.commentRequest delegate:self];
}



-(void)daoRequest:(DAORequest *)request queryOnlineDataSuccess:(NSArray *)records{
    if(self.surveyRequest == request){
        if(records!=nil && [records count]>0){
            _visitFormData = [[records objectAtIndex:0] retain];
            [self loadDataOfQuestions];
            return;
        }
        //no survey found
        allowSubmit = NO;
        self.errorMessage = [ShareLocale textFromKey:@"dialog_error_no_visit_form"];
        [self hideLoadingPopupWithFadeout];

    }
    else if(self.questionsRequest == request){
        if(records!=nil && [records count]>0){
            _questionsData = [records retain];
            [self loadDataOfStoreComment];
            return;
        }
        //survey found but no answer found
        allowSubmit = NO;
        self.errorMessage = [ShareLocale textFromKey:@"dialog_error_no_visit_form"];
        [self hideLoadingPopupWithFadeout];
    }
    else if (self.commentRequest == request){
        [self hideLoadingPopupWithFadeout];
        if(records!=nil && [records count]>0){
            
            NSDateFormatter* df = [[[NSDateFormatter alloc] init] autorelease];
            [df setDateFormat:@"yyyy-MM-dd"];
            GeneralUiTool* ap = [GeneralUiTool shareInstance];
            [df setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT: 0 ]];
            
//            NSDate* visitDate = [_visitPlanData objectForKey:@"Visit_Date__c"];
            NSString* visitDateStr = [_visitPlanData objectForKey:@"emfa__Visit_Date__c"];//[df stringFromDate: visitDate];
            
            //only load last 20 comments
            NSMutableArray* comments = [NSMutableArray array];
            int i=0;
            for(NSDictionary* d in records){
                NSString* actDateStr = d[@"ActivityDate__c"];
                actDateStr = [df stringFromDate:[[GeneralUiTool getSFDateFormatter] dateFromString:actDateStr]];
                if([actDateStr isEqualToString: visitDateStr]){
                    [comments addObject:d];
                    i++;
                    if(i>=20){
                        break;
                    }
                }
            }
            _commentData = [comments retain];
        }else{
            _commentData = [@[] retain];
        }
        [self performSelectorOnMainThread:@selector(reloadVisitFormAndComments) withObject:nil waitUntilDone:NO];
    }
    else if (self.submitRequest == request){
        [self hideLoadingPopupWithFadeout];
        [self performSelectorOnMainThread:@selector(onVisitPlanSubmitted) withObject:nil waitUntilDone:NO];
        return;
    
    }else{
        NSAssert(NO, @"Unknown response");
    }
}


-(void)daoRequest:(DAORequest *)request queryOnlineDataError:(NSString *)response code:(int)code{
    [self hideLoadingPopup];
    [self handleQueryDataErrorCode:code description:response];
}


//- (void)request:(SFRestRequest *)request didLoadResponse:(id)jsonResponse{
//    NSLog(@"StoreInfoViewController::didLoadResponse() jsonResponse = %@", jsonResponse);
//    if (self.submitRequest == request){
//        [self hideLoadingPopupWithFadeout];
//        [self performSelectorOnMainThread:@selector(onVisitPlanSubmitted) withObject:nil waitUntilDone:NO];
//        return;
//    }
//    
//    [self hideLoadingPopupWithFadeout];
//    [self popupErrorDialogWithMessage: [NSString stringWithFormat:@"Error. No record found."]];
//    
//}

-(void) onVisitPlanSubmitted{
    [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"" message:[ShareLocale textFromKey:@"dialog_visit_form_submitted"] yesHanlder:^(id data) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
}

//- (void)request:(SFRestRequest *)request didFailLoadWithError:(NSError*)error{
//    [self hideLoadingPopupWithFadeout];
//    [self popupErrorDialogWithMessage: [NSString stringWithFormat:@"Error: %@", error]];
//}
//
//- (void)requestDidCancelLoad:(SFRestRequest *)request{
//    [self hideLoadingPopupWithFadeout];
//    [self popupErrorDialogWithMessage: @"Request is cancelled"];
//}
//
//- (void)requestDidTimeout:(SFRestRequest *)request{
//    [self hideLoadingPopupWithFadeout];
//    [self popupErrorDialogWithMessage: @"Loading time out"];
//}



#pragma mark - UI reload
-(void) initPreviewContent
{
    [self.scrollView addSubview: self.previewFormView];
    self.previewFormView.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
    CGRect rect = self.previewFormView.frame;
    self.scrollView.contentSize = CGSizeMake(rect.size.width, rect.size.height);
    [self fillinVisitPlan];
    [self fillInExtraContent];
    [self reformatVisitPlan];
    [self.scrollView flashScrollIndicators];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    [_errorMessage release];
    [_storeNameLabel release];
    [_visitDateLabel release];
    [_plannedStartTimeLabel release];
    [_plannedEndTimeLabel release];
    [_checkoutTimeLabel release];
    [_visitWithLabel release];
    [_purposeLabel release];
    [_todoHeaderLabel release];
    [_docHeaderLabel release];
    [_actionHeaderLabel release];
    [_visitFormHeaderLabel release];
    [_storeCommentHeaderLabel release];
    [_docContentView release];
    [_actionItemLabel release];
    [_storeCommentLabel release];
    [_previewFormView release];
    [_scrollView release];
    [_visitFormData release];
    [_visitPlanData release];
    [_commentData release];
    [_todoContentView release];
    [_visitFormView release];
    [_submitButton release];
    [super dealloc];
}


#pragma mark - Button events
- (IBAction)onHomeButtonClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)onBackButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onSubmitButtonClicked:(id)sender {
    if(allowSubmit){
        [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:[ShareLocale textFromKey:@"confirm"] message:[ShareLocale textFromKey:@"dialog_confirm_checkout"] yesHanlder:^(id data) {
            [self submitVisitPlanResult];
        } noHandler:^(id data) {
            
        }];
    }else{
        [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:[ShareLocale textFromKey:@"error"] message: self.errorMessage];
    }
    

}

#pragma mark - Fill in data
-(void)fillinVisitPlan{
//    self.storeNameLabel.text = [NSString stringWithFormat:@"%@ %@", [self getFieldDisplayWithFieldName:@"Store__r.Store_Code__c" withDictionary: _visitPlanData], [self getFieldDisplayWithFieldName:@"Store__r.Name" withDictionary: _visitPlanData]];
    
    self.storeNameLabel.text = [self getFieldDisplayWithFieldName:@"emfa__Store__r.Name" withDictionary: _visitPlanData];
    
    self.visitDateLabel.text = [self getFieldDisplayWithFieldName:@"emfa__Visit_Date__c" withDictionary:_visitPlanData];
    
    NSDate* date;
    NSDateFormatter *humanDF = [[[NSDateFormatter alloc] init] autorelease];
    [humanDF setDateFormat:@"yyyy-MM-dd HH:mm"];
    GeneralUiTool* ap = [GeneralUiTool shareInstance];
    [humanDF setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT: ap.timezoneSecond ]];
    
    date = [[GeneralUiTool getSFDateFormatter] dateFromString:[self getFieldDisplayWithFieldName:@"emfa__Start_Time__c" withDictionary:_visitPlanData]];
    self.plannedStartTimeLabel.text = [humanDF stringFromDate:date];
    
    date = [[GeneralUiTool getSFDateFormatter] dateFromString:[self getFieldDisplayWithFieldName:@"emfa__End_Time__c" withDictionary:_visitPlanData]];
    self.plannedEndTimeLabel.text = [humanDF stringFromDate:date];
    
    date = [[GeneralUiTool getSFDateFormatter] dateFromString:[self getFieldDisplayWithFieldName:@"emfa__Check_In_Time__c" withDictionary:_visitPlanData]];
    self.checkinTimeLabel.text = [humanDF stringFromDate:date];
    
    date = [NSDate date];
    self.checkoutTimeLabel.text = [humanDF stringFromDate:date];
    
    self.visitWithLabel.text = [self getFieldDisplayWithFieldName:@"emfa__Visit_With__c" withDictionary:_visitPlanData];
    
    self.purposeLabel.text = [self getFieldDisplayWithFieldName:@"emfa__Remark__c" withDictionary:_visitPlanData];
    
    self.actionItemLabel.text = [self getFieldDisplayWithFieldName:@"Comment__c" withDictionary:_visitPlanData];
    

}

-(void) fillInExtraContent{
    
    for(NSDictionary* t in self.todoTaskArray){
        [self addTaskAtToDoView:t];
    }
    for(NSDictionary* t in self.docTaskArray){
        [self addTaskAtDocumentView:t];
    }

}

-(void) addTaskAtToDoView:(NSDictionary*)task{
    UIView* contentView = self.todoContentView;
    float ty = contentView.frame.size.height - 25;
    NSString* actualDuration = [self getFieldDisplayWithFieldName:@"emfa__Actual_Duration__c" withDictionary:task];
    NSString* status = [self getFieldDisplayWithFieldName:@"Status" withDictionary:task];
    NSString* category = [self getFieldDisplayWithFieldName:@"emfa__Category__c" withDictionary:task];
    NSString* estimateDuration = [self getFieldDisplayWithFieldName:@"emfa__Estimated_Duration__c" withDictionary:task];
    NSString* subject = [self getFieldDisplayWithFieldName:@"Subject" withDictionary:task];
    
    UILabel* label;
    label = [[UILabel alloc] initWithFrame:CGRectMake(10, ty, 200, 22)];
    label.adjustsFontSizeToFitWidth = YES;
    label.minimumScaleFactor = .5f;
    label.font = [UIFont fontWithName:@"Helvetica Neue" size:15];
    label.textColor = [UIColor colorWithWhite:1 alpha:1];
    label.text = subject;
    [contentView addSubview:label];
    [label release];
    
    label = [[UILabel alloc] initWithFrame:CGRectMake(220, ty, 150, 22)];
    label.adjustsFontSizeToFitWidth = YES;
    label.minimumScaleFactor = .5f;
    label.font = [UIFont fontWithName:@"Helvetica Neue" size:15];
    label.textColor = [UIColor colorWithWhite:1 alpha:1];
    label.text = [ShareLocale textFromKey:category];
    [contentView addSubview:label];
    [label release];
    
    label = [[UILabel alloc] initWithFrame:CGRectMake(380, ty, 150, 22)];
    label.adjustsFontSizeToFitWidth = YES;
    label.minimumScaleFactor = .5f;
    label.font = [UIFont fontWithName:@"Helvetica Neue" size:15];
    label.textColor = [UIColor colorWithWhite:1 alpha:1];
    label.text = [NSString stringWithFormat: [ShareLocale textFromKey:@"mins_planned_format"], estimateDuration];
    [contentView addSubview:label];
    [label release];
    
    label = [[UILabel alloc] initWithFrame:CGRectMake(540, ty, 150, 22)];
    label.adjustsFontSizeToFitWidth = YES;
    label.minimumScaleFactor = .5f;
    label.font = [UIFont fontWithName:@"Helvetica Neue" size:15];
    label.textColor = [UIColor colorWithWhite:1 alpha:1];
    label.text = [actualDuration isEqualToString:@"N/A"]?@"N/A":[NSString stringWithFormat:[ShareLocale textFromKey:@"mins_used_format"], actualDuration];
    [contentView addSubview:label];
    [label release];
    
    label = [[UILabel alloc] initWithFrame:CGRectMake(700, ty, 150, 22)];
    label.adjustsFontSizeToFitWidth = YES;
    label.minimumScaleFactor = .5f;
    label.font = [UIFont fontWithName:@"Helvetica Neue" size:15];
    label.textColor = [UIColor colorWithWhite:1 alpha:1];
    label.text = status;
    if(![status isEqualToString:@"Completed"]){
        label.textColor = [UIColor colorWithRed:.9f green:.2f blue:.2f alpha:1];
    }
    
    if([label.text isEqualToString:@"Not Started"]){
        label.text = [ShareLocale textFromKey:@"task_status_not_started"];
    }
    else if([label.text isEqualToString:@"Completed"]){
        label.text = [ShareLocale textFromKey:@"task_status_completed"];
    }
    
    
    [contentView addSubview:label];
    [label release];

    contentView.frame = CGRectMake(contentView.frame.origin.x, contentView.frame.origin.y, contentView.frame.size.width, contentView.frame.size.height+25);
}

-(void) addTaskAtDocumentView:(NSDictionary*)task{
    UIView* contentView = self.docContentView;
    float ty = contentView.frame.size.height - 25;
    NSString* status = [self getFieldDisplayWithFieldName:@"Status" withDictionary:task];
    NSString* subject = [self getFieldDisplayWithFieldName:@"Subject" withDictionary:task];
    
    UILabel* label;
    label = [[UILabel alloc] initWithFrame:CGRectMake(10, ty, 200, 22)];
    label.adjustsFontSizeToFitWidth = YES;
    label.minimumScaleFactor = .5f;
    label.font = [UIFont fontWithName:@"Helvetica Neue" size:15];
    label.textColor = [UIColor colorWithWhite:1 alpha:1];
    label.text = subject;
    [contentView addSubview:label];
    [label release];
    
    label = [[UILabel alloc] initWithFrame:CGRectMake(700, ty, 150, 22)];
    label.adjustsFontSizeToFitWidth = YES;
    label.minimumScaleFactor = .5f;
    label.font = [UIFont fontWithName:@"Helvetica Neue" size:15];
    label.textColor = [UIColor colorWithWhite:1 alpha:1];
    label.text = status;
    if(![status isEqualToString:@"Completed"]){
        label.textColor = [UIColor colorWithRed:.9f green:.2f blue:.2f alpha:1];
    }
    [contentView addSubview:label];
    [label release];
    
    
    
    contentView.frame = CGRectMake(contentView.frame.origin.x, contentView.frame.origin.y, contentView.frame.size.width, contentView.frame.size.height+25);
}

-(void) addTaskAtVisitPlanCommentView:(NSDictionary*)task{
    
}

-(void) addTaskAtStoreCommentView:(NSDictionary*)task{
    
    if([self.storeCommentLabel.text isEqualToString:@"N/A"]){
        self.storeCommentLabel.text = @"";
    }
    
    NSString* subject = [self getFieldDisplayWithFieldName:@"Subject__c" withDictionary:task];
    NSString* dateStr = [self getFieldDisplayWithFieldName:@"ActivityDate__c" withDictionary:task];
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm"];
    GeneralUiTool* ap = [GeneralUiTool shareInstance];
    [df setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT: ap.timezoneSecond ]];
    
    dateStr = [df stringFromDate: [[GeneralUiTool getSFDateFormatter] dateFromString: dateStr]];
    [df release];
    self.storeCommentLabel.text = [NSString stringWithFormat:@"%@%@     %@\n", self.storeCommentLabel.text, dateStr, subject];
    
}

-(void)reformatVisitPlan{
//    [self.purposeLabel sizeToFit];
//    [self.actionItemLabel sizeToFit];

    
    //todo field
    CGRect todoHF = self.todoHeaderLabel.frame;
    self.todoHeaderLabel.frame = CGRectMake(todoHF.origin.x, self.actionItemLabel.frame.origin.y + self.actionItemLabel.frame.size.height + ROW_PADDING, todoHF.size.width, todoHF.size.height);
    CGRect todoCF = self.todoContentView.frame;
    self.todoContentView.frame = CGRectMake(todoCF.origin.x, self.todoHeaderLabel.frame.origin.y + self.todoHeaderLabel.frame.size.height + HEADER_CONTENT_PADDING, todoCF.size.width, todoCF.size.height);
    
    //DOCUMENT
    CGRect docHF = self.docHeaderLabel.frame;
    self.docHeaderLabel.frame = CGRectMake(docHF.origin.x, self.todoContentView.frame.origin.y + self.todoContentView.frame.size.height + ROW_PADDING, docHF.size.width, docHF.size.height);
    
    CGRect docCF = self.docContentView.frame;
    self.docContentView.frame = CGRectMake(docCF.origin.x, self.docHeaderLabel.frame.origin.y + self.docHeaderLabel.frame.size.height + HEADER_CONTENT_PADDING, docCF.size.width, docCF.size.height);
    
    //ACTION ITEM
//    CGRect actionitemHF = self.actionHeaderLabel.frame;
//    self.actionHeaderLabel.frame = CGRectMake(actionitemHF.origin.x, self.docContentView.frame.origin.y + self.docContentView.frame.size.height + ROW_PADDING, actionitemHF.size.width, actionitemHF.size.height);
//    self.actionHeaderLabel.hidden = YES;
//    CGRect actionitemCF = self.actionItemLabel.frame;
//    self.actionItemLabel.frame = CGRectMake(actionitemCF.origin.x, self.actionHeaderLabel.frame.origin.y, actionitemCF.size.width, actionitemCF.size.height);
//    self.actionItemLabel.hidden = YES;
    
    //STORE VISIT FORM
    CGRect visitHF = self.visitFormHeaderLabel.frame;
    self.visitFormHeaderLabel.frame = CGRectMake(visitHF.origin.x, self.docContentView.frame.origin.y + self.docContentView.frame.size.height + ROW_PADDING, visitHF.size.width, visitHF.size.height);
    
    CGRect visitCF = self.visitFormView.frame;
    self.visitFormView.frame = CGRectMake(visitCF.origin.x, self.visitFormHeaderLabel.frame.origin.y + self.visitFormHeaderLabel.frame.size.height + ROW_PADDING, visitCF.size.width, visitCF.size.height);
    [self.actionItemLabel sizeToFit];
    
    //STORE COMMENTS
    CGRect commentHF = self.storeCommentHeaderLabel.frame;
    self.storeCommentHeaderLabel.frame = CGRectMake(commentHF.origin.x, self.visitFormView.frame.origin.y + self.visitFormView.frame.size.height + ROW_PADDING, commentHF.size.width, commentHF.size.height);
    
    CGRect commentCF = self.storeCommentLabel.frame;
    self.storeCommentLabel.frame = CGRectMake(commentCF.origin.x, self.storeCommentHeaderLabel.frame.origin.y , commentCF.size.width, commentCF.size.height);
    
    //SUBMIT BUTTON
    CGRect submitBF = self.submitButton.frame;
    self.submitButton.frame = CGRectMake(submitBF.origin.x, self.storeCommentLabel.frame.origin.y + self.storeCommentLabel.frame.size.height + 3*ROW_PADDING, submitBF.size.width, submitBF.size.height);
    
    self.previewFormView.frame = CGRectMake(self.previewFormView.frame.origin.x, self.previewFormView.frame.origin.y, self.previewFormView.frame.size.width, self.submitButton.frame.origin.y + self.submitButton.frame.size.height + HEADER_CONTENT_PADDING*5);
    CGRect rect = self.previewFormView.frame;
    self.scrollView.contentSize = CGSizeMake(rect.size.width, rect.size.height);
    
}


-(void)reloadVisitFormAndComments{
    [self reloadVisitFormView];
    [self reloadComments];
    
    [self reformatVisitPlan];
    [self.scrollView flashScrollIndicators];

}


-(void)reloadComments{
    NSArray* commentArray = _commentData;
    for(NSDictionary* c in commentArray){
        [self addTaskAtStoreCommentView:c];
    }
    CGRect rect = CGRectMake(self.storeCommentLabel.frame.origin.x, self.storeCommentLabel.frame.origin.y, self.storeCommentLabel.frame.size.width, self.storeCommentLabel.frame.size.height);
    [self.storeCommentLabel sizeToFit];
    self.storeCommentLabel.frame = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, self.storeCommentLabel.frame.size.height);
}


-(void)reloadVisitFormView{
    questionIndex = 0;
    
    NSArray* answers = [[_visitFormData objectForKey:@"emfa__Question_Answers__r"] objectForKey:@"records"];
    NSArray* questions = _questionsData;
    
    questions = [questions sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSDictionary* firstObj = obj1;
        NSDictionary* secondObj = obj2;
        int order1 = [firstObj[@"emfa__Ordering__c"] intValue];
        int order2 = [secondObj[@"emfa__Ordering__c"] intValue];
        if(order1 == order2){
            return NSOrderedSame;
        }else if (order1 > order2){
            return NSOrderedDescending;
        }else{
            return NSOrderedAscending;
        }
    }];
    
    for(NSDictionary* q in questions){
        NSString* recordTypeName = [self getFieldDisplayWithFieldName:@"RecordType.Name" withDictionary:q];
        if([recordTypeName isEqualToString:@"Question Group"]){
            [self addSectionAtVisitFormView: q];
        }else{
            [self addQuestionAnswerAtVisitFormView: q answerList: answers];
        }
    }
    
    [self reformatVisitPlan];
    [self.scrollView flashScrollIndicators];

    
}

-(void) addSectionAtVisitFormView:(NSDictionary*)question{
    questionIndex = 0;

    UIView* contentView = self.visitFormView;
    float ty = contentView.frame.size.height - 25;

    NSString* title = [self getFieldDisplayWithFieldName:@"emfa__Title__c" withDictionary:question];
    UILabel* label;
    label = [[UILabel alloc] initWithFrame:CGRectMake(10, ty+5, contentView.frame.size.width - 20, 30)];
    label.font = [UIFont fontWithName:@"Helvetic Neue Bold" size:18];
    label.textColor = [UIColor colorWithWhite:1 alpha:1];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = title;
    label.backgroundColor = [UIColor colorWithWhite:0 alpha:.2f];
    [contentView addSubview:label];
    [label release];

    contentView.frame = CGRectMake(contentView.frame.origin.x, contentView.frame.origin.y, contentView.frame.size.width, contentView.frame.size.height+40);
}

-(void) addQuestionAnswerAtVisitFormView:(NSDictionary*)question answerList:(NSArray*)answers{
    questionIndex++;
    
    UIView* contentView = self.visitFormView;
    float ty = contentView.frame.size.height - 25;
    NSString* qid = [self getFieldDisplayWithFieldName:@"Id" withDictionary:question];
    int needExtraComment = [[self getFieldDisplayWithFieldName:@"emfa__Extra_Comment__c" withDictionary:question] intValue];
    NSString* titleVal = [self getFieldDisplayWithFieldName:@"emfa__Title__c" withDictionary:question];
    
    //FND answer!
    NSString* answerVal = nil;
    NSDictionary* answer = nil;
    NSString* commentVal = nil;
    for(NSDictionary* ans in answers){
        NSString* targetQid = [self getFieldDisplayWithFieldName:@"emfa__Question_Master__c" withDictionary:ans];
        if(targetQid!=nil && [targetQid isEqualToString:qid]){
            answer = ans;
            answerVal = [self getFieldDisplayWithFieldName:@"emfa__Answer_Value__c" withDictionary:answer];
            commentVal = [self getFieldDisplayWithFieldName:@"emfa__Comment__c" withDictionary:answer];
            break;
        }
    }

    
    UILabel* label;
    label = [[UILabel alloc] initWithFrame:CGRectMake(10, ty, 500, 22)];
    label.font = [UIFont fontWithName:@"Helvetica Neue" size:15];
    label.textColor = [UIColor colorWithWhite:1 alpha:1];
    label.text = [NSString stringWithFormat:@"%d. %@",questionIndex, titleVal];
    [contentView addSubview:label];
    [label release];
    
    label = [[UILabel alloc] initWithFrame:CGRectMake(510, ty, 400, 22)];
    label.font = [UIFont fontWithName:@"Helvetica Neue" size:15];
    label.textColor = [UIColor colorWithWhite:1 alpha:1];
    if(answer!=nil){
        label.text = answerVal;
    }else{
        label.text = @"Not Answered";
    }
    [contentView addSubview:label];
    [label release];

    if(needExtraComment==1 && commentVal!=nil && ![commentVal isEqualToString:@""] && ![commentVal isEqualToString:@"N/A"]){
        label = [[UILabel alloc] initWithFrame:CGRectMake(10, ty+25, 200, 22)];
        label.font = [UIFont fontWithName:@"Helvetica Neue" size:15];
        label.textColor = [UIColor colorWithWhite:1 alpha:1];
        label.text = [NSString stringWithFormat:@"- Comment: %@", commentVal];
        [contentView addSubview:label];
        [label release];
        contentView.frame = CGRectMake(contentView.frame.origin.x, contentView.frame.origin.y, contentView.frame.size.width, contentView.frame.size.height+25);

    }
    
    
    contentView.frame = CGRectMake(contentView.frame.origin.x, contentView.frame.origin.y, contentView.frame.size.width, contentView.frame.size.height+25);
}


@end
