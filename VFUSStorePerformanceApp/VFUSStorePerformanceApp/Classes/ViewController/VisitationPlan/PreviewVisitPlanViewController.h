//
//  PreviewVisitPlanViewController.h
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 21/6/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"

@interface PreviewVisitPlanViewController : BasicViewController<DAODelegate>



-(id)initWithData:(NSDictionary*)data delegate:(id<CreateVisitPlanDelegate>)delegate;

- (IBAction)onHomeButtonClicked:(id)sender;

- (IBAction)onBackButtonClicked:(id)sender;

- (IBAction)onSaveButtonClicked:(id)sender;

@property (retain, nonatomic) IBOutlet UILabel *visitDateLabel;
@property (retain, nonatomic) IBOutlet UILabel *startTimeLabel;
@property (retain, nonatomic) IBOutlet UILabel *endTimeLabel;
@property (retain, nonatomic) IBOutlet UILabel *purposeLabel;
@property (retain, nonatomic) IBOutlet UILabel *storeLabel;
@property (retain, nonatomic) IBOutlet UILabel *commentsLabel;
@property (retain, nonatomic) IBOutlet UILabel *visitWithLabel;
@property (retain, nonatomic) IBOutlet UIView *todoView;
@property (retain, nonatomic) IBOutlet UIView *docView;

@property (retain, nonatomic) IBOutlet UILabel *headerTitleLabel;

@property (retain, nonatomic) IBOutlet UIButton *formTitle;
@property (retain, nonatomic) IBOutlet UILabel *formDate;
@property (retain, nonatomic) IBOutlet UILabel *formStartTime;
@property (retain, nonatomic) IBOutlet UILabel *formEndTime;
@property (retain, nonatomic) IBOutlet UILabel *formPurpose;
@property (retain, nonatomic) IBOutlet UILabel *formTodoList;
@property (retain, nonatomic) IBOutlet UILabel *formDocCheckList;
@property (retain, nonatomic) IBOutlet UILabel *formStoreCode;
@property (retain, nonatomic) IBOutlet UILabel *formVIsitWith;
@property (retain, nonatomic) IBOutlet UILabel *formComment;
@property (retain, nonatomic) IBOutlet UIButton *saveButton;



@property (nonatomic, assign) id<CreateVisitPlanDelegate> delegate;

@property (retain, nonatomic) CustomWebserviceRequest* postFormRequest;

@property (nonatomic, retain) NSDictionary* data;

@end
