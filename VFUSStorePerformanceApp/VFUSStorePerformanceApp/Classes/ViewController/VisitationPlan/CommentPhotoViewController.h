//
//  CommentPhotoViewController.h
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 7/1/15.
//  Copyright (c) 2015 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol CommentPhotoSelectionDelegate <NSObject>

-(void) onSelectedCommentPhoto:(NSDictionary*)commentData;
-(void) onClickedNewCommentPhoto;
@end


@interface CommentPhotoViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (retain, nonatomic) NSArray* dataArray;
@property (retain, nonatomic) IBOutlet UITableView *tableView;
@property (assign) id<CommentPhotoSelectionDelegate> delegate;
@property (retain, nonatomic) IBOutlet UILabel *headerText;

- (IBAction)onClickNewButton:(id)sender;

-(id) initWithCommentArray:(NSArray*)dataArray;

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;

@end
