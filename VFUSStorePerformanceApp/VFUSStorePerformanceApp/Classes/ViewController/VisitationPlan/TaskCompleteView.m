//
//  TaskCompleteView.m
//  VFStorePerformanceApp_Dev2
//
//  Created by Tony Keung on 1/3/16.
//  Copyright (c) 2016 vf.com. All rights reserved.
//

#import "TaskCompleteView.h"
#import "ShareLocale.h"
#import "GeneralUiTool.h"
#import "DataHelper.h"

@implementation TaskCompleteView

- (id)initWithFrame:(CGRect)frame withDelegate:(id)delegate
{
    
//    self = [super initWithFrame:frame];
    self = [[[[NSBundle mainBundle] loadNibNamed:@"TaskCompleteView" owner:self options:nil] objectAtIndex:0] retain];
    
    if (self) {
        
        self.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height);
        self.delegate = delegate;
        
        self.comment = @"";
        self.duration = 0;
        self.status = @"Not Started";
        [self.commentTextView setDelegate:self];
        [self.durationTextView setDelegate:self];
        self.durationTextView.keyboardType = UIKeyboardTypeNumberPad;
        
        [self.checkboxButton setTitle:@"" forState:UIControlStateNormal];
        [self.checkboxButton setTitle:@"" forState:UIControlStateSelected];
        [self.checkboxButton setBackgroundImage:[UIImage imageNamed:@"checkbox_off.png"] forState:UIControlStateNormal];
        [self.checkboxButton setBackgroundImage:[UIImage imageNamed:@"tickbox_on.png"] forState:UIControlStateSelected];
        
        [self refreshContentView];
    }
    return self;
}

- (IBAction)onClickConfirmButton:(id)sender{
    [self endEditing:YES];
    if ([self isValidForm] ){
        if (self.delegate != nil ){
            
            NSDictionary* data = @{
                                   @"Index": self.indexPath,
                                   @"Comment": self.commentTextView.text,
                                   @"Duration": self.durationTextView.text,
                                   @"Done": [NSNumber numberWithBool:self.checkboxButton.selected]
                                   };
            
            [self.delegate onTaskCompleteComplete:data];
            
            [self setHidden:YES];
            [self cleanAllData];
        }
    }else{
        [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:[ShareLocale textFromKey:@"error"] message:self.errorMessage];
    }
}
- (IBAction)onClickCancelButton:(id)sender{
    [self endEditing:YES];
    [self resetAllData];
    [self setHidden:YES];
}

- (IBAction)onClickCheckboxButton:(id)sender {
    UIButton* checkBox = sender;
    BOOL newVal = ![checkBox isSelected];
    [checkBox setSelected:newVal];
    
}

- (void)refreshAll:(NSDictionary*)dict{
    [self refreshContentView:dict];
}


- (void)resetAllData{
    [self.commentTextView setText:self.comment];
    [self.durationTextView setText:[NSString stringWithFormat:@"%d", self.duration]];
}

- (void)cleanAllData{
    [self.commentTextView setText:@""];
    [self.durationTextView setText:@""];
}

- (void)refreshContentView{
    [self.titleLabel setTitle:[ShareLocale textFromKey:@"taskform_finish_title"] forState:UIControlStateNormal];
    [self.commentLabel setText: [ShareLocale textFromKey:@"taskform_comment:"] ];
    [self.durationLabel setText: [ShareLocale textFromKey:@"visit_dialog_completetask_desc"] ];
    [self.doneLabel setText: [ShareLocale textFromKey:@"todo_done:"]];
    
    [self.confirmButton setTitle: [ShareLocale textFromKey:@"confirm"] forState:UIControlStateNormal];
    [self.cancelButton setTitle: [ShareLocale textFromKey:@"taskform_cancel"] forState:UIControlStateNormal];
}

- (void)refreshContentView:(NSDictionary*)dict{
    [self refreshContentView];
    
    if ( [dict objectForKey:@"emfa__Actual_Duration__c"] == nil || [dict objectForKey:@"emfa__Actual_Duration__c"] == (id)[NSNull null] ) {
        self.duration = 0;
        [self.durationTextView setText:@""];
    }
    else {
        self.duration = [[dict objectForKey:@"emfa__Actual_Duration__c"] intValue];
        [self.durationTextView setText:[NSString stringWithFormat:@"%d", self.duration]];
    }
    
    if ( [dict objectForKey:@"Comment__c"] != nil && [dict objectForKey:@"Comment__c"] != (id)[NSNull null] ) {
        self.comment = [dict objectForKey:@"Comment__c"];
        [self.commentTextView setText:self.comment];
    }
    
    [self.checkboxButton setSelected:NO];
    if ( [dict objectForKey:@"Status"] != nil && [dict objectForKey:@"Status"] != (id)[NSNull null] ) {
        self.status = [dict objectForKey:@"Status"];
        if ( [self.status isEqualToString:@"Completed"] ) {
            [self.checkboxButton setSelected:YES];
        }
        else {
            [self.checkboxButton setSelected:NO];
        }
    }
    
}

#pragma mark - UITextView Delegate
- (void)textViewDidBeginEditing:(UITextView *)textView{
    editing = YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    editing = NO;
}


- (BOOL)isValidForm{
    BOOL result = YES;
    
    if ( self.commentTextView.text == nil || [self.commentTextView.text isEqualToString:@""]){
        result = NO;
        self.errorMessage = [ShareLocale textFromKey: @"error_comment_required"];
    } else if ( [self.checkboxButton isSelected] ) {
        if ( self.durationTextView.text == nil || [self.durationTextView.text isEqualToString:@""]){
            result = NO;
            self.errorMessage = [ShareLocale textFromKey: @"error_duration_required"];
        } else if ( ![DataHelper isIntegerForString:self.durationTextView.text] ) {
            result = NO;
            self.errorMessage = [ShareLocale textFromKey: @"validation_error"];
        }
    }
    else {
        if ( ![self.durationTextView.text isEqualToString:@""] && [self.durationTextView.text intValue] > 0 ){
            result = NO;
            self.errorMessage = [ShareLocale textFromKey: @"error_done_required"];
        }
    }
    return result;
}


- (void)dealloc {
    [_titleLabel release];
    [_commentLabel release];
    [_durationLabel release];
    [_commentTextView release];
    [_durationTextView release];
    [_doneLabel release];
    [_checkboxButton release];
    [super dealloc];
}

@end
