//
//  VisitationViewController.m
//  emFace_Ray
//
//  Created by cheng chok kwong on 11/4/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//
#import "SFRestAPI.h"
#import "ShareLocale.h"
#import "SFRestRequest.h"
#import "VFDAO.h"
#import "DataHelper.h"
#import "VisitationViewController.h"
#import "VisitationFormViewController.h"
#import "CreateVisitPlanViewController.h"
#import "CommentFormViewController.h"
#import "VisitPlanPreviewViewController.h"
#import "Constants.h"
#import "GeneralUiTool.h"
#import "SurveyListViewController.h"
#import "SurveyFormViewController.h"
#import "StoreSelectionViewController.h"
#import "VRGCalendarView.h"
#import "CommentViewController.h"
#import "CommentPopupView.h"



@interface VisitationViewController ()

@end

@implementation VisitationViewController


#pragma mark - UIView basic delegate

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    mode = 0;//0 is calendar view, 1 is list view
    todoTaskDurationSum = 0; //reset total sum minutes of todo task duration
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;

    
    UIView* calendarContainer = [(UIView*) self.view viewWithTag:301];
    
    self.calendarView = [[[VRGCalendarView alloc] initWithBgColor: BUTTON_COLOR_GREEN textColor:[UIColor blackColor]] autorelease];
    self.selectedDate = [NSDate date];
    
    self.filterStoreId = nil;
    self.filterStoreCode = @"Any Store";

    self.logcallButton.hidden = YES;
    self.mspFormButton.hidden = YES;
    self.trashButton.hidden = YES;
    self.visitFormButton.hidden = YES;
    self.sopFormButton.hidden = YES;
    
    
    self.calendarView.delegate = self;
    [calendarContainer addSubview: self.calendarView];
    self.calendarView.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
    
    
    dayListTableView = (UITableView*)[ self.view viewWithTag:303];
    dayListTableView.delegate = self;
    dayListTableView.dataSource = self;
    dayListTableView.backgroundColor = TABLE_BG_COLOR_GREEN;
    dayListTableView.separatorColor = [UIColor colorWithWhite:1 alpha:0];
    
    listTableView = (UITableView*)[ self.view viewWithTag:404];
    listTableView.delegate = self;
    listTableView.dataSource = self;
    listTableView.backgroundColor = TABLE_BG_COLOR_GREEN;
    listTableView.separatorColor = [UIColor colorWithWhite:1 alpha:0];
    
    
    [self.toDoTableView setDelegate:self];
    [self.toDoTableView setDataSource:self];
    [self.toDoTableView setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.1f]];
    [self.toDoTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.documentTableView setDelegate:self];
    [self.documentTableView setDataSource:self];
    [self.documentTableView setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.1f]];
    [self.documentTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self refreshEventListForSelectedDate];
    
    self.taskCompleteView = [[TaskCompleteView alloc] initWithFrame:self.view.frame withDelegate:self];
    [self.view addSubview:self.taskCompleteView];
    [self.taskCompleteView setHidden:YES];
    
//    [self initCommentSidebarWithStoreId: nil];
    
    [self refreshLocale];
    
    //FIX_RELOAD_PAGE
    [self refreshEventListForSelectedDate];
    [self loadData];
    
}

-(void) refreshLocale{
    
    [self.editPlanButton  setTitle: [ShareLocale textFromKey:@"edit"] forState:UIControlStateNormal];
    
    [self.createVisitPlanButton setTitle: [ShareLocale textFromKey:@"visit_new_plan_button"] forState:UIControlStateNormal];
    self.headerTitleLabel.text = [ShareLocale textFromKey:@"visit_title"];
    self.plannedStartLabel.text = [ShareLocale textFromKey:@"visit_planned_start"];
    self.plannedEndLabel.text = [ShareLocale textFromKey:@"visit_planned_end"];
    self.checkinLabel.text = [ShareLocale textFromKey:@"visit_checkin"];
    self.checkoutLabel.text = [ShareLocale textFromKey:@"visit_checkout"];
    self.purposeLabel.text = [ShareLocale textFromKey:@"visit_purpose"];
    self.formVisitWithLabel.text = [ShareLocale textFromKey:@"visit_visitwith"];
    self.commentLabel.text = [ShareLocale textFromKey:@"visit_comment:"];
    
    [self.todoCategory setTitle:[ShareLocale textFromKey:@"todo_category"] forState:UIControlStateNormal];
    [self.todoDone setTitle:[ShareLocale textFromKey:@"todo_done"] forState:UIControlStateNormal];
    [self.todoDur setTitle:[ShareLocale textFromKey:@"todo_dur"] forState:UIControlStateNormal];
    [self.todoEsitmated setTitle:[ShareLocale textFromKey:@"todo_estimated"] forState:UIControlStateNormal];
    [self.todoName setTitle:[ShareLocale textFromKey:@"todo_name"] forState:UIControlStateNormal];
    [self.todoUsed setTitle:[ShareLocale textFromKey:@"todo_used"] forState:UIControlStateNormal];

    [self.docName setTitle: [ShareLocale textFromKey:@"doc_name"] forState:UIControlStateNormal];
    
    [self.mspFormButton setTitle: [ShareLocale textFromKey:@"surveytitle_msp"] forState:UIControlStateNormal];
    [self.sopFormButton setTitle: [ShareLocale textFromKey:@"surveytitle_audit"] forState:UIControlStateNormal];
    [self.visitFormButton setTitle: [ShareLocale textFromKey:@"surveytitle_visit"] forState:UIControlStateNormal];
    [self.logcallButton setTitle: [ShareLocale textFromKey:@"viist_logcall"] forState:UIControlStateNormal];
    
    
    [self.checkinButton setTitle:[ShareLocale textFromKey:@"check-in"] forState:UIControlStateNormal];
    [self.checkoutButton setTitle:[ShareLocale textFromKey:@"check-out"] forState:UIControlStateNormal];
    
    
    [self.viewSwitcherButton setBackgroundImage:[UIImage imageNamed:[ShareLocale textFromKey:@"switch_listview"]] forState:UIControlStateNormal];
    [self.viewSwitcherButton setBackgroundImage:[UIImage imageNamed:[ShareLocale textFromKey:@"switch_listview"]] forState:UIControlStateSelected];
    [self.viewSwitcherButton setBackgroundImage:[UIImage imageNamed:[ShareLocale textFromKey:@"switch_listview"]] forState:UIControlStateHighlighted];
    [self.viewListSwitchButton setBackgroundImage:[UIImage imageNamed:[ShareLocale textFromKey:@"switch_calendar"]] forState:UIControlStateNormal];
    [self.viewListSwitchButton setBackgroundImage:[UIImage imageNamed:[ShareLocale textFromKey:@"switch_calendar"]] forState:UIControlStateSelected];
    [self.viewListSwitchButton setBackgroundImage:[UIImage imageNamed:[ShareLocale textFromKey:@"switch_calendar"]] forState:UIControlStateHighlighted];
    
    
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    //FIX_RELOAD_PAGE
    //[self refreshEventListForSelectedDate];
    //[self loadData];
}


-(void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    //FIX_RELOAD_PAGE
    //[self resetEventDetailView];
    
}




- (void)dealloc {
    [_cacheVisitFormData release];
    [_visitDateLabel release];
    [_storeNameLabel release];
    [_plannedStartDateLabel release];
    [_plannedEndDateLabel release];
    [_checkinTimeLabel release];
    [_checkoutTimeLabel release];
    [_remarksLabel release];
    [_checkinButton release];
    [_checkoutButton release];
    [_logcallButton release];
    [_visitWithLabel release];
    [_documentLabel release];
    [_todoLabel release];
    [_trashButton release];
    [_visitFormButton release];
    [_sopFormButton release];
    [_mspFormButton release];
    [_calendarSidebar release];
    [_listviewSidebar release];
    [_filterButton release];
    [_listviewTitleLabel release];
    [_viewSwitcherButton release];
    [_viewListSwitchButton release];
    [_documentTableView release];
    [_toDoTableView release];
    [_createVisitPlanButton release];
    [_headerTitleLabel release];
    [_plannedStartLabel release];
    [_plannedEndLabel release];
    [_checkinLabel release];
    [_checkoutLabel release];
    [_purposeLabel release];
    [_formVisitWithLabel release];
    [_todoName release];
    [_todoCategory release];
    [_todoDur release];
    [_todoEsitmated release];
    [_todoUsed release];
    [_todoDone release];
    [_docName release];
    [_commentLabel release];
    [_commentValueLabel release];
    [_mailButton release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Handle button event
//open visitation form survey list
- (IBAction)onClickVisitationFormButton:(id)sender {
    BOOL isCheckouted = [self.eventObj objectForKey:@"emfa__Check_Out_Time__c"]!=(id)[NSNull null];
    if ( visitFormNum > 0 ){
        
        SurveyFormViewController *vc = [[SurveyFormViewController alloc] initWithSurveyMode:SurveyFormModePreviewSurvey andSurveyId:VisitForm_SurveyId editable: !isCheckouted ];
        vc.saveDelegate = self;
        [vc setType:SurveyFormTypeVisitForm];
        [vc setCurrVisitPlan:[self getFieldDisplayWithFieldName:@"Id" withDictionary:self.eventObj]];
        [vc setCurrVisitPlanName:[self getFieldDisplayWithFieldName:@"Name" withDictionary:self.eventObj]];
        [vc setCurrSurveyMasterName: [ShareLocale textFromKey:@"surveytitle_visit"]];
        [self.navigationController pushViewController:vc  animated:YES];
        [vc release];
    }else{
        SurveyFormViewController *vc = [[SurveyFormViewController alloc] initWithSurveyMode:SurveyFormModeCreateSurvey andSurvey:nil  editable: !isCheckouted];
        vc.saveDelegate = self;
        [vc setType:SurveyFormTypeVisitForm];
        [vc setCurrVisitPlan:[self getFieldDisplayWithFieldName:@"Id" withDictionary:self.eventObj]];
        [vc setCurrVisitPlanName:[self getFieldDisplayWithFieldName:@"Name" withDictionary:self.eventObj]];
        [vc setCurrSurveyMasterName: [ShareLocale textFromKey:@"surveytitle_visit"] ];
        [self.navigationController pushViewController:vc  animated:YES];
        [vc release];
    }
}


- (IBAction)onClickEditPlanButton:(id)sender{
    if(![[VFDAO sharedInstance] isOnlineMode]){
        [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:[ShareLocale textFromKey:@"sorry"] message:[ShareLocale textFromKey:@"dialog_error_offline_action_not_allowed"] ];
        return;
    }
    
    CreateVisitPlanViewController* vc = [[CreateVisitPlanViewController alloc] initWithDictionary:self.eventObj];
    vc.delegate = self;
    [self.navigationController pushViewController: vc animated:YES];
    [vc release];
}

//open SOP form survey list
- (IBAction)onClickSOPFormButton:(id)sender {
    BOOL isCheckouted = [self.eventObj objectForKey:@"emfa__Check_Out_Time__c"]!=(id)[NSNull null];

    if ( sopFormNum > 0 ){
        SurveyFormViewController *vc = [[SurveyFormViewController alloc] initWithSurveyMode:SurveyFormModePreviewSurvey andSurveyId:SOPForm_SurveyId editable:!isCheckouted ];
        vc.saveDelegate = self;
        [vc setType:SurveyFormTypeSOPForm];
        [vc setCurrVisitPlan:[self getFieldDisplayWithFieldName:@"Id" withDictionary:self.eventObj]];
        [vc setCurrVisitPlanName:[self getFieldDisplayWithFieldName:@"Name" withDictionary:self.eventObj]];
        [vc setCurrSurveyMasterName:@"Store Audit"];
        
        [self.navigationController pushViewController:vc  animated:YES];
        [vc release];
    }else{
        SurveyFormViewController *vc = [[SurveyFormViewController alloc] initWithSurveyMode:SurveyFormModeCreateSurvey andSurvey:nil editable:!isCheckouted];
        vc.saveDelegate = self;
        [vc setType:SurveyFormTypeSOPForm];
        [vc setCurrVisitPlan:[self getFieldDisplayWithFieldName:@"Id" withDictionary:self.eventObj]];
        [vc setCurrVisitPlanName:[self getFieldDisplayWithFieldName:@"Name" withDictionary:self.eventObj]];
        [vc setCurrSurveyMasterName:@"Store Audit"];
        [self.navigationController pushViewController:vc  animated:YES];
        [vc release];
    }
}

//open MSP form survey list
- (IBAction)onClickMSPFormButton:(id)sender {
    BOOL isCheckouted = [self.eventObj objectForKey:@"emfa__Check_Out_Time__c"]!=(id)[NSNull null];

    if ( mspNum > 0 ){
        SurveyFormViewController *vc = [[SurveyFormViewController alloc] initWithSurveyMode:SurveyFormModePreviewSurvey andSurveyId:MSPForm_SurveyId editable:!isCheckouted];
        vc.saveDelegate = self;
        [vc setType:SurveyFormTypeMSPForm];
        [vc setCurrVisitPlan:[self getFieldDisplayWithFieldName:@"Id" withDictionary:self.eventObj]];
        [vc setCurrVisitPlanName:[self getFieldDisplayWithFieldName:@"Name" withDictionary:self.eventObj]];
        [vc setCurrSurveyMasterName:@"MSP Form"];
        [self.navigationController pushViewController:vc  animated:YES];
        [vc release];
    }else{
        SurveyFormViewController *vc = [[SurveyFormViewController alloc] initWithSurveyMode:SurveyFormModeCreateSurvey andSurvey:nil editable:!isCheckouted];
        vc.saveDelegate = self;
        [vc setType:SurveyFormTypeMSPForm];
        [vc setCurrVisitPlan:[self getFieldDisplayWithFieldName:@"Id" withDictionary:self.eventObj]];
        [vc setCurrVisitPlanName:[self getFieldDisplayWithFieldName:@"Name" withDictionary:self.eventObj]];
        [vc setCurrSurveyMasterName:@"MSP Form"];
        [self.navigationController pushViewController:vc  animated:YES];
        [vc release];
    }
}

//open form page for visit plan(new appointment)
- (IBAction)onClickNewVisitPlanButton:(id)sender {
    
    NSDate* date;
    if(self.selectedDate!=nil){
        date = self.selectedDate;
    }else{
        date = [NSDate date];
    }
    NSCalendar *calendar= [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] autorelease];
    NSCalendarUnit unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSDateComponents *dc = [calendar components:unitFlags fromDate:date];
    CreateVisitPlanViewController* vc = [[CreateVisitPlanViewController alloc] initWithYear:dc.year month:dc.month day:dc.day];
    
    vc.delegate = self;
    [self.navigationController pushViewController: vc animated:YES];
    [vc release];
}

-(void)onEditVisitPlanComplete{
    
    if(![NSThread isMainThread]){
        [self performSelectorOnMainThread:@selector(onEditVisitPlanComplete) withObject:nil waitUntilDone:NO];
        return;
    }
    NSLog(@"onEditVisitPlanComplete");
    [self refreshEventListForSelectedDate];
    [self loadData];
}

- (IBAction)onClickLogCallButton:(id)sender {
    
    if(self.commentPhotoPopup==nil){
        UIView* view = (UIView*) sender;
        
        NSMutableArray* storeCommentArray = [NSMutableArray array];
        for(NSDictionary* com in self.commentArray){
            if([com[@"Store__c"] isEqualToString:self.eventObj[@"emfa__Store__c"]] ){
                [storeCommentArray addObject:com];
            }
        }
        
        CommentPhotoViewController* vc = [[CommentPhotoViewController alloc ] initWithCommentArray: storeCommentArray];
        vc.delegate = self;
        self.commentPhotoPopup = [[[UIPopoverController alloc] initWithContentViewController:vc] autorelease];
        self.commentPhotoPopup.delegate = self;
        [self.commentPhotoPopup presentPopoverFromRect: view.frame inView: view.superview permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
        
        [self.commentPhotoPopup setPopoverContentSize: CGSizeMake(430, 600)];

    }

    
    
    
//    CommentFormViewController *vc = [[CommentFormViewController alloc] initWithData:@{
//                @"storeId": [self getFieldDisplayWithFieldName:@"Store__c" withDictionary: self.eventObj],
//                @"storeName": [self getFieldDisplayWithFieldName:@"Store__r.Name" withDictionary: self.eventObj]
//                }];
//
//    [self.navigationController pushViewController: vc animated:YES];
//    [vc release];
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    self.commentPhotoPopup = nil;
}


- (IBAction)onClickHome:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)onClickBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onCheckinButtonClicked:(id)sender {
    if( [self.eventObj objectForKey:@"emfa__Check_In_Time__c"]!=(id)[NSNull null]  ){
        NSLog(@"Already checkin before");
        return;
    }
    [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:[ShareLocale textFromKey:@"check-in"] message:[ShareLocale textFromKey:@"check-in-confirm"] yesHanlder:^(id data) {
        [self startCheckin];
    } noHandler:^(id data) {
        
    }
     ];
    
}

- (IBAction)onCheckoutButtonClicked:(id)sender {
    if( [self.eventObj objectForKey:@"emfa__Check_Out_Time__c"]!=(id)[NSNull null]  ){
        NSLog(@"Already checkout before");
        return;
    }
    
    VisitPlanPreviewViewController* vc = [[VisitPlanPreviewViewController alloc] initWithVisitPlan: self.eventObj visitForm:self.cacheVisitFormData todoTasks:self.toDoTaskArray docTasks:self.documentTaskArray];
    [self.navigationController pushViewController:vc animated:YES];
    [vc release];
    

}

-(UITableViewCell*) getParentTableCell:(UIView*)view{
    NSInteger level = 0;
    UIView* parent = [view superview];
    while(level<10){
        level++;
        if([[parent class] isSubclassOfClass:[UITableViewCell class]]){
            return (UITableViewCell*)parent;
        }
        parent = [parent superview];
    }
    NSAssert(NO, @"No parent found match TableViewCell class");
    return nil;
}

//- (void)onClickToDoCheckBox:(id)sender{
//    UIButton *checkBox = sender;
//    
//    BOOL newVal = ![checkBox isSelected] ;
//    
//    UITableViewCell* cell =  [self getParentTableCell: sender]; //(UITableViewCell*)[[checkBox superview] superview];
//    
//    int index = [self.toDoTableView indexPathForCell:cell].row; //cell.tag;
//    NSMutableDictionary* task = [NSMutableDictionary dictionaryWithDictionary:[self.toDoTaskArray objectAtIndex:index]];
//    UILabel* actualDuratonLabel = (UILabel*)[cell viewWithTag:905];
//
//    if(newVal){
//        [[GeneralUiTool shareInstance] showInutNumberDialogWithTitle:[ShareLocale textFromKey:@"visit_dialog_completetask"] message: [ShareLocale textFromKey:@"visit_dialog_completetask_desc"] yesHanlder:^(id data) {
//            NSString* minutes = (NSString*)data;
//            NSLog(@"USER INPUT = %@", minutes );
//            if([DataHelper isIntegerForString:minutes]){
//                int mins = [minutes intValue];
//                [checkBox setSelected:newVal];
//                actualDuratonLabel.text = [NSString stringWithFormat:[ShareLocale textFromKey:@"task_sub_min"], mins];
//                [self updateStatusForTask:task index:index minutes:mins];
//            }else{
//                [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:[ShareLocale textFromKey:@"error"] message:[ShareLocale textFromKey:@"validation_error"]];
//            }
//            
//        } noHandler:^(id data) {
//            
//        }];
//    }else{
//        [checkdBox setSelected:newVal];
//        actualDuratonLabel.text = [NSString stringWithFormat: [ShareLocale textFromKey:@"task_sub_min"], 0];
//
//        [self updateStatusForTask:task index:index minutes:0];
//    }
//    
//}


-(void) updateStatusForTask:(NSMutableDictionary*)task index:(int)index minutes:(int)mins comment:(NSString*)comment done:(BOOL)done{
    NSString* status;
//    if([[task objectForKey:@"Status"] isEqualToString:@"Completed"]){
    if( !done ){
        status = @"Not Started";
        [task setObject:status forKey:@"Status"];
        [task setObject:[NSNumber numberWithInt:mins] forKey:@"emfa__Actual_Duration__c"];
        [task setObject:comment forKey:@"Comment__c"];
    }else{
        status = @"Completed";
        [task setObject:status forKey:@"Status"];
        [task setObject:[NSNumber numberWithInt:mins] forKey:@"emfa__Actual_Duration__c"];
        [task setObject:comment forKey:@"Comment__c"];
    }
    
    [self.toDoTaskArray replaceObjectAtIndex:index withObject:task];
    
    [self tickToDoCheckBox:[task objectForKey:@"Id"] status:status minutes:mins comment:comment];
}


- (void)onClickDocumentCheckBox:(id)sender{
    NSLog(@"onClickDocumentCheckBox");
    UIButton *checkBox = sender;
    
    if ([checkBox isSelected] ){
        [checkBox setSelected:NO];
    }else {
        [checkBox setSelected:YES];
    }
    
    UITableViewCell* cell = (UITableViewCell*)[[checkBox superview] superview];
    int index = cell.tag;
    
    NSMutableDictionary* task = [NSMutableDictionary dictionaryWithDictionary:[self.documentTaskArray objectAtIndex:index]];
    NSString* status;
    if([[task objectForKey:@"Status"] isEqualToString:@"Completed"]){
        status = @"Not Started";
        [task setObject:status forKey:@"Status"];
    }else{
        status = @"Completed";
        [task setObject:status forKey:@"Status"];
    }
    
    [self.documentTaskArray replaceObjectAtIndex:index withObject:task];
    
    [self tickDocCheckBox:[task objectForKey:@"Id"] status:status];
}

- (IBAction)onFilterButtonClicked:(id)sender {
    UIView* view = (UIView*) sender;
    
    CGRect f = CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, view.frame.size.height);
    
    VFDAO* vf = [VFDAO sharedInstance];
//    NSDictionary* data = [vf.cache objectForKey:@"store_daily_analyse"];
//    NSArray* storeCodeArray = [data allKeys];
//    storeCodeArray = [storeCodeArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
//        NSString* store1 = (NSString*)obj1;
//        NSString* store2 = (NSString*)obj2;
//        return [store1 compare:store2 ];
//    }];
//
//    NSMutableArray* storeArray = [NSMutableArray array];
//    [storeArray addObject:@{@"Id":@"", @"Name":[ShareLocale textFromKey:@"all"], @"Store_Code__c":@""}];
//
//    for(NSString* code in storeCodeArray){
//        NSDictionary* storeData = [data objectForKey:code];
//        NSString* name = [storeData objectForKey:@"name"];
//        NSString* storeId = [storeData objectForKey:@"store_id"];
//        if([name rangeOfString:code].location == NSNotFound){
//            name = [NSString stringWithFormat:@"%@ %@", code, name];
//        }
//        [storeArray addObject:@{@"Id":storeId, @"Name":name, @"Store_Code__c":code}];
//    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray* allStores = [vf getStoresByBrandCode: [defaults objectForKey: USER_PREFER_ACCOUNT_CODE] countryCode:[defaults objectForKey: USER_PREFER_COUNTRY_CODE] needOptionAll:YES];
    
    StoreSelectionViewController* storePopup = [[[StoreSelectionViewController alloc] initWithStoreList: allStores] autorelease];

    
    storePopup.delegate = self;
    self.storeListPopup = [[[UIPopoverController alloc] initWithContentViewController:storePopup] autorelease];
    self.storeListPopup.delegate = self;
    [self.storeListPopup presentPopoverFromRect:f  inView:self.listviewSidebar permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];

    
}

- (IBAction)onMoveToTrashButtonClicked:(id)sender {
    [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"" message: [ShareLocale textFromKey:@"visit_dialog_remove_plan"] yesHanlder:^(id data) {
        [self moveCurentVisitPlanToTrash];
    } noHandler:^(id data) {
        
    }];
}

- (IBAction)onSidebarSwitcherClicked:(id)sender {
    UIButton* button = (UIButton*) sender;
    [button setUserInteractionEnabled:NO];
    if(button.tag == 700){
        UIView* view = self.calendarSidebar;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.2];
        view.frame = CGRectMake(-view.frame.size.width, view.frame.origin.y, view.frame.size.width, view.frame.size.height);
               
        [UIView commitAnimations];
        [self performSelector:@selector(transitionInListViewSidebar) withObject:nil afterDelay:0.2];
        
    }
    else if (button.tag == 600){
        UIView* view = self.listviewSidebar;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.2];
        view.frame = CGRectMake(-view.frame.size.width, view.frame.origin.y, view.frame.size.width, view.frame.size.height);
        
        [UIView commitAnimations];
        [self performSelector:@selector(transitionInCalendarSidebar) withObject:nil afterDelay:0.2];

    }
}

- (IBAction)onSendMailButtonClicked:(id)sender {
    
    if([[VFDAO sharedInstance] isOnlineMode]){
        [[GeneralUiTool shareInstance] showInutDialogWithTitle:@"" message:[ShareLocale textFromKey:@"dialog_mail_hints"] yesHanlder:^(id data) {
            
            postResendMailRequest = [[VFDAO sharedInstance] sendMailOfVisitationId:self visitPlanId:[self.eventObj objectForKey:@"Id"] email:data];
            if(postResendMailRequest!=nil){
                [self showLoadingPopup:self.view];
            }
            
        } noHandler:^(id data) {
            
        }];
    }else{
        [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:[ShareLocale textFromKey:@"sorry"] message:[ShareLocale textFromKey:@"dialog_error_offline_action_not_allowed"]];
    }
    
    
    
}

-(void)transitionInCalendarSidebar{
    mode = 0;
    self.listviewSidebar.hidden = YES;
    self.calendarSidebar.hidden = NO;
    
    UIView* view = self.calendarSidebar;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.2];
    view.frame = CGRectMake(0, view.frame.origin.y, view.frame.size.width, view.frame.size.height);
    [UIView commitAnimations];
    UIButton* button = (UIButton*)[self.calendarSidebar viewWithTag:700];
    [button setUserInteractionEnabled:YES];
    
    [self loadData];
}


-(void)transitionInListViewSidebar{
    mode = 1;
    self.calendarSidebar.hidden = YES;
    self.listviewSidebar.hidden = NO;
        
    UIView* view = self.listviewSidebar;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.2];
    view.frame = CGRectMake(0, view.frame.origin.y, view.frame.size.width, view.frame.size.height);
    [UIView commitAnimations];
    UIButton* button = (UIButton*)[self.listviewSidebar viewWithTag:600];
    [button setUserInteractionEnabled:YES];

    [self loadData];
}


#pragma mark - Survey Save Delegate
-(void) onSurveyFormSave:(NSDictionary*)survey{
    NSLog(@"VisitationViewController onSurveyFormSave survey=%@", survey);
    if ([[survey objectForKey:@"Type__c"] isEqualToString:SURVEY_TYPE_VISIT_FORM]){
        visitFormNum ++;
        VisitForm_SurveyId = [survey objectForKey:@"Id"];
        self.cacheVisitFormData = survey;
        
        [self updateCacheDataWithType:SURVEY_TYPE_VISIT_FORM formId:VisitForm_SurveyId arrayData:self.recordArray arrayData2:self.recordEventArray];
//        [self updateCacheDataWithType:SURVEY_TYPE_VISIT_FORM formId:VisitForm_SurveyId arrayData:self.recordEventArray];
    }
    else if ([[survey objectForKey:@"Type__c"] isEqualToString:SURVEY_TYPE_SOP_Audit]){
        sopFormNum++;
        SOPForm_SurveyId = [survey objectForKey:@"Id"];
        [self updateCacheDataWithType:SURVEY_TYPE_SOP_Audit formId:SOPForm_SurveyId arrayData:self.recordArray arrayData2:self.recordEventArray];
//        [self updateCacheDataWithType:SURVEY_TYPE_SOP_Audit formId:SOPForm_SurveyId arrayData:self.recordEventArray];
    }
    else if ([[survey objectForKey:@"Type__c"] isEqualToString:SURVEY_TYPE_MSP_SURVEY]){
        mspNum++;
        MSPForm_SurveyId = [survey objectForKey:@"Id"];
        [self updateCacheDataWithType:SURVEY_TYPE_MSP_SURVEY formId:MSPForm_SurveyId arrayData:self.recordArray arrayData2:self.recordEventArray];
//        [self updateCacheDataWithType:SURVEY_TYPE_MSP_SURVEY formId:MSPForm_SurveyId arrayData:self.recordEventArray];
    }else{
        NSLog(@"ERROR!!! Incorrect save result");
        
    }
    [self reloadVisitPlanSurveyButtonsStatus];
}


-(void) updateCacheDataWithType:(NSString*)stype formId:(NSString*)formId arrayData:(NSMutableArray*)recordArray arrayData2:(NSMutableArray*)record2Array{
    //testing here
    
    //NSLog(@"recordArray#1 = %@", self.recordArray);
    
    NSMutableDictionary* cloneEventObj = [self.eventObj mutableCopy];
    NSUInteger index = [recordArray indexOfObject: self.eventObj];
    NSUInteger index2 = [record2Array indexOfObject: self.eventObj];
    NSMutableDictionary* surveyDict = self.eventObj[@"emfa__Surveys__r"];
    if(surveyDict==nil || surveyDict==(id)[NSNull null]){
        surveyDict = [@{@"records":@[]} mutableCopy];
    }else{
        surveyDict = [surveyDict mutableCopy];
    }
    [cloneEventObj setObject:surveyDict forKey:@"emfa__Surveys__r"];
    [recordArray replaceObjectAtIndex:index withObject:cloneEventObj];
    [record2Array replaceObjectAtIndex:index2 withObject:cloneEventObj];

    NSArray* records = surveyDict[@"records"];
    NSMutableArray* mrecords = [records mutableCopy];
    BOOL found = NO;
    for(NSDictionary* r in  mrecords){
        if([r[@"Id"] isEqualToString:formId]){
            found = YES;
        }
    }
    if(!found){
        [mrecords addObject:@{@"Type__c": stype, @"Id":formId}];
    }
    surveyDict[@"records"] = mrecords;
    
    VFDAO* vf = [VFDAO sharedInstance];
    //[vf runStandardUpsertTable:DB_TABLE_VISIT_PLAN records:@[cloneEventObj] spec: DB_SPEC_VISIT_PLAN ];
    [vf runRealUpsertTable:DB_TABLE_VISIT_PLAN records:@[cloneEventObj] spec:DB_SPEC_VISIT_PLAN];
    
    self.eventObj = cloneEventObj;
    
    //NSLog(@"recordArray#2 = %@", self.recordArray);
}

-(void) reloadVisitPlanSurveyButtonsStatus{
    NSLog(@"VisitationViewController reloadVisitPlanSurveyButtonsStatus");
    UIImage *orangeImage = [UIImage imageNamed:@"button_orange.png"];
    UIImage *greenImage = [UIImage imageNamed:@"button_green.png"];
    
    if ( visitFormNum > 0 ){
        [self.visitFormButton setBackgroundImage:greenImage forState:UIControlStateNormal];
    }
    else {
        [self.visitFormButton setBackgroundImage:orangeImage forState:UIControlStateNormal];
    }
    if ( sopFormNum > 0 ){
        [self.sopFormButton setBackgroundImage:greenImage forState:UIControlStateNormal];
    }
    else {
        [self.sopFormButton setBackgroundImage:orangeImage forState:UIControlStateNormal];
    }
    if ( mspNum > 0 ){
        [self.mspFormButton setBackgroundImage:greenImage forState:UIControlStateNormal];
    }
    else {
        [self.mspFormButton setBackgroundImage:orangeImage forState:UIControlStateNormal];
    }
    
    id checkIn = [self.eventObj objectForKey:@"emfa__Check_In_Time__c"];
    id checkOut = [self.eventObj objectForKey:@"emfa__Check_Out_Time__c"];

    if(checkIn!=nil && checkIn!=(id)[NSNull null]){
        //already checked in
       if (checkOut!=nil && checkOut!=(id)[NSNull null]) {
           //also checked out
           if ( mspNum > 0 ) self.mspFormButton.hidden = NO;
           else self.mspFormButton.hidden = YES;
           if ( sopFormNum > 0 ) self.sopFormButton.hidden = NO;
           else self.sopFormButton.hidden = YES;
           if ( visitFormNum > 0 ) self.visitFormButton.hidden = NO;
           else self.visitFormButton.hidden = YES;
       }else{
           //still editing, not checkout
           self.mspFormButton.hidden = NO;
           self.sopFormButton.hidden = NO;
           self.visitFormButton.hidden = NO;
       }
    }else{
        //not checked in
        self.mspFormButton.hidden = YES;
        self.sopFormButton.hidden = YES;
        self.visitFormButton.hidden = YES;
        
    }
    
}


#pragma mark - Calendar delegate
-(void)calendarView:(VRGCalendarView *)calendarView switchedToMonth:(int)month targetHeight:(float)targetHeight animated:(BOOL)animated {
    self.selectedMonth = month;

    [self refreshEventListForSelectedDate];
    [self loadData];

}

-(void)calendarView:(VRGCalendarView *)calendarView dateSelected:(NSDate *)date {
    NSLog(@"calendar  dateSelected selectedDate = %@",date);
    
    self.selectedDate = date; //nsdate is in GMT here
    
    [self refreshEventListForSelectedDate];
}


#pragma mark - Popup delegate
-(void) onSelectdStore:(NSDictionary*) storeObj{
    [self.storeListPopup dismissPopoverAnimated:YES];
    self.storeListPopup = nil;
    
    NSString* storeId = [storeObj objectForKey:@"Id"];
    self.filterStoreId = ([storeId isEqualToString:@""])?nil:storeId;
    self.filterStoreCode = ([storeId isEqualToString:@""])?[ShareLocale textFromKey:@"all_store"]: [storeObj objectForKey:@"emfa__Store_Code__c"];
    
    [self loadData];
}


#pragma mark - Table view datasource/delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ( tableView == listTableView || tableView == dayListTableView){
        return [self.recordEventArray count];
    }else if ( tableView == self.toDoTableView){
        return [self.toDoTaskArray count];
    }else if ( tableView == self.documentTableView ){
        return [self.documentTaskArray count];
    }else return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ( tableView == self.documentTableView ){
        return;
    }
    
    if ( tableView == self.toDoTableView ) {
        if(self.eventObj!=nil){
            if([self.eventObj objectForKey:@"emfa__Check_In_Time__c"]!=nil && [self.eventObj objectForKey:@"emfa__Check_In_Time__c"]!=(id)[NSNull null] &&
               ([self.eventObj objectForKey:@"emfa__Check_Out_Time__c"]==nil || [self.eventObj objectForKey:@"emfa__Check_Out_Time__c"]==(id)[NSNull null])
               ){
                [self.taskCompleteView setHidden:NO];
                self.taskCompleteView.indexPath = indexPath;
        
                NSDictionary* data = [self.toDoTaskArray objectAtIndex:indexPath.row];
                [self.taskCompleteView refreshAll:data];
            }
        }
    }
    else if ( tableView == listTableView || tableView == dayListTableView){
        
    NSDictionary* data = [self.recordEventArray objectAtIndex:indexPath.row];
    self.eventObj = data;

    
    self.logcallButton.hidden = NO;
    self.trashButton.hidden = NO;

    //Update Document CheckList && ToDo List
    if ( self.documentTaskArray == nil )self.documentTaskArray = [NSMutableArray array];
    if ( self.toDoTaskArray == nil )self.toDoTaskArray = [NSMutableArray array];
    [self.documentTaskArray removeAllObjects];
    [self.toDoTaskArray removeAllObjects];
    
    todoTaskDurationSum = 0;
    NSDictionary *taskDict = [self.eventObj objectForKey:@"Tasks"];
    if ( taskDict != nil&& taskDict != (id)[NSNull null]){
        NSArray *taskArr = [taskDict objectForKey:@"records"];
        if ( taskArr != nil && taskArr != (id)[NSNull null]){
            
            for ( NSDictionary *task in taskArr ){
                NSLog(@"Task record: %@", [self getFieldDisplayWithFieldName:@"RecordType.Name" withDictionary:task]);
                NSString *recordTypeName = [self getFieldDisplayWithFieldName:@"RecordType.Name" withDictionary:task];
                if ( [recordTypeName isEqualToString:@"Document"] ){
                    [self.documentTaskArray addObject:task];
                }else if ( [recordTypeName isEqualToString:@"ToDo"] ){
                    [self.toDoTaskArray addObject:task];
                    todoTaskDurationSum += [[task objectForKey:@"emfa__Estimated_Duration__c"] intValue];
                }
            }
        }
    }

    
    if(self.eventObj[@"CreatedBy"]==nil){
        NSArray* tasks = self.eventObj[@"toDoTasks"];
        NSArray* docs = self.eventObj[@"documentTasks"];
        [self.documentTaskArray removeAllObjects];
        [self.toDoTaskArray removeAllObjects];
        [self.documentTaskArray addObjectsFromArray:docs];
        
        for(NSDictionary* todo in tasks){
            NSMutableDictionary*md = [todo mutableCopy];
            md[@"emfa__Estimated_Duration__c"] = todo[@"Duration"];
            md[@"emfa__Category__c"] = todo[@"Category"];
            todoTaskDurationSum += [md[@"emfa__Estimated_Duration__c"] intValue];
            [self.toDoTaskArray addObject:md];
        }
        
    }
    
    
    NSLog(@"docList: %@", self.documentTaskArray);
    NSLog(@"toDoList: %@", self.toDoTaskArray);
    
    [self refreshTaskListContentView];
    //Update Bottom Survey Button
    mspNum = 0;
    visitFormNum = 0;
    sopFormNum = 0;
    
    NSDictionary *surveyDict = [self.eventObj objectForKey:@"emfa__Surveys__r"];
    if ( surveyDict != nil&& surveyDict != (id)[NSNull null]){
        NSArray *surveyArr = [surveyDict objectForKey:@"records"];
        if ( surveyArr != nil && surveyArr != (id)[NSNull null]){
            for ( NSDictionary *survey in surveyArr ){
                if ([[survey objectForKey:@"Type__c"] isEqualToString:SURVEY_TYPE_VISIT_FORM]){
                    visitFormNum ++;
                    VisitForm_SurveyId = [survey objectForKey:@"Id"];
                    self.cacheVisitFormData = survey;//cache it so it can be pass to preview later
                }else if ([[survey objectForKey:@"Type__c"] isEqualToString:SURVEY_TYPE_SOP_Audit]){
                    sopFormNum++;
                    SOPForm_SurveyId = [survey objectForKey:@"Id"];
                }else if ([[survey objectForKey:@"Type__c"] isEqualToString: SURVEY_TYPE_MSP_SURVEY]){
                    mspNum ++;
                    MSPForm_SurveyId = [survey objectForKey:@"Id"];
                }
            }
        }
    }
    [self refreshEventDetailForSelectedEvent:data];
    }
    
}

- (NSIndexPath*)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ( tableView == listTableView || tableView == dayListTableView){
        NSDictionary* data = [self.recordEventArray objectAtIndex: indexPath.row];
        
        if ( [data objectForKey:@"isEvent"] != nil && [data objectForKey:@"isEvent"] != (id)[NSNull null] && [[data objectForKey:@"isEvent"] isEqualToString:@"true"] ) {
            return nil;
        }
    }
    
    return indexPath;
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString* identifer = nil;
    if(_tableView == listTableView || _tableView == dayListTableView){
        identifer = @"VisitPlanCell";
    }else{
        identifer = @"VisitPlanTaskCell";
    }
    
    UITableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:identifer];
    if(cell == nil){
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifer] autorelease];
        UIView *selectionColor = [[[UIView alloc] init] autorelease];
        selectionColor.backgroundColor = TABLE_ROW_HIGHLIGHT_COLOR;
        cell.selectedBackgroundView = selectionColor;
        
        cell.textLabel.hidden = YES;
        cell.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        if ( _tableView == listTableView || _tableView == dayListTableView){
            
            [self createNewTextLabelWithDefaultStyle: CGRectMake(30, 5, 100, 40) parent:cell tag: 901];
            [self createNewTextLabelWithDefaultStyle: CGRectMake(130, 5, 180, 40) parent:cell tag: 902];
            
            UIImageView* imgV = [[UIImageView alloc] initWithFrame:CGRectMake(5, 15, 20, 20)];
            imgV.tag = 903;
            [cell addSubview:imgV];

        }
        else if ( _tableView == self.toDoTableView || _tableView == self.documentTableView ){
            UILabel* itemNameLbl = [self createNewTextLabelWithDefaultStyle: CGRectMake(10, 0, 200, 40) parent:cell tag: 901];
            itemNameLbl.lineBreakMode = NSLineBreakByCharWrapping;
            itemNameLbl.numberOfLines = 2;
            UILabel* itemCatLbl = [self createNewTextLabelWithDefaultStyle: CGRectMake(210, 0, 150, 40) parent:cell tag: 902 alignment:NSTextAlignmentLeft];
            itemCatLbl.lineBreakMode = NSLineBreakByCharWrapping;
            itemCatLbl.numberOfLines = 2;
            [self createNewTextLabelWithDefaultStyle: CGRectMake(325, 0, 70, 40) parent:cell tag: 903 alignment:NSTextAlignmentLeft];
            [self createNewTextLabelWithDefaultStyle: CGRectMake(395, 0, 70, 40) parent:cell tag: 904 alignment:NSTextAlignmentLeft];
            [self createNewTextLabelWithDefaultStyle: CGRectMake(465, 0, 70, 40) parent:cell tag: 905 alignment:NSTextAlignmentLeft];
            [self createNewButtonWithDefaultStyle:CGRectMake(534, 2, 36, 36) parent:cell tag: 906];
        }
    }
    cell.tag = indexPath.row;
    if ( _tableView == listTableView || _tableView == dayListTableView){
        NSDictionary* data = [self.recordEventArray objectAtIndex: indexPath.row];
        BOOL isEvent = NO;
        if ( [data objectForKey:@"isEvent"] != nil && [data objectForKey:@"isEvent"] != (id)[NSNull null] && [[data objectForKey:@"isEvent"] isEqualToString:@"true"] ) {
            isEvent = YES;
            
//            cell.selectionStyle = UITableViewCellSelectionStyleNone;
//            cell.userInteractionEnabled = NO;
        }
//        else {
//            cell.selectionStyle = UITableViewCellSelectionStyleDefault;
//            cell.userInteractionEnabled = YES;
//        }
        
        UIImageView* imgIcon = (UIImageView*)[cell viewWithTag:903];
        UILabel* dateLabel = (UILabel*)[cell viewWithTag:901];
        UILabel* storeNameLabel = (UILabel*)[cell viewWithTag:902];
        storeNameLabel.frame = CGRectMake(storeNameLabel.frame.origin.x, storeNameLabel.frame.origin.y, 180, storeNameLabel.frame.size.height);
        storeNameLabel.lineBreakMode = NSLineBreakByCharWrapping;
        storeNameLabel.numberOfLines = 2;
        
        NSDateFormatter* df = [[[NSDateFormatter alloc] init] autorelease];
        if(mode==0) [df setDateFormat:@"HH:mm"];
        else [df setDateFormat:@"dd/MM HH:mm"];
        
        GeneralUiTool* ap = [GeneralUiTool shareInstance];
        [df setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT: ap.timezoneSecond ]];
        
        if ( isEvent ) {
            [df setDateFormat:@"dd/MM HH:mm"];
            NSString* timeStr = [df stringFromDate: [[GeneralUiTool getSFDateFormatter] dateFromString: [data objectForKey:@"ActivityDateTime"] ]];
            dateLabel.text = timeStr;
        }
        else {
            NSString* timeStr = [df stringFromDate: [[GeneralUiTool getSFDateFormatter] dateFromString: [data objectForKey:@"emfa__Start_Time__c"] ]];
            dateLabel.text = timeStr;
        }
        
        NSString* storeName = [self getFieldDisplayWithFieldName:@"emfa__Store__r.Name" withDictionary:data];
        NSString* creatorName = [self getFieldDisplayWithFieldName:@"CreatedBy.Name" withDictionary:data];
        storeNameLabel.text = [NSString stringWithFormat:@"%@ (%@)", storeName, creatorName];
        if ( isEvent ) {
            NSString* eventName = [self getFieldDisplayWithFieldName:@"Subject" withDictionary:data];
            storeNameLabel.text = [NSString stringWithFormat:@"%@ (E)", eventName];
        }
        //
        //Planned_Start_Date_Time__c:XXX
        //Name:XXX
        //CreatedBy {Name:XXX}
        
        //Offline only record must have no CreatedBy information
        
        BOOL isCheckIn = [data objectForKey:@"emfa__Check_In_Time__c"]!=(id)[NSNull null];
        BOOL isCheckOut = [data objectForKey:@"emfa__Check_Out_Time__c"]!=(id)[NSNull null];

        if(isCheckOut && isCheckIn){
            imgIcon.image = [UIImage imageNamed:@"tick_listicon.png"];
        }
        else if (isCheckIn){
            imgIcon.image = [UIImage imageNamed:@"pencil_listicon.png"];
        }
        else{
            imgIcon.image = [UIImage imageNamed:@"calendar_listicon.png"];
        }

        if(data[@"CreatedBy"]==nil && !isEvent){
            dateLabel.text = data[@"emfa__Start_Time__c"];
            storeNameLabel.text = [NSString stringWithFormat:@"%@ (%@)", data[@"Input_Store_Code__c"], [ShareLocale textFromKey:@"offline_only"]];
            imgIcon.image = [UIImage imageNamed:@"calendar_listicon.png"];
        }

        
    }
    else if ( _tableView == self.toDoTableView ){

        NSDictionary *toDoTask = [self.toDoTaskArray objectAtIndex:indexPath.row];
        NSString * toDoSubject = [toDoTask objectForKey:@"Subject"];
        NSString *estDurationStr = [toDoTask objectForKey:@"emfa__Estimated_Duration__c"];
        NSString *actDurationStr = [toDoTask objectForKey:@"emfa__Actual_Duration__c"];
        NSString *categoryStr = [toDoTask objectForKey:@"emfa__Category__c"];
        NSString *statusStr = [toDoTask objectForKey:@"Status"];
        
        UILabel *nameLabel = (UILabel *)[cell viewWithTag:901];
        UILabel *categoryLabel = (UILabel *)[cell viewWithTag:902];
        UILabel *percentageLabel = (UILabel *)[cell viewWithTag:903];
        UILabel *estDuerationLabel = (UILabel *)[cell viewWithTag:904];
        UILabel *actDurationLabel = (UILabel *)[cell viewWithTag:905];
        UIButton *checkBox = (UIButton *)[cell viewWithTag:906];
        
        checkBox.hidden = YES;
        if(self.eventObj!=nil){
            if([self.eventObj objectForKey:@"emfa__Check_In_Time__c"]!=nil && [self.eventObj objectForKey:@"emfa__Check_In_Time__c"]!=(id)[NSNull null] &&
               ([self.eventObj objectForKey:@"emfa__Check_Out_Time__c"]==nil || [self.eventObj objectForKey:@"emfa__Check_Out_Time__c"]==(id)[NSNull null])
               ){
                checkBox.hidden = NO;
            }
        }
        
        
        if ( toDoSubject != nil && toDoSubject != (id)[NSNull null] ){
            nameLabel.text = [NSString stringWithFormat:@"%i. %@", indexPath.row+1, toDoSubject];
            nameLabel.numberOfLines = 0;
            categoryLabel.text = [ShareLocale textFromKey: categoryStr];
            categoryLabel.frame = CGRectMake(categoryLabel.frame.origin.x, categoryLabel.frame.origin.y, 100, categoryLabel.frame.size.height);
            if(todoTaskDurationSum>0){
                int pct = [[NSNumber numberWithFloat:([estDurationStr intValue] * 100.0f / todoTaskDurationSum) ] intValue];
                percentageLabel.text = [NSString stringWithFormat: @"%d%%", pct];
            }else{
                percentageLabel.text = @"N/A";
            }
            
            estDuerationLabel.text = [NSString stringWithFormat: [ShareLocale textFromKey:@"task_sub_min"], [estDurationStr intValue]];
            if ( actDurationStr != nil && actDurationStr != (id)[NSNull null ]){
                actDurationLabel.text = [NSString stringWithFormat: [ShareLocale textFromKey:@"task_sub_min"], [actDurationStr intValue]];
            }else{
                actDurationLabel.text = @"";
            }
            [checkBox setSelected:[statusStr isEqualToString:@"Completed"]];
            [checkBox setTitle:@"" forState:UIControlStateNormal];
            [checkBox setTitle:@"" forState:UIControlStateSelected];
            [checkBox setBackgroundImage:[UIImage imageNamed:@"checkbox_off.png"] forState:UIControlStateNormal];
            [checkBox setBackgroundImage:[UIImage imageNamed:@"tickbox_on.png"] forState:UIControlStateSelected];
//            [checkBox addTarget:self action:@selector(onClickToDoCheckBox:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        if ( (indexPath.row %2) == 0 )[cell setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.05f]];
        else [cell setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.0f]];
    }
    else if ( _tableView == self.documentTableView ){
        NSDictionary *docTask = [self.documentTaskArray objectAtIndex:indexPath.row];
        NSString * docSubject = [docTask objectForKey:@"Subject"];
        NSString *statusStr = [docTask objectForKey:@"Status"];

        UILabel *nameLabel = (UILabel *)[cell viewWithTag:901];
        UIButton *checkBox = (UIButton *)[cell viewWithTag:906];
        
        checkBox.hidden = YES;
        if(self.eventObj!=nil){
            if([self.eventObj objectForKey:@"emfa__Check_In_Time__c"]!=nil && [self.eventObj objectForKey:@"emfa__Check_In_Time__c"]!=(id)[NSNull null] &&
               ([self.eventObj objectForKey:@"emfa__Check_Out_Time__c"]==nil || [self.eventObj objectForKey:@"emfa__Check_Out_Time__c"]==(id)[NSNull null])
               ){
                checkBox.hidden = NO;
            }
        }
        
        if ( docSubject != nil && docSubject != (id)[NSNull null] ){
            
            nameLabel.text = [NSString stringWithFormat:@"%i. %@", indexPath.row+1, docSubject];
            [checkBox setSelected:[statusStr isEqualToString:@"Completed"]];

            [checkBox setTitle:@"" forState:UIControlStateNormal];
            [checkBox setTitle:@"" forState:UIControlStateSelected];
            [checkBox setBackgroundImage:[UIImage imageNamed:@"checkbox_off.png"] forState:UIControlStateNormal];
            [checkBox setBackgroundImage:[UIImage imageNamed:@"tickbox_on.png"] forState:UIControlStateSelected];
            [checkBox addTarget:self action:@selector(onClickDocumentCheckBox:) forControlEvents:UIControlEventTouchUpInside];
        }
        if ( (indexPath.row %2) == 0 )[cell setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.05f]];
        else [cell setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.0f]];
    }
    return cell;

}



#pragma mark - Network delegate
- (void) loadData{
    if(queryVisitPlanRequest==nil){
        [self showLoadingPopup: self.view];
        queryVisitPlanRequest = [[VFDAO sharedInstance] selectVisitationPlansWithDelegate:self];
    }
}

- (void) loadEvent{
    if (queryEventRequest == nil) {
        [self showLoadingPopup:self.view];
        queryEventRequest = [[VFDAO sharedInstance] selectEventWithDelegate:self];
    }
}

- (void) loadComment{
    //if(self.queryCommentRequest == nil){
        self.queryCommentRequest = [[VFDAO sharedInstance] selectCommentsWithDelegate:self withStoreID: nil ];
    //}
}

-(void) startCheckin{
    [self showLoadingPopup: self.view];
    
    postCheckinRequest = [[VFDAO sharedInstance] updateVisitPlanId:[self.eventObj objectForKey:@"Id"] data:@{@"emfa__Check_In_Time__c":[[GeneralUiTool getSFDateFormatter] stringFromDate:[NSDate date]]} delegate:self];
    
}


-(void) tickToDoCheckBox:(NSString*)taskId status:(NSString*)status minutes:(int)mins comment:(NSString*)comment{
    [self showLoadingPopup: self.view];
    self.requestTodoID = taskId;
    
    postToDoTaskRequest = [[VFDAO sharedInstance] updateStatusOfTask:taskId parentId:[self.eventObj objectForKey:@"Id"] status:status duration:[NSNumber numberWithInt:mins] comment:comment delegate:self];
    
}

-(void) tickDocCheckBox:(NSString*)taskId status:(NSString*)status{
    [self showLoadingPopup: self.view];
    self.requestDocID = taskId;
    
    postDocTaskRequest = [[VFDAO sharedInstance] updateStatusOfTask:taskId parentId:[self.eventObj objectForKey:@"Id"] status:status duration:nil delegate:self];
    
}


-(void) moveCurentVisitPlanToTrash{
    [self showLoadingPopup: self.view];
    postTrashRequest = [[VFDAO sharedInstance] updateVisitPlanId:[self.eventObj objectForKey:@"Id"] data:@{@"emfa__Active__c":[NSNumber numberWithBool:NO]} delegate:self];

}

-(void) daoRequest:(DAORequest *)request queryOnlineDataError:(NSString *)response code:(int)code{
    if(postResendMailRequest == request){
        [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"error" message:@"dialog_invalid_email"];
    }
    [self hideLoadingPopup];
    NSLog(@"VisitationViewController:queryOnlineDataError()");
    [super handleQueryDataErrorCode:code description:response];
}

-(void) daoRequest:(DAORequest *)request queryOnlineDataSuccess:(NSArray *)records{
    NSLog(@"VisitationViewController:queryOnlineDataSuccess()");
    if(postResendMailRequest == request){
        [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"" message:[ShareLocale textFromKey:@"dialog_mail_complete"] ];
        [self hideLoadingPopup];
        NSLog(@"mail send done %@", records);
    }
    else if (self.queryCommentRequest == request){
        self.commentArray = [NSMutableArray arrayWithArray: records ];
        [self hideLoadingPopup];
    }
    else if(queryVisitPlanRequest == request){
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString* brandCode = [defaults objectForKey: USER_PREFER_ACCOUNT_CODE];
        NSString* countryCode = [defaults objectForKey:USER_PREFER_COUNTRY_CODE];
        
        self.recordArray = [[NSMutableArray alloc] init];
        for(NSDictionary* obj in records){
            if( [[DataHelper getFieldDisplayWithFieldName:@"emfa__Store__r.emfa__Account__r.Code__c" withDictionary:obj default:@"" ] isEqualToString:brandCode] && [[DataHelper getFieldDisplayWithFieldName:@"emfa__Store__r.emfa__Account__r.Country_Code__c" withDictionary:obj default:@"" ] isEqualToString:countryCode] ){
                [self.recordArray addObject:obj];
            }
        }
        
        NSDateFormatter* df = [[[NSDateFormatter alloc] init] autorelease];
        [df setDateFormat:DEFAULT_SIMPLE_DATE_FORMAT];
        
        NSMutableSet* tempSet = [[NSMutableSet alloc] init];
        for(NSDictionary* event in self.recordArray){
            if([[event objectForKey:@"emfa__Active__c"] boolValue]){
                NSString* visitDateStr = [event objectForKey:@"emfa__Visit_Date__c"];
                [tempSet addObject: [df dateFromString:visitDateStr] ];
            }
        }
        
//        [self loadEvent];
        
        if([NSThread isMainThread]){
            [self.calendarView markDates:[tempSet allObjects]];
            //[self hideLoadingPopup];
            [self onReloadEventDataInUI];
            [self loadComment];
        }else{
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [self.calendarView markDates:[tempSet allObjects]];
                //[self hideLoadingPopup];
                [self onReloadEventDataInUI];
                [self loadComment];
            });
        }
        queryVisitPlanRequest = nil;
    }
    else if ( queryEventRequest == request ) {
        // Visit Plan
        NSDateFormatter* df = [[[NSDateFormatter alloc] init] autorelease];
        [df setDateFormat:DEFAULT_SIMPLE_DATE_FORMAT];
        
        NSMutableSet* tempSet = [[NSMutableSet alloc] init];
        for(NSDictionary* event in self.recordArray){
            if([[event objectForKey:@"emfa__Active__c"] boolValue]){
                NSString* visitDateStr = [event objectForKey:@"emfa__Visit_Date__c"];
                [tempSet addObject: [df dateFromString:visitDateStr] ];
            }
        }
        
        for(NSDictionary* obj in records){
            NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
            dict = [obj mutableCopy];
            [dict setValue:@"true" forKey:@"isEvent"];
            [dict setValue:[obj objectForKey:@"StartDateTime"] forKey:@"emfa__Start_Time__c"];
            [self.recordArray addObject:dict];
            
            NSString* actDateStr = [obj objectForKey:@"ActivityDate"];
            [tempSet addObject: [df dateFromString:actDateStr] ];
        }
        
        
//        [df setDateFormat:DEFAULT_DATE_TIME_FORMAT];
//        NSArray* tempArray;
//        tempArray = [self.recordArray sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
//                NSDate *first = [df dateFromString:[(NSDictionary*)a objectForKey:@"Planned_Start_Date_Time__c"]];
//                NSDate *second = [df dateFromString:[(NSDictionary*)b objectForKey:@"Planned_Start_Date_Time__c"]];
//                return [first compare:second];
//            
//        }];
        
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"emfa__Start_Time__c"
                                                     ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sortedArray = [self.recordArray sortedArrayUsingDescriptors:sortDescriptors];
        self.recordArray = [sortedArray mutableCopy];
        
        if([NSThread isMainThread]){
            [self.calendarView markDates:[tempSet allObjects]];
            //[self hideLoadingPopup];
            [self onReloadEventDataInUI];
            [self loadComment];
        }else{
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [self.calendarView markDates:[tempSet allObjects]];
                //[self hideLoadingPopup];
                [self onReloadEventDataInUI];
                [self loadComment];
            });
        }
        queryEventRequest = nil;
    }
    else if (postCheckinRequest == request){
        [self hideLoadingPopupWithFadeout];
        NSMutableDictionary* newEventObj = [[[NSMutableDictionary alloc] initWithDictionary: self.eventObj] autorelease];
        [newEventObj setValue:[[GeneralUiTool getSFDateFormatter] stringFromDate:[NSDate date]] forKey:@"emfa__Check_In_Time__c"];
        [self replaceEventObject:self.eventObj withNewObject:newEventObj];
        self.eventObj = newEventObj;
        [self refreshEventDetailForSelectedEvent:newEventObj];
    }
    else if (postTrashRequest == request){
        self.eventObj = nil;
        __block VisitationViewController* me = self;

        dispatch_async(dispatch_get_main_queue(), ^{
            [me resetEventDetailView];
            [me loadData];
        });

    }else if (postToDoTaskRequest == request){
        [self hideLoadingPopup];
    }
    else if (postDocTaskRequest == request){
        [self hideLoadingPopup];
    }

}

#pragma mark - Other Business logic

-(void)onReloadEventDataInUI{
    
    if(mode==0){
        self.recordEventArray = [[[NSMutableArray alloc] init] autorelease];
        for(NSDictionary* event in self.recordArray){
            if(([self isEventInSelectedDate:event forDateKey:@"emfa__Visit_Date__c"] || [self isEventInSelectedDate:event forDateKey:@"ActivityDate"]) && [[event objectForKey:@"emfa__Active__c"] boolValue] ){
                [self.recordEventArray addObject:event];
            }
        }
        [self.calendarView setNeedsDisplay];
        [dayListTableView reloadData];
    }
    else{
        //list view mode.
        
        self.listviewTitleLabel.text = [NSString stringWithFormat:[ShareLocale textFromKey:@"visit_allplan_title"], self.filterStoreCode];
        
        self.recordEventArray = [[[NSMutableArray alloc] init] autorelease];
        for(NSDictionary* event in self.recordArray){
            if([[event objectForKey:@"emfa__Active__c"] boolValue] ){
                if(self.filterStoreId!=nil){
                    //if filter exist, only matching record being displayed!
                    if([[event objectForKey:@"emfa__Store__c"] isEqualToString:self.filterStoreId]){
                        [self.recordEventArray addObject:event];
                    }
                }
                else{
                    //no filter exist, show all records
                    [self.recordEventArray addObject:event];
                }
            }
        }
        [listTableView reloadData];
    }
}

-(void) refreshEventListForSelectedDate{
    UILabel* eventTitleButton = (UILabel*)[ self.view viewWithTag:302];
    
    NSCalendar *calendar= [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] autorelease];
    NSCalendarUnit unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSDateComponents *dc = [calendar components:unitFlags fromDate:self.selectedDate];
    
    self.selectedMonth = dc.month;
    
    eventTitleButton.text = [NSString stringWithFormat:[ShareLocale textFromKey:@"visit_list_subtitle"], dc.day,  dc.month, dc.year];
    
    [self onReloadEventDataInUI];
    [self resetEventDetailView];
    
    
    self.logcallButton.hidden = YES;
    self.mspFormButton.hidden = YES;
    self.trashButton.hidden = YES;
    self.visitFormButton.hidden = YES;
    self.sopFormButton.hidden = YES;
    
    
}


-(BOOL) isEventInSelectedDate:(NSDictionary*) data forDateKey:(NSString*)dateKey{
    NSString* dateStr = [data objectForKey:dateKey];
    NSArray* parts = [dateStr componentsSeparatedByString:@"-"];
    
    NSCalendar *calendar= [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] autorelease];
    NSCalendarUnit unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSDateComponents *dc = [calendar components:unitFlags fromDate:self.selectedDate];
    
    if(dc.day == [parts[2] intValue] && dc.month == [parts[1] intValue] && dc.year == [parts[0] intValue]){
        return YES;
    }
    return NO;
}

-(void)resetEventDetailView{
    self.visitDateLabel.text = @"N/A";
    self.storeNameLabel.text = @"N/A";
    self.plannedStartDateLabel.text = @"N/A";
    self.plannedEndDateLabel.text = @"N/A";
    self.checkinTimeLabel.text = @"N/A";
    self.checkoutTimeLabel.text = @"N/A";
    self.remarksLabel.text = @"N/A";
    self.commentValueLabel.text = @"N/A";
//    self.remarksLabel.frame = CGRectMake(self.remarksLabel.frame.origin.x, 132, 150, 22);
//    self.remarksLabel.textAlignment = NSTextAlignmentRight;
    
    self.visitWithLabel.text = @"N/A";
//    self.visitWithLabel.frame = CGRectMake(self.visitWithLabel.frame.origin.x, 132, 182, 22);
//    self.visitWithLabel.textAlignment = NSTextAlignmentRight;

    self.editPlanButton.hidden = YES;
    self.checkinButton.hidden = YES;
    self.checkoutButton.hidden = YES;
    self.documentLabel.text = @"N/A";
    [self.documentLabel sizeToFit];
    self.todoLabel.text = @"N/A";
    [self.todoLabel sizeToFit];
    self.logcallButton.hidden = YES;
    self.mspFormButton.hidden = YES;
    self.trashButton.hidden = YES;
    self.visitFormButton.hidden = YES;
    self.sopFormButton.hidden = YES;
    
    [self.toDoTaskArray removeAllObjects];
    [self.toDoTableView reloadData];
    
    [self.documentTaskArray removeAllObjects];
    [self.documentTableView reloadData];

}

-(void) refreshTaskListContentView{
    NSLog(@"refreshTaskListContentView");
    [self.toDoTableView reloadData];
    [self.documentTableView reloadData];
}


-(void) refreshEventDetailForSelectedEvent:(NSDictionary*) data{
    if(![NSThread isMainThread]){
        [self performSelectorOnMainThread:@selector(refreshEventDetailForSelectedEvent:) withObject:data waitUntilDone:NO];
        return;
    }
    GeneralUiTool* ap = [GeneralUiTool shareInstance];
    
    self.visitDateLabel.text = [data objectForKey:@"emfa__Visit_Date__c"];
    self.storeNameLabel.text = [self getFieldDisplayWithFieldName:@"emfa__Store__r.Name" withDictionary:data];
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    [df setTimeStyle:NSDateFormatterShortStyle];
    [df setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT: ap.timezoneSecond ]];
    self.plannedStartDateLabel.text = [df stringFromDate: [[GeneralUiTool getSFDateFormatter] dateFromString: [data objectForKey:@"emfa__Start_Time__c"] ]];
    self.plannedEndDateLabel.text =[df stringFromDate: [[GeneralUiTool getSFDateFormatter] dateFromString: [data objectForKey:@"emfa__End_Time__c"] ]];

    self.remarksLabel.text = [self getFieldDisplayWithFieldName:@"emfa__Remark__c" withDictionary:data];
//    self.remarksLabel.textAlignment = NSTextAlignmentLeft;
//    self.remarksLabel.frame = CGRectMake(self.remarksLabel.frame.origin.x, self.remarksLabel.frame.origin.y, 180, 61);
//    [self.remarksLabel sizeToFit];
    
    self.commentValueLabel.text = [self getFieldDisplayWithFieldName:@"Comment__c"  withDictionary:data];
    
    self.visitWithLabel.text = [self getFieldDisplayWithFieldName:@"emfa__Visit_With__c" withDictionary:data];
//    self.visitWithLabel.textAlignment = NSTextAlignmentRight;
//    self.visitWithLabel.frame = CGRectMake(self.visitWithLabel.frame.origin.x, self.visitWithLabel.frame.origin.y, 180, 61);
//    [self.visitWithLabel sizeToFit];
    
    [self.toDoTableView reloadData];
    [self.documentTableView reloadData];
    
    self.documentLabel.text = @"";
    self.todoLabel.text = @"";

    NSDateFormatter* fullDateDF = [[NSDateFormatter alloc] init];
    [fullDateDF setDateFormat:@"dd/MM hh:mm a"];
    [fullDateDF setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT: ap.timezoneSecond ]];

    if( [data objectForKey:@"emfa__Check_In_Time__c"]!=(id)[NSNull null]  ){
        self.checkinTimeLabel.text = [fullDateDF stringFromDate: [[GeneralUiTool getSFDateFormatter] dateFromString: [data objectForKey:@"emfa__Check_In_Time__c"] ]];
        self.checkinButton.userInteractionEnabled = NO;
        self.checkinButton.hidden = YES;
        self.editPlanButton.hidden = YES;
        self.mailButton.hidden = YES;
    }
    else{
        self.checkinTimeLabel.text = @"";
        self.checkinButton.userInteractionEnabled = YES;
        self.checkinButton.hidden = NO;
        self.editPlanButton.hidden = NO;
        self.mailButton.hidden = YES;
    }
    if( [data objectForKey:@"emfa__Check_Out_Time__c"]!=(id)[NSNull null]  ){
        self.checkoutTimeLabel.text = [fullDateDF stringFromDate: [[GeneralUiTool getSFDateFormatter] dateFromString: [data objectForKey:@"emfa__Check_Out_Time__c"] ]];
        self.checkoutButton.userInteractionEnabled = NO;
        self.checkoutButton.hidden = YES;
        self.mailButton.hidden = NO;
    }
    else if( [data objectForKey:@"emfa__Check_In_Time__c"]!=(id)[NSNull null]  ){
        self.checkoutTimeLabel.text = @"";
        self.checkoutButton.userInteractionEnabled = YES;
        self.checkoutButton.hidden = NO;
        self.mailButton.hidden = YES;
    }
    else{
        self.checkoutTimeLabel.text = @"N/A";
        self.checkoutButton.hidden = YES;
        self.checkoutButton.userInteractionEnabled = NO;
        self.mailButton.hidden = YES;
    }
    
    if(data[@"CreatedBy"]==nil){
        self.plannedStartDateLabel.text = data[@"emfa__Start_Time__c"];
        self.plannedEndDateLabel.text = data[@"emfa__End_Time__c"];
        self.storeNameLabel.text = [NSString stringWithFormat:@"%@ (%@)", data[@"Input_Store_Code__c"], [ShareLocale textFromKey:@"offline_only"]];
        self.mailButton.hidden = YES;
        self.checkoutButton.hidden = YES;
        self.checkinButton.hidden = YES;
        self.editPlanButton.hidden = YES;
        self.trashButton.hidden = YES;
        self.logcallButton.hidden = YES;
    }
    
    
    [self reloadVisitPlanSurveyButtonsStatus];
    
    
    
    
}


-(void) replaceEventObject:(NSDictionary*) oldData withNewObject:(NSDictionary*)newData{
    for(int i=0; i< [self.recordArray count]; i++){
        NSDictionary* data = [self.recordArray objectAtIndex:i];
        if(data == oldData){
            [self.recordArray replaceObjectAtIndex:i withObject:newData];
            break;
        }
    }
    
    for(int i=0; i< [self.recordEventArray count]; i++){
        NSDictionary* data = [self.recordEventArray objectAtIndex:i];
        if(data == oldData){
            [self.recordEventArray replaceObjectAtIndex:i withObject:newData];
            break;
        }
    }
}


#pragma mark - Popup Comment Photo delegate
-(void) onSelectedCommentPhoto:(NSDictionary*)data{
    
    [self.commentPhotoPopup dismissPopoverAnimated:NO];
    self.commentPhotoPopup = nil;
    
//    CommentViewController* vc = [[CommentViewController alloc] initWithCommentData:data];
//    [vc setModalPresentationStyle:UIModalPresentationCurrentContext]; //UIModalPresentationOverCurrentContext
//    self.modalPresentationStyle = UIModalPresentationCurrentContext |UIModalPresentationFormSheet;
//    [self.navigationController presentViewController:vc animated:NO completion:nil];
    
    CommentPopupView* popup =      [[[NSBundle mainBundle] loadNibNamed:@"CommentPopupView" owner:self options:nil] objectAtIndex:0];
    [popup reloadWithCommentData: data];
    [self.view addSubview:popup];
}

-(void) onClickedNewCommentPhoto{
    [self.commentPhotoPopup dismissPopoverAnimated:NO];
    self.commentPhotoPopup = nil;
    
    NSDictionary* data = nil;
    if (self.eventObj!=nil){
        data = @{
                 @"storeId": [self getFieldDisplayWithFieldName:@"emfa__Store__c" withDictionary:self.eventObj],
                 @"storeName": [self getFieldDisplayWithFieldName:@"emfa__Store__r.Name" withDictionary:self.eventObj]
                 };
    }
    
    CommentFormViewController* vc = [[CommentFormViewController alloc] initWithData:data];
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - SaveCommentViewDelegate
-(void) onSaveCommentSuccess{
//    dispatch_async(dispatch_get_main_queue(), ^{
        [self showLoadingPopup:self.view];
        [self loadComment];
//    });
}

#pragma mark - Others
- (void)onTaskCompleteComplete:(NSDictionary*)dict{
    NSLog(@"onTaskCompleteComplete: %@", dict);
    
    UITableViewCell* cell = [self.toDoTableView cellForRowAtIndexPath:(NSIndexPath*)[dict objectForKey:@"Index"]];
    
    BOOL newVal = [[dict objectForKey:@"Done"] boolValue];
    if ( newVal ) {
        int mins = [[dict objectForKey:@"Duration"] intValue];
        NSString* comment = [dict objectForKey:@"Comment"];
    
        UILabel* actualDuratonLabel = (UILabel*)[cell viewWithTag:905];
        UIButton* checkboxButton = (UIButton*)[cell viewWithTag:906];
        [checkboxButton setSelected:newVal];
        actualDuratonLabel.text = [NSString stringWithFormat:[ShareLocale textFromKey:@"task_sub_min"], mins];
    
        int index = [self.toDoTableView indexPathForCell:cell].row; //cell.tag;
        NSMutableDictionary* task = [NSMutableDictionary dictionaryWithDictionary:[self.toDoTaskArray objectAtIndex:index]];
    
        [self updateStatusForTask:task index:index minutes:mins comment:comment done:newVal];
    }

}

@end
