//
//  CommentPhotoViewController.m
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 7/1/15.
//  Copyright (c) 2015 vf.com. All rights reserved.
//

#import "CommentPhotoViewController.h"
#import "WebImageView.h"
#import "GeneralUiTool.h"
#import "ShareLocale.h"
#import "DataHelper.h"

@interface CommentPhotoViewController ()

@end

@implementation CommentPhotoViewController



-(id) initWithCommentArray:(NSArray*)dataArray{
    
    if(self = [super init]){
        self.dataArray = dataArray;
    }
    
    return self;
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView reloadData];
    
    self.headerText.text = [ShareLocale textFromKey:@"viist_logcall"];

}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataArray count];
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 110;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary* data = [self.dataArray objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CommentPhotoCell"];
    
    if(cell == nil){
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CommentPhotoCell"] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.imageView.hidden = YES;
        
        WebImageView* imgView = [[WebImageView alloc] initWithFrame:CGRectMake(5, 5, 100, 100)];
        imgView.tag = 901;
        imgView.backgroundColor = [UIColor colorWithWhite:.95f alpha:1];
        [cell addSubview: imgView];
        [imgView release];
        
        UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(110, 5, 310, 20)];
        titleLabel.tag = 902;
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
        [cell addSubview: titleLabel];
        
        UILabel* descLabel = [[UILabel alloc] initWithFrame: CGRectMake(110, 30, 310, 60)];
        descLabel.numberOfLines = 3;
        descLabel.tag = 903;
        descLabel.textColor = [UIColor grayColor];
        descLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
        [cell addSubview: descLabel];
        
        UILabel* dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(110, 88, 310, 15)];
        dateLabel.tag = 904;
        dateLabel.textAlignment = NSTextAlignmentRight;
        dateLabel.textColor = [UIColor lightGrayColor];
        dateLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
        [cell addSubview: dateLabel];

    }
    
    UILabel* titleLabel = (UILabel*)[cell viewWithTag:902];
    UILabel* descLabel = (UILabel*)[cell viewWithTag:903];
    UILabel* dateLabel = (UILabel*)[cell viewWithTag:904];
    
    titleLabel.text = gfrd(data,@"Subject__c",@"");
    descLabel.frame = CGRectMake(110, 30, 310, 60);
    descLabel.text = gfrd(data,@"Description__c",@"");
    [descLabel sizeToFit];
    
    NSDateFormatter* simpleDF = [[NSDateFormatter alloc] init];
    [simpleDF setDateFormat:@"yyyy-MM-dd"];
    NSDateFormatter* sfDF = [GeneralUiTool getSFDateFormatter];
    
    dateLabel.text = [simpleDF stringFromDate: [sfDF dateFromString: data[@"ActivityDate__c"] ] ];

    
    WebImageView* imgView = (WebImageView*)[cell viewWithTag:901];

    NSDictionary* mediaDict = [data objectForKey:@"emfa__Shared_Media__r"];
    if(mediaDict!=nil && mediaDict!=(id)[NSNull null] && mediaDict[@"records"]!=nil && mediaDict[@"records"] !=(id)[NSNull null] ){
        NSArray* mediaArray = mediaDict[@"records"];
        if([mediaArray count]>0){
            NSDictionary* mediaData = mediaArray[0];
            if( mediaData[@"emfa__FeedItemId__c"]!=nil && mediaData[@"emfa__FeedItemId__c"]!=[NSNull null] ){
                cell.imageView.image = nil;
                [imgView loadImageWithFeedID: mediaData[@"emfa__FeedItemId__c"] completeBlock:^(WebImageView *imgView, UIImage *img) {

                }];
            }else{
                imgView.image = nil;

            }
        }
        else{
            imgView.image = nil;

        }
    }else{
        imgView.image = nil;

    }
    
    
    
    
    return cell;

}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.delegate!=nil){
        [self.delegate onSelectedCommentPhoto: self.dataArray[indexPath.row]];
    }
}


- (IBAction)onClickNewButton:(id)sender {
    if(self.delegate!=nil){
        [self.delegate onClickedNewCommentPhoto];
    }
}


- (void)dealloc {
    [_tableView release];
    [_headerText release];
    [super dealloc];
}
@end
