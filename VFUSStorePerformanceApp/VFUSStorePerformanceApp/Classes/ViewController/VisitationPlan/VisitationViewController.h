//
//  VisitationViewController.h
//  emFace_Ray
//
//  Created by cheng chok kwong on 11/4/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SFRestAPI.h"
#import "SFRestRequest.h"
#import "VFDAO.h"

#import "BasicViewController.h"
#import "VRGCalendarView.h"
#import "SurveyFormViewController.h"
#import "StoreSelectionViewController.h"
#import "CreateVisitPlanViewController.h"
#import "CommentPhotoViewController.h"
#import "CommentFormViewController.h"
#import "TaskCompleteView.h"

@interface VisitationViewController : BasicViewController<VRGCalendarViewDelegate, UITableViewDataSource, UITableViewDelegate, DAODelegate, UIPopoverControllerDelegate, StoreSelectionViewControllerDelegate, CreateVisitPlanDelegate, SurveyFormSaveDelegate, CommentPhotoSelectionDelegate, CommentFormViewDelegate, TaskCompleteDelegate>{
    
    int todoTaskDurationSum;
    
    UITableView* dayListTableView;
    UITableView* listTableView;
    

    
    DAORequest *queryVisitPlanRequest;
    DAORequest *queryEventRequest;
    DAOBaseRequest *postCheckinRequest;
//    SFRestRequest *postCheckoutRequest;
    DAOBaseRequest *postTrashRequest;
    DAOBaseRequest *postToDoTaskRequest;
    DAOBaseRequest *postDocTaskRequest;
    DAOBaseRequest* postResendMailRequest;
    
    int mode;
    int mspNum;
    int visitFormNum;
    int sopFormNum;
    NSString *MSPForm_SurveyId;
    NSString *VisitForm_SurveyId;
    NSString *SOPForm_SurveyId;
}

@property (retain, nonatomic) DAOBaseRequest *queryCommentRequest;

@property (retain, nonatomic) IBOutlet UIPopoverController *commentPhotoPopup;

@property (retain, nonatomic) IBOutlet UIView *calendarSidebar;
@property (retain, nonatomic) IBOutlet UIView *listviewSidebar;
@property (retain, nonatomic) IBOutlet UIButton *filterButton;
@property (retain, nonatomic) IBOutlet UILabel *listviewTitleLabel;

@property (retain, nonatomic) UIPopoverController* storeListPopup;


@property (retain, nonatomic) IBOutlet UILabel *visitDateLabel;
@property (retain, nonatomic) IBOutlet UILabel *storeNameLabel;
@property (retain, nonatomic) IBOutlet UILabel *plannedStartDateLabel;
@property (retain, nonatomic) IBOutlet UILabel *plannedEndDateLabel;
@property (retain, nonatomic) IBOutlet UILabel *checkinTimeLabel;
@property (retain, nonatomic) IBOutlet UILabel *checkoutTimeLabel;
@property (retain, nonatomic) IBOutlet UILabel *visitWithLabel;
@property (retain, nonatomic) IBOutlet UILabel *documentLabel;
@property (retain, nonatomic) IBOutlet UILabel *todoLabel;
@property (retain, nonatomic) IBOutlet UITableView *documentTableView;
@property (retain, nonatomic) IBOutlet UITableView *toDoTableView;

@property (retain, nonatomic) IBOutlet UILabel *commentLabel;
@property (retain, nonatomic) IBOutlet UILabel *commentValueLabel;

@property (retain, nonatomic) IBOutlet UILabel *remarksLabel;
@property (retain, nonatomic) IBOutlet UIButton *checkinButton;
@property (retain, nonatomic) IBOutlet UIButton *checkoutButton;
@property (retain, nonatomic) IBOutlet UIButton *logcallButton;
@property (nonatomic, retain) VRGCalendarView* calendarView;
@property (nonatomic, retain) NSDate* selectedDate;
@property (nonatomic, assign) int selectedMonth;
@property (nonatomic, retain) NSMutableArray* recordArray;
@property (nonatomic, retain) NSMutableArray* recordEventArray;
@property (nonatomic, retain) NSMutableArray* documentTaskArray;
@property (nonatomic, retain) NSMutableArray* toDoTaskArray;
@property (nonatomic, retain) NSMutableArray* commentArray;

@property (retain, nonatomic) IBOutlet UIButton *mailButton;
@property (retain, nonatomic) IBOutlet UIButton *trashButton;
@property (retain, nonatomic) IBOutlet UIButton *visitFormButton;
@property (retain, nonatomic) IBOutlet UIButton *sopFormButton;
@property (retain, nonatomic) IBOutlet UIButton *mspFormButton;

@property (retain, nonatomic) IBOutlet UIButton *editPlanButton;


@property (retain, nonatomic) IBOutlet UIButton *viewSwitcherButton;
@property (retain, nonatomic) IBOutlet UIButton *viewListSwitchButton;


@property (retain, nonatomic) IBOutlet UIButton *createVisitPlanButton;
@property (retain, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (retain, nonatomic) IBOutlet UILabel *plannedStartLabel;
@property (retain, nonatomic) IBOutlet UILabel *plannedEndLabel;
@property (retain, nonatomic) IBOutlet UILabel *checkinLabel;
@property (retain, nonatomic) IBOutlet UILabel *checkoutLabel;
@property (retain, nonatomic) IBOutlet UILabel *purposeLabel;
@property (retain, nonatomic) IBOutlet UILabel *formVisitWithLabel;


@property (retain, nonatomic) IBOutlet UIButton *todoName;
@property (retain, nonatomic) IBOutlet UIButton *todoCategory;
@property (retain, nonatomic) IBOutlet UIButton *todoDur;
@property (retain, nonatomic) IBOutlet UIButton *todoEsitmated;
@property (retain, nonatomic) IBOutlet UIButton *todoUsed;
@property (retain, nonatomic) IBOutlet UIButton *todoDone;
@property (retain, nonatomic) IBOutlet UIButton *docName;


@property (retain, nonatomic) NSDictionary *eventObj;


@property (retain, nonatomic) NSString* filterStoreId;//SF store
@property (retain, nonatomic) NSString* filterStoreCode;//SF store
@property (retain, nonatomic) NSString* requestTodoID;//TO DO ITEM ID
@property (retain, nonatomic) NSString* requestDocID;//DOC ITEM ID
@property (retain, nonatomic) NSDictionary* cacheVisitFormData;

@property (retain, nonatomic) TaskCompleteView *taskCompleteView;

@property (assign) UIViewController* controllerDelegate;


- (IBAction)onClickHome:(id)sender;
- (IBAction)onClickBack:(id)sender;

- (IBAction)onClickVisitationFormButton:(id)sender;

- (IBAction)onClickSOPFormButton:(id)sender;

- (IBAction)onClickMSPFormButton:(id)sender;

- (IBAction)onClickNewVisitPlanButton:(id)sender;

- (IBAction)onClickLogCallButton:(id)sender;

- (IBAction)onClickEditPlanButton:(id)sender;

- (IBAction)onCheckinButtonClicked:(id)sender;

- (IBAction)onCheckoutButtonClicked:(id)sender;
- (IBAction)onFilterButtonClicked:(id)sender;

- (IBAction)onMoveToTrashButtonClicked:(id)sender;
- (IBAction)onSidebarSwitcherClicked:(id)sender;
- (IBAction)onSendMailButtonClicked:(id)sender;

-(void) refreshEventListForSelectedDate;

@end
