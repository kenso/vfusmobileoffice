//
//  TaskCompleteView.h
//  VFStorePerformanceApp_Dev2
//
//  Created by Tony Keung on 1/3/16.
//  Copyright (c) 2016 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GeneralSelectionPopup.h"

@protocol TaskCompleteDelegate <NSObject>

- (void)onTaskCompleteComplete:(NSDictionary *)dict;

@end


@interface TaskCompleteView : UIView< UITextViewDelegate>{
    BOOL editing;
}

- (id)initWithFrame:(CGRect)frame withDelegate:(id)delegate;


@property (assign, nonatomic) id <TaskCompleteDelegate> delegate;
@property (retain, nonatomic) NSString *errorMessage;

@property (retain, nonatomic) NSIndexPath *indexPath;
@property (retain, nonatomic) NSString *comment;
@property (assign, nonatomic) NSInteger duration;
@property (assign, nonatomic) NSString* status;

@property (retain, nonatomic) IBOutlet UIButton *titleLabel;
@property (retain, nonatomic) IBOutlet UILabel *commentLabel;
@property (retain, nonatomic) IBOutlet UILabel *durationLabel;
@property (retain, nonatomic) IBOutlet UILabel *doneLabel;

@property (retain, nonatomic) IBOutlet UITextView *commentTextView;
@property (retain, nonatomic) IBOutlet UITextView *durationTextView;

@property (retain, nonatomic) IBOutlet UIButton *confirmButton;
@property (retain, nonatomic) IBOutlet UIButton *cancelButton;
@property (retain, nonatomic) IBOutlet UIButton *checkboxButton;

@property (retain, nonatomic) UIView *rootView;

- (IBAction)onClickConfirmButton:(id)sender;
- (IBAction)onClickCancelButton:(id)sender;
- (IBAction)onClickCheckboxButton:(id)sender;


- (void)refreshAll:(NSDictionary*)dict;
- (void)resetAllData;
- (void)cleanAllData;

-(BOOL)isValidForm;

@end
