//
//  CreateVisitPlanViewController.m
//  VFStorePerformanceApp
//
//  Created by Developer 2 on 10/3/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "CreateVisitPlanViewController.h"
#import "Constants.h"
#import "PopupDateTimePicker.h"
#import "DatePickerSelectionViewController.h"
#import "GeneralUiTool.h"
#import "PreviewVisitPlanViewController.h"
#import "DataHelper.h"
#import "ShareLocale.h"

@interface CreateVisitPlanViewController ()

@end

@implementation CreateVisitPlanViewController

/*
 {
 "Check_In_Date_Time__c" = "<null>";
 "Check_Out_Date_Time__c" = "<null>";
 "Document_Checklist__c" = "<null>";
 Id = a0S9000000MMLZrEAP;
 Name = "Visit 4204 on 2014-06-22";
 "Need_MSP__c" = 0;
 "Need_SOP_Audit__c" = 0;
 "Need_Visit_Form__c" = 1;
 "Planned_End_Date_Time__c" = "2014-06-21T17:13:00.000+0000";
 "Planned_Start_Date_Time__c" = "2014-06-21T17:13:00.000+0000";
 "Remark__c" = "<null>";
 "Status__c" = Active;
 "Store__c" = a0O9000000EjpW0EAJ;
 "Store__r" =     {
 Name = "4204 - HOT - OCEAN TERMINAL";
 "Store_Code__c" = 4204;
 attributes =         {
 type = "emfa__Store__c";
 url = "/services/data/v28.0/sobjects/emfa__Store__c/a0O9000000EjpW0EAJ";
 };
 };
 "Survey__r" = "<null>";
 Tasks = "<null>";
 "To_Do_List__c" = "<null>";
 "Visit_Date__c" = "2014-06-22";
 "Visit_With__c" = "<null>";
 attributes =     {
 type = "emfa__Visitation_Plan__c";
 url = "/services/data/v28.0/sobjects/emfa__Visitation_Plan__c/a0S9000000MMLZrEAP";
 };
 }
 */
- (id) initWithDictionary:(NSDictionary*)data{
    self = [super initWithNibName:@"CreateVisitPlanViewController" bundle:nil];
    if (self) {
        self.tempData = [NSDictionary dictionaryWithDictionary: data];
        self.selectedStartDate = [NSDate date];
    }
    return self;
}



- (id) initWithYear:(int)year month:(int)month day:(int)day{
    self = [super initWithNibName:@"CreateVisitPlanViewController" bundle:nil];
    if (self) {
        self.tempData = nil;
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *components = [[NSDateComponents alloc] init];
        [components setDay:day];
        [components setMonth:month];
        [components setYear:year];

        NSDate *now = [NSDate date];
        NSDateComponents *components2 = [calendar components:NSHourCalendarUnit|NSMinuteCalendarUnit fromDate:now];
        
        [components setHour: components2.hour];
        [components setMinute: components2.minute];
        
        self.selectedStartDate = [calendar dateFromComponents:components];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    userAlreadyInput = NO;
    
    NSLog(@"tempData=%@", self.tempData);
    
    NSDateFormatter* df = [[NSDateFormatter alloc ] init];
    [df setDateFormat:DEFAULT_SIMPLE_DATE_FORMAT];
    self.visitDateLabel.text = [df stringFromDate:self.selectedStartDate];
    
    [df setDateFormat:@"HH:mm"];
    self.startTimeLabel.text = [df stringFromDate:self.selectedStartDate];
    self.endTimeLabel.text = [df stringFromDate:self.selectedStartDate];
    
    self.remarkLabel.text = @"";
    self.commentLabel.text = @"";
    
    [self.storeCodeButton setTitle:[ShareLocale textFromKey:@"select_store"] forState:UIControlStateNormal];
    
    self.remarkLabel.delegate = self;
    self.commentLabel.delegate = self;
    self.visitWithLabel.delegate = self;
    self.startTimeLabel.delegate = self;
    self.endTimeLabel.delegate = self;
    self.visitDateLabel.delegate = self;
    
    self.taskCreateView = [[TaskCreateView alloc] initWithFrame:self.view.frame withDelegate:self];
    [self.view addSubview:self.taskCreateView];
    [self.taskCreateView setHidden:YES];
    
    self.toDoTaskArray = [[NSMutableArray alloc] init];
    self.documentTaskArray = [[NSMutableArray alloc] init];
    [self.toDoTableView setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.1f]];
    [self.documentTableView setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.1f]];
    [self.toDoTableView setDataSource:self];
    [self.toDoTableView setDelegate:self];
    [self.documentTableView setDataSource:self];
    [self.documentTableView setDelegate:self];
    [self.toDoTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.documentTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    if(self.tempData != nil ){
        
        NSDateFormatter* sfdf = [GeneralUiTool getSFDateFormatter];
        
        [df setDateFormat:DEFAULT_SIMPLE_DATE_FORMAT];
        self.selectedStartDate = [sfdf dateFromString:[ self.tempData objectForKey:@"emfa__Start_Time__c"]];
        self.visitDateLabel.text = [df stringFromDate: self.selectedStartDate];
        [df setDateFormat:@"HH:mm"];
        self.startTimeLabel.text = [df stringFromDate: [sfdf dateFromString:[ self.tempData objectForKey:@"emfa__Start_Time__c"] ]];
        self.endTimeLabel.text = [df stringFromDate: [sfdf dateFromString:[ self.tempData objectForKey:@"emfa__End_Time__c"]]];
        
        self.remarkLabel.text = [DataHelper getFieldDisplayWithFieldName:@"emfa__Remark__c" withDictionary:self.tempData default:@""];
        self.commentLabel.text = [DataHelper getFieldDisplayWithFieldName:@"Comment__c" withDictionary:self.tempData default:@""];
        self.visitWithLabel.text = [DataHelper getFieldDisplayWithFieldName:@"emfa__Visit_With__c" withDictionary:self.tempData default:@""];
        self.selectedStoreCode = [DataHelper getFieldDisplayWithFieldName:@"emfa__Store__r.emfa__Store_Code__c" withDictionary:self.tempData default:@""];
        
        [self.storeCodeButton setTitle:[DataHelper getFieldDisplayWithFieldName:@"emfa__Store__r.Name" withDictionary:self.tempData default:@""] forState:UIControlStateNormal];

        NSDictionary* taskObj = [self.tempData objectForKey:@"Tasks"];
        if(taskObj!=(id)[NSNull null]){
            NSArray* tasks = [taskObj objectForKey:@"records"];
            for(NSDictionary* task in tasks){
                NSString* recordType = [DataHelper getFieldDisplayWithFieldName:@"RecordType.Name" withDictionary:task default:@""];
                if([recordType isEqualToString:@"Document"]){
                    [self.documentTaskArray addObject:@{
                                                    @"Subject": [task objectForKey:@"Subject"]
                                                    }];
                }
                else if([recordType isEqualToString:@"ToDo"]){
                    [self.toDoTaskArray addObject:@{
                                                    @"Subject": [task objectForKey:@"Subject"],
                                                    @"Category": [task objectForKey:@"emfa__Category__c"],
                                                    @"Duration": [task objectForKey:@"emfa__Estimated_Duration__c"]
                                                    }];
                }
            }
            [self.documentTableView reloadData];
            [self.toDoTableView reloadData];
        }
        
    }
    
    
    [self refreshLocale];
}

-(void) refreshLocale{
    
    self.headerTitleLabel.text = [ShareLocale textFromKey:@"newvisit_title"];
    
    [self.formTitle setTitle:[ShareLocale textFromKey:@"newvisit_formtitle"] forState:UIControlStateNormal];
    self.formDate.text = [ShareLocale textFromKey:@"newvisit_visitdate:"];
    self.formStartTime.text = [ShareLocale textFromKey:@"newvisit_starttime:"];
    self.formEndTime.text = [ShareLocale textFromKey:@"newvisit_endtime:"];
    self.formPurpose.text = [ShareLocale textFromKey:@"newvisit_purpose:"];
    self.formTodoList.text = [ShareLocale textFromKey:@"newvisit_todolist:"];
    self.formDocCheckList.text = [ShareLocale textFromKey:@"newvisit_doclist:"];
    self.formStoreCode.text = [ShareLocale textFromKey:@"newvisit_storecode:"];
    self.formVIsitWith.text = [ShareLocale textFromKey:@"newvisit_visitwith:"];
    self.formComment.text = [ShareLocale textFromKey:@"newvisit_formcomment:"];
    [self.nextButton setTitle:[ShareLocale textFromKey:@"newvisit_next"] forState:UIControlStateNormal];
    
    [self.todoCatLbl setTitle:[ShareLocale textFromKey:@"task_category"] forState:UIControlStateNormal];
    [self.todoSubjectLbl setTitle:[ShareLocale textFromKey:@"task_subject"] forState:UIControlStateNormal];
    [self.todoDurLbl setTitle:[ShareLocale textFromKey:@"task_duration"] forState:UIControlStateNormal];
    [self.todoEstLbl setTitle:[ShareLocale textFromKey:@"task_estimation"] forState:UIControlStateNormal];
    [self.docLbl setTitle:[ShareLocale textFromKey:@"task_document"] forState:UIControlStateNormal];
    
    
    [self.startDateChooseBtn setTitle:[ShareLocale textFromKey:@"choose..."] forState:UIControlStateNormal];
    [self.startTimeChooseBtn setTitle:[ShareLocale textFromKey:@"choose..."] forState:UIControlStateNormal];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (void)dealloc {
    [_tempData release];
//    [_startTimeButton release];
    [_endTimePicker release];
    [_startTimeLabel release];
    [_endTimeLabel release];
    [_remarkLabel release];
    [_visitDateLabel release];
    [_visitWithLabel release];
    [_toDoTableView release];
    [_documentTableView release];
    [_commentLabel release];
    [_storeCodeButton release];
    [_formTitle release];
    [_formDate release];
    [_formStartTime release];
    [_formEndTime release];
    [_formPurpose release];
    [_formTodoList release];
    [_formDocCheckList release];
    [_formStoreCode release];
    [_formVIsitWith release];
    [_formComment release];
    [_headerTitleLabel release];
    [_nextButton release];
    [_startDateChooseBtn release];
    [_startTimeChooseBtn release];
    [_todoSubjectLbl release];
    [_todoCatLbl release];
    [_todoDurLbl release];
    [_todoEstLbl release];
    [_docLbl release];
    [super dealloc];
}



#pragma mark - Control keyboard
- (void)textFieldDidEndEditing:(UITextField *)textField{
    userAlreadyInput = YES;
    NSLog(@"textFieldDidEndEditing");
//    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];

}

- (void)textViewDidEndEditing:(UITextView *)textView{
    userAlreadyInput = YES;
    NSLog(@"textViewDidEndEditing");
//    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];

}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    userAlreadyInput = YES;
    NSLog(@"textViewDidEndEditing");
//    [self.view setFrame:CGRectMake(0,-240,self.view.frame.size.width,self.view.frame.size.height)];
}
- (void)textViewDidBeginEditing:(UITextView *)textView{
    userAlreadyInput = YES;
    NSLog(@"textViewDidBeginEditing");
//    [self.view setFrame:CGRectMake(0,-240,self.view.frame.size.width,self.view.frame.size.height)];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    NSUInteger newLength = [textView.text length] + [text length] - range.length;
    if(newLength>80){
        return NO;
    }
    return YES;
}



#pragma mark - Handle Click event of Back button & Reload button
- (IBAction)onHomeButtonClicked:(id)sender {
    NSLog(@"onHomeButtonClicked");
    [self.view endEditing:YES];
    
    if(userAlreadyInput){
        [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"" message:[ShareLocale textFromKey:@"newvisit_confirm_leave"] yesHanlder:^(id data) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        } noHandler:^(id data) {
            
        }];
    }else{
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    

}


- (IBAction)onBackButtonClicked:(id)sender {
    NSLog(@"onBackButtonClicked");
    [self.view endEditing:YES];
    if(userAlreadyInput){
        [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"" message:[ShareLocale textFromKey:@"newvisit_confirm_leave"] yesHanlder:^(id data) {
            [self.navigationController popViewControllerAnimated:YES];
        } noHandler:^(id data) {
            
        }];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (IBAction)onStartTimeButtonClicked:(id)sender {
    [self.view endEditing:YES];

    userAlreadyInput = YES;
//    self.startTimePicker = [[[PopupDateTimePicker alloc] initWithMode: UIDatePickerModeTime] autorelease];
//    self.startTimePicker.selectedDate = [NSDate date];
//    self.startTimePicker.delegate = self;
//    [self.view addSubview:self.startTimePicker];
    UIView* view = (UIView*)sender;
    
    DatePickerSelectionViewController* vc = [[DatePickerSelectionViewController alloc ] initWithDate:[NSDate dateWithTimeIntervalSince1970: self.selectedStartDate.timeIntervalSince1970] andDateMode:UIDatePickerModeTime withBlock:^(NSDate *date) {
        
        [self.visitDatePopover dismissPopoverAnimated:YES];
        self.visitDatePopover = nil;
        self.selectedStartDate = date;
        [self refreshVisitStartEndTime];

    }];
    
    self.visitDatePopover = [[[UIPopoverController alloc] initWithContentViewController:vc] autorelease];
    
    CGRect f = CGRectMake(340, 220, view.frame.size.width, view.frame.size.height);
    
    [self.visitDatePopover presentPopoverFromRect:f inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}


- (IBAction)onSaveButtonClicked:(id)sender {
    [self.view endEditing:YES];

    if([self validateInputForm]){
        [self saveVisitationPlan];
    }
}

- (IBAction)onVisitDateButtonClicked:(id)sender {
    [self.view endEditing:YES];
    userAlreadyInput = YES;
    UIView* view = (UIView*)sender;
    
    DatePickerSelectionViewController* vc = [[DatePickerSelectionViewController alloc ] initWithDate:[NSDate dateWithTimeIntervalSince1970: self.selectedStartDate.timeIntervalSince1970] andDateMode:UIDatePickerModeDate withBlock:^(NSDate *date) {
            NSCalendar *calendar= [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] autorelease];
            NSCalendarUnit unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;

                NSDateComponents *dc = [calendar components:unitFlags fromDate: date];
                self.visitDateLabel.text = [NSString stringWithFormat:@"%04d-%02d-%02d", dc.year, dc.month, dc.day];
            [self.visitDatePopover dismissPopoverAnimated:YES];
            self.visitDatePopover = nil;
            self.selectedStartDate = date;
    }];

    self.visitDatePopover = [[[UIPopoverController alloc] initWithContentViewController:vc] autorelease];

    CGRect f = CGRectMake(340, 180, view.frame.size.width, view.frame.size.height);

    [self.visitDatePopover presentPopoverFromRect:f inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
}

//- (IBAction)onCheckboxClicked:(id)sender {
//    UIButton* cbx = (UIButton*) sender;
//    if(cbx.tag == 0){
//        cbx.tag = 1;
//        [cbx setBackgroundImage:[UIImage imageNamed:@"checkbox_on.png"] forState:UIControlStateNormal];
//    }else{
//        cbx.tag = 0;
//        [cbx setBackgroundImage:[UIImage imageNamed:@"checkbox_off.png"] forState:UIControlStateNormal];
//    }
//}

- (IBAction)onClickAddNewTask:(id)sender {
    userAlreadyInput = YES;
    [self.view endEditing:YES];

    [self.taskCreateView setHidden:NO];
    UIView* v = (UIView*)sender;

    if ( v.tag == 0 ){
        [self.taskCreateView setMode:TaskCreateModeToDo];
    }else if ( v.tag == 1){
        [self.taskCreateView setMode:TaskCreateModeDocument];
    }
    [self.taskCreateView refreshAll];
}

- (IBAction)onClickStoreCodeButton:(id)sender {
    userAlreadyInput = YES;
    [self.view endEditing:YES];

    UIView* button = (UIView*) sender;
    VFDAO* vf = [VFDAO sharedInstance];
//    NSMutableDictionary* defaults = vf.cache;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray* allStores = [vf getStoresByBrandCode: [defaults objectForKey: USER_PREFER_ACCOUNT_CODE] countryCode:[defaults objectForKey:USER_PREFER_COUNTRY_CODE] needOptionAll:NO];

    
//    NSMutableArray* allStores = [NSMutableArray arrayWithArray:[defaults objectForKey:@"store_search_list"]];
//    NSLog(@"allStores=%@",allStores);
    StoreSelectionViewController* storePopup = [[[StoreSelectionViewController alloc] initWithStoreList: allStores] autorelease];
    storePopup.delegate = self;
    self.storeSelectPopup = [[[UIPopoverController alloc] initWithContentViewController:storePopup] autorelease];
    self.storeSelectPopup.delegate = self;
    [self.storeSelectPopup presentPopoverFromRect:button.frame  inView:button.superview permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
}







#pragma mark - SelectStoreDelegate
-(void) onSelectdStore:(NSDictionary*) storeObj{
    if(self.storeSelectPopup ){
        self.selectedStoreCode = [storeObj objectForKey:@"emfa__Store_Code__c"];
        [self.storeCodeButton setTitle: [storeObj objectForKey:@"Name"] forState:UIControlStateNormal];
        [self.storeSelectPopup dismissPopoverAnimated:YES];
        self.storeSelectPopup = nil;
    }
}
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    
}
#pragma mark - Others

- (void)onTaskCreateComplete:(NSDictionary *)dict{
    NSLog(@"onTaskCreateComplete: %@", dict);
    
    [self addNewTaskToScrollViewWithDict:dict];
    
    //Update Duration with End Time after update toDoList
    [self refreshVisitStartEndTime];
    
}

- (void)refreshVisitStartEndTime{
    NSCalendar *calendar= [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] autorelease];
    NSCalendarUnit unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    
    NSDateComponents *dc = [calendar components:unitFlags fromDate: self.selectedStartDate];
    self.startTimeLabel.text = [NSString stringWithFormat:@"%02d:%02d", dc.hour, dc.minute];
    
    int secondDuration = [self getTotalToDoDuration]*60;
    NSDateComponents *dc2 = [calendar components:unitFlags fromDate: [self.selectedStartDate dateByAddingTimeInterval:secondDuration]];
    
    //Set End Time with ( start Time + Duration)
    self.endTimeLabel.text = [NSString stringWithFormat:@"%02d:%02d", dc2.hour, dc2.minute];
}

- (int)getTotalToDoDuration{
    int totalDuration = 0;
    for ( NSDictionary *dict in self.toDoTaskArray ){
        if ( [dict objectForKey:@"Duration"] != nil && [dict objectForKey:@"Duration"] != (id)[NSNull null] ){
            totalDuration += [[dict objectForKey:@"Duration"] intValue];
        }
    }
    return totalDuration;
}

- (void)addNewTaskToScrollViewWithDict:(NSDictionary *)dict{
    NSString *typeName = [dict objectForKey:@"Type"];
    int index;
    
    if ( [typeName isEqualToString:@"ToDo"] ){
        index = [self.toDoTaskArray count];
        NSLog(@"1 : %i", index);
        [self.toDoTaskArray addObject:dict];

    }else if( [typeName isEqualToString:@"Document"]){
        index = [self.documentTaskArray count];
        [self.documentTaskArray addObject:dict];
        NSLog(@"2 : %i", index);
    }else{
        return;
    }

    if ( [typeName isEqualToString:@"ToDo"] ){
        [self.toDoTableView reloadData];
        [self.toDoTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
    else if( [typeName isEqualToString:@"Document"]){
        [self.documentTableView reloadData];
        [self.documentTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }

}

#pragma mark - Valdiation of user input
-(BOOL)validateDateTime:(NSString*)format content:(NSString*)content{
    if(content==nil || [content length]==0){
        //check if user input anything
        [self showInputErrorDialog: [NSString stringWithFormat:[ShareLocale textFromKey:@"dailog_invalid_input"], content, format] ];
        return NO;
    }
    
    NSDateFormatter* df = [[[NSDateFormatter alloc] init] autorelease];
    [df setDateFormat:format];
    NSDate* resultDate = [df dateFromString: content];
    if(resultDate==nil || resultDate==(id)[NSNull null]){
        //check if user input correct format
        [self showInputErrorDialog: [NSString stringWithFormat:[ShareLocale textFromKey:@"dailog_invalid_input"], content, format] ];
        return NO;
    }
    return YES;
}

-(BOOL) validateInputForm{
   // NSString* remark = self.remarkLabel.text;
    //NSString* storeCode = self.storeCodeButton.titleLabel.text;
    NSString* storeCode = self.selectedStoreCode;
    if(storeCode==nil || [storeCode length]==0){
        [self showInputErrorDialog:[ShareLocale textFromKey:@"dialog_invalid_storecode"]];
        return NO;
    }
    NSString* visitDateStr = self.visitDateLabel.text;
    if (![self validateDateTime:DEFAULT_SIMPLE_DATE_FORMAT content:visitDateStr]){
        return NO;
    }
    
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    [df setDateFormat:DEFAULT_SIMPLE_DATE_FORMAT];
    NSDate* vDate = [df dateFromString:visitDateStr];
    
    NSDateComponents *today = [[[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar] components:NSYearCalendarUnit| NSMonthCalendarUnit | NSDayCalendarUnit fromDate:[NSDate date]];
    NSDateComponents *visitDate = [[[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar] components:NSYearCalendarUnit| NSMonthCalendarUnit | NSDayCalendarUnit fromDate:vDate];

    double todayDayCount = today.year*365 + today.month*31 + today.day;
    double visitDayCount = visitDate.year*365 + visitDate.month*31 + visitDate.day;
    
    if(todayDayCount > visitDayCount){
        [self showInputErrorDialog: [ShareLocale textFromKey:@"dailog_invalid_visitdate"]];
        return NO;
    }
    
    NSString* startTimeStr = self.startTimeLabel.text;
    if (![self validateDateTime:@"HH:mm" content:startTimeStr]){
        return NO;
    }
    
    NSString* endTimeStr = self.endTimeLabel.text;
    if (![self validateDateTime:@"HH:mm" content:endTimeStr]){
        return NO;
    }
    
    
   
    return YES;
}

-(void) showInputErrorDialog:(NSString*) msg{
    [[GeneralUiTool shareInstance] showConfirmDialogWithTitle: [ShareLocale textFromKey:@"validation_error"] message:msg];
}


#pragma mark - Network
-(void) saveVisitationPlan{
    
    
    NSString* remark = self.remarkLabel.text;
    NSString* comment = self.commentLabel.text;
    NSString* storeCode = self.selectedStoreCode;// self.storeCodeButton.titleLabel.text;
    NSString* visitWithData = self.visitWithLabel.text;
    NSString* startTimeLocalStr = self.startTimeLabel.text;
    NSString* endTimeLocalStr = self.endTimeLabel.text;
    NSString* visitDateStr = self.visitDateLabel.text;
    
    // Setup start/end time based on server timezone
    NSCalendar *calendar= [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] autorelease];
    NSCalendarUnit unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    GeneralUiTool *tool = [GeneralUiTool shareInstance];
    [calendar setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:tool.timezoneSecond]];
    NSDateComponents *dcServer = [calendar components:unitFlags fromDate: self.selectedStartDate];
    NSString* startTimeServerStr = [NSString stringWithFormat:@"%02d:%02d", dcServer.hour, dcServer.minute];
    
    NSDateComponents *dcServer2 = [calendar components:unitFlags fromDate: [self.selectedStartDate dateByAddingTimeInterval:[self getTotalToDoDuration]*60]];
    NSString* endTimeServerStr = [NSString stringWithFormat:@"%02d:%02d", dcServer2.hour, dcServer2.minute];
    
    
    NSMutableDictionary* data = [NSMutableDictionary dictionaryWithDictionary: @{
                           @"Name": [NSString stringWithFormat:@"Visit %@ on %@", storeCode, [[GeneralUiTool getSFSimpleDateFormatter] stringFromDate:self.selectedStartDate]],
                           @"emfa__Remark__c":remark,
                           @"Comment__c":comment,
                           @"Input_Store_Code__c": storeCode,
                           @"start_time_local_timezone":  startTimeLocalStr,
                           @"end_time_local_timezone":  endTimeLocalStr,
                           @"emfa__Start_Time__c":  startTimeServerStr,
                           @"emfa__End_Time__c":  endTimeServerStr,
                           @"visit_date_local_timezone": visitDateStr,
                           @"emfa__Visit_Date__c": [[GeneralUiTool getSFSimpleDateFormatter] stringFromDate:self.selectedStartDate],
                           @"emfa__Visit_With__c":visitWithData,
                           
                           @"toDoTasks": self.toDoTaskArray,
                           @"documentTasks": self.documentTaskArray
                           }];
    if(self.tempData!=nil && self.tempData!=(id)[NSNull null]){
        [data setObject:[self.tempData objectForKey:@"Id"] forKey:@"Id"];
    }
    PreviewVisitPlanViewController* vc = [[PreviewVisitPlanViewController alloc] initWithData: data delegate:self.delegate];
    [self.navigationController pushViewController:vc animated:YES];
    
    
}




- (int)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ( tableView == self.toDoTableView ){
        return [self.toDoTaskArray count] >0 ? [self.toDoTaskArray count]:1;
    }else if (tableView == self.documentTableView ){
        return [self.documentTaskArray count] >0 ? [self.documentTaskArray count]:1;
    }else {
        return 0;
    }
}

- (float) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ( tableView == self.toDoTableView ){
        return [self.toDoTaskArray count] >0 ? 40:40;
    }else if (tableView == self.documentTableView ){
        return [self.documentTaskArray count] >0 ? 40:40;
    }else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CreateVisitPlanCell"];
    if(cell == nil){
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CreateVisitPlanCell"] autorelease];
        UIView *selectionColor = [[[UIView alloc] init] autorelease];
        selectionColor.backgroundColor = TABLE_ROW_HIGHLIGHT_COLOR;
        cell.selectedBackgroundView = selectionColor;
        
        cell.textLabel.hidden = YES;
        cell.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        
        UILabel* nameLbl = [self createNewTextLabelWithDefaultStyle: CGRectMake(10, 5, 200, 30) parent:cell tag: 901];
        nameLbl.adjustsFontSizeToFitWidth = YES;
        nameLbl.minimumScaleFactor = .5f;
        UILabel* catLbl = [self createNewTextLabelWithDefaultStyle: CGRectMake(210, 5, 200, 30) parent:cell tag: 902 alignment:NSTextAlignmentRight];
        catLbl.adjustsFontSizeToFitWidth = YES;
        catLbl.minimumScaleFactor = .5f;
        UILabel* durLbl = [self createNewTextLabelWithDefaultStyle: CGRectMake(388, 5, 100, 30) parent:cell tag: 903 alignment:NSTextAlignmentRight];
        durLbl.adjustsFontSizeToFitWidth = YES;
        durLbl.minimumScaleFactor = .5f;
        UILabel* estLbl = [self createNewTextLabelWithDefaultStyle: CGRectMake(440, 5, 100, 30) parent:cell tag: 904 alignment:NSTextAlignmentRight];
        estLbl.adjustsFontSizeToFitWidth = YES;
        estLbl.minimumScaleFactor = .5f;
        
        
    }
    
    if ( tableView == self.toDoTableView){
        if ( [self.toDoTaskArray count] > 0 ){
            NSDictionary* data = [self.toDoTaskArray objectAtIndex: indexPath.row];

            NSString * toDoSubject = [data objectForKey:@"Subject"];
            NSString *categoryStr = [data objectForKey:@"Category"];
            NSString *estDurationStr = [data objectForKey:@"Duration"];
            
            UILabel* subjectLabel = (UILabel*)[cell viewWithTag:901];
            UILabel* categoryLabel = (UILabel*)[cell viewWithTag:902];
            float percentage = ((float)[estDurationStr intValue]/(float)[self getTotalToDoDuration])*100;
            UILabel* percentageLabel = (UILabel*)[cell viewWithTag:903];
            UILabel* estDurationLabel = (UILabel*)[cell viewWithTag:904];
            subjectLabel.text = [NSString stringWithFormat:@"%i. %@", indexPath.row+1, toDoSubject];
            [subjectLabel setTextColor:[UIColor whiteColor]];
            categoryLabel.text = [ShareLocale textFromKey: categoryStr];
            percentageLabel.text = [NSString stringWithFormat:@"%.1f%%", percentage];
            estDurationLabel.text = [NSString stringWithFormat:[ShareLocale textFromKey:@"task_sub_min"], [estDurationStr intValue]];
        }else{
            UILabel* hintLabel = (UILabel*)[cell viewWithTag:901];
            hintLabel.text = [ShareLocale textFromKey:@"no_data"];
            UILabel* categoryLabel = (UILabel*)[cell viewWithTag:902];
            categoryLabel.text = @"";
            UILabel* percentageLabel = (UILabel*)[cell viewWithTag:903];
            percentageLabel.text = @"";
            UILabel* estDurationLabel = (UILabel*)[cell viewWithTag:904];
            estDurationLabel.text = @"";
            
            [hintLabel setTextColor:[UIColor orangeColor]];
        }
        if ( (indexPath.row %2) == 0 )[cell setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.05f]];
        else [cell setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.0f]];
    }
    else if ( tableView == self.documentTableView){
        if ( [self.documentTaskArray count] > 0 ){
            NSDictionary* data = [self.documentTaskArray objectAtIndex: indexPath.row];
            UILabel* subjectLabel = (UILabel*)[cell viewWithTag:901];
            subjectLabel.text = [NSString stringWithFormat:@"%i. %@", indexPath.row+1, [data objectForKey:@"Subject"]];
            [subjectLabel setTextColor:[UIColor whiteColor]];
        }else{
            UILabel* hintLabel = (UILabel*)[cell viewWithTag:901];
            hintLabel.text = [ShareLocale textFromKey:@"no_data"];
            UILabel* categoryLabel = (UILabel*)[cell viewWithTag:902];
            categoryLabel.text = @"";
            UILabel* percentageLabel = (UILabel*)[cell viewWithTag:903];
            percentageLabel.text = @"";
            UILabel* estDurationLabel = (UILabel*)[cell viewWithTag:904];
            estDurationLabel.text = @"";
            [hintLabel setTextColor:[UIColor orangeColor]];

        }
        if ( (indexPath.row %2) == 0 )[cell setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.05f]];
        else [cell setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.0f]];
    }
    
    return cell;
}


- (IBAction)onClickDeleteTodo:(id)sender{
    int index = [self.toDoTableView indexPathForSelectedRow].row;
    if([self.toDoTaskArray count]> index){
        [self.toDoTaskArray removeObjectAtIndex:index];
        [self.toDoTableView reloadData];
        [self refreshVisitStartEndTime];
    }
}


- (IBAction)onClickDeleteDocument:(id)sender{
    int index = [self.documentTableView indexPathForSelectedRow].row;
    int count = [self.documentTaskArray count];
    //!!!??? [self.documentTaskArray count]>index return true!!?
    if(count> index){
        [self.documentTaskArray removeObjectAtIndex:index];
        [self.documentTableView reloadData];
        [self refreshVisitStartEndTime];
    }
}



@end
