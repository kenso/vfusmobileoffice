//
//  CreateVisitPlanViewController.h
//  VFStorePerformanceApp
//
//  Created by Developer 2 on 10/3/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "PopupDateTimePicker.h"
#import "TaskCreateView.h"
#import "DatePickerSelectionViewController.h"
#import "CustomWebserviceRestApi.h"

@protocol CreateVisitPlanDelegate <NSObject>

-(void)onEditVisitPlanComplete;

@end


@interface CreateVisitPlanViewController : BasicViewController< UITextViewDelegate, TaskCreateDelegate, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, StoreSelectionViewControllerDelegate, UIPopoverControllerDelegate>{
    
    BOOL userAlreadyInput;
    
}



@property (retain, nonatomic) NSDictionary *tempData;//used in init between viewdidload

@property (retain, nonatomic) NSDate *selectedStartDate;

@property (nonatomic, assign) id<CreateVisitPlanDelegate> delegate;

@property (retain, nonatomic) IBOutlet UILabel *headerTitleLabel;

@property (retain, nonatomic) IBOutlet UIButton *formTitle;
@property (retain, nonatomic) IBOutlet UILabel *formDate;
@property (retain, nonatomic) IBOutlet UILabel *formStartTime;
@property (retain, nonatomic) IBOutlet UILabel *formEndTime;
@property (retain, nonatomic) IBOutlet UILabel *formPurpose;
@property (retain, nonatomic) IBOutlet UILabel *formTodoList;
@property (retain, nonatomic) IBOutlet UILabel *formDocCheckList;
@property (retain, nonatomic) IBOutlet UILabel *formStoreCode;
@property (retain, nonatomic) IBOutlet UILabel *formVIsitWith;
@property (retain, nonatomic) IBOutlet UILabel *formComment;
@property (retain, nonatomic) IBOutlet UIButton *nextButton;


@property (retain, nonatomic) IBOutlet UIButton *startDateChooseBtn;
@property (retain, nonatomic) IBOutlet UIButton *startTimeChooseBtn;
@property (retain, nonatomic) IBOutlet UIButton *todoSubjectLbl;
@property (retain, nonatomic) IBOutlet UIButton *todoCatLbl;
@property (retain, nonatomic) IBOutlet UIButton *todoDurLbl;
@property (retain, nonatomic) IBOutlet UIButton *todoEstLbl;
@property (retain, nonatomic) IBOutlet UIButton *docLbl;


@property (retain, nonatomic) IBOutlet UITextView *startTimeLabel;
@property (retain, nonatomic) IBOutlet UITextView *endTimeLabel;
@property (retain, nonatomic) IBOutlet UITextView *remarkLabel;
@property (retain, nonatomic) IBOutlet UITextView *commentLabel;
@property (retain, nonatomic) IBOutlet UITextView *visitDateLabel;
@property (retain, nonatomic) IBOutlet UIButton *storeCodeButton;

//@property (retain, nonatomic) IBOutlet UITextView *docCheckListLabel;
//@property (retain, nonatomic) IBOutlet UITextView *todoCheckListLabel;

@property (retain, nonatomic) IBOutlet UITextView *visitWithLabel;


@property (retain, nonatomic) UIPopoverController* storeSelectPopup;

@property (retain, nonatomic) IBOutlet UITableView *toDoTableView;
@property (retain, nonatomic) IBOutlet UITableView *documentTableView;

@property (retain, nonatomic) PopupDateTimePicker* planTimePicker;
@property (retain, nonatomic) PopupDateTimePicker* startTimePicker;
@property (retain, nonatomic) PopupDateTimePicker* endTimePicker;
@property (retain, nonatomic) PopupDateTimePicker* visitDatePicker;

@property (retain, nonatomic) UIPopoverController* visitDatePopover;

@property (retain, nonatomic) TaskCreateView *taskCreateView;
@property (nonatomic, retain) NSMutableArray* documentTaskArray;
@property (nonatomic, retain) NSMutableArray* toDoTaskArray;



@property (retain, nonatomic) NSString* selectedStoreCode;

- (IBAction)onHomeButtonClicked:(id)sender;
- (IBAction)onBackButtonClicked:(id)sender;

- (IBAction)onStartTimeButtonClicked:(id)sender;

- (IBAction)onSaveButtonClicked:(id)sender;

- (IBAction)onVisitDateButtonClicked:(id)sender;


- (IBAction)onClickAddNewTask:(id)sender;
- (IBAction)onClickDeleteTodo:(id)sender;
- (IBAction)onClickDeleteDocument:(id)sender;

- (IBAction)onClickStoreCodeButton:(id)sender;

- (id) initWithYear:(int)year month:(int)month day:(int)day;
- (id) initWithDictionary:(NSDictionary*)data;

@end
