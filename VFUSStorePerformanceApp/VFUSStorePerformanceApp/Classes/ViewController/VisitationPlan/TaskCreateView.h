//
//  TaskCreateView.h
//  VFStorePerformanceApp_Ray
//
//  Created by cheng chok kwong on 4/4/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GeneralSelectionPopup.h"

typedef enum {
    TaskCreateModeToDo,
    TaskCreateModeDocument
} TaskCreateMode;

@protocol TaskCreateDelegate <NSObject>

- (void)onTaskCreateComplete:(NSDictionary *)dict;

@end

@interface TaskCreateView : UIView< UITextViewDelegate, GeneralSelectionPopupDelegate>{
    int selectedCatRow;
    int selectedDurRow;
    BOOL editing;
}

- (id)initWithFrame:(CGRect)frame withDelegate:(id)delegate;


@property (retain, nonatomic) id <TaskCreateDelegate> delegate;
@property (nonatomic) TaskCreateMode mode;
@property (retain, nonatomic) NSString *errorMessage;
@property (retain, nonatomic) NSArray *categoryArr;
@property (retain, nonatomic) NSArray* durationAry;


@property (retain, nonatomic) IBOutlet UIButton *titleLabel;
@property (retain, nonatomic) IBOutlet UILabel *subjectLabel;
@property (retain, nonatomic) IBOutlet UILabel *categoryLabel;
@property (retain, nonatomic) IBOutlet UILabel *durationLabel;

@property (retain, nonatomic) IBOutlet UITextView *subjectTextView;

@property (retain, nonatomic) IBOutlet UIButton *categoryButton;
@property (retain, nonatomic) IBOutlet UIButton *durationButton;
@property (retain, nonatomic) UIPopoverController* popupVC;
@property (retain, nonatomic) UIPopoverController* durationPopupVC;


@property (retain, nonatomic) IBOutlet UIButton *createButton;
@property (retain, nonatomic) IBOutlet UIButton *cancelButton;








- (IBAction)onClickCreateButton:(id)sender;
- (IBAction)onClickCancelButton:(id)sender;
- (IBAction)onClickCategoryButton:(id)sender;
- (IBAction)onClickDurationButton:(id)sender;

- (void)refreshAll;
- (void)cleanAllData;

-(BOOL)isValidForm;
@end
