//
//  TaskCreateView.m
//  VFStorePerformanceApp_Ray
//
//  Created by cheng chok kwong on 4/4/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "TaskCreateView.h"
#import "ShareLocale.h"
#import "GeneralUiTool.h"

@implementation TaskCreateView

- (id)initWithFrame:(CGRect)frame withDelegate:(id)delegate
{
    self = [[[[NSBundle mainBundle] loadNibNamed:@"TaskCreateView" owner:self options:nil] objectAtIndex:0] retain];
    if (self) {
        self.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height);
        self.mode = TaskCreateModeToDo;
        self.delegate = delegate;
        
        selectedCatRow = -1;
        
        selectedDurRow = 0;
        
        [self.subjectTextView setDelegate:self];
        
        NSMutableArray* durationAry = [[NSMutableArray alloc] init];
        for(int i=10; i<=120; i+=10){
            [durationAry addObject: @{
                                      @"label":[NSString stringWithFormat:[ShareLocale textFromKey:@"mins_format"], i],
                                      @"val":[NSNumber numberWithInt:i]} ];
        }
        
        self.durationAry = durationAry;

        self.categoryArr = [[NSArray alloc] initWithObjects:
                                @{@"label":[ShareLocale textFromKey:@"Training"], @"value":@"Training"},
                                @{@"label":[ShareLocale textFromKey:@"Coaching"], @"value":@"Coaching"},
                                @{@"label":[ShareLocale textFromKey:@"Competitor"], @"value":@"Competitor"},
                                @{@"label":[ShareLocale textFromKey:@"Sales review"], @"value":@"Sales review"},
                                @{@"label":[ShareLocale textFromKey:@"Staff issue"], @"value":@"Staff issue"},
                                @{@"label":[ShareLocale textFromKey:@"MSP review"], @"value":@"MSP review"},
                                @{@"label":[ShareLocale textFromKey:@"Store audit review"], @"value":@"Store audit review"},
                                @{@"label":[ShareLocale textFromKey:@"Operations issue"], @"value":@"Operations issue"},
                                @{@"label":[ShareLocale textFromKey:@"VM issue"], @"value":@"VM issue"},
                                @{@"label":[ShareLocale textFromKey:@"Customer/VIP issue"], @"value":@"Customer/VIP issue"},
                                @{@"label":[ShareLocale textFromKey:@"Product/inventory issue"], @"value":@"Product/inventory issue"},
                                @{@"label":[ShareLocale textFromKey:@"Marketing/promotion issue"], @"value":@"Marketing/promotion issue"},
                                @{@"label":[ShareLocale textFromKey:@"System/POS issue"], @"value":@"System/POS issue"},
                                @{@"label":[ShareLocale textFromKey:@"Logistic/delivery issue"], @"value":@"Logistic/delivery issue"},
                                @{@"label":[ShareLocale textFromKey:@"Others"], @"value":@"Others"},
                                nil];
        [self refreshContentView];
        
        [self.durationButton setTitle:[[self.durationAry objectAtIndex:selectedDurRow] objectForKey:@"label"] forState:UIControlStateNormal];
    }
    return self;
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    NSUInteger newLength = [textView.text length] + [text length] - range.length;
    if(newLength>20){
        return NO;
    }
    return YES;
}



- (void)onClickPickerBg:(id)sender{
    [self endEditing:YES];

}

- (void)cleanAllData{
    selectedCatRow = -1;
    selectedDurRow = 0;
    [self.subjectTextView setText:@""];
    [self.durationButton setTitle:[[self.durationAry objectAtIndex: selectedDurRow] objectForKey:@"label"] forState:UIControlStateNormal];
    [self.categoryButton setTitle:[ShareLocale textFromKey:@"choose"] forState:UIControlStateNormal];
}

- (void)refreshAll{
    [self cleanAllData];
    [self refreshContentView];
}

- (void)refreshContentView{
    if ( self.mode == TaskCreateModeToDo){
        [self.titleLabel setTitle:[ShareLocale textFromKey:@"taskform_todo_title"] forState:UIControlStateNormal];
        [self.subjectLabel setText: [ShareLocale textFromKey:@"taskform_subject:"] ];
        [self.categoryLabel setHidden:NO];
        self.durationButton.hidden = NO;
        [self.categoryButton setHidden:NO];
        [self.durationLabel setHidden:NO];
    }
    else if ( self.mode == TaskCreateModeDocument){
        [self.titleLabel setTitle:[ShareLocale textFromKey:@"taskform_doc_title"] forState:UIControlStateNormal];
        [self.subjectLabel setText: [ShareLocale textFromKey:@"taskform_subject:"] ];
        [self.categoryLabel setHidden:YES];
        [self.categoryButton setHidden:YES];
        [self.durationLabel setHidden:YES];
        self.durationButton.hidden = YES;
    }
    
    
    self.categoryLabel.text = [ShareLocale textFromKey:@"taskform_category:"];
    self.durationLabel.text = [ShareLocale textFromKey:@"taskform_duration:"];
    [self.createButton setTitle: [ShareLocale textFromKey:@"taskform_create"] forState:UIControlStateNormal];
    [self.cancelButton setTitle: [ShareLocale textFromKey:@"taskform_cancel"] forState:UIControlStateNormal];
    
}


- (IBAction)onClickCreateButton:(id)sender {
    [self endEditing:YES];
    if ([self isValidForm] ){
        if (self.delegate != nil ){
            NSString *typeName = [[NSString alloc] init];
            NSString *category = [[NSString alloc] init];
            NSString *duration = [[NSString alloc] init];
            duration = [[self.durationAry objectAtIndex: selectedDurRow] objectForKey:@"val"];
            if ( self.mode == TaskCreateModeToDo )typeName = @"ToDo";
            else if ( self.mode == TaskCreateModeDocument )typeName = @"Document";
            
            if ( selectedCatRow >= 0 ){
                category = [[self.categoryArr objectAtIndex:selectedCatRow] objectForKey:@"value"];

            }else{
                category = @"";
            }
            
            NSDictionary* data = @{
                                   @"Type": typeName,
                                   @"Subject":self.subjectTextView.text,
                                   @"Category": category,
                                   @"Duration": duration
                                   };
            
            [self.delegate onTaskCreateComplete:data];
            
            [self setHidden:YES];
            [self cleanAllData];
        }
    }else{
        [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:[ShareLocale textFromKey:@"error"] message:self.errorMessage];
    }
}

- (IBAction)onClickCancelButton:(id)sender {
    [self endEditing:YES];
    [self cleanAllData];
    [self setHidden:YES];
}

- (IBAction)onClickDurationButton:(id)sender{
    [self endEditing:YES];
    
    UIView* v = (UIView*)sender;
    
    GeneralSelectionPopup* vc = [[GeneralSelectionPopup alloc ] initWithArray: self.durationAry];
    vc.delegate = self;
    self.durationPopupVC = [[[UIPopoverController alloc] initWithContentViewController:vc] autorelease];
    
    [self.durationPopupVC presentPopoverFromRect:CGRectMake(420,340, v.frame.size.width, v.frame.size.height) inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
    
    
}


- (IBAction)onClickCategoryButton:(id)sender {
    [self endEditing:YES];
    
    UIView* v = (UIView*)sender;
    
    GeneralSelectionPopup* vc = [[GeneralSelectionPopup alloc ] initWithArray: self.categoryArr];
    vc.delegate = self;
    self.popupVC = [[[UIPopoverController alloc] initWithContentViewController:vc] autorelease];
    
    [self.popupVC presentPopoverFromRect:CGRectMake(420,290, v.frame.size.width, v.frame.size.height) inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
    
    
//    if ( !editing ){
//        if ( selectedCatRow == -1 ){
//            selectedCatRow = 0;
//            [self.categoryButton setTitle:[[self.categoryArr objectAtIndex:0] objectForKey:@"label"] forState:UIControlStateNormal];
//        }
//    }
}

#pragma mark - UITextView Delegate
- (void)textViewDidBeginEditing:(UITextView *)textView{
    editing = YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    editing = NO;
}

#pragma mark - PickerView Delegate

-(void) onRowSelected:(int) index{
    if(self.popupVC!=nil){
        [self.categoryButton setTitle:[[self.categoryArr objectAtIndex:index] objectForKey:@"label"] forState:UIControlStateNormal];
        selectedCatRow = index;
        [self.popupVC dismissPopoverAnimated:YES];
        self.popupVC = nil;
    }
    else if (self.durationPopupVC!=nil){
        [self.durationButton setTitle:[[self.durationAry objectAtIndex:index] objectForKey:@"label"] forState:UIControlStateNormal];
        selectedDurRow = index;
        [self.durationPopupVC dismissPopoverAnimated:YES];
        self.durationPopupVC = nil;
    }
}


- (BOOL)isValidForm{
    BOOL result = YES;
    
    if ( self.subjectTextView.text == nil || [self.subjectTextView.text isEqualToString:@""]){
        result = NO;
        self.errorMessage = [ShareLocale textFromKey: @"error_subject_required"];
    }else{
        if([self.subjectTextView.text length]>80){
           result = NO;
            self.errorMessage = [ShareLocale textFromKey: @"error_subject_too_long"];
        }
    }
    if ( self.mode == TaskCreateModeToDo ){
        if ( selectedCatRow == -1) {
            result = NO;
            self.errorMessage = [ShareLocale textFromKey: @"error_category_required"];
        }
    }
    
    return result;
}
- (void)dealloc {
    [_titleLabel release];
    [_subjectLabel release];
    [_categoryLabel release];
    [_subjectTextView release];
    [_categoryButton release];
    [_durationLabel release];
    [_durationButton release];
    [_createButton release];
    [_cancelButton release];
    [super dealloc];
}

@end
