//
//  PreviewVisitPlanViewController.m
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 21/6/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "ShareLocale.h"
#import "GeneralUiTool.h"
#import "CreateVisitPlanViewController.h"
#import "PreviewVisitPlanViewController.h"

@interface PreviewVisitPlanViewController ()

@end




@implementation PreviewVisitPlanViewController

-(id)initWithData:(NSDictionary*)data delegate:(id<CreateVisitPlanDelegate>)delegate
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        self.data = data;
        self.delegate = delegate;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    /*
     NSDictionary* data = @{
     @"Name": [NSString stringWithFormat:@"Visit %@ on %@", storeCode, visitDateStr],
     @"Remark__c":remark,
     @"Comment__c":comment,
     @"Input_Store_Code__c": storeCode,
     @"Planned_Start_Date_Time__c":  startTimeStr,
     @"Planned_End_Date_Time__c":  endTimeStr,
     @"Visit_Date__c": visitDateStr,
     @"Visit_With__c":visitWithData,
     
     @"toDoTasks": self.toDoTaskArray,
     @"documentTasks": self.documentTaskArray
     };
     */
    self.purposeLabel.text = [self.data objectForKey:@"emfa__Remark__c"];
    self.visitDateLabel.text = [self.data objectForKey:@"visit_date_local_timezone"];
    self.commentsLabel.text = [self.data objectForKey:@"Comment__c"];
    self.startTimeLabel.text = [self.data objectForKey:@"start_time_local_timezone"];
    self.endTimeLabel.text = [self.data objectForKey:@"end_time_local_timezone"];
    self.visitWithLabel.text = [self.data objectForKey:@"emfa__Visit_With__c"];
    self.storeLabel.text = [self.data objectForKey:@"Input_Store_Code__c"];
    
    NSArray* toDoTaskArray = [self.data objectForKey:@"toDoTasks"];
    NSArray* docTaskArray = [self.data objectForKey:@"documentTasks"];
    
    UIView* parent = self.todoView.superview;
    
    
    int index = 0;
    int maxHeight = 0;
    CGRect frame = self.todoView.frame;
    UIScrollView* scrollView = [[UIScrollView alloc] initWithFrame:frame];
    [self.todoView removeFromSuperview];
    
    UIView* todoView = [[UIView alloc] init];
    scrollView.showsHorizontalScrollIndicator = YES;
    scrollView.backgroundColor = [UIColor colorWithWhite:1 alpha:.2f];
    [scrollView addSubview:todoView];

    for(NSDictionary* todo in toDoTaskArray){
        UILabel* namelbl = [self createNewTextLabelWithDefaultStyle:CGRectMake(0, index*24, 180, 24) parent:todoView tag:0 alignment:NSTextAlignmentLeft];
        UILabel* categorylbl = [self createNewTextLabelWithDefaultStyle:CGRectMake(190, index*24, 170, 24) parent:todoView tag:0 alignment:NSTextAlignmentLeft];
        UILabel* durationlbl = [self createNewTextLabelWithDefaultStyle:CGRectMake(370, index*24, 50, 24) parent:todoView tag:0 alignment:NSTextAlignmentRight];
        
        namelbl.textColor = [UIColor whiteColor];
        categorylbl.textColor = [UIColor whiteColor];
        durationlbl.textColor = [UIColor whiteColor];
        
        namelbl.adjustsFontSizeToFitWidth = YES;
        categorylbl.adjustsFontSizeToFitWidth = YES;
        durationlbl.adjustsFontSizeToFitWidth = YES;
        
        namelbl.minimumScaleFactor = .5f;
        categorylbl.minimumScaleFactor = .5f;
        durationlbl.minimumScaleFactor = .5f;

        namelbl.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        categorylbl.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        durationlbl.backgroundColor = [UIColor colorWithWhite:1 alpha:0];

        
        namelbl.text = [todo objectForKey:@"Subject"];
        categorylbl.text = [ShareLocale textFromKey:[todo objectForKey:@"Category"]];
        durationlbl.text = [NSString stringWithFormat:[ShareLocale textFromKey:@"mins_format"], [[todo objectForKey:@"Duration"] intValue]];
        index++;
        maxHeight += 24;
    }
    if(maxHeight<frame.size.height) maxHeight = frame.size.height;
    todoView.frame = CGRectMake(0,0, frame.size.width, maxHeight);
    scrollView.contentSize = CGSizeMake(frame.size.width, maxHeight);
    [parent addSubview: scrollView ];
    [scrollView release];
    
    
    
    index = 0;
    maxHeight = 0;
    frame = self.docView.frame;
    scrollView = [[UIScrollView alloc] initWithFrame:frame];
    [self.docView removeFromSuperview];
    
    UIView* docView = [[UIView alloc] init];
    scrollView.showsHorizontalScrollIndicator = YES;
    scrollView.backgroundColor = [UIColor colorWithWhite:1 alpha:.2f];
    [scrollView addSubview:docView];
    for(NSDictionary* doc in docTaskArray){
        UILabel* namelbl = [self createNewTextLabelWithDefaultStyle:CGRectMake(0, index*24, 300, 24) parent:docView tag:0 alignment:NSTextAlignmentLeft];
        namelbl.adjustsFontSizeToFitWidth = YES;
        namelbl.minimumScaleFactor = 10;

        namelbl.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        namelbl.text = [doc objectForKey:@"Subject"];
        [namelbl setUserInteractionEnabled:NO];

        index++;
        maxHeight += 24;
    }
    if(maxHeight<frame.size.height) maxHeight = frame.size.height;
    docView.frame = CGRectMake(0,0, frame.size.width, maxHeight);
    scrollView.contentSize = CGSizeMake(frame.size.width, maxHeight);
    [parent addSubview: scrollView ];
    [scrollView release];
    
    
    [self refreshLocale];
    

}

-(void) refreshLocale{
    self.headerTitleLabel.text = [ShareLocale textFromKey:@"newvisit_preview_title"];
    
    [self.formTitle setTitle:[ShareLocale textFromKey:@"newvisit_formtitle"] forState:UIControlStateNormal];
    self.formDate.text = [ShareLocale textFromKey:@"newvisit_visitdate:"];
    self.formStartTime.text = [ShareLocale textFromKey:@"newvisit_starttime:"];
    self.formEndTime.text = [ShareLocale textFromKey:@"newvisit_endtime:"];
    self.formPurpose.text = [ShareLocale textFromKey:@"newvisit_purpose:"];
    self.formTodoList.text = [ShareLocale textFromKey:@"newvisit_todolist:"];
    self.formDocCheckList.text = [ShareLocale textFromKey:@"newvisit_doclist:"];
    self.formStoreCode.text = [ShareLocale textFromKey:@"newvisit_storecode:"];
    self.formVIsitWith.text = [ShareLocale textFromKey:@"newvisit_visitwith:"];
    self.formComment.text = [ShareLocale textFromKey:@"newvisit_formcomment:"];
    [self.saveButton setTitle:[ShareLocale textFromKey:@"newvisit_save"] forState:UIControlStateNormal];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}

#pragma mark - Handle Click event of Back button & Reload button
- (IBAction)onHomeButtonClicked:(id)sender {

    [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"" message:[ShareLocale textFromKey:@"newvisit_confirm_leave"] yesHanlder:^(id data) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    } noHandler:^(id data) {
        
    }];

}

- (IBAction)onBackButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onSaveButtonClicked:(id)sender {
    [self showLoadingPopup:self.view];
    
    
    self.postFormRequest = [[VFDAO sharedInstance] saveVisitPlanWithDelegate:self visitPlan:self.data];
}





#pragma mark - CustomWebserviceApi Delegate
-(void)daoRequest:(DAOBaseRequest*)request queryOnlineDataSuccess:(NSArray*) response{
    [self hideLoadingPopupWithFadeout];
    if ( self.postFormRequest == request){
        self.postFormRequest = nil;
        [self performSelectorOnMainThread:@selector(showOnlineSubmitionSuccessDialog) withObject:nil waitUntilDone:YES];
        return;
    }
}

-(void)daoRequest:(DAOBaseRequest*)request queryOnlineDataError:(NSString*) response code:(int)code{
    [self hideLoadingPopupWithFadeout];
    self.postFormRequest = nil;
    [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:[ShareLocale textFromKey:@"error"] message: [ShareLocale textFromKey: @"dialog_saved_fail"] ];
}



-(void) showOnlineSubmitionSuccessDialog{
    [self hideLoadingPopupWithFadeout];

    [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:[ShareLocale textFromKey:@"confirm"] message: [ShareLocale textFromKey:@"dialog_confirm_saved"] yesHanlder:^(id data) {
        if(self.delegate) [self.delegate onEditVisitPlanComplete];
        [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:1] animated:YES];
    }];
}


- (void)dealloc {
    [_visitDateLabel release];
    [_startTimeLabel release];
    [_endTimeLabel release];
    [_purposeLabel release];
    [_commentsLabel release];
    [_visitWithLabel release];
    [_storeLabel release];
    [_todoView release];
    [_docView release];
    [super dealloc];
}
@end
