//
//  NewsDetailViewController.m
//  VFStorePerformanceApp
//
//  Created by Developer 2 on 5/3/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "ShareLocale.h"
#import "GeneralUiTool.h"
#import "NewsDetailViewController.h"

@interface NewsDetailViewController ()

@end

@implementation NewsDetailViewController

- (id)initWithData:(NSDictionary*)data
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        self.data = data;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //self.titleLabel.text = [self.data objectForKey:@"Name"];
    self.messageContentLabel.text = [self.data objectForKey:@"emfa__Description__c"];
    self.messageContentLabel.scrollEnabled = YES;
    self.messageContentLabel.selectable = NO;

    
    self.receiverGroupLabel.text = [self.data objectForKey:@"emfa__Category__c"];
    
    NSString* pdateStr = [self getFieldDisplayWithFieldName:@"emfa__Published_Start_Date__c" withDictionary: self.data];
    NSDate* pdate = [[GeneralUiTool getSFDateFormatter] dateFromString: pdateStr];
    NSDateFormatter* df = [[[NSDateFormatter alloc] init] autorelease];
    [df setDateStyle:NSDateFormatterShortStyle];
    [df setTimeStyle:NSDateFormatterShortStyle];

    
    self.publishDateLabel.text = [df stringFromDate:pdate];
    
    
    self.messageTitleLabel.text = [self.data objectForKey:@"Name"];
    
    self.formContentLabel.text = [ShareLocale textFromKey:@"msgdpage_form_content"];
    self.formDateLabel.text = [ShareLocale textFromKey:@"msgdpage_form_date"];
    self.formReceiverLabel.text = [ShareLocale textFromKey:@"msgdpage_form_receiver"];
    self.formTitleLabel.text = [ShareLocale textFromKey:@"msgdpage_form_title"];

    self.titleLabel.text = [ShareLocale textFromKey:@"msgdpage_title"];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



#pragma mark - Handle Click event of Back button & Reload button
- (IBAction)onBackButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onHomeButtonClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (void)dealloc {
    [_messageParentView release];
    [_titleLabel release];
    [_receiverGroupLabel release];
    [_publishDateLabel release];
    [_messageContentLabel release];
    [_messageTitleLabel release];
    [super dealloc];
}


@end
