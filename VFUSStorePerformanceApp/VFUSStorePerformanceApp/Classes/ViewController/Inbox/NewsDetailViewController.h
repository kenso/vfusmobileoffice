//
//  NewsDetailViewController.h
//  VFStorePerformanceApp
//
//  Created by Developer 2 on 5/3/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "BasicViewController.h"

@interface NewsDetailViewController : BasicViewController

@property (retain, nonatomic) IBOutlet UIView *messageParentView;

@property (retain, nonatomic) NSDictionary* data;

@property (retain, nonatomic) IBOutlet UILabel *titleLabel;
@property (retain, nonatomic) IBOutlet UILabel *receiverGroupLabel;
@property (retain, nonatomic) IBOutlet UILabel *publishDateLabel;
@property (retain, nonatomic) IBOutlet UITextView *messageContentLabel;
@property (retain, nonatomic) IBOutlet UILabel *messageTitleLabel;

- (IBAction)onBackButtonClicked:(id)sender;
- (IBAction)onHomeButtonClicked:(id)sender;

- (id)initWithData:(NSDictionary*)data;


@property (retain, nonatomic) IBOutlet UILabel *formReceiverLabel;
@property (retain, nonatomic) IBOutlet UILabel *formTitleLabel;
@property (retain, nonatomic) IBOutlet UILabel *formDateLabel;
@property (retain, nonatomic) IBOutlet UILabel *formContentLabel;

@end
