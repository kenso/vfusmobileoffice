//
//  NewsViewController.h
//  VFStorePerformanceApp
//
//  Created by Developer 2 on 5/3/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "BasicViewController.h"
#import "SFRestAPI.h"
#import "SFRestRequest.h"
#import "BasicViewController.h"
#import "VFDAO.h"

@interface NewsViewController : BasicViewController<DAODelegate, UITableViewDelegate, UITableViewDataSource>{
    
}

@property (retain, nonatomic) IBOutlet UILabel *titleLabel;

@property (retain, nonatomic) IBOutlet UIButton *headerTitleLabel;
@property (retain, nonatomic) IBOutlet UIButton *headerDateLabel;
@property (retain, nonatomic) IBOutlet UIButton *headerReceiverLabel;

@property (nonatomic, retain) NSMutableArray* recordArray;


@property (retain, nonatomic) IBOutlet UITableView *messagesTableView;

- (IBAction)onBackButtonClicked:(id)sender;

- (IBAction)onHomeButtonClicked:(id)sender;

@end
