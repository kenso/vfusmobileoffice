//
//  NewsViewController.m
//  VFStorePerformanceApp
//
//  Created by Developer 2 on 5/3/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "ShareLocale.h"
#import "NewsViewController.h"
#import "Constants.h"
#import "NewsDetailViewController.h"
#import "GeneralUiTool.h"

@interface NewsViewController ()

@end

@implementation NewsViewController

- (id)init
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.messagesTableView.backgroundColor = TABLE_BG_COLOR_BLUE;
    self.messagesTableView.backgroundView = nil;
    self.recordArray = [NSMutableArray array];
    self.messagesTableView.delegate = self;
    self.messagesTableView.dataSource = self;
    
    [self initLocale];
    
    [self loadData];

}


-(void) initLocale{
    [self.headerDateLabel setTitle: [ShareLocale textFromKey:@"msgpage_header_date"] forState:UIControlStateNormal];
    [self.headerReceiverLabel setTitle: [ShareLocale textFromKey:@"msgpage_header_receiver"] forState:UIControlStateNormal];
    [self.headerTitleLabel setTitle: [ShareLocale textFromKey:@"msgpage_header_title"] forState:UIControlStateNormal];
    
    self.titleLabel.text = [ShareLocale textFromKey:@"msgpage_title"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Table view delegate
-(float) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NewsDetailViewController* vc = [[[NewsDetailViewController alloc] initWithData:[self.recordArray objectAtIndex:indexPath.row]] autorelease];
    [self.navigationController pushViewController:vc animated:YES];
    [tableView deselectRowAtIndexPath: indexPath animated:NO];
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
    if(cell == nil){
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyIdentifier"] autorelease];
        cell.textLabel.hidden = YES;
        cell.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        UIView *selectionColor = [[[UIView alloc] init] autorelease];
        selectionColor.backgroundColor = TABLE_ROW_HIGHLIGHT_COLOR;
        cell.selectedBackgroundView = selectionColor;
        
        [self createNewTextLabelWithDefaultStyle: CGRectMake(10, 5, 580, 40) parent:cell tag: 901];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(600, 5, 180, 40) parent:cell tag: 902];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(790, 5, 220, 40) parent:cell tag: 903];
        
    }
    
    cell.tag = indexPath.row;
    
    NSDictionary* data = [self.recordArray objectAtIndex: indexPath.row];
    
    UILabel* txtView1 = (UILabel*)[cell viewWithTag:901];
    txtView1.minimumScaleFactor = .6f;
    txtView1.adjustsFontSizeToFitWidth = YES;
    txtView1.text = [self getFieldDisplayWithFieldName:@"Name" withDictionary: data];

    UILabel* txtView2 = (UILabel*)[cell viewWithTag:902];
    txtView2.text = [data objectForKey:@"emfa__Category__c"];
    txtView2.numberOfLines = 2;
    [txtView2 sizeToFit];
    CGRect rect = txtView2.frame;
    rect.origin.y = (50 - rect.size.height)/2;
    txtView2.frame = rect;
    
    UILabel* txtView3 = (UILabel*)[cell viewWithTag:903];
    NSString* pdateStr = [self getFieldDisplayWithFieldName:@"emfa__Published_Start_Date__c" withDictionary: data];
    NSDate* pdate = [[GeneralUiTool getSFDateFormatter] dateFromString: pdateStr];
    NSDateFormatter* df = [[[NSDateFormatter alloc] init] autorelease];
    [df setDateStyle:NSDateFormatterShortStyle];
    [df setTimeStyle:NSDateFormatterShortStyle];
    txtView3.text =  [df stringFromDate: pdate];
    txtView3.numberOfLines = 2;
    [txtView3 sizeToFit];
    CGRect rect3 = txtView3.frame;
    rect3.origin.y = (50 - rect3.size.height)/2;
    txtView3.frame = rect3;
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.recordArray count];
}


#pragma mark - Network Delegate
- (void) loadData{
    [self showLoadingPopup: self.view];
    
    [[VFDAO sharedInstance] selectBroadcastNoticeWithDelegate:self];
    
    
}

-(void)daoRequest:(DAORequest *)request queryOnlineDataSuccess:(NSArray *)records{
    [self hideLoadingPopupWithFadeout];
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        self.recordArray = [[[NSMutableArray alloc] initWithArray:records] autorelease];
        [self.messagesTableView reloadData];
    });
}

-(void)daoRequest:(DAORequest *)request queryOnlineDataError:(NSString *)response code:(int)code{
    [self hideLoadingPopupWithFadeout];
    [super handleQueryDataErrorCode:code description:response];
}




#pragma mark - Handle Click event of Back button & Reload button
- (IBAction)onBackButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onHomeButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc {
    [_messagesTableView release];
    [_headerTitleLabel release];
    [_headerDateLabel release];
    [_titleLabel release];
    [_headerReceiverLabel release];
    [super dealloc];
}
@end
