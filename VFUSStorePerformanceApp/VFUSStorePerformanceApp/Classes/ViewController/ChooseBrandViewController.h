//
//  ChooseBrandViewController.h
//
//  Created by Developer 2 on 4/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SFRestAPI.h"
#import "SFRestRequest.h"
#import "BasicViewController.h"


@interface ChooseBrandViewController : BasicViewController<SFRestDelegate, UITableViewDelegate, UITableViewDataSource>{
}
@property (retain, nonatomic) IBOutlet UIButton *backButton;
@property (retain, nonatomic) IBOutlet UIButton *reloadButton;

@property (retain, nonatomic) IBOutlet UITableView *brandTableView;

@property (retain, nonatomic) NSArray* recordAry;

-(IBAction)onBackButtonClicked:(id)sender;

-(IBAction)onReloadButtonClicked:(id)sender;

@end

