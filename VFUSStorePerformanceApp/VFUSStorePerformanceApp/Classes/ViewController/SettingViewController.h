//
//  SettingViewController.h
//  VF Ordering App
//
//  Created by Developer 2 on 3/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SFRestAPI.h"
#import "SFRestRequest.h"
#import "BasicViewController.h"
#import <MessageUI/MessageUI.h>

@interface SettingViewController : BasicViewController<SFRestDelegate, MFMailComposeViewControllerDelegate>


@property (retain, nonatomic) NSMutableDictionary* countryCodeMap;


@property (retain, nonatomic) IBOutlet UIButton *backButton;
@property (retain, nonatomic) IBOutlet UIButton *logoutButton;

@property (retain, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (retain, nonatomic) IBOutlet UIButton *bugReportButton;


@property (retain, nonatomic) IBOutlet UIButton *theme1Button;
@property (retain, nonatomic) IBOutlet UIButton *theme2Button;
@property (retain, nonatomic) IBOutlet UIButton *theme3Button;
@property (retain, nonatomic) IBOutlet UIButton *theme4Button;
@property (retain, nonatomic) IBOutlet UIButton *theme5Button;
@property (retain, nonatomic) IBOutlet UIButton *theme6Button;
@property (retain, nonatomic) IBOutlet UIButton *theme7Button;

@property (retain, nonatomic) IBOutlet UIButton *lang1Button;
@property (retain, nonatomic) IBOutlet UIButton *lang2Button;
@property (retain, nonatomic) IBOutlet UIButton *lang3Button;
@property (retain, nonatomic) IBOutlet UIButton *lang4Button;
@property (retain, nonatomic) IBOutlet UIButton *lang5Button;

- (IBAction)onThemeChanged:(id)sender;

- (IBAction)onLogoutButtonClicked:(id)sender;

- (IBAction)onBackButtonClicked:(id)sender ;

- (IBAction)onLanguageChanged:(id)sender;

- (IBAction)onContactButtonClicked:(id)sender;

- (IBAction)onCountryButtonClicked:(id)sender;

@property (retain, nonatomic) IBOutlet UILabel *versionLabel;
@property (retain, nonatomic) IBOutlet UILabel *userLabel;

@property (retain, nonatomic) IBOutlet UILabel *countryLeftLabel;
@property (retain, nonatomic) IBOutlet UILabel *languageLeftLabel;

@property (retain, nonatomic) IBOutlet UILabel *userLeftLabel;
@property (retain, nonatomic) IBOutlet UILabel *versionLeftLabel;
@property (retain, nonatomic) IBOutlet UILabel *brandLeftLabel;

@end
