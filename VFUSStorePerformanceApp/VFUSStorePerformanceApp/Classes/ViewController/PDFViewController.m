//
//  PDFViewController.m
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 15/6/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "PDFViewController.h"
#import "ShareLocale.h"
#import "ImageLoader.h"
#import "GeneralUiTool.h"

@interface PDFViewController ()

@end

@implementation PDFViewController

-(id) initWithFeedItemID:(NSString*)feedItemID
{
    self = [super initWithNibName:@"PDFViewController" bundle: nil];
    if (self) {
        self.feedItemID = feedItemID;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self loadData];
    
    self.headerTitleLabel.text = [ShareLocale textFromKey:@"pdfpage_title"];
    
}

-(void) loadData{
    [self showLoadingPopup:self.view];
    ImageLoader* loader = [ImageLoader shareInstance];
    [loader downloadDocWithFeedID:self.feedItemID type:@"pdf" delegate:self];
}

-(void) onRequestSuccessOfRequest:(ImageLoaderRequest *)req feedItemID:(NSString *)feedItemId response:(id)jsonResponse contentType:(NSString *)contentType{
    [self hideLoadingPopup];
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    if(contentType!=nil && [contentType isEqualToString:@"pdf"]){
        feedItemId = [NSString stringWithFormat:@"/%@.pdf", feedItemId];
    }
    else{
        feedItemId = [NSString stringWithFormat:@"/%@", feedItemId];
    }
    
    NSString *path = [applicationDocumentsDir stringByAppendingPathComponent:feedItemId];
    NSURL *url = [NSURL fileURLWithPath:path];
    NSURLRequest *request = [NSURLRequest requestWithURL:url ];
    [self.webview loadRequest:request];

}

//
//-(void) reloadPDFInWebView:(NSString*)html{
//    [self.webview loadHTMLString:html baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle]bundlePath]]];
//    
//}

-(void) onRequestFailOfRequest:(ImageLoaderRequest *)req feedItemID:(NSString *)feedItemId error:(NSError *)error{
    [self hideLoadingPopup];
    [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"Error" message:[NSString stringWithFormat:@"Fail to load PDF due to: %@", error] ];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (IBAction)onHomeButtonClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)onBackButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc {
    [_webview release];
    [_headerTitleLabel release];
    [super dealloc];
}
@end
