//
//  ImagePreviewViewController.m
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 16/6/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "ImagePreviewViewController.h"
#import "ImageLoader.h"
#import "GeneralUiTool.h"
#import "ShareLocale.h"


@interface ImagePreviewViewController ()

@end

@implementation ImagePreviewViewController


-(id) initWithFeedItemID:(NSString*)feedItemID
{
    self = [super initWithNibName:@"ImagePreviewViewController" bundle: nil];
    if (self) {
        self.feedItemID = feedItemID;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self loadData];
    
    [self refreshLocale];
    
}

-(void) refreshLocale{
    
    self.headerTitleLabel.text = [ShareLocale textFromKey:@"photopage_title"];
    
    
}

-(void) loadData{
    [self showLoadingPopup:self.view];
    ImageLoader* loader = [ImageLoader shareInstance];
    [loader downloadImageWithFeedID:self.feedItemID delegate:self];
}
-(void) onRequestSuccessOfRequest:(ImageLoaderRequest *)req feedItemID:(NSString *)feedItemId response:(id)jsonResponse contentType:(NSString *)contentType{

    [self hideLoadingPopup];
    
    NSData* imageData = (NSData*) jsonResponse;

    [self performSelectorOnMainThread:@selector(reloadImageView:) withObject:imageData waitUntilDone:NO];
    
}

-(void) reloadImageView:(NSData*)imageData{
    self.photoView.image = [UIImage imageWithData:imageData];
}


-(void) onRequestFailOfRequest:(ImageLoaderRequest *)req feedItemID:(NSString *)feedItemId error:(NSError *)error{
    [self hideLoadingPopup];
    [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"Error" message:[NSString stringWithFormat:@"Fail to load Photo due to: %@", error] ];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (IBAction)onHomeButtonClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)onBackButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc {

    [_headerTitleLabel release];
    [super dealloc];
}

@end
