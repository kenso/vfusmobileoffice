//
//  StoreSelectionViewController.m
//  VFStorePerformanceApp_Billy
//
//  Created by Developer 2 on 1/4/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "StoreSelectionViewController.h"
#import "Constants.h"

@interface StoreSelectionViewController ()

@end

@implementation StoreSelectionViewController




- (id)initWithStoreList:(NSArray*)storeArray{
    self = [super initWithNibName:@"StoreSelectionViewController" bundle:nil];
    if (self) {
        self.recordArray = storeArray;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.recordArray = @[
                             @{@"Id":@"", @"Name": @"Any", @"emfa__Store_Code__c":@""},
                             @{@"Id":@"a0O9000000EjAHmEAN", @"Name": @"4201_Tung Chung Citygate", @"emfa__Store_Code__c":@"4201"}, @{@"Id":@"a0O9000000EjFUJEA3", @"Name":@"4202_New Town Plaza", @"emfa__Store_Code__c":@"4202"}
                             ];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [_tableView release];
    [super dealloc];
}



#pragma mark - Table view datasource/delegate

-(float) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.recordArray count];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    NSDictionary* data = [self.recordArray objectAtIndex:indexPath.row];
    if(self.delegate!=nil){
        [self.delegate onSelectdStore: data];
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tview cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary* data = [self.recordArray objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tview dequeueReusableCellWithIdentifier:@"StoreSelectionCell"];
    if(cell == nil){
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"StoreSelectionCell"] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.textLabel.hidden = YES;
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(20, 5, 300, 40)];
        label.tag = 901;
        label.text = @"";
        [cell addSubview:label];
    }
    
    UILabel* dateLabel = (UILabel*)[cell viewWithTag:901];
    dateLabel.text = [data objectForKey:@"Name"];
    cell.tag = indexPath.row;
    return cell;
    
}

@end
