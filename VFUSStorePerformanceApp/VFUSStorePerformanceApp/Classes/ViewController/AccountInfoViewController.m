//
//  AccountInfoViewController.m
//
//  Created by Developer 2 on 9/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import "AccountInfoViewController.h"
#import "UIHelper.h"
#import "Constants.h"
#import "StoreInfoViewController.h"

@interface AccountInfoViewController ()

@end

@implementation AccountInfoViewController

- (id)initWithDictionary:(NSDictionary*)data
{
    self = [super init];
    if (self) {
        self.data = data;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.contactTable.delegate = self;
    self.contactTable.dataSource = self;
    self.storesTable.delegate = self;
    self.storesTable.dataSource = self;
    
    self.storeRecords = [NSMutableArray array];
    self.contactRecords = [NSMutableArray array];
    
    [UIHelper applyPurpleSmallButtonImageOnButton:self.backButton];
    
    self.contactTable.backgroundColor = TABLE_BG_COLOR_GREEN;
    self.contactTable.backgroundView = nil;
    self.storesTable.backgroundColor = TABLE_BG_COLOR_BLUE;
    self.storesTable.backgroundView = nil;
    
    [self fillData];
    [self loadData];

    
}

-(void)fillData{
    self.parentCompanyNameLabel.text = [self getFieldDisplayWithFieldName:@"Parent.Name" withDictionary:self.data];
    self.companyNameLabel.text = [self getFieldDisplayWithFieldName:@"Name" withDictionary:self.data];
    self.customerCodeLabel.text = [self getFieldDisplayWithFieldName:@"Customer_Code__c" withDictionary:self.data];
    self.SAPCodeLabel.text = [self getFieldDisplayWithFieldName:@"Account_Code__c" withDictionary:self.data];
    self.creditLimitLabel.text = [self getSimplePriceStringWithFloat: [[self.data objectForKey: @"Credit_Limit__c"] floatValue]];
//    self.creditLimitLabel.text = [self getFieldDisplayWithFieldName:@"Credit_Limit__c" withDictionary:self.data];
    self.customerGroupLabel.text = [self getFieldDisplayWithFieldName:@"Customer_Group__r.Name" withDictionary:self.data];
    self.emailLabel.text = [self getFieldDisplayWithFieldName:@"Email__c" withDictionary:self.data];
    self.mailAddrLabel.text = [self getFieldDisplayWithFieldName:@"Mailing_Address" withDictionary:self.data];
    self.phoneLabel.text = [self getFieldDisplayWithFieldName:@"Phone" withDictionary:self.data];
    self.faxLabel.text = [self getFieldDisplayWithFieldName:@"Fax" withDictionary:self.data];
    self.websiteLabel.text = [self getFieldDisplayWithFieldName:@"Website" withDictionary:self.data];
    self.accountReceivableLabel.text = [self getSimplePriceStringWithFloat: [[self.data objectForKey: @"Account_Receivable__c"] floatValue]];
//    self.accountReceivableLabel.text = [self getFieldDisplayWithFieldName:@"Account_Receivable__c" withDictionary:self.data];
}


- (void)dealloc {
    [_companyNameLabel release];
    [_customerCodeLabel release];
    [_SAPCodeLabel release];
    [_customerGroupLabel release];
    [_accountReceivableLabel release];
    [_creditLimitLabel release];
    [_phoneLabel release];
    [_faxLabel release];
    [_emailLabel release];
    [_websiteLabel release];
    [_mailAddrLabel release];
    [_storesTable release];
    [_contactTable release];
    [_parentCompanyNameLabel release];
    [super dealloc];
}


#pragma mark - Network handling
-(void) loadData{
    [self showLoadingPopup: self.view];
    
    requestCount = 2;
    
    NSString* storeSql = [NSString stringWithFormat:@"SELECT Id, Name, emfa__Account__c, emfa__Address__c, Address_Chinese__c, Address_LatLong__c, emfa__Email__c, emfa__Fax__c,  emfa__Phone__c, emfa__Primary_Latitude__c, emfa__Primary_Longitude__c  FROM emfa__Store__c WHERE emfa__Active__c = TRUE AND emfa__Account__c = '%@' ORDER BY Name ASC", [self.data  objectForKey:@"Id"]];
    //Opening_Hours__c, Day_Off__c, Store_Code__c, SAP_Site_Code__c, Store_Type__c, (SELECT Id, Name, Department, Description, Email, Fax, HomePhone, MobilePhone, Phone, Title FROM Contacts__r),(SELECT Id, Name, Store__c, emfa__FeedItemId__c FROM Shared_Media__r)
    fetchStoreRequest = [[SFRestAPI sharedInstance] requestForQuery:storeSql];
    [[SFRestAPI sharedInstance] send:fetchStoreRequest delegate:self];
    
    NSString* contactSql = [NSString stringWithFormat:@"SELECT Id, Name, Department, Description, Email, Fax, HomePhone, MobilePhone, Phone, Title, Account.Name, Store__r.Name FROM Contact WHERE Account.Id = '%@' ORDER BY Store__r.Name ASC", [self.data  objectForKey:@"Id"]];
    fetchContactRequest = [[SFRestAPI sharedInstance] requestForQuery:contactSql];
    [[SFRestAPI sharedInstance] send:fetchContactRequest delegate:self];
    
}

- (void)request:(SFRestRequest *)request didLoadResponse:(id)jsonResponse{
    id done = [jsonResponse objectForKey:@"done"];
    if(done!=nil && [done intValue]==1){
        NSArray* records = [jsonResponse objectForKey:@"records"];
        if(fetchContactRequest == request){
            [self.contactRecords removeAllObjects];
            [self.contactRecords addObjectsFromArray:  records];
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [self.contactTable reloadData];
            });
        }else{
            [self.storeRecords removeAllObjects];
            [self.storeRecords addObjectsFromArray:  records];
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [self.storesTable reloadData];
            });
        }
        requestCount--;
        if(requestCount<=0){
            [self hideLoadingPopupWithFadeout];
        }
    }else{
        
    }
}


- (void)request:(SFRestRequest *)request didFailLoadWithError:(NSError*)error{
    
}


- (void)requestDidCancelLoad:(SFRestRequest *)request{
    
}


- (void)requestDidTimeout:(SFRestRequest *)request{
    
}

#pragma mark - table data source and delegate

-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.storesTable){
        NSDictionary* storeData = [self.storeRecords objectAtIndex:indexPath.row];
        StoreInfoViewController* vc = [[[StoreInfoViewController alloc] initWithDictionary:storeData] autorelease];
        [self.navigationController pushViewController: vc animated:YES];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView == self.contactTable){
        return [self.contactRecords count];
    }else if (tableView == self.storesTable){
        return [self.storeRecords count];
    }else{
        return 0;
    }
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(tableView == self.contactTable){
        return [self getContactTableViewCell:indexPath.row];
    }else{
        return [self getStoreTableViewCell:indexPath.row];
    }
}


-(UITableViewCell*)getContactTableViewCell:(int)index{
    UITableViewCell *cell = [self.contactTable dequeueReusableCellWithIdentifier:@"ContactCell"];
    if(cell == nil){
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ContactCell"] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.hidden = YES;
        cell.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(10, 0, 200, 40) parent:cell tag: 901];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(209, 0, 113, 40) parent:cell tag: 902];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(322, 0, 113, 40) parent:cell tag: 903];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(435, 0, 174, 40) parent:cell tag: 904];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(609, 0, 221, 40) parent:cell tag: 905];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(830, 0, 184, 40) parent:cell tag: 906];
    }
    NSDictionary* data = [self.contactRecords objectAtIndex: index];
    UITextView* txtView1 = (UITextView*)[cell viewWithTag:901];
    txtView1.text = [self getFieldDisplayWithFieldName:@"Store__r.Name" withDictionary: data];
    UITextView* txtView2 = (UITextView*)[cell viewWithTag:902];
    txtView2.text = [self getFieldDisplayWithFieldName:@"Name" withDictionary: data];
    UITextView* txtView3 = (UITextView*)[cell viewWithTag:903];
    txtView3.text = [self getFieldDisplayWithFieldName:@"Phone" withDictionary: data];
    UITextView* txtView4 = (UITextView*)[cell viewWithTag:904];
    txtView4.text = [self getFieldDisplayWithFieldName:@"MobilePhone" withDictionary: data];
    UITextView* txtView5 = (UITextView*)[cell viewWithTag:905];
    txtView5.text = [self getFieldDisplayWithFieldName:@"Title" withDictionary: data];
    UITextView* txtView6 = (UITextView*)[cell viewWithTag:906];
    txtView6.text = [self getFieldDisplayWithFieldName:@"Department" withDictionary: data];
    
    return cell;
}


-(UITableViewCell*)getStoreTableViewCell:(int)index{
    UITableViewCell *cell = [self.contactTable dequeueReusableCellWithIdentifier:@"StoreCell"];
    if(cell == nil){
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"StoreCell"] autorelease];
        cell.textLabel.hidden = YES;
        cell.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
        UIView* bgView = [[[UIView alloc] init] autorelease];
        bgView.backgroundColor = [UIColor colorWithWhite:1 alpha:0.1];
        cell.selectedBackgroundView = bgView;
        [self createNewTextLabelWithDefaultStyle: CGRectMake(10, 0, 200, 40) parent:cell tag: 901];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(209, 0, 412, 40) parent:cell tag: 902];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(621, 0, 113, 40) parent:cell tag: 903];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(734, 0, 152, 40) parent:cell tag: 904];
        [self createNewTextLabelWithDefaultStyle: CGRectMake(883, 0, 131, 40) parent:cell tag: 905];
    }
    NSDictionary* data = [self.storeRecords objectAtIndex: index];
    UITextView* txtView1 = (UITextView*)[cell viewWithTag:901];
    txtView1.text = [self getFieldDisplayWithFieldName:@"Name" withDictionary: data];
    UITextView* txtView2 = (UITextView*)[cell viewWithTag:902];
    txtView2.text = [self getFieldDisplayWithFieldName:@"emfa__Address__c" withDictionary: data];
    UITextView* txtView3 = (UITextView*)[cell viewWithTag:903];
    txtView3.text = [self getFieldDisplayWithFieldName:@"Store_Code__c" withDictionary: data];
    UITextView* txtView4 = (UITextView*)[cell viewWithTag:904];
    txtView4.text = [self getFieldDisplayWithFieldName:@"emfa__Phone__c" withDictionary: data];
    UITextView* txtView6 = (UITextView*)[cell viewWithTag:905];
    txtView6.text = [self getFieldDisplayWithFieldName:@"Day_Off__c" withDictionary: data];
    
    return cell;
}




#pragma mark - UI Button handler
- (IBAction) onBackButtonClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
