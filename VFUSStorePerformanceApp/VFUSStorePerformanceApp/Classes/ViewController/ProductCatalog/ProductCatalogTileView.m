//
//  ProductCatalogTileView.m
//  PernodRicardMobileApp
//
//  Created by fai on 1/3/13.
//  Copyright (c) 2013 Laputa. All rights reserved.
//

#import "ProductCatalogTileView.h"


@implementation ProductCatalogTileView

@synthesize delegate;
@synthesize btn;

- (id)initWithFrame:(CGRect)frame andProductCatalogListItem:(ProductCatalogListItem*)myListItem andTag:(int)myTag
{
    
    NSLog(@"Function: ProductCatalogTileView initWithFrame: start");
    @try {
        self = [super initWithFrame:frame];
        if (self) {
            // Initialization code
            
            float borderWidth= 0;
            
//            [self setBackgroundColor:[CommonUtils getColorConstantForKey:@"product catalog tile border color"]];
            
            
            imgX = borderWidth;
            imgY = imgX;
            imgWidth = self.frame.size.width - (borderWidth*2);
            imgHeight = self.frame.size.height - (borderWidth*2);

            
            
            BOOL hasBGImage = YES;
            UIImageView *btnBGImage = nil;
            if (myListItem.itemImageURL) {
            //    NSLog(@"myListItem.itemImageURL = %@, X:%f, Y:%f", myListItem.itemImageURL, imgX, imgY);
//                btnBGImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:myListItem.itemImageURL]];
            } else if (myListItem.itemImageFileName) {
                NSLog(@"myListItem.itemImageFileName = %@, X:%f, Y:%f", myListItem.itemImageFileName, imgX, imgY);
                btnBGImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, imgWidth, imgHeight)];
                [btnBGImage setImage:[UIImage imageNamed:myListItem.itemImageFileName]];
            } else if ( myListItem.itemImageFeedId ){
                NSLog(@"Load Btn Image with feedId: %@", myListItem.itemImageFeedId);
//                btnBGImage = [[CommonUtils shareInstance] getSFDocImageWithFeedId: myListItem.itemImageFeedId andFrame:CGRectMake(0, 0, imgWidth, imgHeight)];
            } else {
                hasBGImage = NO;
            }
            
            btn = [UIButton buttonWithType:UIButtonTypeCustom];
            
            [btn addSubview:btnBGImage];
            [btn sendSubviewToBack:btnBGImage];
            
            [btn setFrame:CGRectMake(0, 0, imgWidth, imgHeight)];
            //[btn setFrame:CGRectMake(imgX, imgY, imgWidth, imgHeight)];
            [btn addTarget:self action:@selector(btnTouchAction:) forControlEvents:UIControlEventTouchUpInside];
            [btn setTag:myTag];

            [self addSubview:btn];
            if ( myListItem.objectType == 2 ){
                UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, imgWidth, imgHeight)];
                label.text = myListItem.itemName;
                [label setTextColor:[UIColor whiteColor]];
                [label setTextAlignment:NSTextAlignmentCenter];
                [label setFont:[UIFont systemFontOfSize:32.0f]];
                [self addSubview:label];
            }
        }
        NSLog(@"Function: ProductCatalogTileView initWithFrame: end");
        return self;

    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
    }

- (void)btnTouchAction:(id)sender{
    NSLog(@"button touched: %i", [sender tag]);
    
    [delegate tileTouchAction:[sender tag]];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/



@end
