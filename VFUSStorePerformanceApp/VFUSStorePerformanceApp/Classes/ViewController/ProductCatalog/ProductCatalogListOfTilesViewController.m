//
//  ProductCatalogListOfTilesViewController.m
//  PernodRicardMobileApp
//
//  Created by fai on 1/3/13.
//  Copyright (c) 2013 Laputa. All rights reserved.
//

#import "ProductCatalogListOfTilesViewController.h"
#import "ProductCatalogListItem.h"
#import "ProductCatalogTileView.h"

#import "SubbrandDetailsViewController.h"
#import <CustomSalesforceSDK/SFSyncManager.h>

#import "Brand.h"
#import "SharedMedia.h"
#import "SubBrand.h"
#import "Product.h"

@interface ProductCatalogListOfTilesViewController ()

@end

@implementation ProductCatalogListOfTilesViewController

@synthesize arrListItem;
@synthesize arrTileView;
@synthesize navigationBarTitle;
@synthesize recordId;
@synthesize titleImageView;
@synthesize logoImageView;
@synthesize subTitleLabel;
@synthesize titleRecordId;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andMode:(int)myMode andTitle:(NSString*)myTitle andRecordId:(NSString*)myRecordId;
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    NSLog(@"Function: ProductCatalogListOfTilesViewController Start");
    if (self) {
        // Custom initialization
        mode=myMode;
        arrListItem = [[NSMutableArray alloc] init];
        arrTileView = [[NSMutableArray alloc] init];
        navigationBarTitle = [[NSMutableString alloc] initWithString:myTitle];
        recordId = [[NSMutableString alloc] initWithString:myRecordId];
        titleRecordId = [[NSMutableString alloc] initWithString:@""];
        self.displayObjs = [[NSMutableArray alloc] init];
        
        NSLog(@"My Record Id: %@", recordId);
    }
    return self;
}

-(void)Mode2Settings
{
    NSLog(@"Function: Mode2Settings Start");
    @try {
        
        NSInteger useIndex = 0;
        NSArray *item1 = [NSArray arrayWithObjects:@"C_title-lnter.png", [NSNumber numberWithFloat:496.0], [NSNumber numberWithFloat:35], nil];
        NSArray *item2 = [NSArray arrayWithObjects:@"C_title-local.png", [NSNumber numberWithFloat:314.0], [NSNumber numberWithFloat:35], nil];
        NSArray *item3 = [NSArray arrayWithObjects:@"C_title-modern.png", [NSNumber numberWithFloat:341.0], [NSNumber numberWithFloat:35], nil];
//        NSArray *itemList = [NSArray arrayWithObjects: item1, item2, item3, nil];
  NSArray *itemList = [NSArray arrayWithObjects: item2, item1, item3, nil];
        
        
        if ( [recordId isEqualToString:@"100"] )
            useIndex = 0;
        else if ( [recordId isEqualToString:@"200"] )
            useIndex = 1;
        else if ( [recordId isEqualToString:@"300"] )
            useIndex = 2;
        
        NSArray *useItem = [itemList objectAtIndex:useIndex];
        
        float width = [[useItem objectAtIndex:1]floatValue];
        float height = [[useItem objectAtIndex:2]floatValue];
        
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        screenRect.origin.x = 1024 - 50 - width;
        screenRect.origin.y = 50;
        screenRect.size.width = width;
        screenRect.size.height = height;
//        [titleImageView setFrame:screenRect];
//        [titleImageView setImage:[UIImage imageNamed:[useItem objectAtIndex:0]]];
        titleRecordId = recordId;

    }
    @catch (NSException *exception) {
        
    }
    @finally {
        NSLog(@"Function: Mode2Settings End");
    }

}

-(void)Mode3Settings
{
    NSLog(@"Function: Mode3Settings Start");
    @try {
        NSInteger useIndex = 0;
        NSArray *item1 = [NSArray arrayWithObjects:@"C_title-lnter.png", [NSNumber numberWithFloat:496.0], [NSNumber numberWithFloat:35], nil];
        NSArray *item2 = [NSArray arrayWithObjects:@"C_title-local.png", [NSNumber numberWithFloat:314.0], [NSNumber numberWithFloat:35], nil];
        NSArray *item3 = [NSArray arrayWithObjects:@"C_title-modern.png", [NSNumber numberWithFloat:341.0], [NSNumber numberWithFloat:35], nil];
       // NSArray *itemList = [NSArray arrayWithObjects: item1, item2, item3, nil];
        NSArray *itemList = [NSArray arrayWithObjects: item2, item1, item3, nil];
        
        
        if ( [titleRecordId isEqualToString:@"100"] )
            useIndex = 0;
        else if ( [titleRecordId isEqualToString:@"200"] )
            useIndex = 1;
        else if ( [titleRecordId isEqualToString:@"300"] )
            useIndex = 2;
        
        NSArray *useItem = [itemList objectAtIndex:useIndex];
        
        float width = [[useItem objectAtIndex:1]floatValue];
        float height = [[useItem objectAtIndex:2]floatValue];
        
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        screenRect.origin.x = 1024 - 50 - width;
        screenRect.origin.y = 34;
        screenRect.size.width = width;
        screenRect.size.height = height;
//        [titleImageView setFrame:screenRect];
//        [titleImageView setImage:[UIImage imageNamed:[useItem objectAtIndex:0]]];
        
        screenRect = subTitleLabel.frame;
        screenRect.origin.y = 70;
        subTitleLabel.frame = screenRect;
        subTitleLabel.text = navigationBarTitle;

    }
    @catch (NSException *exception) {
        
    }
    @finally {
        NSLog(@"Function: Mode3Settings End");
    }
}

-(void)ScrollViewSettings
{
    NSLog(@"Function: ScrollViewSettings Start");
    @try {
        /////Set up ScrollView
        tilePerPage =6;
        numTilePerRow= 3;
        slideWidth = mainViewWrapper.frame.size.width;
        slideHeight = mainViewWrapper.frame.size.height;
        scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, slideWidth, slideHeight)];
        //scrollView.contentSize = CGSizeMake(slideWidth*numPage, slideHeight);
        scrollView.maximumZoomScale = 1.0;
        scrollView.minimumZoomScale = 1.0;
        scrollView.showsHorizontalScrollIndicator=NO;
        scrollView.showsVerticalScrollIndicator=NO;
        scrollView.clipsToBounds = YES;
        [scrollView setBackgroundColor:[UIColor clearColor]];
        [scrollView setPagingEnabled:YES];
        [scrollView setDelegate:self];
        
        [[self view] addSubview:scrollView];

    }
    @catch (NSException *exception) {
        
    }
    @finally {
        NSLog(@"Function: ScrollViewSettings End");
    }
    
}

- (void)viewDidLoad
{
    NSLog(@"Function: viewDidLoad Start");
    
    @try {
        [super viewDidLoad];
        self.navigationController.navigationBarHidden = YES;
        self.title = @"Notification History";
        
        
        if ( mode == 1 || mode == 2 )
        {
            [self Mode2Settings];
        }
        else if ( mode == 3 )
        {
            [self Mode3Settings];
        }
        
        [self ScrollViewSettings];
        [self AddBackButton];

        [self displayContent];

        UIImageView *bgView = [[UIImageView alloc] initWithFrame: [self.view frame]];
        [bgView setImage: [UIImage imageNamed:@"image4.jpg"]];
        [self.view addSubview:bgView];
        [self.view sendSubviewToBack:bgView];
        
    }
    @catch (NSException *exception) {
         NSLog(@"Exception=%@", exception);
    }
    @finally {
        NSLog(@"Function: viewDidLoad End");
    }
    
}

-(void)AddBackButton
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self
               action:@selector(backButtonClicked:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setImage:[UIImage imageNamed:@"btnBack.png"] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"btnBack-on.png"] forState:UIControlStateHighlighted];
    button.frame = CGRectMake(25.0, 25.0, 75.0, 75.0);
    [[self view] addSubview:button];

}


- (IBAction)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)displayContent
{
    NSLog(@"Function: displayContent Start");
    @try {
        NSString *theRecordId = [NSString stringWithFormat:@"1"];
        if (self.displayObjs ){
            
        if ( mode == 1 ){
            NSLog(@"rayTest mode : %i", mode);
            [self createListItemWithRecordId:theRecordId andItemName:@"Vans" andItemImageFileName:@"brand_Vans_a.jpg" andObjectType:1];
            [self createListItemWithRecordId:theRecordId andItemName:@"Vans" andItemImageFileName:@"brand_TheNorthFace_a.jpg" andObjectType:1];
            [self createListItemWithRecordId:theRecordId andItemName:@"Vans" andItemImageFileName:@"brand_WranglerHero_a.jpg" andObjectType:1];
            [self createListItemWithRecordId:theRecordId andItemName:@"Vans" andItemImageFileName:@"brand_7forAllManKind_a.jpg" andObjectType:1];
            [self createListItemWithRecordId:theRecordId andItemName:@"Vans" andItemImageFileName:@"brand_bulwark_a.jpg" andObjectType:1];
            [self createListItemWithRecordId:theRecordId andItemName:@"Vans" andItemImageFileName:@"brand_EllaMoss_a.jpg" andObjectType:1];
            [self createListItemWithRecordId:theRecordId andItemName:@"Vans" andItemImageFileName:@"brand_lee_a.jpg" andObjectType:1];
            [self createListItemWithRecordId:theRecordId andItemName:@"Vans" andItemImageFileName:@"brand_rustler_a.jpg" andObjectType:1];
        }else if ( mode == 2){
            [self createListItemWithRecordId:theRecordId andItemName:@"SKATE" andItemImageFileName:@"C_btnBlue.png" andObjectType:2];
            [self createListItemWithRecordId:theRecordId andItemName:@"SURF" andItemImageFileName:@"C_btnGreen.png" andObjectType:2];
            [self createListItemWithRecordId:theRecordId andItemName:@"SNOW" andItemImageFileName:@"C_btnOrange.png" andObjectType:2];
            [self createListItemWithRecordId:theRecordId andItemName:@"BMX" andItemImageFileName:@"C_btnBlue.png" andObjectType:2];
            [self createListItemWithRecordId:theRecordId andItemName:@"LXVI" andItemImageFileName:@"C_btnGreen.png" andObjectType:2];
            [self createListItemWithRecordId:theRecordId andItemName:@"WOMENS" andItemImageFileName:@"C_btnOrange.png" andObjectType:2];

        }else if ( mode == 3){
            [self createListItemWithRecordId:theRecordId andItemName:@"Vans" andItemImageFileName:@"1-1.jpg" andObjectType:1];
            [self createListItemWithRecordId:theRecordId andItemName:@"Vans" andItemImageFileName:@"1-2.jpg" andObjectType:1];
            [self createListItemWithRecordId:theRecordId andItemName:@"Vans" andItemImageFileName:@"1-3.jpg" andObjectType:1];
            [self createListItemWithRecordId:theRecordId andItemName:@"Vans" andItemImageFileName:@"1-4.jpg" andObjectType:1];
            [self createListItemWithRecordId:theRecordId andItemName:@"Vans" andItemImageFileName:@"1-5.jpg" andObjectType:1];
            [self createListItemWithRecordId:theRecordId andItemName:@"Vans" andItemImageFileName:@"1-6.jpg" andObjectType:1];
            [self createListItemWithRecordId:theRecordId andItemName:@"Vans" andItemImageFileName:@"1-7.jpg" andObjectType:1];
            [self createListItemWithRecordId:theRecordId andItemName:@"Vans" andItemImageFileName:@"1-8.jpg" andObjectType:1];
            [self createListItemWithRecordId:theRecordId andItemName:@"Vans" andItemImageFileName:@"1-9.jpg" andObjectType:1];
        }
        
/*
        if (self.displayObjs ){
            for ( SFSyncObject * syncObj in self.displayObjs) {
                if ( mode == 1 ){
                    theRecordId = [syncObj valueForFieldName:SFOBJ_COMMON_FN_ID];
                    theName = [syncObj valueForFieldName:SFOBJ_COMMON_FN_NAME];
                    theFeedId = [[[syncObj valueForFieldName:SFOBJ_BRAND_CHILD_DOC] objectAtIndex:0] valueForFieldName:SFOBJ_SHARED_MEDIA_FN_FEEDITEM_ID];
                }
                else if(mode==2) { //1=product brands
                    theRecordId = [syncObj valueForFieldName:SFOBJ_COMMON_FN_ID];
                    theName = [syncObj valueForFieldName:SFOBJ_COMMON_FN_NAME];
                    theFeedId = [[[syncObj valueForFieldName:SFOBJ_SUB_BRAND_CHILD_DOC] objectAtIndex:0] valueForFieldName:SFOBJ_SHARED_MEDIA_FN_FEEDITEM_ID];
                } else if(mode==3) { //1=product sub-brands
                    theRecordId = [syncObj valueForFieldName:SFOBJ_COMMON_FN_ID];
                    theName = [syncObj valueForFieldName:SFOBJ_COMMON_FN_NAME];
                    theFeedId = [[[syncObj valueForFieldName:SFOBJ_PRODUCT_CHILD_DOC] objectAtIndex:0] valueForFieldName:SFOBJ_SHARED_MEDIA_FN_FEEDITEM_ID];
                }
                
//                NSURL *imageURL = [syncObj childDocFileUrlWhereTypeField:fnChildDocType equalToString:btnBgType];
                NSLog(@"createListItemWithRecordId: %@", theRecordId);
                [self createListItemWithRecordId:theRecordId andItemName:theName andItemImageFeedId:theFeedId andObjectType:mode];
            }
*/
            NSLog(@"[arrListItem count]: %i", [arrListItem count]);
            
            numTile=[arrListItem count];
            
            //NSLog(@"Number of Pages: %i", numPage);
            
            tilePerPage = 6;
            numTilePerRow = 3;
            float tileWidth = 225;
            float tileHeight = 225;
            float firstTileX = 150;
            float firstTileY = 125;
            float tileSpacingX = tileWidth + 25;
            float tileSpacingY = tileHeight + 25;
            
            
            if ( mode == 1 || mode == 2 )
            {
                tilePerPage = 6;
                numTilePerRow = 3;
                tileWidth = 225;
                tileHeight = 225;
                firstTileX = 150;
                firstTileY = 125;
                tileSpacingX = tileWidth + 25;
                tileSpacingY = tileHeight + 25;
                numPage=ceil((float)numTile/(float)tilePerPage);
                
                scrollView.contentSize = CGSizeMake(slideWidth*numPage, slideHeight);
            }
            else if ( mode == 3 )
            {
                tilePerPage = 15;
                numTilePerRow = 5;
                tileWidth = 150;
                tileHeight = 150;
                firstTileX = 107;
                firstTileY = 125;
                tileSpacingX = tileWidth + 15;
                tileSpacingY = tileHeight + 15;
                numPage=ceil((float)numTile/(float)tilePerPage);
                
                scrollView.contentSize = CGSizeMake(slideWidth*numPage, slideHeight);
            }
            
            int myPage=0;
            int myItemCountInPage=0;
            
            NSLog(@"Number of wine list: %i", [arrListItem count]);
            for(int n=0; n<[arrListItem count]; n++){
                
                float myPageX = myPage*slideWidth;
                float myRow = floor(myItemCountInPage/numTilePerRow);
                float myColumn = myItemCountInPage % numTilePerRow;
                
                float myX=myPageX + firstTileX + myColumn*tileSpacingX;
                float myY=firstTileY + myRow*tileSpacingY;
                
               NSLog(@"Wine Image Name: %@, x:%f, y:%f, tileWidth:%f, tileHeight:%f", [[arrListItem objectAtIndex:n] itemImageFeedId], myX, myY, tileWidth, tileHeight);
                ProductCatalogTileView *tile = [[ProductCatalogTileView alloc] initWithFrame:CGRectMake(myX, myY, tileWidth, tileHeight) andProductCatalogListItem:[arrListItem objectAtIndex:n] andTag:n];
                [scrollView addSubview:tile];
                [tile setDelegate:self];
                [arrTileView addObject:tile];
                [tile release];
                
                myItemCountInPage++;
                if(myItemCountInPage>=tilePerPage){
                    myPage++;
                    myItemCountInPage=0;
                }
            }
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"Stop in exeption: %@", exception.description);
    }
    @finally {
        NSLog(@"Function: displayContent End");
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)displayLocalizedTextAndNavBarSetup{
    self.title = [NSString stringWithFormat:@"%@", navigationBarTitle];
    
}




- (void)createListItemWithRecordId:(NSString*)myRecordId andItemName:(NSString*)myItemName andItemImageFileName:(NSString*)myItemImageFileName andObjectType:(int)myObjectType{
    ProductCatalogListItem *myListItem = [[ProductCatalogListItem alloc] initWithRecordId:myRecordId andItemName:myItemName andItemImageFileName:myItemImageFileName andObjectType:myObjectType];
    [arrListItem addObject:myListItem];
    [myListItem release];
}

- (void)createListItemWithRecordId:(NSString*)myRecordId andItemName:(NSString*)myItemName andItemImageFeedId:(NSString*)myItemFeedId andObjectType:(int)myObjectType{
    ProductCatalogListItem *myListItem = [[ProductCatalogListItem alloc] initWithRecordId:myRecordId andItemName:myItemName andItemImageFeedId:myItemFeedId andObjectType:myObjectType];
    [arrListItem addObject:myListItem];
    [myListItem release];
 
}

- (void)createListItemWithRecordId:(NSString*)myRecordId andItemName:(NSString*)myItemName andItemImageURL:(NSURL*)myItemImageURL andObjectType:(int)myObjectType{
    ProductCatalogListItem *myListItem = [[ProductCatalogListItem alloc] initWithRecordId:myRecordId andItemName:myItemName andItemImageURL:myItemImageURL andObjectType:myObjectType];
    [arrListItem addObject:myListItem];
    [myListItem release];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	// Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    if(interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        return YES;
    }
    else if(interfaceOrientation == UIInterfaceOrientationLandscapeLeft) {
        return YES;
    }
    else {
        return NO;
    }
}

- (void)dealloc {
    [mainViewWrapper release];
    [_displayObjs release];
    [titleImageView release];
    [logoImageView release];
    [subTitleLabel release];
    [super dealloc];
}
- (void)viewDidUnload {
    [mainViewWrapper release];
    mainViewWrapper = nil;
    self.displayObjs = nil;
    [arrListItem release];
    arrListItem = nil;
    [arrTileView release];
    arrTileView = nil;
    [scrollView release];
    scrollView = nil;
    [recordId release];
    recordId = nil;
    [titleRecordId release];
    titleRecordId = nil;
    [navigationBarTitle release];
    navigationBarTitle = nil;
    [self setTitleImageView:nil];
    [self setLogoImageView:nil];
    [self setSubTitleLabel:nil];
    [super viewDidUnload];
}



////TileView Delegate Function
- (void)tileTouchAction:(int)myTag{
    NSLog(@"Function: tileTouchAction: Start");
    
    
    @try {
        if([[[arrListItem objectAtIndex:myTag] recordId] isEqualToString:@"00000000"]){
            NSLog(@"Skip tileTouchAction");
            return;
        }
        
        
        if(mode<3){ //list of categories, or list of brands
            ProductCatalogListOfTilesViewController *nextView=[[ProductCatalogListOfTilesViewController alloc]initWithNibName:@"ProductCatalogListOfTilesViewController" bundle:nil andMode:(mode+1) andTitle:[[arrListItem objectAtIndex:myTag] itemName] andRecordId:[[arrListItem objectAtIndex:myTag] recordId]];
            
            if ( mode == 1 )
            {
                nextView.titleRecordId = [NSMutableString stringWithString:[[arrListItem objectAtIndex:myTag] recordId]];
            }
            else if ( mode == 2 )
            {
                nextView.titleRecordId = titleRecordId;
            }
            else if ( mode == 3 )
            {
                nextView.titleRecordId = titleRecordId;
            }
            
            NSLog(@"///////////// nextView.titleRecordId = %d, %@, %@", mode, nextView.titleRecordId, [[arrListItem objectAtIndex:myTag] recordId]);
            
            [self.navigationController pushViewController:nextView animated:YES];
            [nextView release];
        }
        else if(mode==3){
            SubbrandDetailsViewController *nextView = [[SubbrandDetailsViewController alloc] initWithNibName:@"SubbrandDetailsViewController" bundle:nil andProduct:nil];
/*            SubbrandDetailsViewController *nextView=[[SubbrandDetailsViewController 
 andTitle:[[arrListItem objectAtIndex:myTag] itemName] andRecordId:[[arrListItem objectAtIndex:myTag] recordId]];
 */
            [nextView.titleRecordId setString:@"12345678"];//titleRecordId;
            [self.navigationController pushViewController:nextView animated:YES];
            [nextView release];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"exception: %@", exception.description);
    }
    @finally {
        NSLog(@"Function: tileTouchAction: End");
    }
    
}

- (NSURL*)getFileURLFromDocs:(NSArray*)docs withChildField:(NSString*)childField andChildExtensionField:(NSString*)childExtField whereTypeField:(NSString*)typeField equalToString:(NSString*)typeStr
{
//    NSLog(@"Function: getFileURLFromDocs: Start");
//    NSURL *result = nil;
//    
//    @try {
//        if (docs && [docs count] > 0) {
//            SFSyncObject *foundDoc = nil;
//            SFSyncObject *foundBinObj = nil;
//            for (SFSyncObject *doc in docs) {
//                NSString *docType = [doc valueForFieldName:typeField];
//                if (docType && [docType isEqualToString:typeStr]) {
//                    foundDoc = doc;
//                    break;
//                }
//            }
//            if (foundDoc) {
//                NSArray *binObjs = [foundDoc valueForFieldName:childField];
//                if (binObjs && [binObjs count] > 0) {
//                    foundBinObj = [binObjs objectAtIndex:0];
//                }
//            }
//            if (foundBinObj) {
//                NSString *binObjId = [foundBinObj valueForFieldName:SFOBJ_COMMON_FN_ID];
//                NSString *fileExt = [foundBinObj valueForFieldName:childExtField];
//                result = [SFBinaryHelper fileURLBySFID:binObjId fileExtension:fileExt];
//            }
//        }
//        return result;
//        
//    }
//    @catch (NSException *exception) {
//        result=nil;
//    }
//    @finally {
//        NSLog(@"Function: getFileURLFromDocs: End");
//        return  result;
//    }
    return nil;
}


@end

