//
//  ProductCatalogListItem.h
//  PernodRicardMobileApp
//
//  Created by fai on 1/3/13.
//  Copyright (c) 2013 Laputa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductCatalogListItem : NSObject{
    NSMutableString *recordId;
    NSMutableString *itemName;
    NSMutableString *itemImageFeedId;
    NSMutableString *itemImageFileName;
    int objectType; //1=product category, 2=product brand, 3=product sub-brand
}

@property (nonatomic, retain) NSMutableString *recordId;
@property (nonatomic, retain) NSMutableString *itemName;
@property (nonatomic, retain) NSMutableString *itemImageFeedId;
@property (nonatomic, retain) NSMutableString *itemImageFileName;
@property (nonatomic, retain) NSURL *itemImageURL;
@property (nonatomic, assign) int objectType;

- (id)initWithRecordId:(NSString*)myRecordId andItemName:(NSString*)myItemName andItemImageFileName:(NSString*)myItemImageFileName andObjectType:(int)myObjectType;
- (id)initWithRecordId:(NSString*)myRecordId andItemName:(NSString*)myItemName andItemImageFeedId:(NSString *)feedId andObjectType:(int)myObjectType;
- (id)initWithRecordId:(NSString*)myRecordId andItemName:(NSString*)myItemName andItemImageURL:(NSURL*)myItemImageURL andObjectType:(int)myObjectType;


@end
