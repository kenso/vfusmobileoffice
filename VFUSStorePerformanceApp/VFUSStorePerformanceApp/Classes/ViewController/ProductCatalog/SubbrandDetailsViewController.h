//
//  SubbrandDetailsViewController.h
//  PernodRicardMobileApp
//
//  Created by fai on 3/3/13.
//  Copyright (c) 2013 Laputa. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SubbrandDetailsMainVisualView.h"
#import "ProductCatalogRightOverlayView.h"

@interface SubbrandDetailsViewController : UIViewController<UIScrollViewDelegate, SFSyncDelegate>{
    IBOutlet UIView *mainViewWrapper;
    
    NSMutableString *navigationBarTitle;
    NSMutableString *recordId;
    SubbrandDetailsMainVisualView *mainVisualView;
    
    ProductCatalogRightOverlayView *descriptionOverlay;
    ProductCatalogRightOverlayView *itemGroupListOverlay;
    Boolean isOpenDescriptionOverlay;
    Boolean isOpenItemGroupListOverlay;
    
    float rightOverlayWidth;
    float rightOverlayHeight;
    float rightOverlayShowX;
    float rightOverlayHideX;
    float rightOverlayY;
    
    id currentView;
}
@property (retain, nonatomic) IBOutlet UIImageView *titleImageView;
@property (retain, nonatomic) IBOutlet UILabel *subTitleLabel;
@property (retain, nonatomic) Product *mProduct;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andProduct:(Product*)myProduct;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andTitle:(NSString*)myTitle andRecordId:(NSString*)myRecordId;


- (void)displayLocalizedTextAndNavBarSetup;

- (void)loadData;

//- (void)createButtonWithText:(NSString*)myText andFrame:(CGRect)myRect andTag:(int)myTag;
//- (void)btnAction:(id)sender;

@property (nonatomic, retain) NSMutableString *navigationBarTitle;
@property (nonatomic, retain) NSMutableString *recordId;
@property (nonatomic, retain) NSMutableString *titleRecordId;
@property (nonatomic, retain) SubBrand *mySubBrand;

@property (retain, nonatomic) IBOutlet UIView *ProInfoView;
@property (retain, nonatomic) IBOutlet UIView *ItemGroupsView;
@property (retain, nonatomic) IBOutlet UIView *DescView;
- (IBAction)onClickCheckIn:(id)sender;



- (void)playVideo:(NSString*)videoFileName;

@end
