//
//  ProductCatalogListItem.m
//  PernodRicardMobileApp
//
//  Created by fai on 1/3/13.
//  Copyright (c) 2013 Laputa. All rights reserved.
//

#import "ProductCatalogListItem.h"

@implementation ProductCatalogListItem

@synthesize recordId;
@synthesize itemName;
@synthesize itemImageFileName;
@synthesize objectType;

- (id)initWithRecordId:(NSString*)myRecordId andItemName:(NSString*)myItemName andItemImageFileName:(NSString*)myItemImageFileName andObjectType:(int)myObjectType
{
    self = [super init];
    if (self) {
        // Initialization code
        objectType=myObjectType; //1=product category, 2=product brand, 3=product sub-brand
        
        recordId = [[NSMutableString alloc] initWithString:myRecordId];
        itemName = [[NSMutableString alloc] initWithString:myItemName];
        itemImageFileName = [[NSMutableString alloc] initWithString:myItemImageFileName];
        self.itemImageURL = nil;
        
    }
    return self;
}

- (id)initWithRecordId:(NSString*)myRecordId andItemName:(NSString*)myItemName andItemImageURL:(NSURL*)myItemImageURL andObjectType:(int)myObjectType
{
    self = [super init];
    if (self) {
        // Initialization code
        objectType=myObjectType; //1=product category, 2=product brand, 3=product sub-brand
        
        recordId = [[NSMutableString alloc] initWithString:myRecordId];
        itemName = [[NSMutableString alloc] initWithString:myItemName];
        itemImageFileName = nil;
        self.itemImageURL = myItemImageURL;
        
    }
    return self;
}

- (id)initWithRecordId:(NSString*)myRecordId andItemName:(NSString*)myItemName andItemImageFeedId:(NSString *)feedId andObjectType:(int)myObjectType
{
    self = [super init];
    if (self) {
        // Initialization code
        objectType=myObjectType; //1=product category, 2=product brand, 3=product sub-brand
        
        recordId = [[NSMutableString alloc] initWithString:myRecordId];
        itemName = [[NSMutableString alloc] initWithString:myItemName];
        itemImageFileName = nil;
        NSLog(@"initTileImageWithFeedId: %@", feedId);
        if ( feedId ){
            self.itemImageFeedId = [[NSMutableString alloc] initWithString:feedId];
        }
        self.itemImageURL = nil;
        
    }
    return self;
}

- (void)dealloc {
    [recordId release];
    [itemName release];
    [itemImageFileName release];
    [_itemImageFeedId release];
    [_itemImageURL release];
    [super dealloc];
}




@end
