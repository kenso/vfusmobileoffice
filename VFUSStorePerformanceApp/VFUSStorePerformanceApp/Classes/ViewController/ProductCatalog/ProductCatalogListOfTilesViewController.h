//
//  ProductCatalogListOfTilesViewController.h
//  PernodRicardMobileApp
//
//  Created by fai on 1/3/13.
//  Copyright (c) 2013 Laputa. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ProductCatalogListOfTilesViewController : UIViewController<UIScrollViewDelegate>{
    int mode; //1=product category list; 2=brand list; 3=subbrand list
    
    IBOutlet UIView *mainViewWrapper;
    
    int numTile;
    float slideWidth;
    float slideHeight;
    int tilePerPage;
    int numPage;
    int numTilePerRow;
    UIScrollView* scrollView;
    
    NSMutableArray *arrListItem;
    NSMutableArray *arrTileView;
    
    NSMutableString *navigationBarTitle;
    NSMutableString *recordId;
}
@property (retain, nonatomic) IBOutlet UIImageView *titleImageView;
@property (retain, nonatomic) IBOutlet UIImageView *logoImageView;
@property (retain, nonatomic) IBOutlet UILabel *subTitleLabel;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andMode:(int)myMode andTitle:(NSString*)myTitle andRecordId:(NSString*)myRecordId;

- (void)displayLocalizedTextAndNavBarSetup;

- (void)loadData;

- (void)createListItemWithRecordId:(NSString*)myRecordId andItemName:(NSString*)myItemName andItemImageFeedId:(NSString*)myItemFeedId andObjectType:(int)myObjectType;
- (void)createListItemWithRecordId:(NSString*)myRecordId andItemName:(NSString*)myItemName andItemImageFileName:(NSString*)myItemImageFileName andObjectType:(int)myObjectType;

@property (nonatomic, retain) NSMutableArray *arrListItem;
@property (nonatomic, retain) NSMutableArray *arrTileView;
@property (nonatomic, retain) NSMutableString *navigationBarTitle;
@property (nonatomic, retain) NSMutableString *recordId;
@property (nonatomic, retain) NSMutableString *titleRecordId;
@property (nonatomic, retain) NSMutableArray *displayObjs;

@end
