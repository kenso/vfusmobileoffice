//
//  ProductCatalogRightOverlayView.m
//  PernodRicardMobileApp
//
//  Created by fai on 4/3/13.
//  Copyright (c) 2013 Laputa. All rights reserved.
//

#import "ProductCatalogRightOverlayView.h"

@implementation ProductCatalogRightOverlayView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:[UIColor clearColor]];
        
        paddingLeft=10;
        float paddingTop=10;
        contentStartY=70;
        
        scrollViewWidth=self.frame.size.width - (paddingLeft*1);
        scrollViewHeight=self.frame.size.height - (paddingTop*2);
        
        maxHeightForContentWithoutScrolling = scrollViewHeight - contentStartY;
        
        
        scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(paddingLeft, paddingTop, scrollViewWidth, scrollViewHeight)];
        scrollView.contentSize = CGSizeMake(scrollViewWidth, scrollViewHeight);
        //scrollView.contentSize = CGSizeMake(460, 900);
        scrollView.maximumZoomScale = 1.0;
        scrollView.minimumZoomScale = 1.0;
        scrollView.showsHorizontalScrollIndicator=NO;
        scrollView.showsVerticalScrollIndicator=YES;
        scrollView.clipsToBounds = YES;
        [scrollView setBackgroundColor:[UIColor clearColor]];
        //[scrollView setPagingEnabled:YES];
        //[scrollView setDelegate:self];
        [self addSubview:scrollView];
        
        
        labelHeading = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width - (paddingLeft*2), 40)];
        [scrollView addSubview:labelHeading];
        [labelHeading setFont:[UIFont fontWithName:@"Calibri" size:21.0f]];
        [labelHeading setTextColor:[UIColor whiteColor]];
        [labelHeading setBackgroundColor:[UIColor clearColor]];
        
        fieldNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, contentStartY, self.frame.size.width - (paddingLeft*2), 100)];
        [scrollView addSubview:fieldNameLabel];
        [fieldNameLabel setFont:[UIFont fontWithName:@"Calibri" size:21.0f]];
        [fieldNameLabel setTextColor:[UIColor whiteColor]];
        [fieldNameLabel setBackgroundColor:[UIColor clearColor]];
        fieldNameLabel.numberOfLines = 0;
        [fieldNameLabel sizeToFit];
        [fieldNameLabel release];
        
        
    }
    return self;
}



/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */


- (void)addDescription:(NSString*)myDescription andHeading:(NSString*)myHeading{
    [labelHeading setText:myHeading];
    
    
    CGRect rect = fieldNameLabel.frame;
    rect.size.width = self.frame.size.width - (paddingLeft*2);
    [fieldNameLabel setFrame:rect];
    
    [fieldNameLabel setText:myDescription];
    fieldNameLabel.numberOfLines = 0;
    [fieldNameLabel sizeToFit];
    
    float rowHeight = fieldNameLabel.frame.size.height;
    
    if(rowHeight>maxHeightForContentWithoutScrolling){
        [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, contentStartY+rowHeight)];
    }
}

- (void)addArrayOfItems:(NSArray*)myArray andHeading:(NSString*)myHeading{
    [labelHeading setText:myHeading];
    
    
    NSMutableString *myStr = [[NSMutableString alloc] initWithString:@""];
    for(int n=0; n<[myArray count]; n++){
        [myStr appendString:[myArray objectAtIndex:n]];
        [myStr appendString:@"\n"];
    }
    
    
    [fieldNameLabel setText:myStr];
    fieldNameLabel.numberOfLines = 0;
    [fieldNameLabel sizeToFit];
    [myStr release];
}

@end