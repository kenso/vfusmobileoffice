//
//  SubbrandDetailsMainVisualView.h
//  PernodRicardMobileApp
//
//  Created by fai on 4/3/13.
//  Copyright (c) 2013 Laputa. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol subbrandDetailsMainVisualViewDelegateFunctions <NSObject>
- (void)playVideo:(NSString*)videoFileName;
@end

@interface SubbrandDetailsMainVisualView : UIView<UIScrollViewDelegate>{
    id <subbrandDetailsMainVisualViewDelegateFunctions> delegate;
    
    //UIImageView *imgView;
    UIColor *imgViewColor;
    UIColor *bgColor;
    UIColor *borderColor;
    float borderWidth;
    float padding;
    float imgViewWidth;
    float imgViewHeight;
    NSMutableArray *arrSlide;
    UIScrollView* scrollView;
    UIPageControl* pageControl;
    int numSlide;
    
    NSMutableArray *arrVideoFileUrl;
}

- (void)addImageView:(UIImageView*)imgView;
- (void)addImage:(UIImage*)img;
//- (void)addVideoThumbnail:(NSURL*)videoURL;
- (void)addVideoUrl:(NSURL*)mp4FileUrl andPreviewImageUrl:(NSURL*)previewImageUrl;
- (void)addPDF:(NSURL*)pdfFileURL;
- (void)scrollViewDidScroll:(UIScrollView *)sender;
- (void)btnAction:(id)sender;

@property (nonatomic, retain) NSMutableArray *arrVideoFileUrl;

@property (retain) id delegate;


@end
