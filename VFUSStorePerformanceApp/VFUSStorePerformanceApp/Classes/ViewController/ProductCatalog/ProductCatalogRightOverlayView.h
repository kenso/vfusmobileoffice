//
//  ProductCatalogRightOverlayView.h
//  PernodRicardMobileApp
//
//  Created by fai on 4/3/13.
//  Copyright (c) 2013 Laputa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductCatalogRightOverlayView : UIView{
    UILabel *labelHeading;
    UILabel *fieldNameLabel;
    float paddingLeft;
    UIScrollView *scrollView;
    float scrollViewWidth;
    float scrollViewHeight;
    float maxHeightForContentWithoutScrolling;
    float contentStartY;
}

- (void)addDescription:(NSString*)myDescription andHeading:(NSString*)myHeading;
- (void)addArrayOfItems:(NSArray*)myArray andHeading:(NSString*)myHeading;




@end

