//
//  SubbrandDetailsViewController.m
//  PernodRicardMobileApp
//
//  Created by fai on 3/3/13.
//  Copyright (c) 2013 Laputa. All rights reserved.
//

#import "SubbrandDetailsViewController.h"
#import "CommonUtils.h"
#import <CustomSalesforceSDK/SFSyncManager.h>

#import <MediaPlayer/MediaPlayer.h>

#import "SharedMedia.h"
#import "Product.h"

@interface SubbrandDetailsViewController ()

@end

@implementation SubbrandDetailsViewController

@synthesize navigationBarTitle;
@synthesize recordId;
@synthesize titleImageView;
@synthesize subTitleLabel;
@synthesize titleRecordId;
@synthesize ProInfoView;
@synthesize ItemGroupsView;
@synthesize DescView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andProduct:(Product*)myProduct
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        navigationBarTitle = [[NSMutableString alloc] initWithString:@"Demo Product"];
        recordId = [[NSMutableString alloc] initWithString:@"121"];
        titleRecordId = [[NSMutableString alloc] initWithString:@""];
        self.mySubBrand = nil;
        self.mProduct= myProduct;
        currentView = nil;
        
        isOpenDescriptionOverlay = NO;
        isOpenItemGroupListOverlay = NO;
        
        rightOverlayWidth = 315;
        rightOverlayHeight = 605;
        rightOverlayShowX = 1024 - rightOverlayWidth;
        rightOverlayHideX = 1024;
        rightOverlayY = 75-64;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andTitle:(NSString*)myTitle andRecordId:(NSString*)myRecordId
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        navigationBarTitle = [[NSMutableString alloc] initWithString:myTitle];
        recordId = [[NSMutableString alloc] initWithString:myRecordId];
        titleRecordId = [[NSMutableString alloc] initWithString:@""];
        self.mySubBrand = nil;
        currentView = nil;
        
        isOpenDescriptionOverlay = NO;
        isOpenItemGroupListOverlay = NO;
        
        rightOverlayWidth = 315;
        rightOverlayHeight = 605;
        rightOverlayShowX = 1024 - rightOverlayWidth;
        rightOverlayHideX = 1024;
        rightOverlayY = 75-64;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    [self displayLocalizedTextAndNavBarSetup];

    UIView* view = [[UIView alloc] initWithFrame: CGRectMake(25, 125, 700, 500)];
    [view setBackgroundColor:[UIColor whiteColor]];
    [view setAlpha:0.2f];
    [view setOpaque:YES];
    
    mainVisualView = [[SubbrandDetailsMainVisualView alloc] initWithFrame:CGRectMake(25+10, 125+10, 670, 465)];
    [mainVisualView setDelegate:self];
    
    [[self view] addSubview:view];
    [[self view] addSubview:mainVisualView];
    
//    [self createButtonWithText:@"Item Groups" andFrame:CGRectMake(681, 697-64, 149, 43) andTag:0];
//    [self createButtonWithText:@"Product Info" andFrame:CGRectMake(848, 697-64, 149, 43) andTag:1];

    descriptionOverlay = [[ProductCatalogRightOverlayView alloc] initWithFrame:CGRectMake(0, 10, 275, 400)];
    itemGroupListOverlay = [[ProductCatalogRightOverlayView alloc] initWithFrame:CGRectMake(0, 10, 275, 400)];
    
    [ItemGroupsView addSubview:itemGroupListOverlay];
    [ProInfoView addSubview:descriptionOverlay];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(backButtonClicked:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setImage:[UIImage imageNamed:@"btnBack.png"] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"btnBack-On.png"] forState:UIControlStateHighlighted];
    button.frame = CGRectMake(25.0, 25.0, 75.0, 75.0);
    [[self view] addSubview:button];
    
//    [self setTitleImage];
    [self displayContent];
//    [self loadData];
}

- (IBAction)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    NSLog(@"Click");
}

- (void)setTitleImage
{
    NSInteger useIndex = 0;
    NSArray *item1 = [NSArray arrayWithObjects:@"C_title-lnter.png", [NSNumber numberWithFloat:496.0], [NSNumber numberWithFloat:35], nil];
    NSArray *item2 = [NSArray arrayWithObjects:@"C_title-local.png", [NSNumber numberWithFloat:314.0], [NSNumber numberWithFloat:35], nil];
    NSArray *item3 = [NSArray arrayWithObjects:@"C_title-modern.png", [NSNumber numberWithFloat:341.0], [NSNumber numberWithFloat:35], nil];
    NSArray *itemList = [NSArray arrayWithObjects: item1, item2, item3, nil];

    if ( [titleRecordId isEqualToString:@"100"] )
        useIndex = 0;
    else if ( [titleRecordId isEqualToString:@"200"] )
        useIndex = 1;
    else if ( [titleRecordId isEqualToString:@"300"] )
        useIndex = 2;
    
    NSArray *useItem = [itemList objectAtIndex:useIndex];
    
    float width = [[useItem objectAtIndex:1]floatValue];
    float height = [[useItem objectAtIndex:2]floatValue];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    screenRect.origin.x = 1024 - 50 - width;
    screenRect.origin.y = 34;
    screenRect.size.width = width;
    screenRect.size.height = height;
    
    UIImageView* view = [[UIImageView alloc] initWithFrame: screenRect];
//    [view setImage:[UIImage imageNamed:[useItem objectAtIndex:0]]];
    [[self view] addSubview:view];
    [view release];
    screenRect = subTitleLabel.frame;
    screenRect.origin.y = 70;
    subTitleLabel.frame = screenRect;
    subTitleLabel.text = @"ERA PRO (BLACK)";//navigationBarTitle;
}

- (void)loadData{
    /*
    SFSyncRequest *req = [[SFSyncManager sharedInstance]requestForQueryWithSFObjectClass:[SubBrand class] SFID:self.recordId];
    [req includeChildName:SFOBJ_SUB_BRAND_CHILD_DOC ofObjectClass:[SubBrand class]];
    [req includeChildName:SFOBJ_SUB_BRAND_DOC_CHILD_FEED ofObjectClass:[SharedMedia class]];
    [req includeChildName:SFOBJ_SUB_BRAND_CHILD_ITEM_GROUP ofObjectClass:[SubBrand class]];
    [[SFSyncManager sharedInstance] syncDataWithSyncRequest:req delegate:self];
     */
}

- (void)displayContent
{
        NSLog(@"displayContent start 1");
//    if (self.mProduct) {
        // Description
        NSLog(@"displayContent start 2");
        
        NSString *description = @"The Black Label Collection Era Pro has a co-molded polyurethane footbed with gel impact heels. It has a combination insole board that flexes at the ball of your foot and signature Waffle Outsole. - See more at: http://shop.vans.com/catalog/Vans/en_US/style/vfb9sx.html#sthash.eDu7tdal.dpuf";//self.mProduct.description;
        if (description == nil) description = @"";
        [descriptionOverlay addDescription:description andHeading:@"Description"];
        
        // Item groups
        /*
        Product *myProduct = [self.mySubBrand valueForFieldName:SFOBJ_SUB_BRAND_CHILD_ITEM_GROUP];
        NSMutableString *itemGroupListStr = [NSMutableString stringWithString:@""];
        if (itemGroups && [itemGroups count] > 0) {
            for (ItemGroup *ig in itemGroups) {
                [itemGroupListStr appendString:[ig valueForFieldName:SFOBJ_ITEM_GROUP_FN_NAME]];
                [itemGroupListStr appendString:@"\n\n"];
            }
        }
        [itemGroupListOverlay addDescription:itemGroupListStr andHeading:@"Item Groups"];
        [itemGroupListOverlay setNeedsDisplay];
        */
        NSMutableString *itemGroupListStr = [NSMutableString stringWithString:@""];
        [itemGroupListStr appendString:@"中山公園龍之夢               300"];
        [itemGroupListStr appendString:@"\n\n"];
    [itemGroupListStr appendString:@"九海百盛廣場                  50"];
    [itemGroupListStr appendString:@"\n\n"];
    [itemGroupListStr appendString:@"大悅城                            120"];
    [itemGroupListStr appendString:@"\n\n"];
    [itemGroupListStr appendString:@"百聯又一城                     400"];
    [itemGroupListStr appendString:@"\n\n"];
    [itemGroupListStr appendString:@"中環百聯                        70"];
    [itemGroupListStr appendString:@"\n\n"];
    [itemGroupListStr appendString:@"第一八佰伴                     100"];
    [itemGroupListStr appendString:@"\n\n"];

    [itemGroupListOverlay addDescription:itemGroupListStr andHeading:@"China - Shanghai"];
    [itemGroupListOverlay setNeedsDisplay];
    
        // Media files

        UIImageView  *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 240, 320)];
        [imageView1 setImage:[UIImage imageNamed:@"1-1-1.jpg"]];
        [mainVisualView addImageView:imageView1];
        UIImageView  *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 240, 320)];
        [imageView2 setImage:[UIImage imageNamed:@"1-1-2.jpg"]];
        [mainVisualView addImageView:imageView2];
        UIImageView  *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 240, 320)];
        [imageView3 setImage:[UIImage imageNamed:@"1-1-3.jpg"]];
        [mainVisualView addImageView:imageView3];

    /*
        NSArray *videos = [self.mySubBrand childDocFileUrlsWhereTypeField:SFOBJ_SUB_BRAND_DOC_FN_CATEGORY equalToString:SFOBJ_SUB_BRAND_DOC_PVAL_CATEGORY_VIDEO];
        NSURL *previewImageURL = [firstImages count] > 0 ? [firstImages objectAtIndex:0] : nil;
        for (NSURL *videoURL in videos) {
            [mainVisualView addVideoUrl:videoURL andPreviewImageUrl:previewImageURL];
        }
     */
    
        NSString *videoPath = [[NSBundle mainBundle] pathForResource:@"Vans2013" ofType:@"mp4"];
        NSURL *videoURL = [[NSURL alloc] initFileURLWithPath:videoPath];
        NSString *previewPath = [[NSBundle mainBundle] pathForResource:@"Vans2013_Preview" ofType:@"png"];
        NSURL *previewURL = [[NSURL alloc] initFileURLWithPath:previewPath];
            [mainVisualView addVideoUrl:videoURL andPreviewImageUrl:previewURL];

    
        /*
        if ( self.mProduct.docs && [self.mProduct.docs count] > 0 ){
            for ( SharedMedia *doc in self.mProduct.docs){
                [mainVisualView addImageView: [[CommonUtils shareInstance] getSFDocImageWithFeedId:doc.feedItemId andFrame:CGRectMake(0,0,240,320)]];                
            }
        }*/
        /*
        // First image
        NSArray *firstImages = [self.mySubBrand childDocFileUrlsWhereTypeField:SFOBJ_SUB_BRAND_DOC_FN_CATEGORY equalToString:SFOBJ_SUB_BRAND_DOC_PVAL_CATEGORY_IMAGE_FIRST];
        for (NSURL *imageURL in firstImages) {
            [mainVisualView addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:imageURL]]];
        }
        // Other images
        NSArray *otherImages = [self.mySubBrand childDocFileUrlsWhereTypeField:SFOBJ_SUB_BRAND_DOC_FN_CATEGORY equalToString:SFOBJ_SUB_BRAND_DOC_PVAL_CATEGORY_IMAGE];
        for (NSURL *imageURL in otherImages) {
            [mainVisualView addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:imageURL]]];
        }
        // Videos
        NSArray *videos = [self.mySubBrand childDocFileUrlsWhereTypeField:SFOBJ_SUB_BRAND_DOC_FN_CATEGORY equalToString:SFOBJ_SUB_BRAND_DOC_PVAL_CATEGORY_VIDEO];
        NSURL *previewImageURL = [firstImages count] > 0 ? [firstImages objectAtIndex:0] : nil;
        for (NSURL *videoURL in videos) {
            [mainVisualView addVideoUrl:videoURL andPreviewImageUrl:previewImageURL];
        }
        // PDFs
        NSArray *pdfs = [self.mySubBrand childDocFileUrlsWhereTypeField:SFOBJ_SUB_BRAND_DOC_FN_CATEGORY equalToString:SFOBJ_SUB_BRAND_DOC_PVAL_CATEGORY_DOC];
        for (NSURL *pdfURL in pdfs) {
            [mainVisualView addPDF:pdfURL];
        }
         */
        [mainVisualView setNeedsDisplay];

//    }
}

- (void)syncResult:(id)syncResult syncRequest:(SFSyncRequest*)syncRequest
{
    if (syncResult && [syncResult count] > 0) {
        self.mySubBrand = [syncResult objectAtIndex:0];
        [self displayContent];
    } else {
        self.mySubBrand = nil;
    }
}

- (void)syncFailLoadRequest:(SFSyncRequest*)syncRequest error:(NSError*)error
{
    NSLog(@"TODO: syncFailLoadRequest");
}

- (void)syncCancelLoadRequest:(SFSyncRequest*)syncRequest
{
    NSLog(@"TODO: syncCancelLoadRequest");
}

- (void)syncTimeoutRequest:(SFSyncRequest*)syncRequest
{
    NSLog(@"TODO: syncTimeoutRequest");
}

- (void)displayLocalizedTextAndNavBarSetup{
    /////Navigation Bar Setup with localized text
    self.title = [NSString stringWithFormat:@"%@", navigationBarTitle];
    
    //self.title = [NSString stringWithFormat:@"%@", [CommonUtils getLocalizedStringForKey:@"Product Categories"]];
    //self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:[CommonUtils getLocalizedStringForKey:@"Show Location on Map"] style:UIBarButtonItemStyleBordered target:self action:@selector(btnShowMapHandler:)        ] autorelease];
    //self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:[CommonUtils getLocalizedStringForKey:@"Settings"] style:UIBarButtonItemStyleBordered target:self action:@selector(btnSettingsHandler:)        ] autorelease];
    //[self.navigationItem setHidesBackButton:YES];
    //[[self navigationController] setNavigationBarHidden:NO animated:NO];
    
}
/*
- (void)createButtonWithText:(NSString*)myText andFrame:(CGRect)myRect andTag:(int)myTag{
    
    UIButton *myBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [myBtn setFrame:myRect];
    [myBtn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    //[myBtn addTarget:self action:@selector(btnSelectAction:) forControlEvents:UIControlEventTouchUpInside];
    
    //[myButton setHighlighted:NO];
    UIImage *newImage = [[UIImage imageNamed:@"btn_bg.png"] stretchableImageWithLeftCapWidth:8.0 topCapHeight:8.0];
    [myBtn setBackgroundImage:newImage forState:UIControlStateNormal];
    
    [myBtn setTitle:myText forState:UIControlStateNormal];
    
    [myBtn.titleLabel setFont:[UIFont fontWithName:@"Arial" size:14.0f]];
    [myBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [myBtn setTitle:myText forState:UIControlStateNormal];
    
    [myBtn setTag:myTag];
    
    [[self view] addSubview:myBtn];
}
*/
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	// Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    if(interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        return YES;
    }
    else if(interfaceOrientation == UIInterfaceOrientationLandscapeLeft) {
        return YES;
    }
    else {
        return NO;
    }
}

///Play video button delegate function
- (IBAction)onClickCheckIn:(id)sender {
    UIAlertView *messageAlert = [[UIAlertView alloc]
                                 initWithTitle:@"Check-In Success" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [messageAlert show];
    [messageAlert release];
}

- (void)playVideo:(NSURL*)videoFileURL{
    MPMoviePlayerController *moviePlayer =
    [[MPMoviePlayerController alloc] initWithContentURL:videoFileURL];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:moviePlayer];
    
    moviePlayer.controlStyle = MPMovieControlStyleDefault;
    moviePlayer.shouldAutoplay = YES;
    [self.view addSubview:moviePlayer.view];
    [moviePlayer setFullscreen:YES animated:YES];
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    
    MPMoviePlayerController *moviePlayer = [notification object];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:moviePlayer];
    
    if ([moviePlayer
         respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [moviePlayer.view removeFromSuperview];
    }
    [moviePlayer release];
}

- (void)animationFinished:(NSString *)animationID finished:(BOOL)finished context:(void *)context
{
}

- (IBAction)productInformationClicked:(id)sender {
    CGRect rect = [DescView frame];
    [UIView beginAnimations:@"productInformationClicked" context:nil];
    [UIView setAnimationDuration:0.3f];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationBeginsFromCurrentState:NO];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(animationFinished:finished:context:)];
    
    rect.origin.y=0;
    rect.origin.x=0;
    [DescView setFrame:rect];
    
    [UIView commitAnimations];

}

- (IBAction)itemGroups:(id)sender {
    CGRect rect = [DescView frame];
    [UIView beginAnimations:@"itemGroups" context:NULL];
    [UIView setAnimationDuration:0.3f];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationBeginsFromCurrentState:NO];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(animationFinished:finished:context:)];

    rect.origin.y=-400;
    rect.origin.x=0;
    [DescView setFrame:rect];

    [UIView commitAnimations];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [[SFSyncManager sharedInstance] cancelSyncWithDelegate:self];
    [_mySubBrand release];
    [mainViewWrapper release];
    [titleImageView release];
    [subTitleLabel release];
    [ProInfoView release];
    [ItemGroupsView release];
    [DescView release];
    [super dealloc];
}

- (void)viewDidUnload {
    self.mySubBrand = nil;
    [mainViewWrapper release];
    mainViewWrapper = nil;
    [self setTitleImageView:nil];
    [self setSubTitleLabel:nil];
    [self setProInfoView:nil];
    [self setItemGroupsView:nil];
    [self setDescView:nil];
    [super viewDidUnload];
}
@end
