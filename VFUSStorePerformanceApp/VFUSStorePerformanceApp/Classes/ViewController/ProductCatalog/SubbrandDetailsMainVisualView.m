//
//  SubbrandDetailsMainVisualView.m
//  PernodRicardMobileApp
//
//  Created by fai on 4/3/13.
//  Copyright (c) 2013 Laputa. All rights reserved.
//

#import "SubbrandDetailsMainVisualView.h"

//#import <MediaPlayer/MediaPlayer.h>

@implementation SubbrandDetailsMainVisualView

@synthesize delegate;

@synthesize arrVideoFileUrl;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        arrVideoFileUrl = [[NSMutableArray alloc] init];
        
        
        borderWidth=1;
        padding=borderWidth;

        imgViewColor = [UIColor clearColor];
        
        bgColor = [UIColor clearColor];
        
        borderColor = [UIColor lightGrayColor];
        //borderColor = [UIColor colorWithRed:0.0627f green:0.263f blue:0.478f alpha:1.0f];
        
        [self setBackgroundColor:bgColor];
        
        
        numSlide=0;
        
        
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(borderWidth, borderWidth, frame.size.width-(borderWidth*2), frame.size.height-(borderWidth*2))];
        [bgView setBackgroundColor:bgColor];
        [self addSubview:bgView];
        [bgView release];
        
        
        
        
        imgViewWidth = frame.size.width-(padding*2);
        imgViewHeight = frame.size.height-(padding*2);
        
        
        
        /////////Scroll View Setup
        arrSlide = [[NSMutableArray alloc] init];
        
        /////Set up ScrollView
        float slideWidth=frame.size.width-(padding*2);
        float slideHeight=frame.size.height-(padding*2);
        scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(padding, padding, slideWidth, slideHeight)];
    
        scrollView.contentSize = CGSizeMake(slideWidth*numSlide, slideHeight);
        //scrollView.contentSize = CGSizeMake(460, 900);
        scrollView.maximumZoomScale = 1.0;
        scrollView.minimumZoomScale = 1.0;
        scrollView.showsHorizontalScrollIndicator=NO;
        scrollView.showsVerticalScrollIndicator=NO;
        scrollView.clipsToBounds = YES;
        [scrollView setBackgroundColor:imgViewColor];
        [scrollView setPagingEnabled:YES];
        [scrollView setDelegate:self];
        
        
        [self addSubview:scrollView];
        
        
        
        
        
        // Init Page Control
        
        pageControl = [[UIPageControl alloc] init];
        //pageControl.frame = CGRectMake(0, 0, 490, 40);
        pageControl.frame = CGRectMake(borderWidth, self.frame.size.height+8, slideWidth, 12);
        pageControl.numberOfPages = numSlide;
        pageControl.currentPage = 0;
        //[pageControl setBackgroundColor:[UIColor blackColor]];
        //[pageControl setBackgroundColor:[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.05f]];
        //[pageControl setBackgroundColor:[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.09f]];
        //[pageControl setBackgroundColor:[UIColor lightGrayColor]];
        //*//[pageControl setBackgroundColor:[UIColor colorWithRed:0.0627f green:0.263f blue:0.478f alpha:0.6f]];
        //[pageControl setBackgroundColor:borderColor];
        //[pageControl setBackgroundColor:[UIColor colorWithRed:0.267f green:0.267f blue:0.267f alpha:0.3f]];
        //[pageControl setPageIndicatorTintColor:[UIColor lightGrayColor]];
        //self.pageControl.pageIndicatorTintColor = [UIColor blueColor];
        //[self.pageControl setCurrentPageIndicatorTintColor:[UIColor colorWithRed:0 green:175.0/255.0 blue:176.0/255.0 alpha:1.0]];
        //[self.pageControl setPageIndicatorTintColor:[UIColor colorWithRed:0.996 green:0.788 blue:0.180 alpha:1.000]];
        
        [pageControl setHidesForSinglePage:YES];
        
        
        [self addSubview:pageControl];
        
        
    }
    return self;
}

- (void)addImageView:(UIImageView*)imgView{
    float myX = numSlide*imgViewWidth;
    float myY = 0;
    
    [imgView setFrame:CGRectMake(myX, myY, imgViewWidth, imgViewHeight)];
    [imgView setContentMode:UIViewContentModeScaleAspectFit];
    
    [scrollView addSubview:imgView];
    //[scrollView setBackgroundColor:[UIColor greenColor]];
    
    numSlide++;
    
    [pageControl setNumberOfPages:numSlide];
    scrollView.contentSize = CGSizeMake(imgViewWidth*numSlide, imgViewHeight);
}

- (void)addImage:(UIImage*)img{
    float myX = numSlide*imgViewWidth;
    float myY = 0;
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(myX, myY, imgViewWidth, imgViewHeight)];
    //[imgView setBackgroundColor:imgViewColor];
    //[imgView setBackgroundColor:[UIColor blueColor]];
    [imgView setContentMode:UIViewContentModeScaleAspectFit];
    [imgView setImage:img];
    
    [scrollView addSubview:imgView];
    //[scrollView setBackgroundColor:[UIColor greenColor]];
    
    numSlide++;
    
    [pageControl setNumberOfPages:numSlide];
    scrollView.contentSize = CGSizeMake(imgViewWidth*numSlide, imgViewHeight);
}

- (void)addPDF:(NSURL*)pdfFileURL{
    float myX = numSlide*imgViewWidth;
    float myY = 0;
    
    UIWebView *_webView = [[UIWebView alloc] initWithFrame:CGRectMake(myX, myY, imgViewWidth, imgViewHeight)];
    [_webView loadRequest:[NSURLRequest requestWithURL:pdfFileURL]];
    
    [scrollView addSubview:_webView];
    //[scrollView setBackgroundColor:[UIColor greenColor]];
    
    numSlide++;
    
    [pageControl setNumberOfPages:numSlide];
    scrollView.contentSize = CGSizeMake(imgViewWidth*numSlide, imgViewHeight);
}

/*
- (void)addVideoThumbnail:(NSURL*)videoURL{
    MPMoviePlayerController *player = [[[MPMoviePlayerController alloc] initWithContentURL:videoURL]autorelease];
    UIImage  *thumbnail = [player thumbnailImageAtTime:1.0 timeOption:MPMovieTimeOptionNearestKeyFrame];
    player = nil;
    
    
    
    float myX = numSlide*imgViewWidth;
    float myY = 0;
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(myX, myY, imgViewWidth, imgViewHeight)];
    //[imgView setBackgroundColor:imgViewColor];
    //[imgView setBackgroundColor:[UIColor blueColor]];
    [imgView setContentMode:UIViewContentModeScaleAspectFit];
    [imgView setImage:thumbnail];
    
    [scrollView addSubview:imgView];
    //[scrollView setBackgroundColor:[UIColor greenColor]];
    
    numSlide++;
    
    [pageControl setNumberOfPages:numSlide];
    scrollView.contentSize = CGSizeMake(imgViewWidth*numSlide, imgViewHeight);
}
*/


- (void)addVideoUrl:(NSURL*)mp4FileUrl andPreviewImageUrl:(NSURL*)previewImageUrl
{
    /*
    NSURL *videoURL = [NSURL fileURLWithPath:[[NSBundle mainBundle]
                                         pathForResource:mp4FileName ofType:@"mp4"]];
    
    
    MPMoviePlayerController *player = [[[MPMoviePlayerController alloc] initWithContentURL:videoURL]autorelease];
    UIImage  *thumbnail = [player thumbnailImageAtTime:1.0 timeOption:MPMovieTimeOptionNearestKeyFrame];
    player = nil;
    */
    
    
    float myX = numSlide*imgViewWidth;
    float myY = 0;
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(myX, myY, imgViewWidth, imgViewHeight)];
    //[imgView setBackgroundColor:imgViewColor];
    //[imgView setBackgroundColor:[UIColor blueColor]];
    [imgView setContentMode:UIViewContentModeScaleAspectFit];
    UIImage *previewImage = previewImageUrl ? [UIImage imageWithData:[NSData dataWithContentsOfURL:previewImageUrl]] : [UIImage imageNamed:@"slideshow_video_thumbnail_bg.png"];
    [imgView setImage:previewImage];
    
    [scrollView addSubview:imgView];
    //[scrollView setBackgroundColor:[UIColor greenColor]];
    
    
    
    ////Play Video Button
    [arrVideoFileUrl addObject:mp4FileUrl];
    
    int myTag = [arrVideoFileUrl count] -1;
    
    UIButton *myBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [myBtn setFrame:CGRectMake(myX+278,myY+184,200,200)];
    [myBtn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    //[myBtn addTarget:self action:@selector(btnSelectAction:) forControlEvents:UIControlEventTouchUpInside];
    
    //[myButton setHighlighted:NO];
    [myBtn setBackgroundImage:[UIImage imageNamed:@"slideshow_video_thumbnail_button.png"] forState:UIControlStateNormal];   
    
    [myBtn setTag:myTag];
    [scrollView addSubview:myBtn];
    numSlide++;
    
    [pageControl setNumberOfPages:numSlide];
    scrollView.contentSize = CGSizeMake(imgViewWidth*numSlide, imgViewHeight);
}



- (void)btnAction:(id)sender{
    [delegate playVideo:[arrVideoFileUrl objectAtIndex:[sender tag]]];
}



- (void)scrollViewDidScroll:(UIScrollView *)sender {
    // Update the page when more than 50% of the previous/next page is visible
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    pageControl.currentPage = page;
}




/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end

