//
//  ProductCatalogTileView.h
//  PernodRicardMobileApp
//
//  Created by fai on 1/3/13.
//  Copyright (c) 2013 Laputa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductCatalogListItem.h"
#import "SFImageView.h"

@protocol productCatalogTileViewDelegateFunctions <NSObject>
- (void)tileTouchAction:(int)myTag;
@end

@interface ProductCatalogTileView : UIView{
    id <productCatalogTileViewDelegateFunctions> delegate;
    float imgX;
    float imgY;
    float imgWidth;
    float imgHeight;
    UIButton *btn;
}

- (id)initWithFrame:(CGRect)frame andProductCatalogListItem:(ProductCatalogListItem*)myListItem andTag:(int)myTag;

- (void)btnTouchAction:(id)sender;

@property (retain) id btn;
@property (retain) id delegate;

@end
