//
//  BasicViewController.h
//
//  Created by Developer 2 on 4/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentSidebarView.h"

@interface BasicViewController : UIViewController


@property (retain) CommentSidebarView* commentSidebar;
@property (retain) UIView* lodingView;

-(void) changeLoadingPopupText:(NSString*)text;

-(void) showLoadingPopup:(UIView*)parent;
-(void) showLoadingPopup:(UIView*)parent text:(NSString*)text;


-(void) hideLoadingPopup;

-(void) hideLoadingPopupWithFadeout;

-(UIButton*) createNewButtonWithDefaultStyle:(CGRect)rect parent:(UIView*)parent tag:(NSInteger) tag;

-(UIButton*) createNewButton:(CGRect)rect parent:(UIView*)parent tag:(NSInteger) tag color:(int)colorIndex;

-(UITextField*) createNewTextFieldWithDefaultStyle:(CGRect)rect parent:(UIView*)parent tag:(NSInteger) tag;
-(UITextField*) createNewTextFieldWithDefaultStyle:(CGRect)rect parent:(UIView*)parent tag:(NSInteger) tag alignment:(NSTextAlignment)alignment;
-(UITextField*) createNewTextFieldWithDefaultStyle:(CGRect)rect parent:(UIView*)parent tag:(NSInteger) tag alignment:(NSTextAlignment)alignment vAlignment:(UIControlContentVerticalAlignment)vAlignment;
-(UILabel*) createNewTextLabelWithDefaultStyle:(CGRect)rect parent:(UIView*)parent tag:(NSInteger) tag;
-(UILabel*) createNewTextLabelWithDefaultStyle:(CGRect)rect parent:(UIView*)parent tag:(NSInteger)tag alignment:(NSTextAlignment)alignment;
-(NSString*) getFieldDisplayWithFieldName:(NSString*)fieldName withDictionary:(NSDictionary*) data;

-(void) popupErrorDialogWithMessage:(NSString*)msg;
- (NSNumberFormatter*)getSimplePriceFormatter;
- (NSNumberFormatter*)getFullPriceFormatter;
- (NSString*)getSimplePriceStringWithFloat:(float)price;
- (NSString*)getFullPriceStringWithFloat:(float)price;
- (NSString*)getAllPriceStringWithFloat:(float)price;
-(void) refreshBackground;

-(BOOL)handleQueryDataErrorCode:(int)code description:(NSString*)message;

-(void) initCommentSidebarWithStoreId:(NSString*)storeId;


@end
