//
//  PDFViewController.h
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 15/6/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicViewController.h"
#import "ImageLoader.h"

@interface PDFViewController : BasicViewController<ImageLoaderDelegate>

@property (retain, nonatomic) IBOutlet UILabel *headerTitleLabel;

@property (retain, nonatomic) IBOutlet UIWebView *webview;

@property (nonatomic, retain) NSString* feedItemID;

-(id) initWithFeedItemID:(NSString*)feedItemID;

- (IBAction)onHomeButtonClicked:(id)sender;

- (IBAction)onBackButtonClicked:(id)sender;

@end
