//
//  StoreData.m
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 6/11/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "StoreData.h"


@implementation StoreDataStruct
-(id)init{
    if (self =[super init]){
        self.wtd = @0;
        self.mtd = @0;
        self.ytd = @0;
        self.lywtd = @0;
        self.lymtd = @0;
        self.lyytd = @0;
    }
    return self;
}
@end



@implementation StoreData

-(id)init{
    if (self =[super init]){
        self.opsSales = [[StoreDataStruct alloc] init];
        self.discount = [[StoreDataStruct alloc] init];
        self.prod = [[StoreDataStruct alloc] init];
        self.atv = [[StoreDataStruct alloc] init];
        self.upt = [[StoreDataStruct alloc] init];
        self.txn = [[StoreDataStruct alloc] init];
        self.ttxn = [[StoreDataStruct alloc] init];
        self.visitor = [[StoreDataStruct alloc] init];
        self.tvisitor = [[StoreDataStruct alloc] init];
        self.peel = [[StoreDataStruct alloc] init];
        self.cr = [[StoreDataStruct alloc] init];
    }
    return self;
}



-(NSMutableDictionary*) dictionary{
  return [NSMutableDictionary dictionaryWithDictionary: @{
       //basic info
       @"Id": @"",
       @"Name": @"",
       @"Data_Level__c": @"DS",
       @"aeId": @"",
       @"Store_Code__c": @"",
       @"Profile__c": @"",
       @"Store_Type__c": @"",
       @"emfa__Address__c": @"",
       @"emfa__Email__c": @"",
       @"emfa__Fax__c": @"",
       @"emfa__Phone__c": @"",
       @"LastModifiedDate": @"",
       @"emfa__Primary_Latitude__c": @"",
       @"emfa__Primary_Longitude__c": @"",
       
       //dashboard data
       @"size": @"",
       @"Weekly_Cross_Sales__c": @"",
       @"Weekly_Unit_Count__c": @"",
       @"Weekly_Outside_Traffic__c": @"",
       
       @"Monthly_Cross_Sales__c": @"",
       @"Monthly_Unit_Count__c": @"",
       @"Monthly_Outside_Traffic__c": @"",
       
       @"Yearly_Cross_Sales__c": @"",
       @"Yearly_Unit_Count__c": @"",
       @"Yearly_Outside_Traffic__c": @"",
       
       //Last year weekly data
       @"Weekly_Ops_Sales_LY": self.opsSales.lywtd ,
       @"Weekly_Discount_LY": self.discount.lywtd ,
       @"Weekly_Sales_Productivity_LY": self.prod.lywtd,
       @"Weekly_ATV_LY": self.atv.lywtd,
       @"Weekly_UPT_LY":self.upt.lywtd,
       @"Weekly_Transaction_Count_LY": self.txn.lywtd,
       @"Weekly_Traffic_Transaction_Count_LY": self.ttxn.lywtd,
       @"Weekly_Visitor_Count_LY": self.visitor.lywtd,
       @"Weekly_T_Visitor_Count_LY": self.tvisitor.lywtd,
       @"Weekly_Pel_Off_LY": self.peel.lywtd,
       @"Weekly_Conversion_Rate_LY": self.cr.lywtd,
       
       //Last year monthly data
       @"Monthly_Ops_Sales_LY": self.opsSales.lymtd,
       @"Monthly_Discount_LY": self.discount.lymtd,
       @"Monthly_Sales_Productivity_LY": self.prod.lymtd,
       @"Monthly_ATV_LY": self.atv.lymtd,
       @"Monthly_UPT_LY": self.upt.lymtd,
       @"Monthly_Transaction_Count_LY": self.txn.lymtd,
       @"Monthly_Traffic_Transaction_Count_LY": self.ttxn.lymtd,
       @"Monthly_Visitor_Count_LY": self.visitor.lymtd,
       @"Monthly_T_Visitor_Count_LY": self.tvisitor.lymtd,
       @"Monthly_Pel_Off_LY": self.peel.lymtd,
       @"Monthly_Conversion_Rate_LY": self.cr.lymtd,
       
       //Last year yearly data
       @"Yearly_Ops_Sales_LY": self.opsSales.lyytd,
       @"Yearly_Discount_LY": self.discount.lyytd,
       @"Yearly_Sales_Productivity_LY": self.prod.lyytd,
       @"Yearly_ATV_LY": self.atv.lyytd,
       @"Yearly_UPT_LY": self.upt.lyytd,
       @"Yearly_Transaction_Count_LY": self.txn.lyytd,
       @"Yearly_Traffic_Transaction_Count_LY": self.ttxn.lyytd,
       @"Yearly_Visitor_Count_LY": self.visitor.lyytd,
       @"Yearly_T_Visitor_Count_LY": self.tvisitor.lyytd,
       @"Yearly_Pel_Off_LY": self.peel.lyytd,
       @"Yearly_Conversion_Rate_LY": self.cr.lyytd,
       
       //this year weekly data
       @"Weekly_Ops_Sales__c":  self.opsSales.wtd,
       @"Weekly_Discount__c":  self.discount.wtd,
       @"Weekly_Sales_Productivity__c": self.prod.wtd,
       @"Weekly_ATV__c":  self.atv.wtd,
       @"Weekly_UPT__c":  self.upt.wtd,
       @"Weekly_Transaction_Count__c": self.txn.wtd,
       @"Weekly_Traffic_Transaction_Count__c": self.ttxn.wtd,
       @"Weekly_Visitor_Count__c": self.visitor.wtd,
       @"Weekly_T_Visitor_Count__c": self.tvisitor.wtd,
       @"Weekly_Pel_Off__c": self.peel.wtd,
       @"Weekly_Conversion_Rate__c": self.cr.wtd,
       
       //this year monthly data
       @"Monthly_Ops_Sales__c": self.opsSales.mtd,
       @"Monthly_Discount__c":  self.discount.mtd,
       @"Monthly_Sales_Productivity__c": self.prod.mtd,
       @"Monthly_ATV__c": self.atv.mtd,
       @"Monthly_UPT__c": self.upt.mtd,
       @"Monthly_Transaction_Count__c": self.txn.mtd,
       @"Monthly_Traffic_Transaction_Count__c": self.ttxn.mtd,
       @"Monthly_Visitor_Count__c": self.visitor.mtd,
       @"Monthly_T_Visitor_Count__c": self.tvisitor.mtd,
       @"Monthly_Pel_Off__c": self.peel.mtd,
       @"Monthly_Conversion_Rate__c": self.cr.mtd,
       
       //this year yearly data
       @"Yearly_Ops_Sales__c": self.opsSales.ytd,
       @"Yearly_Discount__c": self.discount.ytd,
       @"Yearly_Sales_Productivity__c": self.prod.ytd,
       @"Yearly_ATV__c": self.atv.ytd,
       @"Yearly_UPT__c": self.upt.ytd,
       @"Yearly_Transaction_Count__c": self.txn.ytd,
       @"Yearly_Traffic_Transaction_Count__c": self.ttxn.ytd,
       @"Yearly_Visitor_Count__c": self.visitor.ytd,
       @"Yearly_T_Visitor_Count__c": self.tvisitor.ytd,
       @"Yearly_Pel_Off__c": self.peel.ytd,
       @"Yearly_Conversion_Rate__c": self.cr.ytd,
       
       //targets
       @"Weekly_Ops_Sales_Target__c": @0,
       @"Monthly_Ops_Sales_Target__c": @0,
       @"Yearly_Ops_Sales_Target__c": @0,
       @"Weekly_ATV_Target__c": @0,
       @"Monthly_ATV_Target__c": @0,
       @"Yearly_ATV_Target__c": @0,
       @"Weekly_UPT_Target__c": @0,
       @"Monthly_UPT_Target__c": @0,
       @"Yearly_UPT_Target__c": @0,
       @"Weekly_Conversion_Rate_Target__c": @0,
       @"Monthly_Conversion_Rate_Target__c": @0,
       @"Yearly_Conversion_Rate_Target__c": @0,
       @"Weekly_Discount_Target__c": @0,
       @"Monthly_Discount_Target__c": @0,
       @"Yearly_Discount_Target__c": @0,
       
   }] ;
}


-(void)dealloc{
    [_peel release];
    [_cr release];
    [_atv release];
    [_upt release];
    [_discount release];
    [_ttxn release];
    [_opsSales release];
    [_visitor release];
    [_tvisitor release];
    [_prod release];
    [_txn release];
    
    [super dealloc];
}

@end
