//
//  StoreData.h
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 6/11/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StoreDataStruct : NSObject

@property (nonatomic, retain) NSNumber* wtd;
@property (nonatomic, retain) NSNumber* mtd;
@property (nonatomic, retain) NSNumber* ytd;
@property (nonatomic, retain) NSNumber* lywtd;
@property (nonatomic, retain) NSNumber* lymtd;
@property (nonatomic, retain) NSNumber* lyytd;

@end

@interface StoreData : NSObject



@property (nonatomic, retain) StoreDataStruct* opsSales;
@property (nonatomic, retain) StoreDataStruct* discount;
@property (nonatomic, retain) StoreDataStruct* prod;
@property (nonatomic, retain) StoreDataStruct* atv;
@property (nonatomic, retain) StoreDataStruct* upt;
@property (nonatomic, retain) StoreDataStruct* txn;
@property (nonatomic, retain) StoreDataStruct* ttxn;//txn with traffic, not sales
@property (nonatomic, retain) StoreDataStruct* tvisitor;//visitor with traffic, not sales
@property (nonatomic, retain) StoreDataStruct* visitor;
@property (nonatomic, retain) StoreDataStruct* peel;
@property (nonatomic, retain) StoreDataStruct* cr;

-(NSMutableDictionary*) dictionary;




@end
