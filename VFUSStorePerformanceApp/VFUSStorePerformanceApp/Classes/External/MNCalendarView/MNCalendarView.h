//
//  MNCalendarView.h
//  MNCalendarView
//
//  Created by Min Kim on 7/23/13.
//  Copyright (c) 2013 min. All rights reserved.
//

#import <UIKit/UIKit.h>

#define MN_MINUTE 60.f
#define MN_HOUR   MN_MINUTE * 60.f
#define MN_DAY    MN_HOUR * 24.f
#define MN_WEEK   MN_DAY * 7.f
#define MN_YEAR   MN_DAY * 365.f

@protocol MNCalendarViewDelegate;

@interface MNCalendarView : UIView <UICollectionViewDataSource, UICollectionViewDelegate>{
    NSDate *_selectedDate;
    UIColor* _backgroundColor;
    UIColor* _cellActiveColor;
    UIColor* _cellInactiveColor;
}

@property(nonatomic, retain) UICollectionView *collectionView;

@property(nonatomic,assign) id<MNCalendarViewDelegate> delegate;

@property(nonatomic,retain) NSCalendar *calendar;
@property(nonatomic,retain)   NSDate     *fromDate;
@property(nonatomic,retain)   NSDate     *toDate;

@property(nonatomic,retain) UIColor *separatorColor UI_APPEARANCE_SELECTOR; // default is the standard separator gray

@property(nonatomic,retain) Class headerViewClass;
@property(nonatomic,retain) Class weekdayCellClass;
@property(nonatomic,retain) Class dayCellClass;

- (void)reloadData;
- (void)registerUICollectionViewClasses;
- (void)setSelectedDate:(NSDate *)selectedDate ;

- (id)initWithFrame:(CGRect)frame backgroundColor:(UIColor*)bgColor sepColor:(UIColor*)sepColor;

@end

@protocol MNCalendarViewDelegate <NSObject>

@optional

- (BOOL)calendarView:(MNCalendarView *)calendarView shouldSelectDate:(NSDate *)date;
- (void)calendarView:(MNCalendarView *)calendarView didSelectDate:(NSDate *)date;

@end
