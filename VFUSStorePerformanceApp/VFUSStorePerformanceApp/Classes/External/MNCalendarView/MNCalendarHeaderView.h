//
//  MNCalendarHeaderView.h
//  MNCalendarView
//
//  Created by Min Kim on 7/26/13.
//  Copyright (c) 2013 min. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *const MNCalendarHeaderViewIdentifier;

@interface MNCalendarHeaderView : UICollectionReusableView

@property(nonatomic, retain) UILabel *titleLabel;

@property(nonatomic, retain) UIButton *prevButton;
@property(nonatomic, retain) UIButton *nextButton;

@property(nonatomic, retain) NSDate *date;

@property(nonatomic, assign) id delegate;

@end
