//
//  Constants.h
//
//  Created by Developer 2 on 9/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import <Foundation/Foundation.h>


#define NETWORK_ERROR_CODE_1001     -1001
#define NETWORK_ERROR_CODE_1005     -1005
#define NETWORK_ERROR_CODE_1003     -1003
#define NETWORK_ERROR_CODE_1009     -1009
#define NETWORK_ERROR_CODE_1012     -1012

#define TABLE_SEPARATOR_COLOR [UIColor colorWithWhite:1 alpha:.2f]

#define TABLE_ROW_HIGHLIGHT_COLOR [UIColor colorWithWhite:1 alpha:.1f]


#define CHART_COLOR_BLUE [UIColor colorWithRed:0/255.0 green:120/255.0 blue:251/255.0 alpha:1]
#define CHART_COLOR_RED [UIColor colorWithRed:220/255.0 green:21/255.0 blue:83/255.0 alpha:1]
#define CHART_COLOR_GREEN [UIColor colorWithRed:0/255.0 green:210/255.0 blue:45/255.0 alpha:1]
#define CHART_COLOR_BORDER_GREY [UIColor colorWithRed:127/255.0 green:127/255.0 blue:127/255.0 alpha:1]


#define BUTTON_COLOR_RED [UIColor colorWithRed:22/255.0 green:87/255.0 blue:184/255.0 alpha:0.8]
#define BUTTON_COLOR_GREEN [UIColor colorWithRed:22/255.0 green:184/255.0 blue:22/255.0 alpha:0.8]
#define BUTTON_COLOR_BLUE [UIColor colorWithRed:22/255.0 green:87/255.0 blue:184/255.0 alpha:0.8]
#define BUTTON_COLOR_YELLOW [UIColor colorWithRed:184/255.0 green:87/255.0 blue:22/255.0 alpha:0.8]
#define BUTTON_COLOR_PURPLE [UIColor colorWithRed:184/255.0 green:22/255.0 blue:73/255.0 alpha:0.8]
#define BUTTON_COLOR_ORANGE [UIColor colorWithRed:22/255.0 green:87/255.0 blue:184/255.0 alpha:0.8]

#define TABLE_BG_COLOR_RED [UIColor colorWithRed:100/255.0 green:13/255.0 blue:40/255.0 alpha:0.8]
#define TABLE_BG_COLOR_GREEN [UIColor colorWithRed:13/255.0 green:100/255.0 blue:15/255.0 alpha:0.8]
#define TABLE_BG_COLOR_BLUE [UIColor colorWithRed:13/255.0 green:76/255.0 blue:100/255.0 alpha:0.8]

#define TABLE_BG_COLOR_ORAGE [UIColor colorWithRed:134/255.0 green:64/255.0 blue:17/255.0 alpha:0.8]


#define TABLE_HEADER_BG_COLOR_RED [UIColor colorWithRed:194/255.0 green:0/255.0 blue:74/255.0 alpha:1]
#define TABLE_HEADER_BG_COLOR_GREEN [UIColor colorWithRed:21/255.0 green:165/255.0 blue:24/255.0 alpha:1]
#define TABLE_HEADER_BG_COLOR_BLUE [UIColor colorWithRed:25/255.0 green:4/255.0 blue:255/255.0 alpha:1]

#define WARNING_COLOR [UIColor colorWithRed:212.0f/255 green:0 blue:0 alpha:1]

#define DEFAULT_SIMPLE_DATE_FORMAT @"yyyy-MM-dd"
#define DEFAULT_DATE_TIME_FORMAT @"yyyy-MM-dd HH:mm"

#define TEMP_IMAGE_NAME @"vf_image_temp_%i"
#define ONLINE_IMAGE_NAME @"vf_image_online_%i"

//#define RemoteAccessConsumerKey @"3MVG9Y6d_Btp4xp5ibujRLSa81SvlmP4UMkAwfiF8vzh68IvohtgsPfRlCONo_vYn9hhOK7cJ7JCQZwzoyTVg"
//#define RemoteAccessSecret @"8341342047834015041"
#define RemoteAccessConsumerKey @"3MVG9uudbyLbNPZPLW7BNRRsrPYlIoIecuGib_yAXxnL2aH9UVFp5NoGyQWJ05.Gl501Nes4g7kiz75OyMwug"
#define RemoteAccessSecret @"7436130958271126495"

#define OAuthRedirectURI @"vfushttp://auth_success"

#define SURVEY_TYPE_VISIT_FORM @"Visit Form"
#define SURVEY_TYPE_SOP_Audit @"SOP Audit"
#define SURVEY_TYPE_MSP_SURVEY @"MSP Survey"

#define VIEWMODE_DS     1
#define VIEWMODE_STORE  0

#define ACCOUNT_TBL_TAG 100
#define ACCOUNT_VAN_TAG 200
#define ACCOUNT_KTG_TAG 300
#define ACCOUNT_LEE_TAG 1000001
#define ACCOUNT_TNF_TAG 1000002
#define ACCOUNT_WGR_TAG 1000003
#define ACCOUNT_NAP_TAG 1000004

#define LOCAL_FILTER_NAME @"custom_filter"

#define ACCOUNT_TBL_CODE @"TBL"
#define ACCOUNT_VAN_CODE @"VAN"
#define ACCOUNT_KTG_CODE @"KPL"
#define ACCOUNT_LEE_CODE @"LEE"
#define ACCOUNT_TNF_CODE @"TNF"
#define ACCOUNT_WGR_CODE @"WGR"
#define ACCOUNT_NAP_CODE @"NAP"


#define AVAIL_BRAND_UNIQUE_CODES @"AVAIL_BRAND_UNIQUE_CODES"
#define AVAIL_COUNTRY_CODES @"AVAIL_COUNTRY_CODES"
#define AVAIL_ACCOUNT_CODES @"AVAIL_ACCOUNT_CODES"
#define AVAIL_ACCOUNT_IDS @"AVAIL_ACCOUNT_IDS"
#define USER_PREFER_COUNTRY_CODE @"USER_PREFER_COUNTRY_CODE"
#define USER_PREFER_ACCOUNT_CODE @"USER_PREFER_ACCOUNT_CODE"
#define USER_PREFER_ACCOUNT_ID @"USER_PREFER_ACCOUNT_ID"
@interface Constants : NSObject

@end
