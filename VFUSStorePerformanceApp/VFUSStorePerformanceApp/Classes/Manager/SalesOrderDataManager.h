//
//  SalesOrderDataManager.h
//
//  Handle the SalesOrder discount rule
//
//  Created by Developer 2 on 19/12/13.
//  Copyright (c) 2013 VF. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ProductLineDiscountRule : NSObject

@property (nonatomic, assign) int discountAmount;
@property (nonatomic, assign) int discountBaseAmount;
@property (nonatomic, assign) float discountPCT;
@property (nonatomic, assign) float accuDiscountPCT;

@property (nonatomic, retain) NSString* name;

-(id)initWithName:(NSString*)name;

@end

@interface SalesOrderDataManager : NSObject{
    NSMutableArray* normalOrderArray;
    NSMutableArray* sampleOrderArray;
    NSMutableArray* writeoffOrderArray;
    
    NSMutableArray* discountTable;
    
    NSDictionary* brandData;

}


+(SalesOrderDataManager*) shareInstance;

+(ProductLineDiscountRule*) calculateDiscountRuleForProductData:(NSDictionary*)data withDiscountTable:(NSArray*)discountTable;

-(void)reset;

-(void)addNormalLineItemData:(NSDictionary*)data;

-(void)addSampleLineItemData:(NSDictionary*)data;

-(void)addWriteoffLineItemData:(NSDictionary*)data;

-(void)addDiscountRuleDataAry:(NSArray*)discountRuleAry;

-(void)setBrandData:(NSDictionary*)data;


@end
