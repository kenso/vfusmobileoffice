//
//  SalesOrderDataManager.m
//
//  Created by Developer 2 on 19/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import "SalesOrderDataManager.h"
#import "DataHelper.h"



@implementation ProductLineDiscountRule

-(id)initWithName:(NSString *)name{
    self = [super init];
    if(self){
        self.name = name;
    }
    return self;
}

@end

@implementation SalesOrderDataManager

static SalesOrderDataManager* instance;


+(SalesOrderDataManager*) shareInstance{
    if(instance==nil){
        instance = [[SalesOrderDataManager alloc] init];
    }
    return instance;
}

-(id)init{
    self = [super init];
    if(self!=nil){
        normalOrderArray = [[NSMutableArray alloc] init];
        sampleOrderArray = [[NSMutableArray alloc] init];
        writeoffOrderArray = [[NSMutableArray alloc] init];
        discountTable = [[NSMutableArray alloc] init];
        
    }
    return self;
}

-(void)reset{
    [normalOrderArray removeAllObjects];
    [sampleOrderArray removeAllObjects];
    [writeoffOrderArray removeAllObjects];
    [discountTable removeAllObjects];
    if(brandData!=nil) [brandData release];
    brandData = nil;
}

-(void)dealloc{
    [normalOrderArray dealloc];
    [sampleOrderArray dealloc];
    [writeoffOrderArray dealloc];
    [discountTable dealloc];
    [super dealloc];
}

-(void)addNormalLineItemData:(NSDictionary*)data{
    [normalOrderArray addObject:data];
}

-(void)addSampleLineItemData:(NSDictionary*)data{
    [sampleOrderArray addObject:data];
}

-(void)addWriteoffLineItemData:(NSDictionary*)data{
    [writeoffOrderArray addObject:data];
}

-(void)addDiscountRuleDataAry:(NSArray*)discountRuleAry{
    [discountTable addObjectsFromArray:discountRuleAry];
}

-(void)setBrandData:(NSDictionary*)data{
    brandData = [[NSDictionary alloc] initWithDictionary:data];
}




+(ProductLineDiscountRule*) calculateDiscountRuleForProductData:(NSDictionary*)data withDiscountTable:(NSArray*)discountTable{
    
    NSString* thisLineId = [data objectForKey:@"emfa__Product_Sub_Brand__c"];
    if(thisLineId==nil || thisLineId==(id)[NSNull null] || [thisLineId isEqualToString:@"N/A"]){
        thisLineId = [DataHelper getFieldDisplayWithFieldName:@"emfa__Product_SKU__r.emfa__Product_Sub_Brand__c" withDictionary:data];
    }
    thisLineId = [thisLineId substringToIndex:15];
    
    if(discountTable!=nil && [discountTable count]>0){
        for(NSDictionary* table in discountTable){
            
            NSString* lineID = [table objectForKey:@"Product_Line__c"];
            lineID = [lineID substringToIndex:15];
            
            if([lineID isEqualToString:thisLineId]){

                NSString* type = [DataHelper getFieldDisplayWithFieldName:@"RecordType.Name" withDictionary:table];
                if([type  isEqualToString:@"Free Item"]){
                    int standardVal = [[data objectForKey:@"Standard_Qty__c"] intValue];
                    int discountBase = [[table objectForKey:@"Discount_Base_Qty__c"] intValue];
                    int discountVal = 0;
                    while(standardVal>=discountBase){
                        standardVal -= discountBase;
                        discountVal += [[table objectForKey:@"Discount_Free_Qty__c"] intValue];
                    }
                    ProductLineDiscountRule* rule = [[[ProductLineDiscountRule alloc] initWithName:@"A1"] autorelease];
                    rule.discountBaseAmount = discountBase;
                    rule.discountAmount = discountVal;
                    return rule;
                }else if([type isEqualToString:@"Fixed Discount Percentage"]){
                    ProductLineDiscountRule* rule = [[[ProductLineDiscountRule alloc] initWithName:@"A2"] autorelease];
                    float pctVal = [[table objectForKey:@"Fixed_Discount_Percentage__c"] floatValue];
                    rule.discountPCT = pctVal/100.0f;
                    return rule;
                }else if([type isEqualToString:@"Accu Discount"]){
                    ProductLineDiscountRule* rule = [[[ProductLineDiscountRule alloc] initWithName:@"A3"] autorelease];
                    float pctVal = [[table objectForKey:@"Accu_Discount_Percentage__c"] floatValue];
                    rule.accuDiscountPCT = pctVal/100.0f;
                    return rule;
                }
            }
        }
    }
    return nil;
    
}


@end
