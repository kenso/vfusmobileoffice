//
//  OrderCreationManager.m
//  VF Ordering App
//
//  Created by Developer 2 on 4/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import "OrderCreationManager.h"
#import "DataHelper.h"

@implementation OrderCreationManager

static OrderCreationManager* instance;


+(OrderCreationManager*) shareInstance{
    if(instance==nil){
        instance = [[OrderCreationManager alloc] init];
    }
    return instance;
}


-(void) setDataWithDictionary:(NSDictionary *)dict{
    self.brandId = [DataHelper getFieldDisplayWithFieldName:@"Product_Brand__c" withDictionary:dict];
    if(self.brandId==nil || [self.brandId isEqualToString:@"N/A"]){
        self.brandId =[DataHelper getFieldDisplayWithFieldName:@"Product_Brand__r.Id" withDictionary:dict];
    }
    self.brandName = [DataHelper getFieldDisplayWithFieldName:@"Product_Brand__r.Name" withDictionary:dict];
    self.accountId =[DataHelper getFieldDisplayWithFieldName:@"emfa__Account__c" withDictionary:dict];
    if(self.accountId==nil || [self.accountId isEqualToString:@"N/A"]){
        self.accountId =[DataHelper getFieldDisplayWithFieldName:@"emfa__Account__r.Id" withDictionary:dict];
    }
    self.accountName = [DataHelper getFieldDisplayWithFieldName:@"emfa__Account__r.Name" withDictionary:dict];
    self.accountCode = [DataHelper getFieldDisplayWithFieldName:@"emfa__Account__r.Account_Code__c" withDictionary:dict];
    self.toDivHead = [[DataHelper getFieldDisplayWithFieldName:@"emfa__Account__r.To_Div_Head__c" withDictionary:dict] boolValue];
    
    NSString* customerGroup = [DataHelper getFieldDisplayWithFieldName:@"emfa__Account__r.Customer_Group__c" withDictionary:dict];
    if(customerGroup!=nil && ![customerGroup isEqualToString:@"N/A"]){
        self.customerGroup = customerGroup;
    }
    self.storeId = [DataHelper getFieldDisplayWithFieldName:@"Store__c" withDictionary:dict];
    if(self.storeId==nil || [self.storeId isEqualToString:@"N/A"]){
        self.storeId =[DataHelper getFieldDisplayWithFieldName:@"Store__r.Id" withDictionary:dict];
    }
    self.storeName = [DataHelper getFieldDisplayWithFieldName:@"Store__r.Name" withDictionary:dict];
    self.brandAccuAmountLine = [[DataHelper getFieldDisplayWithFieldName:@"Product_Brand__r.Accu_Amount_Line__c" withDictionary:dict] floatValue];
    self.brandOnTopAmountLine = [[DataHelper getFieldDisplayWithFieldName:@"Product_Brand__r.OnTop_Amount_Line__c" withDictionary:dict] floatValue];
    self.brandOnTopDiscountPCT = [[DataHelper getFieldDisplayWithFieldName:@"Product_Brand__r.OnTop_Discount_Percentage__c" withDictionary:dict] floatValue];
    self.brandApprovalCriticalAmount = [[DataHelper getFieldDisplayWithFieldName:@"Product_Brand__r.Approval_Criticial_Amount__c" withDictionary:dict  ] floatValue];
    self.brandApprovalCriticalDiscountPCT = [[DataHelper getFieldDisplayWithFieldName:@"Product_Brand__r.Approval_Critical_Discount__c" withDictionary:dict  ] floatValue];

}



-(void) dealloc{
    self.accountId = nil;
    self.accountName = nil;
    
    self.storeId = nil;
    self.storeName = nil;

    [super dealloc];
}

@end
