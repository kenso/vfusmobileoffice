//
//  OrderCreationManager.h
//  VF Ordering App
//
//  Handle the save/recal process
//
//  Created by Developer 2 on 4/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderCreationManager : NSObject

@property (retain) NSString* accountId;
@property (retain) NSString* accountName;
@property (retain) NSString* accountCode; //SAP code
@property (nonatomic, assign) BOOL toDivHead;

@property (retain) NSString* storeId;
@property (retain) NSString* storeName;

@property (retain) NSString* brandId;
@property (retain) NSString* brandName;

@property (assign) float brandApprovalCriticalAmount;
@property (assign) float brandApprovalCriticalDiscountPCT;
@property (assign) float brandAccuAmountLine;
@property (assign) float brandOnTopAmountLine;
@property (assign) float brandOnTopDiscountPCT;

@property (retain) NSString* customerGroup;

@property (assign) float netPriceLimit;
@property (assign) float discountWarningPCT;
@property (assign) float sampleCostLimit;
@property (assign) float writeoffCostLimit;


+(OrderCreationManager*) shareInstance;

-(void) setDataWithDictionary:(NSDictionary*)dict;


@end
