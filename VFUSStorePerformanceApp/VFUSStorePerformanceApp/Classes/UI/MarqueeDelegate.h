//
//  MarqueeDelegate.h
//  VFStorePerformanceApp_Billy2
//
//  Created by Billy Lo on 13/4/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MarqueeDelegate <NSObject>

-(void)marqueeView:(UIView*)mview didClicked:(int)index;

-(NSInteger)numberOfRowForMarqueeView:(UIView *)mview ;
-(UIView*)marqueeView:(UIView *)mview viewAtRow:(int)index;

@end
