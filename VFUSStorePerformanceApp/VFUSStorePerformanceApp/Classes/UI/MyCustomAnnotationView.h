//
//  MyCustomAnnotationView.h
//  PernodRicardMobileApp
//
//  Created by  Chris on 3/7/13.
//  Copyright (c) 2013 Laputa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

//@interface MyCustomAnnotationView : MKAnnotationView
@interface MyCustomAnnotationView : MKPinAnnotationView
{
    // Custom data members
}
// Custom properties and methods.
@end

