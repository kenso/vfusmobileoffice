//
//  CustomMKAnnotation.m
//  MerchandisingApp
//
//  Created by Ken Tsang on 27/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//


#import "CustomMKAnnotation.h"

@implementation CustomMKAnnotation
@synthesize coordinate;
@synthesize annoIndex;

- (NSString *)subtitle{
	return mySubtitle;
}
- (NSString *)title{
	return myTitle;
}

-(id)initWithCoordinate:(CLLocationCoordinate2D) c{
    /////Get coordinates only
    
	coordinate=c;
	NSLog(@"%f,%f",c.latitude,c.longitude);
	return self;
}

-(id)initWithCoordinate:(CLLocationCoordinate2D)c andTitle:(NSString*)t andSubtitle:(NSString*)s{
    /////Get coordinates, title and subtitle
    
    coordinate=c;
	NSLog(@"%f,%f",c.latitude,c.longitude);
    myTitle=t;
    mySubtitle=s;
    
    
    
	return self;
}





@end

