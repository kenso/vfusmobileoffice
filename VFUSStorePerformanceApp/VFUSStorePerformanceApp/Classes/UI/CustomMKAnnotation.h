//
//  CustomMKAnnotation.h
//  MerchandisingApp
//
//  Created by Ken Tsang on 27/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface CustomMKAnnotation : NSObject<MKAnnotation> {
    CLLocationCoordinate2D coordinate;
    NSString *myTitle;
    NSString *mySubtitle;
    int annoIndex;
}

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, assign) int annoIndex;

-(id)initWithCoordinate:(CLLocationCoordinate2D)coordinate;
-(id)initWithCoordinate:(CLLocationCoordinate2D)coordinate andTitle:(NSString*)title andSubtitle:(NSString*)subtitle;
- (NSString *)subtitle;
- (NSString *)title;

@end

