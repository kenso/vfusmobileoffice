//
//  MarqueeView.h
//  VFStorePerformanceApp_Billy2
//
//  Created by Billy Lo on 13/4/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MarqueeDelegate.h"

@interface MarqueeView : UIView

@property (assign, nonatomic) id<MarqueeDelegate> delegate;

- (id)initWithFrame:(CGRect)frame;
- (void)reloadMessageMarqueeViewData;
//- (void) startAnimation;

@end


