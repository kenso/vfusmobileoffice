//
//  MarqueeView.m
//  VFStorePerformanceApp_Billy2
//
//  Created by Billy Lo on 13/4/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "MarqueeView.h"

//
// This class implement a vertical Marquee view.
//It load a 
//
@implementation MarqueeView

- (id)initWithFrame:(CGRect)frame;
{
    self = [super initWithFrame:frame];
    if (self) {
        self.clipsToBounds = YES;
    }
    return self;
}


- (void)reloadMessageMarqueeViewData{
    NSArray* children = [self subviews];
    for(UIView* v in children){
        [v removeFromSuperview];
    }
    
    NSInteger count = [self.delegate numberOfRowForMarqueeView:self];
    
    int lastY = 0;
    
    for(int i=0; i<count; i++){
        UIView* view = [self.delegate marqueeView:self viewAtRow:i];
        view.frame = CGRectMake(view.frame.origin.x, lastY, view.frame.size.width, view.frame.size.height);
        lastY = lastY + view.frame.size.height;
        [self addSubview:view];
        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onRowTapEvent:)];
        
        [view setUserInteractionEnabled:YES];
        [view addGestureRecognizer:gesture];
        view.tag = i;
        [gesture release];
        
    }
    
    
    children = self.subviews;
    
    lastY = 0;
    int i=0;
    
    float totalHeight = 0;
    float finalRowH = 0;
    for(UIView* view in children){
        totalHeight = totalHeight + view.frame.size.height;
        finalRowH = view.frame.size.height;
    }
    totalHeight += finalRowH;
    
    float dur = [self.delegate numberOfRowForMarqueeView:self]*3;
    
    for(UIView* view in children){
        UIView* v = view;
        v.frame = CGRectMake(v.frame.origin.x, lastY, v.frame.size.width, v.frame.size.height);

        lastY = lastY + v.frame.size.height;
        const float finalY = lastY - totalHeight;
        [UIView animateWithDuration: dur  delay:0.05f options:(UIViewAnimationOptionRepeat|UIViewAnimationOptionAllowUserInteraction|UIViewAnimationCurveLinear) animations:^{
            v.frame = CGRectMake(v.frame.origin.x, finalY, v.frame.size.width, v.frame.size.height);
        } completion:^(BOOL finished) {
            
        }];
        i++;
    }
    
}

-(void)onRowTapEvent:(UITapGestureRecognizer*)gesture{
    NSLog(@"onRowTapEvent");
    UIView* view = gesture.view;
    NSInteger v= view.tag;
    [self.delegate marqueeView:self didClicked:v];
}







@end
