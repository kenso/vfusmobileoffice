//
//  UIHelper.h
//  VF Ordering App
//
//  Created by Developer 2 on 6/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIHelper : NSObject


+(void) applyGreenButtonImageOnButton:(UIButton*)button;

+(void) applyGreenTinyButtonImageOnButton:(UIButton*)button;

+(void) applyGreenSmallButtonImageOnButton:(UIButton*)button;

+(void) applyPurpleTinyButtonImageOnButton:(UIButton*)button;

+(void) applyPurpleSmallButtonImageOnButton:(UIButton*)button;

+(void) applyPurpleButtonImageOnButton:(UIButton*)button;

+(void) applyBlueSmallButtonImageOnButton:(UIButton*)button;
    
+(void) applyBlueButtonImageOnButton:(UIButton*)button;

+(void) applyBlueTinyButtonImageOnButton:(UIButton*)button;

+(void) applyYellowButtonImageOnButton:(UIButton*)button;

+(void) applyYellowSmallButtonImageOnButton:(UIButton*)button;

+(void) applyYellowTinyButtonImageOnButton:(UIButton*)button;

+(void) applyOrangeButtonImageOnButton:(UIButton*)button;

+(void) applyOrangeSmallButtonImageOnButton:(UIButton*)button;

+(void) applyOrangeTinyButtonImageOnButton:(UIButton*)button;

@end
