//
//  MyCustomAnnotationView.m
//  PernodRicardMobileApp
//
//  Created by  Chris on 3/7/13.
//  Copyright (c) 2013 Laputa. All rights reserved.
//

#import "MyCustomAnnotationView.h"

@implementation MyCustomAnnotationView
/*
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
*/
- (id)initWithAnnotation:(id <MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self)
    {


       // NSLog(@" initWithAnnotation -----------------------------------------------");
        // Set the frame size to the appropriate values.
        CGRect  myFrame = self.frame;
        myFrame.size.width = 40;
        myFrame.size.height = 40;
        
        self.frame = myFrame;

        // The opaque property is YES by default. Setting it to
        // NO allows map content to show through any unrendered
        // parts of your view.

        self.opaque = NO;
     //   self.opaque = YES;
       // [self setAlpha:0.8];
    }
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
