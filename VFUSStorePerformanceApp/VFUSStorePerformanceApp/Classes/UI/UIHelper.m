//
//  UIHelper.m
//  VF Ordering App
//
//  Created by Developer 2 on 6/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import "UIHelper.h"

@implementation UIHelper


+(void) applyGreenButtonImageOnButton:(UIButton*)button{
    
    UIImage *originalImage = [UIImage imageNamed:@"button_green.png"];
    UIEdgeInsets insets = UIEdgeInsetsMake(20, 0, 0, 20);//top, left, bottom, right
    
    UIImage *stretchableImage = [originalImage resizableImageWithCapInsets:insets];
    [button setBackgroundImage:stretchableImage forState:UIControlStateNormal];
    
}

+(void) applyGreenSmallButtonImageOnButton:(UIButton*)button{
    
    UIImage *originalImage = [UIImage imageNamed:@"button_green_s.png"];
    UIEdgeInsets insets = UIEdgeInsetsMake(10, 0, 0, 10);//top, left, bottom, right
    
    UIImage *stretchableImage = [originalImage resizableImageWithCapInsets:insets];
    [button setBackgroundImage:stretchableImage forState:UIControlStateNormal];
    
}


+(void) applyOrangeTinyButtonImageOnButton:(UIButton*)button{
    
    UIImage *originalImage = [UIImage imageNamed:@"button_orange_t.png"];
    UIEdgeInsets insets = UIEdgeInsetsMake(5, 0, 0, 5);//top, left, bottom, right
    
    UIImage *stretchableImage = [originalImage resizableImageWithCapInsets:insets];
    [button setBackgroundImage:stretchableImage forState:UIControlStateNormal];
    
}



+(void) applyOrangeButtonImageOnButton:(UIButton*)button{
    
    UIImage *originalImage = [UIImage imageNamed:@"button_orange.png"];
    UIEdgeInsets insets = UIEdgeInsetsMake(20, 0, 0, 20);//top, left, bottom, right
    
    UIImage *stretchableImage = [originalImage resizableImageWithCapInsets:insets];
    [button setBackgroundImage:stretchableImage forState:UIControlStateNormal];
    
}



+(void) applyOrangeSmallButtonImageOnButton:(UIButton*)button{
    
    UIImage *originalImage = [UIImage imageNamed:@"button_orange_s.png"];
    UIEdgeInsets insets = UIEdgeInsetsMake(10, 0, 0, 10);//top, left, bottom, right
    
    UIImage *stretchableImage = [originalImage resizableImageWithCapInsets:insets];
    [button setBackgroundImage:stretchableImage forState:UIControlStateNormal];
    
}


+(void) applyYellowButtonImageOnButton:(UIButton*)button{
    
    UIImage *originalImage = [UIImage imageNamed:@"button_yellow.png"];
    UIEdgeInsets insets = UIEdgeInsetsMake(20, 0, 0, 20);//top, left, bottom, right
    
    UIImage *stretchableImage = [originalImage resizableImageWithCapInsets:insets];
    [button setBackgroundImage:stretchableImage forState:UIControlStateNormal];
    
}



+(void) applyYellowTinyButtonImageOnButton:(UIButton*)button{
    
    UIImage *originalImage = [UIImage imageNamed:@"button_yellow_t.png"];
    UIEdgeInsets insets = UIEdgeInsetsMake(5, 0, 0, 5);//top, left, bottom, right
    
    UIImage *stretchableImage = [originalImage resizableImageWithCapInsets:insets];
    [button setBackgroundImage:stretchableImage forState:UIControlStateNormal];
    
}



+(void) applyYellowSmallButtonImageOnButton:(UIButton*)button{
    
    UIImage *originalImage = [UIImage imageNamed:@"button_yellow_s.png"];
    UIEdgeInsets insets = UIEdgeInsetsMake(10, 0, 0, 10);//top, left, bottom, right
    
    UIImage *stretchableImage = [originalImage resizableImageWithCapInsets:insets];
    [button setBackgroundImage:stretchableImage forState:UIControlStateNormal];
    
}



+(void) applyGreenTinyButtonImageOnButton:(UIButton*)button{
    UIImage *originalImage = [UIImage imageNamed:@"button_green_t.png"];
    UIEdgeInsets insets = UIEdgeInsetsMake(5, 0, 0, 5);//top, left, bottom, right
    
    UIImage *stretchableImage = [originalImage resizableImageWithCapInsets:insets];
    [button setBackgroundImage:stretchableImage forState:UIControlStateNormal];
    
}


+(void) applyPurpleButtonImageOnButton:(UIButton*)button{
    
    UIImage *originalImage = [UIImage imageNamed:@"button_purple.png"];
    UIEdgeInsets insets = UIEdgeInsetsMake(20, 0, 0, 20);//top, left, bottom, right
    
    UIImage *stretchableImage = [originalImage resizableImageWithCapInsets:insets];
    [button setBackgroundImage:stretchableImage forState:UIControlStateNormal];
    
}

+(void) applyPurpleTinyButtonImageOnButton:(UIButton*)button{
    
    UIImage *originalImage = [UIImage imageNamed:@"button_purple_t.png"];
    UIEdgeInsets insets = UIEdgeInsetsMake(5, 0, 0, 5);//top, left, bottom, right
    
    UIImage *stretchableImage = [originalImage resizableImageWithCapInsets:insets];
    [button setBackgroundImage:stretchableImage forState:UIControlStateNormal];
    
}



+(void) applyPurpleSmallButtonImageOnButton:(UIButton*)button{
    
    UIImage *originalImage = [UIImage imageNamed:@"button_purple_s.png"];
    UIEdgeInsets insets = UIEdgeInsetsMake(10, 0, 0, 10);//top, left, bottom, right
    
    UIImage *stretchableImage = [originalImage resizableImageWithCapInsets:insets];
    [button setBackgroundImage:stretchableImage forState:UIControlStateNormal];
    
}




+(void) applyBlueButtonImageOnButton:(UIButton*)button{
    
    UIImage *originalImage = [UIImage imageNamed:@"button_blue.png"];
    UIEdgeInsets insets = UIEdgeInsetsMake(20, 0, 0, 20);//top, left, bottom, right
    
    UIImage *stretchableImage = [originalImage resizableImageWithCapInsets:insets];
    [button setBackgroundImage:stretchableImage forState:UIControlStateNormal];
    
}



+(void) applyBlueSmallButtonImageOnButton:(UIButton*)button{
    
    UIImage *originalImage = [UIImage imageNamed:@"button_blue_s.png"];
    UIEdgeInsets insets = UIEdgeInsetsMake(10, 0, 0, 10);//top, left, bottom, right
    
    UIImage *stretchableImage = [originalImage resizableImageWithCapInsets:insets];
    [button setBackgroundImage:stretchableImage forState:UIControlStateNormal];
    
}


+(void) applyBlueTinyButtonImageOnButton:(UIButton*)button{
    
    UIImage *originalImage = [UIImage imageNamed:@"button_blue_t.png"];
    UIEdgeInsets insets = UIEdgeInsetsMake(5, 0, 0, 5);//top, left, bottom, right
    
    UIImage *stretchableImage = [originalImage resizableImageWithCapInsets:insets];
    [button setBackgroundImage:stretchableImage forState:UIControlStateNormal];
    
}




@end
