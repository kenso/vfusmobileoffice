/*
 Copyright (c) 2011, salesforce.com, inc. All rights reserved.
 
 Redistribution and use of this software in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this list of conditions
 and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list of
 conditions and the following disclaimer in the documentation and/or other materials provided
 with the distribution.
 * Neither the name of salesforce.com, inc. nor the names of its contributors may be used to
 endorse or promote products derived from this software without specific prior written
 permission of salesforce.com, inc.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#import "AppDelegate.h"
#import "GeneralUiTool.h"
#import "ShareLocale.h"
#import "InitialViewController.h"
#import "SFAccountManager.h"
#import "SFAuthenticationManager.h"
#import "SFPushNotificationManager.h"
#import "BusinessLogicHelper.h"
#import "SFOAuthInfo.h"
#import "SFLogger.h"
#import "HomeViewController.h"
#import "UIHelper.h"
#import "Constants.h"

// Fill these in when creating a new Connected Application on Force.com

@interface AppDelegate ()

/**
 * Success block to call when authentication completes.
 */
@property (nonatomic, copy) SFOAuthFlowSuccessCallbackBlock initialLoginSuccessBlock;

/**
 * Failure block to calls if authentication fails.
 */
@property (nonatomic, copy) SFOAuthFlowFailureCallbackBlock initialLoginFailureBlock;

/**
 * Handles the notification from SFAuthenticationManager that a logout has been initiated.
 * @param notification The notification containing the details of the logout.
 */
- (void)logoutInitiated:(NSNotification *)notification;

/**
 * Handles the notification from SFAuthenticationManager that the login host has changed in
 * the Settings application for this app.
 * @param The notification whose userInfo dictionary contains:
 *        - kSFLoginHostChangedNotificationOriginalHostKey: The original host, prior to host change.
 *        - kSFLoginHostChangedNotificationUpdatedHostKey: The updated (new) login host.
 */
- (void)loginHostChanged:(NSNotification *)notification;

/**
 * Convenience method for setting up the main UIViewController and setting self.window's rootViewController
 * property accordingly.
 */
- (void)setupRootViewController;

/**
 * (Re-)sets the view state when the app first loads (or post-logout).
 */
- (void)initializeAppViewState;

@end

@implementation AppDelegate

@synthesize window = _window;
@synthesize initialLoginSuccessBlock = _initialLoginSuccessBlock;
@synthesize initialLoginFailureBlock = _initialLoginFailureBlock;

- (id)init
{
    self = [super init];
    if (self) {
        
        
#ifndef DEBUG
        [SFLogger setLogLevel:SFLogLevelWarning];
#endif
        
#ifdef DEBUG
        [SFLogger setLogLevel:SFLogLevelDebug];
#endif

        
        // These SFAccountManager settings are the minimum required to identify the Connected App.
        [SFAccountManager setClientId:RemoteAccessConsumerKey];
        [SFAccountManager setRedirectUri:OAuthRedirectURI];
        [SFAccountManager setScopes:[NSSet setWithObjects:@"api", nil]];
        
        // Logout and login host change handlers.
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logoutInitiated:) name:kSFUserLogoutNotification object:[SFAuthenticationManager sharedManager]];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginHostChanged:) name:kSFLoginHostChangedNotification object:[SFAuthenticationManager sharedManager]];
        
        // Blocks to execute once authentication has completed.  You could define these at the different boundaries where
        // authentication is initiated, if you have specific logic for each case.
        self.initialLoginSuccessBlock = ^(SFOAuthInfo *info) {
            NSLog(@"Login SUCCESS %@", info);
            if(info.authType == SFOAuthTypeUserAgent){
                //New logged in with username / password
                //It only happened when network is available
                VFDAO* dao = [VFDAO sharedInstance];
                [dao isOnlineMode:YES];
                
//                NSDictionary *defaultsDictionary = [[NSUserDefaults standardUserDefaults] dictionaryRepresentation];
//                for (NSString *key in [defaultsDictionary allKeys]) {
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
//                }
            }
            else if (info.authType == SFOAuthTypeRefresh){
                //Logined before, just refresh.  User don't need input user name and password
            }
            [self setupRootViewController];
        };
        self.initialLoginFailureBlock = ^(SFOAuthInfo *info, NSError *error) {
            NSLog(@"Login FAIL");
            [[SFAuthenticationManager sharedManager] logout];
        };
        
        /*
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
         */

    }
    
    return self;
}


- (void)keyboardDidShow:(NSNotification *)notification
{
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    UIView *topView = window.rootViewController.view;
    NSLog(@"%@", notification.name);
    
    [topView setFrame:CGRectMake(-180,0,topView.frame.size.width,topView.frame.size.height)];
    
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    UIView *topView = window.rootViewController.view;

    [topView setFrame:CGRectMake(0,0,topView.frame.size.width,topView.frame.size.height)];
}





- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSFUserLogoutNotification object:[SFAuthenticationManager sharedManager]];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSFLoginHostChangedNotification object:[SFAuthenticationManager sharedManager]];
    [super dealloc];
}

#pragma mark - App delegate lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{


    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self initializeAppViewState];
    
    
    [BusinessLogicHelper initAllThresholdDefaultValue];
    [self initailizeNetworkSetting];

    
    //NOtification Code here
    //[[SFPushNotificationManager sharedInstance] registerForRemoteNotifications];
    //
    

    [self login];
    
    return YES;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    //
    // Uncomment the code below to register your device token with the push notification manager
    //
    //[[SFPushNotificationManager sharedInstance] didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
    //if ([SFAccountManager sharedInstance].credentials.accessToken != nil) {
    //    [[SFPushNotificationManager sharedInstance] registerForSalesforceNotifications];
    //}
    //
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    // Respond to any push notification registration errors here.
}

#pragma mark - Private methods


-(void) initailizeNetworkSetting{
    NSTimeInterval timeoutSeconds = 60;
    [SFNetworkEngine sharedInstance].operationTimeout = timeoutSeconds;

}

- (void)initializeAppViewState
{
    self.window.rootViewController = [[InitialViewController alloc] initWithNibName:nil bundle:nil];
    [self.window makeKeyAndVisible];
}

- (void)setupRootViewController
{
    //Set default language code if not exist
    [[ShareLocale sharedLocalSystem] initLanguage];
    
    VFDAO* dao = [VFDAO sharedInstance];
    [dao setupReachability];//reachibility can only be run after login!

    
    HomeViewController *rootVC = [[HomeViewController alloc] initWithNibName:nil bundle:nil];
    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:rootVC];
    self.window.rootViewController = navVC;
}

- (void)logoutInitiated:(NSNotification *)notification
{
    [self log:SFLogLevelDebug msg:@"Logout notification received.  Resetting app."];
    NSUserDefaults *localCache = [NSUserDefaults standardUserDefaults];
    [localCache removeObjectForKey:@"userName"];

    [[VFDAO sharedInstance] resetDB];
    [self initializeAppViewState];
    [self login];
}

- (void)loginHostChanged:(NSNotification *)notification
{
    [self log:SFLogLevelDebug msg:@"Login host changed notification received.  Resetting app."];
    [self initializeAppViewState];
    [self login];
}





#pragma mark - Custom Login


-(void) login{
    
    [[SFAuthenticationManager sharedManager] loginWithCompletion:self.initialLoginSuccessBlock failure:self.initialLoginFailureBlock];
}


//
//-(void) showLoginForm{
//    if(self.loginFormView == nil){
//        NSLog(@"show Login form");
////        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults] ;
////        [defaults removeObjectForKey:@"username"];
////        [defaults removeObjectForKey:@"password"];
////        [defaults synchronize];
//
//        NSArray *views = [[NSBundle mainBundle] loadNibNamed:@"CustomLoginView" owner:self options:nil];
//        self.loginFormView = [views objectAtIndex:0];
//        
//        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
//        UIView *topView = window.rootViewController.view;
//        [topView addSubview:self.loginFormView];
//        
//        UITextField* userNameTxt = (UITextField*)[self.loginFormView viewWithTag:100];
//        UITextField* passwordTxt = (UITextField*)[self.loginFormView viewWithTag:200];
//        UIButton* loginButton = (UIButton*)[self.loginFormView viewWithTag:300];
//        [UIHelper applyBlueSmallButtonImageOnButton:loginButton];
//        
//        UITextField* errorMessageTxt = (UITextField*)[self.loginFormView viewWithTag:400];
//        errorMessageTxt.text = (self.loginErrorMessage==nil)?@"":[NSString stringWithFormat:@"Error: %@", self.loginErrorMessage] ;
//        
//        userNameTxt.leftView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20) ] autorelease];
//        userNameTxt.leftViewMode = UITextFieldViewModeAlways;
//        passwordTxt.leftView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20) ] autorelease];
//        passwordTxt.leftViewMode = UITextFieldViewModeAlways;
//        
//        [loginButton addTarget:self action:@selector(onLoginButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
//    }
//}
//
//
//
//
//-(void) onLoginButtonClicked:(id)sender{
//    
//    loginCount = 0;
//    
//
//    [self login];
//}
//
//-(void) removeLoginForm{
//    if(self.loginFormView != nil){
//        NSLog(@"Remove Login form");
//        [self.loginFormView removeFromSuperview];
//        self.loginFormView = nil;
//
//    }
//}
//
//- (void)startAuthenticationByUsername:(NSString *)username password:(NSString *)password
//{
//
//    NSLog(@"startAuthenticationByUsername...name=%@...pw=%@", username, password);
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://test.salesforce.com/services/oauth2/token"]];
//    [request setHTTPMethod:@"POST"];
//    NSString *params = [NSString stringWithFormat:@"%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@"
//                        , @"format", @"json"
//                        , @"grant_type", @"password"
//                        , @"client_id", RemoteAccessConsumerKey
//                        , @"client_secret", RemoteAccessSecret
//                        , @"username", username
//                        , @"password", password];
//    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
//    [NSURLConnection connectionWithRequest:request delegate:self];
// 
//    [self removeLoginForm];
//}
//
//
//- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
//{
//    self.receivedData = [NSMutableData dataWithCapacity:1024];
//    [self.receivedData setLength:0];
//}
//
//
//- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
//{
//    [self.receivedData appendData:data];
//}
//
//- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
//    NSLog(@"didFailWithError...error=%@", error);
//    [self showLoginForm];
//}
//
//
//- (void)connectionDidFinishLoading:(NSURLConnection *)connection
//{
//    NSLog(@"connectionDidFinishLoading...END");
//    
//    NSError *finalError = nil;
//    if (self.receivedData && [self.receivedData length] > 0) {
//        NSError *error = nil;
//        id jsonObj = [NSJSONSerialization JSONObjectWithData:self.receivedData options:NSJSONReadingMutableContainers error:&error];
//        if (jsonObj && [jsonObj isKindOfClass:[NSDictionary class]]) {
//            NSDictionary *jsonDict = (NSDictionary *)jsonObj;
//            
//            NSLog(@"json obj = %@", jsonDict);
//            
//            if (jsonDict[@"error"]) {
//                
//                self.loginErrorMessage = jsonDict[@"error_description"];
//                
//                finalError = [NSError errorWithDomain:jsonDict[@"error"] code:1000 userInfo:@{@"description":jsonDict[@"error_description"]} ];
//                self.initialLoginFailureBlock(nil, finalError);
//                return;
//            } else {
//                NSString *timestampStr = jsonDict[@"issued_at"];
//                NSDate *issuedAtDate = timestampStr ? [NSDate dateWithTimeIntervalSince1970:[timestampStr longLongValue]] : nil;
//
//
//                SFAccountManager *acctManager = [SFAccountManager sharedInstance];
//                SFOAuthCredentials *cred = acctManager.coordinator.credentials;
//                SFOAuthInfo *authInfo = [[SFOAuthInfo alloc] initWithAuthType:SFOAuthTypeUserAgent];
//                
//                cred.identityUrl = jsonDict[@"id"]? [NSURL URLWithString:jsonDict[@"id"]] : nil;
//                cred.accessToken = jsonDict[@"access_token"];
//                cred.refreshToken = nil;
//                cred.instanceUrl = jsonDict[@"instance_url"]?[NSURL URLWithString:jsonDict[@"instance_url"]] : nil;
//                cred.issuedAt = issuedAtDate;
//                
//                NSLog(@"instanceUrl = %@", cred.instanceUrl);
//                NSLog(@"accessToken = %@", cred.accessToken);
//
//                acctManager.credentials = cred;
//                [acctManager.idCoordinator initiateIdentityDataRetrieval];
//                
//                NSLog(@"coordinator=%@",[SFRestAPI sharedInstance].coordinator );
//                NSLog(@"acctManager.coordinator=%@",acctManager.coordinator );
//
//                [self removeLoginForm ];
//                
//                [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"step" message:@"1"];
//                
//                self.loginErrorMessage = nil;
//                
//                [[GeneralUiTool shareInstance] showConfirmDialogWithTitle:@"step" message:@"2"];
//                
//                self.initialLoginSuccessBlock(authInfo);//SFOAuthInfo
//                return;
//            }
//        }
//    }
//    if (finalError == nil) {
//        NSError* err= [NSError errorWithDomain:kSFOAuthErrorDomain code:kSFOAuthErrorUnknown userInfo:@{@"description": @"Unknown error"}];
//        
//        self.loginErrorMessage = @"Unknown Error";
//        
//        self.initialLoginFailureBlock(nil, err);
//    }
//    
//}


#pragma mark - Orientation control
- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    return UIInterfaceOrientationMaskAll;
}




@end
