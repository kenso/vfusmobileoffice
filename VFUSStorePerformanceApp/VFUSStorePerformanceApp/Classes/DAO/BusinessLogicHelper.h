//
//  BusinessLogicHelper.h
//  VFStorePerformanceApp
//
//  Created by Developer 2 on 3/3/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <Foundation/Foundation.h>


#define W_PREF_OPS_SALES @"W_OPS_SALES"
#define W_PREF_DISCOUNT @"W_DISCOUNT"
#define W_PREF_SALES_PROD @"W_SALES_PROD"
#define W_PREF_ATV @"W_ATV"
#define W_PREF_UPT @"W_UPT"
#define W_PREF_TXN @"W_TXN"
#define W_PREF_VISITOR_COUNT @"W_VISITOR_COUNT"
#define W_PREF_PEL_OFF @"W_PEL_OFF"
#define W_PREF_CONV_RATE @"W_CONV_RATE"

#define M_PREF_OPS_SALES @"M_OPS_SALES"
#define M_PREF_DISCOUNT @"M_DISCOUNT"
#define M_PREF_SALES_PROD @"M_SALES_PROD"
#define M_PREF_ATV @"M_ATV"
#define M_PREF_UPT @"M_UPT"
#define M_PREF_TXN @"M_TXN"
#define M_PREF_VISITOR_COUNT @"M_VISITOR_COUNT"
#define M_PREF_PEL_OFF @"M_PEL_OFF"
#define M_PREF_CONV_RATE @"M_CONV_RATE"

#define Y_PREF_OPS_SALES @"Y_OPS_SALES"
#define Y_PREF_DISCOUNT @"Y_DISCOUNT"
#define Y_PREF_SALES_PROD @"Y_SALES_PROD"
#define Y_PREF_ATV @"Y_ATV"
#define Y_PREF_UPT @"Y_UPT"
#define Y_PREF_TXN @"Y_TXN"
#define Y_PREF_VISITOR_COUNT @"Y_VISITOR_COUNT"
#define Y_PREF_PEL_OFF @"Y_PEL_OFF"
#define Y_PREF_CONV_RATE @"Y_CONV_RATE"



#define CARD_OPS_SALES 551
#define CARD_DISCOUNT 552
#define CARD_SALES_PRODUCTIVITY 553
#define CARD_ATV 554
#define CARD_UPT 555
#define CARD_TRANSACTION_COUNT 556
#define CARD_VISITOR_COUNT 557
#define CARD_PEL_OFF 558
#define CARD_CONVERSION_RATE 559

#define MODE_INVENTORY 601



enum THRESHOLD_LEVEL : NSUInteger{
    HIGH = 1,
    NORMAL = 2,
    LOW = 3
};


@interface BusinessLogicHelper : NSObject



+(void) initAllThresholdDefaultValue;

//Set default high/low default value for Threshold of given type
+(void) setDefaultThresholdLevelForType:(NSString*)type highValue:(float)high lowValue:(float)low;

+(void) setHighThresholdLevelForType:(NSString*)type value:(float)value;
+(void) setLowThresholdLevelForType:(NSString*)type value:(float)value;

+(UIImage*) getColorPinForThreshold:(NSUInteger)threshold;
+(NSUInteger) getThresholdLevelForType:(NSString*)type value:(float)value;



+(float) getThresholdHighLevel:(NSString*)type;
+(float) getThresholdLowLevel:(NSString*)type;

+(UIImage*) getGoodPin;
+(UIImage*) getBadPin;


+(NSString*) convertToThousandBase:(float)v;
+(NSString*) convertValueToLimitedCharacterString:(float)v;
+(NSString*) convertValueToLimitedCharacterString:(float)v format:(NSString*)format;



@end
