//
//  VFDAO.h
//  VFStorePerformanceApp_Billy
//
//  Created by Developer 2 on 22/4/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SFRestAPI.h"
#import "SFRestRequest.h"
#import "CustomWebserviceRestAPI.h"
#import "DAO.h"


//SmartStore Table Name / Variable Name
#define SMART_STORE_NAME                @"VF_DB"
#define DB_ID                           @"DB_ID"
#define DB_DATA                         @"DB_DATA"
#define DB_TABLE_STORE                  @"Store"
#define DB_TABLE_CAPACITY               @"Store_Capacity"
#define DB_TABLE_TARGET                 @"Targets"
#define DB_TABLE_NOTIFICATION           @"Notification"
#define DB_TABLE_USERINFO               @"UserInfo"//replaced by AppInit
#define DB_TABLE_APP_INIT               @"AppInit"
#define DB_TABLE_ACCOUNT_INFO           @"AccountInfoDetail"
#define DB_TABLE_TASK                   @"Task"
#define DB_TABLE_COMMENT                @"Comment"
//#define DB_TABLE_COMMENT_MEDIA          @"Comment_Media"
#define DB_TABLE_STORE_ANALYSIS         @"Store_Chart_Data"
#define DB_TABLE_VISIT_PLAN             @"Visit_Plan"
#define DB_TABLE_STORE_REPORT           @"Store_Report"
#define DB_TABLE_EVENT                  @"Event"
#define DB_TABLE_SURVEY_FORM            @"Survey_Form"
#define DB_TABLE_SURVEY_MASTER          @"Survey_Master"
#define DB_TABLE_SURVEY_MASTER_QUESTION @"Survey_Master_Question"
#define DB_TABLE_KNOWLEDGE              @"Knowledge"
#define DB_TABLE_USAGE                  @"Usage_Report"
#define DB_TABLE_INVENTORY              @"Inventory"
#define DB_TABLE_STOCK_SALES            @"Stock_n_Sales"
#define DB_TABLE_SALES_MIX              @"Sales_Mix_Period"
//#define TABLE_PENDING_COMMENT           @"P_Comment"
#define TABLE_PENDING_VISIT_PLAN        @"P_Visit_Plan"
//#define TABLE_PENDING_SHARED_MEDIA      @"P_Shared_Media"
#define TABLE_PENDING_SHARED_MEDIA      @"P_Shared_Media2"

//SmartStore Table Structure Spec
#define DB_SPEC_TASK                    @[@{@"path":@"Id", @"type":@"string"}, @{@"path":@"dirty", @"type":@"string"}, @{@"path":@"RecordTypeId", @"type":@"string"}, @{@"path":@"WhatId", @"type":@"string"} ]
#define DB_SPEC_COMMENT                    @[@{@"path":@"Id", @"type":@"string"}, @{@"path":@"dirty", @"type":@"string"}, @{@"path":@"Store__c", @"type":@"string" }, @{@"path":@"ActivityDate__c", @"type":@"string" } ]
#define DB_SPEC_VISIT_PLAN              @[@{@"path":@"Id", @"type":@"string"}, @{@"path":@"dirty", @"type":@"string"}, @{@"path":@"emfa__Active__c", @"type":@"integer"},  @{@"path":@"emfa___Start_Date_Time__c", @"type":@"string"}]
#define DB_SPEC_STORE_REPORT              @[@{@"path":@"Id", @"type":@"string"}, @{@"path":@"emfa__Active__c", @"type":@"integer"},  @{@"path":@"emfa__Start_Time__c", @"type":@"string"}, @{@"path":@"emfa__Store__c", @"type":@"string"}]
#define DB_SPEC_SURVEY_FORM             @[ @{@"path":@"Id", @"type":@"string"},@{@"path":@"Master_Type__c", @"type":@"string"}, @{@"path":@"dirty", @"type":@"string"}, @{@"path":@"CreatedDate", @"type":@"string"}, @{@"path":@"emfa__Visitation_Plan__c",@"type":@"string"} ]
#define DB_SPEC_EVENT              @[@{@"path":@"Id", @"type":@"string"}, @{@"path":@"Subject", @"type":@"string"}, @{@"path":@"ActivityDate", @"type":@"string"},  @{@"path":@"ActivityDateTime", @"type":@"string"}, @{@"path":@"DurationInMinutes", @"type":@"string"}, @{@"path":@"StartDateTime", @"type":@"string"}, @{@"path":@"EndDateTime", @"type":@"string"}]


//REST API NAME
#define REST_API_APP_INIT_GET           @"AppInitRestAPI3"
#define REST_API_SURVEY_MASTER_GET      @"GetSurveyMasterResponse"
//#define REST_API_USERINFO_GET           @"UserInfoRestAPI"
#define REST_API_VISITPLAN_SAVE         @"VFVisitationPlanRestAPI"
#define REST_API_SURVEY_SAVE            @"VFSurveyRestAPI"
#define REST_API_CHARTDATE_GET          @"StoreChartDataRestAPI"
#define REST_API_VISIT_EMAIL_SEND       @"VFResendVisitationResultEmailAPI"
#define REST_API_USAGE_REPORT           @"UsageReportAPI"



/**
 *  VFDAO control all data access method, including both online and offline.
 */

@interface VFDAO : DAO<CustomWebserviceRestDelegate>

@property (nonatomic, retain) CustomWebserviceRequest* customRequest;
@property (nonatomic, retain) CustomWebserviceRequest* saveNewCommentRequest;
@property (nonatomic, assign) id<DAODelegate> daoDelegate;

+(id) sharedInstance;

@property (nonatomic, retain) NSMutableDictionary* cache;

-(void) setupReachability;

-(void)resetDB;
-(BOOL)isDBExisted;

#pragma mark - Helper function
-(NSDictionary*) getSurveyMasterFromCacheWithID:(NSString*)surveyMasterID;

-(NSDictionary*) getLatestSurveyMasterFromCacheWithType:(NSString*)surveyType;

    
#pragma mark - Store method
-(NSArray*)getStoresByBrandCode:(NSString*)brandCode countryCode:(NSString*)countryCode  needOptionAll:(BOOL)needAll;

-(NSDictionary*)getSingleStoreById:(NSString*)storeId;

-(DAOBaseRequest*) selectRecentStoreReportsWithDelegate:(id<DAODelegate>)delegate storeId:(NSString*)storeId;


//-(DAORequest*) selectStoresWithDelegate:(id<DAODelegate>)delegate;

-(DAORequest*) selectStoreWithDelegate:(id<DAODelegate>)delegate storeId:(NSString*)sId;

#pragma mark - Knowledge & Document >> Product_SKU__c
-(DAOBaseRequest*) selectKnowledgeResourcesWithDelegate:(id<DAODelegate>)delegate brandCode:(NSString*)brandId;

#pragma mark - Broadcast Notice
-(DAORequest*) selectBroadcastNoticeWithDelegate:(id<DAODelegate>)delegate;

#pragma mark - Mail
-(CustomWebserviceRequest*) sendMailOfVisitationId:(id<DAODelegate>)delegate visitPlanId:(NSString*)visitPlanId email:(NSString*)email;


#pragma mark - Survey Master & Form
//one survet form
-(DAORequest*) selectSurveyFormWithDelegate:(id<DAODelegate>)delegate surveyId:(NSString*)sID;
//multi survey form
-(DAORequest*) selectSurveysFormWithDelegate:(id<DAODelegate>)delegate surveyType:(NSString*)type;
//survey masters
-(DAORequest*) selectSurveyTemplatesWithDelegate:(id<DAODelegate>)delegate surveyType:(NSString*)type;
//questions
-(DAORequest*) selectQuestionsWithDelegate:(id<DAODelegate>)delegate withMasterID:(NSString*)surveyMasterID;

#pragma mark - Visit Plan
-(DAORequest*) selectVisitationPlansWithDelegate:(id<DAODelegate>)delegate;
-(DAORequest*) selectSurveyWithDelegate:(id<DAODelegate>)delegate withId:(NSString*)surveyID;

#pragma mark - Event
-(DAORequest*) selectEventWithDelegate:(id<DAODelegate>)delegate;


#pragma mark - Stock On Hand
-(DAOBaseRequest*) selectStockOnHandWithDelegate:(id<DAODelegate>)delegate storeCode:(NSString*)code date:(NSString*)date;


#pragma mark - Inventory
-(DAOBaseRequest*) selectInventoryWithDelegate:(id<DAODelegate>)delegate storeCode:(NSString*)code;

#pragma mark - Marketing Items - SalesMixWeek
-(DAOBaseRequest*) selectSalesMixWithDelegate:(id<DAODelegate>)delegate storeCode:(NSString*)code year:(int)year week:(int)week;

#pragma mark - Stock and Sales
//-(DAOBaseRequest*) selectStockAndSalesWithDelegate:(id<DAODelegate>)delegate storeCode:(NSString*)code date:(NSString*)date;
-(DAOBaseRequest*) selectStockAndSalesWithDelegate:(id<DAODelegate>)delegate storeCode:(NSString*)code year:(int)year week:(int)week;


#pragma mark - Comments
-(DAORequest*) selectCommentsWithDelegate:(id<DAODelegate>)delegate withStoreID:(NSString*)storeId;

#pragma mark - Custom Request - AppInit
//-(DAOBaseRequest*) selectUserInfoDataWithDelegate:(id<DAODelegate>)delegate;

-(DAOBaseRequest*) selectAppInitDataWithDelegate:(id<DAODelegate>)delegate parameter:(NSDictionary*)data;
-(DAOBaseRequest*) selectBrandDetailDataWithDelegate:(id<DAODelegate>)delegate parameter:(NSDictionary*)data;
-(DAOBaseRequest*) selectCapacityWithDelegate:(id<DAODelegate>)delegate;
-(DAOBaseRequest*) selectTargetsWithDelegate:(id<DAODelegate>)delegate;
-(DAOBaseRequest*) selectSurveyMasterDataWithDelegate:(id<DAODelegate>)delegate;


#pragma mark - Custom Request - Store Chart
-(DAOBaseRequest*) selectStoreChartDataWithDelegate:(id<DAODelegate>)delegate storeId:(NSString*)sid;

#pragma mark - Custom Request - Usage Report
-(CustomWebserviceRequest*) selectUsageReportWithDelegate:(id<DAODelegate>)delegate;


#pragma mark - Special Handling for any submission request
-(DAOBaseRequest*) saveNewComment:(NSDictionary*)data delegate:(id<DAODelegate>)delegate;
-(CustomWebserviceRequest*) saveVisitPlanWithDelegate:(id<DAODelegate>)delegate visitPlan:(NSDictionary*)data;
-(DAOBaseRequest*) updateVisitPlanId:(NSString*)planId data:(NSDictionary*)data delegate:(id<DAODelegate>)delegate;
-(DAOBaseRequest*) updateStatusOfTask:(NSString*)taskId  parentId:(NSString*)parentId  status:(NSString*)status duration:(NSNumber*)minutes delegate:(id<DAODelegate>)delegate;
-(DAOBaseRequest*) updateStatusOfTask:(NSString*)taskId  parentId:(NSString*)parentId  status:(NSString*)status duration:(NSNumber*)minutes comment:(NSString*)comment delegate:(id<DAODelegate>)delegate;
-(DAOBaseRequest*) upsertSurveyForm:(NSDictionary*)surveyDict delegate:(id<DAODelegate>)delegate;

-(NSMutableDictionary*) convertSurveyObjectToParameter:(NSDictionary*)object;


#pragma mark - Offline function
-(NSArray*) loadDirtyRecordFromTable:(NSString*)tableName;
-(NSArray*) loadPendingRecordsFromTable:(NSString*)tableName;
-(void) removePendingRecordFromTable:(NSString*)tableName soupId:(NSString*)dbId;

#pragma mark - OFFLINE Function helper(Internal use only)
-(void) runStandardUpsertTable:(NSString*)tableName records:(NSArray*)records spec:(NSArray*)spec;
-(void) runStandardUpsertTable:(NSString*)tableName records:(NSArray*)records spec:(NSArray*)spec externalPath:(NSString*)externalPath;
-(void) runRealUpsertTable:(NSString*)tableName records:(NSArray*)records spec:(NSArray*)spec;

-(void) saveRecordAsPendingByUpsertTable:(NSString*)tableName records:(NSArray*)records spec:(NSArray*)spec;

-(NSDictionary*) loadRecordFromTable:(NSString*)tableName ofDBId:(NSString*)objId;

-(NSDictionary*) loadRecordFromTable:(NSString*)tableName ofId:(NSString*)objId;

@end
