//
//  VFDAO.m
//  VFStorePerformanceApp_Billy
//
//  Created by Developer 2 on 22/4/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "VFDAO.h"
#import "CustomWebserviceRestApi.h"
#import "SFDateUtil.h"
#import "ShareLocale.h"

#import "SFSmartStore.h"
#import "Constants.h"
#import "SFQuerySpec.h"


@implementation VFDAO


//singleton
+ (id)sharedInstance {
    static VFDAO *sharedMyInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyInstance = [[self alloc] init];
    });
    return sharedMyInstance;
}


- (id)init {
    if (self = [super init]) {
        self.cache = [[[NSMutableDictionary alloc] init] autorelease];
    }
    return self;
}

- (void)dealloc {
    [super dealloc];
}


-(void)resetDB{
    SFSmartStore* db = [SFSmartStore sharedStoreWithName:SMART_STORE_NAME];
    [db removeAllSoups];
    
    //TODO: Try remove downloaded files(images,pdf,xls,ppt or video) but fail.
    //All smartstore operation fail after calling NSfileManager
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSFileManager* manager = [NSFileManager defaultManager];
    NSArray *directoryContent = [manager contentsOfDirectoryAtPath:applicationDocumentsDir error:NULL];
    NSError* err = nil;
    for(NSString* fileName in directoryContent){
        if(fileName!=nil && ![fileName isEqualToString:@"stores"]){//TODO: where does this name come from?
            [manager removeItemAtPath:[NSString stringWithFormat:@"%@/%@", applicationDocumentsDir, fileName] error:&err];
            if(err!=nil) NSLog(@"Delete file fail. err=%@", err);
        }
    }
    

    
    
    NSLog(@"stop here");
    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    if ([paths count] > 0)
//    {
//        NSLog(@"Path: %@", [paths objectAtIndex:0]);
//        
//        NSError *error = nil;
//        NSFileManager *fileManager = [NSFileManager defaultManager];
//        
//        // Remove all files in the documents directory
//        BOOL deleted = [fileManager removeItemAtPath:[paths objectAtIndex:0] error:&error];
//        
//        if (deleted != YES || error != nil)
//        {
//            // Deal with the error...
//            NSLog(@"Delete document file fail %@", error);
//        }
//        else
//            // Recreate the Documents directory
//            [fileManager createDirectoryAtPath:[paths objectAtIndex:0] withIntermediateDirectories:NO attributes:nil error:&error];
//        
//    }

}


-(BOOL) isDBExisted{
    if(![[SFSmartStore sharedStoreWithName:SMART_STORE_NAME ]soupExists:DB_TABLE_USERINFO ]){
        return NO;
    }
    if(![[SFSmartStore sharedStoreWithName:SMART_STORE_NAME ]soupExists:DB_TABLE_STORE ]){
        return NO;
    }
    if(![[SFSmartStore sharedStoreWithName:SMART_STORE_NAME ]soupExists:DB_TABLE_STORE_ANALYSIS ]){
        return NO;
    }
    if(![[SFSmartStore sharedStoreWithName:SMART_STORE_NAME ]soupExists:DB_TABLE_INVENTORY ]){
        return NO;
    }
    if(![[SFSmartStore sharedStoreWithName:SMART_STORE_NAME ]soupExists:DB_TABLE_KNOWLEDGE ]){
        return NO;
    }
    if(![[SFSmartStore sharedStoreWithName:SMART_STORE_NAME ]soupExists:DB_TABLE_USAGE ]){
        return NO;
    }
    if(![[SFSmartStore sharedStoreWithName:SMART_STORE_NAME ]soupExists:DB_TABLE_NOTIFICATION ]){
        return NO;
    }
    if(![[SFSmartStore sharedStoreWithName:SMART_STORE_NAME ]soupExists:DB_TABLE_SALES_MIX ]){
        return NO;
    }
    if(![[SFSmartStore sharedStoreWithName:SMART_STORE_NAME ]soupExists:DB_TABLE_STOCK_SALES ]){
        return NO;
    }
    if(![[SFSmartStore sharedStoreWithName:SMART_STORE_NAME ]soupExists:DB_TABLE_SURVEY_FORM ]){
        return NO;
    }
    if(![[SFSmartStore sharedStoreWithName:SMART_STORE_NAME ]soupExists:DB_TABLE_SURVEY_MASTER ]){
        return NO;
    }
    if(![[SFSmartStore sharedStoreWithName:SMART_STORE_NAME ]soupExists:DB_TABLE_SURVEY_MASTER_QUESTION ]){
        return NO;
    }
    if(![[SFSmartStore sharedStoreWithName:SMART_STORE_NAME ]soupExists:DB_TABLE_TASK ]){
        return NO;
    }
    if(![[SFSmartStore sharedStoreWithName:SMART_STORE_NAME ]soupExists:DB_TABLE_VISIT_PLAN ]){
        return NO;
    }
//    if(![[SFSmartStore sharedStoreWithName:SMART_STORE_NAME ]soupExists:DB_TABLE_EVENT ]){
//        return NO;
//    }
//    return YES;
}


-(void) setupReachability{
    [SFNetworkEngine sharedInstance].reachabilityChangedHandler = ^(SFNetworkStatus ns){
        if(SFNotReachable == ns){
            self.isNetworkReachable = NO;
        }else{
            self.isNetworkReachable = YES;
        }
        NSLog(@"self.isNetworkReachable now is %@", self.isNetworkReachable?@"ONLINE":@"OFFLINE");
    };
}


#pragma mark - Helper function
-(NSDictionary*) getSurveyMasterFromCacheWithID:(NSString*)surveyMasterID{
    NSArray * surveyMasterArr = [self.cache objectForKey:@"survey_master"];
    for(NSDictionary* master in surveyMasterArr){
        if([[master objectForKey:@"Id"] isEqualToString:surveyMasterID]){
            return master;
        }
    }
    return nil;
}

-(NSDictionary*) getLatestSurveyMasterFromCacheWithType:(NSString*)surveyType {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString* countryCode = [defaults objectForKey:USER_PREFER_COUNTRY_CODE]; //SG|MY|JP|TW|HK|CN|KR
    NSString* brandCode = [defaults objectForKey:USER_PREFER_ACCOUNT_CODE]; //TBL|VAN|KPL|LEE|TNF|WGR|NAP
    
    NSArray * surveyMasterArr = [self.cache objectForKey:@"survey_master"];
    NSMutableArray* fitTypeMasters = [NSMutableArray array];
    for(NSDictionary* master in surveyMasterArr){
        if([[master objectForKey:@"emfa__Type__c"] isEqualToString:surveyType] && [[master objectForKey:@"emfa__Status__c"] isEqualToString:@"Published"]){
            //Bug Fix at by Billy
            NSString* target = master[@"Target_Group__c"];//KPL|LEE|NAPA|TBL|TNF|VANS|WGR and HK|TW|CN|JP|KR|MY|SG
            
            if ( [target rangeOfString:brandCode].location != NSNotFound && [target rangeOfString:countryCode].location != NSNotFound) {
                [fitTypeMasters  addObject:master];
            }
        }
    }
    return [fitTypeMasters lastObject];//from SFDC SOQL, it is already sorted by LastModifiedDate ASC, so the last one is the latest published Survey Master
}

#pragma mark - Store

//Get a list for store filtering/searching
-(NSArray*)getStoresByBrandCode:(NSString*)brandCode countryCode:(NSString*)countryCode  needOptionAll:(BOOL)needAll{
    NSMutableArray* allStores = [NSMutableArray array];
    for(NSDictionary* store in self.cache[@"store_search_list"]){
        if(store[@"account_code"]!=(id)[NSNull null] &&  [store[@"account_code"] isEqualToString:brandCode]
          && store[@"country_code"]!=(id)[NSNull null] &&  [store[@"country_code"] isEqualToString:countryCode]
        ){
            NSMutableDictionary* cloneStore = [NSMutableDictionary dictionaryWithDictionary:store];
            cloneStore[@"Name"] = [NSString stringWithFormat:@"%@ %@", store[@"emfa__Store_Code__c"], store[@"Name"] ];
            [allStores addObject: cloneStore];
        }else{
            //NSLog(@"getStoresByBrandCode not match [%@ %@] ~ [%@ %@]", store[@"account_code"], store[@"country_code"], brandCode, countryCode);
        }
    }
    if(needAll) {
        [allStores insertObject:@{@"Id":@"", @"Name":[ShareLocale textFromKey:@"all"], @"emfa__Store_Code__c":@""}  atIndex:0];
    }
    return allStores;
}


-(NSDictionary*)getSingleStoreById:(NSString*)storeId{
    for(NSDictionary* store in self.cache[@"store_search_list"]){
        if(store[@"Id"]!=(id)[NSNull null] &&  [store[@"Id"] isEqualToString:storeId]){
            NSMutableDictionary* cloneStore = [NSMutableDictionary dictionaryWithDictionary:store];
            cloneStore[@"Name"] = [NSString stringWithFormat:@"%@ %@", store[@"Store_Code__c"], store[@"Name"] ];
            return cloneStore;
        }
    }
    return nil;
}




//OFFLINE DONE
//Get one store info
-(DAOBaseRequest*) selectStoreWithDelegate:(id<DAODelegate>)delegate storeId:(NSString*)sId{
    NSLog(@"VFDAO::selectStoreWithDelegate()");
    if(_isOnlineMode){
        NSString* sql = [NSString stringWithFormat:@"SELECT Id, Name, Store_Name_CN__c, emfa__Account__c, Store_Type__c, \
                         emfa__Address__c, Brand_Code__c, System_Code__c, POS_Store_Code__c, Store_Code__c, Country__c, \
                            emfa__Email__c, emfa__Fax__c, emfa__Phone__c, emfa__Primary_Latitude__c, City__c, Address_CN__c, \
                            emfa__Primary_Longitude__c, Opening_Date__c, Closure_Date__c, Comp_Status__c, Key_Store__c, \
                            Profile__c, Region__c, Retail_AE_ID__c, Retail_AE_Name__c, Size_Net__c, Sales_Person__c, \
                            (SELECT Id, Name, Department, Description, Email, Fax, HomePhone, MobilePhone, \
                                Phone, Title FROM Contacts__r) \
                         FROM emfa__Store__c \
                         WHERE Id = '%@' ", sId];
                         //(Select Id, Name, Subject__c, Description__c, ActivityDate__c, Store_Name__c FROM Comments__r ORDER BY ActivityDate__c DESC) \
                         //(Select Id, Subject, Description, ActivityDate, Status from Tasks WHERE ActivityDate!=null Order By CreatedDate desc)
        return [self queryOnlineDataBySQL:sql delegate:delegate postDelegate:^(NSArray *records) {
            [self runStandardUpsertTable: DB_TABLE_STORE records:records spec: @[
                                                                                      @{@"path":@"Id",
                                                                                        @"type":@"string"}
                                                                                      ,@{@"path":@"Store_Code__c",
                                                                                         @"type":@"string"}
                                                                                      ,@{@"path":@"emfa__Active__c",
                                                                                         @"type":@"boolean"}
                                                                                      ] ];
        }];
    }else{
        CustomWebserviceRequest* req = [[[CustomWebserviceRequest alloc] init] autorelease];
        [self runStandardSelectFromTable:DB_TABLE_STORE request:req delegate:delegate queryDict:@{   @"queryType":@"exact", @"matchKey":sId, @"indexPath":@"Id", @"order":@"descending", @"pageSize":[NSNumber numberWithInt:5000]}];
        return req;
    }
}


#pragma mark - Knowledge & Resources
//OFFLINE DONE
-(DAOBaseRequest*) selectKnowledgeResourcesWithDelegate:(id<DAODelegate>)delegate brandCode:(NSString*)brandCode{
    NSLog(@"VFDAO::selectKnowledgeResourcesWithDelegate()");
    if(_isOnlineMode){
        NSString* sql = [NSString stringWithFormat: @"SELECT Id, Name, CreatedDate, Category__c, Brand_Code__c, Target_Group__c, (SELECT Id, emfa__FeedItemId__c, emfa__File_Name__c, emfa__File_Ext__c FROM Shared_Media__r) FROM emfa__Product_SKU__c WHERE Target_Group__c like '%%%@%%' ORDER BY CreatedDate DESC", brandCode];
        return [self queryOnlineDataBySQL:sql delegate:delegate postDelegate:^(NSArray *records) {
            [self runStandardUpsertTable:DB_TABLE_KNOWLEDGE records:records spec: @[ @{@"path":@"Id", @"type":@"string"},@{@"path":@"Target_Group__c", @"type":@"string"}, @{@"path":@"CreatedDate", @"type":@"string"}]];

        }];
    }
    else{
        CustomWebserviceRequest* req = [[[CustomWebserviceRequest alloc] init] autorelease];
        
        NSString* searchPhase = [NSString stringWithFormat:@"%%%@%%", brandCode];
        
        [self runStandardSelectFromTable:DB_TABLE_KNOWLEDGE request:req delegate:delegate queryDict:@{   @"queryType":@"like", @"likeKey":searchPhase, @"indexPath":@"Target_Group__c", @"order":@"descending", @"pageSize":[NSNumber numberWithInt:5000]}];
        return req;
            
    }
}

#pragma mark - Broadcast
//OFFLINE DONE
-(DAOBaseRequest*) selectBroadcastNoticeWithDelegate:(id<DAODelegate>)delegate{
    NSLog(@"VFDAO::selectBroadcastNoticeWithDelegate()");
    if(_isOnlineMode){

        NSString* sql = @"SELECT Id, Name, emfa__Description__c, emfa__Published_Start_Date__c, emfa__Category__c, LastModifiedDate FROM emfa__Notification__c WHERE emfa__Published_Start_Date__c <= TODAY ORDER BY emfa__Published_Start_Date__c DESC";
        return [self queryOnlineDataBySQL:sql delegate:delegate postDelegate:^(NSArray *records) {
            
            SFSmartStore* db = [SFSmartStore sharedStoreWithName:SMART_STORE_NAME];
            if(![db soupExists:DB_TABLE_NOTIFICATION]){
                NSLog(@"AppDAO>> SmartStore: Table DB_TABLE_NOTIFICATION does not exist. Create it now...");
                BOOL success = [db registerSoup:DB_TABLE_NOTIFICATION withIndexSpecs:
                                @[
                                  @{@"path":@"Id",
                                    @"type":@"string"}
                                  ,
                                  @{@"path":@"Published_Date__c",
                                    @"type":@"date"}
                                  ]];
                NSLog(@"SmartStore: SOUP %@ created %@", DB_TABLE_NOTIFICATION, success?@"success":@"fail");                if(!success){
                    NSLog(@"VFDAO>> Skip upsert into DB.");
                    return;
                }
            }
            NSLog(@"AppDAO>> SmartStore: Upsert new DB_TABLE_NOTIFICATION records...");
            NSError* error = nil;
            [db upsertEntries:records toSoup:DB_TABLE_NOTIFICATION withExternalIdPath:@"Id" error:&error];
            
        }];
    }
    else{
        DAOBaseRequest* request = [[DAOBaseRequest alloc] init];
        
        SFSmartStore* db = [SFSmartStore sharedStoreWithName:SMART_STORE_NAME];
        if([db soupExists:DB_TABLE_NOTIFICATION]){
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NSLog(@"selectBroadcastNoticeWithDelegate: Load records from local table DB_TABLE_NOTIFICATION");
                
                NSDictionary* queryDict =  @{@"queryType":@"range",
                                             @"indexPath":@"Id",
                                             @"pageSize":[NSNumber numberWithInt:10000]};
                SFQuerySpec *querySpec = [[[SFQuerySpec alloc] initWithDictionary:queryDict
                                                                    withSoupName:DB_TABLE_NOTIFICATION] autorelease];
                NSArray *dbRecords = [db queryWithQuerySpec:querySpec pageIndex:0];
                NSLog(@"Process done. Local record count = %d", [dbRecords count]);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [delegate daoRequest:request queryOnlineDataSuccess:dbRecords];
                });
            });
        }
        else{
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [delegate daoRequest:request queryOnlineDataSuccess:@[]];
                });
            });
        }
        
        return request;
    }
}


#pragma mark - SurveyForm
//OFFLINE DONE
-(DAOBaseRequest*) selectSurveyFormWithDelegate:(id<DAODelegate>)delegate surveyId:(NSString*)sID{
    NSLog(@"VFDAO::selectSurveyFormWithDelegate()");
    if(_isOnlineMode){
        NSString* sql = [NSString stringWithFormat: @"SELECT Id, Name, emfa__Survey_Master2__c, CreatedDate, CreatedBy.Name, emfa__Store__r.Name, Visitation_Plan_Name__c, emfa__Visitation_Plan__c, Total_Score__c, Maximum_Score__c, (SELECT Id, Name, emfa__Answer_Master__c, emfa__Answer_Value__c , emfa__Comment__c, emfa__Question_Master__c, emfa__Survey2__c FROM emfa__Question_Answers__r) FROM emfa__Survey__c Where Id='%@' LIMIT 1", sID];
        return [self queryOnlineDataBySQL:sql delegate:delegate postDelegate:^(NSArray *records) {
            [self runStandardUpsertTable:DB_TABLE_SURVEY_FORM records:records spec: DB_SPEC_SURVEY_FORM];
        }];
    }else{
        CustomWebserviceRequest* req = [[[CustomWebserviceRequest alloc] init] autorelease];
        [self runStandardSelectFromTable:DB_TABLE_SURVEY_FORM request:req delegate:delegate queryDict:@{   @"queryType":@"exact", @"matchKey":sID, @"indexPath":@"Id", @"order":@"descending", @"pageSize":[NSNumber numberWithInt:1]}];
        return req;
    }
}


//OFFLINE DONE
-(DAOBaseRequest*) selectSurveyWithDelegate:(id<DAODelegate>)delegate withId:(NSString*)surveyId{
    //NSString* sql = [NSString stringWithFormat:@"SELECT Id, Name, emfa__Survey_Master__c, CreatedDate, CreatedBy.Name, Store__r.Name, Visitation_Plan_Name__c, (SELECT Id, Name, emfa__Answer_Master__c, emfa__Answer_Value__c , emfa__Comment__c, emfa__Question_Master__c, emfa__Survey__c FROM emfa__Question_Answer__r) FROM emfa__Survey__c Where Id='%@' LIMIT 1", surveyId];
    if(_isOnlineMode){
        
        NSString* sql = [NSString stringWithFormat: @"SELECT Id, Name, emfa__Survey_Master2__c, Total_Score__c, Maximum_Score__c, CreatedDate, CreatedBy.Name, PDF_Report_Attached__c, emfa__Store__r.Name, emfa__Visitation_Plan__c, Visitation_Plan_Name__c, Master_Type__c, (SELECT Id, Name, emfa__Answer_Master__c, emfa__Answer_Value__c , emfa__Comment__c, emfa__Question_Master__c, emfa__Survey2__c FROM emfa__Question_Answers__r), (SELECT Id, emfa__FeedItemId__c, emfa__File_Name__c, emfa__File_Ext__c FROM Shared_Media__r) FROM emfa__Survey__c Where Id='%@' LIMIT 1", surveyId];
        return [self queryOnlineDataBySQL:sql delegate:delegate postDelegate:^(NSArray *records) {
            [self runStandardUpsertTable:DB_TABLE_SURVEY_FORM records:records spec: DB_SPEC_SURVEY_FORM];
        }];
    }else{
        CustomWebserviceRequest* req = [[[CustomWebserviceRequest alloc] init] autorelease];
        [self runStandardSelectFromTable:DB_TABLE_SURVEY_FORM request:req delegate:delegate queryDict:@{   @"queryType":@"exact", @"matchKey":surveyId, @"indexPath":@"Id", @"order":@"descending", @"pageSize":[NSNumber numberWithInt:1]}];
        return req;
    }
}

//OFFLINE DONE
-(DAOBaseRequest*) selectSurveysFormWithDelegate:(id<DAODelegate>)delegate surveyType:(NSString*)type{
    if(_isOnlineMode){
        
        NSString* sql = [NSString stringWithFormat: @"SELECT Id, Name, Total_Score__c, Maximum_Score__c, Account__c, Account__r.Code__c, Account__r.Country_Code__c, emfa__Store__r.emfa__Account__r.Country_Code__c, emfa__Store__r.emfa__Account__r.Code__c, emfa__Survey_Master2__r.Total_Score__c, emfa__Survey_Master2__c, CreatedDate, CreatedBy.Name, PDF_Report_Attached__c, Store__r.Store_Code__c, Store__r.Name, Visitation_Plan_Name__c, Visitation_Plan__c, Visitor__c, Visit_Start_Date_Time__c, Master_Type__c, (SELECT Id, Name, emfa__Answer_Master__c, emfa__Answer_Value__c , emfa__Comment__c, emfa__Question_Master__c, emfa__Survey2__c FROM emfa__Question_Answers__r), (SELECT Id, emfa__FeedItemId__c, emfa__File_Name__c, emfa__File_Ext__c FROM Shared_Media__r) FROM emfa__Survey__c Where Master_Type__c='%@' ORDER BY CreatedDate Desc", type ];
        return [self queryOnlineDataBySQL:sql delegate:delegate postDelegate:^(NSArray *records) {
            [self runStandardUpsertTable:DB_TABLE_SURVEY_FORM records:records spec: DB_SPEC_SURVEY_FORM];
        }];
    }else{
        CustomWebserviceRequest* req = [[[CustomWebserviceRequest alloc] init] autorelease];
        
        [self runStandardSelectFromTable:DB_TABLE_SURVEY_FORM request:req delegate:delegate queryDict:@{   @"queryType":@"exact", @"matchKey":type, @"indexPath":@"Master_Type__c", @"order":@"descending", @"pageSize":[NSNumber numberWithInt:2000]}];
        return req;
    }
}

#pragma mark - Survey Master
//OFFLINE DONE
-(DAOBaseRequest*) selectSurveyTemplatesWithDelegate:(id<DAODelegate>)delegate surveyType:(NSString*)type{
    if(_isOnlineMode){
        NSString* sql = [NSString stringWithFormat: @"SELECT Id, Name, emfa__Type__c, emfa__Header__c, emfa__Footer__c, Target_Group__c, Number_Of_Question__c, Number_Of_Survey_Created__c, Question_Count__c, emfa__Status__c, CreatedDate, LastModifiedDate, Total_Score__c, (SELECT Id, Name, emfa__Description__c , emfa__Extra_Comment__c, emfa__Max_Number__c, emfa__Min_Number__c , emfa__Ordering__c , emfa__Required__c, emfa__Survey_Master2__c,  emfa__Title__c, RecordTypeId FROM emfa__Question_Masters__r ORDER BY emfa__Ordering__c ASC) FROM emfa__Survey_Master__c WHERE emfa__Type__c='%@' ORDER BY CreatedDate ASC", type];
        return [self queryOnlineDataBySQL:sql delegate:delegate postDelegate:^(NSArray *records) {
            [self runStandardUpsertTable:DB_TABLE_SURVEY_MASTER records:records spec: @[
                                                                                        @{@"path":@"Id",
                                                                                          @"type":@"string"}
                                                                                        ,@{@"path":@"emfa__Ordering__c",
                                                                                           @"type":@"integer"}
                                                                                        ,@{@"path":@"emfa__Type__c", @"type":@"string"}
                                                                                        ,@{@"path":@"CreatedDate", @"type":@"string"}
                                                                                        ]];
        }];
    }else{
        CustomWebserviceRequest* req = [[[CustomWebserviceRequest alloc] init] autorelease];
        [self runStandardSelectFromTable:DB_TABLE_SURVEY_MASTER request:req delegate:delegate queryDict:@{   @"queryType":@"exact", @"indexPath":@"Type__c", @"order":@"descending", @"matchKey":type, @"pageSize":[NSNumber numberWithInt:500]}];
        return req;
    }
}

//OFFLINE DONE
-(DAOBaseRequest*) selectQuestionsWithDelegate:(id<DAODelegate>)delegate withMasterID:(NSString*)surveyMasterID{
    if(_isOnlineMode){
        NSString* sql = [NSString stringWithFormat:@"SELECT Id, Name, emfa__Description__c, emfa__Extra_Comment__c, emfa__Ordering__c, emfa__Survey_Master2__c, emfa__Title__c, RecordType.Name FROM emfa__Question_Master__c Where emfa__Survey_Master2__c='%@' ORDER BY emfa__Ordering__c ASC", surveyMasterID];
        return [self queryOnlineDataBySQL:sql delegate:delegate postDelegate:^(NSArray *records) {
            [self runStandardUpsertTable:DB_TABLE_SURVEY_MASTER_QUESTION records:records spec: @[ @{@"path":@"Id", @"type":@"string"}, @{@"path":@"emfa__Ordering__c", @"type":@"integer"}, @{@"path":@"emfa__Survey_Master2__c", @"type":@"string"}]];
        }];
    }
    else{
        CustomWebserviceRequest* req = [[[CustomWebserviceRequest alloc] init] autorelease];
        [self runStandardSelectFromTable:DB_TABLE_SURVEY_MASTER_QUESTION request:req delegate:delegate queryDict:@{   @"queryType":@"exact", @"indexPath":@"emfa__Survey_Master__c", @"order":@"descending", @"matchKey":surveyMasterID, @"pageSize":[NSNumber numberWithInt:500]}];
        return req;
    }
}


#pragma mark - Event
-(DAOBaseRequest*) selectEventWithDelegate:(id<DAODelegate>)delegate{
    if(_isOnlineMode){
        NSString* sql = [NSString stringWithFormat: @"SELECT Id, Subject, ActivityDate, ActivityDateTime, DurationInMinutes, StartDateTime, EndDateTime FROM Event WHERE From_Visit_Plan__c = FALSE ORDER BY StartDateTime DESC LIMIT 800"];
        return [self queryOnlineDataBySQL:sql delegate:delegate postDelegate:^(NSArray *records) {
            [self runStandardUpsertTable:DB_TABLE_EVENT records:records spec: DB_SPEC_EVENT ];
        }];
    }
    else{
        CustomWebserviceRequest* req = [[[CustomWebserviceRequest alloc] init] autorelease];
        [self runStandardSelectFromTable: DB_TABLE_EVENT request:req delegate:delegate queryDict:@{   @"queryType":@"range", @"indexPath":@"StartDateTime", @"order":@"descending", @"pageSize":[NSNumber numberWithInt:2000]}];
        return req;
        
    }
}


#pragma mark - Visit Plan

//OFFLINE DONE
-(DAOBaseRequest*) selectRecentStoreReportsWithDelegate:(id<DAODelegate>)delegate storeId:(NSString*)storeId{
    if(_isOnlineMode){
        NSString* sql = [NSString stringWithFormat: @"SELECT Id, Name, emfa__Start_Time__c, \
                         (Select Id, Total_Score__c, Maximum_Score__c, Type__c, Master_Type__c from emfa__Surveys__r) \
                         FROM emfa__Visitation_Plan__c WHERE emfa__Active__c='TRUE' AND emfa__Check_Out_Time__c!=null AND emfa__Store__c='%@' ORDER BY emfa__Start_Time__c DESC LIMIT 12", storeId];
        return [self queryOnlineDataBySQL:sql delegate:delegate postDelegate:^(NSArray *records) {
            [self runStandardUpsertTable:DB_TABLE_STORE_REPORT records:records spec: DB_SPEC_STORE_REPORT ];
        }];
    }
    else{
        NSString* sql = [NSString stringWithFormat:@"SELECT * FROM {%@} WHERE {%@:Store__c} = '%@'",  DB_TABLE_STORE_REPORT, DB_TABLE_STORE_REPORT, storeId];
        CustomWebserviceRequest* req = [[[CustomWebserviceRequest alloc] init] autorelease];
        [self runStandardSmartSqlFromTable:DB_TABLE_STORE_REPORT request:req delegate:delegate queryDict:@{   @"queryType":@"smart", @"smartSql":sql, @"order":@"descending", @"pageSize":[NSNumber numberWithInt:12]}];
        return req;

        
    }
    
}



//OFFLINE DONE
-(DAOBaseRequest*) selectVisitationPlansWithDelegate:(id<DAODelegate>)delegate{
    if(_isOnlineMode){
        NSString* sql = [NSString stringWithFormat: @"SELECT Id, Name, Actual_Duration__c, emfa__Store__r.emfa__Store_Code__c, emfa__Store__r.emfa__Account__r.Country_Code__c, emfa__Store__r.emfa__Account__r.Code__c, emfa__Store__r.Name, emfa__Visit_Date__c, emfa__Active__c, CreatedBy.Name, emfa__Store__c, emfa__Check_In_Time__c, emfa__Check_Out_Time__c, emfa__End_Time__c, emfa__Remark__c, Planned_Duration__c, emfa__Start_Time__c, To_Do_List__c, Document_Checklist__c, emfa__Visit_With__c, Comment__c, Need_Visit_Form__c, Need_MSP__c, Need_SOP_Audit__c, \
                         (Select Id, Type__c from emfa__Surveys__r), \
                         (select Id, Subject, RecordType.Name, Status, emfa__Actual_Duration__c, emfa__Estimated_Duration__c, emfa__Category__c, Comment__c from Tasks) \
            FROM emfa__Visitation_Plan__c ORDER BY emfa__Start_Time__c DESC LIMIT 800"];
        return [self queryOnlineDataBySQL:sql delegate:delegate postDelegate:^(NSArray *records) {
            [self runStandardUpsertTable:DB_TABLE_VISIT_PLAN records:records spec: DB_SPEC_VISIT_PLAN ];
        }];
    }
    else{
        CustomWebserviceRequest* req = [[[CustomWebserviceRequest alloc] init] autorelease];
        
        //
        //instead of loading record from local DB directly, we combined the data from tables DB_TABLE_VISIT_PLAN and TABLE_PENDING_VISIT_PLAN
        //
        
        //[self runStandardSelectFromTable: DB_TABLE_VISIT_PLAN request:req delegate:delegate queryDict:@{   @"queryType":@"range", @"indexPath":@"Planned_Start_Date_Time__c", @"order":@"descending", @"pageSize":[NSNumber numberWithInt:2000]}];
        
        SFSmartStore* db = [SFSmartStore sharedStoreWithName:SMART_STORE_NAME];
        if([db soupExists:DB_TABLE_VISIT_PLAN]){
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NSLog(@"selectVisitationPlansWithDelegate: Load records from local table %@", DB_TABLE_VISIT_PLAN);
                
                SFQuerySpec *querySpec = [[[SFQuerySpec alloc] initWithDictionary:@{   @"queryType":@"range", @"indexPath":@"emfa__Start_Time__c", @"order":@"descending", @"pageSize":[NSNumber numberWithInt:2000]}
                                                                     withSoupName:DB_TABLE_VISIT_PLAN] autorelease];
                NSArray *dbRecords = [db queryWithQuerySpec:querySpec pageIndex:0];
                NSArray* offlineUnsavedRecords = [self loadPendingRecordsFromTable:TABLE_PENDING_VISIT_PLAN];
                NSMutableArray* mdbRecords = [dbRecords mutableCopy];
                for(NSDictionary* offlineRecord in offlineUnsavedRecords){
                    [mdbRecords addObject:offlineRecord[DB_DATA]];
                }
                dbRecords = [mdbRecords copy];
                mdbRecords = nil;
                NSLog(@"Process done. Local record count = %d", [dbRecords count]);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [delegate daoRequest:req queryOnlineDataSuccess:dbRecords];
                });
            });
        }
        else{
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [delegate daoRequest:req queryOnlineDataSuccess:@[]];
                });
            });
        }
        
        return req;
        
    }

}




#pragma mark - Load Stock On Hand
//OFFLINE_DONE
-(DAOBaseRequest*) selectStockOnHandWithDelegate:(id<DAODelegate>)delegate storeCode:(NSString*)code date:(NSString*)date{
    NSString* sql;
    NSLog(@"selectStockOnHandWithDelegate");
    NSDateFormatter* df = [[[NSDateFormatter alloc] init] autorelease];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSString *dateInString = [SFDateUtil toSOQLDateTimeString:[df dateFromString: date ] isDateTime:false];
    
    if(_isOnlineMode){
        sql = [NSString stringWithFormat: @"SELECT Id, Amount_On_Hand__c, Category__c, Cut_To_Day__c, Gender__c, Quantity_On_Hand__c, SKU_Quantity__c, Store_Code__c FROM Stock_On_Hand__c WHERE Store_Code__c = '%@' AND Cut_To_Day__c = %@", code, dateInString];
        return [self queryOnlineDataBySQL:sql delegate:delegate postDelegate:^(NSArray *records) {
            [self runStandardUpsertTable:DB_TABLE_INVENTORY records:records spec: @[ @{@"path":@"Cut_To_Day__c", @"type":@"string"},@{@"path":@"Id", @"type":@"string"}, @{@"path":@"Store_Code__c", @"type":@"string"}, @{ @"path":@"Financial_Year__c", @"type":@"integer"}, @{@"path":@"Financial_Week__c",@"type":@"integer"}, @{@"path":@"Cut_To_Day__c",@"type":@"string"} ]];
        }];
    }
    else{
        NSString* sql = [NSString stringWithFormat:@"SELECT * FROM {%@} WHERE {%@:Store_Code__c} = '%@' AND {%@:Cut_To_Day__c}=%@",  DB_TABLE_INVENTORY, DB_TABLE_INVENTORY, code, DB_TABLE_INVENTORY, dateInString];
        CustomWebserviceRequest* req = [[[CustomWebserviceRequest alloc] init] autorelease];
        [self runStandardSmartSqlFromTable:DB_TABLE_STOCK_SALES request:req delegate:delegate queryDict:@{   @"queryType":@"smart", @"smartSql":sql, @"order":@"descending", @"pageSize":[NSNumber numberWithInt:3000]}];
        return req;
    }
}



#pragma mark - Load Sales mix to find % of marketing items
//OFFLINE_DONE
-(DAOBaseRequest*) selectSalesMixWithDelegate:(id<DAODelegate>)delegate storeCode:(NSString*)code year:(int)year week:(int)week{
    
    NSLog(@"selectSalesMixWithDelegate");
    if(_isOnlineMode){
        NSString* sql = [NSString stringWithFormat: @"SELECT Id, Category__c, Store_Code__c, OPS_Sales_Amount__c, Financial_Week__c, Financial_Year__c, Gender__c FROM Sales_Mix_Period__c WHERE Period__c='W' AND Store_Code__c = '%@' AND Financial_Week__c = %d AND Financial_Year__c = %d", code, week, year];
        return [self queryOnlineDataBySQL:sql delegate:delegate postDelegate:^(NSArray *records) {
            [self runStandardUpsertTable:DB_TABLE_SALES_MIX records:records spec: @[ @{@"path":@"Period__c", @"type":@"string"},@{@"path":@"Id", @"type":@"string"}, @{@"path":@"Store_Code__c", @"type":@"string"}, @{ @"path":@"Financial_Week__c", @"type":@"integer"}, @{@"path":@"Financial_Year__c",@"type":@"integer"} ]];
        }];
    }else{
        NSString* sql = [NSString stringWithFormat:@"SELECT * FROM {%@} WHERE {%@:Period__c} = 'W' AND {%@:Store_Code__c}=%@ AND {%@:Financial_Week__c}=%d AND {%@:Financial_Year__c=%d}",  DB_TABLE_SALES_MIX, DB_TABLE_SALES_MIX, DB_TABLE_SALES_MIX, code, DB_TABLE_SALES_MIX, week, DB_TABLE_SALES_MIX, year];
        CustomWebserviceRequest* req = [[[CustomWebserviceRequest alloc] init] autorelease];
        [self runStandardSmartSqlFromTable:DB_TABLE_SALES_MIX request:req delegate:delegate queryDict:@{   @"queryType":@"smart", @"smartSql":sql, @"order":@"descending", @"pageSize":[NSNumber numberWithInt:3000]}];
        return req;
    }
}

#pragma mark - Low Stock And Top Sales
//OFFLINE_DONE
-(DAOBaseRequest*) selectStockAndSalesWithDelegate:(id<DAODelegate>)delegate storeCode:(NSString*)code year:(int)year week:(int)week{

    NSLog(@"selectStockAndSalesWithDelegate");
    
    if(_isOnlineMode){
        NSString* sql = [NSString stringWithFormat: @"SELECT Id, Brand_Code__c, Cross_Sales_Amount__c, Cut_To_Day__c, Financial_Week__c, Financial_Month__c, Financial_Year__c, Generic_Article__c, Item_Description__c, Sales_Quantity__c, SBU__c, Gender__c, OPS_Sales_Amount__c, Stock_On_Hand__c, Store__c, Store_Code__c, Type__c, Period__c FROM Stock_and_Sales_Period__c WHERE Period__c='W' AND (Type__c = 'T' OR Type__c = 'L' OR Type__c = 'C') AND Store_Code__c = '%@' AND Financial_Year__c=%d AND Financial_Week__c=%d ORDER BY Cut_To_Day__c DESC LIMIT 300", code, year, week];
        
        return [self queryOnlineDataBySQL:sql delegate:delegate postDelegate:^(NSArray *records) {
            [self runStandardUpsertTable:DB_TABLE_STOCK_SALES records:records spec: @[ @{@"path":@"Cut_To_Day__c", @"type":@"string"},@{@"path":@"Id", @"type":@"string"}, @{@"path":@"Store_Code__c", @"type":@"string"}, @{ @"path":@"Financial_Year__c", @"type":@"integer"}, @{@"path":@"Financial_Week__c",@"type":@"integer"} ]];
        }];
    }
    else{
        //smartSql
        NSString* sql = [NSString stringWithFormat:@"SELECT * FROM {%@} WHERE {%@:Store_Code__c} = '%@' AND {%@:Financial_Year__c}=%d AND {%@:Financial_Week__c}=%d ",  DB_TABLE_STOCK_SALES,DB_TABLE_STOCK_SALES, code, DB_TABLE_STOCK_SALES, year, DB_TABLE_STOCK_SALES,  week];
        CustomWebserviceRequest* req = [[[CustomWebserviceRequest alloc] init] autorelease];
        [self runStandardSmartSqlFromTable:DB_TABLE_STOCK_SALES request:req delegate:delegate queryDict:@{   @"queryType":@"smart", @"smartSql":sql, @"order":@"descending", @"pageSize":[NSNumber numberWithInt:150]}];
        return req;
        
    }

}

#pragma mark - Inventory
//OFFLINE DONE, how to run exact and ordering in the same time!?
-(DAOBaseRequest*) selectInventoryWithDelegate:(id<DAODelegate>)delegate storeCode:(NSString*)code{
    
    NSLog(@"VFDAO::selectInventoryWithDelegate()");
    if(_isOnlineMode){
        NSString* sql = [NSString stringWithFormat: @"SELECT Id, Name, Brand_Code__c, CreatedDate, Category__c, Inventory_Class__c, Amount_On_Hand__c, Cut_To_Day__c, Gender__c, Quantity_On_Hand__c, Store__c, Store_Code__c, System_Code__c FROM Stock_On_Hand__c Where Store_Code__c='%@' AND Category__c!=null ORDER BY Cut_To_Day__c DESC LIMIT 30", code];
        return [self queryOnlineDataBySQL:sql delegate:delegate postDelegate:^(NSArray *records) {
            [self runStandardUpsertTable:DB_TABLE_INVENTORY records:records spec: @[ @{@"path":@"Cut_To_Day__c", @"type":@"string"},@{@"path":@"Id", @"type":@"string"}, @{@"path":@"Store_Code__c", @"type":@"string"}, @{ @"path":@"Financial_Year__c", @"type":@"integer"}, @{@"path":@"Financial_Week__c",@"type":@"integer"}, @{@"path":@"Cut_To_Day__c",@"type":@"string"} ]];
            }];
    }
    else{
        CustomWebserviceRequest* req = [[[CustomWebserviceRequest alloc] init] autorelease];
        [self runStandardSelectFromTable:DB_TABLE_INVENTORY request:req delegate:delegate queryDict:@{   @"queryType":@"exact", @"indexPath":@"Store_Code__c", @"matchKey":code, @"order":@"descending", @"pageSize":[NSNumber numberWithInt:30]}];
        return req;
        
    }
}




#pragma mark - Comments
//OFFLINE DONE
-(DAOBaseRequest*) selectCommentsWithDelegate:(id<DAODelegate>)delegate withStoreID:(NSString*)storeId{
    NSString* sql;
    if(storeId!=nil){
        if(_isOnlineMode){
            //@"SELECT Id, Subject, Description, ActivityDate, Status, CreatedBy.Name, WhatId, RecordTypeId FROM Task WHERE WhatId = '%@' ORDER BY CreatedDate DESC LIMIT 500"
            sql = [NSString stringWithFormat: @"SELECT Id, Subject__c, Description__c, ActivityDate__c, Creator__r.Name, Store__c, (SELECT Id, emfa__FeedItemId__c, emfa__File_Name__c, emfa__File_Ext__c FROM emfa__Shared_Media__r) FROM emfa__Comment__c WHERE Store__c = '%@' ORDER BY ActivityDate__c DESC LIMIT 2000", storeId];
            return [self queryOnlineDataBySQL:sql delegate:delegate postDelegate:^(NSArray *records) {
                [self runStandardUpsertTable:DB_TABLE_COMMENT records:records spec: DB_SPEC_COMMENT];
            }];
        }
        else{
            CustomWebserviceRequest* req = [[[CustomWebserviceRequest alloc] init] autorelease];

            [self runStandardSelectFromTable:DB_TABLE_COMMENT request:req delegate: delegate queryDict:@{   @"queryType":@"exact", @"indexPath":@"Store__c", @"matchKey": storeId, @"order":@"descending", @"pageSize":[NSNumber numberWithInt:2000]}];
            return req;
        } 
    }else{
        if(_isOnlineMode){
            //SELECT Id, Subject, Description, ActivityDate, Status, CreatedBy.Name, WhatId, RecordTypeId FROM Task WHERE RecordTypeId='%@'ORDER BY CreatedDate DESC LIMIT 2000
            sql = @"SELECT Id, Subject__c, Description__c, ActivityDate__c, Creator__r.Name, Store__c, (SELECT Id, emfa__FeedItemId__c, emfa__File_Name__c, emfa__File_Ext__c FROM emfa__Shared_Media__r) FROM emfa__Comment__c ORDER BY CreatedDate DESC";
            return [self queryOnlineDataBySQL:sql delegate:delegate postDelegate:^(NSArray *records) {
                [self runStandardUpsertTable:DB_TABLE_COMMENT records:records spec: DB_SPEC_COMMENT];
            }];
        }
        else{
            CustomWebserviceRequest* req = [[[CustomWebserviceRequest alloc] init] autorelease];
            NSString* matchKey = [self.cache objectForKey:@"record_type_comment"];
            if(matchKey==nil || matchKey==(id)[NSNull null]){
                matchKey = @"";
            }
            [self runStandardSelectFromTable:DB_TABLE_COMMENT request:req delegate:delegate queryDict:@{   @"queryType":@"range", @"indexPath":@"ActivityDate__c", @"matchKey": matchKey, @"order":@"descending", @"pageSize":[NSNumber numberWithInt:2000]}];
            return req;

        }
    }
}




#pragma mark - Mail
-(CustomWebserviceRequest*) sendMailOfVisitationId:(id<DAODelegate>)delegate visitPlanId:(NSString*)visitPlanId email:(NSString*)email{
    NSLog(@"VFDAO sendMailOfVisitationId");
    NSAssert(self.customRequest==nil, @"sendMailOfVisitationId Error! Last request(%@) is still running", self.customRequest.name);
    
    if(_isOnlineMode){
        self.customRequest = [[CustomWebserviceRestApi sharedInstance] sendRequestASyncWithApiName:REST_API_VISIT_EMAIL_SEND data:@{@"visitPlanId":visitPlanId, @"emails":email} method:@"POST" delegate:self postBlock:^(NSArray *records) {
            
            
        }];
        self.daoDelegate = delegate;
        return self.customRequest;
    }else{
        NSAssert(NO, @"Mailing function is not work in offline mode");
        return nil;
    }
}



#pragma mark - Report Data
//OFFLINE DONE
-(DAOBaseRequest*) selectStoreChartDataWithDelegate:(id<DAODelegate>)delegate storeId:(NSString*)sid{
    NSLog(@"VFDAO selectStoreChartDataWithDelegate");
    NSAssert(self.customRequest==nil, @"selectStoreChartDataWithDelegate Error! Last request(%@) is still running", self.customRequest.name);
    
    
    if(_isOnlineMode){
        self.customRequest = [[CustomWebserviceRestApi sharedInstance] sendRequestASyncWithApiName:REST_API_CHARTDATE_GET data:@{@"store_id":sid, @"lang":[[ShareLocale sharedLocalSystem] getLanguage] } method:@"POST" delegate:self postBlock:^(NSArray *records) {
            
            SFSmartStore* db = [SFSmartStore sharedStoreWithName:SMART_STORE_NAME];
            if(![db soupExists:DB_TABLE_STORE_ANALYSIS]){
                NSLog(@"SmartStore: Table DB_TABLE_STORE_ANALYSIS does not exist. Create it now...");
                BOOL success = [db registerSoup:DB_TABLE_STORE_ANALYSIS withIndexSpecs:
                                @[
                                  @{@"path":@"storeCode",
                                    @"type":@"string"}
                                  
                                  ]];
                NSLog(@"SmartStore: SOUP %@ created %@", DB_TABLE_STORE_ANALYSIS, success?@"success":@"fail");
            }
            NSLog(@"SmartStore: Upsert DB_TABLE_STORE_ANALYSIS records...");
            NSError* error = nil;
            [db upsertEntries:records toSoup:DB_TABLE_STORE_ANALYSIS withExternalIdPath:@"storeCode" error:&error];
            if(error!=nil){
                NSLog(@"Upsert error %@", error);
            }
            
        }];
        self.daoDelegate = delegate;
        return self.customRequest;
    }
    else{
        CustomWebserviceRequest* req = [[[CustomWebserviceRequest alloc] init] autorelease];

        SFSmartStore* db = [SFSmartStore sharedStoreWithName:SMART_STORE_NAME];
        if([db soupExists:DB_TABLE_STORE_ANALYSIS]){
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NSLog(@"selectStoreChartDataWithDelegate: Load records from local table %@", DB_TABLE_STORE_ANALYSIS);
                
                NSDictionary* queryDict =  @{@"queryType":@"exact",
                                             @"indexPath":@"storeCode",
                                             @"matchKey": sid,
                                             @"pageSize":[NSNumber numberWithInt:1]};
                SFQuerySpec *querySpec = [[[SFQuerySpec alloc] initWithDictionary:queryDict
                                                                    withSoupName:DB_TABLE_STORE_ANALYSIS] autorelease];
                NSArray *dbRecords = [db queryWithQuerySpec:querySpec pageIndex:0];
                NSLog(@"Process done. DB_TABLE_STORE_ANALYSIS Local record count = %d", [dbRecords count]);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [delegate daoRequest:req queryOnlineDataSuccess:dbRecords];
                });
            });
        }else{
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [delegate daoRequest:req queryOnlineDataSuccess:@[]];
                });
            });
        }
        
        return req;
    }
}



#pragma mark - Usage Report
//OFFLINE DONE
-(CustomWebserviceRequest*) selectUsageReportWithDelegate:(id<DAODelegate>)delegate{
    NSLog(@"VFDAO selectUsageReportWithDelegate");
    NSAssert(self.customRequest==nil, @"selectUsageReportWithDelegate Error! Last request(%@) is still running", self.customRequest.name);
    if(_isOnlineMode){
        self.customRequest = [[CustomWebserviceRestApi sharedInstance] sendRequestASyncWithApiName:REST_API_USAGE_REPORT data:@{}  method:@"GET" delegate:self postBlock:^(NSArray *records) {
            
            SFSmartStore* db = [SFSmartStore sharedStoreWithName:SMART_STORE_NAME];
            if(![db soupExists:DB_TABLE_USAGE]){
                NSLog(@"SmartStore: Table DB_TABLE_USAGE does not exist. Create it now...");
                BOOL success = [db registerSoup:DB_TABLE_USAGE withIndexSpecs:
                                @[
                                  @{@"path":@"userId",
                                    @"type":@"string"}
                                  
                                  ]];
                NSLog(@"SmartStore: SOUP %@ created %@", DB_TABLE_USAGE, success?@"success":@"fail");
            }
            NSLog(@"SmartStore: Upsert DB_TABLE_STORE_USAGE records...");
            NSError* error = nil;
            [db upsertEntries:records toSoup:DB_TABLE_USAGE withExternalIdPath:@"userId" error:&error];
            if(error!=nil){
                NSLog(@"Upsert error %@", error);
            }
            
        }];
        self.daoDelegate = delegate;
        return self.customRequest;
    }
    else{
        CustomWebserviceRequest* req = [[[CustomWebserviceRequest alloc] init] autorelease];
        
        SFSmartStore* db = [SFSmartStore sharedStoreWithName:SMART_STORE_NAME];
        if([db soupExists:DB_TABLE_USAGE]){
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NSLog(@"selectUsageReportWithDelegate: Load records from local table %@", DB_TABLE_USAGE);
                
                NSDictionary* queryDict =  @{@"queryType":@"range",
                                             @"indexPath":@"userId",
                                             @"pageSize":[NSNumber numberWithInt:1]};
                SFQuerySpec *querySpec = [[[SFQuerySpec alloc] initWithDictionary:queryDict
                                                                     withSoupName:DB_TABLE_USAGE] autorelease];
                NSArray *dbRecords = [db queryWithQuerySpec:querySpec pageIndex:0];
                NSLog(@"Process done. DB_TABLE_USAGE Local record count = %d", [dbRecords count]);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [delegate daoRequest:req queryOnlineDataSuccess:dbRecords];
                });
            });
        }else{
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [delegate daoRequest:req queryOnlineDataSuccess:@[]];
                });
            });
        }
        
        return req;
    }
}


#pragma mark - App Init & Survey master query (Replace original UserInfo)
-(DAOBaseRequest*) selectCapacityWithDelegate:(id<DAODelegate>)delegate{
    if(_isOnlineMode){
        NSString* sql = [NSString stringWithFormat:@"Select Id, Name, System_Code__c, Capacity__c, Capacity2__c, Category__c, Store__c, Store_Code__c, Brand_Code__c FROM Store_Capacity__c WHERE Category__c!=null ORDER BY Brand_Code__c ASC, Store_Code__c ASC"];
        return [self queryOnlineDataBySQL:sql delegate:delegate postDelegate:^(NSArray *records) {
            [self runStandardUpsertTable: DB_TABLE_CAPACITY records:records spec: @[
                                                                                 @{@"path":@"Id",
                                                                                   @"type":@"string"}
                                                                                 ,@{@"path":@"Brand_Code__c",
                                                                                    @"type":@"string"}
                                                                                 ,@{@"path":@"Store_Code__c",
                                                                                    @"type":@"string"}
                                                                                 ] ];
        }];
    }else{
        CustomWebserviceRequest* req = [[[CustomWebserviceRequest alloc] init] autorelease];
        [self runStandardSelectFromTable:DB_TABLE_CAPACITY request:req delegate:delegate queryDict:@{   @"queryType":@"range", @"indexPath":@"Brand_Code__c", @"order":@"ascending", @"pageSize":[NSNumber numberWithInt:5000]}];
        return req;
    }
}

-(DAOBaseRequest*) selectTargetsWithDelegate:(id<DAODelegate>)delegate{
    if(_isOnlineMode){
//        NSString* sql = [NSString stringWithFormat:@"Select ID, Financial_Week__c, Financial_Year__c, \
//                         Financial_Month__c, Financial_Date__c, Type__c, Store__c, Brand_Code__c, Store_Code__c, \
//                         OPS_Sales_Amount__c, ATV__c, UPT__C, Conversion_Rate__c, Discount__c, System_Code__c \
//                         FROM Store_Performance_Target__c \
//                         ORDER BY Financial_Year__c DESC, Financial_Month__c DESC, Financial_Week__c DESC, Financial_Date__c DESC "];
        NSInteger nowYear = [self.cache[@"fxyear"] integerValue];
        NSInteger nowMonth =[self.cache[@"fxmonth"] integerValue];
        NSInteger nowWeek =[self.cache[@"fxweek"] integerValue];

        NSInteger lastYear = nowYear - 1;
        
        //Example:
        //Now Y=2015, M=4, W=11 >>
        //  thisYearStartWeek=-1, lastYearStartWeek=51
        //

        NSInteger thisYearStartWeek = nowWeek-12;
        NSInteger lastYearStartWeek = 999;
        
        if(thisYearStartWeek<1){
            lastYearStartWeek = 52 + thisYearStartWeek;
            thisYearStartWeek = 1;
        }
        NSInteger thisYearStartMonth = nowMonth-12;
        NSInteger lastYearStartMonth = 999;
        
        if(thisYearStartMonth<1){
            lastYearStartMonth = 12 + thisYearStartMonth;
            thisYearStartMonth = 1;
        }
        
        //Fix Admin account memory bug
        NSString* sql = [NSString stringWithFormat:@"Select ID, Financial_Week__c, Financial_Year__c, \
                         Financial_Month__c, Financial_Date__c, Type__c, Store__c, Brand_Code__c, Store_Code__c, \
                         OPS_Sales_Amount__c, ATV__c, UPT__C, Conversion_Rate__c, Discount__c, System_Code__c \
                         FROM Store_Performance_Target__c \
                         WHERE \
                         ( Type__c='daily' AND Financial_Date__c=LAST_N_DAYS:15) \
                         OR \
                         ( Type__c='mtd' AND Financial_Year__c=%ld AND Financial_Month__c=%ld) \
                         OR \
                         ( Type__c='weekly' AND ( (Financial_Year__c=%ld AND Financial_Week__c<=%ld AND Financial_Week__c>=%ld) \
                             OR (Financial_Year__c=%ld AND Financial_Week__c>=%ld) ) \
                         )\
                         OR \
                         (Type__c='wtd' AND Financial_Year__c=%ld AND Financial_Week__c=%ld) \
                         OR \
                         ( Type__c='monthly' AND ( (Financial_Year__c=%ld AND Financial_Month__c>=%ld) OR \
                                                  (Financial_Year__c=%ld AND Financial_Month__c>=%ld) ))\
                         ORDER BY Financial_Year__c DESC, Financial_Month__c DESC, Financial_Week__c DESC, Financial_Date__c DESC ", (long)nowYear, (long)nowMonth,
                            (long)nowYear, (long)nowWeek, (long)thisYearStartWeek,
                            (long)lastYear, (long)lastYearStartWeek,
                            (long)nowYear, (long)nowWeek,
                            (long)nowYear, (long)thisYearStartMonth,
                         (long)lastYear, (long)lastYearStartMonth
                         ];
        
        return [self queryOnlineDataBySQL:sql delegate:delegate postDelegate:^(NSArray *records) {
            [self runStandardUpsertTable: DB_TABLE_TARGET records:records spec: @[
                                                                                    @{@"path":@"Id",
                                                                                      @"type":@"string"}
                                                                                    ,@{@"path":@"Financial_Year__c",
                                                                                       @"type":@"string"}
                                                                                    ,@{@"path":@"Financial_Month__c",
                                                                                       @"type":@"string"}
                                                                                     ,@{@"path":@"Financial_Week__c",
                                                                                        @"type":@"string"}
                                                                                    ,@{@"path":@"Financial_Date__c",
                                                                                       @"type":@"string"}
                                                                                    ] ];
        }];
    }else{
        //FIXME: should sort by 4 fields
        CustomWebserviceRequest* req = [[[CustomWebserviceRequest alloc] init] autorelease];
        [self runStandardSelectFromTable:DB_TABLE_TARGET request:req delegate:delegate queryDict:@{   @"queryType":@"range", @"indexPath":@"Brand_Code__c", @"order":@"ascending", @"pageSize":[NSNumber numberWithInt:5000]}];
        return req;
    }
}



-(DAOBaseRequest*) selectSurveyMasterDataWithDelegate:(id<DAODelegate>)delegate{
    if(_isOnlineMode){
        NSString*  sql = [NSString stringWithFormat:@"SELECT Id, Name, emfa__Type__c, emfa__Header__c, emfa__Footer__c, \
                                                Number_Of_Question__c, Number_Of_Survey_Created__c, \
                                                Question_Count__c, emfa__Status__c, Total_Score__c, \
                                                CreatedDate, LastModifiedDate, Target_Group__c, \
                                                (SELECT Id, Name, emfa__Description__c , emfa__Extra_Comment__c, \
                                                 emfa__Max_Number__c, emfa__Min_Number__c , emfa__Ordering__c , emfa__Required__c, \
                                                 emfa__Survey_Master2__c,  emfa__Title__c, RecordTypeId, Yes_Score__c, No_Score__c \
                                                 FROM emfa__Question_Masters__r ORDER BY emfa__Ordering__c ASC)  \
                     FROM emfa__Survey_Master__c ORDER BY CreatedDate ASC"];
        //It must be ASC.  iPad app will use the last Published Survey Master as default one.
        return [self queryOnlineDataBySQL:sql delegate:delegate postDelegate:^(NSArray *records) {
            [self runStandardUpsertTable: DB_TABLE_SURVEY_MASTER records:records spec: @[
                                                                                    @{@"path":@"Id",
                                                                                      @"type":@"string"}
                                                                                    ,@{@"path":@"emfa__Ordering__c",
                                                                                       @"type":@"integer"}
                                                                                    ,@{@"path":@"Type__c", @"type":@"string"}
                                                                                    ,@{@"path":@"CreatedDate", @"type":@"string"}
                                                                                    ]
             
             ];
        }];
    }else{
        CustomWebserviceRequest* req = [[[CustomWebserviceRequest alloc] init] autorelease];
        [self runStandardSelectFromTable:DB_TABLE_SURVEY_MASTER request:req delegate:delegate queryDict:@{   @"queryType":@"range", @"indexPath":@"emfa__Ordering__c", @"order":@"ascending", @"pageSize":[NSNumber numberWithInt:5000]}];
        return req;
    }

}



-(DAOBaseRequest*) selectAppInitDataWithDelegate:(id<DAODelegate>)delegate parameter:(NSDictionary*)data{
    NSAssert(self.customRequest==nil, @"selectAppInitWithDelegate Error! Last request(%@) is still running", self.customRequest.name);
    NSLog(@"VFDAO selectAppInitWithDelegate");
    
    if(_isOnlineMode){
        return [self sendCustomRestRequest:REST_API_APP_INIT_GET method:SFRestMethodGET parameter:data delegate:delegate postDelegate:^(NSArray *records) {
            SFSmartStore* db = [SFSmartStore sharedStoreWithName:SMART_STORE_NAME];
            if(![db soupExists:DB_TABLE_APP_INIT]){
                
                NSLog(@"SmartStore: Table DB_TABLE_APP_INIT does not exist. Create it now...");
                
                BOOL success = [db registerSoup:DB_TABLE_APP_INIT withIndexSpecs:
                                @[
                                  @{@"path":@"user_name",
                                    @"type":@"string"}
                                  
                                  ]];
                NSLog(@"SmartStore: SOUP %@ created %@", DB_TABLE_APP_INIT, success?@"success":@"fail");
            }
            NSLog(@"SmartStore: Upsert DB_TABLE_APP_INIT records...");
            NSError* error = nil;
            [db upsertEntries:records toSoup:DB_TABLE_APP_INIT withExternalIdPath:@"user_name" error:&error];
            if(error!=nil){
                NSLog(@"Upsert error %@", error);
            }
        }];
    }
    else{
        CustomWebserviceRequest* req = [[[CustomWebserviceRequest alloc] init] autorelease];
        
        SFSmartStore* db = [SFSmartStore sharedStoreWithName:SMART_STORE_NAME];
        if([db soupExists:DB_TABLE_APP_INIT]){
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NSLog(@"selectAppInitDataWithDelegate: Load records from local table %@", DB_TABLE_APP_INIT);
                
                NSDictionary* queryDict =  @{@"queryType":@"range",
                                             @"indexPath":@"user_name",
                                             @"pageSize":[NSNumber numberWithInt:10]};
                SFQuerySpec *querySpec = [[[SFQuerySpec alloc] initWithDictionary:queryDict
                                                                     withSoupName:DB_TABLE_APP_INIT] autorelease];
                NSArray *dbRecords = [db queryWithQuerySpec:querySpec pageIndex:0];
                NSLog(@"Process done. DB_TABLE_APP_INIT Local record count = %d", [dbRecords count]);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [delegate daoRequest: req queryOnlineDataSuccess:dbRecords];
                });
            });
        }
        else{
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [delegate daoRequest: req queryOnlineDataSuccess:@[]];
                });
            });
        }
        
        return req;
        
    }
}




-(DAOBaseRequest*) selectBrandDetailDataWithDelegate:(id<DAODelegate>)delegate parameter:(NSDictionary*)data{
    NSAssert(self.customRequest==nil, @"selectBrandDetailDataWithDelegate Error! Last request(%@) is still running", self.customRequest.name);
    NSLog(@"VFDAO selectBrandDetailDataWithDelegate");
    
    NSString* fullBrandCode = [NSString stringWithFormat:@"%@%@", data[@"Code__c"], data[@"Country_Code__c"]];
    
    if(_isOnlineMode){
        return [self sendCustomRestRequest:REST_API_APP_INIT_GET method:SFRestMethodGET parameter:data delegate:delegate postDelegate:^(NSArray *records) {
            SFSmartStore* db = [SFSmartStore sharedStoreWithName:SMART_STORE_NAME];
            if(![db soupExists:DB_TABLE_ACCOUNT_INFO]){
                
                NSLog(@"SmartStore: Table DB_TABLE_APP_INIT does not exist. Create it now...");
                
                BOOL success = [db registerSoup:DB_TABLE_ACCOUNT_INFO withIndexSpecs:
                                @[
                                  @{@"path":@"brandCode",
                                    @"type":@"string"}
                                  
                                  ]];
                NSLog(@"SmartStore: SOUP %@ created %@", DB_TABLE_ACCOUNT_INFO, success?@"success":@"fail");
            }
            NSLog(@"SmartStore: Upsert DB_TABLE_APP_INIT records...");
            NSError* error = nil;
            [db upsertEntries:records toSoup:DB_TABLE_ACCOUNT_INFO withExternalIdPath:@"brandCode" error:&error];
            if(error!=nil){
                NSLog(@"Upsert error %@", error);
            }
        }];
    }
    else{
        CustomWebserviceRequest* req = [[[CustomWebserviceRequest alloc] init] autorelease];
        
        SFSmartStore* db = [SFSmartStore sharedStoreWithName:SMART_STORE_NAME];
        if([db soupExists:DB_TABLE_ACCOUNT_INFO]){
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NSLog(@"selectBrandDetailDataWithDelegate: Load records from local table %@", DB_TABLE_ACCOUNT_INFO);
                
                NSDictionary* queryDict =  @{@"queryType":@"exact",
                                             @"indexPath":@"brandCode",
                                             @"matchKey":fullBrandCode,
                                             @"pageSize":[NSNumber numberWithInt:10]};
                SFQuerySpec *querySpec = [[[SFQuerySpec alloc] initWithDictionary:queryDict
                                                                     withSoupName:DB_TABLE_ACCOUNT_INFO] autorelease];
                NSArray *dbRecords = [db queryWithQuerySpec:querySpec pageIndex:0];
                NSLog(@"Process done. DB_TABLE_APP_INIT Local record count = %d", [dbRecords count]);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [delegate daoRequest: req queryOnlineDataSuccess:dbRecords];
                });
            });
        }
        else{
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [delegate daoRequest: req queryOnlineDataSuccess:@[]];
                });
            });
        }
        
        return req;
        
    }
}





#pragma mark - UserInfo
//OFFLINE DONE
//-(DAOBaseRequest*) selectUserInfoDataWithDelegate:(id<DAODelegate>)delegate {
//    NSAssert(self.customRequest==nil, @"selectUserInfoDataWithDelegate Error! Last request(%@) is still running", self.customRequest.name);
//    NSLog(@"VFDAO selectUserInfoDataWithDelegate");
//    
//    if(_isOnlineMode){
//        return [self sendCustomRestRequest:REST_API_USERINFO_GET method:SFRestMethodGET parameter:[NSDictionary dictionary] delegate:delegate postDelegate:^(NSArray *records) {
//            SFSmartStore* db = [SFSmartStore sharedStoreWithName:SMART_STORE_NAME];
//            if(![db soupExists:DB_TABLE_USERINFO]){
//                NSLog(@"SmartStore: Table DB_TABLE_USERINFO does not exist. Create it now...");
//                BOOL success = [db registerSoup:DB_TABLE_USERINFO withIndexSpecs:
//                                @[
//                                  @{@"path":@"user_name",
//                                    @"type":@"string"}
//                                  
//                                  ]];
//                NSLog(@"SmartStore: SOUP %@ created %@", DB_TABLE_USERINFO, success?@"success":@"fail");
//            }
//            NSLog(@"SmartStore: Upsert DB_TABLE_USERINFO records...");
//            NSError* error = nil;
//            [db upsertEntries:records toSoup:DB_TABLE_USERINFO withExternalIdPath:@"user_name" error:&error];
//            if(error!=nil){
//                NSLog(@"Upsert error %@", error);
//            }
//        }];
//    }else{
//        CustomWebserviceRequest* req = [[[CustomWebserviceRequest alloc] init] autorelease];
//        
//        SFSmartStore* db = [SFSmartStore sharedStoreWithName:SMART_STORE_NAME];
//        if([db soupExists:DB_TABLE_USERINFO]){
//            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                NSLog(@"selectUserInfoDataWithDelegate: Load records from local table %@", DB_TABLE_USERINFO);
//                
//                NSDictionary* queryDict =  @{@"queryType":@"range",
//                                             @"indexPath":@"user_name",
//                                             @"pageSize":[NSNumber numberWithInt:10]};
//                SFQuerySpec *querySpec = [[[SFQuerySpec alloc] initWithDictionary:queryDict
//                                                                     withSoupName:DB_TABLE_USERINFO] autorelease];
//                NSArray *dbRecords = [db queryWithQuerySpec:querySpec pageIndex:0];
//                NSLog(@"Process done. DB_TABLE_USERINFO Local record count = %d", [dbRecords count]);
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [delegate daoRequest: req queryOnlineDataSuccess:dbRecords];
//                });
//            });
//        }else{
//            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [delegate daoRequest: req queryOnlineDataSuccess:@[]];
//                });
//            });
//        }
//        
//        return req;
//
//    }
//    
//}




#pragma mark - Special Handling for any submission request


//Save comment
-(DAOBaseRequest*) saveNewComment:(NSDictionary*)data delegate:(id<DAODelegate>)delegate{
    //Sample Data
    //    NSDictionary* data = @{
    //                           @"Subject":title,
    //                           @"Description": comment,
    //                           @"RecordTypeId":self.commentRecordId,
    //                           @"WhatId":self.storeId,
    //                           @"ActivityDate":[df stringFromDate:[NSDate date]],
    //                           @"Status":@"Completed"
    //                           };

    if(_isOnlineMode){
        NSLog(@"saveNewComment online");
        DAORequest* req = [[DAORequest alloc] init];
        req.name = @"saveNewComment";
        self.runningRequest = req;
        [req release];
        self.runningRequest.sfRequest = [[SFRestAPI sharedInstance] requestForCreateWithObjectType:@"emfa__Comment__c" fields:data];
        self.runningRequestDelegate = delegate;
        self.runningRequestPostDelegate = nil;
        [[SFRestAPI sharedInstance] send:self.runningRequest.sfRequest delegate:self];
        
        return self.runningRequest;
    }else{
        NSLog(@"saveNewComment offline");
        DAORequest* req = [[DAORequest alloc] init];
        req.name = @"saveNewComment";
        
        NSString* insertedId = [self generateNewLocalIDOfTable: DB_TABLE_COMMENT];
        
        NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary: data];  //@{DB_ID:insertedId, DB_DATA: data};
        dict[@"Id"] = insertedId;
        dict[@"dirty"] = @"Y";
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self runStandardUpsertTable:DB_TABLE_COMMENT records:@[dict] spec:DB_SPEC_COMMENT];
//            [self saveRecordAsPendingByUpsertTable:TABLE_PENDING_COMMENT records:@[dict] spec:@[
//                                                                                            @{@"path":DB_ID,
//                                                                                              @"type":@"string"},
//                                                                                            
//                                                                                            ]];
            dispatch_async(dispatch_get_main_queue(), ^{
                [delegate daoRequest:req queryOnlineDataSuccess:@[insertedId]];
            });
        });
        return req;
    }
}


//Save visit plan
-(CustomWebserviceRequest*) saveVisitPlanWithDelegate:(id<DAODelegate>)delegate visitPlan:(NSDictionary*)data{
    if(_isOnlineMode){
        NSLog(@"saveVisitPlanWithDelegate online");
        self.customRequest = [[CustomWebserviceRestApi sharedInstance] sendRequestASyncWithApiName:REST_API_VISITPLAN_SAVE data:data delegate:self postBlock:^(NSArray *records) {
            
        }];
        self.daoDelegate = delegate;
        return self.customRequest;
    }else{
        NSLog(@"saveVisitPlanWithDelegate offline");
        CustomWebserviceRequest* req = [[CustomWebserviceRequest alloc] init];
        req.name = @"saveVisitPlanWithDelegate";
        
        NSString* insertedId = [self generateNewLocalIDOfTable:TABLE_PENDING_VISIT_PLAN];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString* brandCode = [defaults objectForKey: USER_PREFER_ACCOUNT_CODE];
        NSString* countryCode = [defaults objectForKey: USER_PREFER_COUNTRY_CODE];

        
        NSMutableDictionary* offlineData = [data mutableCopy];
        offlineData[@"Store__r"] = @{@"emfa__Account__r":@{@"Code__c":brandCode, @"Country_Code__c":countryCode}};
        
        NSDictionary* dict = @{DB_ID:insertedId, DB_DATA: offlineData};
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self saveRecordAsPendingByUpsertTable:TABLE_PENDING_VISIT_PLAN records:@[dict] spec:@[
                                                                                                @{@"path":DB_ID,
                                                                                                  @"type":@"string"},
                                                                                                
                                                                                                ]];
            dispatch_async(dispatch_get_main_queue(), ^{
                [delegate daoRequest:req queryOnlineDataSuccess:@[insertedId]];
            });
        });
        return req;
    }

}

//Update visit plan
//It can only update the records of visit plan, but not children record
-(DAOBaseRequest*) updateVisitPlanId:(NSString*)planId data:(NSDictionary*)data delegate:(id<DAODelegate>)delegate{

    if(_isOnlineMode){
        NSLog(@"updateVisitPlanId online data=%@", data);
        DAORequest* req = [[DAORequest alloc] init];
        req.name = @"updateVisitPlanId";
        self.runningRequest = req;
        [req release];
        self.runningRequest.sfRequest = [[SFRestAPI sharedInstance] requestForUpdateWithObjectType:@"emfa__Visitation_Plan__c" objectId:planId fields: data];
        self.runningRequestDelegate = delegate;
        self.runningRequestPostDelegate = nil;
        [[SFRestAPI sharedInstance] send:self.runningRequest.sfRequest delegate:self];
        
        return self.runningRequest;
    }else{
        NSLog(@"updateVisitPlanId offline data=%@", data);
        DAORequest* req = [[DAORequest alloc] init];
        req.name = @"updateVisitPlanId";
        
        NSMutableDictionary* mdata = [NSMutableDictionary dictionaryWithDictionary:data];
        [mdata setObject:@"Y" forKey:@"dirty"];
        if([data objectForKey:@"emfa__Active__c"]!=nil){
            [mdata setObject:@"Y" forKey:@"is_trash"];
        }
        else if ([data objectForKey:@"emfa__Check_Out_Time__c"]!=nil){
            [mdata setObject:@"Y" forKey:@"is_checkout"];
        }
        else if ([data objectForKey:@"emfa__Check_In_Time__c"]!=nil){
            [mdata setObject:@"Y" forKey:@"is_checkin"];
        }
        else{
            NSAssert(NO, @"Not valid action");
        }
        [mdata setObject:planId forKey:@"Id"];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self runRealUpsertTable:DB_TABLE_VISIT_PLAN records:@[mdata] spec:DB_SPEC_VISIT_PLAN];
            dispatch_async(dispatch_get_main_queue(), ^{
                [delegate daoRequest:req queryOnlineDataSuccess:@[[mdata objectForKey:@"Id"]]];
            });
        });
        return req;
    }

}


-(DAOBaseRequest*) updateStatusOfTask:(NSString*)taskId parentId:(NSString*)parentId status:(NSString*)status duration:(NSNumber*)minutes delegate:(id<DAODelegate>)delegate{
    
    NSDictionary* data;
    if(minutes!=nil){
        data = @{@"Status":status, @"Actual_Duration__c": minutes};
    }else{
        data = @{@"Status":status};
    }

    if(_isOnlineMode){
        NSLog(@"updateStatusOfTask online");
        DAORequest* req = [[DAORequest alloc] init];
        req.name = @"updateStatusOfTask";
        self.runningRequest = req;
        [req release];
        self.runningRequest.sfRequest = [[SFRestAPI sharedInstance] requestForUpdateWithObjectType:@"Task" objectId: taskId fields:data];
        
        self.runningRequestDelegate = delegate;
        self.runningRequestPostDelegate = nil;
        [[SFRestAPI sharedInstance] send:self.runningRequest.sfRequest delegate:self];
        
        return self.runningRequest;
    }else{
 
        NSLog(@"updateStatusOfTask offline");
        DAORequest* req = [[DAORequest alloc] init];
        req.name = @"updateStatusOfTask";
        
        NSMutableDictionary* mdata = [NSMutableDictionary dictionaryWithDictionary:data];
        [mdata setObject:@"Y" forKey:@"dirty"];
        [mdata setObject:taskId forKey:@"Id"];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            //Update Task table, mark the record as dirty so system can upload it to server
            [self runRealUpsertTable:DB_TABLE_TASK records:@[mdata] spec:DB_SPEC_TASK];
            
            NSMutableDictionary* visitPlan = [NSMutableDictionary dictionaryWithDictionary: [self loadRecordFromTable:DB_TABLE_VISIT_PLAN sfId:parentId]];
            NSArray* tasks = [[visitPlan objectForKey:@"Tasks"] objectForKey:@"records"];
            for(NSMutableDictionary* taskData in tasks){
                if( [[taskData objectForKey:@"Id"] isEqualToString: taskId]){
                    [taskData setObject:status forKey:@"Status"];
                    if(minutes!=nil) [taskData setObject:minutes forKey:@"Actual_Duration__c"];
                }
            }
            [self runStandardUpsertTable:DB_TABLE_VISIT_PLAN records:@[visitPlan] spec:DB_SPEC_VISIT_PLAN];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [delegate daoRequest:req queryOnlineDataSuccess:@[taskId]];
            });
        });
        return req;
    }
}

-(DAOBaseRequest*) updateStatusOfTask:(NSString*)taskId parentId:(NSString*)parentId status:(NSString*)status duration:(NSNumber*)minutes comment:(NSString*)comment delegate:(id<DAODelegate>)delegate{
    
    NSDictionary* data;
    if(minutes!=nil || comment!=nil){
        if ( comment == nil ) {
            data = @{@"Status":status, @"emfa__Actual_Duration__c": minutes};
        }
        else if ( minutes == nil ){
            data = @{@"Status":status, @"Comment__c": comment};
        }
        else {
            data = @{@"Status":status, @"emfa__Actual_Duration__c": minutes, @"Comment__c":comment};
        }
    }else{
        data = @{@"Status":status};
    }
    
    if(_isOnlineMode){
        NSLog(@"updateStatusOfTask online");
        DAORequest* req = [[DAORequest alloc] init];
        req.name = @"updateStatusOfTask";
        self.runningRequest = req;
        [req release];
        self.runningRequest.sfRequest = [[SFRestAPI sharedInstance] requestForUpdateWithObjectType:@"Task" objectId: taskId fields:data];
        
        self.runningRequestDelegate = delegate;
        self.runningRequestPostDelegate = nil;
        [[SFRestAPI sharedInstance] send:self.runningRequest.sfRequest delegate:self];
        
        return self.runningRequest;
    }else{
        
        NSLog(@"updateStatusOfTask offline");
        DAORequest* req = [[DAORequest alloc] init];
        req.name = @"updateStatusOfTask";
        
        NSMutableDictionary* mdata = [NSMutableDictionary dictionaryWithDictionary:data];
        [mdata setObject:@"Y" forKey:@"dirty"];
        [mdata setObject:taskId forKey:@"Id"];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            //Update Task table, mark the record as dirty so system can upload it to server
            [self runRealUpsertTable:DB_TABLE_TASK records:@[mdata] spec:DB_SPEC_TASK];
            
            NSMutableDictionary* visitPlan = [NSMutableDictionary dictionaryWithDictionary: [self loadRecordFromTable:DB_TABLE_VISIT_PLAN sfId:parentId]];
            NSArray* tasks = [[visitPlan objectForKey:@"Tasks"] objectForKey:@"records"];
            for(NSMutableDictionary* taskData in tasks){
                if( [[taskData objectForKey:@"Id"] isEqualToString: taskId]){
                    [taskData setObject:status forKey:@"Status"];
                    if(minutes!=nil) [taskData setObject:minutes forKey:@"emfa__Actual_Duration__c"];
                    if(comment!=nil) [taskData setObject:comment forKey:@"Comment__c"];
                }
            }
            [self runStandardUpsertTable:DB_TABLE_VISIT_PLAN records:@[visitPlan] spec:DB_SPEC_VISIT_PLAN];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [delegate daoRequest:req queryOnlineDataSuccess:@[taskId]];
            });
        });
        return req;
    }
}



-(DAOBaseRequest*) upsertSurveyForm:(NSDictionary*)surveyDict delegate:(id<DAODelegate>)delegate{
    if(_isOnlineMode){
        NSLog(@"upsertSurveyForm online");
        //SurveyDict = {
        //  Visit_Plan__c = xxx,
        //  emfa__Survey_Master__c = xxx,
        //  answers = {
        //      1 = {
        //             emfa__Answer_Value__c, emfa__Comment__c, emfa__Question_Master__c, emfa__Required__c, tag
        //      },
        //      2 = {}, ...
        //  }
        //}
        self.customRequest = [[CustomWebserviceRestApi sharedInstance] sendRequestASyncWithApiName:REST_API_SURVEY_SAVE data:surveyDict delegate:self postBlock:^(NSArray *records) {
            //dont need insert it to local DB, as the survey list will be reload immd in the Visit store flow
            if([records count]>0  ){
                NSString* sid = [[records lastObject] objectForKey:@"insertid"];
                NSMutableDictionary* dict = [self convertSurveyParameterToObject:surveyDict];
                [dict setObject:sid forKey:@"Id"];
                [self runStandardUpsertTable:DB_TABLE_SURVEY_FORM records:@[dict] spec:DB_SPEC_SURVEY_FORM];
            }
        }];
        self.daoDelegate = delegate;
        return self.customRequest;
    }
    else{
        NSLog(@"upsertSurveyForm offline");
        CustomWebserviceRequest* req = [[CustomWebserviceRequest alloc] init];
        req.name = @"upsertSurveyForm";
        
        //SELECT Id, Name, emfa__Survey_Master__c, CreatedDate, CreatedBy.Name, Store__r.Name, Visitation_Plan_Name__c, Visitation_Plan__c, (SELECT Id, Name, emfa__Answer_Master__c, emfa__Answer_Value__c , emfa__Comment__c, emfa__Question_Master__c, emfa__Survey__c FROM emfa__Question_Answer__r)
        NSMutableDictionary* dict = [self convertSurveyParameterToObject:surveyDict];
        [dict setObject:@"Y" forKey:@"dirty"];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//            [self saveRecordAsPendingByUpsertTable:DB_TABLE_SURVEY_FORM records:@[dict] spec:DB_SPEC_SURVEY_FORM];
            [self runStandardUpsertTable:DB_TABLE_SURVEY_FORM records:@[dict] spec:DB_SPEC_SURVEY_FORM];
            dispatch_async(dispatch_get_main_queue(), ^{
                [delegate daoRequest:req queryOnlineDataSuccess:@[ @{ @"insertid":[dict objectForKey:@"Id"]} ] ];
            });
        });
        return req;
    }
}

//As the survey submission rest api have different structure to DB structure
-(NSMutableDictionary*) convertSurveyParameterToObject:(NSDictionary*)parameters{
    NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithDictionary:parameters];
    NSString* insertedId = [dict objectForKey:@"Id"];
    if(insertedId==nil){
        insertedId = [self generateNewLocalIDOfTable:DB_TABLE_SURVEY_FORM];
        [dict setObject:insertedId forKey:@"Id"];
    }
    [dict setObject:[parameters objectForKey:@"emfa__Survey_Master2__c"] forKey:@"emfa__Survey_Master_2_c"];
    [dict setObject:[parameters objectForKey:@"Visit_Plan__c"] forKey:@"Visit_Plan__c"];
//    [dict setObject:[parameters objectForKey:@"Visitation_Plan_Name__c"] forKey:@"Visitation_Plan_Name__c"];
    NSDictionary* answerObj = [parameters objectForKey:@"answers" ];
    NSMutableArray* surveyAnswers = [NSMutableArray array];
    NSArray* allKeys = [answerObj allKeys];
    for(NSString* key in allKeys){
        NSDictionary* answer = [answerObj objectForKey:key];
        NSDictionary* panswer = @{ @"emfa__Answer_Value__c":[answer objectForKey:@"emfa__Answer_Value__c"],
                                   @"emfa__Comment__c":[answer objectForKey:@"emfa__Comment__c"],
                                   @"emfa__Question_Master__c":[answer objectForKey:@"emfa__Question_Master__c"]
                                   };
        [surveyAnswers addObject:panswer];
    }
    [dict setObject:@{@"records":surveyAnswers} forKey:@"emfa__Question_Answer__r"];
    return dict;
}

-(NSMutableDictionary*) convertSurveyObjectToParameter:(NSDictionary*)object{
    NSMutableDictionary* dict = [NSMutableDictionary dictionary];
    NSString* insertedId = [object objectForKey:@"Id"];
    if([insertedId rangeOfString:DB_TABLE_SURVEY_FORM].location != NSNotFound){
        [dict removeObjectForKey:@"Id"];//remove local generated ID
    }else{
        [dict setObject:insertedId forKey:@"Id"];
    }
    [dict setObject:[object objectForKey:@"emfa__Survey_Master2__c"] forKey:@"emfa__Survey_Master2__c"];
    [dict setObject:[object objectForKey:@"Visit_Plan__c"] forKey:@"Visit_Plan__c"];
    NSDictionary* answerObj = [object objectForKey:@"emfa__Question_Answer__r" ];
    
    NSMutableArray* surveyAnswers = [NSMutableArray array];
    NSArray* allAnswers = [answerObj objectForKey:@"records"];
    for(NSDictionary* answer in allAnswers){
        NSDictionary* panswer = @{ @"emfa__Answer_Value__c":[answer objectForKey:@"emfa__Answer_Value__c"],
                                   @"emfa__Comment__c":[answer objectForKey:@"emfa__Comment__c"],
                                   @"emfa__Question_Master__c":[answer objectForKey:@"emfa__Question_Master__c"]
                                   };
        [surveyAnswers addObject:panswer];
    }
    
    NSMutableDictionary* answerMap = [NSMutableDictionary dictionary];
    int index = 1;
    for(NSDictionary* ans in surveyAnswers){
        [answerMap setObject:ans forKey:[NSString stringWithFormat:@"%d",index]];
        index++;
    }
    [dict setObject:answerMap forKey:@"answers"];
    return dict;
}



#pragma mark - General Custom Request
-(void) onRequestComplete:(CustomWebserviceRequest*)request response:(NSDictionary*) response{
    if(self.customRequest == request){
        self.customRequest = nil;
        NSString* success = [response objectForKey:@"success"];
        if(success!=nil && [success isEqualToString:@"true"]){
            [self.daoDelegate daoRequest:request queryOnlineDataSuccess:@[response]];
        }else{
            [self onRequestFail:request error: @"request not success"];
        }
    }else{
        NSAssert(NO, @"Error! CustomRequest can only run once each time.");
    }
}



-(void) onRequestFail:(CustomWebserviceRequest*)request error:(NSString*)error{
    if(self.customRequest == request){
        self.customRequest = nil;
        NSLog(@"Error! error = %@", error);
        
        int code = 1;//unknown error
        NSUInteger index = [error rangeOfString:@"Code="].location;
        if(index!=NSNotFound){
            NSString* codeStr = [error substringFromIndex:index+5];
            index = [codeStr rangeOfString:@" "].location;
            if(index!=NSNotFound){
                code = [[codeStr substringToIndex:index] intValue];
            }
        }
        
        
        [self.daoDelegate daoRequest:request queryOnlineDataError:error code: code ];
    }else{
        NSAssert(NO, @"Error! customRequest can only run once each time.");
    }
}


#pragma mark - OFFLINE Function helper(Internal use only)
-(void) runStandardUpsertTable:(NSString*)tableName records:(NSArray*)records{
    [self runStandardUpsertTable:tableName records:records spec:@[
                                                                  @{@"path":@"Id",
                                                                    @"type":@"string"}
                                                                  ]];
}

//This upsert is different to SFDC.  It replace the whole object instead of updating the fields!
-(void) runStandardUpsertTable:(NSString*)tableName records:(NSArray*)records spec:(NSArray*)spec{
    [self runStandardUpsertTable:tableName records:records spec:spec externalPath:@"Id"];
}

-(void) runStandardUpsertTable:(NSString*)tableName records:(NSArray*)records spec:(NSArray*)spec externalPath:(NSString*)externalPath{
    SFSmartStore* db = [SFSmartStore sharedStoreWithName:SMART_STORE_NAME];
    if(![db soupExists:tableName]){
        NSLog(@"VFDAO>> runStandardUpsertTable >> SmartStore: Table '%@' does not exist. Create it now...", tableName);
        BOOL success = [db registerSoup:tableName withIndexSpecs:spec];
        //NSLog(@"SmartStore: Table %@ created %@", tableName, success?@"success":@"fail");
        
        NSLog(@"SmartStore: SOUP %@ created %@", tableName, success?@"success":@"fail");
        
        if(!success){
            NSLog(@"VFDAO>> Skip upsert into DB.");
            return;
        }
    }
    
    NSLog(@"VFDAO>> runStandardUpsertTable >> SmartStore: Upsert new %@ records...", tableName);
    NSError* error = nil;
    NSArray* savedRecord = [db upsertEntries:records toSoup:tableName withExternalIdPath:externalPath error:&error];
    if(error){
        NSLog(@"error=%@", error);
    }else{
        NSLog(@"saved records = %d", [savedRecord count]);
    }
}

//This upsert will update fields instead of replace whole object, but it is slower then runStandardUpsertTable as it load the object from sqlite first.
-(void) runRealUpsertTable:(NSString*)tableName records:(NSArray*)records spec:(NSArray*)spec{
    SFSmartStore* db = [SFSmartStore sharedStoreWithName:SMART_STORE_NAME];
    if(![db soupExists:tableName]){
        NSLog(@"VFDAO>> runStandardUpsertTable >> SmartStore: Table '%@' does not exist. Create it now...", tableName);
        BOOL success = [db registerSoup:tableName withIndexSpecs:spec];
        NSLog(@"SmartStore: Table %@ created %@", tableName, success?@"success":@"fail");
        if(!success){
            NSLog(@"VFDAO>> Skip upsert into DB.");
            return;
        }
    }
    for(NSDictionary* data in records){
        
        NSLog(@"VFDAO>> runStandardUpsertTable >> SmartStore: load new %@ records...", tableName);
        
        NSMutableDictionary* dbRecord = [NSMutableDictionary dictionaryWithDictionary:[self loadRecordFromTable:tableName sfId:[data objectForKey:@"Id"]]];
        
        if(dbRecord!=nil){
            for(NSString* key in [data allKeys]){
                [dbRecord setObject:[data objectForKey:key] forKey:key];
            }
        
            NSLog(@"VFDAO>> runStandardUpsertTable >> SmartStore: Upsert new %@ records...", tableName);
            NSError* error = nil;
            [db upsertEntries:@[dbRecord] toSoup:tableName withExternalIdPath:@"Id" error:&error];
            if(error!=nil){
                NSLog(@"VFDAO>> runStandardUpsertTable >> SmartStore: Upsert dail");
            }
        }
    }
    
}


-(NSDictionary*)loadRecordFromTable:(NSString*)tableName sfId:(NSString*)sfId{
    SFSmartStore* db = [SFSmartStore sharedStoreWithName:SMART_STORE_NAME];

    SFQuerySpec *querySpec = [[[SFQuerySpec alloc] initWithDictionary:@{@"queryType":@"exact",
                                                                        @"indexPath":@"Id",
                                                                        @"matchKey": sfId,
                                                                        @"pageSize":[NSNumber numberWithInt:1] }
                                                         withSoupName:tableName] autorelease];
    
    NSArray* dbRecords = [db queryWithQuerySpec:querySpec pageIndex:0];
    if([dbRecords count]==1){
        return [dbRecords lastObject];
    }else{
        return nil;
    }
}





-(void) runStandardSelectFromTable:(NSString*)tableName request:(DAOBaseRequest*)request delegate:(id<DAODelegate>)delegate{
    [self runStandardSelectFromTable:tableName request:request delegate:delegate queryDict:
        @{   @"queryType":@"range",
            @"indexPath":@"Id",
            @"pageSize":[NSNumber numberWithInt:10000]}
     ];

}

-(void) runStandardSmartSqlFromTable:(NSString*)tableName request:(DAOBaseRequest*)request delegate:(id<DAODelegate>)delegate queryDict:(NSDictionary*)queryDict{
    
    SFSmartStore* db = [SFSmartStore sharedStoreWithName:SMART_STORE_NAME];
    if([db soupExists:tableName]){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSLog(@"runStandardSmartSqlFromTable: Load records from local table %@", tableName);
            
            SFQuerySpec *querySpec = [[[SFQuerySpec alloc] initWithDictionary:queryDict
                                                                 withSoupName:tableName] autorelease];
            NSArray *dbRecords = [db queryWithQuerySpec:querySpec pageIndex:0];
            NSLog(@"Process done. Local record count = %d", [dbRecords count]);
            
            NSMutableArray* finalRecords = [NSMutableArray array];
            for(NSArray* obj in dbRecords){
                if([[obj objectAtIndex:1] isKindOfClass: [NSDictionary class]]){
                    [finalRecords addObject:[obj objectAtIndex:1]];
                }
            }
            dbRecords = nil;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [delegate daoRequest:request queryOnlineDataSuccess:finalRecords];
            });
        });
    }
    else{
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [delegate daoRequest:request queryOnlineDataSuccess:@[]];
            });
        });
    }
}

-(void) runStandardSelectFromTable:(NSString*)tableName request:(DAOBaseRequest*)request delegate:(id<DAODelegate>)delegate queryDict:(NSDictionary*)queryDict{

    SFSmartStore* db = [SFSmartStore sharedStoreWithName:SMART_STORE_NAME];
    if([db soupExists:tableName]){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSLog(@"selectStoresWithDelegate: Load records from local table %@", tableName);
            
            SFQuerySpec *querySpec = [[[SFQuerySpec alloc] initWithDictionary:queryDict
                                                                withSoupName:tableName] autorelease];
            NSArray *dbRecords = [db queryWithQuerySpec:querySpec pageIndex:0];
            NSLog(@"Process done. Local record count = %d", [dbRecords count]);
            dispatch_async(dispatch_get_main_queue(), ^{
                [delegate daoRequest:request queryOnlineDataSuccess:dbRecords];
            });
        });
    }
    else{
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [delegate daoRequest:request queryOnlineDataSuccess:@[]];
            });
        });
    }
}

-(NSString*) generateNewLocalIDOfTable:(NSString*)tableName{
    return [NSString stringWithFormat:@"%f_%@",  [NSDate date].timeIntervalSince1970, tableName ];
}



-(void) saveRecordAsPendingByUpsertTable:(NSString*)tableName records:(NSArray*)records spec:(NSArray*)spec{
    SFSmartStore* db = [SFSmartStore sharedStoreWithName:SMART_STORE_NAME];
    if(![db soupExists:tableName]){
        NSLog(@"VFDAO>> saveRecordAsPendingByUpsertTable >> SmartStore: Table '%@' does not exist. Create it now...", tableName);
        BOOL success = [db registerSoup:tableName withIndexSpecs:spec];
        NSLog(@"SmartStore: Table %@ created %@", tableName, success?@"success":@"fail");
        if(!success){
            NSLog(@"VFDAO>> Skip upsert into DB.");
            return;
        }
    }
    NSLog(@"VFDAO>> saveRecordAsPendingByUpsertTable >> SmartStore: Upsert new %@ records...", tableName);
    NSError* error = nil;
    [db upsertEntries:records toSoup:tableName withExternalIdPath:DB_ID error:&error];
    if(error!=nil){
        NSLog(@"Insert fail, %@", error);
    }else{
        NSLog(@"Insert success");
    }
}


-(NSArray*) loadDirtyRecordFromTable:(NSString*)tableName{
    SFSmartStore* db = [SFSmartStore sharedStoreWithName:SMART_STORE_NAME];
    if([db soupExists:tableName]){
        SFQuerySpec *querySpec = [[[SFQuerySpec alloc] initWithDictionary:@{@"queryType":@"exact",
                                                                            @"indexPath":@"dirty",
                                                                            @"matchKey":@"Y",
                                                                            @"pageSize":[NSNumber numberWithInt:5000],
                                                                            @"order":@"descending"}
                                                             withSoupName:tableName] autorelease];
        NSArray *dbRecords = [db queryWithQuerySpec:querySpec pageIndex:0];
        return dbRecords;
    }else{
        NSLog(@"No dirty record can be found as the %@ table is not created", tableName);
        return @[];
    }

}


//return array in ID descending order sync
-(NSArray*) loadPendingRecordsFromTable:(NSString*)tableName{
    SFSmartStore* db = [SFSmartStore sharedStoreWithName:SMART_STORE_NAME];
    if([db soupExists:tableName]){
        SFQuerySpec *querySpec = [[[SFQuerySpec alloc] initWithDictionary:@{@"queryType":@"range",
                                                                           @"indexPath":@"_soupEntryId",
                                                                           @"pageSize":[NSNumber numberWithInt:5000],
                                                                           @"order":@"descending"}
                                                            withSoupName:tableName] autorelease];
        NSArray *dbRecords = [db queryWithQuerySpec:querySpec pageIndex:0];
        return dbRecords;
    }else{
        NSLog(@"loadPendingRecordsFromTable: No Pending record can be found as the Pending table is not created");
        return @[];
    }
}

//return one data record of specific DB ID
-(NSDictionary*) loadRecordFromTable:(NSString*)tableName ofDBId:(NSString*)objId{
    SFSmartStore* db = [SFSmartStore sharedStoreWithName:SMART_STORE_NAME];
    if([db soupExists:tableName]){
        SFQuerySpec *querySpec = [[[SFQuerySpec alloc] initWithDictionary:@{@"queryType":@"exact",
                                                                            @"matchKey":objId,
                                                                            @"indexPath": DB_ID,
                                                                            @"pageSize":[NSNumber numberWithInt:1],
                                                                            @"order":@"descending"}
                                                             withSoupName:tableName] autorelease];
        NSArray *dbRecords = [db queryWithQuerySpec:querySpec pageIndex:0];
        if([dbRecords count]==1){
            return dbRecords[0];
        }else{
            return nil;
        }
    }
    else{
        NSLog(@"loadRecordFromTable: No Pending record can be found as the Pending table is not created");
        return nil;
    }
}

//return one data record of specific ID
-(NSDictionary*) loadRecordFromTable:(NSString*)tableName ofId:(NSString*)objId{
    SFSmartStore* db = [SFSmartStore sharedStoreWithName:SMART_STORE_NAME];
    if([db soupExists:tableName]){
        SFQuerySpec *querySpec = [[[SFQuerySpec alloc] initWithDictionary:@{@"queryType":@"exact",
                                                                            @"matchKey":objId,
                                                                            @"indexPath": @"Id",
                                                                            @"pageSize":[NSNumber numberWithInt:1],
                                                                            @"order":@"descending"}
                                                             withSoupName:tableName] autorelease];
        NSArray *dbRecords = [db queryWithQuerySpec:querySpec pageIndex:0];
        if([dbRecords count]==1){
            return dbRecords[0];
        }else{
            return nil;
        }
    }
    else{
        NSLog(@"loadRecordFromTable: No Pending record can be found as the Pending table is not created");
        return nil;
    }
}




-(void) removePendingRecordFromTable:(NSString*)tableName soupId:(NSString*)dbId{
    SFSmartStore* db = [SFSmartStore sharedStoreWithName:SMART_STORE_NAME];
    if([db soupExists:tableName]){
        [db removeEntries:@[dbId] fromSoup:tableName];
        return;
    }
}


@end