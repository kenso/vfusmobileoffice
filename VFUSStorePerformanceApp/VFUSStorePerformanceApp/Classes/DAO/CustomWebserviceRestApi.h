//
//  SaveOrderRestAction.h
//
//  Created by Developer 2 on 12/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DAORequest.h"

@interface CustomWebserviceRequest : DAOBaseRequest

@property (nonatomic, retain) NSString* name;

@end


@protocol CustomWebserviceRestDelegate <NSObject>

-(void) onRequestComplete:(CustomWebserviceRequest*)request response:(NSDictionary*) response;
-(void) onRequestFail:(CustomWebserviceRequest*)request error:(NSString*)error;

@end



@interface CustomWebserviceRestApi : NSObject<SFRestDelegate>

+(id) sharedInstance;

@property (nonatomic, retain) CustomWebserviceRequest* request;
@property (nonatomic, assign) id<CustomWebserviceRestDelegate> delegate;
@property (nonatomic, copy) void (^postBlock) (NSArray* records);

-(CustomWebserviceRequest*) sendRequestASyncWithApiName:(NSString*)apiName data:(NSDictionary*)dict delegate:(id<CustomWebserviceRestDelegate>)delegate postBlock: (void (^) (NSArray* records))postDelegate;
-(CustomWebserviceRequest*) sendRequestASyncWithApiName:(NSString*)apiName data:(NSDictionary*)dict method:(NSString*)method  delegate:(id<CustomWebserviceRestDelegate>)delegate postBlock: (void (^) (NSArray* records))postDelegate;


@end
