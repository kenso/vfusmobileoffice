//
//  BusinessLogicHelper.m
//  VFStorePerformanceApp
//
//  Created by Developer 2 on 3/3/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "BusinessLogicHelper.h"


@implementation BusinessLogicHelper


+(void) initAllThresholdDefaultValue{
    [BusinessLogicHelper setDefaultThresholdLevelForType:W_PREF_OPS_SALES highValue:470000 lowValue:470000];
    [BusinessLogicHelper setDefaultThresholdLevelForType:W_PREF_DISCOUNT highValue:12 lowValue:12];
    [BusinessLogicHelper setDefaultThresholdLevelForType:W_PREF_SALES_PROD highValue:13000 lowValue:13000];
    [BusinessLogicHelper setDefaultThresholdLevelForType:W_PREF_ATV highValue:800 lowValue:800];
    [BusinessLogicHelper setDefaultThresholdLevelForType:W_PREF_UPT highValue:1.8 lowValue:1.8];
    [BusinessLogicHelper setDefaultThresholdLevelForType:W_PREF_TXN highValue:2800 lowValue:2800];
    [BusinessLogicHelper setDefaultThresholdLevelForType:W_PREF_VISITOR_COUNT highValue:35000 lowValue:35000];
    [BusinessLogicHelper setDefaultThresholdLevelForType:W_PREF_PEL_OFF highValue:9 lowValue:9];
    [BusinessLogicHelper setDefaultThresholdLevelForType:W_PREF_CONV_RATE highValue:3.2 lowValue:3.2];
    
    [BusinessLogicHelper setDefaultThresholdLevelForType:M_PREF_OPS_SALES highValue:400000*4 lowValue:400000*4];
    [BusinessLogicHelper setDefaultThresholdLevelForType:M_PREF_DISCOUNT highValue:12 lowValue:12];
    [BusinessLogicHelper setDefaultThresholdLevelForType:M_PREF_SALES_PROD highValue:13000*4 lowValue:13000*4];
    [BusinessLogicHelper setDefaultThresholdLevelForType:M_PREF_ATV highValue:800 lowValue:800];
    [BusinessLogicHelper setDefaultThresholdLevelForType:M_PREF_UPT highValue:1.8 lowValue:1.8];
    [BusinessLogicHelper setDefaultThresholdLevelForType:M_PREF_TXN highValue:2800*4 lowValue:2800*4];
    [BusinessLogicHelper setDefaultThresholdLevelForType:M_PREF_VISITOR_COUNT highValue:35000*4 lowValue:35000*4];
    [BusinessLogicHelper setDefaultThresholdLevelForType:M_PREF_PEL_OFF highValue:9 lowValue:9];
    [BusinessLogicHelper setDefaultThresholdLevelForType:M_PREF_CONV_RATE highValue:3.2 lowValue:3.2];
    
    [BusinessLogicHelper setDefaultThresholdLevelForType:Y_PREF_OPS_SALES highValue:400000*52 lowValue:400000*52];
    [BusinessLogicHelper setDefaultThresholdLevelForType:Y_PREF_DISCOUNT highValue:12 lowValue:12];
    [BusinessLogicHelper setDefaultThresholdLevelForType:Y_PREF_SALES_PROD highValue:13000*52 lowValue:13000*52];
    [BusinessLogicHelper setDefaultThresholdLevelForType:Y_PREF_ATV highValue:800 lowValue:800];
    [BusinessLogicHelper setDefaultThresholdLevelForType:Y_PREF_UPT highValue:1.8 lowValue:1.8];
    [BusinessLogicHelper setDefaultThresholdLevelForType:Y_PREF_TXN highValue:2800*52 lowValue:2800*52];
    [BusinessLogicHelper setDefaultThresholdLevelForType:Y_PREF_VISITOR_COUNT highValue:35000*15 lowValue:35000*52];
    [BusinessLogicHelper setDefaultThresholdLevelForType:Y_PREF_PEL_OFF highValue:9 lowValue:9];
    [BusinessLogicHelper setDefaultThresholdLevelForType:Y_PREF_CONV_RATE highValue:3.2 lowValue:3.2];
    
}

+(void) setDefaultThresholdLevelForType:(NSString*)type highValue:(float)high lowValue:(float)low{
    [BusinessLogicHelper setHighThresholdLevelForType:type value:high];
    [BusinessLogicHelper setLowThresholdLevelForType:type value:low];
    
//Following only run when app does not have original config
//    NSLog(@"setDefaultThresholdLevelForType type=%@", type);
//    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
//    NSDictionary* thresoldObj = [defaults dictionaryForKey:type];
//    NSMutableDictionary* newData;
//
//    if(thresoldObj==nil || thresoldObj==(id)[NSNull null]){
//        newData = [[[NSMutableDictionary alloc] init] autorelease];
//        [defaults setObject:newData forKey:type];
//        [defaults synchronize];
//        newData = [[[NSMutableDictionary alloc] initWithDictionary:[defaults dictionaryForKey:type]] autorelease];
//        [newData setObject:[NSNumber numberWithFloat:high] forKey:@"High"];
//        [newData setObject:[NSNumber numberWithFloat:low] forKey:@"Low"];
//        [defaults setObject:newData forKey:type];
//        [defaults synchronize];
//    }else{
//        NSLog(@"thresoldObj is %@", thresoldObj);
//
//    }
}

+(void) setHighThresholdLevelForType:(NSString*)type value:(float)value{
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    NSDictionary* thresoldObj = [defaults dictionaryForKey:type];
    NSMutableDictionary* newData;
    if(thresoldObj==nil){
        newData = [[[NSMutableDictionary alloc] init] autorelease];
        [defaults setObject:newData forKey:type];
        [defaults synchronize];
        newData = [[[NSMutableDictionary alloc] initWithDictionary:[defaults dictionaryForKey:type]] autorelease];
    }else{
        newData = [[[NSMutableDictionary alloc] initWithDictionary:thresoldObj] autorelease];
    }
    [newData setObject:[NSNumber numberWithFloat:value] forKey:@"High"];
    [defaults setObject:newData forKey:type];
    [defaults synchronize];
    
}

+(void) setLowThresholdLevelForType:(NSString*)type value:(float)value{
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    NSDictionary* thresoldObj = [defaults dictionaryForKey:type];
    NSMutableDictionary* newData;
    if(thresoldObj==nil){
        newData = [[[NSMutableDictionary alloc] init] autorelease];
        [defaults setObject:newData forKey:type];
        [defaults synchronize];
        newData = [[[NSMutableDictionary alloc] initWithDictionary:[defaults dictionaryForKey:type]] autorelease];
    }else{
        newData = [[[NSMutableDictionary alloc] initWithDictionary:thresoldObj] autorelease];
    }
    [newData setObject:[NSNumber numberWithFloat:value] forKey:@"Low"];
    [defaults setObject:newData forKey:type];
    [defaults synchronize];
}

+(float) getThresholdHighLevel:(NSString*)type{
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    NSDictionary* thresoldObj = [defaults dictionaryForKey:type];
    return [thresoldObj[@"High"] floatValue];
//    return [[thresoldObj objectForKey:@"High"] floatValue];
}

+(float) getThresholdLowLevel:(NSString*)type{
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    NSDictionary* thresoldObj = [defaults dictionaryForKey:type];
    return [thresoldObj[@"Low"] floatValue];
    //return [[thresoldObj objectForKey:@"Low"] floatValue];
}



+(NSUInteger) getThresholdLevelForType:(NSString*)type value:(float)value{
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];

    NSDictionary* thresoldObj = [defaults dictionaryForKey:type];
    float high, normal, low;
    if(thresoldObj==nil){
        high = 0;
        low = 0;
        normal = 0;
    }
    high = [[thresoldObj objectForKey:@"High"] floatValue];
    low = [[thresoldObj objectForKey:@"Low"] floatValue];
    
    if(value < low){
        return LOW;
    }else if (value >= high){
        return HIGH;
    }else{
        return NORMAL;
    }
}


+(UIImage*) getGoodPin{
    return [UIImage imageNamed: @"map-pin-green-lo.png"];
}

+(UIImage*) getBadPin{
    return [UIImage imageNamed: @"map-pin-red-lo.png"];
}


+(UIImage*) getColorPinForThreshold:(NSUInteger)threshold{
    if ( threshold == HIGH){
        return [UIImage imageNamed: @"map-pin-green-lo.png"];
    }else if  ( threshold == LOW ){
        return [UIImage imageNamed: @"map-pin-red-lo.png"];
    }else {
        return [UIImage imageNamed: @"map-pin-yellow-lo.png"];
    }
}



+(NSString*) convertToThousandBase:(float)v{
    v = v / 1000;
    return [NSString stringWithFormat:@"%.2fk", v];
}

+(NSString*) convertValueToLimitedCharacterString:(float)v{
    return [BusinessLogicHelper convertValueToLimitedCharacterString:v format:@"%.2f"];
}

+(NSString*) convertValueToLimitedCharacterString:(float)v format:(NSString*)format{
    
    if(v>1000*1000){
        v = v / 1000 / 1000;
        NSString* f = [NSString stringWithFormat:@"%@m", format];
        return [NSString stringWithFormat:f, v];
    }else if (v > 1000){
        v = v / 1000;
        NSString* f = [NSString stringWithFormat:@"%@k", format];
        return [NSString stringWithFormat:f, v];
    }else{
        NSString* f = [NSString stringWithFormat:@"%@", format];
        return [NSString stringWithFormat:f, v];
    }
}


@end
