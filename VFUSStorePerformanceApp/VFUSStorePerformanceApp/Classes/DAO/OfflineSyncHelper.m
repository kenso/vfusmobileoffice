//
//  OfflineSyncHelper.m
//  VFStorePerformanceApp_Dev2
//
//  Created by Billy Lo on 16/7/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "OfflineSyncHelper.h"
#import "VFDAO.h"
#import "ImageLoader.h"
#import "ImageSaver.h"
#import "ShareLocale.h"
#import "DataHelper.h"


@implementation OfflineSyncHelper


//Upload Flow:
//      allPendingCommentsRecords
//      allPendingVisitPlansRecords
//      allDirtyTrashedVisitPlans
//      allDirtyCheckinVisitPlans
//      allDirtyVisitPlanTasks
//      allDirtySurveys
//      allDirtyCheckoutVisitPlans

//Download Flow:
//   Start - All request are sent after another one end,
//           whole process is terminated for any error.
//      userInfoRequest
//      storeInfoRequest x N
//      commentsRequest
//      notificationRequest
//      chartDataRequest x N
//      inventoryRequest x N
//      stockSalesRequest x N
//      stockOnHandRequest x N
//      salesMixRequest x N
//      surveyMasterAuditRequest
//      surveyMasterMSPRequest
//      surveyMasterVisitRequest
//      surveyMSPRequest x N
//      surveyVisitRequest x N
//      surveyAuditRequest x N
//      surveyQUestionRequest x N
//      tblKnowledgeRequest
//      vanKnowledgeRequest
//      allVisitPlanRequest
//   DONE - call complete block
//



//singleton
+ (id)sharedInstance {
    static VFDAO *sharedMyInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyInstance = [[self alloc] init];
    });
    return sharedMyInstance;
}


-(id)init{
    self = [super init];
    skipChartDataRequest = NO;
    self.offlineDownloadOption = 0;
    return self;
}




#pragma mark - Network upload/download kickoff

-(void) startDownloadWithCompleteBlock: (void (^)(BOOL success))successBlock updateBlock:(void (^)(DAOBaseRequest* req, NSArray* response, NSString* messageText))updateBlock{
    
    VFDAO* dao = [VFDAO sharedInstance];
    
    if(self.offlineDownloadOption == 0){
        //only erase all data if it is full data set refresh
        [dao resetDB];
    }
    self.completeBlock = successBlock;
    self.updateBlock = updateBlock;
    self.appInitRequest = [dao selectAppInitDataWithDelegate:self parameter:@{@"action":@"load_brands"}];

}



-(void) startUploadDataWithCompleteBlock: (void (^)(BOOL success))successBlock  {
    NSAssert( [[VFDAO sharedInstance] isOnlineMode] , @"This function can only run in Online Mode");
    self.completeBlock = successBlock;
    self.updateBlock = nil;
    
    __block OfflineSyncHelper* me = self;

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        //all newly created comment in offline mode
        me.allPendingCommentsRecords = [NSMutableArray arrayWithArray:[[VFDAO sharedInstance] loadDirtyRecordFromTable:DB_TABLE_COMMENT]];
        
        //all newly created visit plan in offline mode
        me.allPendingVisitPlansRecords = [NSMutableArray arrayWithArray:[[VFDAO sharedInstance] loadPendingRecordsFromTable:TABLE_PENDING_VISIT_PLAN]];
        
        //all deletd visit plan in offline mode
        NSArray* allDirtyVisitPlans = [NSMutableArray arrayWithArray:[[VFDAO sharedInstance] loadDirtyRecordFromTable:DB_TABLE_VISIT_PLAN]];
        
        me.allDirtyTrashedVisitPlans = [NSMutableArray array];
        me.allDirtyCheckoutVisitPlans = [NSMutableArray array];
        me.allDirtyCheckinVisitPlans = [NSMutableArray array];
        for(NSDictionary* plan in allDirtyVisitPlans){
            if([[plan objectForKey:@"is_trash"] isEqualToString:@"Y"]){
                [me.allDirtyTrashedVisitPlans addObject:plan];
            }
            if([[plan objectForKey:@"is_checkout"] isEqualToString:@"Y"]){
                [me.allDirtyCheckoutVisitPlans addObject:plan];
            }
            if([[plan objectForKey:@"is_checkin"] isEqualToString:@"Y"]){
                [me.allDirtyCheckinVisitPlans addObject:plan];
            }
        }
        me.allDirtyVisitPlanTasks = [NSMutableArray arrayWithArray:[[VFDAO sharedInstance] loadDirtyRecordFromTable:DB_TABLE_TASK]];
        me.allDirtySurveys = [NSMutableArray arrayWithArray:[[VFDAO sharedInstance] loadDirtyRecordFromTable:DB_TABLE_SURVEY_FORM]];
        
        [me uploadNextPendingCommentWithResponse:nil];
    });
}


#pragma mark - Upload functions

//Comments
-(void) onUploadPnedingCommentComplete{
    NSString* soupId = self.uploadCommentRequest.name;
    VFDAO* vf = [VFDAO sharedInstance];
    [vf removePendingRecordFromTable: DB_TABLE_COMMENT soupId:soupId];
}

-(void) uploadNextPendingCommentWithResponse:(NSArray*)response{
    
    BOOL skipUpload = NO;
    if(response!=nil && self.commentInProgress!=nil && [response count]>0 &&
       self.commentInProgress[@"Shared_Media__r"]!=nil && self.commentInProgress[@"Shared_Media__r"]!=(id)[NSNull null]
       && self.commentInProgress[@"Shared_Media__r"][@"records"]!=nil && self.commentInProgress[@"Shared_Media__r"][@"records"]!=(id)[NSNull null] ){
        NSArray* mediaAry =  self.commentInProgress[@"Shared_Media__r"][@"records"];
        NSMutableArray* mediaParaArray = [NSMutableArray array];
        NSInteger i=1;
        for(NSDictionary* media in mediaAry){
            NSString* fileName = media[@"emfa__FeedItemId__c"];
            NSDictionary* data = @{
                                   @"objId": response[0],
                                   @"category": [NSString stringWithFormat:@"Comment Photo %ld", (long)i],
                                   @"localFileName": fileName
                                   };
            [mediaParaArray addObject: data];
            i++;
        }
        if([mediaParaArray count]>0){
            [[ImageSaver shareInstance] uploadImagesWithFeedItemIDArray:mediaParaArray delegate:self];
        }else{
            skipUpload = YES;
        }
    }
    else{
        skipUpload = YES;
    }
    
    if(skipUpload){
        self.commentInProgress = nil;
        if ([self.allPendingCommentsRecords count]>0){
            NSDictionary* data = [NSDictionary dictionaryWithDictionary: [self.allPendingCommentsRecords lastObject]];
            [self.allPendingCommentsRecords removeLastObject];
            NSDictionary* newComment = @{
                                         @"ActivityDate__c": data[@"ActivityDate__c"],
                                         @"Creator__c": data[@"Creator__c"],
                                         @"Description__c": gfrd(data, @"Description__c", @""),
                                         @"Store__c": data[@"Store__c"],
                                         @"Subject__c": gfrd(data, @"Subject__c", @""),
                                         };
            self.uploadCommentRequest = [[VFDAO sharedInstance] saveNewComment: newComment  delegate:self];
            self.commentInProgress = data;
            self.uploadCommentRequest.name = [NSString stringWithFormat:@"%@", data[@"_soupEntryId"] ];
        }
        else{
            self.uploadCommentRequest = nil;
            [self uploadNextPendingVisitPlan];
        }
    }
}

//Visit Plan
-(void) onUploadPnedingVisitPlanComplete{
    NSString* soupId = self.uploadVisitPlanRequest.name;
    VFDAO* vf = [VFDAO sharedInstance];
    [vf removePendingRecordFromTable:TABLE_PENDING_VISIT_PLAN soupId:soupId];
}

-(void) uploadNextPendingVisitPlan{
    if([self.allPendingVisitPlansRecords count]>0){
        NSDictionary* data = [NSDictionary dictionaryWithDictionary: [self.allPendingVisitPlansRecords lastObject]];
        [self.allPendingVisitPlansRecords removeLastObject];
        
        self.uploadVisitPlanRequest = [[VFDAO sharedInstance] saveVisitPlanWithDelegate:self visitPlan:[data objectForKey:DB_DATA]];
        self.uploadVisitPlanRequest.name = [NSString stringWithFormat:@"%@", [data objectForKey:@"_soupEntryId"]];
    }
    else{
        self.uploadVisitPlanRequest = nil;
        [self uploadNextDirtyTrashVisitPlan];
    }
}



//Trashed Visit Plan
-(void) uploadNextDirtyTrashVisitPlan{
    if([self.allDirtyTrashedVisitPlans count]>0){
        NSDictionary* data = [NSDictionary dictionaryWithDictionary: [self.allDirtyTrashedVisitPlans lastObject]];
        [self.allDirtyTrashedVisitPlans removeLastObject];
        
        //local db record ID
        NSString* sfID = [data objectForKey:@"Id"];
        NSString* dbID = [data objectForKey:@"_soupEntryId"];
        //remove unecessary fields
        data = @{
                    @"Status__c": [data objectForKey:@"Status__c"]
                 };
        
        self.updateTrashedVisitPlanRequest = [[VFDAO sharedInstance] updateVisitPlanId:sfID data:data delegate:self];
        self.updateTrashedVisitPlanRequest.name = [NSString stringWithFormat:@"%@", dbID];
    }else{
        self.updateTrashedVisitPlanRequest = nil;
        [self uploadNextDirtyCheckinVisitPlan];
    }
}



-(void) onUploadTrashedVisitPlanComplete{
    NSString* soupId = self.updateTrashedVisitPlanRequest.name;
    VFDAO* vf = [VFDAO sharedInstance];
    [vf removePendingRecordFromTable:DB_TABLE_VISIT_PLAN soupId:soupId];
}


//ToDo or Document task
-(void) uploadNextDirtyTask{
    if([self.allDirtyVisitPlanTasks count]>0){
        NSDictionary* data = [NSDictionary dictionaryWithDictionary: [self.allDirtyVisitPlanTasks lastObject]];
        [self.allDirtyVisitPlanTasks removeLastObject];
        
        //local db record ID
        NSString* sfID = [data objectForKey:@"Id"];
        NSString* dbID = [data objectForKey:@"_soupEntryId"];
        //remove unecessary fields
        
        self.updateVisitPlanTaskRequest = [[VFDAO sharedInstance] updateStatusOfTask:sfID parentId:[data objectForKey:@"WhatId"] status:[data objectForKey:@"Status"] duration:[data objectForKey:@"Actual_Duration__c"] delegate:self];
        self.updateVisitPlanTaskRequest.name = [NSString stringWithFormat:@"%@", dbID];
    }else{
        self.updateVisitPlanTaskRequest = nil;
        [self uploadNextDirtySurvey];
        //[self uploadNextDirtyCheckoutVisitPlan];
    }
}

-(void) onUploadVisitPlanTaskComplete{
    NSString* soupId = self.updateVisitPlanTaskRequest.name;
    VFDAO* vf = [VFDAO sharedInstance];
    [vf removePendingRecordFromTable:DB_TABLE_TASK soupId:soupId];
}


//Survey
-(void) uploadNextDirtySurvey{
    if([self.allDirtySurveys count]>0){
        NSDictionary* data = [NSDictionary dictionaryWithDictionary: [self.allDirtySurveys lastObject]];
        [self.allDirtySurveys removeLastObject];
        
        //local db record ID
        NSString* dbID = [data objectForKey:@"_soupEntryId"];
        //remove unecessary fields
        
        NSMutableDictionary* mdata = [[VFDAO sharedInstance] convertSurveyObjectToParameter:data];
        
        self.updateSurveyRequest = [[VFDAO sharedInstance] upsertSurveyForm:mdata delegate:self];
        
        self.updateSurveyRequest.name = [NSString stringWithFormat:@"%@", dbID];
    }else{
        self.updateSurveyRequest = nil;
        [self uploadNextDirtyCheckoutVisitPlan];
    }
}

-(void) onUploadSurveyComplete{
    NSString* soupId = self.updateSurveyRequest.name;
    VFDAO* vf = [VFDAO sharedInstance];
    [vf removePendingRecordFromTable:DB_TABLE_SURVEY_FORM soupId:soupId];
}



//Checkin visit plan
-(void) uploadNextDirtyCheckinVisitPlan{
    if([self.allDirtyCheckinVisitPlans count]>0){
        NSDictionary* data = [NSDictionary dictionaryWithDictionary: [self.allDirtyCheckinVisitPlans lastObject]];
        [self.allDirtyCheckinVisitPlans removeLastObject];
        
        //local db record ID
        NSString* sfID = [data objectForKey:@"Id"];
        NSString* dbID = [data objectForKey:@"_soupEntryId"];
        //remove unecessary fields
        data = @{
                 @"Check_In_Date_Time__c": [data objectForKey:@"Check_In_Date_Time__c"]
                 };
        
        self.updateCheckinVisitPlanRequest = [[VFDAO sharedInstance] updateVisitPlanId:sfID data:data delegate:self];
        self.updateCheckinVisitPlanRequest.name = [NSString stringWithFormat:@"%@", dbID];
    }else{
        self.updateCheckinVisitPlanRequest = nil;
        [self uploadNextDirtyTask];
    }
}
-(void) onUploadCheckinVisitPlanComplete{
    NSString* soupId = self.updateCheckinVisitPlanRequest.name;
    VFDAO* vf = [VFDAO sharedInstance];
    [vf removePendingRecordFromTable:DB_TABLE_VISIT_PLAN soupId:soupId];
}




//Checkin comment document
//-(void) uploadNextDirtyCommentMedia{
//    if([self.allDirtyCommentMedia count]>0){
//        NSArray* commentMediaArray = [NSArray arrayWithArray: self.allDirtyCommentMedia];
//        self.allDirtyCommentMedia = nil;
//        
//        [[ImageSaver shareInstance] uploadImagesWithFeedItemIDArray:commentMediaArray delegate:self];
//        
//    }else{
//        [self uploadNextDirtyTask];
//    }
//}

-(void) onAllImageUploadedCompleted{
    [self uploadNextPendingCommentWithResponse:nil];
}

-(void) onImageUploadedCompleted:(NSString*)feedId{
    
}

-(void) onImageUploadedFail:(NSString*)feedId error:(NSError*)error{
    NSLog(@"onImageUploadedFail fail feedId = %@, reason: %@", feedId, error);
    
}





//Checkout visit plan
-(void) uploadNextDirtyCheckoutVisitPlan{
    if([self.allDirtyCheckoutVisitPlans count]>0){
        NSDictionary* data = [NSDictionary dictionaryWithDictionary: [self.allDirtyCheckoutVisitPlans lastObject]];
        [self.allDirtyCheckoutVisitPlans removeLastObject];
        
        //local db record ID
        NSString* sfID = [data objectForKey:@"Id"];
        NSString* dbID = [data objectForKey:@"_soupEntryId"];
        //remove unecessary fields
        data = @{
                 @"Check_Out_Date_Time__c": [data objectForKey:@"Check_Out_Date_Time__c"]
                 };
        
        self.updateCheckoutVisitPlanRequest = [[VFDAO sharedInstance] updateVisitPlanId:sfID data:data delegate:self];
        self.updateCheckoutVisitPlanRequest.name = [NSString stringWithFormat:@"%@", dbID];
    }else{
        self.updateCheckoutVisitPlanRequest = nil;
        if([NSThread isMainThread]){
            self.completeBlock(YES);
        }else{
            __block OfflineSyncHelper* me = self;

            dispatch_async(dispatch_get_main_queue(), ^{
                me.completeBlock(YES);
            });
        }

    }
}
-(void) onUploadCheckoutVisitPlanComplete{
    NSString* soupId = self.updateCheckoutVisitPlanRequest.name;
    VFDAO* vf = [VFDAO sharedInstance];
    [vf removePendingRecordFromTable:DB_TABLE_VISIT_PLAN soupId:soupId];
}



#pragma mark Handle Response Fucntion
-(void) onAppInitRequestComplete:(DAOBaseRequest*)req response:(NSArray*)response{
    if([response count]>0){
        self.updateBlock(req, response, [ShareLocale textFromKey:@"Loading user info..."]);
        NSDictionary* userInfoData = [response objectAtIndex:0];
        
        self.chartDataStoreIdArray = [NSMutableArray array];
        self.salesMixStoreCodeArray = [NSMutableArray array];
        self.inventoryStoreCodeArray = [NSMutableArray array];
        self.stockOnhandStoreCodeArray = [NSMutableArray array];
        self.stockSalesStoreCodeArray = [NSMutableArray array];
        self.surveyMasterIdArray = [NSMutableArray array];
        self.storeInfoCodeArray = [NSMutableArray array];
        self.accountArray = [userInfoData[@"accountList"] mutableCopy];
        
        fxWeek = [[userInfoData objectForKey:@"fxweek"] intValue];
        fxMonth = [[userInfoData objectForKey:@"fxmonth"] intValue];
        fxYear = [[userInfoData objectForKey:@"fxyear"] intValue];
        fxDate = [userInfoData objectForKey:@"fxdate"];
        
        [self loadBrandDetailRequest];
    }else{
        [self onCompleteDownload:NO];
    }
}


-(void) onLoadBrandDetailRequestComplete:(DAOBaseRequest*)req response:(NSArray*)response{
    if([response count]>0){

        self.updateBlock(req, response, [ShareLocale textFromKey:@"Loading user info..."]);

        [self.accountArray removeLastObject];
        NSDictionary* brandData = [response firstObject];
        NSDictionary* storeData = [brandData objectForKey:@"store_data"];
        NSArray* allKeys = [storeData allKeys];
        for(NSString* k in allKeys){
            [self.chartDataStoreIdArray addObject:[[storeData objectForKey:k] objectForKey:@"store_id"]];
            [self.storeInfoCodeArray addObject:[[storeData objectForKey:k] objectForKey:@"store_id"]];
            [self.inventoryStoreCodeArray addObject:k];
            [self.salesMixStoreCodeArray addObject:k];
            [self.stockOnhandStoreCodeArray addObject:k];
            [self.stockSalesStoreCodeArray addObject:k];
        }
        
        if([self.accountArray count]>0){
            [self loadBrandDetailRequest];
        }else{
            [self loadCapacityRequest];
        }
    }

}

-(void) loadBrandDetailRequest{
    NSDictionary* account = [self.accountArray lastObject];

    self.brandDetailRequest = [[VFDAO sharedInstance] selectBrandDetailDataWithDelegate:self parameter:@{@"action":@"load_data", @"code":account[@"Code__c"], @"countryCode":account[@"Country_Code__c"], @"fxdate": fxDate, @"fxyr":[NSString stringWithFormat:@"%d", fxYear],@"fxmo":[NSString stringWithFormat:@"%d", fxMonth],@"fxwk":[NSString stringWithFormat:@"%d", fxWeek]}];

}


-(void) loadCapacityRequest{
    self.capacityRequest = [[VFDAO sharedInstance] selectCapacityWithDelegate:self];
}

-(void) onCapacityRequestComplete:(DAOBaseRequest*)req response:(NSArray*)response{
    self.updateBlock(req, response, [ShareLocale textFromKey:@"Loading capacity..."]);
    [self loadSalesTargetsRequest];
}

-(void) loadSalesTargetsRequest{
    self.targetsRequest = [[VFDAO sharedInstance] selectTargetsWithDelegate:self];
}

-(void) onSalesTargetRequest:(DAOBaseRequest*)req response:(NSArray*)response{
    self.updateBlock(req, response, [ShareLocale textFromKey:@"Loading sales targets..."]);
    [self loadUsageReport];
}

-(void) loadUsageReport{
    if(self.offlineDownloadOption == 100){
        self.commentsRequest = [[VFDAO sharedInstance] selectCommentsWithDelegate:self withStoreID:nil];
        return;
    }
    self.usageRequest = [[VFDAO sharedInstance] selectUsageReportWithDelegate:self];
}

-(void) onUsageRequestComplete:(DAOBaseRequest*)req response:(NSArray*)response{
    self.updateBlock(req, response, [ShareLocale textFromKey:@"Loading user info..."]);
    if([response count]>0){
        [self loadNextStoreInfo];
    }else{
        [self onCompleteDownload:NO];
    }
}


-(void) loadNextStoreInfo{
    if([self.storeInfoCodeArray count]>0){
        NSString* sid = [[self.storeInfoCodeArray lastObject] copy];
        [self.storeInfoCodeArray removeLastObject];
        self.storeInfoRequest = [[VFDAO sharedInstance] selectStoreWithDelegate:self storeId:sid];
        self.updateBlock(self.storeInfoRequest, nil, [ShareLocale textFromKey:@"Loading Store Info..."]);
    }else{
        self.storeInfoRequest = nil;
        self.commentsRequest = [[VFDAO sharedInstance] selectCommentsWithDelegate:self withStoreID:nil];
    }
}


-(void) onInventoryRequestComplete{//Inventory tab, Fixed with latest cut to date
    if([self.inventoryStoreCodeArray count]>0){
        NSString* storeCode = [[self.inventoryStoreCodeArray lastObject] copy];
        [self.inventoryStoreCodeArray removeLastObject];
        
        VFDAO* vf = [VFDAO sharedInstance];
        self.inventoryRequest = [vf selectInventoryWithDelegate:self storeCode: storeCode ];
        self.updateBlock(self.inventoryRequest, nil, [ShareLocale textFromKey:@"Loading inventory..."]);
    }else{
        self.inventoryRequest = nil;
        [self loadStockAndSales];

    }
}



-(void) onStockAndSalesRequestComplete:(NSArray*)response{
    self.updateBlock(self.stockSalesRequest, response, [ShareLocale textFromKey:@"Loading stock and sales..."]);
    for(NSDictionary* data in response){
        NSString* type = [data objectForKey:@"Type__c"];
        if ([type isEqualToString:@"C"]){
            if(self.marketingCutToDate==nil) self.marketingCutToDate = [data objectForKey:@"Cut_To_Day__c"];
            break;
        }
    }
    if(self.marketingCutToDate!=nil) [self.stockSalesCutDayDict setObject:self.marketingCutToDate forKey:self.lastStockSalesStoreCode];
    self.lastStockSalesStoreCode = nil;

}

-(void) loadStockAndSales{
    if([self.stockSalesStoreCodeArray count]>0){
        
        NSString* storeCode = [[self.stockSalesStoreCodeArray lastObject] copy];
        [self.stockSalesStoreCodeArray removeLastObject];
        self.lastStockSalesStoreCode = storeCode;

        VFDAO* vf = [VFDAO sharedInstance];
        self.stockSalesRequest = [vf selectStockAndSalesWithDelegate:self storeCode:storeCode year: fxYear week: fxWeek-1 ];
    }else{
        self.stockSalesRequest = nil;
        
        [self onStockOnhandRequestComplete:nil];
    }
}

-(void) onSalesMixRequestComplete:(NSArray*)response{//Marketing tab,
    self.updateBlock(self.salesMixRequest, response, [ShareLocale textFromKey:@"Loading Sales Mix..."]);
    if([self.salesMixStoreCodeArray count]>0){
        NSString* storeCode = [[self.salesMixStoreCodeArray lastObject] copy];
        [self.salesMixStoreCodeArray removeLastObject];
        VFDAO* vf = [VFDAO sharedInstance];
        
        self.salesMixRequest = [vf selectSalesMixWithDelegate:self storeCode:storeCode  year: fxYear week:fxWeek-1 ];
    }else{
        self.salesMixRequest = nil;
        [self loadSurveyMaster];

    }
}


-(void) onStockOnhandRequestComplete:(NSArray*)response{//Marketing tab,
    self.updateBlock(self.stockOnHandRequest, response, [ShareLocale textFromKey:@"Loading Stock on hand..."]);
    if([self.stockOnhandStoreCodeArray count]>0){
        NSString* storeCode = [[self.stockOnhandStoreCodeArray lastObject] copy];
        [self.stockOnhandStoreCodeArray removeLastObject];
        VFDAO* vf = [VFDAO sharedInstance];

        NSString* d = [self.stockSalesCutDayDict objectForKey:storeCode];
        if(d!=nil){
            self.stockOnHandRequest = [vf selectStockOnHandWithDelegate:self storeCode: storeCode date: d ];
        }else{
            [self onStockOnhandRequestComplete: response];
        }
    }
    else{
        self.stockOnHandRequest = nil;
        [self onSalesMixRequestComplete:response];
        
    }
}




-(void) loadNextCommentDocument{
    
    if([self.commentMediaDictList count] >0){
        NSDictionary* data = [[self.commentMediaDictList lastObject] copy];
        [self.commentMediaDictList removeLastObject];
        NSString* feedId = [data objectForKey:@"feedId"];
//        NSString* type = [data objectForKey:@"type"];
        self.commentMediaDownloadRequest = [[ImageLoader shareInstance] downloadImageWithFeedID:feedId delegate:self];
        self.updateBlock(nil, nil, [ShareLocale textFromKey:@"Loading comments..."]);
    }
    else
    {
        self.commentMediaDownloadRequest = nil;
        self.notificationRequest = [[VFDAO sharedInstance] selectBroadcastNoticeWithDelegate:self];
    }
    
}




-(void) onAllCommentRequestCompleteWithResponse:(NSArray*)records{
    NSLog(@"onAllCommentRequestCompleteWithResponse=%@", records);
    
    self.commentMediaDictList = [NSMutableArray array];
    
    for(NSDictionary* docData in records){
        NSDictionary* mediaResponse = [docData objectForKey:@"Shared_Media__r"];
        if(mediaResponse!=nil && mediaResponse!=(id)[NSNull null]){
            NSArray* medias = [mediaResponse objectForKey:@"records"];
            if([medias count] > 0){
                NSDictionary* mediaData = [medias firstObject];
                NSString* feedId = [mediaData objectForKey:@"emfa__FeedItemId__c"];
                NSString* ext = [mediaData objectForKey:@"emfa__File_Ext__c"];
                [self.commentMediaDictList addObject: @{@"feedId":feedId, @"type":ext} ];
            }
        }
    }
    
    if(self.offlineDownloadOption==100){
        self.notificationRequest = [[VFDAO sharedInstance] selectBroadcastNoticeWithDelegate:self];
    }
    else {
        [self loadNextCommentDocument];
    }
    

}



-(void) onChartDataRequestComplete{
    if ([self.chartDataStoreIdArray count]>0 && !skipChartDataRequest){
        NSString* storeId = [[self.chartDataStoreIdArray lastObject] copy];
        [self.chartDataStoreIdArray removeLastObject];
        self.chartDataRequest = [[VFDAO sharedInstance] selectStoreChartDataWithDelegate:self storeId:storeId];
        self.updateBlock(self.chartDataRequest, nil, [ShareLocale textFromKey:@"Loading diagram..."]);
    }
    else{
        self.chartDataRequest = nil;
        [self onInventoryRequestComplete];
    }
}


-(void) loadSurveyQuestion{
    self.updateBlock(self.surveyQUestionRequest, nil, [ShareLocale textFromKey:@"Loading form question..."]);
    if([self.surveyMasterIdArray count]>0){
        NSString* masterId = [[self.surveyMasterIdArray lastObject] copy];
        [self.surveyMasterIdArray removeLastObject];
        self.surveyQUestionRequest = [[VFDAO sharedInstance] selectQuestionsWithDelegate:self withMasterID:masterId ];
    }
    else{
        self.surveyQUestionRequest = nil;
        if(self.offlineDownloadOption==100){
            [self loadVisitPlanList];
        }
        else{
            [self loadAllDocumentList];
        }
    }

}


-(void) handleSurveyMasterResponse:(NSArray*)response{
    for(NSDictionary* master in response){
        [self.surveyMasterIdArray addObject:[master objectForKey:@"Id"]];
    }
}

-(void) loadSurveyMaster{
    self.surveyMasterRequest = [[VFDAO sharedInstance] selectSurveyMasterDataWithDelegate:self];
}
//-(void) loadSurveyMasterMSP{
//    self.surveyMasterMSPRequest = [[VFDAO sharedInstance] selectSurveyTemplatesWithDelegate:self surveyType:@"MSP Survey"];
//
//}
//
//-(void) loadSurveyMasterAudit{
//    self.surveyMasterAuditRequest = [[VFDAO sharedInstance] selectSurveyTemplatesWithDelegate:self surveyType:@"SOP Audit"];
//}
//
//-(void) loadSurveyMasterVisit{
//    self.surveyMasterVisitRequest = [[VFDAO sharedInstance] selectSurveyTemplatesWithDelegate:self surveyType:@"Visit Form"];
//}

-(void) loadSurveyMSP{
    self.updateBlock(self.surveyMSPRequest, nil, [ShareLocale textFromKey:@"Loading MSP form..."]);
    self.surveyMSPRequest = [[VFDAO sharedInstance] selectSurveysFormWithDelegate:self surveyType:@"MSP Survey"];
}

-(void) loadSurveyAudit{
    self.updateBlock(self.surveyAuditRequest, nil, [ShareLocale textFromKey:@"Loading Audit form..."]);
    self.surveyAuditRequest = [[VFDAO sharedInstance] selectSurveysFormWithDelegate:self surveyType:@"SOP Audit"];
}

-(void) loadSurveyVisit{
    self.updateBlock(self.surveyVisitRequest, nil, [ShareLocale textFromKey:@"Loading Visit form..."]);
    self.surveyVisitRequest = [[VFDAO sharedInstance] selectSurveysFormWithDelegate:self surveyType:@"Visit Form"];
}


-(void) loadTBLDocumentList{
    self.updateBlock(self.tblKnowledgeRequest, nil, [ShareLocale textFromKey:@"Loading TBL document..."]);
    self.tblKnowledgeRequest = [[VFDAO sharedInstance] selectKnowledgeResourcesWithDelegate:self brandCode: @"TBL"];
}

-(void) loadVANsDocumentList{
    self.updateBlock(self.vanKnowledgeRequest, nil, [ShareLocale textFromKey:@"Loading VANS document..."]);
    self.vanKnowledgeRequest = [[VFDAO sharedInstance] selectKnowledgeResourcesWithDelegate:self brandCode: @"VAN"];
}

-(void) loadKPLDocumentList{
    self.updateBlock(self.kplKnowledgeRequest, nil, [ShareLocale textFromKey:@"Loading KPL document..."]);
    self.kplKnowledgeRequest = [[VFDAO sharedInstance] selectKnowledgeResourcesWithDelegate:self brandCode: @"KPL"];
}

-(void) loadWGRDocumentList{
    self.updateBlock(self.tblKnowledgeRequest, nil, [ShareLocale textFromKey:@"Loading WGR document..."]);
    self.wgrKnowledgeRequest = [[VFDAO sharedInstance] selectKnowledgeResourcesWithDelegate:self brandCode: @"TBL"];
}

-(void) loadTNFDocumentList{
    self.updateBlock(self.vanKnowledgeRequest, nil, [ShareLocale textFromKey:@"Loading TNF document..."]);
    self.tnfKnowledgeRequest = [[VFDAO sharedInstance] selectKnowledgeResourcesWithDelegate:self brandCode: @"VAN"];
}

-(void) loadLEEDocumentList{
    self.updateBlock(self.kplKnowledgeRequest, nil, [ShareLocale textFromKey:@"Loading LEE document..."]);
    self.leeKnowledgeRequest = [[VFDAO sharedInstance] selectKnowledgeResourcesWithDelegate:self brandCode: @"KPL"];
}

-(void) loadAllDocumentList{
    self.updateBlock(self.allKnowledgeRequest, nil, [ShareLocale textFromKey:@"Loading VF document..."]);
    self.allKnowledgeRequest = [[VFDAO sharedInstance] selectKnowledgeResourcesWithDelegate:self brandCode: @"All"];
}

-(void) loadVisitPlanList{
    self.updateBlock(self.allVisitPlanRequest, nil, [ShareLocale textFromKey:@"Loading Visit Plans..."]);
    self.allVisitPlanRequest = [[VFDAO sharedInstance] selectVisitationPlansWithDelegate:self];
}

-(void) handleKnowledgeResponse:(NSArray*)records{
    
    NSLog(@"handleKnowledgeResponse=%@", records);
    
    for(NSDictionary* docData in records){
        NSDictionary* mediaResponse = [docData objectForKey:@"Shared_Media__r"];
        if(mediaResponse!=nil && mediaResponse!=(id)[NSNull null]){
            NSArray* medias = [mediaResponse objectForKey:@"records"];
            if([medias count] > 0){
                NSDictionary* mediaData = [medias firstObject];
                NSString* feedId = [mediaData objectForKey:@"emfa__FeedItemId__c"];
                NSString* ext = [mediaData objectForKey:@"emfa__File_Ext__c"];
                [self.documentDictList addObject: @{@"feedId":feedId, @"type":ext} ];
            }
        }
    }
}


-(void) loadNextDocment{
    self.updateBlock(nil, nil, [ShareLocale textFromKey:@"Loading documents..."]);
    if([self.documentDictList count] >0){
        NSDictionary* data = [[self.documentDictList lastObject] copy];
        [self.documentDictList removeLastObject];
        NSString* feedId = [data objectForKey:@"feedId"];
        NSString* type = [data objectForKey:@"type"];
        self.knowledgeMediaDownloadRequest = [[ImageLoader shareInstance] downloadDocWithFeedID:feedId type:type delegate:self];
    }
    else{
        self.knowledgeMediaDownloadRequest = nil;
        [self onCompleteDownload:YES];
    }
}


#pragma mark Final Compelte Handler
-(void) onCompleteDownload:(BOOL)success{
    __block OfflineSyncHelper* me = self;

    dispatch_async(dispatch_get_main_queue(), ^{
        me.completeBlock(success);
    });
}




#pragma mark - Image Delegate
-(void) onRequestSuccessOfRequest:(ImageLoaderRequest *)req feedItemID:(NSString *)feedItemId response:(id)jsonResponse contentType:(NSString *)contentType{
    if(self.commentMediaDownloadRequest == req){
        [self loadNextCommentDocument];
    }
    else if (self.knowledgeMediaDownloadRequest == req){
        [self loadNextDocment];
    }
    else{
        NSAssert(NO, @"No media request match");
    }

}

-(void) onRequestFailOfRequest:(ImageLoaderRequest *)req feedItemID:(NSString *)feedItemId error:(NSError *)error{
    NSAssert(NO, @"onRequestFailOfRequest %@", error);
    if(self.commentMediaDownloadRequest == req){
        [self loadNextCommentDocument];
    }
    else if (self.knowledgeMediaDownloadRequest == req){
        [self loadNextDocment];
    }
    else{
        NSAssert(NO, @"No media request match %@  %@",feedItemId,  error);
    }
}



#pragma mark - Network Delegate
-(void)daoRequest:(DAOBaseRequest*)request queryOnlineDataSuccess:(NSArray*) response{
    if(self.appInitRequest == request){
        DAOBaseRequest* req = [self.appInitRequest retain];
        self.appInitRequest = nil;
        NSLog(@"appInitRequest done");
        [self onAppInitRequestComplete:req response:response ];
        [req release];
    }
    else if(self.brandDetailRequest == request){
        DAOBaseRequest* req = [self.brandDetailRequest retain];
        self.brandDetailRequest = nil;
        NSLog(@"brandDetailRequest done");
        [self onLoadBrandDetailRequestComplete:req response:response ];
        [req release];
    }
    else if (self.usageRequest == request){
        DAOBaseRequest* req = [self.usageRequest retain];
        self.appInitRequest = nil;
        NSLog(@"usageRequest done");
        [self onUsageRequestComplete:req response:response ];
        [req release];
        req = nil;
    }
    else if (self.commentsRequest == request){
        self.commentsRequest = nil;
        self.updateBlock(self.commentsRequest, response, [ShareLocale textFromKey:@"Loading comments..."]);
        NSLog(@"allCommentsRequest done");
        [self onAllCommentRequestCompleteWithResponse:response];
    }
    else if (self.capacityRequest == request){
        DAOBaseRequest* req = [self.capacityRequest retain];
        self.capacityRequest = nil;
        NSLog(@"capacityRequest done");
        [self onCapacityRequestComplete:request response:response];
        [req release];
        req = nil;
    }
    else if (self.targetsRequest == request){
        DAOBaseRequest* req = [self.targetsRequest retain];
        self.targetsRequest = nil;
        NSLog(@"targetsRequest done");
        [self onSalesTargetRequest:request response:response];
        [req release];
        req = nil;
    }
    else if (self.notificationRequest == request){
        self.updateBlock(self.notificationRequest, response, [ShareLocale textFromKey:@"Loading Notification..."]);
        self.notificationRequest = nil;
        NSLog(@"notificationRequest done");
        if(self.offlineDownloadOption==100){
            [self loadSurveyMaster];
        }else{
            [self onChartDataRequestComplete];
        }
    }
    else if (self.chartDataRequest == request){
        self.chartDataRequest = nil;
        NSLog(@"chartDataRequest done");
        [self onChartDataRequestComplete];
    }
    else if (self.inventoryRequest == request){
        self.inventoryRequest = nil;
        NSLog(@"inventoryRequest done");
        [self onInventoryRequestComplete];
    }
    else if (self.stockSalesRequest == request){
        self.stockSalesRequest = nil;
        NSLog(@"stockSalesRequest done");
        self.stockSalesCutDayDict = [NSMutableDictionary dictionary];
        [self onStockAndSalesRequestComplete:response];
        [self loadStockAndSales];
    }
    else if (self.salesMixRequest == request){
        self.salesMixRequest = nil;
        NSLog(@"salesMixRequest done");
        [self onSalesMixRequestComplete: nil];
    }
    else if (self.stockOnHandRequest == request){
        self.stockOnHandRequest = nil;
        NSLog(@"stockOnHandRequest done");
        [self onStockOnhandRequestComplete:nil];
    }
//    else if (self.surveyMasterAuditRequest == request){
//        self.updateBlock(self.surveyMasterAuditRequest, response, [ShareLocale textFromKey:@"Loading Audit master..."]);
//        self.surveyMasterAuditRequest = nil;
//        NSLog(@"surveyMasterAuditRequest done");
//        [self handleSurveyMasterResponse:response];
//        [self loadSurveyMasterMSP];
//    }
//    else if (self.surveyMasterMSPRequest == request){
//        self.updateBlock(self.surveyMasterMSPRequest, response, [ShareLocale textFromKey:@"Loading MSP master..."]);
//        self.surveyMasterMSPRequest = nil;
//        [self handleSurveyMasterResponse:response];
//        NSLog(@"surveyMasterMSPRequest done");
//        [self loadSurveyMasterVisit];
//    }
//    else if (self.surveyMasterVisitRequest == request){
//        self.updateBlock(self.surveyMasterVisitRequest, response, [ShareLocale textFromKey:@"Loading Visit master..."]);
//        self.surveyMasterVisitRequest = nil;
//        [self handleSurveyMasterResponse:response];
//        NSLog(@"surveyMasterVisitRequest done");
//        [self loadSurveyMSP];
//    }
    else if (self.surveyMasterRequest == request){
        self.updateBlock(self.surveyMasterRequest, response, [ShareLocale textFromKey:@"Loading survey master..."]);
        self.surveyMasterRequest = nil;
        [self handleSurveyMasterResponse:response];
        [self loadSurveyMSP];
    }
    else if (self.surveyMSPRequest == request){
        self.surveyMSPRequest = nil;
        NSLog(@"surveyMSPRequest done");
        [self loadSurveyVisit];
    }
    else if (self.surveyVisitRequest == request){
        self.surveyVisitRequest = nil;
        NSLog(@"surveyVisitRequest done");
        [self loadSurveyAudit];
    }
    else if (self.surveyAuditRequest == request){
        self.surveyAuditRequest = nil;
        NSLog(@"surveyAuditRequest done");
        [self loadSurveyQuestion];
    }
    else if (self.surveyQUestionRequest == request){
        [self loadSurveyQuestion];
    }
    else if (self.allKnowledgeRequest == request){
        self.documentDictList = [NSMutableArray array];
        self.allKnowledgeRequest = nil;
        [self handleKnowledgeResponse:response];
        NSLog(@"allKnowledgeRequest done");
        [self loadTBLDocumentList];
    }
    else if (self.tblKnowledgeRequest == request){
        self.documentDictList = [NSMutableArray array];
        self.tblKnowledgeRequest = nil;
        [self handleKnowledgeResponse:response];
        NSLog(@"tblKnowledgeRequest done");
        [self loadVANsDocumentList];
    }
    else if (self.vanKnowledgeRequest == request){
        self.vanKnowledgeRequest = nil;
        [self handleKnowledgeResponse:response];
        NSLog(@"vanKnowledgeRequest done");
        [self loadKPLDocumentList];
    }
    else if (self.kplKnowledgeRequest == request){
        self.kplKnowledgeRequest = nil;
        [self handleKnowledgeResponse:response];
        NSLog(@"kplKnowledgeRequest done");
        [self loadLEEDocumentList];
    }
    else if (self.leeKnowledgeRequest == request){
        self.leeKnowledgeRequest = nil;
        [self handleKnowledgeResponse:response];
        NSLog(@"leeKnowledgeRequest done");
        [self loadTNFDocumentList];
    }
    else if (self.tnfKnowledgeRequest == request){
        self.tnfKnowledgeRequest = nil;
        [self handleKnowledgeResponse:response];
        NSLog(@"tnfKnowledgeRequest done");
        [self loadWGRDocumentList];
    }
    else if (self.wgrKnowledgeRequest == request){
        self.wgrKnowledgeRequest = nil;
        [self handleKnowledgeResponse:response];
        NSLog(@"wgrKnowledgeRequest done");
        [self loadVisitPlanList];
    }
    else if (self.allVisitPlanRequest == request){
        NSLog(@"allVisitPlanRequest done");
        if(self.offlineDownloadOption == 100){
            [self onCompleteDownload:YES];
        }else{
            [self loadNextDocment];
        }
    }
    else if (self.uploadCommentRequest == request){
        NSLog(@"uploadCommentRequest done");
        [self onUploadPnedingCommentComplete];
        [self uploadNextPendingCommentWithResponse: response];
    }
    else if (self.uploadVisitPlanRequest == request){
        NSLog(@"uploadVisitPlanRequest done");
        [self onUploadPnedingVisitPlanComplete];
        [self uploadNextPendingVisitPlan];
    }else if (self.storeInfoRequest == request){
        NSLog(@"storeInfoRequest done");
        [self loadNextStoreInfo];
    }
    else if (self.updateTrashedVisitPlanRequest == request){
        NSLog(@"updateTrashedVisitPlanRequest done");
        [self onUploadTrashedVisitPlanComplete];
        [self uploadNextDirtyTrashVisitPlan];
    }
    else if (self.updateCheckoutVisitPlanRequest == request){
        NSLog(@"updateCheckoutVisitPlanRequest done");
        [self onUploadCheckoutVisitPlanComplete];
        [self uploadNextDirtyCheckoutVisitPlan];
    }
    else if (self.updateCheckinVisitPlanRequest == request){
        NSLog(@"updateCheckinVisitPlanRequest done");
        [self onUploadCheckinVisitPlanComplete];
        [self uploadNextDirtyCheckinVisitPlan];
    }
    else if (self.updateVisitPlanTaskRequest == request){
        NSLog(@"updateVisitPlanTaskRequest done");
        [self onUploadVisitPlanTaskComplete];
        [self uploadNextDirtyTask];
    }
    else if (self.updateSurveyRequest == request){
        NSLog(@"updateSurveyRequest done");
        [self onUploadSurveyComplete];
        [self uploadNextDirtySurvey];
        
    }
    

}



-(void) daoRequest:(DAOBaseRequest*)request queryOnlineDataError:(NSString*) response code:(int)code{
    self.completeBlock(NO);
}








@end
