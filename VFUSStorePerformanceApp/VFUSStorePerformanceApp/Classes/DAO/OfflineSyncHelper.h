//
//  OfflineSyncHelper.h
//  VFStorePerformanceApp_Dev2
//
//  Created by Billy Lo on 16/7/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VFDAO.h"
#import "ImageLoader.h"
#import "ImageSaver.h"

@interface OfflineSyncHelper : NSObject<DAODelegate, ImageLoaderDelegate, ImageSaverDelegate>{
    
    BOOL skipChartDataRequest;
    int fxYear;
    int fxWeek;
    int fxMonth;
    NSDate* fxDate;
}

+(id) sharedInstance;

@property (nonatomic, retain) NSMutableDictionary* stockSalesCutDayDict;

@property (nonatomic, copy) void (^completeBlock)(BOOL success);
@property (nonatomic, copy) void (^updateBlock)(DAOBaseRequest* request, NSArray* response, NSString* text);


@property (nonatomic, retain) NSMutableArray* documentDictList;//eg: [@{"feedId":@"xxx", @"type":@"pdf"}, etc...]

@property (nonatomic, retain) NSMutableArray* accountArray;
@property (nonatomic, retain) NSMutableArray* storeInfoCodeArray;
@property (nonatomic, retain) NSMutableArray* surveyMasterIdArray;
@property (nonatomic, retain) NSMutableArray* chartDataStoreIdArray;
@property (nonatomic, retain) NSMutableArray* inventoryStoreCodeArray;
@property (nonatomic, retain) NSMutableArray* stockSalesStoreCodeArray;
@property (nonatomic, retain) NSMutableArray* salesMixStoreCodeArray;
@property (nonatomic, retain) NSMutableArray* stockOnhandStoreCodeArray;

@property (nonatomic, retain) NSMutableArray* allPendingCommentsRecords;
@property (nonatomic, retain) NSMutableArray* allPendingVisitPlansRecords;
@property (nonatomic, retain) NSMutableArray* allDirtyTrashedVisitPlans;//trashed
@property (nonatomic, retain) NSMutableArray* allDirtyCheckoutVisitPlans;//checkout
@property (nonatomic, retain) NSMutableArray* allDirtyCheckinVisitPlans;//checkin
@property (nonatomic, retain) NSMutableArray* allDirtyVisitPlanTasks;//todo or doc
@property (nonatomic, retain) NSMutableArray* allDirtySurveys;//Survey
@property (nonatomic, retain) NSMutableArray* allDirtyCommentMedia;//Media of comment


//Upload
@property (nonatomic, retain) DAOBaseRequest* uploadCommentRequest;
@property (nonatomic, retain) DAOBaseRequest* uploadVisitPlanRequest;
@property (nonatomic, retain) DAOBaseRequest* updateTrashedVisitPlanRequest;
@property (nonatomic, retain) DAOBaseRequest* updateCheckoutVisitPlanRequest;
@property (nonatomic, retain) DAOBaseRequest* updateCheckinVisitPlanRequest;
@property (nonatomic, retain) DAOBaseRequest* updateVisitPlanTaskRequest;
@property (nonatomic, retain) DAOBaseRequest* updateSurveyRequest;

//Download - basic and Visit request
@property (nonatomic, retain) DAOBaseRequest* appInitRequest;
@property (nonatomic, retain) DAOBaseRequest* brandDetailRequest;
@property (nonatomic, retain) DAOBaseRequest* commentsRequest;
@property (nonatomic, retain) DAOBaseRequest* notificationRequest;
//@property (nonatomic, retain) DAOBaseRequest* surveyMasterMSPRequest;
//@property (nonatomic, retain) DAOBaseRequest* surveyMasterAuditRequest;
//@property (nonatomic, retain) DAOBaseRequest* surveyMasterVisitRequest;
@property (nonatomic, retain) DAOBaseRequest* surveyMasterRequest;
@property (nonatomic, retain) DAOBaseRequest* surveyMSPRequest;
@property (nonatomic, retain) DAOBaseRequest* surveyAuditRequest;
@property (nonatomic, retain) DAOBaseRequest* surveyVisitRequest;
@property (nonatomic, retain) DAOBaseRequest* allVisitPlanRequest;
@property (nonatomic, retain) DAOBaseRequest* surveyQUestionRequest;

@property (nonatomic, retain) DAOBaseRequest* capacityRequest;
@property (nonatomic, retain) DAOBaseRequest* targetsRequest;

@property (nonatomic, retain) NSMutableArray* commentMediaDictList;
@property (nonatomic, retain) ImageLoaderRequest* commentMediaDownloadRequest;
@property (nonatomic, retain) ImageLoaderRequest* knowledgeMediaDownloadRequest;
//Download - other request
@property (nonatomic, retain) DAOBaseRequest* usageRequest;
@property (nonatomic, retain) DAOBaseRequest* storeInfoRequest;
@property (nonatomic, retain) DAOBaseRequest* chartDataRequest;
@property (nonatomic, retain) DAOBaseRequest* inventoryRequest;
@property (nonatomic, retain) DAOBaseRequest* stockSalesRequest;
@property (nonatomic, retain) DAOBaseRequest* salesMixRequest;
@property (nonatomic, retain) DAOBaseRequest* stockOnHandRequest;
@property (nonatomic, retain) DAOBaseRequest* allKnowledgeRequest;


@property (nonatomic, retain) DAOBaseRequest* tblKnowledgeRequest;
@property (nonatomic, retain) DAOBaseRequest* vanKnowledgeRequest;
@property (nonatomic, retain) DAOBaseRequest* kplKnowledgeRequest;
@property (nonatomic, retain) DAOBaseRequest* leeKnowledgeRequest;
@property (nonatomic, retain) DAOBaseRequest* wgrKnowledgeRequest;
@property (nonatomic, retain) DAOBaseRequest* tnfKnowledgeRequest;



@property (nonatomic, retain) NSDate* marketingCutToDate;
@property (nonatomic, retain) NSString* lastStockSalesStoreCode;

@property (nonatomic, retain) NSDictionary* commentInProgress;


//0 - Download everything
//100 - Download only visit data
@property (nonatomic, assign) int offlineDownloadOption;


-(void) startDownloadWithCompleteBlock: (void (^)(BOOL success))successBlock updateBlock:(void (^)(DAOBaseRequest* req, NSArray* response, NSString* messageText))updateBlock;




/**
 *   @brief  start the async process of uploading local unsaved data to SF server.
 *
 *   @param  successBlock
 *
 *   @return abc
 *
 */
-(void) startUploadDataWithCompleteBlock: (void (^)(BOOL success))successBlock;


@end
