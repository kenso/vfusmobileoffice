//
//  SaveOrderRestAction.m
//
//  Created by Developer 2 on 12/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import "CustomWebserviceRestApi.h"
#import "SFOAuthCoordinator.h"
#import <SalesforceNetworkSDK/SFNetworkEngine.h>
#import "SFRestAPI.h"
#import "VFDAO.h"

/**
 *
 *  Should not used in the future as it does not handle error properly.
 *  Error may happened if access token is expiried.
 *
 */
@implementation CustomWebserviceRequest

@end

@implementation CustomWebserviceRestApi

+ (id)sharedInstance {
    static VFDAO *sharedMyInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyInstance = [[self alloc] init];
    });
    return sharedMyInstance;
}



-(CustomWebserviceRequest*) sendRequestASyncWithApiName:(NSString*)apiName data:(NSDictionary*)dict delegate:(id<CustomWebserviceRestDelegate>)delegate postBlock: (void (^) (NSArray* records))postDelegate{
    NSLog(@"CustomWebserviceRestApi apiName=%@, parameter=%@ ", apiName, dict);
    return [self sendRequestASyncWithApiName: apiName data:dict method:@"POST" delegate:delegate postBlock:postDelegate];
}

//return a allocated CustomWebserviceRequest.  Caller should be response the memory control
-(CustomWebserviceRequest*) sendRequestASyncWithApiName:(NSString*)apiName data:(NSDictionary*)dict method:(NSString*)method  delegate:(id<CustomWebserviceRestDelegate>)delegate postBlock: (void (^) (NSArray* records))postDelegate{
    
    NSLog(@"CustomWebserviceRestApi apiName=%@, parameter=%@, method=%@", apiName, dict, method);
//    NSTimeInterval startTime = [NSDate date].timeIntervalSince1970;
//    NSError* error;
//    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    
//    SFOAuthCoordinator *coord = [SFRestAPI sharedInstance].coordinator;
//    NSString* token = coord.credentials.accessToken;
    
//    NSOperationQueue *queue = [[[NSOperationQueue alloc] init] autorelease];
//    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"%@/services/apexrest/%@",coord.credentials.instanceUrl.absoluteString, apiName]];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//    [request setHTTPMethod:method];
//    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    if([method isEqualToString:@"POST"]) [request setValue:[NSString stringWithFormat:@"%i", jsonData.length] forHTTPHeaderField: @"Content-Length"];
//    [request setValue: [NSString stringWithFormat:@"OAuth %@", token ] forHTTPHeaderField:@"Authorization"];
//    if([method isEqualToString:@"POST"]) [request setHTTPBody:jsonData];
    self.postBlock = postDelegate;
    
    self.request = [[CustomWebserviceRequest alloc] init] ;
    self.request.name = apiName;
    SFRestMethod m;
    if([method isEqualToString:@"GET"]){
        m = SFRestMethodGET;
    }else if([method isEqualToString:@"POST"]){
        m = SFRestMethodPOST;
    }else if([method isEqualToString:@"PUT"]){
        m = SFRestMethodPUT;
    }else if([method isEqualToString:@"DELETE"]){
        m = SFRestMethodDELETE;
    }
    else if([method isEqualToString:@"PATCH"]){
        m = SFRestMethodPATCH;
    }else{
        m = SFRestMethodGET;
    }
    
    SFRestRequest *sfRequest = [[SFRestRequest alloc] init] ;
    sfRequest.endpoint = [NSString stringWithFormat:@"/services/apexrest/%@", apiName];
    sfRequest.method = m;
    sfRequest.path = [NSString stringWithFormat:@"/services/apexrest/%@", apiName];
    if(dict!=nil) sfRequest.queryParams = dict;
    
    self.request.sfRequest = sfRequest;
    self.delegate = delegate;
    [[SFRestAPI sharedInstance] send:self.request.sfRequest delegate:self];
    
//
//    
//    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse* res, NSData* data, NSError* err){
//        
//        NSLog(@"CustomWebserviceRestApi consume %.4f seconds", [NSDate date].timeIntervalSince1970 - startTime);
//
//        
//        if(err){
//            NSLog(@"CustomWebserviceRestApi with error: %@", err);
//            
//            dispatch_async(dispatch_get_main_queue(), ^(void) {
//                [delegate onRequestFail:req error:[NSString stringWithFormat:@"%@",err ]];
//            });
//
//            return;
//        }
//        
//        NSError *e = nil;
//        id json = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &e];
//        
////        NSLog(@"CustomWebserviceRestApi json = %@", json);
//
//        
//        if(e!=nil || json==nil){
//            NSLog(@"CustomWebserviceRestApi with parsing error: %@", err);
//            dispatch_async(dispatch_get_main_queue(), ^(void) {
//
//                [delegate onRequestFail:req error:[NSString stringWithFormat:@"%@",e ]];
//            });
//
//        }else{
//            if([json isKindOfClass:[NSDictionary class]]){
//                NSString* success = (NSString*)[json objectForKey:@"success"];
//                if(success!=nil && [@"true" isEqualToString:success]){
//                    postDelegate(@[json]);
//                    dispatch_async(dispatch_get_main_queue(), ^(void) {
//                        [delegate onRequestComplete:req response:json];
//                    });
//                    
//                    
//                }else{
//                    NSLog(@"CustomWebserviceRestApi error#2 %@", json);
//                    dispatch_async(dispatch_get_main_queue(), ^(void) {
//                        [delegate onRequestFail:req error:@"Request return fail"];
//                        
//                    });
//                    
//                }
//            }else{
//                dispatch_async(dispatch_get_main_queue(), ^(void) {
//                    NSArray* t = json;
//                    NSDictionary* tt = [t objectAtIndex:0];
//                    [delegate onRequestFail:req error:[tt objectForKey:@"message"]];
//                    
//                });
//            }
//        }
//        
//    }];
    
    return self.request;
    
    
}





- (void)request:(SFRestRequest *)request didLoadResponse:(id)dataResponse{
    //    NSLog(@"VFDAO::didLoadResponse() dataResponse=%@", dataResponse);
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        
        //NSDictionary* response = dataResponse;
        NSError* e = nil;
        NSDictionary* resultData;
        
        if( [dataResponse isKindOfClass:[NSMutableData class]] ){
            resultData =  [NSJSONSerialization JSONObjectWithData:dataResponse options:0 error:&e];
            if(e!=nil){
                NSLog(@"error=%@", e);
                [self.delegate onRequestFail:self.request error:@"Parse fail"];
                return;
            }
        }else{
            resultData = dataResponse;
        }

        
        if([resultData isKindOfClass:[NSDictionary class]]){
            NSString* success = (NSString*)[resultData objectForKey:@"success"];
            if(success!=nil && [@"true" isEqualToString:success]){
                self.postBlock(@[resultData]);
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    [self.delegate onRequestComplete:self.request response:resultData];
                });

            }else{
                NSLog(@"CustomWebserviceRestApi error#13 %@", resultData);
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    [self.delegate onRequestFail:self.request error:@"Request return fail"];

                });

            }
        }else{
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                NSLog(@"CustomWebserviceRestApi error#14 %@", resultData);
                NSArray* t = (NSArray*)resultData;
                NSDictionary* tt = [t objectAtIndex:0];
                [self.delegate onRequestFail:self.request error:[tt objectForKey:@"message"]];
                
            });
        }
    });

    
}


- (void)request:(SFRestRequest *)request didFailLoadWithError:(NSError*)error{
    NSLog(@"VFDAO::didFailLoadWithError() error=%@", error);
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [self.delegate onRequestFail:self.request error: [NSString stringWithFormat:@"%@",error ] ];
    });
}


- (void)requestDidCancelLoad:(SFRestRequest *)request{
    NSLog(@"Unknown request cancel %@", request);
    dispatch_async(dispatch_get_main_queue(), ^(void) {
         [self.delegate onRequestFail:self.request error: @"Request Cancelled" ];
    });
    
}


- (void)requestDidTimeout:(SFRestRequest *)request{
    NSLog(@"Unknown request timeout %@", request);
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [self.delegate onRequestFail:self.request error: @"Request timeout" ];
    });
}





@end
