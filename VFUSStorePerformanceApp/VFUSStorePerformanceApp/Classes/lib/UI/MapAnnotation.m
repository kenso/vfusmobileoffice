//
//  MapAnnotation.m
//  VF Ordering App
//
//  Created by Developer 2 on 10/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import "MapAnnotation.h"



@implementation MapAnnotation

@synthesize coordinate = _coordinate,
            title = _title,
subtitle = _subtitle;

- (id)initWithCoordinate:(CLLocationCoordinate2D)coord title:(NSString *)title subtitle:(NSString *)subTitle
{
    self = [super init];
    
    if (self != nil)
    {
        _coordinate = coord;
        _title = title;
        _subtitle = subTitle;
    }
    
    return self;
}



- (void)replaceSubtitle:(NSString*)t{
    if(_subtitle!=t){
        [_subtitle release];
        _subtitle = [t retain];
    }
}

@end
