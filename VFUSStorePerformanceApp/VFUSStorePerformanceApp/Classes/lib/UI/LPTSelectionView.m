//
//  LPTSelectionView.m
//  VF Ordering App
//
//  Created by Developer 2 on 9/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import "LPTSelectionView.h"

@implementation LPTSelectionView

- (id)initWithParentViewController:(UIViewController*)vc data:(NSArray*)dataAry delegate:(id<LPTSelectionViewDelegate>)dele
{
    self = [super init];
    if (self) {
        self.dismisserViewController = vc;
        viewDelegate = dele;
        [vc.view addSubview:self];
        self.delegate = self;
        self.dataSource = self;
        self.records = dataAry;
        
        
    }
    return self;
}


//-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
//    return @"Choose action";
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [viewDelegate onSelectionAtIndex: indexPath.row  pickerView:self];
    [self hide];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.records count];
}

-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return LPTSelectionViewCellHeight;
}

-(float)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LPTSelectionViewCellTitleHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LPTSelectionViewCell"];
    if(cell == nil){
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"LPTSelectionViewCell"] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        cell.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
    }
    cell.textLabel.text = [self.records objectAtIndex:indexPath.row];
    return cell;
}


- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, LPTSelectionViewCellTitleHeight)] autorelease];
    [headerView setBackgroundColor:[UIColor lightGrayColor]];
    UILabel* titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.bounds.size.width, LPTSelectionViewCellTitleHeight - 5*2)] autorelease];
    titleLabel.text = @"Choose Action";
    titleLabel.font = [UIFont fontWithName:@"Helvetica Neue Bold" size:18];
    titleLabel.textColor = [UIColor blackColor];
    [headerView addSubview:titleLabel];
    return headerView;
}




- (void)didMoveToSuperview {
    if (self.superview) {
        
        self.frame = CGRectMake(self.superview.bounds.origin.x, self.superview.bounds.size.height, self.superview.bounds.size.width, LPTSelectionViewHeight);
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        
        if (![self.dismisserViewController.view.subviews containsObject:self.dismisserView]) {
            self.dismisserView = UIView.new;
            self.dismisserView.frame = self.dismisserViewController.view.bounds;
            self.dismisserView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            self.dismisserView.backgroundColor = UIColor.blackColor;
            self.dismisserView.alpha = 0.f;
            
            [self.dismisserView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hide)]];
            [self.dismisserViewController.view addSubview:self.dismisserView];
        }
    }
}

- (void)show {
    [self reloadData];
    __block LPTSelectionView* me = self;

    [UIView animateWithDuration:LPTSelectionViewAnimationDuration animations:^{
        me.frame = CGRectMake(me.superview.bounds.origin.x, me.superview.bounds.size.height - LPTSelectionViewHeight, me.superview.bounds.size.width, LPTSelectionViewHeight);

        me.dismisserView.frame = CGRectMake(me.dismisserViewController.view.bounds.origin.x, me.dismisserViewController.view.bounds.origin.y, me.dismisserViewController.view.bounds.size.width, me.dismisserViewController.view.bounds.size.height - LPTSelectionViewHeight);
        me.dismisserView.alpha = LPTSelectionViewDismissViewAlpha;
        
    }];
    
}

- (void)hide {
    __block LPTSelectionView* me = self;
    [UIView animateWithDuration:LPTSelectionViewAnimationDuration animations:^{
        me.frame = CGRectMake(me.superview.bounds.origin.x, me.superview.bounds.size.height, me.superview.bounds.size.width, LPTSelectionViewHeight);
        me.dismisserView.frame = me.dismisserViewController.view.bounds;
        me.dismisserView.alpha = 0.f;
    }];
    
}

@end
