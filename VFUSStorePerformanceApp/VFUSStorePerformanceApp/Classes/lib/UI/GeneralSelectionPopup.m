//
//  GeneralSelectionPopup.m
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 12/5/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "GeneralSelectionPopup.h"

@interface GeneralSelectionPopup ()

@end

@implementation GeneralSelectionPopup


- (id)initAsMultiSelectTableWithDataArray:(NSArray*)rowDataArray selectedArray:(NSArray*)selectedArray{
    self = [super initWithNibName:@"GeneralSelectionPopup" bundle:nil];
    if (self) {
        self.defaultSelectedArray = selectedArray;
        self.recordArray = rowDataArray;
        self.supportMultiSelection = YES;
    }
    return self;
}

- (id)initWithArray:(NSArray*)rowDataArray{
    self = [super initWithNibName:@"GeneralSelectionPopup" bundle:nil];
    if (self) {
        self.recordArray = rowDataArray;
        self.supportMultiSelection = NO;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.recordArray = @[
                             ];
        self.supportMultiSelection = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.allowsMultipleSelection = self.supportMultiSelection;
    [self.tableView reloadData];
    
    for(NSString* val in self.defaultSelectedArray){
        for(NSDictionary* data in self.recordArray){
            if([data[@"val"] isEqualToString:val]){
                [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:[self.recordArray indexOfObject:data] inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
            }
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [_tableView release];
    [_tableView release];
    [super dealloc];
}



#pragma mark - Table view datasource/delegate

-(float) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.recordArray count];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(self.supportMultiSelection){
        //do nth
    }else{
        if(self.delegate!=nil){
            [self.delegate onRowSelected: indexPath.row];
        }
    }
}

-(void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    if(self.supportMultiSelection){
        NSArray *indexPathsForSelectedRows = [self.tableView indexPathsForSelectedRows];
        if(self.delegate!=nil){
            [self.delegate onMultiRowSelected: indexPathsForSelectedRows];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tview cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary* data = [self.recordArray objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tview dequeueReusableCellWithIdentifier:@"GeneralSelectionCell"];
    if(cell == nil){
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"GeneralSelectionCell"] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.textLabel.hidden = YES;
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(20, 5, 300, 40)];
        label.tag = 901;
        label.text = @"";
        [cell addSubview:label];
    }
    
    UILabel* dateLabel = (UILabel*)[cell viewWithTag:901];
    dateLabel.text = [data objectForKey:@"label"];
    cell.tag = indexPath.row;
    return cell;
    
}

@end
