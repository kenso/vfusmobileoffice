//
//  PopupTimePicker.m
//  VFStorePerformanceApp
//
//  Created by Developer 2 on 21/3/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "PopupDateTimePicker.h"

@implementation PopupDateTimePicker

- (id)initWithMode:(UIDatePickerMode)mode
{
    self = [super initWithFrame:CGRectMake(0, 0, 1024, 768)];
    if (self) {
        
        UIView* baseView = [[UIView alloc] initWithFrame:CGRectMake(0, 768 - 216 - 40, 1024, 216 + 40)];
        baseView.tag = 125143;
        [self addSubview:baseView];
        baseView.backgroundColor = [UIColor whiteColor];
        UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
        [self addGestureRecognizer:singleFingerTap];
        [singleFingerTap release];
        
        UIDatePicker *timePick = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, 40, 1024, 216)];
        timePick.tag = 18726;
        timePick.datePickerMode = mode;
        [baseView addSubview:timePick];
        
        [timePick release];
        [baseView release];
    }
    return self;
}


-(void)handleSingleTap:(id)sender{
 
    UIDatePicker* picker = (UIDatePicker*)[self viewWithTag: 18726];
    NSDate* newDate = [[NSDate alloc] initWithTimeInterval:0 sinceDate: picker.date];

    NSLog(@"date = %@", picker.date);
    
    [[self viewWithTag:125143] removeFromSuperview];
    [self removeFromSuperview];
    
    
    if(self.delegate)
        [self.delegate onConfirmPopupDatePicker:self date:newDate];
    

}

- (void)setSelectedDate:(NSDate*)date{
    UIDatePicker* picker = (UIDatePicker*)[self viewWithTag: 18726];
    picker.date = date;
}


@end
