//
//  GeneralSelectionPopup.h
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 12/5/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GeneralSelectionPopupDelegate <NSObject>

@optional
-(void) onRowSelected:(int) index;//used when supportMultiSelection=NO
-(void) onMultiRowSelected:(NSArray*) indexArray; //used when supportMultiSelection=YES

@end

@interface GeneralSelectionPopup : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (retain, nonatomic) IBOutlet UITableView *tableView;

@property (retain, nonatomic) NSArray* defaultSelectedArray;


@property (retain, nonatomic) NSArray* recordArray;//string array

@property (assign, nonatomic) id<GeneralSelectionPopupDelegate> delegate;

@property (assign) BOOL supportMultiSelection;

- (id)initWithArray:(NSArray*)storeArray;
- (id)initAsMultiSelectTableWithDataArray:(NSArray*)rowDataArray selectedArray:(NSArray*)selectedArray;


@end

