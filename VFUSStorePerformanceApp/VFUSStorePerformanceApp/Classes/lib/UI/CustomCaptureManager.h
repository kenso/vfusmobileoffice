//
//  CustomCaptureManager.h
//  wyeth_report_ipad
//
//  Created by Developer on 5/5/14.
//  Copyright (c) 2014 laputab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMedia/CoreMedia.h>
#import <AVFoundation/AVFoundation.h>

@protocol CustomCaptureDelegate <NSObject>

-(void) onCompleteCaptureImage:(UIImageView* )targetView;
-(void) onChooseImageFromGallery:(UIImageView*)targetView;

@end

@interface CustomCaptureManager : NSObject{
    UIButton *captureButton;
    UIView *bgView;
}

+(id) shareInstance;

- (void)resetWithTargetView:(UIImageView *)targetView andDelegate:(id <CustomCaptureDelegate>)delegate;
- (void)resetWithTargetView:(UIImageView *)targetView andButton:(UIButton *)button;
- (void)setUpPreviewRootView:(UIView *)rootView;
- (void)setupAVCapture;
- (void)captureNow;
    
#define kImageCapturedSuccessfully @"imageCapturedSuccessfully"

@property (nonatomic, retain) id<CustomCaptureDelegate> delegate;

@property (nonatomic) BOOL isTakingPhoto;
@property (nonatomic, retain) UIView *rootView;
@property (retain) AVCaptureVideoPreviewLayer *previewLayer;
@property (retain) AVCaptureSession *session;
@property (retain) AVCaptureStillImageOutput *stillImageOutput;
@property (nonatomic, retain) UIImageView *targetImageView;
@property (nonatomic, retain) UIButton *targetButton;
@property (nonatomic, retain) UIImage *stillImage;


@end
