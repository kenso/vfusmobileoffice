//
//  DatePickerSelectionViewController.h
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 21/6/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void (^XYZSimpleBlock)(void);


typedef void (^DatePickerComplete) (NSDate* date);



@interface DatePickerSelectionViewController : UIViewController

- (id)initWithDate:(NSDate*)date andDateMode:(UIDatePickerMode)mode withBlock:(DatePickerComplete) block;

@property (nonatomic, retain) NSDate* date;
@property (nonatomic, assign) UIDatePickerMode mode;

@property (nonatomic, copy) DatePickerComplete callbackBlock;

@end
