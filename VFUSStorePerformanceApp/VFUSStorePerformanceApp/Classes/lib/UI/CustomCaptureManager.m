//
//  CustomCaptureManager.m
//  wyeth_report_ipad
//
//  Created by Developer on 5/5/14.
//  Copyright (c) 2014 laputab. All rights reserved.
//

#import "CustomCaptureManager.h"
#import <ImageIO/ImageIO.h>

@implementation CustomCaptureManager

static CustomCaptureManager* myself;

#define CAPTURE_MANAGER_PREVIEW_WIDTH    400
#define CAPTURE_MANAGER_PREVIEW_HEIGHT   300

+(id) shareInstance{
    if(myself==nil){
        myself = [[CustomCaptureManager alloc] init];
    }
    return myself;
}

- (void)setUpPreviewRootView:(UIView *)rootView{
    self.rootView = rootView;
}

- (void)resetWithTargetView:(UIImageView *)targetView{
    [self resetWithTargetView:targetView andDelegate:nil];
}

- (void)resetWithTargetView:(UIImageView *)targetView andDelegate:(id<CustomCaptureDelegate>)delegate{
    self.delegate = delegate;

    [self cleanSetting];
    
    self.targetImageView = targetView;
    [self setupAVCapture];
    [self setupGalleryButton];
    _isTakingPhoto = YES;
    
}

- (void)resetWithTargetView:(UIImageView *)targetView andButton:(UIButton *)button{
    
    if ( _isTakingPhoto && self.targetButton == button ){
        [self captureNow];
        return;
    }
    
    [self cleanSetting];
    
//    [button addTarget:self action:@selector(onClickButton:) forControlEvents:UIControlEventTouchUpInside];
    self.targetImageView = targetView;
    self.targetButton = button;
    [self setupAVCapture];
    _isTakingPhoto = YES;
}


- (void)setupAVCapture {
    NSError *error = nil;
    
    _session = [AVCaptureSession new];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        [_session setSessionPreset:AVCaptureSessionPreset1280x720];
    else
        [_session setSessionPreset:AVCaptureSessionPresetPhoto];
    
    // Select a video device, make an input
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    AVCaptureDeviceInput *deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
    
    if ([_session canAddInput:deviceInput])
        [_session addInput:deviceInput];
    
    self.stillImageOutput = [[AVCaptureStillImageOutput alloc] init];

    
    NSDictionary *outputSettings = @{ AVVideoCodecKey : AVVideoCodecJPEG};
    [self.stillImageOutput setOutputSettings: outputSettings];
    [_session addOutput:self.stillImageOutput];
    
    
    _previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_session];
    //    [self.view.layer addSublayer:previewLayer];
    [_previewLayer setBackgroundColor:[[UIColor blackColor] CGColor]];
    [_previewLayer setVideoGravity:AVLayerVideoGravityResizeAspect];
    CALayer *rootLayer = self.rootView.layer;// [_targetImageView layer];
    [rootLayer setMasksToBounds:YES];
    
//    [_previewLayer setFrame:[rootLayer bounds]];
    int preview_x = self.rootView.frame.size.width - CAPTURE_MANAGER_PREVIEW_WIDTH;
    int preview_y = 0;
    [_previewLayer setFrame:CGRectMake(preview_x, preview_y, CAPTURE_MANAGER_PREVIEW_WIDTH, CAPTURE_MANAGER_PREVIEW_HEIGHT)];
    
    AVCaptureConnection *previewLayerConnection=_previewLayer.connection;
//    previewLayerConnection.videoScaleAndCropFactor = .5f;// >> Scale?

    if ([previewLayerConnection isVideoOrientationSupported])
        [previewLayerConnection setVideoOrientation:(AVCaptureVideoOrientation)[[UIApplication sharedApplication] statusBarOrientation]];

    if (captureButton == nil){
        captureButton = [[UIButton alloc] init];
        [captureButton addTarget:self action:@selector(onClickCapture:) forControlEvents:UIControlEventTouchUpInside];
    }
    if (bgView == nil ){
        bgView = [[UIView alloc] init];
        [bgView setBackgroundColor:[UIColor colorWithWhite:0.5f alpha:0.7f]];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickBgView:)];
        [bgView setUserInteractionEnabled:YES];
        [bgView addGestureRecognizer:tap];
    }
    [captureButton setFrame: _previewLayer.frame];
    [bgView setFrame:rootLayer.frame];
    
    
    [self.rootView addSubview:bgView];
    [self.rootView addSubview:captureButton];
    [rootLayer addSublayer:_previewLayer];
    
    [_previewLayer setHidden:YES];
    [self performSelector:@selector(showPreviewLayer) withObject:nil afterDelay:1.0];
    
    [_session startRunning];
    
}

- (void)showPreviewLayer{
    [self.previewLayer setHidden:NO];
}


- (void)captureNow{
    
    AVCaptureConnection *videoConnection = nil;
    for (AVCaptureConnection *connection in self.stillImageOutput.connections)
    {
        if ([connection isVideoOrientationSupported])
            [connection setVideoOrientation:(AVCaptureVideoOrientation)[[UIApplication sharedApplication] statusBarOrientation]];
        for (AVCaptureInputPort *port in [connection inputPorts])
        {
            if ([[port mediaType] isEqual:AVMediaTypeVideo] )
            {
                videoConnection = connection;
                break;
            }
        }
        if (videoConnection) { break; }
    }
    
    if(videoConnection!=nil){
        [self.stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection
                                                           completionHandler: ^(CMSampleBufferRef imageSampleBuffer, NSError *error)
         {
             

             NSLog(@"error: %@", error);
                
             NSLog(@"Here is the complete capture");
             CFDictionaryRef exifAttachments = CMGetAttachment( imageSampleBuffer, kCGImagePropertyExifDictionary, NULL);
             if (exifAttachments)
             {
                 // Do something with the attachments.
                 NSLog(@"attachements: %@", exifAttachments);
                 
                 NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
                 UIImage *image = [[UIImage alloc] initWithData:imageData];
                 
                 _targetImageView.image = image;
                 if ( self.delegate != nil ){
                     NSLog(@"onCompleteCaptureImage in CaptureManager with delegate: %@", self.delegate);
                     [self.delegate onCompleteCaptureImage:_targetImageView];
                 }
                 [self cleanSetting];
             }
             else{
                 NSLog(@"no attachments");
                 return;
             }

                
         }];
    }
}

- (void)setupGalleryButton{
    int icon_x = self.rootView.frame.size.width - CAPTURE_MANAGER_PREVIEW_WIDTH-72;

    
    UIButton *galleryButton = [[UIButton alloc] initWithFrame:CGRectMake(
                                                                        icon_x,
                                                                        0,
                                                                        72,
                                                                         72)];
    [galleryButton setImage:[UIImage imageNamed:@"folder_open"] forState:UIControlStateNormal];
    UIColor *bgColor = [UIColor colorWithRed:(8/255.0) green:(75/255.0) blue:(191/255.0) alpha:1.0f];
    [galleryButton setBackgroundColor:bgColor];
    [galleryButton addTarget:self action:@selector(onClickGalleryButton:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:galleryButton];
}

- (void)onClickBgView:(id)sender{
    [self cleanSetting];
}

- (void)onClickCapture:(id)sender{
    [self captureNow];
}

- (void)onClickGalleryButton:(id)sender{
    if ( self.delegate != nil ){
        [self.delegate onChooseImageFromGallery:self.targetImageView];
        [self cleanSetting];
    }
}

- (void)cleanSetting{
    _isTakingPhoto = NO;
    [bgView removeFromSuperview];
    [captureButton removeFromSuperview];
    
    [[_previewLayer session] stopRunning];
    [[_previewLayer session] removeOutput:self.stillImageOutput];
    [_previewLayer removeFromSuperlayer];
}

@end
