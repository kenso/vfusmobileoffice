//
//  MapAnnotation.h
//  VF Ordering App
//
//  Created by Developer 2 on 10/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>


@interface MapAnnotation : NSObject<MKAnnotation>{
    CLLocationCoordinate2D _coordinate;
    NSString* _title;
    NSString* subtitle;
}



- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate title:(NSString*)title subtitle:(NSString*)subtitle;


- (void)replaceSubtitle:(NSString*)t;



@end
