//
//  URLParser.h
//  NextMediaCoupon
//
//  Created by Developer 2 on 29/10/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface URLParser : NSObject {
    NSArray *variables;
}

@property (nonatomic, retain) NSArray *variables;

- (id)initWithURLString:(NSString *)url;
- (NSString *)valueForVariable:(NSString *)varName;

@end