//
//  DatePickerSelectionViewController.m
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 21/6/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "DatePickerSelectionViewController.h"
#import "ShareLocale.h"

@interface DatePickerSelectionViewController ()

@end

@implementation DatePickerSelectionViewController

- (id)initWithDate:(NSDate*)date andDateMode:(UIDatePickerMode)mode withBlock:(DatePickerComplete) block
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        self.date = date;
        self.mode = mode;
        self.callbackBlock = block;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.preferredContentSize = CGSizeMake(320, 266);

    UIDatePicker* picker = (UIDatePicker*)[self.view viewWithTag:100];
    picker.date = self.date;
    picker.datePickerMode = self.mode;
    
    UIButton* saveButton = (UIButton*)[self.view viewWithTag:200];
    [saveButton setTitle:[ShareLocale textFromKey:@"confirm"] forState:UIControlStateNormal];
    [saveButton addTarget:self action:@selector(onConfirmButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}



-(void) onConfirmButtonClicked:(UIView*)button{
    NSLog(@"onConfirmButtonClicked");
    UIDatePicker* picker = (UIDatePicker*)[self.view viewWithTag:100];
    
    self.callbackBlock(picker.date);
    

}





@end
