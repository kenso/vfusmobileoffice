//
//  GeneralUiTool.m
//  ClientelingIPhone
//
//  Created by Developer 2 on 17/4/13.
//  Copyright (c) 2013 Laputa. All rights reserved.
//

#import "GeneralUiTool.h"
#import "Constants.h"
#import <QuartzCore/QuartzCore.h>
#import "ShareLocale.h"

@implementation GeneralUiTool

static UIActivityIndicatorView* loadingView;

static GeneralUiTool* myself;

+(id) shareInstance{
    if(myself==nil){
        myself = [[GeneralUiTool alloc] init];
    }
    return myself;
}

-(void) pauseUserInteraction{
    if(![UIApplication sharedApplication].isIgnoringInteractionEvents){
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    }
}

-(void) resumeUserInteraction{
    
    while([UIApplication sharedApplication].isIgnoringInteractionEvents){
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }

}


+(void)showLoadingView:(UIView*)p{
    
    float w = p.frame.size.width;
    float h = p.frame.size.height;

    loadingView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleGray];
    loadingView.center = CGPointMake(w/2, h/2 - loadingView.frame.size.height - 50 );
    
    loadingView.hidesWhenStopped = YES;
    [p addSubview:loadingView];
    [loadingView startAnimating];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
}

+(void)hideLoadingView{
    [loadingView removeFromSuperview];
    [loadingView release];
    loadingView = nil;
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    
}





//Alert dialog
- (void) showConfirmDialogWithTitle:(NSString*)title message:(NSString*)message{
    UIAlertView *messageAlert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle: [ShareLocale textFromKey:@"OK"] otherButtonTitles:nil];
    [messageAlert show];
    [messageAlert release];
    self.globalYesBlock = nil;
    self.globalNoBlock = nil;
}

- (void) showConfirmDialogWithTitle:(NSString*)title message:(NSString*)message yesHanlder:(AlertClickHandlerBlock )yesBlock{
    
    self.globalYesBlock = yesBlock;
    
    NSString* yesTxt = NSLocalizedString([ShareLocale textFromKey: @"OK" ], nil);
    
    UIAlertView *messageAlert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:yesTxt otherButtonTitles:nil];
    [messageAlert show];
    [messageAlert release];
}

- (void) showConfirmDialogWithTitle:(NSString*)title message:(NSString*)message yesHanlder:(AlertClickHandlerBlock )yesBlock noHandler:(AlertClickHandlerBlock )noBlock{
    self.globalYesBlock = yesBlock;
    self.globalNoBlock = noBlock;
    NSString* yesTxt = NSLocalizedString([ShareLocale textFromKey:@"OK"], nil);
    NSString* noTxt = NSLocalizedString([ShareLocale textFromKey: @"Cancel"], nil);
    UIAlertView *messageAlert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:yesTxt otherButtonTitles:noTxt,nil];
    [messageAlert show];
    [messageAlert release];
}

- (void) showInutDialogWithTitle:(NSString*)title message:(NSString*)message yesHanlder:(AlertClickHandlerBlock )yesBlock noHandler:(AlertClickHandlerBlock )noBlock{
    self.globalYesBlock = yesBlock;
    self.globalNoBlock = noBlock;
    NSString* yesTxt = NSLocalizedString([ShareLocale textFromKey:@"OK"], nil);
    NSString* noTxt = NSLocalizedString([ShareLocale textFromKey:@"Cancel"], nil);
    
    UIAlertView *messageAlert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:yesTxt otherButtonTitles:noTxt,nil];
    
    messageAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [messageAlert show];
    [messageAlert release];
}

- (void) showInutNumberDialogWithTitle:(NSString*)title message:(NSString*)message yesHanlder:(AlertClickHandlerBlock )yesBlock noHandler:(AlertClickHandlerBlock )noBlock{
    self.globalYesBlock = yesBlock;
    self.globalNoBlock = noBlock;
    NSString* yesTxt = NSLocalizedString([ShareLocale textFromKey:@"OK"], nil);
    NSString* noTxt = NSLocalizedString([ShareLocale textFromKey:@"Cancel"], nil);
    
    UIAlertView *messageAlert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:yesTxt otherButtonTitles:noTxt,nil];
    
    messageAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField * alertTextField = [messageAlert textFieldAtIndex:0];
    alertTextField.keyboardType = UIKeyboardTypeNumberPad;

    [messageAlert show];
    [messageAlert release];
}

- (void) showInutDialogWithTitle:(NSString*)title message:(NSString*)message hint:(NSString*)hint yesTitle:(NSString*)yesTitle noTitle:(NSString*)noTitle yesHanlder:(AlertClickHandlerBlock )yesBlock noHandler:(AlertClickHandlerBlock )noBlock{
    self.globalYesBlock = yesBlock;
    self.globalNoBlock = noBlock;
    
    UIAlertView *messageAlert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:yesTitle otherButtonTitles:noTitle,nil];
    messageAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [messageAlert textFieldAtIndex:0].placeholder = hint;
    [messageAlert textFieldAtIndex:0].text = hint;
    [messageAlert show];
    [messageAlert release];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
//    NSLog(@"didDismissWithButtonIndex %i", buttonIndex);
    
    if(alertView.alertViewStyle == UIAlertViewStylePlainTextInput){
        if(buttonIndex==0){
            self.globalYesBlock([[alertView textFieldAtIndex:0] text]);
        }else if(buttonIndex==1){
            self.globalNoBlock(nil);
        }
    }
    else{
        if(buttonIndex==0){
            self.globalYesBlock(nil);
        }else if(buttonIndex==1){
            self.globalNoBlock(nil);
        }
    }
    
}

// Method to handle null
+ (NSString*)getStringValue:(id)string{
    ///return an empty string if it is a null object
    NSString *strContent = [NSString stringWithFormat:@"%@",string];
    
    if((id)strContent==nil || [strContent isEqualToString:@""] || [strContent isEqualToString:@"<null>"] || !strContent){
        return @"";
    }
    
    return string;
}

+ (NSDateFormatter*) getSFDateFormatter
{
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZZZ"];
    GeneralUiTool* ap = [GeneralUiTool shareInstance];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT: ap.timezoneSecond ]];
    return dateFormatter;
}

+ (NSDateFormatter*) getSFSimpleDateFormatter
{
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    [dateFormatter setDateFormat:DEFAULT_SIMPLE_DATE_FORMAT];
    GeneralUiTool* ap = [GeneralUiTool shareInstance];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT: ap.timezoneSecond ]];
    return dateFormatter;
}

+ (NSDateFormatter*) getSFSimpleDateTimeFormatter
{
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    [dateFormatter setDateFormat:@"yyyy-MMM-dd HH:mm"];
    GeneralUiTool* ap = [GeneralUiTool shareInstance];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT: ap.timezoneSecond ]];
    return dateFormatter;
}

+ (NSString*) getDisplayDateWithSFDate:(NSString *)sfDate
{
    NSDateFormatter * sfFormatter = [GeneralUiTool getSFDateFormatter];
    NSDateFormatter * formatter = [GeneralUiTool getSFSimpleDateFormatter];
    
    return [formatter stringFromDate:[sfFormatter dateFromString:sfDate]];

}

+ (NSMutableString*)getSFDateWithDay:(NSString *)myDay andTime:(NSString *) myStartTime
{
    int Year;
    int Month;
    int Day;
    int Hour;
    int Minute;
    int Second;
    
    NSArray *arrDate = [myDay componentsSeparatedByString:@"-"];
    Year = [[arrDate objectAtIndex:0] intValue];
    Month = [[arrDate objectAtIndex:1] intValue];
    Day = [[arrDate objectAtIndex:2] intValue];
    
    arrDate = [myStartTime componentsSeparatedByString:@":"];
    Hour   = [[arrDate objectAtIndex:0] intValue];
    Minute = [[arrDate objectAtIndex:1] intValue];
    Second = 0;
    
    NSCalendar *cal = [NSCalendar autoupdatingCurrentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setYear:Year];
    [components setMonth: Month];
    [components setDay: Day];
    [components setHour: Hour];
    [components setMinute: Minute];
    [components setSecond: Second];
    NSDate *date = [cal dateFromComponents:components];
    
    [components release];
    
    NSDateFormatter *SFDateFormatter = [GeneralUiTool getSFDateFormatter];
    return (NSMutableString*)[SFDateFormatter stringFromDate: date];
};

+ (void)setEffectForView:(UIView*)view BorderColor:(UIColor*)color andBorderWidth:(float)borderWidth andCornerRadius:(float)cornerRadius{
    /*
     [[view layer] setBorderColor:[color CGColor]];
     [[view layer] setBorderWidth:borderWidth];
     [[view layer] setCornerRadius:cornerRadius];
     */
    [view setClipsToBounds: YES];
}

// Methods related to constants
+ (NSString*)getConstantForKey:(NSString*)key{
    return NSLocalizedStringFromTable(key, @"InfoPlist", @"");
}



+ (UIFont*)getFontWithConstantKey:(NSString*)key{
    NSString *strFont = [self getConstantForKey:key];
    NSArray *arrFont= [strFont componentsSeparatedByString:@","];
    return [UIFont fontWithName:[arrFont objectAtIndex:0] size:[[arrFont objectAtIndex:1] floatValue]];
}


+ (NSArray*)getTableColumnXConstantForKey:(NSString*)key{
    NSString *strColumnX = NSLocalizedStringFromTable(key, @"InfoPlist", @"");
    NSArray *arrColumnX = [strColumnX componentsSeparatedByString:@","];
    NSMutableArray *arrMutableFloatColumnX = [[NSMutableArray alloc] init];
    for(int n=0; n<[arrColumnX count]; n++){
        [arrMutableFloatColumnX addObject:[NSNumber numberWithFloat:[[arrColumnX objectAtIndex:n] floatValue]]];
    }
    NSArray *arrFloatColumnX = [NSArray arrayWithArray:arrMutableFloatColumnX];
    [arrMutableFloatColumnX release];
    return arrFloatColumnX;
}

+(void)fadeInView:(UIView *)view
{
    view.alpha = 0;
    [UIView animateWithDuration:0.3
                          delay:0.0  /* do not add a delay because we will use performSelector. */
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^ {
                         view.alpha = 1;
                     }
                     completion:^(BOOL finished) {
                     }];
}

+(void)fadeOutView:(UIView *)view removeView:(BOOL)remove
{
    [UIView animateWithDuration:0.3
                          delay:0.0  /* do not add a delay because we will use performSelector. */
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^ {
                         view.alpha = 0.0f;
                     }
                     completion:^(BOOL finished) {
                         if ( remove) [view removeFromSuperview];
                     }];
}

+ (void)createModuleBlockEffect:(UIView *)view{
    [[view layer] setCornerRadius:6.0f];
    [[view layer] setBorderColor: [[UIColor lightGrayColor] CGColor]];
    [[view layer] setBorderWidth:1.0f];
}



+ (void)translateButton:(UIButton*)button withTitle:(NSString*)key{
    [button setTitle:NSLocalizedString(key, nil) forState:UIControlStateNormal];
    [button setTitle:NSLocalizedString(key, nil) forState:UIControlStateSelected];
    [button setTitle:NSLocalizedString(key, nil) forState:UIControlStateDisabled];
}



+(NSInteger) getParentTableCellTag:(UIView*)view{
    NSInteger level = 0;
    UIView* parent = [view superview];
    while(level<10){
        level++;
        if([[parent class] isSubclassOfClass:[UITableViewCell class]]){
            return parent.tag;
        }
        parent = [parent superview];
    }
    NSAssert(NO, @"No parent found match TableViewCell class");
    return -1;
}

@end
