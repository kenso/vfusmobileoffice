//
//  UIImagePickerController+Landscape.m
//  VFStorePerformanceApp
//
//  Created by Developer 2 on 5/3/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "UIImagePickerController+Landscape.h"

@implementation UIImagePickerController (Landscape)

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape;
}




@end
