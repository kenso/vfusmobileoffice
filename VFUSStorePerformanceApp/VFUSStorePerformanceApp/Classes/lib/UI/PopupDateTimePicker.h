//
//  PopupTimePicker.h
//  VFStorePerformanceApp
//
//  Created by Developer 2 on 21/3/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol PopupDateTimePickerDelegate <NSObject>

-(void)onConfirmPopupDatePicker:(UIView*)picker date:(NSDate*)date;

@end

@interface PopupDateTimePicker : UIView

- (void)setSelectedDate:(NSDate*)date;

- (id)initWithMode:(UIDatePickerMode)mode;

@property (nonatomic, assign) id<PopupDateTimePickerDelegate> delegate;

@end

