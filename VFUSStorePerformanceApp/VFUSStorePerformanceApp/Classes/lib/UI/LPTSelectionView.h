//
//  LPTSelectionView.h
//  VF Ordering App
//
//  Created by Developer 2 on 9/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import <UIKit/UIKit.h>


#define LPTSelectionViewCellTitleHeight 40
#define LPTSelectionViewCellHeight 80
#define LPTSelectionViewHeight 280
#define LPTSelectionViewAnimationDuration 0.4f
#define LPTSelectionViewDismissViewAlpha 0.66f
#define LPTSelectionViewkAnimationDuration 0.25f


@protocol LPTSelectionViewDelegate

-(void) onSelectionAtIndex:(int)index pickerView:(UITableView*)pickerView;

@end



@interface LPTSelectionView : UITableView<UITableViewDelegate, UITableViewDataSource>{
    id<LPTSelectionViewDelegate> viewDelegate;

}




- (id) initWithParentViewController:(UIViewController*)vc data:(NSArray*)dataAry delegate:(id<LPTSelectionViewDelegate>)dele;
- (void) show;
- (void) hide;

//@property (nonatomic, retain) UILabel* titleLabel;
@property (nonatomic, assign) UIViewController *dismisserViewController;
@property (nonatomic, assign) UIView *dismisserView;
@property (nonatomic, retain) NSArray *records;



@end
