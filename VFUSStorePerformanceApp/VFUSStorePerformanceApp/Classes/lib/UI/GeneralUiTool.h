//
//  GeneralUiTool.h
//  ClientelingIPhone
//  Static class that providing common ui function
//
//  Created by Developer 2 on 17/4/13.
//  Copyright (c) 2013 Laputa. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef void (^AlertClickHandlerBlock)        (id data);

@interface GeneralUiTool : NSObject{
}

@property (copy, nonatomic) void (^globalYesBlock)(id data);
@property (copy, nonatomic) void (^globalNoBlock)(id data);

@property (assign, nonatomic) NSInteger timezoneSecond;


+(id) shareInstance;


-(void) pauseUserInteraction;
-(void) resumeUserInteraction;

+(void) hideLoadingView;
+(void) showLoadingView:(UIView*)parent;

- (void) showConfirmDialogWithTitle:(NSString*)title message:(NSString*)message;
- (void) showConfirmDialogWithTitle:(NSString*)title message:(NSString*)message yesHanlder:(AlertClickHandlerBlock )block;
- (void) showConfirmDialogWithTitle:(NSString*)title message:(NSString*)message yesHanlder:(AlertClickHandlerBlock )yesBlock noHandler:(AlertClickHandlerBlock )noBlock;
- (void) showInutDialogWithTitle:(NSString*)title message:(NSString*)message yesHanlder:(AlertClickHandlerBlock )yesBlock noHandler:(AlertClickHandlerBlock )noBlock;
- (void) showInutDialogWithTitle:(NSString*)title message:(NSString*)message hint:(NSString*)hint  yesTitle:(NSString*)yesTitle noTitle:(NSString*)noTitle yesHanlder:(AlertClickHandlerBlock )yesBlock noHandler:(AlertClickHandlerBlock )noBlock;
- (void) showInutNumberDialogWithTitle:(NSString*)title message:(NSString*)message yesHanlder:(AlertClickHandlerBlock )yesBlock noHandler:(AlertClickHandlerBlock )noBlock;

+ (NSString*)getStringValue:(id)string;

+ (NSDateFormatter*) getSFDateFormatter;
+ (NSDateFormatter*) getSFSimpleDateFormatter;
+ (NSString*) getDisplayDateWithSFDate:(NSString *)sfDate;
+ (NSDateFormatter*) getSFSimpleDateTimeFormatter;
+ (NSMutableString*)getSFDateWithDay:(NSString *)myDay andTime:(NSString *) myStartTime;
+ (void)setEffectForView:(UIView*)view BorderColor:(UIColor*)color andBorderWidth:(float)borderWidth andCornerRadius:(float)cornerRadius;
+ (NSString*)getConstantForKey:(NSString*)key;
+ (UIFont*)getFontWithConstantKey:(NSString*)key;
+ (NSArray*)getTableColumnXConstantForKey:(NSString*)key;

+(void)fadeInView:(UIView *)view;
+(void)fadeOutView:(UIView *)view removeView:(BOOL)remove;

+ (void)createModuleBlockEffect:(UIView *)view;

+ (void)translateButton:(UIButton*)button withTitle:(NSString*)key;

+(NSInteger) getParentTableCellTag:(UIView*)view;

    
@end
