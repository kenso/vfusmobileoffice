//
//  ChartPie.m
//  VFStorePerformanceApp
//
//  Created by Developer 2 on 7/3/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "ChartPie.h"
#import "CorePlot-CocoaTouch.h"


@implementation ChartPie


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.graphFrame = frame;
        self.pieFrame = frame;
        self.radius = frame.size.width*.4f;
        self.labelArray = @[];
    }
    return self;
}


- (id)initWithFrame:(CGRect)frame pieFrame:(CGRect)pieFrame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.graphFrame = frame;
        self.pieFrame = pieFrame;
        self.radius = frame.size.width*.4f;
        self.labelArray = @[];
    }
    return self;
}



//init chart
-(void) generateGraph{
    CPTGraphHostingView* view = [[CPTGraphHostingView alloc] initWithFrame:self.graphFrame ];
    view.allowPinchScaling = NO;
    [self addSubview: view];

    CPTGraph* graph = [[CPTXYGraph alloc] initWithFrame:self.pieFrame];
    view.hostedGraph = graph;
    graph.paddingBottom = 0;
    graph.paddingTop = 0;
    graph.paddingLeft = 0;
    graph.paddingRight = 0;
    //    [graph applyTheme: [CPTTheme themeNamed:kCPTPlainWhiteTheme]];

    //config text style
    CPTMutableTextStyle *textStyle = [CPTMutableTextStyle textStyle];
    textStyle.color = [CPTColor whiteColor];
    textStyle.fontName = @"Helvetica-Bold";
    textStyle.fontSize = 20.0f;

    //config title
    graph.title = self.title;
    graph.titleTextStyle = textStyle;
    graph.titlePlotAreaFrameAnchor = CPTRectAnchorTop;
    graph.titleDisplacement = CGPointMake(0.0f, 0.0f);

    CPTPieChart* piePlot = [[CPTPieChart alloc] init];
    piePlot.dataSource = self;
    piePlot.pieRadius = self.radius;
    piePlot.identifier =@"Pie Chart 1";
    piePlot.startAngle = M_PI_4;
    piePlot.sliceDirection = CPTPieDirectionCounterClockwise;
    piePlot.centerAnchor = CGPointMake(0.5, 0.45);
    piePlot.borderLineStyle= [CPTLineStyle lineStyle];
    piePlot.delegate = self;
    [graph addPlot:piePlot];
    
    
    CPTLegend *theLegend = [CPTLegend legendWithGraph:graph];
    theLegend.numberOfColumns = 1;
    theLegend.fill = [CPTFill fillWithColor:[CPTColor whiteColor]];
    theLegend.borderLineStyle = [CPTLineStyle lineStyle];
    theLegend.cornerRadius = 5.0;
    graph.legend = theLegend;
    graph.legendAnchor = CPTRectAnchorRight;
//    CGFloat legendPadding = -(self.view.bounds.size.width / 8);
 //   graph.legendDisplacement = CGPointMake(legendPadding, 0.0);


    
    [piePlot release];
    [graph release];
    [view release];

}


-(void) dealloc{
    self.title = nil;
    self.labelArray = nil;
    self.colorArray = nil;
    [super dealloc];
}

#pragma mark - Delegate

-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot{
    return 3;
}

-(NSNumber*) numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)idx{
    return [self.valueArray objectAtIndex:idx];
}

-(CPTLayer*) dataLabelForPlot:(CPTPlot *)plot recordIndex:(NSUInteger)idx{
    CPTTextLayer *label = [[CPTTextLayer alloc] initWithText:[self.labelArray objectAtIndex:idx]];
    return label;
}

-(CPTFill *)sliceFillForPieChart:(CPTPieChart *)pieChart recordIndex:(NSUInteger)index{
    NSArray* colorChoices;
    if(self.colorArray == nil){
        colorChoices = @[ [CPTFill fillWithColor: [CPTColor colorWithComponentRed:168/255.0f green:0 blue:142/255.0f alpha:1]],
                         [CPTFill fillWithColor: [CPTColor colorWithComponentRed:49/255.0f green:105/255.0f blue:192/255.0f alpha:1]],
                         [CPTFill fillWithColor: [CPTColor colorWithComponentRed:0/255.0f green:150/255.0f blue:66/255.0f alpha:1]],
                         [CPTFill fillWithColor: [CPTColor colorWithComponentRed:255/255.0f green:149/255.0f blue:68/255.0f alpha:1]],
                         [CPTFill fillWithColor: [CPTColor colorWithComponentRed:236/255.0f green:45/255.0f blue:37/255.0f alpha:1]]
                        ];
    }else{
        colorChoices = [[NSMutableArray alloc] init];
        for(UIColor* color in self.colorArray){
            const CGFloat* components = CGColorGetComponents(color.CGColor);
            [((NSMutableArray*)colorChoices) addObject: [CPTFill fillWithColor: [CPTColor colorWithComponentRed:components[0]  green:components[1] blue:components[1] alpha:1]]];
        }
    }
    
    int colorIndex = index % [colorChoices count];
    return [colorChoices objectAtIndex:colorIndex];
}


-(NSString *)legendTitleForPieChart:(CPTPieChart *)pieChart recordIndex:(NSUInteger)index {
    return [self.labelArray objectAtIndex:index];

}

@end
