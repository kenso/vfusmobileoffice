//
//  ChartBar.h
//  VFStorePerformanceApp
//
//  Created by Developer 2 on 7/3/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"

@interface ChartBar : UIView<CPTPlotDataSource>


- (id)initWithFrame:(CGRect)frame;
- (id)initWithFrame:(CGRect)frame pieFrame:(CGRect)pieFrame;

-(void) generateGraph;

@property (nonatomic, assign) CGRect graphFrame;
@property (nonatomic, assign) CGRect barFrame;
@property (nonatomic, retain) NSString* title;
@property (nonatomic, retain) NSString* xTitle;
@property (nonatomic, retain) NSString* yTitle;


@property (nonatomic, retain) NSArray* dates;//NSString array - X, dates in example
@property (nonatomic, retain) NSDictionary* sets;
@property (nonatomic, retain) NSDictionary* data;
//@property (nonatomic, retain) NSArray* colorArray;//UIColor array


@end
