//
//  ChartLine.h
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 16/5/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"


@interface ChartLine : UIView<CPTPlotDataSource>


- (id)initWithFrame:(CGRect)frame;
-(void) generateGraph;

@property (nonatomic, retain) CPTScatterPlot* plot1;
@property (nonatomic, retain) CPTScatterPlot* plot2;
@property (nonatomic, retain) CPTGraph* graph;

@property (nonatomic, assign) CGRect graphFrame;

@property (nonatomic, assign) CGRect lineFrame;

@property (nonatomic, retain) NSString* title;

@property (nonatomic, assign) double startY;
@property (nonatomic, assign) double intervalY;
@property (nonatomic, assign) double intervalX;

@property (nonatomic, retain) NSArray* xLabelArray;//NSNumber array - X

@property (nonatomic, retain) NSArray* valueArray1;//NSNumber array - Y
@property (nonatomic, retain) NSArray* valueArray2;//NSNumber array - Y
@property (nonatomic, retain) NSArray* valueLabelArray1;//NSNumber array - Y
//@property (nonatomic, retain) NSArray* valueLabelArray2;//NSNumber array - Y



@end
