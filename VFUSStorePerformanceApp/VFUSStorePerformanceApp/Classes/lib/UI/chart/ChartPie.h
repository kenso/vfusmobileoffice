//
//  ChartPie.h
//  VFStorePerformanceApp
//
//  Created by Developer 2 on 7/3/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"


@interface ChartPie : UIView<CPTPlotDataSource>


- (id)initWithFrame:(CGRect)frame;
- (id)initWithFrame:(CGRect)frame pieFrame:(CGRect)pieFrame;

-(void) generateGraph;

@property (nonatomic, assign) CGRect graphFrame;
@property (nonatomic, assign) CGRect pieFrame;
@property (nonatomic, retain) NSString* title;
@property (nonatomic, assign) float radius;
@property (nonatomic, retain) NSArray* labelArray;//NSString array - X
@property (nonatomic, retain) NSArray* valueArray;//UIColor array - Y
@property (nonatomic, retain) NSArray* colorArray;//UIColor array



@end
