//
//  ChartLine.m
//  VFStorePerformanceApp_Dev2
//
//  Created by Developer2 on 16/5/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "ChartLine.h"
#import "CorePlot-CocoaTouch.h"

#define PLOT_1 @"PLOT_1"
#define PLOT_2 @"PLOT_2"



@implementation ChartLine



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

    }
    return self;
}



-(void) generateGraph{
    //init
    CPTGraphHostingView* hostView = [(CPTGraphHostingView *) [CPTGraphHostingView alloc] initWithFrame:self.bounds];
	hostView.allowPinchScaling = YES;
	[self addSubview: hostView];
    self.graph = [[CPTXYGraph alloc] initWithFrame: hostView.bounds];
	[self.graph applyTheme:[CPTTheme themeNamed: kCPTPlainWhiteTheme  ]]; //kCPTDarkGradientTheme
	hostView.hostedGraph = self.graph;
    [hostView release];
    //title
    self.graph.title = @"";//self.title;
    //styling
    CPTMutableTextStyle *titleStyle = [CPTMutableTextStyle textStyle];
	titleStyle.color = [CPTColor blackColor];
	//titleStyle.fontName = @"Helvetica-Bold";
	titleStyle.fontSize = 14.0f;
    self.graph.paddingBottom = 0;
    self.graph.paddingTop = 0;
    self.graph.paddingLeft = 0;
    self.graph.paddingRight = 0;
	self.graph.titleTextStyle = titleStyle;
	self.graph.titlePlotAreaFrameAnchor = CPTRectAnchorTop;
	self.graph.titleDisplacement = CGPointMake(0.0f, 2.0f);
	[self.graph.plotAreaFrame setPaddingTop:0.0f];
	[self.graph.plotAreaFrame setPaddingLeft:0.0f];
	[self.graph.plotAreaFrame setPaddingBottom:0.0f];
	[self.graph.plotAreaFrame setPaddingRight:0.0f];
    self.graph.borderColor = [[UIColor whiteColor] CGColor] ;
    self.graph.borderWidth = 0;
	CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) self.graph.defaultPlotSpace;
	plotSpace.allowsUserInteraction = YES;


    
    self.plot1 = [[CPTScatterPlot alloc] init];
	self.plot1.dataSource = self;
	self.plot1.identifier = PLOT_1;
	CPTColor *color1 = [CPTColor redColor];
	[self.graph addPlot:self.plot1 toPlotSpace:plotSpace];

    
	self.plot2 = [[CPTScatterPlot alloc] init];
    self.plot2.dataSource = self;
	self.plot2.identifier = PLOT_2;
	CPTColor *color2 = [CPTColor orangeColor];
	[self.graph addPlot:self.plot2 toPlotSpace:plotSpace];

    // 3 - Set up plot space
	[plotSpace scaleToFitPlots:[NSArray arrayWithObjects:self.plot1, self.plot2, nil]];
	CPTMutablePlotRange *xRange = [plotSpace.xRange mutableCopy];
	[xRange expandRangeByFactor:CPTDecimalFromCGFloat(2.0f)];
    if([self.valueArray1 count]<=3){
        [xRange expandRangeByFactor:CPTDecimalFromCGFloat(2.2f)];
    }

	plotSpace.xRange = xRange;//assumption: plot1 and plot2 must have same xRange

	CPTMutablePlotRange *yRange = [plotSpace.yRange mutableCopy];
	[yRange expandRangeByFactor:CPTDecimalFromCGFloat(2.3f)];
    CPTXYAxisSet* axisSet = (CPTXYAxisSet*)self.graph.axisSet;
//    axisSet.yAxis.majorIntervalLength = [[NSNumber numberWithFloat: self.intervalY] decimalValue];

    axisSet.yAxis.orthogonalCoordinateDecimal = CPTDecimalFromString(@"-0.5");
//    axisSet.yAxis.majorIntervalLength  = [[NSNumber numberWithDouble:10.0] decimalValue];
//    axisSet.yAxis.minorTickLabelOffset  = 1;
    axisSet.yAxis.labelingPolicy = CPTAxisLabelingPolicyNone;

    axisSet.xAxis.orthogonalCoordinateDecimal = CPTDecimalFromString([NSString stringWithFormat:@"%.8f", self.startY]);
//    axisSet.xAxis.majorIntervalLength = [[NSNumber numberWithFloat: self.intervalX] decimalValue];
    axisSet.xAxis.labelingPolicy = CPTAxisLabelingPolicyNone;
    
    
    CPTMutableTextStyle* txtStyle = [[CPTMutableTextStyle alloc] init];
    txtStyle.color = [CPTColor blackColor];
    txtStyle.fontSize = 10;
    axisSet.xAxis.labelTextStyle = txtStyle;

    CPTMutableTextStyle* yTxtStyle = [[CPTMutableTextStyle alloc] init];
    yTxtStyle.color = [CPTColor blackColor];
    yTxtStyle.fontSize = 10;
    
    axisSet.yAxis.labelTextStyle = yTxtStyle;

    
    NSArray *xAxisLabels = self.xLabelArray;
    NSUInteger labelLocation = 0;
    NSMutableArray *customLabels = [NSMutableArray array];
    for (NSString *xlabel in xAxisLabels ) {
        CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithText: xlabel textStyle: txtStyle];
        newLabel.tickLocation = [[NSNumber numberWithInteger: labelLocation] decimalValue] ;
        newLabel.offset = 1;
        newLabel.rotation = M_PI/4;
        labelLocation++;
        [customLabels addObject: newLabel];
        [newLabel release];
    }
    NSSet* set =  [NSSet setWithArray:customLabels];
    axisSet.xAxis.axisLabels = set;
    NSMutableArray *yLabels = [NSMutableArray array];
    
    //Generate Y label here
    //For each Y axis, we only have 3 label: bottom, middle and top
    NSNumber* maxYVal = [NSNumber numberWithInt: NSIntegerMin];
    NSNumber* minYVal = [NSNumber numberWithInt: NSIntegerMax];
    
    
    for(NSNumber* yVal in self.valueArray1){
        if([yVal doubleValue] > [maxYVal doubleValue]){
            maxYVal = yVal;
        }
        if([yVal doubleValue] < [minYVal doubleValue]){
            minYVal = yVal;
        }
    }
    
    for(NSNumber* yVal in self.valueArray2){
        if(yVal!=(id)[NSNull null]){
            if([yVal doubleValue] > [maxYVal doubleValue]){
                maxYVal = yVal;
            }
            if([yVal doubleValue] < [minYVal doubleValue]){
                minYVal = yVal;
            }
        }
    }

    long double quarterLen =([maxYVal doubleValue] - [minYVal doubleValue])/3;
    long double interval = quarterLen;

    if(interval>0){
        NSInteger decimalPoint = 0;
        while(YES){
            if(interval>1){
                interval = roundl(interval);
                break;
            }
            else{
                decimalPoint++;
                interval = interval * 10;
            }
        }

        while(decimalPoint>0){
            interval = interval/10;
            decimalPoint--;
        }
    }else{
        interval = 1;
    }
    
    

    NSMutableArray* tickValArray = [[NSMutableArray alloc] init];
    double val = [minYVal doubleValue];
    while(val <= [maxYVal doubleValue] + interval){
        [tickValArray addObject: [NSNumber numberWithDouble: val]];
        val += interval;
    }
    
    NSString* defaultFormat = @"%.0f -";
    NSInteger maxDiff = 0;
    if([minYVal doubleValue]/10<1){
        maxDiff = 1;
        defaultFormat = @"%.1f -";
    }
    if (interval<0.1){
        maxDiff = 2;
        defaultFormat = @"%.2f -";
    }
    
    for(NSInteger i=0; i<[tickValArray count]; i++){
        if(i<([tickValArray count]-1)){
            double diff =  fabs([tickValArray[i+1] doubleValue] - [tickValArray[i] doubleValue]);
            if(diff<0.1 && maxDiff<2){
                maxDiff = 2;
                defaultFormat = @"%.2f -";
            }
            else if (diff<1 && maxDiff<=1){
                maxDiff = 1;
                defaultFormat = @"%.1f -";
            }
        }
    }
    
    
    for(NSNumber* val in tickValArray){
        NSString* ylabel = [NSString stringWithFormat:defaultFormat, [val doubleValue]];
        CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithText: ylabel textStyle: yTxtStyle];
        newLabel.tickLocation = [val decimalValue];
        newLabel.offset = -2;
        [yLabels addObject: newLabel ];
        
    }
    
    set =  [NSSet setWithArray:yLabels];
    axisSet.yAxis.axisLabels = set;
    
    //Control the diagram zooming
	plotSpace.yRange = yRange;

	// 4 - Create styles and symbols
    //Line1
    CPTMutableLineStyle *style1 = [self.plot1.dataLineStyle mutableCopy];
    style1.lineWidth = 2.5;
    style1.lineColor = color1;
    self.plot1.dataLineStyle = style1;
	CPTMutableLineStyle *linestyle1 = [CPTMutableLineStyle lineStyle];
	linestyle1.lineColor = color1;
	CPTPlotSymbol *symbol1 = [CPTPlotSymbol ellipsePlotSymbol];
	symbol1.fill = [CPTFill fillWithColor:color1];
	symbol1.lineStyle = linestyle1;
	symbol1.size = CGSizeMake(6.0f, 6.0f);
	self.plot1.plotSymbol = symbol1;
    //Line2
    CPTMutableLineStyle *style2 = [self.plot1.dataLineStyle mutableCopy];
    style2.lineWidth = 2.5;
    style2.lineColor = color2;
    self.plot2.dataLineStyle = style2;
    CPTMutableLineStyle *linestyle2 = [CPTMutableLineStyle lineStyle];
    linestyle2.lineColor = color2;
    linestyle2.lineWidth = 2;
    CPTPlotSymbol *symbol2 = [CPTPlotSymbol ellipsePlotSymbol];
    symbol2.fill = [CPTFill fillWithColor: color2];
    symbol2.lineStyle = linestyle2;
    symbol2.size = CGSizeMake(6.0f, 6.0f);
    self.plot2.plotSymbol = symbol2;

    
    [self.graph release];
    [self.plot1 release];
    [self.plot2 release];
    [txtStyle release];
    [yTxtStyle release];
}


-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
	return [self.xLabelArray count];//both line should have same number of point
}


-(void)dealloc{
    
    [_graph removePlot:_plot1];
    [_graph removePlot:_plot2];
    [_plot1 release];
    [_plot2 release];
    [_graph release];
    
    [_title release];
    [_xLabelArray release];
    [_valueArray1 release];
    [_valueArray2 release];
    [_valueLabelArray1 release];

    [super dealloc];
}


-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index {
	switch (fieldEnum) {
		case CPTScatterPlotFieldX:
            if (index < [self.xLabelArray count]) {
                return [NSNumber numberWithUnsignedInteger:index];
            }
            return nil;
		case CPTScatterPlotFieldY:
			if ([plot.identifier isEqual:PLOT_1]) {
				return self.valueArray1[index];
			}
            else if ([plot.identifier isEqual:PLOT_2]) {
                if(index<[self.valueArray2 count] && self.valueArray2[index]!=(id)[NSNull null]){
                    return self.valueArray2[index];
                }
                return nil;
			}
			return nil;
	}
	return [NSDecimalNumber zero];
}

@end
