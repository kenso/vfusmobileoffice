//
//  DataHelper.m
//
//  Created by Developer 2 on 19/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import "DataHelper.h"

@implementation DataHelper


+(NSString*) getFieldDisplayWithFieldName:(NSString*)fieldName withDictionary:(NSDictionary*) data{
    
    return [DataHelper getFieldDisplayWithFieldName:fieldName withDictionary:data default:@"N/A"];
}





+(NSString*) getFieldDisplayWithFieldName:(NSString*)fieldName withDictionary:(NSDictionary*) data default:(NSString*)defaulVal{
    id val;
    if ([fieldName rangeOfString:@"."].location != NSNotFound) {
        
        NSArray* parts = [fieldName componentsSeparatedByString:@"."];
        
        NSString* field1 = [parts objectAtIndex:0];
        NSString* field2 = [parts objectAtIndex:1];
        
        id parentVal = [data objectForKey:field1];
        if(parentVal == nil || parentVal == (id)[NSNull null] ){
            return defaulVal;
        }else{
            if([parts count]==2){
                val = [parentVal objectForKey:field2];
            }
            else{
                NSString* rightPart = [fieldName substringFromIndex: [fieldName rangeOfString:@"."].location+1];
                val = [DataHelper getFieldDisplayWithFieldName:rightPart withDictionary:parentVal default:defaulVal];
            }
            
        }
    }else{
        val = [data objectForKey:fieldName];
    }
    
    
    if(val == nil || val == (id)[NSNull null] ){
        return defaulVal;
    }else if ([val isKindOfClass:[NSNumber class]]){
        return [NSString stringWithFormat:@"%@", val];
    }else if([val isEqualToString:@"<null>"]){
        return defaulVal;
    }
    return [NSString stringWithFormat:@"%@", val];
    
}




+(BOOL) isSameSalesforceId:(NSString*)id1 and:(NSString*)id2{
    return [[id1 substringToIndex:15] isEqualToString: [id2 substringToIndex:15]];
}

+(BOOL) isIntegerForString:(NSString*)val{
    if (val!=nil) {
        NSScanner* scan = [NSScanner scannerWithString:val];
        int val;
        return [scan scanInt:&val] && [scan isAtEnd];
    }
    return NO;
}

@end
