//
//  DataHelper.h
//
//  Created by Developer 2 on 19/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import <Foundation/Foundation.h>

//Get Field Recursively from a dictionary.  The field name is dot separated
#define gfr(data,field) [DataHelper getFieldDisplayWithFieldName:(field) withDictionary: (data)]
//Get Field Recurisvely from a dictionary with Default value.  The field name is dot separtated
#define gfrd(data,field,defVal) [DataHelper getFieldDisplayWithFieldName:(field) withDictionary:(data) default:(defVal)]


@interface DataHelper : NSObject


+(NSString*) getFieldDisplayWithFieldName:(NSString*)fieldName withDictionary:(NSDictionary*) data;
+(NSString*) getFieldDisplayWithFieldName:(NSString*)fieldName withDictionary:(NSDictionary*) data default:(NSString*)defaulVal;

+(BOOL) isSameSalesforceId:(NSString*)id1 and:(NSString*)id2;

+(BOOL) isIntegerForString:(NSString*)val;

@end
