//
//  DateParser.m
//
//  Created by Developer 2 on 18/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import "DateParser.h"

@implementation DateParser


+(NSString*) getStringFromDate:(NSDate*)date format:(NSString*) formatStr{
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    [df setDateFormat:formatStr];
    NSString* dateStr = [df stringFromDate:date];
    [df release];
    return dateStr;
}

+(NSDate*) getDateFromString:(NSString*)dateStr format:(NSString*) formatStr{
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    [df setDateFormat:formatStr];
    NSDate* date = [df dateFromString:dateStr];
    [df release];
    return date;
}

@end
