//
//  DateParser.h
//
//  Created by Developer 2 on 18/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateParser : NSObject

+(NSString*) getStringFromDate:(NSDate*)date format:(NSString*) formatStr;

+(NSDate*) getDateFromString:(NSString*)dateStr format:(NSString*) formatStr;

@end
