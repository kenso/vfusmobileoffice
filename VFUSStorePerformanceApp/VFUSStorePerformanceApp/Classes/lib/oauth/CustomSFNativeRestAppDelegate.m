//
//  CustomSFNativeRestAppDelegate.m
//  CustomSalesforceSDK
//
//  Created by Calvin Luk on 7/6/13.
//  Copyright (c) 2013 Calvin Luk. All rights reserved.
//

#import "CustomSFNativeRestAppDelegate.h"
//#import "SFAccountManager.h"
//#import "CustomSFAuthenticationManager.h"

/*
 NOTE if you ever need to update these, you can obtain them from your Salesforce org,
 (When you are logged in as an org administrator, go to Setup -> Develop -> Remote Access -> New )
 */


// Fill these in when creating a new Remote Access client on Force.com
static NSString *const CustomRemoteAccessConsumerKey = @"3MVG9Iu66FKeHhINkB1l7xt7kR8czFcCTUhgoA8Ol2Ltf1eYHOU4SqQRSEitYFDUpqRWcoQ2.dBv_a1Dyu5xa";
static NSString *const CustomOAuthRedirectURI = @"testsfdc:///mobilesdk/detect/oauth/done";



// Error identifiers from Salesforce standard SDK
static NSString * const kSFOAuthError                           = @"error";
static NSString * const kSFOAuthErrorDescription                = @"error_description";

static NSString * const kSFOAuthErrorTypeMalformedResponse          = @"malformed_response";
static NSString * const kSFOAuthErrorTypeAccessDenied               = @"access_denied";
static NSString * const KSFOAuthErrorTypeInvalidClientId            = @"invalid_client_id"; // invalid_client_id:'client identifier invalid'
// this may be returned when the refresh token is revoked
// TODO: needs clarification
static NSString * const kSFOAuthErrorTypeInvalidClient              = @"invalid_client";    // invalid_client:'invalid client credentials'
// this is returned when refresh token is revoked
static NSString * const kSFOAuthErrorTypeInvalidClientCredentials   = @"invalid_client_credentials"; // this is documented but hasn't been witnessed
static NSString * const kSFOAuthErrorTypeInvalidGrant               = @"invalid_grant";
static NSString * const kSFOAuthErrorTypeInvalidRequest             = @"invalid_request";
static NSString * const kSFOAuthErrorTypeInactiveUser               = @"inactive_user";
static NSString * const kSFOAuthErrorTypeInactiveOrg                = @"inactive_org";
static NSString * const kSFOAuthErrorTypeRateLimitExceeded          = @"rate_limit_exceeded";
static NSString * const kSFOAuthErrorTypeUnsupportedResponseType    = @"unsupported_response_type";
static NSString * const kSFOAuthErrorTypeTimeout                    = @"auth_timeout";



@interface CustomSFNativeRestAppDelegate()

@property (atomic, retain) NSMutableData *receivedData;

@end

@implementation CustomSFNativeRestAppDelegate

#pragma mark - App lifecycle


#pragma mark - Customized Features Settings

- (BOOL)allowOffline
{
    return NO;
}

- (UIViewController *)customWelcomeViewController
{
    return nil;
}

- (BOOL)supportCustomLogin
{
    return NO;
}

#pragma mark - Remote Access / OAuth configuration


- (NSString*)remoteAccessConsumerKey {
    return CustomRemoteAccessConsumerKey;
}

- (NSString*)oauthRedirectURI {
    return CustomOAuthRedirectURI;
}

#pragma mark - Customized Features

// Allow users using the app in offline
/*- (void)oauthCoordinator:(SFOAuthCoordinator *)coordinator didFailWithError:(NSError *)error {
 if ([self allowOffline] && -1009 == error.code) {
 [self dismissAuthViewControllerIfPresent];
 } else {
 [super oauthCoordinator:coordinator didFailWithError:error];
 }
 }
 
 - (void)dismissAuthViewControllerIfPresent
 {
 if (![NSThread isMainThread]) {
 dispatch_async(dispatch_get_main_queue(), ^{
 [self dismissAuthViewControllerIfPresent];
 });
 return;
 }
 
 if (self.authViewController != nil) {
 [self.window.rootViewController dismissViewControllerAnimated:YES
 completion:^{
 self.authViewController = nil;
 [self performSelector:@selector(loggedIn)];
 }];
 } else {
 [self performSelector:@selector(loggedIn)];
 }
 }*/

// Custom login way
- (void)login
{
    if (![self supportCustomLogin]) {
        [super login];
    } else {
        if ([SFAccountManager sharedInstance].credentials.refreshToken)
            [SFAccountManager sharedInstance].credentials.refreshToken = nil;
        // Start authentication
        [[CustomSFAuthenticationManager sharedCustomManager]
         login:self
         completion:^(SFOAuthInfo *authInfo) {
             [self postAuthSuccessProcesses:authInfo];
         }
         failure:^(SFOAuthInfo *authInfo, NSError *error) {
             [self log:SFLogLevelWarning format:@"Login failed with the following error: %@.  Logging out.", [error localizedDescription]];
             [self logout];
         }];
        
    }
}
- (void)startCustomAuthentication
{
    NSLog(@"Must override startCustomAuthentication if support custom login");
}
- (void)successCustomAuthenticationWithIdentityURL:(NSURL *)identityURL accessToken:(NSString *)accessToken refreshToken:(NSString *)refreshToken instanceURL:(NSURL *)instanceURL issuedAtDate:(NSDate *)issuedAtDate
{
    [[CustomSFAuthenticationManager sharedCustomManager] successCustomAuthenticationWithIdentityURL:identityURL accessToken:accessToken refreshToken:refreshToken instanceURL:instanceURL issuedAtDate:issuedAtDate];
}
- (void)failCustomAuthenticationWithError:(NSError *)error
{
    [[CustomSFAuthenticationManager sharedCustomManager] failCustomAuthenticationWithError:error];
}

// Pre-defined authentication ways

// By username and password
- (NSString *)remoteAccessConsumerSecret
{
    NSLog(@"%@", @"You must override remoteAccessConsumerSecret if use username password authentication");
    return nil;
}

- (void)startAuthenticationByUsername:(NSString *)username password:(NSString *)password
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://login.salesforce.com/services/oauth2/token"]];
    [request setHTTPMethod:@"POST"];
    NSString *params = [NSString stringWithFormat:@"%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@"
                        , @"format", @"json"
                        , @"grant_type", @"password"
                        , @"client_id", [self remoteAccessConsumerKey]
                        , @"client_secret", [self remoteAccessConsumerSecret]
                        , @"username", username
                        , @"password", password];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    [NSURLConnection connectionWithRequest:request delegate:self];
}

#pragma mark - NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    self.receivedData = [NSMutableData dataWithCapacity:1024];
    [self.receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.receivedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *finalError = nil;
    if (self.receivedData && [self.receivedData length] > 0) {
        NSError *error = nil;
        id jsonObj = [NSJSONSerialization JSONObjectWithData:self.receivedData options:NSJSONReadingMutableContainers error:&error];
        if (jsonObj && [jsonObj isKindOfClass:[NSDictionary class]]) {
            NSDictionary *jsonDict = (NSDictionary *)jsonObj;
            if (jsonDict[kSFOAuthError]) {
                finalError = [[self class] errorWithType:jsonDict[kSFOAuthError] description:jsonDict[kSFOAuthErrorDescription]];
            } else {
                NSString *timestampStr = jsonDict[@"issued_at"];
                NSDate *issuedAtDate = timestampStr ? [NSDate dateWithTimeIntervalSince1970:[timestampStr longLongValue]] : nil;
                
                [self
                 successCustomAuthenticationWithIdentityURL:
                 jsonDict[@"id"] ? [NSURL URLWithString:jsonDict[@"id"]] : nil
                 accessToken:jsonDict[@"access_token"]
                 refreshToken:jsonDict[@"access_token"]
                 instanceURL:jsonDict[@"instance_url"] ? [NSURL URLWithString:jsonDict[@"instance_url"]] : nil
                 issuedAtDate:issuedAtDate];
                
                return;
            }
        }
    }
    if (finalError == nil) {
        [NSError errorWithDomain:kSFOAuthErrorDomain code:kSFOAuthErrorUnknown userInfo:@{kSFOAuthErrorDescription: @"Unknown error"}];
    }
    [self failCustomAuthenticationWithError:finalError];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"%@", @"Connection failure when doing authentication");
    [self failCustomAuthenticationWithError:error];
}

+ (NSError *)errorWithType:(NSString *)type description:(NSString *)description {
    NSAssert(type, @"error type can't be nil");
    
    NSString *localized = [NSString stringWithFormat:@"%@ %@ : %@", kSFOAuthErrorDomain, type, description];
    NSInteger code = kSFOAuthErrorUnknown;
    if ([type isEqualToString:kSFOAuthErrorTypeAccessDenied]) {
        code = kSFOAuthErrorAccessDenied;
    } else if ([type isEqualToString:kSFOAuthErrorTypeMalformedResponse]) {
        code = kSFOAuthErrorMalformed;
    } else if ([type isEqualToString:@"invalid_client_id"]) {
        code = kSFOAuthErrorInvalidClientId;
    } else if ([type isEqualToString:kSFOAuthErrorTypeInvalidClient]) {
        code = kSFOAuthErrorInvalidClientCredentials;
    } else if ([type isEqualToString:kSFOAuthErrorTypeInvalidClientCredentials]) {
        code = kSFOAuthErrorInvalidClientCredentials;
    } else if ([type isEqualToString:kSFOAuthErrorTypeInvalidGrant]) {
        code = kSFOAuthErrorInvalidGrant;
    } else if ([type isEqualToString:kSFOAuthErrorTypeInvalidRequest]) {
        code = kSFOAuthErrorInvalidRequest;
    } else if ([type isEqualToString:kSFOAuthErrorTypeInactiveUser]) {
        code = kSFOAuthErrorInactiveUser;
    }  else if ([type isEqualToString:kSFOAuthErrorTypeInactiveOrg]) {
        code = kSFOAuthErrorInactiveOrg;
    }  else if ([type isEqualToString:kSFOAuthErrorTypeRateLimitExceeded]) {
        code = kSFOAuthErrorRateLimitExceeded;
    }  else if ([type isEqualToString:kSFOAuthErrorTypeUnsupportedResponseType]) {
        code = kSFOAuthErrorUnsupportedResponseType;
    } else if ([type isEqualToString:kSFOAuthErrorTypeTimeout])
        code = kSFOAuthErrorTimeout;
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:type,        kSFOAuthError,
                          description, kSFOAuthErrorDescription,
                          localized,   NSLocalizedDescriptionKey,
                          nil];
    NSError *error = [NSError errorWithDomain:kSFOAuthErrorDomain code:code userInfo:dict];
    return error;
}

@end
