//
//  CustomSFNativeRestAppDelegate.h
//  Shiseido Ordering App
//
//  Created by Developer 2 on 13/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SFNativeRestAppDelegate.h"

@interface CustomSFNativeRestAppDelegate : SFNativeRestAppDelegate <NSURLConnectionDelegate>

/* Deprecated
 * New SDK supports offline
 - (BOOL)allowOffline;
 */
/* Deprecated
 * Override SFNativeRootViewController.xib or SFAuthorizingViewController.xib under NativeRestApp
 - (UIViewController *)customWelcomeViewController;
 */
- (BOOL)supportCustomLogin;
- (void)startCustomAuthentication;
- (void)successCustomAuthenticationWithIdentityURL:(NSURL *)identityURL accessToken:(NSString *)accessToken refreshToken:(NSString *)refreshToken instanceURL:(NSURL *)instanceURL issuedAtDate:(NSDate *)issuedAtDate;
- (void)failCustomAuthenticationWithError:(NSError *)error;

// Pre-defined authentication ways
// By username and password
- (NSString *)remoteAccessConsumerSecret;
- (void)startAuthenticationByUsername:(NSString *)username password:(NSString *)password;

@end
