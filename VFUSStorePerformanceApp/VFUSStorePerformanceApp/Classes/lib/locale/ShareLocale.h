//
//  ShareLocale.h
//  VFStorePerformanceApp_Billy2
//
//  Created by Developer2 on 4/7/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShareLocale : NSObject

+(NSString*)textFromKey:(NSString*)key;

#define LocalizedString(key) [[ShareLocale sharedLocalSystem] localizedStringForKey:(key)]
#define LocalizationSetLanguage(language) [[ShareLocale sharedLocalSystem] setLanguage:(language)]


// a singleton:
+ (ShareLocale*) sharedLocalSystem;

-(void) initLanguage;

// this gets the string localized:
- (NSString*) localizedStringForKey:(NSString*) key;

- (NSString*) getLanguage;


//set a new language:
- (void) setLanguage:(NSString*) lang;

@end


