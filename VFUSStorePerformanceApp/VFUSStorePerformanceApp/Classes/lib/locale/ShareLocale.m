//
//  ShareLocale.m
//  VFStorePerformanceApp_Billy2
//
//  Created by Developer2 on 4/7/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "ShareLocale.h"



// Singleton
static ShareLocale* SingleLocalSystem = nil;
static NSBundle* myBundle = nil;

@implementation ShareLocale

+(NSString*)textFromKey:(NSString*)key{
    
    
    return [[ShareLocale sharedLocalSystem] localizedStringForKey:key];
    
    //NSBundle* bundle = [NSBundle mainBundle];
    //NSString *stringsPath = [bundle pathForResource:@"Localizable" ofType:@"strings"];
    //NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:stringsPath];
    
    //return [bundle localizedStringForKey:key value:@"" table:nil];
    
    
    //return NSLocalizedString(key, nil);
}





//-------------------------------------------------------------
// allways return the same singleton
//-------------------------------------------------------------
+ (ShareLocale*) sharedLocalSystem {
    // lazy instantiation
    if (SingleLocalSystem == nil) {
        SingleLocalSystem = [[ShareLocale alloc] init];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString* lang = [defaults objectForKey:@"app_lang"];
        if(lang==nil){
            [defaults setValue:@"en" forKey:@"app_lang"];
        }

    }
    return SingleLocalSystem;
}


//-------------------------------------------------------------
// initiating
//-------------------------------------------------------------
- (id) init {
    self = [super init];
    if (self) {
        // use systems main bundle as default bundle
        myBundle = [NSBundle mainBundle];
    }
    return self;
}



-(void) initLanguage{
    NSString* lang = [self getLanguage];
    if(lang==nil){
        [self setLanguage:@"en"];
    }else{
        [self setLanguage:lang];
    }
}


//-------------------------------------------------------------
// translate a string
//-------------------------------------------------------------
// you can use this macro:
// LocalizedString(@"Text");
- (NSString*) localizedStringForKey:(NSString*) key {
    // this is almost exactly what is done when calling the macro NSLocalizedString(@"Text",@"comment")
    // the difference is: here we do not use the systems main bundle, but a bundle
    // we selected manually before (see "setLanguage")
    return [myBundle localizedStringForKey:key value:@"" table:nil];
}


- (NSString*) getLanguage {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:@"app_lang"];
}



//-------------------------------------------------------------
// set a new language
//-------------------------------------------------------------
// you can use this macro:
// LocalizationSetLanguage(@"German") or LocalizationSetLanguage(@"de");
- (void) setLanguage:(NSString*) lang {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:lang forKey:@"app_lang"];
    [defaults synchronize];
    
    
    // path to this languages bundle
    NSString *path = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj" ];
    if (path == nil) {
        // there is no bundle for that language
        // use main bundle instead
        myBundle = [NSBundle mainBundle];
    } else {
        
        // use this bundle as my bundle from now on:
        myBundle = [NSBundle bundleWithPath:path];
        
        // to be absolutely shure (this is probably unnecessary):
        if (myBundle == nil) {
            myBundle = [NSBundle mainBundle];
        }
    }
}




@end
