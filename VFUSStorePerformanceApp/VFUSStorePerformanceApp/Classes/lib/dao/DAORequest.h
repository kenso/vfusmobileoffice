//
//  DAORequest.h
//  VFStorePerformanceApp_Billy
//
//  Created by Developer 2 on 22/4/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SFRestAPI.h"
#import "SFRestRequest.h"


@interface DAOBaseRequest : NSObject{
    
}

@property (nonatomic, retain) SFRestRequest* sfRequest;
@property (nonatomic, retain) NSString* name;


@end


@interface DAORequest : DAOBaseRequest{
    
}


@end
