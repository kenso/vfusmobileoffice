//
//  DAO.h
//  VFStorePerformanceApp_Billy
//
//  Created by Developer 2 on 22/4/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SFRestAPI.h"
#import "SFRestRequest.h"
#import "DAORequest.h"


@protocol DAODelegate

//response must contains error(int) and data(nsarray) field
-(void)daoRequest:(DAOBaseRequest*)request queryOnlineDataSuccess:(NSArray*) response;

//response must contains error(int) and description(string)
-(void)daoRequest:(DAOBaseRequest*)request queryOnlineDataError:(NSString*) response code:(int)code;

@end





@interface DAO : NSObject<SFRestDelegate>{
    BOOL _isOnlineMode;
}

@property (nonatomic, assign) BOOL isNetworkReachable;

-(void) isOnlineMode:(BOOL)b;
-(BOOL) isOnlineMode;

@property (nonatomic, retain) NSMutableArray* moreRecords;
@property (nonatomic, retain) DAOBaseRequest* runningMoreOriginalRequest;
@property (nonatomic, assign) id<DAODelegate> runningMoreOriginalRequestDelegate;
@property (nonatomic, copy) void (^runningMoreOriginalRequestPostDelegate)(NSArray* records);

@property (nonatomic, retain) DAOBaseRequest* runningRequest;
@property (nonatomic, assign) id<DAODelegate> runningRequestDelegate;
@property (nonatomic, copy) void (^runningRequestPostDelegate)(NSArray* records);

@property (nonatomic, retain) DAOBaseRequest* subrunningRequest;
@property (nonatomic, assign) id<DAODelegate> subrunningRequestDelegate;

//-(DAOBaseRequest*) queryOnlineDataOnBackgroundBySQL:(NSString*)sql delegate:(id<DAODelegate>)delegate;

-(DAOBaseRequest*) sendCustomRestRequest:(NSString*)apiName method:(SFRestMethod)method parameter:(NSDictionary*)data delegate:(id<DAODelegate>)delegate postDelegate:(void (^) (NSArray* records))postDelegate;

-(DAOBaseRequest*) queryOnlineDataBySQL:(NSString*)sql delegate:(id<DAODelegate>)delegate postDelegate:(void (^) (NSArray* records))postDelegate;


@end
