//
//  DAO.m
//  VFStorePerformanceApp_Billy
//
//  Created by Developer 2 on 22/4/14.
//  Copyright (c) 2014 vf.com. All rights reserved.
//

#import "DAO.h"

@implementation DAO


-(id)init{
    self = [super init];
    if(self){
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSNumber* isOnlineMode = [defaults objectForKey:@"isOnlineMode"];
        
        if(isOnlineMode!=nil && ![isOnlineMode boolValue])
            _isOnlineMode = NO;
        else
            _isOnlineMode = YES;
        self.isNetworkReachable = YES;
    }
    return self;
}

-(void) isOnlineMode:(BOOL)b{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithBool:b] forKey:@"isOnlineMode"];
    [defaults synchronize];
    _isOnlineMode = b;
}

-(BOOL) isOnlineMode{
    return _isOnlineMode;
}


-(DAOBaseRequest*) sendCustomRestRequest:(NSString*)apiName method:(SFRestMethod)method parameter:(NSDictionary*)data delegate:(id<DAODelegate>)delegate postDelegate:(void (^) (NSArray* records))postDelegate{
    NSLog(@"VFDAO::sendCustomRestRequest() apiName=%@", apiName);
    
    NSAssert(self.runningRequest==nil, @"DAO sendCustomRestRequest existingRequest=%@, apiName=%@",self.runningRequest.name, apiName);
    
    DAORequest* req = [[DAORequest alloc] init];
    req.name = apiName;
    self.runningRequest = req;
    
    [req release];
    
    SFRestRequest *sfRequest = [[SFRestRequest alloc] init] ;
    sfRequest.method = method;
    sfRequest.endpoint = [NSString stringWithFormat:@"/services/apexrest/%@", apiName];
    sfRequest.path = [NSString stringWithFormat:@"/services/apexrest/%@", apiName];

    if(data!=nil){
        sfRequest.queryParams = data;
    }
    
    NSLog(@"VFDAO::sendCustomRestRequest() path=%@", sfRequest.path);

    
    
    self.runningRequest.sfRequest = sfRequest;
    self.runningRequestDelegate = delegate;
    self.runningRequestPostDelegate = postDelegate;
    [[SFRestAPI sharedInstance] send:self.runningRequest.sfRequest delegate:self];
    
    return self.runningRequest;
    
}



-(void) sendMoreRequest:(NSString*)path{
    
    DAORequest* req = [[DAORequest alloc] init];
    req.name = @"More";
    self.runningRequest = req;
    [req release];

    SFRestRequest *sfRequest = [SFRestRequest requestWithMethod:SFRestMethodGET path:path queryParams:nil];
    self.runningRequest.sfRequest = sfRequest;
    [[SFRestAPI sharedInstance] send:self.runningRequest.sfRequest delegate:self];

}


-(DAOBaseRequest*) queryOnlineDataBySQL:(NSString*)sql delegate:(id<DAODelegate>)delegate postDelegate:(void (^) (NSArray* records))postDelegate{
    NSLog(@"VFDAO::queryOnlineDataBySQL() sql=%@", sql);
    
    NSAssert(self.runningRequest==nil, @"DAO queryOnlineDataBySQL existingRequest=%@, sql=%@",self.runningRequest.name, sql);
    
    DAORequest* req = [[DAORequest alloc] init];
    req.name = sql;
    self.runningRequest = req;
    [req release];
    self.runningRequest.sfRequest = [[SFRestAPI sharedInstance] requestForQuery: [NSString stringWithFormat: @"%@", sql]];
    self.runningRequestDelegate = delegate;
    self.runningRequestPostDelegate = postDelegate;
    [[SFRestAPI sharedInstance] send:self.runningRequest.sfRequest delegate:self];

    return self.runningRequest;
    
}

#pragma mark - SFRestDelegate

- (void)request:(SFRestRequest *)request didLoadResponse:(id)dataResponse{
//    NSLog(@"VFDAO::didLoadResponse() dataResponse=%@", dataResponse);
    if(self.runningRequest.sfRequest == request){
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            DAOBaseRequest* req = self.runningRequest;
            [req retain];
            id<DAODelegate> dele = self.runningRequestDelegate;
            void (^postDelegate)(NSArray* records) = [self.runningRequestPostDelegate copy];
            self.runningRequestPostDelegate = nil;

            self.runningRequest.sfRequest = nil;
            self.runningRequest = nil;
            self.runningRequestDelegate = nil;
            
            NSDictionary* response = dataResponse;
            
            //Custom api flow
            if([dataResponse isKindOfClass:[NSMutableData class]]){
                //NSString *strResult = [[NSString alloc] initWithData:dataResponse encoding:NSUTF8StringEncoding];
                NSError* e=nil;
                
                response = [NSJSONSerialization JSONObjectWithData:dataResponse options:0 error:&e];
                if(e!=nil){
                    NSLog(@"error=%@", e);
                    NSString* text = [[NSString alloc] initWithData:dataResponse encoding: NSASCIIStringEncoding];
                    NSLog(@"text=%@", text);
                }
                NSArray* records = @[response];
                if(postDelegate!=nil) postDelegate(records);
                [dele daoRequest:req queryOnlineDataSuccess:records];
            }
            else{
                
                NSArray* dataArray = [response objectForKey:@"records"];
                if(dataArray!=nil){//query response
                    if(response[@"nextRecordsUrl"]!=nil){
                        //when try to load records more than 2000, SF auto hide
                        if(self.moreRecords==nil){
                            self.moreRecords = [NSMutableArray array];
                        }
                        [self.moreRecords addObjectsFromArray:dataArray];
                        if(self.runningMoreOriginalRequest==nil){
                            self.runningMoreOriginalRequest = req;
                            self.runningMoreOriginalRequestDelegate = dele;
                            self.runningMoreOriginalRequestPostDelegate = postDelegate;
                        }
                        [self sendMoreRequest: response[@"nextRecordsUrl"] ];
                        return;
                    }
                    else if(self.moreRecords!=nil){
                        //final response for a chain more request
                        [self.moreRecords addObjectsFromArray:dataArray];
                        dataArray = self.moreRecords;
                        [dataArray retain];
                        req = self.runningMoreOriginalRequest;
                        [req retain];
                        dele = self.runningMoreOriginalRequestDelegate;
                        postDelegate = [self.runningMoreOriginalRequestPostDelegate copy];
                        
                        self.runningMoreOriginalRequest = nil;
                        self.runningMoreOriginalRequestDelegate = nil;
                        self.runningMoreOriginalRequestPostDelegate = nil;
                        self.moreRecords = nil;
                    }
                    if(postDelegate!=nil) postDelegate(dataArray);
                    [dele daoRequest:req queryOnlineDataSuccess:dataArray];
                }
                else{//update or create response
                    NSString* insertedId = [response objectForKey:@"id"];
                    if(insertedId==nil) {
                        insertedId = (id)[NSNull null];
                    }
                    if(postDelegate!=nil) postDelegate(@[insertedId]);
                    [dele daoRequest:req queryOnlineDataSuccess:@[insertedId]];
                }
            }
        });
    }
    else if(self.subrunningRequest.sfRequest == request){
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            DAOBaseRequest* req = self.subrunningRequest;
            id<DAODelegate> dele = self.subrunningRequestDelegate;
            self.subrunningRequest.sfRequest = nil;
            self.subrunningRequest = nil;
            self.subrunningRequestDelegate = nil;
            NSDictionary* response = dataResponse;
            NSArray* dataArray = [response objectForKey:@"records"];
            [dele daoRequest:req queryOnlineDataSuccess:dataArray];
        });
    }
    else{
        NSLog(@"Unknown request response");
    }
        
}


- (void)request:(SFRestRequest *)request didFailLoadWithError:(NSError*)error{
    NSLog(@"VFDAO::didFailLoadWithError() error=%@", error);
    if(self.runningRequest.sfRequest == request){
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            DAOBaseRequest* req = self.runningRequest;
            id<DAODelegate> dele = self.runningRequestDelegate;
            self.runningRequest.sfRequest = nil;
            self.runningRequest = nil;
            self.runningRequestDelegate = nil;
            int errorCode = error.code;
            NSString* errorMsg = error.description;
            [dele daoRequest:req queryOnlineDataError:errorMsg code:errorCode];
        });
    }else{
        NSLog(@"Unknown request error:%@", error);
    }
}


- (void)requestDidCancelLoad:(SFRestRequest *)request{
    NSLog(@"Unknown request cancel %@", request);
    if(self.runningRequest.sfRequest == request){
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            DAOBaseRequest* req = self.runningRequest;
            id<DAODelegate> dele = self.runningRequestDelegate;
            self.runningRequest.sfRequest = nil;
            self.runningRequest = nil;
            self.runningRequestDelegate = nil;
            [dele daoRequest:req queryOnlineDataError:@"Request was cancelled." code:80001];
        });
    }else{
        NSLog(@"Unknown request error:%@", @"Cancelled");
    }

}


- (void)requestDidTimeout:(SFRestRequest *)request{
    NSLog(@"Unknown request timeout %@", request);
    if(self.runningRequest.sfRequest == request){
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            DAOBaseRequest* req = self.runningRequest;
            id<DAODelegate> dele = self.runningRequestDelegate;
            self.runningRequest.sfRequest = nil;
            self.runningRequest = nil;
            self.runningRequestDelegate = nil;
            [dele daoRequest:req queryOnlineDataError:@"Request was timeout." code:80002];
        });
    }else{
        NSLog(@"Unknown request error:%@", @"Timtout");
    }
}


@end
