//
//  SFBinaryImageView.h
//  emFace
//
//  Created by Calvin Luk on 6/6/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageLoader.h"

@interface SFImageView : UIImageView <ImageLoaderDelegate>

- (id)initBinaryImageViewWithFrame:(CGRect)frame;

-(void) onRequestSuccessOfRequest:(ImageLoaderRequest *)req feedItemID:(NSString *)feedItemId response:(id)jsonResponse contentType:(NSString *)contentType;

-(void) onRequestFailOfRequest:(ImageLoaderRequest *)req feedItemID:(NSString *)feedItemId error:(NSError *)error;

-(void) loadImageWithFeedItemID:(NSString*) feedItemId;


@property (nonatomic, retain) UIColor *loadingBackgroundColor;
@property (nonatomic, retain) NSArray *loadingImages;
@property (nonatomic, assign) NSTimeInterval loadingImageDuration;
@property (nonatomic, assign) NSInteger loadingImageRepeatCount;

@property (nonatomic, retain) UIColor *failedBackgroundColor;
@property (nonatomic, retain) UIImage *failedImage;

@end
