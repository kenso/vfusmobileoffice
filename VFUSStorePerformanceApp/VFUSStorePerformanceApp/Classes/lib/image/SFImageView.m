//
//  SFBinaryImageView.m
//  emFace
//
//  Created by Calvin Luk on 6/6/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import "SFImageView.h"

//static NSArray *_defaultLoadingImages = nil;

@interface SFImageView()

@property (nonatomic, assign) BOOL isSFBinImageView;
@property (nonatomic, assign) UIViewContentMode saveContentMode;

@end

@implementation SFImageView

- (id)init
{
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    return [self initBinaryImageViewWithFrame:frame];
}

- (id)initBinaryImageViewWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    self.isSFBinImageView = YES;
    self.failedImage = nil;
    self.loadingBackgroundColor = nil;
    self.loadingImageDuration = 1.0f;
    self.loadingImageRepeatCount = 0;
    self.failedBackgroundColor = [UIColor colorWithRed:0.7 green:0.2 blue:0.2 alpha:1];
}

-(void) onRequestSuccessOfRequest:(ImageLoaderRequest *)req feedItemID:(NSString *)feedItemId response:(id)jsonResponse contentType:(NSString *)contentType{
    NSData* imageData = jsonResponse;
    [self applyImage:imageData];
}

-(void) onRequestFailOfRequest:(ImageLoaderRequest *)req feedItemID:(NSString *)feedItemId error:(NSError *)error{
    self.image = self.failedImage;
}

-(void) loadImageWithFeedItemID:(NSString*) feedItemId{
    NSData* imageData = [[ImageLoader shareInstance] loadImageFromDatabaseByFeedId:feedItemId];
    if(imageData==nil){
        //image not exist in local database, load from SF online
        [[ImageLoader shareInstance] downloadImageWithFeedID:feedItemId delegate:self];
    }else{
        [self applyImage:imageData];
    }
}

-(void) applyImage:(NSData*)imageData{
    UIImage *img = [UIImage imageWithData:imageData];
    if(img!=nil){
        img = [[ImageLoader shareInstance] imageByScalingAndCroppingForSize:self.frame.size image:img];
        if(img!=nil) {
            [self setImage:img];
            self.alpha = 1;
        }
    }
}


@end
