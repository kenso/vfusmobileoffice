//
//  WebImageView.h
//  NextMediaTimeEat
//
//  Created by Developer2 on 29/8/14.
//  Copyright (c) 2014 Laputab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomCaptureManager.h"



/**
 *  @brief Extend of ImageView that handle auto loading SF Image, Cancelling loading, post callback block, etc.
 *
 */
@interface WebImageView : UIImageView<CustomCaptureDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>{
    BOOL isPlayingVideo;
    BOOL showLargeImageWhenLongPressed;
    BOOL showCaptureImageWhenClicked;
    BOOL isLargeImagePopupShown;
    BOOL isCapturePopupShown;
    UIViewController* vc;
    UIView* popupParentView;
    UIView* clickPopupParentView;
    
    
    
}

typedef void (^OnImageChangedBlock)(WebImageView* webImgView, NSString* imgLocalPath);

@property (nonatomic, retain) NSString* feedId;

@property (nonatomic, assign) BOOL imageLoaded;//once system downloaded image, or user changed image, this flag changed to YES
@property (nonatomic, assign) BOOL isVideo;
@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, assign) BOOL forceReloadFromWeb;
@property (nonatomic, retain) NSMutableDictionary* data;
//@property (nonatomic, copy) void (^onClickBlock)(WebImageView* webImgView);
@property (nonatomic, copy) OnImageChangedBlock onImageChangedBlock;
//@property (nonatomic, copy) void (^onLongPressBlock)(WebImageView* webImgView);

@property (nonatomic, strong) UIPopoverController *popOver;
@property (nonatomic, strong) UIPopoverController *logoPopOver;

-(id) initWithFrame:(CGRect)frame;

-(void) enableLongPressPreviewOnParentView:(UIView*)view onViewController:(UIViewController*)v;

-(void) enableClickCaptureOnParentView:(UIView*)view onViewController:(UIViewController*)vc;

-(NSString*) saveImageInDB:(UIImage*)img;


#pragma mark - Main function for loading image.
/**
 *
 *  @brief Load image of a given FeedId, and callback completeBlock after image download process end.
 *
 */
-(void) downloadImageWithFeedID: (NSString*)feedId completeBlock:(void (^) (BOOL success, NSString* feedId, UIImage* img)) completeBlock;

/**
 *
 *  @brief Load image of a given URL, and callback completeBlock after image download process end.
 *
 */
-(void) loadImageWithURL:(NSString*)url completeBlock:(void (^) (WebImageView* imgView, UIImage* img)) completeBlock;


#pragma mark - Private function for download image from Web, save/load image from DB
-(void) saveImageInDatabase:(NSString* )feedId imageData:(NSData*)imgData;

-(UIImage*) loadImageFromDatabaseByFeedId:(NSString*)feedId;



-(void) postImageLoaded:(UIImage*)img ofFeedId:(NSString*)feedId completeBlock:(void (^) (WebImageView* imgView, UIImage* img)) completeBlock;

-(void) loadImageWithFeedID:(NSString*)feedId completeBlock:(void (^) (WebImageView* imgView, UIImage* img)) completeBlock;


@end
