//
//  BatchImageDownloader.h
//  NextMediaCoupon
//
//  Created by Developer 2 on 7/8/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ImageLoaderRequest : NSObject
@end


@protocol ImageLoaderDelegate

-(void) onRequestSuccessOfRequest:(ImageLoaderRequest*)req feedItemID:(NSString*)feedItemId response:(id)jsonResponse contentType:(NSString*)contentType;

-(void) onRequestFailOfRequest:(ImageLoaderRequest*)req feedItemID:(NSString*)feedItemId error:(NSError*)error;

@end



@interface ImageLoader : NSObject



@property (nonatomic, retain) NSString* apiBaseUrl;






+(ImageLoader*) shareInstance;



//Resize given image to fixed size
- (UIImage*)imageByScalingAndCroppingForSize:(CGSize)targetSize image:(UIImage*)sourceImage;

//Async Load image from Network
- (ImageLoaderRequest*) downloadImageWithFeedID: (NSString*)feedId  delegate:(id<ImageLoaderDelegate>) delegate;

//Async Load PDF from Network
-(ImageLoaderRequest*) downloadDocWithFeedID: (NSString*)feedId type:(NSString*)fileExt delegate:(id<ImageLoaderDelegate>) delegate;


//Save image to disk
- (void) saveImageInDatabase:(NSString* )feedId imageData:(NSData*)imgData;

//Load image from disk
- (NSData*) loadImageFromDatabaseByFeedId:(NSString*)feedId;



-(void) saveDocInDatabase:(NSString* )feedId data:(NSData*)data;



-(NSData*) loadDocFromDatabaseByFeedId:(NSString*)feedId;





@end
