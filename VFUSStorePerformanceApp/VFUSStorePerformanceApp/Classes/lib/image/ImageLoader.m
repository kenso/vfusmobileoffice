//
//  BatchImageDownloader.m
//  NextMediaCoupon
//
//  Created by Developer 2 on 7/8/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//


#import "SFRestAPI.h"
#import "SFRestRequest.h"
#import "SFOAuthCoordinator.h"
#import "ImageLoader.h"

@implementation ImageLoaderRequest : NSObject
@end


@implementation ImageLoader




+(ImageLoader*) shareInstance{
    static ImageLoader *instance = nil;
    @synchronized(self) {
        if (instance == nil)
            instance = [[self alloc] init];
    }
    return instance;

}


-(ImageLoaderRequest*) downloadDocWithFeedID: (NSString*)feedId type:(NSString*)fileExt delegate:(id<ImageLoaderDelegate>) delegate{
    ImageLoaderRequest* uniqueRequest = [[ImageLoaderRequest alloc] init];
    
    NSData* data = [self loadDocFromDatabaseByFeedId:[NSString stringWithFormat:@"%@.%@", feedId, fileExt]];
    if(data!=nil){
        dispatch_async(dispatch_get_main_queue(), ^{
            [delegate onRequestSuccessOfRequest:uniqueRequest feedItemID:feedId response:data contentType: fileExt];
        });

        return uniqueRequest;
    }
    
    SFRestAPI* api = [SFRestAPI sharedInstance];
    SFOAuthCoordinator *coord = [SFRestAPI sharedInstance].coordinator;
    NSString* token = coord.credentials.accessToken;
    
    NSOperationQueue *queue = [[[NSOperationQueue alloc] init] autorelease];
    
    NSString* path = [NSString stringWithFormat:@"%@/services/data/%@/sobjects/FeedItem/%@/ContentData/", coord.credentials.instanceUrl.absoluteString, api.apiVersion, feedId];
    
    NSURL *url = [NSURL URLWithString: path ];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    [request setValue: [NSString stringWithFormat:@"OAuth %@", token ] forHTTPHeaderField:@"Authorization"];
    
    NSLog(@"Get binary data from %@", path);
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse* res, NSData* data, NSError* err){
        if(err){
            [delegate onRequestFailOfRequest: uniqueRequest feedItemID:feedId error:err];
            return;
        }
        
        NSLog(@"res=%@", res);
        
        NSDictionary* headers = [(NSHTTPURLResponse *)res allHeaderFields];
        
        NSString* contentType = [headers objectForKey:@"Content-Type" ];
        contentType = [contentType lowercaseString];
        NSArray* parts = [contentType componentsSeparatedByString:@"/"];
        contentType = [parts lastObject];
        
        if (fileExt!=nil){
            NSString* docFeedId = [NSString stringWithFormat:@"%@.%@", feedId, fileExt];
            [[ImageLoader shareInstance] saveDocInDatabase:docFeedId data:data];
            [delegate onRequestSuccessOfRequest:uniqueRequest feedItemID:feedId response:data contentType:fileExt];
        }
        else{
            [[ImageLoader shareInstance] saveDocInDatabase:feedId data:data];
            [delegate onRequestSuccessOfRequest:uniqueRequest feedItemID:feedId response:data contentType:fileExt];

        }
        

    }];
    return uniqueRequest;
}




-(ImageLoaderRequest*) downloadImageWithFeedID: (NSString*)feedId  delegate:(id<ImageLoaderDelegate>) delegate{
    
    ImageLoaderRequest* uniqueRequest = [[ImageLoaderRequest alloc] init];
    
    SFRestAPI* api = [SFRestAPI sharedInstance];
    SFOAuthCoordinator *coord = [SFRestAPI sharedInstance].coordinator;
    NSString* token = coord.credentials.accessToken;
    
    NSOperationQueue *queue = [[[NSOperationQueue alloc] init] autorelease];
    
    NSString* path = [NSString stringWithFormat:@"%@/services/data/%@/sobjects/FeedItem/%@/ContentData/", coord.credentials.instanceUrl.absoluteString, api.apiVersion, feedId];
    
    NSURL *url = [NSURL URLWithString: path ];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    [request setValue: [NSString stringWithFormat:@"OAuth %@", token ] forHTTPHeaderField:@"Authorization"];
    
    NSLog(@"Get binary data from %@", path);
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse* res, NSData* data, NSError* err){
        if(err){
            [delegate onRequestFailOfRequest:uniqueRequest feedItemID:feedId error:err];
            return ;
        }
        [[ImageLoader shareInstance] saveImageInDatabase:feedId imageData:data];
        NSDictionary* headers = [(NSHTTPURLResponse *)res allHeaderFields];
        
        NSString* contentType = [headers objectForKey:@"Content-Type" ];
        contentType = [contentType lowercaseString];

        [delegate onRequestSuccessOfRequest:uniqueRequest feedItemID:feedId response:data contentType:contentType];
    }];
    return uniqueRequest;
    
}

//convert a source image into a new image that cropped and resized with given targetSize
- (UIImage*)imageByScalingAndCroppingForSize:(CGSize)targetSize image:(UIImage*)sourceImage
{
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    
    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor)
        {
            scaleFactor = widthFactor; // scale to fit height
        }
        else
        {
            scaleFactor = heightFactor; // scale to fit width
        }
        
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else
        {
            if (widthFactor < heightFactor)
            {
                thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
            }
        }
    }
    
    UIGraphicsBeginImageContext(targetSize); // this will crop
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    if(newImage == nil)
    {
        NSLog(@"could not scale image");
    }
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    
    return newImage;
}






-(void) saveDocInDatabase:(NSString* )feedId data:(NSData*)data{
    if(data==nil){
        NSLog(@"Save document to local fail. data is null. feedId = %@", feedId);
        return;
    }
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *storePath = [applicationDocumentsDir stringByAppendingPathComponent:feedId];
    BOOL done = [data writeToFile:storePath atomically:YES];
    if(!done){
        NSLog(NSLocalizedString(@"warning.save_local_file", nil));
    }
    else {
        NSLog(@"Save file to local successfully. path=%@", storePath);
    }
}

-(NSData*) loadDocFromDatabaseByFeedId:(NSString*)feedId{
    NSLog(@"loadDocFromDatabaseByFeedId feedId=%@", feedId);
    
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *storePath = [applicationDocumentsDir stringByAppendingPathComponent:feedId];
    NSLog(@"loadDocFromDatabaseByFeedId storePath=%@", storePath);
    NSData *data = [[[NSData alloc] initWithContentsOfURL:[NSURL fileURLWithPath:storePath]] autorelease];
    return data;
}





-(void) saveImageInDatabase:(NSString* )feedId imageData:(NSData*)imgData{
    if(imgData==nil){
        NSLog(@"Save file to local fail. data is null. feedId = %@", feedId);
        return;
    }
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *storePath = [applicationDocumentsDir stringByAppendingPathComponent:feedId];
    BOOL done = [imgData writeToFile:storePath atomically:YES];
    if(!done) {
        NSLog(NSLocalizedString(@"warning.save_local_file", nil));
    }
    else {
        NSLog(@"Save file to local successfully. path=%@", storePath);
    }
}

-(NSData*) loadImageFromDatabaseByFeedId:(NSString*)feedId{
    NSLog(@"loadImageFromDatabaseByFeedId feedId=%@", feedId);

    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *storePath = [applicationDocumentsDir stringByAppendingPathComponent:feedId];
    NSLog(@"loadImageFromDatabaseByFeedId storePath=%@", storePath);
    NSData *imgData = [[[NSData alloc] initWithContentsOfURL:[NSURL fileURLWithPath:storePath]] autorelease];
    return imgData;
}


@end
