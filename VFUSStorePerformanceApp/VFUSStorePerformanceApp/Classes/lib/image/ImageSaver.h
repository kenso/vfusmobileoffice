//
//  ImageSaver.h
//  VF Ordering App
//
//  Created by Developer 2 on 11/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//

#import <Foundation/Foundation.h>


#define SAVE_IMAGE_RESTA_PI_URL @"SharedMediaUploadRestAPI"


@protocol ImageSaverDelegate <NSObject>

-(void) onAllImageUploadedCompleted;
-(void) onImageUploadedCompleted:(NSString*)feedId ;
-(void) onImageUploadedFail:(NSString*)feedId error:(NSError*)error;

@end




@interface ImageSaver : NSObject{
    NSMutableArray* pendingFileDataArray;
    id<ImageSaverDelegate> delegate;
}


+(ImageSaver*) shareInstance;

/**
 *      objId - string
        category - string
        feedId - string
        locaFileName - string
        shouldBeRemoved - bool
 */
-(void) uploadImagesWithFeedItemIDArray:(NSArray*)idAry delegate:(id<ImageSaverDelegate>)dele;


@end
