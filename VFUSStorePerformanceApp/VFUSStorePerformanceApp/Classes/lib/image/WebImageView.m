//
//  WebImageView.m
//  NextMediaTimeEat
//
//  Created by Developer2 on 29/8/14.
//  Copyright (c) 2014 Laputab. All rights reserved.
//
#import <MediaPlayer/MediaPlayer.h>
#import "WebImageView.h"
#import "SFRestAPI+Blocks.h"
#import "SFOAuthCoordinator.h"
#import "CustomCaptureManager.h"
#import "ShareLocale.h"
#import "ImageLoader.h"


@implementation WebImageView
@synthesize logoPopOver;

-(id) initWithCoder:(NSCoder *)aDecoder{
    if(self = [super initWithCoder:aDecoder]){
        [self setup];
    }
    return self;
}


-(id) initWithFrame:(CGRect)frame{
    
    if(self = [super initWithFrame:frame]){
        [self setup];
    }
    return self;
}

-(void) setup{
    self.data = [NSMutableDictionary dictionary];
    self.isVideo = NO;
    
    self.imageLoaded = NO;
    
    UILongPressGestureRecognizer* longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onLongPressFunc:)];
    longPress.minimumPressDuration = 0.5f;
    longPress.allowableMovement = 30.0f;
    [self addGestureRecognizer: longPress];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickFunc:)];
    singleTap.numberOfTapsRequired = 1;
    [self addGestureRecognizer: singleTap];
    self.isLoading = NO;
    showLargeImageWhenLongPressed = NO;
    isLargeImagePopupShown = NO;
    [self setUserInteractionEnabled:YES];
    
    if(self.image==nil){
        self.image = [UIImage imageNamed:@"no_image"];
    }
}

-(void) enableClickCaptureOnParentView:(UIView*)view  onViewController:(UIViewController*)v{
    showCaptureImageWhenClicked = YES;
    clickPopupParentView = view;
    vc = v;
}

-(void) disableClickCaptrue{
    showCaptureImageWhenClicked = NO;
    clickPopupParentView = nil;
}


-(void) enableLongPressPreviewOnParentView:(UIView*)view onViewController:(UIViewController*)v{
    showLargeImageWhenLongPressed = YES;
    popupParentView = view;
    vc = v;
}

-(void) disableLongPressPreview{
    showLargeImageWhenLongPressed = NO;
    popupParentView = nil;
}


-(void) onLongPressFunc:(id)sender{
    if(self.isLoading){
        return;
    }
    if(showLargeImageWhenLongPressed){
        if(!self.isVideo){
            if(!isLargeImagePopupShown){
                [self showLargeImageViewPopup];
            }
        }
        else{
            [self playVideo];
        }
    }
}

-(void) onClickFunc:(id)sender{
    if(self.isLoading){
        return;
    }
    if(showCaptureImageWhenClicked){
        if(!isCapturePopupShown){
            [self showCaptureViewPopup];
        }
    }
//    if(self.onClickBlock!=nil){
//        self.onClickBlock(self);
//    }
}


-(void) loadImageWithURL:(NSString*)url completeBlock:(void (^) (WebImageView* imgView, UIImage* img)) completeBlock{
    NSLog(@"loadImageWithFeedID url=%@",url);
    
    self.isLoading = YES;
    self.feedId = url;
    
    self.image = [UIImage imageNamed:@"loading_image"];

    __block WebImageView* me = self;

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        UIImage* img = nil;
        
        if(!_forceReloadFromWeb){
            NSString* escaped = [WebImageView escapeString:url];
            img = [me loadImageFromDatabaseByFeedId:escaped];
        }
        if(img==nil){
            //load from web
            [self downloadImageWithURL:url completeBlock:^(BOOL success, NSString *url, UIImage *img) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    me.isLoading = NO;
                    [me postImageLoaded:img ofFeedId:url completeBlock:completeBlock];
                });
            }];
        }else{
            //load from DB
            dispatch_async(dispatch_get_main_queue(), ^{
                me.isLoading = NO;
                [me postImageLoaded:img ofFeedId:url completeBlock:completeBlock];
            });
        }
    });
    
}







-(void) loadImageWithFeedID:(NSString*)feedId completeBlock:(void (^) (WebImageView* imgView, UIImage* img)) completeBlock{
    NSLog(@"loadImageWithFeedID feedId=%@",feedId);
    
    self.feedId = feedId;
    
    self.image = [UIImage imageNamed:@"loading_image"];

    __block WebImageView* me = self;

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        UIImage* img = nil;
        
        if(!_forceReloadFromWeb){
            img = [me loadImageFromDatabaseByFeedId:feedId];
        }
        if(img==nil){
            NSData* localData = [me loadDataFromDatabaseByFeedId:feedId];
            if( localData!=nil ){
                //data exist but not valid image
                dispatch_async(dispatch_get_main_queue(), ^{
                    [me postImageLoaded: localData ofFeedId:feedId completeBlock:completeBlock];
                });
            }
            else{
                //load from web
                [me downloadImageWithFeedID:feedId completeBlock:^(BOOL success, NSString *feedId, UIImage *img) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [me postImageLoaded:img ofFeedId:feedId completeBlock:completeBlock];
                    });
                }];
            }
        }else{
            //load from DB
            dispatch_async(dispatch_get_main_queue(), ^{
                [me postImageLoaded:img ofFeedId:feedId completeBlock:completeBlock];
            });            
        }
    });
    
}



+(NSString*) escapeString:(NSString*)unescaped{
    NSString *escapedString = [unescaped stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    return escapedString;
}






#pragma mark - Functions for download image from web, save/load image from DB
-(void) postImageLoaded:(UIImage*)img ofFeedId:(NSString*)feedId completeBlock:(void (^) (WebImageView* imgView, UIImage* img)) completeBlock{
    if([self.feedId isEqualToString:feedId]){
        self.imageLoaded = YES;
        if(!self.isVideo){
            [self setImage:img];
        }else{
            [self setImage:[UIImage imageNamed:@"video"]];
        }
        if(completeBlock!=nil){
            completeBlock(self, img);
        }

    }else{
        NSLog(@"WebImageView.postImageLoaded load complete but feedId not match. Skip futher behavior");
    }
}




-(void) downloadImageWithURL: (NSString*)urlPath completeBlock:(void (^) (BOOL success, NSString* url, UIImage* img)) completeBlock{
    NSLog(@"downloadImageWithURL urlPath=%@",urlPath);
    
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    NSURL *url = [NSURL URLWithString: urlPath ];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse* res, NSData* data, NSError* err){
        if(err){
            if(completeBlock!=nil) {
                completeBlock(NO, urlPath, nil);
            }
            return;
        }

        [self saveImageInDatabase:[WebImageView escapeString: urlPath] imageData:data];
        NSLog(@"WebImageView.downloadImageWithFeedID Save image to local done");
        if(completeBlock!=nil) {
            UIImage *img = (data!=nil)?[UIImage imageWithData:data]:nil;
            completeBlock(YES, urlPath, img);
        }
    }];
    
}



-(void) downloadImageWithFeedID: (NSString*)feedId completeBlock:(void (^) (BOOL success, NSString* feedId, UIImage* img)) completeBlock{
    NSLog(@"WebImageView.downloadImageWithFeedID feedId=%@",feedId);
    
    SFRestAPI* api = [SFRestAPI sharedInstance];
    SFOAuthCoordinator *coord = [SFRestAPI sharedInstance].coordinator;
    NSString* token = coord.credentials.accessToken;
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    NSString* path = [NSString stringWithFormat:@"%@/services/data/%@/sobjects/FeedItem/%@/ContentData/", coord.credentials.instanceUrl.absoluteString, api.apiVersion, feedId];
    
    NSURL *url = [NSURL URLWithString: path ];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    [request setValue: [NSString stringWithFormat:@"OAuth %@", token ] forHTTPHeaderField:@"Authorization"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse* res, NSData* data, NSError* err){
        if(err){
            if(completeBlock!=nil) {
                completeBlock(NO, feedId, nil);
            }
            return;
        }
        [self saveImageInDatabase:feedId imageData:data];
        NSLog(@"WebImageView.downloadImageWithFeedID Save image to local done");
        if(completeBlock!=nil) {
            if(self.isVideo){
                completeBlock(YES, feedId, (UIImage*)data);
            }else{
                UIImage *img = (data!=nil)?[UIImage imageWithData:data]:nil;
                completeBlock(YES, feedId, img);
            }
        }
    }];
    
}




-(void) saveImageInDatabase:(NSString* )feedId imageData:(NSData*)imgData{
    if(imgData==nil){
        NSLog(@"WebImageView.saveImageInDatabase. feedId = %@", feedId);
        return;
    }
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *storePath = [applicationDocumentsDir stringByAppendingPathComponent:feedId];
    
    BOOL done = [imgData writeToFile:storePath atomically:YES];
    if(!done) {
        NSLog(NSLocalizedString(@"warning.save_local_file", nil));
    }
    else {
        NSLog(@"Save file to local successfully. path=%@", storePath);
    }
}


-(NSData*) loadDataFromDatabaseByFeedId:(NSString*)feedId{
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *storePath = [applicationDocumentsDir stringByAppendingPathComponent:feedId];
    NSData *imgData = [[NSData alloc] initWithContentsOfURL:[NSURL fileURLWithPath:storePath]];
    return imgData;
}


-(UIImage*) loadImageFromDatabaseByFeedId:(NSString*)feedId{
    NSLog(@"WebImageView.loadImageFromDatabaseByFeedId feedId=%@", feedId);
    NSData *imgData = [self loadDataFromDatabaseByFeedId:feedId];
    UIImage *img = (imgData!=nil)?[UIImage imageWithData:imgData]:nil;
    
    //Oriented image fix - read back the suggested orientation
//    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    //NSInteger imageOrientation = [[userDefaults objectForKey:@"kImageOrientation"] integerValue];
    //img = [[UIImage alloc] initWithCGImage: img.CGImage scale:1.0 orientation: imageOrientation];
    img = [[UIImage alloc] initWithCGImage: img.CGImage];
    
    return img;
}




#pragma mark - Handle Image Picker or Camera
-(void) showCaptureViewPopup{
    isCapturePopupShown = YES;
    [[CustomCaptureManager shareInstance] setUpPreviewRootView: clickPopupParentView];
    [[CustomCaptureManager shareInstance] resetWithTargetView:self andDelegate:self];
    [self hideCaptureViewPopup];
    
}

-(void) onCompleteCaptureImage:(UIImageView* )targetView{
    if (self.onImageChangedBlock){
        self.imageLoaded = YES;
        self.onImageChangedBlock(self, [self saveImageInDB:targetView.image]);
    }
}

-(void) onChooseImageFromGallery:(UIImageView*)targetView{
    if(vc!=nil){
        UIImagePickerController* picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]){
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:picker];
            [popover presentPopoverFromRect:targetView.bounds inView:targetView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            self.logoPopOver = popover;
            //[vc presentViewController: picker animated:YES completion:nil];
        }
    }
}




-(void) showLargeImageViewPopup{
    isLargeImagePopupShown = YES;
    
    NSMutableDictionary* webData = self.data;
    
    NSInteger size = 600;
    
    UIView* container = [[UIView alloc] initWithFrame: popupParentView.frame];
    [container setBackgroundColor:[UIColor colorWithWhite:0 alpha:.7f]];
    WebImageView* imgView = [[WebImageView alloc] initWithFrame:CGRectMake(popupParentView.frame.size.width/2 - size/2, popupParentView.frame.size.height/2 - size/2, size, size)];
    imgView.backgroundColor = [UIColor whiteColor];
    [imgView loadImageWithFeedID: webData[@"emfa__FeedItemId__c"] completeBlock:^(WebImageView *imgView, UIImage *img) {
        [imgView setContentMode:UIViewContentModeScaleAspectFit];
    }];
    [container addSubview:imgView];
    [popupParentView addSubview: container];
    container.tag = 4567;
    
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideImageViewPopup)];
    singleTap.numberOfTapsRequired = 1;
    [container setUserInteractionEnabled:YES];
    [container addGestureRecognizer:singleTap];
}


-(void) hideImageViewPopup{
    isLargeImagePopupShown = NO;
    [[popupParentView viewWithTag:4567] removeFromSuperview];
}



#pragma mark - ImagePicker Delegate
- (void)imagePickerControllerDidCancel:(UIImagePickerController *) inpicker {
    [vc dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *) inpicker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage* img = [info objectForKey:UIImagePickerControllerOriginalImage];
    self.imageLoaded = YES;
    [self setImage: img];
    
    //update DATA here
    if (self.onImageChangedBlock){
        self.onImageChangedBlock(self, [self saveImageInDB:img]);
    }

    [logoPopOver dismissPopoverAnimated:YES];
    [vc dismissViewControllerAnimated:YES completion:nil];
}


-(void) hideCaptureViewPopup{
    isCapturePopupShown = NO;
}


-(NSString*) saveImageInDB:(UIImage*)img {
    NSData *imgData = UIImageJPEGRepresentation(img, 1);
    
    NSDate* now = [NSDate date];
    
    //Oriented image fix - Store the image orientation
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setInteger:[img imageOrientation] forKey:@"kImageOrientation"];

    
    double d = now.timeIntervalSince1970;
    d = d*1000;
    NSString* timestampIntStr = [NSString stringWithFormat:@"cache_%.0f", (double)(d)];

    [self saveImageInDatabase:timestampIntStr imageData:imgData];
    
    return timestampIntStr;
    
}




-(void) playVideo{
    if(isPlayingVideo){
        return;
    }
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    
//    NSBundle *bundle = [NSBundle mainBundle];
//    NSString *moviePath = [bundle pathForResource:@"HTS3111" ofType:@"mov"];
//    
//    NSURL *movieURL = [NSURL fileURLWithPath:moviePath] ;
//    
//    MPMoviePlayerViewController* moviePlayerController = [[MPMoviePlayerViewController alloc] initWithContentURL: movieURL];
//    moviePlayerController.moviePlayer.movieSourceType = MPMovieSourceTypeFile;
//
//    [vc.navigationController presentMoviePlayerViewControllerAnimated: moviePlayerController];
//
//    isPlayingVideo = YES;
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(doneButtonClick:)
//                                                 name:MPMoviePlayerPlaybackDidFinishNotification
//                                               object:nil];

    
    
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *storePath = [applicationDocumentsDir stringByAppendingPathComponent:self.data[@"emfa__FeedItemId__c"]];
    
    
    storePath = [self createFileAliasWithPath:storePath];
    
    if(storePath!=nil){
        NSURL* url = [NSURL fileURLWithPath:storePath];
        if(url!=nil){
            MPMoviePlayerViewController* moviePlayerController = [[MPMoviePlayerViewController alloc] initWithContentURL: url];
            moviePlayerController.moviePlayer.movieSourceType = MPMovieSourceTypeUnknown;

            [vc.navigationController presentMoviePlayerViewControllerAnimated: moviePlayerController];
            
            isPlayingVideo = YES;
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(doneButtonClick:)
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:nil];

        }
    }
    
}


-(NSString*) createFileAliasWithPath:(NSString*)path{
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSString *slink = [path stringByAppendingPathExtension: self.data[@"emfa__File_Ext__c"]];
    if (![filemgr fileExistsAtPath:slink]) {
        NSError *error = nil;
        [filemgr createSymbolicLinkAtPath: slink withDestinationPath: path error: &error];
        if (error) {
            if(error.code == 516){
                NSLog(@"Generate symbolic success, slink=%@", slink);
                return slink;
            }
            return nil;
        }else{
            NSLog(@"Generate symbolic success, slink=%@", slink);
            return slink;
        }
    }
    return slink;
}


-(void) doneButtonClick:(id)sender{
    isPlayingVideo = NO;
    
}

#pragma UIImagePicker
-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    [viewController.navigationItem setTitle: @"" ];
}


@end
