//
//  ImageSaver.m
//  VF Ordering App
//
//  Created by Developer 2 on 11/12/13.
//  Copyright (c) 2013 Laputab. All rights reserved.
//
#include <math.h>
#include <stdio.h>


#import "VFDAO.h"
#import "ImageSaver.h"
#import "ImageLoader.h"
#import "SFRestAPI.h"
#import "SFOAuthCoordinator.h"

@implementation ImageSaver





+(ImageSaver*) shareInstance{
    static ImageSaver *instance = nil;
    @synchronized(self) {
        if (instance == nil)
            instance = [[self alloc] init];
    }
    return instance;
    
}



-(id) init{
    pendingFileDataArray = [[NSMutableArray alloc] init];
    return self;
}

-(void)dealloc{
    [pendingFileDataArray release];
    [super dealloc];
}



-(void) uploadImagesWithFeedItemIDArray:(NSArray*)dataAry delegate:(id<ImageSaverDelegate>)dele{
    delegate = dele;
    [pendingFileDataArray removeAllObjects];
    [pendingFileDataArray addObjectsFromArray:dataAry];
    
    [self processNextDocument];
}


-(NSData*) resizeImageData:(NSData*) originalData toWidth:(NSInteger)w height:(NSInteger)h{
    UIImage* originalImg =  [UIImage imageWithData:originalData ];
    
    float actualHeight = originalImg.size.height;
    float actualWidth = originalImg.size.width;
    
    float ratioX = w / actualWidth;
    float ratioY = h / actualHeight;
    float ratio = MIN(ratioX, ratioY);
    float newWidth = actualWidth * ratio;
    float newHeight = actualHeight * ratio;
    
    CGContextRef bitmap;
    CGImageRef imageRef = [originalImg CGImage];
    CGBitmapInfo bitmapInfo = CGImageGetBitmapInfo(imageRef);
	CGColorSpaceRef colorSpaceInfo = CGImageGetColorSpace(imageRef);
    if (bitmapInfo == kCGImageAlphaNone) {
        bitmapInfo = kCGImageAlphaNoneSkipLast;
    }
    int t1 = CGImageGetBitsPerComponent(imageRef);
    int t2 = 0;//CGImageGetBytesPerRow(imageRef);
    
    if (originalImg.imageOrientation == UIImageOrientationUp || originalImg.imageOrientation == UIImageOrientationDown) {
        //take photo run here
        bitmap = CGBitmapContextCreate(NULL, newWidth, newHeight, t1, t2, colorSpaceInfo, bitmapInfo);
        
    } else {
        bitmap = CGBitmapContextCreate(NULL, newHeight, newWidth, t1, t2, colorSpaceInfo, bitmapInfo);
    }
    
    if (originalImg.imageOrientation == UIImageOrientationLeft) {
        CGContextRotateCTM (bitmap, M_PI_2);
        CGContextTranslateCTM (bitmap, 0, -newHeight);
        
    } else if (originalImg.imageOrientation == UIImageOrientationRight) {
        CGContextRotateCTM (bitmap, - M_PI_2);
        CGContextTranslateCTM (bitmap, -newWidth, 0);
        
    } else if (originalImg.imageOrientation == UIImageOrientationUp) {
        // NOTHING
    } else if (originalImg.imageOrientation == UIImageOrientationDown) {
        CGContextTranslateCTM (bitmap, newWidth, newHeight);
        CGContextRotateCTM (bitmap, - M_PI);
    }

    CGContextDrawImage(bitmap, CGRectMake(0, 0, newWidth, newHeight), imageRef);
    CGImageRef ref = CGBitmapContextCreateImage(bitmap);
    UIImage* newImg = [UIImage imageWithCGImage:ref];
    
    CGContextRelease(bitmap);
    CGImageRelease(ref);

    
    NSData* d = UIImageJPEGRepresentation(newImg, .8f);
    if(d==nil){
        return UIImagePNGRepresentation(newImg);
    }else{
        return d;
    }
}



-(void)processNextDocument{
    if([pendingFileDataArray count]==0){
        [self performSelectorOnMainThread:@selector(callbackAllSuccessDelegate) withObject:nil waitUntilDone:NO];
        return;
    }
    
    NSDictionary* imgData = [pendingFileDataArray objectAtIndex:0];
    
    NSString* objId = [[imgData objectForKey:@"objId"] copy];
    NSString* category = [[ [imgData objectForKey:@"category"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] copy];
    NSString* feedId = [[imgData objectForKey:@"feedId"] copy];
    NSString* localFileName = [[imgData objectForKey:@"localFileName"] copy];
    [objId autorelease];
    [category autorelease];
    [feedId autorelease];
    [localFileName autorelease];
    bool shouldBeRemoved = [[imgData objectForKey:@"shouldBeRemoved"] boolValue];

    [pendingFileDataArray removeObjectAtIndex:0];
    
 
    if ( feedId==nil || [feedId isEqualToString:@""] || shouldBeRemoved){
        
        SFOAuthCoordinator *coord = [SFRestAPI sharedInstance].coordinator;
        NSString* token = coord.credentials.accessToken;
        
        NSOperationQueue *queue = [[[NSOperationQueue alloc] init] autorelease];
        NSString* Longpath = [NSString stringWithFormat:@"%@/services/apexrest/%@?objId=%@&category=%@", coord.credentials.instanceUrl.absoluteString, SAVE_IMAGE_RESTA_PI_URL, objId, category ];
        if(shouldBeRemoved){
            Longpath = [NSString stringWithFormat:@"%@&del=true", Longpath];
        }
        
        
        VFDAO* vf = [VFDAO sharedInstance];
        if( ![vf isOnlineMode] ){
            NSDictionary* d = @{
                                @"objId":objId,
                                @"category":category,
                                @"localFileName":localFileName};
            //actually useless START
            [vf runStandardUpsertTable:TABLE_PENDING_SHARED_MEDIA records:@[d]
                                                                spec:@[ @{
                                                                         @"path": @"objId",
                                                                         @"type": @"string"},
                                                                        ] externalPath:@"objId"];
            //END
            [self performSelectorOnMainThread:@selector(callbackSuccessDelegate:) withObject:d waitUntilDone:NO];
            [self processNextDocument];
            return;
        }
        
        NSLog(@"Try access url = %@", Longpath);
        
        NSURL *url = [NSURL URLWithString: Longpath ];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"PUT"];
        [request setValue: [NSString stringWithFormat:@"OAuth %@", token ] forHTTPHeaderField:@"Authorization"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        if(!shouldBeRemoved){
            NSData* localData = [[ImageLoader shareInstance] loadImageFromDatabaseByFeedId:localFileName];
            localData = [self resizeImageData: localData toWidth:1024 height:768];
            [request setHTTPBody: localData];
        }

        __block ImageSaver* me = self;
        __block NSString* blockObjId = [objId copy];
        __block NSString* blockFeedId = [feedId copy];
        __block NSString* blockCategory = [category copy];
        [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse* res, NSData* data, NSError* err){
            
            if(err){
                NSLog(@"Insert with error: %@", err);
                [me performSelectorOnMainThread:@selector(callbackFailDelegate:) withObject:@{ @"feedId" : blockFeedId,
                                                                                                 @"error" : err
                                                                                                } waitUntilDone:NO];
                return;
            }
            else if ( [res isKindOfClass:[NSHTTPURLResponse class]]){
                //when there is VF,
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*) res;
                if (httpResponse.statusCode >= 400){
                    NSLog(@"Insert with error: %@", err);
                    [me performSelectorOnMainThread:@selector(callbackFailDelegate:) withObject:@{ @"feedId" : @"",
                                                                                                   @"error" : [NSString stringWithFormat: @"Error Code: %ld",(long)httpResponse.statusCode ]
                                                                                                   } waitUntilDone:NO];
                }else{
                    [me performSelectorOnMainThread:@selector(callbackSuccessDelegate:) withObject:@{
                                                                                                     @"objId": blockObjId,
                                                                                                     @"category": blockCategory
                                                                                                     } waitUntilDone:NO];
                }
            }
            else{
                NSLog(@"Upload done");
                [me performSelectorOnMainThread:@selector(callbackSuccessDelegate:) withObject:@{
                                                                                                   @"objId": blockObjId,
                                                                                                   @"category": blockCategory
                                                                                                   } waitUntilDone:NO];
            }
            [me processNextDocument];
        }];
    }else{
        NSLog(@"skip upload");

        //just skip it because not modified
        [self processNextDocument];
    }

}

-(void) callbackFailDelegate:(NSDictionary*) data{
    id<ImageSaverDelegate> saveDelegate = delegate;
    delegate = nil;
    [pendingFileDataArray removeAllObjects];
    [saveDelegate onImageUploadedFail:[data objectForKey:@"feedId"] error:[data objectForKey:@"error"]];
}

-(void) callbackSuccessDelegate:(NSDictionary*)data{
    VFDAO* vf = [VFDAO sharedInstance];
    if( ![vf isOnlineMode] && data[@"localFileName"]!=nil && data[@"objId"]!=nil && data[@"localFileName"]!=[NSNull null] && data[@"objId"]!=[NSNull null]){
        
        NSMutableDictionary* localComment = [ [NSMutableDictionary alloc] initWithDictionary: [vf loadRecordFromTable:DB_TABLE_COMMENT ofId: data[@"objId"]] ];
        if(localComment!=nil){
            localComment[@"Shared_Media__r"] = @{
                                         @"done" : @1,
                                         @"records": @[@{@"emfa__FeedItemId__c":data[@"localFileName"], @"emfa__File_Ext__c":@"jpg"}]
                                         };
            [vf runStandardUpsertTable:DB_TABLE_COMMENT records:@[localComment] spec:DB_SPEC_COMMENT];

        }
        
    }
}

-(void) callbackAllSuccessDelegate{
    id<ImageSaverDelegate> saveDelegate = delegate;
    delegate = nil;
    [saveDelegate onAllImageUploadedCompleted];
}

@end
